#!/bin/sh
#
#	Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
#

prefix="@prefix@"

dclversion="@DCLVERSION@"
dclvernum="@DCLVERNUM@"
dcllang="@DCLLANG@"

bindir="@BINDIR@"
libdir="@LIBDIR@"
incdir="@INCDIR@"

dcllibname="@DCLLIBNAME@"
dcllibfile="@DCLLIBFILE@"
dcllibopt="@DCLLIBOPT@"
dcllibpath="@DCLLIBPATH@"

dbasename="@DBASENAME@"
dbasedir="@DBASEDIR@"

xincpath="@XINCPATH@"
xlibpath="@XLIBPATH@"
xlibopt="@XLIBOPT@"

cc="@CC@"
cflags="@CFLAGS@"
fc="@FC@"
fflags="@FFLAGS@"

ldflags="@LDFLAGS@"
ldlibs="@LDLIBS@"

gtkversion="@GTKVERSION@"

usage()
{
	cat <<EOF
Usage: dclconfig [OPTIONS]
Options: You may consult most of macro variables in Mkinclude file, such as
  --prefix, --dclversion, --dclvernum, --dcllang, --bindir, --libdir,
  --incdir, --dcllibname, --dcllibfile, --dcllibopt, --dcllibpath,
  --dbasename, --dbasedir, --xincpath, --xlibpath, --xlibopt, --cc, 
  --cflags, --fc, --fflags, --ldflags, --ldlibs, --gtkversion
EOF
	exit $1
}

if test $# -eq 0; then
	usage 1 1>&2
fi

while test $# -gt 0; do
  case $1 in
    --prefix)
      echo $prefix
      ;;
    --dclversion)
      echo $dclversion
      ;;
    --dclvernum)
      echo $dclvernum
      ;;
    --dcllang)
      echo $dcllang
      ;;
    --bindir)
      echo $bindir
      ;;
    --libdir)
      echo $libdir
      ;;
    --incdir)
      echo $incdir
      ;;
    --dcllibname)
      echo $dcllibname
      ;;
    --dcllibfile)
      echo $dcllibfile
      ;;
    --dcllibopt)
      echo $dcllibopt
      ;;
    --dcllibpath)
      echo $dcllibpath
      ;;
    --dbasename)
      echo $dbasename
      ;;
    --dbasedir)
      echo $dbasedir
      ;;
    --xincpath)
      echo $xincpath
      ;;
    --xlibpath)
      echo $xlibpath
      ;;
    --xlibopt)
      echo $xlibopt
      ;;
    --cc)
      echo $cc
      ;;
    --cflags)
      echo $cflags
      ;;
    --fc)
      echo $fc
      ;;
    --fflags)
      echo $fflags
      ;;
    --ldflags)
      echo $ldflags
      ;;
    --ldlibs)
      echo $ldlibs
      ;;
    --gtkversion)
      echo $gtkversion
      ;;
    *)
      usage 1 1>&2
      ;;
  esac
  shift
done

