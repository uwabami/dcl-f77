'-----------------------------------
	Option Explicit

	Dim strKeyroot
	Dim strKeyname
	Dim KeySet
	Dim MaxVersion
	Dim strGFDCroot
	Dim strGFDCname

	Dim ENV
	Dim package
	Dim VFPATH
	Dim VFENV 
	Dim FC 

	Dim WSHShell
	Dim objFSO      ' FileSystemObject
	Dim objFile     ' 繝輔ぃ繧、繝ォ譖ク縺崎セシ縺ソ逕ィ

	Dim strDCLValue
	Dim strVFValue

	Dim Keyname
	Dim Ret


	FC = "ifort /MD "
	VFENV = "ifortvars.bat"

'Set Registry Key Root

	Set KeySet = CreateObject("Scripting.Dictionary")
	If Is64() then
		strKeyroot = "SoftWare\Wow6432Node\Intel\Compilers\Fortran"
		strGFDCroot = "Software\Wow6432Node\GFD_Dennou_Club\Fortran"
	Else
		strKeyroot = "SoftWare\Intel\Compilers\Fortran"	
		strGFDCroot = "Software\GFD_Dennou_Club\Fortran"
	End If
	KeySet = GetRegValueArray(strKeyroot)
	MaxVersion = GetMaxVersion(KeySet)
	strKeyroot = strKeyroot & "\" & MaxVersion


' 莉・荳九?繧ゅ?繧定ェュ縺ソ蜃コ縺励※SETENV.BAT 	縺ォ譖ク縺榊?縺励※縺?¥縺ィ蜷梧凾縺ォ
' 迺ー蠅?、画焚縺ォ險ュ螳壹☆繧具シ?
	strDCLValue = Array("DLIB", "DWBIN", "GL:DSPATH", "GTKPATH")
	strVFValue = Array("FC", "VFENV", "VFPATH")

	Set objFSO = WScript.CreateObject("Scripting.FileSystemObject")
    Set objFile = objFSO.OpenTextFile("setenv.bat", 2, True)

	For Each Keyname In strDCLValue
		Ret = GetRegValue(strGFDCroot,Keyname)
	    objFile.Write("set " & Keyname & "=" & Ret & vbCrLf)
	Next

' GTK 髢「騾」
objFile.Write("@set GTK_INC=%GTKPATH%\include;%GTKPATH%\include\gtk-2.0;%GTKPATH%\include\glib-2.0;%GTKPATH%\include\pango-1.0;%GTKPATH%\include\atk-1.0;%GTKPATH%\include\cairo;%GTKPATH%\include\fontconfig;%GTKPATH%\include\freetype2;%GTKPATH%\include\gail-1.0;%GTKPATH%\include\gdk-pixbuf-2.0;%GTKPATH%\include\gio-win32-2.0;%GTKPATH%\include\libpng14;%GTKPATH%\include\pixman-1;%GTKPATH%\lib\gtk-2.0\include;%GTKPATH%\lib\glib-2.0\include;%INCLUDE%" & vbCrLf)
	objFile.Write("@SET INCLUDE=%INCLUDE%;%GTK_INC%" & vbCrLf)
	objFile.Write("@SET GTK_LIB=%GTKPATH%\lib" & vbCrLf)
	objFile.Write("@SET LIB=%LIB%;%GTK_LIB%" & vbCrLf)
	objFile.Write("@SET GTKLIB_NAME=pango-1.0.lib pangoft2-1.0.lib pangowin32-1.0.lib atk-1.0.lib gdk_pixbuf-2.0.lib gdk-win32-2.0.lib gtk-win32-2.0.lib glib-2.0.lib gmodule-2.0.lib gobject-2.0.lib gthread-2.0.lib iconv.lib" & vbCrLf)
	objFile.Write("set " & "PATH" & "=" & "%PATH%;%GTKPATH%\bin" & vbCrLf)

'莉翫?繝舌?繧ク繝ァ繝ウ縺ァ縺ッ縺昴≧縺ェ縺」縺ヲ縺?↑縺?′縲（fort 縺悟ョ溯。後〒縺阪↑縺?→縺阪□縺?'險ュ螳壹☆繧九ｈ縺?↓縺励◆縺??

	VFPATH = GetRegValue(strKeyroot,"ProductDir")
	
	objFile.Write("set " & "FC" & "=" & FC & vbCrLf)
	objFile.Write("set " & "PATH" & "=" & "%PATH%;%DWBIN%" & vbCrLf)
	objFile.Write("@IF ""%1""==""MACRO"" goto macro" & vbCrLf)
	objFile.Write("@goto comline"& vbCrLf)
	objFile.Write(":macro"& vbCrLf)
	objFile.Write("call """ & VFPATH & "\BIN\" & VFENV & _
                  """" & " ia32" & vbCrLf)
	objFile.Write("goto end" & vbCrLf)

	objFile.Write(":comline"& vbCrLf)
	objFile.Write("%comspec% /k """ & VFPATH & "\BIN\" & _
                  VFENV & """" & " ia32" & vbCrLf)
	objFile.Write(":end" & vbCrLf)

'Get EnvironmentValue

''	strKeyname = "ProductDir"
''	Ret = GetRegValue(strKeyroot,strKeyname)

''	strGFDCname = "DLIB"
''	Ret = GetRegValue(strGFDCroot,strGFDCname)
'繝輔ぃ繧、繝ォ縺ォ蜃コ蜉?"set " & strGFDCname & "=" & Ret


    objFile.Close



' 繝輔ぃ繧、繝ォ繝上Φ繝峨Ν繧偵け繝ュ繝シ繧コ縺吶ｋ縲?Set objFile = Nothing
Set objFSO = Nothing




'縺薙?蜈医↓髢「謨ー繧剃クヲ縺ケ縺ヲ縺?¥

Function GetRegValue(strKeyroot,strKeyname)
	Dim WshShell

	Set WshShell = WScript.CreateObject("WScript.Shell")
	GetRegValue = WshShell.RegRead("HKLM" & "\" & strKeyroot & "\" & strKeyname)
	Set WshShell = Nothing
End Function

Function GetRegValueArray(strKeyname)
	Dim Wcls
	Dim Locator
	Dim Service
	Dim Ret
	Dim KeySet
	Dim Key

	Dim MaxVersion
	Dim Version

	Const HKEY_CURRENT_USER = &H80000001
	Const HKEY_LOCAL_MACHINE = &H80000002

	Set Locator = WScript.CreateObject	("WbemScripting.SWbemLocator")
	Set Service = Locator.ConnectServer(vbNullString, "root\default")
	Set Wcls = Service.Get("StdRegProv")
	Wcls.EnumKey HKEY_LOCAL_MACHINE, strKeyname ,KeySet

	GetRegValueArray=KeySet

	Set Wcls = Nothing
	Set Service = Nothing
	Set Locator = Nothing
End Function

Function Is64()
	Dim softwares
	Dim Key
	Dim SystemBits

	SystemBits = False
	Set softwares = CreateObject("Scripting.Dictionary")
	softwares = GetRegValueArray("Software")
	For Each Key In softwares
		If Key = "Wow6432Node"	Then 
			SystemBits = True
		End If
	Next
	Is64 = SystemBits

End Function

Function GetMaxVersion(KeySet)
	Dim MaxVersion
	Dim Key
	Dim Ret
	Dim Version

	MaxVersion = 0
	For Each Key In KeySet
   		Ret = Ret & Key & vbCrLf

		If IsNumeric(Key) Then
			Version = CSng(Key)
		Else
			Version = 0
		End If

		If Version > MaxVersion Then
		    MaxVersion = Version
		End If
	Next

	GetMaxVersion = Version

end Function
