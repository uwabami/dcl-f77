      PROGRAM UXYZ3

      PARAMETER( ID0=19960101, ND=90, RND=ND )

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN( IWS )
      CALL GRFRM

      CALL GRSWND( 0.0, RND, -90., 90. )
      CALL GRSVPT( 0.2, 0.8,  0.2, 0.8 )
      CALL GRSTRN( 1 )
      CALL GRSTRF

      CALL UCXACL( 'B', ID0, ND )
      CALL UCXACL( 'T', ID0, ND )

      CALL UYAXDV( 'L', 10., 30. )
      CALL UYAXDV( 'R', 10., 30. )
      CALL UYSTTL( 'L', 'LATITUDE', 0. )

      CALL UXMTTL( 'T', 'UCXACL/UYAXDV', 0. )

      CALL GRCLS

      END
