      PROGRAM QUICK1

      PARAMETER( NMAX=50 )
      REAL X(0:NMAX), Y(0:NMAX)

*-- ロジスティク模型 ----
      R    = 3.7
      X(0) = 0.0
      Y(0) = 0.5
      DO 10 N=0,NMAX-1
        X(N+1) = X(N) + 1.0
        Y(N+1) = R*Y(N)*(1.-Y(N))
   10 CONTINUE

*-- グラフ ----
      CALL GROPN( 1 )
      CALL GRFRM
      CALL USGRPH( NMAX+1, X, Y )
      CALL GRCLS

      END
