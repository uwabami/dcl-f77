      PROGRAM KIHON2

      PARAMETER( NMAX=40, IMAX=4 )
      REAL X(0:NMAX), Y(0:NMAX,IMAX)

      DT = 4.* 3.14159 / NMAX
      DO 20 N=0,NMAX
        X(N) = REAL(N)/REAL(NMAX)
        DO 10 I=1,IMAX
          Y(N,I) = 0.2*SIN(N*DT) + 1. - 0.2*I
   10   CONTINUE
   20 CONTINUE

      CALL SWCSTX('FNAME','KIHON2')
      CALL SWLSTX('LSEP',.TRUE.)

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL SGOPN( IWS )

*-- ラインインデクス: frame 1 --
      CALL SGFRM
      CALL SLPVPR( 1 )

      CALL SGPLV( NMAX+1, X, Y(0,1) )

      DO 30 I=2,IMAX
        CALL SGSPLI( 2*I )
        CALL SGPLV( NMAX+1, X, Y(0,I) )
   30 CONTINUE

      CALL SGSPLI( 1 )

*-- ラインタイプ: frame 2 --
      CALL SGFRM
      CALL SLPVPR( 1 )

      CALL SGPLV( NMAX+1, X, Y(0,1) )

      DO 40 I=2,IMAX
        CALL SGSPLT( I )
        CALL SGPLV( NMAX+1, X, Y(0,I) )
   40 CONTINUE

      CALL SGCLS

      END
