      PROGRAM KIHON5

      PARAMETER( NMAX=40, IMAX=4 )
      REAL X(0:NMAX,IMAX), Y(0:NMAX,IMAX), XC(IMAX), YC(IMAX)

      DATA XC/0.25, 0.75, 0.25, 0.75/
      DATA YC/0.75, 0.75, 0.25, 0.25/

      DT = 2.* 3.14159 / 6
      DO 10 N=0,5
      DO 10 I=1,IMAX-1
        X(N,I) = 0.2*COS(N*DT) + XC(I)
        Y(N,I) = 0.2*SIN(N*DT) + YC(I)
   10 CONTINUE

      DT = 4.* 3.14159 / NMAX
      DO 20 N=0,NMAX
        X(N,IMAX) = 0.4*REAL(N)/REAL(NMAX) - 0.2 + XC(IMAX)
        Y(N,IMAX) = 0.2*SIN(N*DT)                + YC(IMAX)
   20 CONTINUE

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL SGOPN( IWS )
      CALL SGLSET( 'LSOFTF', .TRUE. )
      CALL SGFRM

*-- デフォルト ----
      CALL SGTNV( 6, X(0,1), Y(0,1) )

*-- トーンパターン ----
      CALL SGSTNP( 3 )
      CALL SGTNV( 6, X(0,2), Y(0,2) )

      CALL SGSTNP( 601 )
      CALL SGTNV( 6, X(0,3), Y(0,3) )

*-- ねじれた多角形 ----
      CALL SGTNV( NMAX+1, X(0,4), Y(0,4) )

      CALL SGCLS

      END
