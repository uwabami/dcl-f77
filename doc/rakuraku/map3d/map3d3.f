      PROGRAM MAP3D3
      PARAMETER( NX=16, NY=16 )
      PARAMETER( XMIN=120, XMAX=150, YMIN=20, YMAX=50 )
      PARAMETER( FACT=0.2 )
      REAL U(NX,NY), V(NX,NY), ALON(NX), ALAT(NY)

      DO 10 I=1,NX
        ALON(I) = XMIN + (XMAX-XMIN)*(I-1)/(NX-1)
   10 CONTINUE
      DO 20 J=1,NY
        ALAT(J) = YMIN + (YMAX-YMIN)*(J-1)/(NY-1)
   20 CONTINUE

      DO 30 J=1,NY
      DO 30 I=1,NX
        U(I,J) =   (J-1-(NY-1)/2.) * FACT
        V(I,J) = - (I-1-(NX-1)/2.) * FACT
   30 CONTINUE

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN( IWS )
      CALL GRFRM
      CALL SGLSET( 'LCLIP', .TRUE. )

      CALL GRSWND( XMIN, XMAX, YMIN, YMAX )
      CALL GRSVPT( 0.1, 0.9, 0.1, 0.9 )
      CALL GRSTRN( 10 )
      CALL UMPFIT
      CALL GRSTRF

      CALL SLPVPR( 3 )
      CALL UMRSET( 'DGRIDMJ', 10. )
      CALL UMRSET( 'DGRIDMN',  2. )
      CALL UMPMAP( 'coast_world' )
      CALL UMPGLB

      CALL SGSLAI( 3 )
      DO 40 J=1,NY
      DO 40 I=1,NX
        CALL SGLAU( ALON(I), ALAT(J), ALON(I)+U(I,J), ALAT(J)+V(I,J) )
   40   CONTINUE

      CALL GRCLS

      END
