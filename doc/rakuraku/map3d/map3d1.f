      PROGRAM MAP3D1

      PARAMETER( NP=14 )
      INTEGER   ITR(NP)
      REAL      FCT(NP)
      CHARACTER CTTL*32

      DATA ITR /   10,   11,   12,   13,   14,   15,
     +             20,   21,   22,   23,   30,   31,   32,   33/
      DATA FCT / 0.12, 0.12, 0.14, 0.14, 0.14, 0.14,
     +           0.11, 0.16, 0.12, 0.12, 0.40, 0.12, 0.12, 0.17/

      CALL SWCSTX('FNAME','map3d1')
      CALL SWLSTX('LSEP',.TRUE.)

      WRITE(*,*) ' WORKSTATION IS (I) ? ;'

      CALL SWCSTX('FNAME','MAP3D1')
      CALL SWLSTX('LSEP',.TRUE.)

      CALL SGPWSN
      READ (*,*) IWS

      CALL SGOPN( -ABS(IWS) )

      CALL SLRAT( 2., 3. )
      CALL SLDIV( 'Y', 2, 3 )

      CALL SGLSET( 'LCLIP', .TRUE. )
      CALL SGRSET( 'STLAT1', 45. )
      CALL SGRSET( 'STLAT2', 30. )

      CALL UMLSET( 'LGRIDMN', .FALSE. )
      CALL UMISET( 'INDEXMJ', 1 )

      CALL SGSTXS( 0.035 )
      CALL SGSTXI( 3 )

      DO 10 I=1,NP
        CALL SGFRM

        CALL SGSVPT( 0.1, 0.9, 0.1, 0.9 )
        CALL SGSSIM( FCT(I), 0., 0. )
        CALL SGSMPL( 0., 90., 0. )
        IF ( ITR(I).EQ.30 ) THEN
          CALL SGSTXY( -180., 180.,   0., 90. )
        ELSE
          CALL SGSTXY( -180., 180., -90., 90. )
        END IF
        CALL SGSTRN( ITR(I) )
        CALL SGSTRF

        CALL SLPWWR( 1 )
        CALL SLPVPR( 1 )
        CALL SGTRNL( ITR(I), CTTL )
        CALL SGTXR( 0.5, 0.95, CTTL )
        CTTL = 'ITR='
        WRITE(CTTL(5:6),'(I2)') ITR(I)
        CALL SGTXR( 0.5, 0.05, CTTL )

        CALL UMPMAP( 'coast_world' )
        CALL UMPGLB

        IF ( ITR(I).EQ.23 ) THEN
          CALL SGFRM
          CALL SGFRM
        END IF
   10 CONTINUE

      CALL SGCLS

      END
