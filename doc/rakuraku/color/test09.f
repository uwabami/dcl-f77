*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM TEST09

      PARAMETER ( NP=14 )
      PARAMETER ( NX=73, NY=37 )
      PARAMETER ( XMIN=0, XMAX=360, YMIN=-90, YMAX=90 )

      REAL      FCT(NP), P(NX,NY)
      CHARACTER CTTL*32, CTR(NP)*3

      EXTERNAL  ISGTRC

      DATA CTR /'CYL','MER','MWD','HMR','EK6','KTD',
     +          'CON','COA','COC','BON',
     +          'OTG','PST','AZM','AZA'/
      DATA FCT / 0.12, 0.12, 0.14, 0.14, 0.14, 0.14,
     +           0.11, 0.16, 0.12, 0.12,
     +           0.40, 0.12, 0.12, 0.17/


      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      OPEN(11,FILE='t811231.dat',FORM='FORMATTED')
      DO 10 J=1,NY
        READ(11,'(10F8.3)') (P(I,J),I=1,NX)
   10 CONTINUE
      CLOSE(11)

      DO 20 IR=190,245,5
        R=IR
        AMIN=R
        AMAX=R+5
*       IDX=(R-180)*1.4*1000+999
        IDX=INT((R-170)*1.25)*1000+999
        CALL UESTLV(AMIN,AMAX,IDX)
   20 CONTINUE 

      CALL SGLSET( 'LSOFTF', .TRUE. )
      CALL SWLSET( 'LCMCH', .TRUE. )
      CALL SGRSET( 'STLAT1', 75.0 )
      CALL SGRSET( 'STLAT2', 60.0 )
      CALL UMLSET( 'LGRIDMJ', .FALSE. )
      CALL UMRSET( 'DGRIDMN', 30.0 )

      CALL SGQCMN(NN)

      CALL SGSCMN(1)

      CALL SGOPN( IWS )

      DO 30 I=1,NP

        ICN=MOD(I-1,NN)+1
        CALL SGSCMN(ICN)

        CALL SGFRM

        CALL SGSSIM( FCT(I), 0.0, 0.0 )
        CALL SGSMPL( 165.0, 60.0, 0.0 )
        CALL SGSVPT( 0.1, 0.9, 0.1, 0.9 )
        CALL SGSWND(XMIN, XMAX, YMIN, YMAX)
        IF (CTR(I).EQ.'OTG') THEN
          CALL SGSTXY( -180.0, 180.0,   0.0, 90.0 )
        ELSE
          CALL SGSTXY( -180.0, 180.0, -90.0, 90.0 )
        END IF
        CALL SGSTRN( ISGTRC(CTR(I)) )
        CALL SGSTRF

        CALL SGLSET( 'LCLIP', .TRUE. )
        CALL SLPWWR( 1 )
        CALL SLPVPR( 1 )
        CALL SGTRNL( ISGTRC(CTR(I)), CTTL )
        CALL SGTXZR( 0.5, 0.95, CTTL, 0.03, 0, 0, 3 )

        CALL UETONE( P, NX, NX, NY )
        CALL UDCNTR( P, NX, NX, NY )
        CALL UMFMAP( 'coast_world' )
        CALL UMPMAP( 'coast_world' )
        CALL UMPGLB
        CALL SLPVPR( 1 )

        CALL SWPCLS

   30 CONTINUE

      CALL SGCLS

      END
