      PROGRAM MISS1

      PARAMETER( NMAX=40 )
      PARAMETER( PI=3.14159 )
      PARAMETER( XMIN=0., XMAX=4*PI, YMIN=-1., YMAX=1. )
      REAL X(0:NMAX), Y(0:NMAX)

      DT = XMAX/NMAX
      DO 10 N=0,NMAX
        X(N) = N*DT
        Y(N) = SIN(X(N))
   10 CONTINUE

      N1 = NMAX/4
      Y(N1-1) = 999.
      Y(N1  ) = 999.
      Y(N1+1) = 999.

      N2 = N1*3
      Y(N2-1) = 999.
      Y(N2+1) = 999.

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN( IWS )
      CALL GRFRM

*-- 欠損値処理なし ----
      CALL GRSWND( XMIN, XMAX, YMIN, YMAX )
      CALL GRSVPT(  0.1,  0.9,  0.7,  0.9 )
      CALL GRSTRN( 1 )
      CALL GRSTRF

      CALL SGPLU( NMAX+1, X, Y )

      CALL GRSWND( XMIN, XMAX, YMIN, YMAX )
      CALL GRSVPT(  0.1,  0.9,  0.5,  0.7 )
      CALL GRSTRN( 1 )
      CALL GRSTRF

      CALL SGSPMT( 3 )
      CALL SGPMU( NMAX+1, X, Y )

*-- 欠損値処理 ----
      CALL GLLSET( 'LMISS', .TRUE. )

      CALL GRSWND( XMIN, XMAX, YMIN, YMAX )
      CALL GRSVPT(  0.1,  0.9,  0.3,  0.5 )
      CALL GRSTRN( 1 )
      CALL GRSTRF

      CALL SGPLU( NMAX+1, X, Y )

      CALL GRSWND( XMIN, XMAX, YMIN, YMAX )
      CALL GRSVPT(  0.1,  0.9,  0.1,  0.3 )
      CALL GRSTRN( 1 )
      CALL GRSTRF

      CALL SGPMU( NMAX+1, X, Y )

      CALL GRCLS

      END
