      PROGRAM U2D5

      PARAMETER( NX=37, NY=37 )
      PARAMETER( XMIN=0, XMAX=360, YMIN=-90, YMAX=90 )
      PARAMETER( PI=3.14159, DRAD=PI/180 )
      REAL P(NX,NY)

      DO 10 J=1,NY
      DO 10 I=1,NX
        ALON = ( XMIN + (XMAX-XMIN)*(I-1)/(NX-1) ) * DRAD
        ALAT = ( YMIN + (YMAX-YMIN)*(J-1)/(NY-1) ) * DRAD
        SLAT = SIN(ALAT)
        P(I,J) = 3*SQRT(1-SLAT**2)*SLAT*COS(ALON) - 0.5*(3*SLAT**2-1)
   10 CONTINUE

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN( IWS )
      CALL SGLSET( 'LSOFTF', .TRUE. )
      CALL GRFRM

      CALL GRSWND( XMIN, XMAX, YMIN, YMAX )
      CALL GRSVPT(  0.2,  0.8,  0.2,  0.8 )
      CALL GRSTRN( 1 )
      CALL GRSTRF

      DO 20 K=-5,3
        TLEV1 = 0.4*K
        TLEV2 = TLEV1 + 0.4
        IF(K.LE.-1) THEN
          IPAT = 600 + ABS(K+1)
        ELSE
          IPAT =  30 + K
        END IF
        CALL UESTLV( TLEV1, TLEV2, IPAT )
   20 CONTINUE

      CALL USDAXS
      CALL UDGCLB( P, NX, NX, NY, 0.4 )
      CALL UDCNTR( P, NX, NX, NY )
      CALL UETONE( P, NX, NX, NY )

      CALL GRCLS

      END
