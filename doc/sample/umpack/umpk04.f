*-----------------------------------------------------------------------
      PROGRAM UMPK04

      PARAMETER ( NX=19, NY=19 )
      PARAMETER ( XMIN=  0, XMAX=360, YMIN=-90, YMAX=+90 )
      PARAMETER ( PI=3.141592, DRAD=PI/180, DZ=0.05, DP=0.2 )

      REAL      P(NX,NY)


      DO 20 J = 1, NY
        DO 10 I = 1, NX
          ALON = ( XMIN + (XMAX-XMIN) * (I-1) / (NX-1) ) * DRAD
          ALAT = ( YMIN + (YMAX-YMIN) * (J-1) / (NY-1) ) * DRAD
          SLAT = SIN(ALAT)
          P(I,J) = COS(ALON) * (1-SLAT**2) * SIN(2*PI*SLAT) + DZ
   10   CONTINUE
   20 CONTINUE

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN( IWS )

      CALL GLRGET( 'RMISS', RMISS )
      CALL SGLSET( 'LSOFTF', .FALSE. )

      CALL GRFRM

      CALL GRSWND( XMIN, XMAX, YMIN, YMAX )
      CALL GRSVPT( 0.1, 0.9, 0.1, 0.9 )
      CALL GRSSIM( 0.4, 0.0, 0.0 )
      CALL GRSMPL( 165.0, 60.0, 0.0 )
      CALL GRSTXY( -180.0, 180.0, 0.0, 90.0 )
      CALL GRSTRN( 30 )
      CALL GRSTRF
      CALL SGLSET( 'LCLIP', .TRUE. )

      CALL UESTLV( RMISS,  -DP, 201 )
      CALL UESTLV(    DP, DP*2, 401 )
      CALL UESTLV( DP*2, RMISS, 402 )
      CALL UETONE( P, NX, NX, NY )

      CALL UDGCLB( P, NX, NX, NY, DP )
      CALL UDCNTR( P, NX, NX, NY )

      CALL UMPMAP( 'coast_world' )
      CALL UMPGLB

      CALL GRCLS

      END
