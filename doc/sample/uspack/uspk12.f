*-----------------------------------------------------------------------
      PROGRAM USPK12

      PARAMETER(NT=51, NZ=21, ZMIN=20., ZMAX=50., TMAX=5.)
      PARAMETER(DZ=(ZMAX-ZMIN)/(NZ-1), DT=TMAX/(NT-1))
      REAL U(NT, NZ)

      DO 20 J=1, NZ
        Z  = DZ*(J-1)
        UZ = EXP(-0.2*Z)*(Z**0.5)
        DO 10 I=1, NT
          T  = DT*(I-1) - 2*EXP(-0.1*Z)
          U(I,J) = UZ*SIN(3.*T)
   10   CONTINUE
   20 CONTINUE

*-----------------------------------------------------------------------

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN(IWS)
      CALL GRFRM

      CALL GRSWND(0., TMAX, ZMIN, ZMAX)
      CALL GRSVPT(0.2, 0.8, 0.2, 0.8)
      CALL GRSTRN(1)
      CALL GRSTRF

      CALL USSTTL('TIME', 'YEAR', 'HEIGHT', 'km')

      CALL USDAXS

      CALL UDCNTR(U, NT, NT, NZ)

      CALL GRCLS

      END
