*-----------------------------------------------------------------------
      PROGRAM USPK08

      PARAMETER(N=200)
      CHARACTER USGI*3
      REAL T(N), Z(N)
      DOUBLE PRECISION R, A

      R = 0.2D0
      A = 4.0D0
      DO 10 I=1, N
        R = A*R*(1.D0-R)
        Z2  = (FLOAT(I-5)/40.)**2
        T(I) = 20.*EXP(-Z2) + R*2.
        Z(I) = I*2
   10 CONTINUE

*-----------------------------------------------------------------------

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN(IWS)
      CALL GRFRM

*          --- OMAJINAI ---
      CALL USLSET('LYINV'   , .TRUE.)
      CALL UZLSET('LABELXT' , .TRUE.)
      CALL USCSET('CYSPOS'  , 'B')

      CALL USCSET('CXSIDE'  , 'T')
      CALL USCSET('CYSIDE'  , 'L')

      CALL USSTTL('TEMPERATURE', 'C|'//USGI(4)//'"', 'DEPTH', 'm')
      CALL USGRPH(N, T, Z)

      CALL GRCLS

      END
