*-----------------------------------------------------------------------
      PROGRAM U2DF02

      PARAMETER ( NX=19, NY=19 )
      PARAMETER ( XMIN=0, XMAX=360, DX1=10, DX2=60 )
      PARAMETER ( YMIN=-90, YMAX=90, MY=7, NC=3 )
      PARAMETER ( PI=3.141592, DRAD=PI/180, DZ=0.05 )

      REAL      P(NX, NY), UY1(NY), UY2(MY)
      CHARACTER CH(MY)*(NC)

      DATA      CH/ 'SP ', '60S', '30S', 'EQ ', '30N', '60N', 'NP ' /


      DO 20 J = 1, NY
        DO 10 I = 1, NX
          ALON = ( XMIN + (XMAX-XMIN) * (I-1) / (NX-1) ) * DRAD
          ALAT = ( YMIN + (YMAX-YMIN) * (J-1) / (NY-1) ) * DRAD
          SLAT = SIN(ALAT)
          P(I,J) = COS(ALON) * (1-SLAT**2) * SIN(2*PI*SLAT) + DZ
   10   CONTINUE
   20 CONTINUE

      DO 30 J = 1, NY
        UY1(J) = SIN( ( YMIN + (YMAX-YMIN) * (J-1) / (NY-1) ) * DRAD )
   30 CONTINUE
      DO 40 J = 1, MY
        UY2(J) = SIN( ( YMIN + (YMAX-YMIN) * (J-1) / (MY-1) ) * DRAD )
   40 CONTINUE

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN( IWS )
      CALL GRFRM

      CALL GRSWND( XMIN, XMAX, UY1(1), UY1(NY) )
      CALL GRSVPT( 0.2, 0.8, 0.2, 0.8 )
      CALL GRSTRN( 1 )
      CALL GRSTRF

      CALL UXAXDV( 'B', DX1, DX2 )
      CALL UXAXDV( 'T', DX1, DX2 )
      CALL UXSTTL( 'B', 'LONGITUDE', 0.0 )

      CALL UYAXLB( 'L', UY1, NY, UY2, CH, NC, MY )
      CALL UYAXLB( 'R', UY1, NY, UY2, CH, NC, MY )
      CALL UYSTTL( 'L', 'LATITUDE', 0.0 )

      CALL UWSGXB( XMIN, XMAX, NX )
      CALL UWSGYA( UY1, NY )

      CALL UDSFMT( '(F6.1)' )
      CALL UDGCLB( P, NX, NX, NY , 0.2 )
      CALL UDSCLV( 0.1, 1, 4, ' ', 0.01 )

      CALL UDCNTR( P, NX, NX, NY )

      CALL GRCLS

      END
