*-----------------------------------------------------------------------
      PROGRAM SGPK03

      PARAMETER(N=41)
      DIMENSION X(N), Y(N)


      DT = 4.* 3.14159 / (N-1)
      DO 10 I=1,N
        Y(I) = SIN(DT*(I-1))*0.15
        X(I) = REAL(I-1)/REAL(N-1)
   10 CONTINUE

      CALL SWCSTX('FNAME','SGPK03')
      CALL SWLSTX('LSEP',.TRUE.)

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ(*,*) IWS

      CALL SGOPN(IWS)

*----------------------------- page 1 ---------------------------------
      CALL SGFRM

      CALL SGSWND( 0.0,  1.0, -0.8, 0.2)
      CALL SGSVPT( 0.15, 0.85, 0.1, 0.9)
      CALL SGSTRN(1)
      CALL SGSTRF
      CALL SLPVPR(1)
      CALL SGPLU(N, X, Y)                   ! <-- 1本目

      CALL SGSWND( 0.0,  1.0, -0.7, 0.3)
      CALL SGSTRF
      CALL SGSPLI(2)                        ! <-- Line INDEX 設定
      CALL SGPLU(N, X, Y)                   ! <-- 2本目

      CALL SGSWND( 0.0,  1.0, -0.6, 0.4)
      CALL SGSTRF
      CALL SGSPLI(3)                        ! <-- Line INDEX 設定
      CALL SGPLU(N, X, Y)                   ! <-- 3本目

      CALL SGSWND( 0.0,  1.0, -0.4, 0.6)
      CALL SGSTRF
      CALL SGSPLT(2)                        ! <-- Line TYPE 設定 (破線)
      CALL SGPLU(N, X, Y)                   ! <-- 4本目

      CALL SGSWND( 0.0,  1.0, -0.3, 0.7)
      CALL SGSTRF
      CALL SGSPLT(3)                        ! <-- Line TYPE 設定 (点線)
      CALL SGPLU(N, X, Y)                   ! <-- 5本目

      CALL SGSWND( 0.0,  1.0, -0.2, 0.8)
      CALL SGSTRF
      CALL SGSPLT(4)                        ! <-- Line TYPE 設定 (一点鎖線)
      CALL SGPLU(N, X, Y)                   ! <-- 6本目

*----------------------------- page 2 ---------------------------------

      CALL SGFRM

      CALL SGSWND( 0.0,  1.0, -0.8, 0.2)
      CALL SGSVPT( 0.15, 0.85, 0.1, 0.9)
      CALL SGSTRN(1)
      CALL SGSTRF
      CALL SLPVPR(1)

      CALL SGRSET('BITLEN', 0.006)          ! <-- サイクル長の変更
      CALL SGSPLT(4)
      CALL SGPLU(N, X, Y)                   ! <-- 1本目

      CALL SGSWND( 0.0,  1.0, -0.6, 0.4)
      CALL SGSTRF
      CALL SGSPLI(2)

      CALL BITPCI('1111111100100100', ITYPE)! <-- パターン生成
      CALL SGSPLT(ITYPE)                    ! <-- パターン設定
      CALL SGPLU(N, X, Y)                   ! <-- 2本目

      CALL SGSWND( 0.0,  1.0, -0.4, 0.6)
      CALL SGSTRF
      CALL SGISET('NBITS', 32)
      CALL BITPCI('10010010011111000111110001111100', ITYPE)
      CALL SGSPLT(ITYPE)
      CALL SGPLU(N, X, Y)

      CALL SGSWND( 0.0,  1.0, -0.2, 0.8)
      CALL SGSTRF
      CALL SGSPLI(1)
      CALL SGSPLT(1)

      N1=N/4
      Y(N1-1) = 999.                        ! <-- 欠損値
      Y(N1  ) = 999.
      Y(N1+1) = 999.

      N2=N1*3
      Y(N2-1) = 999.
      Y(N2+1) = 999.

      CALL GLLSET('LMISS',.TRUE.)
      CALL SGPLU(N, X, Y)                   ! <-- 4本目

      CALL SGCLS

      END
