*-----------------------------------------------------------------------
      PROGRAM UXYZ06

      PARAMETER ( ID0=19811201, ND=720 )


      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ(*,*) IWS

      CALL GROPN( IWS )

      CALL UZFACT( 0.8 )

      CALL GRFRM

      RND=ND
      CALL SGSWND( -180.0, 180.0, RND, 0.0 )
      CALL SGSVPT( 0.2, 0.8, 0.2, 0.8 )
      CALL SGSTRN( 1 )
      CALL SGSTRF

      CALL UXAXDV( 'B', 10.0, 60.0 )
      CALL UXAXDV( 'T', 10.0, 60.0 )
      CALL UXSTTL( 'B', 'LONGITUDE', 0.0 )

      CALL UCYACL( 'L', ID0, ND )
      CALL UZLSET( 'LABELYR', .TRUE. )
      CALL UYAXDV( 'R', 20.0, 100.0 )
      CALL UYSTTL( 'R', 'DAY NUMBER', 0.0 )

      CALL UXMTTL( 'T', 'UXAXDV/UCYACL', 0.0 )

      CALL GRCLS

      END
