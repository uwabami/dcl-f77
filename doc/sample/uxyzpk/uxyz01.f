*-----------------------------------------------------------------------
      PROGRAM UXYZ01


      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ(*,*) IWS

      CALL GROPN( IWS )

      CALL GRFRM

      CALL SGSWND( -180.0, +180.0, -90.0, +90.0 )
      CALL SGSVPT( 0.2, 0.8, 0.3, 0.7 )
      CALL SGSTRN( 1 )
      CALL SGSTRF

      CALL UXAXDV( 'B', 10.0, 60.0 )
      CALL UXAXDV( 'T', 10.0, 60.0 )
      CALL UXSTTL( 'B', 'LONGITUDE', 0.0 )
      CALL UXSTTL( 'B', '<- WEST      EAST ->', 0.0 )

      CALL UYAXDV( 'L', 10.0, 30.0 )
      CALL UYAXDV( 'R', 10.0, 30.0 )
      CALL UYSTTL( 'L', 'LATITUDE', 0.0 )
      CALL UYSTTL( 'L', '<- SH    NH ->', 0.0 )

      CALL UXMTTL( 'T', 'UXAXDV/UYAXDV', 0.0 )

      CALL GRCLS

      END
