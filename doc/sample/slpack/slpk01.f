*-----------------------------------------------------------------------
      PROGRAM SLPK01

      CHARACTER*7  CTXT

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ(*,*) IWS

      CALL SGOPN(-ABS(IWS))
      CALL SLMGN(0.1, 0.1, 0.05, 0.05)    ! <-- 第1レベルのマージン

      CALL SLDIV('S', 3, 5)               ! <-- フレーム分割 (第2レベル)
      CALL SLMGN(0.05, 0.05, 0.05, 0.05)  ! <-- 第2レベルのマージン
      CALL SLRAT(1., 1.)                  ! <-- 第2レベルの縦横比を指定
      CALL SGSTXI(2)
      CALL SGSTXS(0.1)

      DO 100 I=1, 15
        CALL SGFRM                        ! <--
        CALL SLPWWR(1)                    ! <    この中は普通に1ページ
        WRITE(CTXT, '(''FRAME'',I2.2)') I ! <    描画するのと同じ
        CALL SGTXV(0.5, 0.5, CTXT)        ! <--
  100 CONTINUE

      CALL SGCLS

      END
