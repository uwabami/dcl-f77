*-----------------------------------------------------------------------
      PROGRAM SLPK02

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ(*,*) IWS

      CALL SGOPN(IWS)

      CALL SLMGN(0., 0., 0.05, 0.05)                       ! <-- マージン設定
      CALL SLRAT(1., 1.)                                   ! <-- 縦横比設定
      CALL SLSTTL('FIGURE TITLE',  'T',  0., -1., 0.02, 1) ! <-+
      CALL SLSTTL('page:#PAGE',    'B',  1.,  1., 0.02, 2) ! <-+ タイトル設定
      CALL SLSTTL('PROG.NAME',     'B', -1.,  1., 0.02, 3) ! <-+

      CALL SGFRM                         ! <-- ここで実際にタイトルが書かれる
      CALL SLPWWR(1)
      CALL SGTXZV(0.5, 0.5, 'FIGURE', 0.05, 0, 0, 1)

      CALL SGCLS

      END
