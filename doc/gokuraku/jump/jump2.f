      PROGRAM JUMP2

      PARAMETER ( NMAX=100 )
      REAL X(NMAX), Y(NMAX)

*-- データ ----
      R  = 0.2
      R0 = 0.0
      DO 10 I=1,NMAX
        R  = 3.6*R*(1.-R)
        R0 = R0 + R*4 - 2.58
        X2 = (I-50)**2
        REXP = 4.*I/NMAX
        X(I) = 10**REXP
        Y(I) = 1.E5*EXP(-X2) + 10.**R0
   10 CONTINUE
      Y(20) = 1.E4
      Y(40) = 2.E3
      Y(65) = 3.E4
      Y(70) = 5.E2

*-- グラフ ----
      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN( IWS )
      CALL GRFRM

      CALL GRSWND( 1.E0, 1.E4, 1.E-1, 1.E6 )
      CALL GRSVPT(  0.2,  0.8,   0.2,  0.8 )
      CALL GRSTRN( 4 )
      CALL GRSTRF

      CALL USSTTL( 'FREQUENCY', '/s', 'POWER', 'm|2"/s|2"' )
      CALL USDAXS

      CALL UULIN( NMAX, X, Y )

      CALL GRCLS

      END
