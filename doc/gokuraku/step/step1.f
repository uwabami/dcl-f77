      PROGRAM STEP1

      PARAMETER( NMAX=201, IMAX=5 )
      REAL X(NMAX), Y0(NMAX), Y1(NMAX), Y2(NMAX), Y3(NMAX), A(IMAX)

*-- データ ----
      PI = 3.14159
      DO 10 I=1,IMAX
        II = 2*I - 1
        A(I) = (-1)**I *2./(II*PI)
   10 CONTINUE

      DO 20 N=1,NMAX
        X(N)  = 1.*(N-1)/(NMAX-1)
        T     = 2.*PI*X(N)
        IF(T.LT.PI/2. .OR. T.GE.PI*3./2.) THEN
          Y0(N) = 0.
        ELSE
          Y0(N) = 1.
        END IF
        Y1(N) = 0.5 + A(1)*COS(T) 
        Y2(N) = 0.5
        Y3(N) = 0.5
        DO 30 I=1,IMAX
          II = 2*I - 1
          IF(I .LE. 3) Y2(N) = Y2(N) + A(I)*COS(II*T)
          Y3(N) = Y3(N) + A(I)*COS(II*T)
   30   CONTINUE
   20 CONTINUE

*-- グラフ ----
      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN( IWS )
      CALL GRFRM

      CALL USSPNT( NMAX, X, Y0 )
      CALL USSPNT( NMAX, X, Y1 )
      CALL USSPNT( NMAX, X, Y2 )
      CALL USSPNT( NMAX, X, Y3 )
      CALL USPFIT
      CALL GRSTRF

      CALL USSTTL( 'FREQUENCY', '/DAY', 'RESPONSE', '' )
      CALL USDAXS

      CALL UULIN( NMAX, X, Y0 )

      CALL UUSLNT( 2 )
      CALL UUSLNI( 3 )
      CALL UULIN( NMAX, X, Y1 )

      CALL UUSLNT( 3 )
      CALL UULIN( NMAX, X, Y2 )

      CALL UUSLNT( 4 )
      CALL UULIN( NMAX, X, Y3 )

      CALL GRCLS

      END
