      PROGRAM U1D1

      PARAMETER( NMAX=51, XMIN=1945, XMAX=1995 )
      REAL Y(NMAX)

*-- データ ----
      Y0 = 0.5
      DO 10 N=1,NMAX
        Y(N) = 5.*Y0 + 10.
        Y0   = 3.7*Y0*(1.-Y0)
   10 CONTINUE

      CALL GLRGET( 'RUNDEF', RUNDEF )

*-- グラフ ----
      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN( IWS )
      CALL GRFRM

      CALL GRSWND( XMIN, XMAX, RUNDEF, RUNDEF )
      CALL USSTTL( 'TIME', 'YEAR', 'TEMPERATURE', 'DEG' )
      CALL USGRPH( NMAX, RUNDEF, Y )

      CALL GRCLS

      END
