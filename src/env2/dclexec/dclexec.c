/*
 *      Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main (int argc, char *argv[])
{
    int i, np;
    char carg[40], cmd[200], *cpar, *ceq;
    int toupper();
    int retv;

    if (argc < 2) {
	printf ("Usage: dclexec command options...\n");
    }
    else {
	np = 0;
	strcpy (cmd, "env");
	for (i = 2; i < argc; i++) {
	    strcpy (carg, argv[i]);
	    ceq = strchr (carg, '=');
	    if (carg[0] == '-' && carg[3] == ':' && ceq != NULL) {
		carg[0] = ' ';
		cpar = &carg[0];
		while (--ceq >= cpar) {
		    *ceq = toupper (*ceq);
		}
		strcat (cmd, cpar);
		np++;
	    }
	}
	strcat (cmd, " ");
	strcat (cmd, argv[1]);
	retv=system (cmd);
    }
}
