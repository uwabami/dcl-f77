#!/bin/bash
#
port=57010

usage_exit() {
        echo "Usage: $0 remotehost" 1>&2
        exit 1
}

while getopts h OPT
do
    case $OPT in
        h)  usage_exit
            ;;
    esac
done

shift $((OPTIND - 1))

if [ $# = 0 ]; then
    usage_exit
fi

echo ssh -A -t $1 -R $port:localhost:$port -N
echo dclsrv

ssh -A -t $1 -R $port:localhost:$port -N &
sshpid=$!

trap "kill $sshpid; exit" 2

while true
do
    dclsrv
done

