*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM DCLCLR

      PARAMETER (NP=5)
      PARAMETER (XMIN=0.07,XMAX=0.97,YMIN=0.03,YMAX=0.93)
      PARAMETER (DX=(XMAX-XMIN)/10,DY=(YMAX-YMIN)/10)

      REAL      XBOX(NP),YBOX(NP)
      CHARACTER CHR*2,CTTL*8

      DATA      XBOX/ 0.0, 1.0, 1.0, 0.0, 0.0/
      DATA      YBOX/ 0.0, 0.0, 1.0, 1.0, 0.0/


      CALL SWISTX('IWIDTH ',600)
      CALL SWISTX('IHEIGHT',600)
      CTTL='DCLCLR'
      CALL CLOWER(CTTL)
      CALL SWCSTX('TITLE',CTTL)

      CALL GLISTX('MAXMSG',0)
      CALL SGISTX('IFONT',2)
      CALL SGLSTX('LCORNER',.FALSE.)

      CALL GROPN(1)
      CALL SGFRM
      CALL SGSWND(0.0,1.0,0.0,1.0)
      CALL SGSTRN(1)

      DO 20 J=0,9
        DO 10 I=0,9
          X1=XMIN+DX*I
          X2=X1+DX
          Y1=YMIN+DY*(9-J)
          Y2=Y1+DY
          LEVEL=(I+J*10)*1000+999
          CALL SGSVPT(X1,X2,Y1,Y2)
          CALL SGSTRF
          CALL SGTNZU(NP,XBOX,YBOX,LEVEL)
          CALL SGPLZU(NP,XBOX,YBOX,1,1)
   10   CONTINUE
   20 CONTINUE

      CALL SGSVPT(0.0,1.0,0.0,1.0)
      CALL SGSTRF

      DO 30 I=0,9
        XX=XMIN+DX*(I+0.5)
        YY=(1+YMAX)/2
        WRITE(CHR,'(I1)') I
        CALL SGTXZV(XX,YY,CHR,0.025,0,0,1)
   30 CONTINUE

      DO 40 J=0,9
        XX=XMIN/2
        YY=YMIN+DY*(9-J+0.5)
        WRITE(CHR,'(I2)') J*10
        CALL SGTXZV(XX,YY,CHR,0.025,0,0,1)
   40 CONTINUE

      CALL SGCLS

      END
