*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM DCLTRF

      PARAMETER (MAXNTR=99)

      LOGICAL   LTR
      CHARACTER CTR1*3,CTR2*20,CPARA*4,CFMT*4,CMSG*80

      EXTERNAL  LENC


      CALL OSQARN(NP)
      IF (NP.EQ.0) THEN
        N1=1
        N2=MAXNTR
      ELSE
        CALL OSGARG(1,CPARA)
        CFMT='(I#)'
        CALL CHNGI(CFMT,'#',LENC(CPARA),'(I1)')
        READ(CPARA,CFMT) N1
        N2=N1
      END IF

      CALL GLIGET('IOUNIT',IU)
      DO 10 N=N1,N2
        CALL SGTRQF(N,LTR)
        IF (LTR) THEN
          CALL SGTRNS(N,CTR1)
          CALL SGTRNL(N,CTR2)
          WRITE(IU,'(A,I2,5A)') ' NTR = ',N,' ; ',CTR1,' ; ',CTR2,' ;'
        ELSE
          IF (N1.EQ.N2) THEN
            CMSG='TRANSFORMATION NUMBER ## IS NOT DEFINED.'
            CALL CHNGI(CMSG,'##',N,'(I2)')
            CALL MSGDMP('M','DCLTRF',CMSG)
          END IF
        END IF
   10 CONTINUE

      END
