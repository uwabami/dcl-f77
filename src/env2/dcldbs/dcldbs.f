*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM DCLDBS

      PARAMETER (NDX=2,NGL=1,NSW=6)

      CHARACTER CDXLST(NDX)*8,CGLLST(NGL)*8,CSWLST(NSW)*8,CFNAME*1024

      DATA      CDXLST/'DSPATH','DUPATH'/
      DATA      CGLLST/'DCLRC'/
      DATA      CSWLST/'BITMAP','CL2TN','CMAPLIST','CLRMAP',
     +                 'FONT1','FONT2' /


      CALL GLIGET('IOUNIT',IU)
      WRITE(IU,'(A)') '*** PATH NAME FOR DATABASE'
      DO 10 N=1,NDX
        CALL GLCGET(CDXLST(N),CFNAME)
        WRITE(IU,'(A)') CDXLST(N)//' : '//CFNAME(1:LENC(CFNAME))
   10 CONTINUE

      WRITE(IU,'(A)') '*** FILE NAME FOR SYSLIB'
      DO 20 N=1,NGL
        CALL GLQFNM(CGLLST(N),CFNAME)
        WRITE(IU,'(A)') CGLLST(N)//' : '//CFNAME(1:LENC(CFNAME))
   20 CONTINUE

      WRITE(IU,'(A)') '*** FILE NAME FOR SWPACK/SZPACK'
      DO 30 N=1,NSW
        CALL SWQFNM(CSWLST(N),CFNAME)
        WRITE(IU,'(A)') CSWLST(N)//' : '//CFNAME(1:LENC(CFNAME))
   30 CONTINUE

      END
