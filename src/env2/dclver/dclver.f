*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM DCLVER

      CHARACTER CVNAME*20


      CALL GLIGET('IOUNIT',IU)
      CALL DCLVNM(CVNAME)
      WRITE(IU,'(A)') CVNAME(1:LENC(CVNAME))

      END
