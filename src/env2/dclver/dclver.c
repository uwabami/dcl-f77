/*
 *      Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
 */
#include <string.h>

#define LENVN 20

main()
{
    char vname[LENVN];

    void dclvnm_(), cfchr();

    dclvnm_(vname, LENVN);
    cfchr (vname, LENVN);
    printf("%s\n", vname);
}

void cfchr (ch1, lch)
    char *ch1;
    int  lch;
{
    int nc;
    char *nl;

    for (nc = lch; nc > 1; --nc) {
	nl = ch1 + nc - 1;
	if (*nl != '\0' && *nl != ' ')
	    break;
    }
    *(nl + 1) = '\0';
}
