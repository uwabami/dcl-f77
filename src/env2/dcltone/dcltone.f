*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM DCLTON

      PARAMETER (NP=5)
      PARAMETER (XMIN=0.08,XMAX=0.98,YMIN=0.04,YMAX=0.94)
      PARAMETER (DX=(XMAX-XMIN)/6,DY=(YMAX-YMIN)/6)

      REAL      XBOX(NP),YBOX(NP)
      CHARACTER CHR*2,CTTL*16,CPARA*4,CFMT*4

      DATA      XBOX/ 0.0, 1.0, 1.0, 0.0, 0.0/
      DATA      YBOX/ 0.0, 0.0, 1.0, 1.0, 0.0/


      CALL OSQARN(NPR)
      IF (NPR.EQ.0) THEN
        CALL GLIGET('IIUNIT',II)
        CALL GLIGET('IOUNIT',IO)
        WRITE(IO,*) ' TONE PATTERN (0-6) (I) ? ;'
        READ(II,*) N
      ELSE
        CALL OSGARG(1,CPARA)
        CFMT='(I#)'
        CALL CHNGI(CFMT,'#',LENC(CPARA),'(I1)')
        READ(CPARA,CFMT) N
      END IF

      CALL SWISTX('IWIDTH ',600)
      CALL SWISTX('IHEIGHT',600)

      CTTL='DCLTONE (#)'
      CALL CHNGI(CTTL,'#',N,'(I1)')
      CALL CLOWER(CTTL)
      CALL SWCSTX('TITLE',CTTL)

      CALL GLISTX('MAXMSG',0)
      CALL SGISTX('IFONT',2)
      CALL SGLSTX('LCORNER',.FALSE.)

      CALL GROPN(1)
      CALL SGFRM
      CALL SGSWND(0.0,1.0,0.0,1.0)
      CALL SGSTRN(1)

      DO 20 J=0,5
        DO 10 I=0,5
          X1=XMIN+DX*I
          X2=X1+DX*0.9
          Y1=YMIN+DY*(5-J)
          Y2=Y1+DY*0.9
          LEVEL=I+J*10+N*100
          CALL SGSVPT(X1,X2,Y1,Y2)
          CALL SGSTRF
          CALL SGTNZU(NP,XBOX,YBOX,LEVEL)
          CALL SGPLZU(NP,XBOX,YBOX,1,1)
   10   CONTINUE
   20 CONTINUE

      CALL SGSVPT(0.0,1.0,0.0,1.0)
      CALL SGSTRF

      DO 30 I=0,5
        XX=XMIN+DX*(I+0.45)
        YY=(1+YMAX)/2
        WRITE(CHR,'(I1)') I
        CALL SGTXZV(XX,YY,CHR,0.025,0,0,1)
   30 CONTINUE

      DO 40 J=0,5
        XX=XMIN/2
        YY=YMIN+DY*(5-J+0.45)
        WRITE(CHR,'(I2)') J*10
        CALL SGTXZV(XX,YY,CHR,0.025,0,0,1)
   40 CONTINUE

      CALL SGCLS

      END
