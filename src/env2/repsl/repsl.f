*-----------------------------------------------------------------------
*     PRESISION OF REAL VARIABLE                             (94/09/17)
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM REPSL

      PARAMETER(LBIT=32)

      PI = ATAN(1.)*4

      EMIN = 1.
      EMAX = 0.
      DO 100 I=1,179
        TH = PI/180*I
        S0 = SIN(TH)
        S1 = S0
        I0 = 0
        I1 = 1

        CALL SBYTE(S0,I0,LBIT-1,1)
        CALL SBYTE(S1,I1,LBIT-1,1)

        DELTA = S1-S0
        EPS = ABS (DELTA / S1)

        EMAX = MAX(EPS,EMAX)
        EMIN = MIN(EPS,EMIN)

  100 CONTINUE

      CALL GLIGET('IOUNIT',IOU)

*     WRITE(IOU,*) ' MAX ERROR = ',EMAX
*     WRITE(IOU,*) ' MIN ERROR = ',EMIN
      WRITE(IOU,'(A,1P,E10.3)')
     +     '< REPSL > IN GLPGET/GLPSET SHOULD BE ',EMAX*10

      STOP
      END
