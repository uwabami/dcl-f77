*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM DCLFNT

      CHARACTER CH*3,USGI*3,CTTL*16


      CALL SWISTX('IWIDTH ',600)
      CALL SWISTX('IHEIGHT',600)

      CTTL='DCLFONT (#)'
      CALL SGIGET('IFONT',IFONT)
      CALL CHNGI(CTTL,'#',IFONT,'(I1)')
      CALL CLOWER(CTTL)
      CALL SWCSTX('TITLE',CTTL)

      CALL GLISTX('MAXMSG',0)
      CALL SGLSTX('LCORNER',.FALSE.)

      CALL GROPN(1)

      CALL SGLSET('LCNTL',.FALSE.)

      CALL SGFRM

      CALL SGSVPT(0.05,0.95,0.05,0.95)
      CALL SGSWND(0.0,16.0,0.0,16.0)
      CALL SGSTRN(1)
      CALL SGSTRF

      DO 10 I=0,16
        CALL SGLNZU(REAL(I),0.0,REAL(I),16.0,2)
        CALL SGLNZU(0.0,REAL(I),16.0,REAL(I),2)
   10 CONTINUE

      DO 15 I=0,255
        UX=I/16+0.5
        UY=16-MOD(I,16)-0.5
        CALL SGTXZU(UX,UY,USGI(I),0.035,0,0,3)
        UX=UX+0.48
        UY=UY+0.38
        WRITE(CH,'(I3)') I
        CALL SGTXZU(UX,UY,CH,0.01,0,+1,1)
   15 CONTINUE

      CALL SGCLS

      END
