*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM DCLCMP

      CHARACTER CMD*1024

      EXTERNAL  LENC


      CALL GLIGET('MSGUNIT',IU)

      CALL SWCMLL
      CALL SWQCMN(MAXCMD)
      DO 10 I=1,MAXCMD
        CALL SWQCMD(I,CMD)
        NC=LENC(CMD)
        WRITE(IU,'(TR1,I2.2,A2,A)') I,': ',CMD(1:NC)
   10 CONTINUE

      END
