*-----------------------------------------------------------------------
*     NORMALIZATION TRANSFORMATION (INCLUDING MAP PROJECTION)
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE STFTRF(UX, UY, VX, VY)

      LOGICAL   LMAPA

      LOGICAL   LMISS, LMAP

      SAVE


      IF (LMISS .AND. (UX.EQ.RMISS .OR. UY.EQ.RMISS)) THEN
        VX = RMISS
        VY = RMISS
        RETURN
      END IF

      CALL STFRAD(UX, UY, UX1, UY1)

      IF (LMAP) THEN
        CALL STFROT(UX1, UY1, TX, TY)
        CALL STFTRN(TX,  TY,  VX, VY)
      ELSE
        CALL STFTRN(UX1, UY1, VX, VY)
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY STITRF(VX, VY, UX, UY)

      IF (LMAP) THEN
        CALL STITRN(VX, VY, TX, TY)
        IF (TX.EQ.RNA .OR. TY.EQ.RNA) THEN
          UX = RNA
          UY = RNA
          RETURN
        END IF
        CALL STIROT(TX, TY, UX1, UY1)
      ELSE
        CALL STITRN(VX, VY, UX1, UY1)
      END IF

      CALL STIRAD(UX1, UY1, UX, UY)

      RETURN
*-----------------------------------------------------------------------
      ENTRY STQTRF(LMAPA)

      LMAPA = LMAP

      RETURN
*-----------------------------------------------------------------------
      ENTRY STSTRF(LMAPA)

      LMAP = LMAPA
      CALL GLLGET('LMISS',LMISS)
      CALL GLRGET('RMISS',RMISS)
      CALL GLRGET('RUNDEF',RNA)

      END
