*-----------------------------------------------------------------------
*     UNIT CONVERSION (DEG<->RAD)
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE STFRAD(X, Y, RX, RY)

      LOGICAL   LXDEG, LYDEG

      LOGICAL   LXD, LYD

      EXTERNAL  RFPI

      SAVE


      IF (LXD) THEN
        RX = X*CP
      ELSE
        RX = X
      END IF

      IF (LYD) THEN
        RY = Y*CP
      ELSE
        RY = Y
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY STIRAD(RX, RY, X, Y)

      IF (LXD) THEN
        X = RX/CP
      ELSE
        X = RX
      END IF

      IF (LYD) THEN
        Y = RY/CP
      ELSE
        Y = RY
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY STSRAD(LXDEG, LYDEG)

      LXD = LXDEG
      LYD = LYDEG

      CP = RFPI()/180

      RETURN
      END
