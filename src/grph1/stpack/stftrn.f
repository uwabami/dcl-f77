*-----------------------------------------------------------------------
*     NORMALIZATION TRANSFORMATION (INCLUDING MAP PROJECTION)
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE STFTRN(UX, UY, VX, VY)

      SAVE

      IF (ITRZ.EQ.1) THEN
        XX = UX
        YY = UY
      ELSE IF (ITRZ.EQ.2) THEN
        CALL SGQWND(UXMIN,UXMAX,UYMIN,UYMAX)
        XX = UX
        UYS = SIGN(1.0,UYMIN)*UY
        IF (UYS.LE.0.0) CALL MSGDMP('E','STFTRN',
     +            'UY HAS INVALID SIGN FOR LOG TRANSFORMATION OR IS 0')
        YY = LOG10(UYS)
      ELSE IF (ITRZ.EQ.3) THEN
        CALL SGQWND(UXMIN,UXMAX,UYMIN,UYMAX)
        UXS = SIGN(1.0,UXMIN)*UX
        IF (UXS.LE.0.0) CALL MSGDMP('E','STFTRN',
     +            'UX HAS INVALID SIGN FOR LOG TRANSFORMATION OR IS 0')
        XX = LOG10(UXS)
        YY = UY
      ELSE IF (ITRZ.EQ.4) THEN
        CALL SGQWND(UXMIN,UXMAX,UYMIN,UYMAX)
        UXS = SIGN(1.0,UXMIN)*UX
        UYS = SIGN(1.0,UYMIN)*UY
        IF (UXS.LE.0.0) CALL MSGDMP('E','STFTRN',
     +            'UX HAS INVALID SIGN FOR LOG TRANSFORMATION OR IS 0')
        IF (UYS.LE.0.0) CALL MSGDMP('E','STFTRN',
     +            'UY HAS INVALID SIGN FOR LOG TRANSFORMATION OR IS 0')
        XX = LOG10(UXS)
        YY = LOG10(UYS)
      ELSE IF (ITRZ.EQ.5) THEN
        CALL CT2PC(UX, UY, XX, YY)
      ELSE IF (ITRZ.EQ.6) THEN
        CALL CT2BC(UX, UY, XX, YY)
      ELSE IF (ITRZ.EQ.10) THEN
        CALL MPFCYL(UX, UY, XX, YY)
      ELSE IF (ITRZ.EQ.11) THEN
        CALL MPFMER(UX, UY, XX, YY)
      ELSE IF (ITRZ.EQ.12) THEN
        CALL MPFMWD(UX, UY, XX, YY)
      ELSE IF (ITRZ.EQ.13) THEN
        CALL MPFHMR(UX, UY, XX, YY)
      ELSE IF (ITRZ.EQ.14) THEN
        CALL MPFEK6(UX, UY, XX, YY)
      ELSE IF (ITRZ.EQ.15) THEN
        CALL MPFKTD(UX, UY, XX, YY)
      ELSE IF (ITRZ.EQ.16) THEN
        CALL MPFMIL(UX, UY, XX, YY)
      ELSE IF (ITRZ.EQ.17) THEN
        CALL MPFRBS(UX, UY, XX, YY)
      ELSE IF (ITRZ.EQ.18) THEN
        CALL MPFSIN(UX, UY, XX, YY)
      ELSE IF (ITRZ.EQ.19) THEN
        CALL MPFVDG(UX, UY, XX, YY)
      ELSE IF (ITRZ.EQ.20) THEN
        CALL MPFCON(UX, UY, XX, YY)
      ELSE IF (ITRZ.EQ.21) THEN
        CALL MPFCOA(UX, UY, XX, YY)
      ELSE IF (ITRZ.EQ.22) THEN
        CALL MPFCOC(UX, UY, XX, YY)
      ELSE IF (ITRZ.EQ.23) THEN
        CALL MPFBON(UX, UY, XX, YY)
      ELSE IF (ITRZ.EQ.24) THEN
        CALL MPFPLC(UX, UY, XX, YY)
      ELSE IF (ITRZ.EQ.30) THEN
        CALL MPFOTG(UX, UY, XX, YY)
      ELSE IF (ITRZ.EQ.31) THEN
        CALL MPFPST(UX, UY, XX, YY)
      ELSE IF (ITRZ.EQ.32) THEN
        CALL MPFAZM(UX, UY, XX, YY)
      ELSE IF (ITRZ.EQ.33) THEN
        CALL MPFAZA(UX, UY, XX, YY)
      ELSE IF (ITRZ.EQ.34) THEN
        CALL MPFGNO(UX, UY, XX, YY)
      ELSE IF (ITRZ.EQ.40) THEN
        CALL MPFVDG(UX, UY, XX, YY)
      ELSE IF (ITRZ.EQ.51) THEN
        CALL G2FCTR(UX, UY, XX, YY)
      ELSE IF (ITRZ.EQ.99) THEN
        CALL STFUSR(UX, UY, XX, YY)
      END IF

      IF (XX.EQ.RNA .OR. YY.EQ.RNA) THEN
        VX = RNA
        VY = RNA
      ELSE
        VX = CX*XX + VX0
        VY = CY*YY + VY0
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY STITRN(VX, VY, UX, UY)

      XX = (VX - VX0) / CX
      YY = (VY - VY0) / CY

      IF (ITRZ.EQ.1) THEN
        UX = XX
        UY = YY
      ELSE IF (ITRZ.EQ.2) THEN
        CALL SGQWND(UXMIN,UXMAX,UYMIN,UYMAX)
        UX = XX
        UY = SIGN(1.0,UYMIN)*10**YY
      ELSE IF (ITRZ.EQ.3) THEN
        CALL SGQWND(UXMIN,UXMAX,UYMIN,UYMAX)
        UX = SIGN(1.0,UXMIN)*10**XX
        UY = VY
      ELSE IF (ITRZ.EQ.4) THEN
        CALL SGQWND(UXMIN,UXMAX,UYMIN,UYMAX)
        UX = SIGN(1.0,UXMIN)*10**XX
        UY = SIGN(1.0,UYMIN)*10**YY
      ELSE IF (ITRZ.EQ.5) THEN
        CALL CT2CP(XX, YY, UX, UY)
      ELSE IF (ITRZ.EQ.6) THEN
        CALL MSGDMP('E', 'STITRN',
     +       'INVERSE TRANSFORMATION IS NOT DEFINED FOR ITR=6.')
      ELSE IF (ITRZ.EQ.10) THEN
        CALL MPICYL(XX, YY, UX, UY)
      ELSE IF (ITRZ.EQ.11) THEN
        CALL MPIMER(XX, YY, UX, UY)
      ELSE IF (ITRZ.EQ.12) THEN
        CALL MPIMWD(XX, YY, UX, UY)
      ELSE IF (ITRZ.EQ.13) THEN
        CALL MPIHMR(XX, YY, UX, UY)
      ELSE IF (ITRZ.EQ.14) THEN
        CALL MPIEK6(XX, YY, UX, UY)
      ELSE IF (ITRZ.EQ.15) THEN
        CALL MPIKTD(XX, YY, UX, UY)
      ELSE IF (ITRZ.EQ.16) THEN
        CALL MPIMIL(XX, YY, UX, UY)
      ELSE IF (ITRZ.EQ.17) THEN
        CALL MPIRBS(XX, YY, UX, UY)
      ELSE IF (ITRZ.EQ.18) THEN
        CALL MPISIN(XX, YY, UX, UY)
      ELSE IF (ITRZ.EQ.19) THEN
        CALL MPIVDG(XX, YY, UX, UY)
      ELSE IF (ITRZ.EQ.20) THEN
        CALL MPICON(XX, YY, UX, UY)
      ELSE IF (ITRZ.EQ.21) THEN
        CALL MPICOA(XX, YY, UX, UY)
      ELSE IF (ITRZ.EQ.22) THEN
        CALL MPICOC(XX, YY, UX, UY)
      ELSE IF (ITRZ.EQ.23) THEN
        CALL MPIBON(XX, YY, UX, UY)
      ELSE IF (ITRZ.EQ.24) THEN
        CALL MPIPLC(XX, YY, UX, UY)
      ELSE IF (ITRZ.EQ.30) THEN
        CALL MPIOTG(XX, YY, UX, UY)
      ELSE IF (ITRZ.EQ.31) THEN
        CALL MPIPST(XX, YY, UX, UY)
      ELSE IF (ITRZ.EQ.32) THEN
        CALL MPIAZM(XX, YY, UX, UY)
      ELSE IF (ITRZ.EQ.33) THEN
        CALL MPIAZA(XX, YY, UX, UY)
      ELSE IF (ITRZ.EQ.34) THEN
        CALL MPIGNO(XX, YY, UX, UY)
      ELSE IF (ITRZ.EQ.51) THEN
        CALL G2ICTR(XX, YY, UX, UY)
      ELSE IF (ITRZ.EQ.99) THEN
        CALL STIUSR(XX, YY, UX, UY)
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY STSTRI(ITR)

      ITRZ = ITR

      CALL GLRGET('RUNDEF',RNA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY STSTRP(CXA, CYA, VXOFF, VYOFF)

      CX  = CXA
      CY  = CYA
      VX0 = VXOFF
      VY0 = VYOFF

      RETURN
      END
