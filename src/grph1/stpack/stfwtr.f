*-----------------------------------------------------------------------
*     WORKSTATION TRANSFORMATION
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE STFWTR(RX,RY,WX,WY)

      LOGICAL   LWTRFZ,LGSWRC,LPRMSG,LRNE1

      EXTERNAL  LRNE1

      SAVE

      DATA      LWTRFZ/.FALSE./,LGSWRC/.FALSE./,LPRMSG/.TRUE./


      IF (.NOT.LWTRFZ) THEN
        CALL MSGDMP('E','STFWTR',
     +    'WORKSTATION TRANSFORMATION IS NOT DEFINED.')
      END IF
      IF (.NOT.LGSWRC) THEN
        CALL MSGDMP('E','STFWTR',
     +    'WORKSTATION RECTANGLE IS NOT DEFINED.')
      END IF

      WXX = CX*RX + WX0
      WYY = CY*RY + WY0
      IF (IWTRFZ.EQ.1) THEN
        WX =  WXX + WSXMNZ
        WY =  WYY + WSYMNZ
      ELSE IF (IWTRFZ.EQ.2) THEN
        WX =  WYY + WSXMNZ
        WY = -WXX + WSYMXZ
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY STIWTR(WX,WY,RX,RY)

      IF (.NOT.LWTRFZ) THEN
        CALL MSGDMP('E','STIWTR',
     +    'WORKSTATION TRANSFORMATION IS NOT DEFINED.')
      END IF
      IF (.NOT.LGSWRC) THEN
        CALL MSGDMP('E','STIWTR',
     +    'WORKSTATION RECTANGLE IS NOT DEFINED.')
      END IF

      IF (IWTRFZ.EQ.1) THEN
        WXX =  WX - WSXMNZ
        WYY =  WY - WSYMNZ
      ELSE IF (IWTRFZ.EQ.2) THEN
        WXX = -WY + WSYMXZ
        WYY =  WX - WSXMNZ
      END IF
      RX = (WXX - WX0) / CX
      RY = (WYY - WY0) / CY

      RETURN
*-----------------------------------------------------------------------
      ENTRY STSWTR(RXMIN,RXMAX,RYMIN,RYMAX,
     +             WXMIN,WXMAX,WYMIN,WYMAX,IWTRF)

      IF (.NOT.(RXMIN.LT.RXMAX .AND. RYMIN.LT.RYMAX)) THEN
        CALL MSGDMP('E','STSWTR',
     +    'WORKSTATION WINDOW DEFINITION IS INVALID.')
      END IF
      IF (.NOT.((0.LE.RXMIN .AND. RXMAX.LE.1)
     +    .AND. (0.LE.RYMIN .AND. RYMAX.LE.1))) THEN
        CALL MSGDMP('E','STSWTR','WORKSTATION WINDOW IS NOT WITHIN '
     +    //'THE NORMALIZED DEVICE COORDINATE UNIT SQUARE.')
      END IF
      IF (.NOT.(IWTRF.EQ.1 .OR. IWTRF.EQ.2)) THEN
        CALL MSGDMP('E','STSWTR',
     +    'TRANSFORMATION FUNCTION NUMBER IS INVALID.')
      END IF

      RWN = (RYMAX-RYMIN) / (RXMAX-RXMIN)
      RVP = (WYMAX-WYMIN) / (WXMAX-WXMIN)

      IF (LRNE1(RWN,RVP).AND.LPRMSG) THEN
        CALL MSGDMP('W','STSWTR',
     +    'WORKSTATION VIEWPORT WAS MODIFIED.')
        LPRMSG=.FALSE.
      END IF

      DX  = WXMAX - WXMIN
      DY  = WYMAX - WYMIN
      DWX = MIN (DX,     DY/RWN)
      DWY = MIN (DX*RWN, DY    )

      WXMINZ = (WXMIN + WXMAX - DWX)/2
      WXMAXZ = (WXMIN + WXMAX + DWX)/2
      WYMINZ = (WYMIN + WYMAX - DWY)/2
      WYMAXZ = (WYMIN + WYMAX + DWY)/2

      RXMINZ = RXMIN
      RXMAXZ = RXMAX
      RYMINZ = RYMIN
      RYMAXZ = RYMAX

      IWTRFZ = IWTRF
      LWTRFZ = .TRUE.

      CX = (WXMAXZ-WXMINZ) / (RXMAXZ-RXMINZ)
      CY = (WYMAXZ-WYMINZ) / (RYMAXZ-RYMINZ)
      WX0 = WXMINZ - CX*RXMINZ
      WY0 = WYMINZ - CY*RYMINZ

      CALL SZSCLL(RXMINZ,RXMAXZ,RYMINZ,RYMAXZ,0)

      RETURN
*-----------------------------------------------------------------------
      ENTRY STQWTR(RXMIN,RXMAX,RYMIN,RYMAX,
     +             WXMIN,WXMAX,WYMIN,WYMAX,IWTRF)

      IF (.NOT.LWTRFZ) THEN
        CALL MSGDMP('E','STQWTR',
     +    'WORKSTATION TRANSFORMATION IS NOT DEFINED.')
      END IF

      RXMIN = RXMINZ
      RXMAX = RXMAXZ
      RYMIN = RYMINZ
      RYMAX = RYMAXZ

      WXMIN = WXMINZ
      WXMAX = WXMAXZ
      WYMIN = WYMINZ
      WYMAX = WYMAXZ

      IWTRF = IWTRFZ

      RETURN
*-----------------------------------------------------------------------
      ENTRY STSWRC(WSXMN,WSXMX,WSYMN,WSYMX)

      IF (.NOT.(WSXMN.LT.WSXMX .AND. WSYMN.LT.WSYMX)) THEN
        CALL MSGDMP('E','STSWRC',
     +    'WORKSTATION RECTANGLE IS INVALID.')
      END IF

      WSXMNZ = WSXMN
      WSXMXZ = WSXMX
      WSYMNZ = WSYMN
      WSYMXZ = WSYMX
      LGSWRC = .TRUE.

      RETURN
*-----------------------------------------------------------------------
      ENTRY STQWRC(WSXMN,WSXMX,WSYMN,WSYMX)

      IF (.NOT.LGSWRC) THEN
        CALL MSGDMP('E','STQWRC',
     +    'WORKSTATION RECTANGLE IS NOT DEFINED.')
      END IF

      WSXMN = WSXMNZ
      WSXMX = WSXMXZ
      WSYMN = WSYMNZ
      WSYMX = WSYMXZ

      RETURN
      END
