*----------------------------------------------------------------------
*     ROTATION OF LON-LAT COORDINATE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE STFROT(UX, UY, TX, TY)

      EXTERNAL  RFPI

      SAVE


      CALL CR3S(THETAZ, PHIZ, PSIZ, PI2-UY, UX, Y1, TX)
      TY = PI2 - Y1

      RETURN
*-----------------------------------------------------------------------
      ENTRY STIROT(TX, TY, UX, UY)

      CALL CR3S(-THETAZ, -PSIZ, -PHIZ, PI2-TY, TX, Y1, UX)
      UY = PI2 - Y1

      RETURN
*-----------------------------------------------------------------------
      ENTRY STSROT(THETA, PHI, PSI)

      THETAZ = THETA
      PHIZ   = PHI
      PSIZ   = PSI

      PI2 = RFPI()/2

      RETURN
      END
