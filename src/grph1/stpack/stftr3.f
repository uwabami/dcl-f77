*-----------------------------------------------------------------------
*     3-D TRANSFORMATION
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE STFTR3(UX, UY, UZ, VX, VY, VZ)

      LOGICAL   LXRD, LYRD, LZRD, LXLG, LYLG, LZLG

      LOGICAL   LMISS, LXRDZ, LYRDZ, LZRDZ, LXLGZ, LYLGZ, LZLGZ

      EXTERNAL  RFPI

      SAVE


      IF (LMISS .AND. 
     #    (UX.EQ.RMISS .OR. UY.EQ.RMISS .OR. UZ.EQ.RMISS)) THEN
        VX = RMISS
        VY = RMISS
        VZ = RMISS
        RETURN
      END IF

      XX = UX
      YY = UY
      ZZ = UZ

      IF (ITRZ.EQ.1) THEN
        IF (LXLGZ.OR.LYLGZ.OR.LZLGZ) 
     +       CALL SCQWND(UXMIN3,UXMAX3,UYMIN3,UYMAX3,UZMIN3,UZMAX3)
        IF (LXLGZ) THEN
          XX = SIGN(1.0,UXMIN3)*XX
          IF (XX.GT.0.0) THEN
            XX = LOG10(XX)
          ELSE
            CALL MSGDMP('E','STFTR3',
     +           'UX HAS INVALID SIGN FOR LOG TRANSFORMATION OR IS 0')
          END IF
        END IF
        IF (LYLGZ) THEN
          YY = SIGN(1.0,UYMIN3)*YY
          IF (YY.GT.0.0) THEN
            YY = LOG10(YY)
          ELSE
            CALL MSGDMP('E','STFTR3',
     +           'UY HAS INVALID SIGN FOR LOG TRANSFORMATION OR IS 0')
          END IF
        END IF
        IF (LZLGZ) THEN
          ZZ = SIGN(1.0,UZMIN3)*ZZ
          IF (ZZ.GT.0.0) THEN
            ZZ = LOG10(ZZ)
          ELSE
            CALL MSGDMP('E','STFTR3',
     +           'UZ HAS INVALID SIGN FOR LOG TRANSFORMATION OR IS 0')
          END IF
        END IF
        XXX = XX
        YYY = YY
        ZZZ = ZZ
      ELSE IF (ITRZ.EQ.2) THEN
        IF (LXLGZ.OR.LZLGZ) 
     +       CALL SCQWND(UXMIN3,UXMAX3,UYMIN3,UYMAX3,UZMIN3,UZMAX3)
        IF (LXLGZ) THEN 
          XX = SIGN(1.0,UXMIN3)*XX
          IF (XX.GT.0.0) THEN
            XX = LOG10(XX)
          ELSE
            CALL MSGDMP('E','STFTR3',
     +           'UX HAS INVALID SIGN FOR LOG TRANSFORMATION OR IS 0')
          END IF
        END IF
        IF (LYRDZ) YY = YY*CP
        IF (LZLGZ) THEN
          ZZ = SIGN(1.0,UZMIN3)*ZZ
          IF (ZZ.GT.0.0) THEN
            ZZ = LOG10(ZZ)
          ELSE
            CALL MSGDMP('E','STFTR3',
     +           'UZ HAS INVALID SIGN FOR LOG TRANSFORMATION OR IS 0')
          END IF
        END IF
        ZZZ = ZZ
        CALL CT2PC(XX,YY,XXX,YYY)
      ELSE IF (ITRZ.EQ.3) THEN
        IF (LXLGZ) 
     +     CALL SCQWND(UXMIN3,UXMAX3,UYMIN3,UYMAX3,UZMIN3,UZMAX3)
        IF (LXLGZ) THEN
          XX = SIGN(1.0,UXMIN3)*XX
          IF (XX.GT.0.0) THEN
            XX = LOG10(XX)
          ELSE
            CALL MSGDMP('E','STFTR3',
     +           'UX HAS INVALID SIGN FOR LOG TRANSFORMATION OR IS 0')
          END IF
        END IF
        IF(LYRDZ) YY = YY*CP
        IF(LZRDZ) ZZ = ZZ*CP
        CALL CT3SC(XX, YY, ZZ, XXX, YYY, ZZZ)
      END IF

      VX = CX*XXX + VX0
      VY = CY*YYY + VY0
      VZ = CZ*ZZZ + VZ0

      RETURN
*-----------------------------------------------------------------------
      ENTRY STSTR3(ITR, CXA, CYA, CZA, VXOFF, VYOFF, VZOFF)

      ITRZ = ITR
      CX  = CXA
      CY  = CYA
      CZ  = CZA
      VX0 = VXOFF
      VY0 = VYOFF
      VZ0 = VZOFF
      CP  = RFPI()/180

      CALL GLLGET('LMISS',LMISS)
      CALL GLRGET('RMISS',RMISS)

      RETURN
*-----------------------------------------------------------------------
      ENTRY STSRD3(LXRD, LYRD, LZRD)

      LXRDZ = LXRD
      LYRDZ = LYRD
      LZRDZ = LZRD

      RETURN
*-----------------------------------------------------------------------
      ENTRY STSLG3(LXLG, LYLG, LZLG)

      LXLGZ = LXLG
      LYLGZ = LYLG
      LZLGZ = LZLG

      END
