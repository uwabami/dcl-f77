*-----------------------------------------------------------------------
*     SLPVPC : PLOT VIEWPORT CORNER MARKS
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SLPVPC(INDEX,RC)


      IF (INDEX.LE.0) THEN
        IF (INDEX.EQ.0) THEN
          CALL MSGDMP('M','SLPVPC','LINE INDEX IS ZERO / DO NOTHING.')
          RETURN
        ELSE
          CALL MSGDMP('E','SLPVPC','LINE INDEX IS LESS THAN ZERO.')
        END IF
      END IF

      IF (RC.LE.0) THEN
        IF (RC.EQ.0) THEN
          CALL MSGDMP('E','SLPVPC',
     +         'LENGTH OF CORNER MARKS IS ZERO / DO NOTHING.')
          RETURN
        ELSE
          CALL MSGDMP('E','SLPVPC',
     +         'LENGTH OF CORNER MARKS IS LESS THAN ZERO.')
        END IF
      END IF

      CALL SGQVPT(VXMIN,VXMAX,VYMIN,VYMAX)

      CALL SZSLTI(1,INDEX)

      CALL SZOPLV
      CALL SZMVLV(VXMIN   ,VYMIN+RC)
      CALL SZPLLV(VXMIN   ,VYMIN   )
      CALL SZPLLV(VXMIN+RC,VYMIN   )
      CALL SZCLLV
      CALL SZOPLV
      CALL SZMVLV(VXMAX-RC,VYMIN   )
      CALL SZPLLV(VXMAX   ,VYMIN   )
      CALL SZPLLV(VXMAX   ,VYMIN+RC)
      CALL SZCLLV
      CALL SZOPLV
      CALL SZMVLV(VXMAX   ,VYMAX-RC)
      CALL SZPLLV(VXMAX   ,VYMAX   )
      CALL SZPLLV(VXMAX-RC,VYMAX   )
      CALL SZCLLV
      CALL SZOPLV
      CALL SZMVLV(VXMIN+RC,VYMAX   )
      CALL SZPLLV(VXMIN   ,VYMAX   )
      CALL SZPLLV(VXMIN   ,VYMAX-RC)
      CALL SZCLLV

      END
