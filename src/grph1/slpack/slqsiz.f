*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SLQSIZ(CSIZE,DX,DY)

      CHARACTER CSIZE*(*)

      LOGICAL   LCHREQ,LCHRD
      CHARACTER CS1*1,CS2*1

      EXTERNAL  LCHREQ,LCHRD


      CS1=CSIZE(1:1)
      CS2=CSIZE(2:2)
      IF (LCHREQ(CS1,'A')) THEN
        DX=118.9
        DY=84.1
      ELSE IF (LCHREQ(CS1,'B')) THEN
        DX=145.6
        DY=103.0
      ELSE
        CALL MSGDMP('E','SLQSIZ','SIZE PARAMETER IS INVALID.')
      END IF

      IF (.NOT.LCHRD(CS2)) THEN
        CALL MSGDMP('E','SLQSIZ','SIZE PARAMETER IS INVALID.')
      END IF

      READ(CS2,'(I1)') NS
      DO 10 I=1,NS
        DX2=DX/2
        DX=DY
        DY=DX2
   10 CONTINUE

      END
