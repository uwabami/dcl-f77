*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SLRAT(RX,RY)

      IF (RX.LT.0 .OR. RY.LT.0) THEN
        CALL MSGDMP('E','SLRAT ',
     +    'PROPORTION PARAMETER IS LESS THAN ZERO.')
      END IF

      CALL SGIGET('NLEVEL',LEV)

      CALL SLQRCT(LEV,1,XAMIN,XAMAX,YAMIN,YAMAX)
      ARAT=(YAMAX-YAMIN)/(XAMAX-XAMIN)
      BRAT=RY/RX
      IF (ARAT.GE.BRAT) THEN
        XX=0
        YY=(1-BRAT/ARAT)/2
      ELSE
        XX=(1-ARAT/BRAT)/2
        YY=0
      END IF
      CALL SLMGN(XX,XX,YY,YY)

      END
