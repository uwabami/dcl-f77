*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SLDIV(CFORM,IX,IY)

      CHARACTER CFORM*(*)

      LOGICAL   LCHREQ
      CHARACTER CF1*1

      COMMON    /SLBLK1/ XMIN,XMAX,YMIN,YMAX,NN
      PARAMETER (MAXFR=1000,MAXLEV=3)
      INTEGER   NN(0:MAXLEV)
      REAL      XMIN(MAXFR),XMAX(MAXFR),YMIN(MAXFR),YMAX(MAXFR)

      EXTERNAL  LCHREQ,ISUM0

      SAVE


      CF1=CFORM(1:1)
      IF (.NOT.(LCHREQ(CF1,'T') .OR. LCHREQ(CF1,'Y')
     +     .OR. LCHREQ(CF1,'L') .OR. LCHREQ(CF1,'S'))) THEN
        CALL MSGDMP('E','SLDIV ','DIVISION DIRECTION IS INVALID.')
      END IF

      CALL SGIGET('NLEVEL',LEV)

      IF (LEV.GE.MAXLEV) THEN
        CALL MSGDMP('E','SLDIV ',
     +    'NUMBER OF DIVISION IS IN EXCESS OF MUXIMUM.')
      END IF

      LEV=LEV+1
      NXY=IX*IY
      NN(LEV)=NXY*NN(LEV-1)
      ND=ISUM0(NN,LEV+1,1)
      IF (ND.GT.MAXFR) THEN
        CALL MSGDMP('E','SLDIV ',
     +    'TOTAL FRAME NUMBER IS IN EXCESS OF MAXIMUM.')
      END IF

      L1=ISUM0(NN,LEV-1,1)+1
      L2=ISUM0(NN,LEV,1)
      LN=L2+1-NXY
      DO 10 LI=L1,L2
        LN=LN+NXY
        CALL SLDIVZ(XMIN(LI),XMAX(LI),YMIN(LI),YMAX(LI),CFORM,IX,IY,
     +              XMIN(LN),XMAX(LN),YMIN(LN),YMAX(LN))
   10 CONTINUE

      CALL SGISET('NLEVEL',LEV)

      END
