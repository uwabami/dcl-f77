*-----------------------------------------------------------------------
*     BASIC PLOT SUBROUTINES
*-----------------------------------------------------------------------
*     SLPVPR : PLOT VIEWPORT RECTANGLE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SLPVPR(INDEX)


      IF (INDEX.LE.0) THEN
        IF (INDEX.EQ.0) THEN
          CALL MSGDMP('M','SLPVPR','LINE INDEX IS ZERO / DO NOTHING.')
          RETURN
        ELSE
          CALL MSGDMP('E','SLPVPR','LINE INDEX IS LESS THAN ZERO.')
        END IF
      END IF

      CALL SGQVPT(VXMIN,VXMAX,VYMIN,VYMAX)

      CALL SZSLTI(1,INDEX)

      CALL SZOPLV
      CALL SZMVLV(VXMIN,VYMIN)
      CALL SZPLLV(VXMAX,VYMIN)
      CALL SZPLLV(VXMAX,VYMAX)
      CALL SZPLLV(VXMIN,VYMAX)
      CALL SZPLLV(VXMIN,VYMIN)
      CALL SZCLLV

      END
