*-----------------------------------------------------------------------
*     SLPWWC : PLOT WORKSTATION-WINDOW CORNER MARKS
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SLPWWC(INDEX,RC)


      IF (INDEX.LE.0) THEN
        IF (INDEX.EQ.0) THEN
          CALL MSGDMP('M','SLPWWC','LINE INDEX IS ZERO / DO NOTHING.')
          RETURN
        ELSE
          CALL MSGDMP('E','SLPWWC','LINE INDEX IS LESS THAN ZERO.')
        END IF
      END IF

      IF (RC.LE.0) THEN
        IF (RC.EQ.0) THEN
          CALL MSGDMP('E','SLPWWC',
     +         'LENGTH OF CORNER MARKS IS ZERO / DO NOTHING.')
          RETURN
        ELSE
          CALL MSGDMP('E','SLPWWC',
     +         'LENGTH OF CORNER MARKS IS LESS THAN ZERO.')
        END IF
      END IF

      CALL STQWTR(RXMIN,RXMAX,RYMIN,RYMAX,WXMIN,WXMAX,WYMIN,WYMAX,ITR)

      CALL SZSLTI(1,INDEX)

      CALL SZOPLR
      CALL SZMVLR(RXMIN   ,RYMIN+RC)
      CALL SZPLLR(RXMIN   ,RYMIN   )
      CALL SZPLLR(RXMIN+RC,RYMIN   )
      CALL SZCLLR
      CALL SZOPLR
      CALL SZMVLR(RXMAX-RC,RYMIN   )
      CALL SZPLLR(RXMAX   ,RYMIN   )
      CALL SZPLLR(RXMAX   ,RYMIN+RC)
      CALL SZCLLR
      CALL SZOPLR
      CALL SZMVLR(RXMAX   ,RYMAX-RC)
      CALL SZPLLR(RXMAX   ,RYMAX   )
      CALL SZPLLR(RXMAX-RC,RYMAX   )
      CALL SZCLLR
      CALL SZOPLR
      CALL SZMVLR(RXMIN+RC,RYMAX   )
      CALL SZPLLR(RXMIN   ,RYMAX   )
      CALL SZPLLR(RXMIN   ,RYMAX-RC)
      CALL SZCLLR

      END
