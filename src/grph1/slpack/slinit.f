*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SLINIT(WXMAX,WYMAX,FACT)

      CHARACTER CSZEZ*(*)

      LOGICAL   LCHREQ,LCHRD
      CHARACTER CSIZE*3,CS1*1,CS2*1,CS3*1

      COMMON    /SLBLK1/ XMIN,XMAX,YMIN,YMAX,NN
      PARAMETER (MAXFR=1000,MAXLEV=3)
      INTEGER   NN(0:MAXLEV)
      REAL      XMIN(MAXFR),XMAX(MAXFR),YMIN(MAXFR),YMAX(MAXFR)

      EXTERNAL  LCHREQ,LENC,LCHRD

      SAVE


      IF (.NOT.(WXMAX.GT.0 .AND. WYMAX.GT.0)) THEN
        CALL MSGDMP('E','SLINIT',
     +    'WIDTH OF LAYOUT RECTANGLE IS LESS THAN ZERO.')
      END IF
      IF (.NOT.(FACT.GT.0)) THEN
        CALL MSGDMP('E','SLINIT','SCALING FACTOR IS LESS THAN ZERO.')
      END IF

      NN(0)=1
      XMIN(1)=0
      XMAX(1)=WXMAX
      YMIN(1)=0
      YMAX(1)=WYMAX

      NN(1)=1
      XMIN(2)=0
      XMAX(2)=WXMAX
      YMIN(2)=0
      YMAX(2)=WYMAX

      CALL SGISET('NLEVEL',1)
      CALL SGRSET('FACTOR',FACT)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SLSIZE(CSZEZ)

      CSIZE=CSZEZ
      CS1=CSIZE(1:1)
      CS2=CSIZE(2:2)
      CS3=CSIZE(3:3)
      IF (.NOT.(LCHREQ(CS1,'A') .OR. LCHREQ(CS1,'B'))) THEN
        CALL MSGDMP('E','SLSIZE','SIZE PARAMETER IS INVALID.')
      END IF
      IF (.NOT.LCHRD(CS2)) THEN
        CALL MSGDMP('E','SLSIZE','SIZE PARAMETER IS INVALID.')
      END IF

      CALL SLQSIZ(CSIZE,DXX,DYY)

      IF (LCHREQ(CS3,'Y') .OR. LCHREQ(CS3,'L')) THEN
        DX=DXX
        DY=DYY
      ELSE IF (LCHREQ(CS3,'T') .OR. LCHREQ(CS3,'P')) THEN
        DX=DYY
        DY=DXX
      ELSE
        IF (.NOT.(LENC(CSIZE).EQ.2 .OR. LCHREQ(CS3,'A'))) THEN
          CALL MSGDMP('W','SLSIZE','SIZE PARAMETER IS INVALID.')
          CALL MSGDMP('M','-CNT.-',
     +      ''''//CSIZE(1:2)//'A'//''' IS ASSUMED.')
        END IF
        IF (XMAX(1)-XMIN(1).GT.YMAX(1)-YMIN(1)) THEN
          DX=DXX
          DY=DYY
        ELSE
          DX=DYY
          DY=DXX
        END IF
      END IF

      CALL SGRGET('FACTOR',FACTR)

      BWX=(XMAX(2)-XMIN(2))*FACTR
      BWY=(YMAX(2)-YMIN(2))*FACTR

      IF (BWX.LT.DX .OR. BWY.LT.DY) THEN
        CALL MSGDMP('W','SLSIZE',
     +    'SELECTED SIZE '''//CSIZE//''' CANNOT BE ASSIGNED.')
        CALL MSGDMP('M','-CNT.-','DO NOTHING.')
        RETURN
      END IF

      XX=(1-DX/BWX)/2
      YY=(1-DY/BWY)/2
      CALL SLMGNZ(XMIN(1),XMAX(1),YMIN(1),YMAX(1),XX,XX,YY,YY)

      XMIN(2)=XMIN(1)
      XMAX(2)=XMAX(1)
      YMIN(2)=YMIN(1)
      YMAX(2)=YMAX(1)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SLFORM(DXA,DYA)

      DX=DXA
      DY=DYA

      CALL SGRGET('FACTOR',FACTR)

      BWX=(XMAX(2)-XMIN(2))*FACTR
      BWY=(YMAX(2)-YMIN(2))*FACTR

      IF (BWX.LT.DX .OR. BWY.LT.DY) THEN
        CALL MSGDMP('W','SLFORM','SELECTED SIZE CANNOT BE ASSIGNED.')
        CALL MSGDMP('M','-CNT.-','DO NOTHING.')
        RETURN
      END IF

      XX=(1-DX/BWX)/2
      YY=(1-DY/BWY)/2
      CALL SLMGNZ(XMIN(1),XMAX(1),YMIN(1),YMAX(1),XX,XX,YY,YY)

      XMIN(2)=XMIN(1)
      XMAX(2)=XMAX(1)
      YMIN(2)=YMIN(1)
      YMAX(2)=YMAX(1)

      RETURN
      END
