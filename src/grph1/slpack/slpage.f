*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SLPAGE(LEV,IFRM,IPAGE)

      COMMON    /SLBLK1/ XMIN,XMAX,YMIN,YMAX,NN
      PARAMETER (MAXFR=1000,MAXLEV=3)
      INTEGER   NN(0:MAXLEV)
      REAL      XMIN(MAXFR),XMAX(MAXFR),YMIN(MAXFR),YMAX(MAXFR)

      CALL SGIGET('NLEVEL',LEVC)

      IF (.NOT.(0.LE.LEV .AND. LEV.LE.LEVC)) THEN
        CALL MSGDMP('E','SLPAGE','LEVEL NUMBER IS INVALID.')
      END IF
      IF (IFRM.LE.0) THEN
        CALL MSGDMP('E','SLPAGE','FRAME NUMBER IS LESS THAN ZERO.')
      END IF

      IPAGE=(IFRM-1)/NN(LEV)+1
      IFRM=MOD(IFRM-1,NN(LEV))+1

      END
