*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SLMGN(XL,XR,YB,YT)

      COMMON    /SLBLK1/ XMIN,XMAX,YMIN,YMAX,NN
      PARAMETER (MAXFR=1000,MAXLEV=3)
      INTEGER   NN(0:MAXLEV)
      REAL      XMIN(MAXFR),XMAX(MAXFR),YMIN(MAXFR),YMAX(MAXFR)

      EXTERNAL  ISUM0


      IF (XL.LT.0 .OR. XR.LT.0 .OR. YB.LT.0 .OR. YT.LT.0) THEN
        CALL MSGDMP('E','SLMGN ',
     +    'MARGIN PARAMETER IS LESS THAN ZERO.')
      END IF
      IF (XL+XR.GE.1.0 .OR. YB+YT.GE.1.0) THEN
        CALL MSGDMP('E','SLMGN ',
     +    'SUM OF MARGIN PARAMETERS IS LARGER THAN 1.0.')
      END IF

      CALL SGIGET('NLEVEL',LEV)

      L1=ISUM0(NN,LEV,1)+1
      L2=ISUM0(NN,LEV+1,1)
      DO 10 LI=L1,L2
        CALL SLMGNZ(XMIN(LI),XMAX(LI),YMIN(LI),YMAX(LI),XL,XR,YB,YT)
   10 CONTINUE

      END
