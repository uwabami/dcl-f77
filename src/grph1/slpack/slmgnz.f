*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SLMGNZ(XAMIN,XAMAX,YAMIN,YAMAX,XL,XR,YB,YT)


      WXA=XAMAX-XAMIN
      WYA=YAMAX-YAMIN

      IF (WXA.LT.0 .OR. WYA.LT.0) THEN
        CALL MSGDMP('E','SLMGNZ','RECTANGLE DEFINITION IS INVALID.')
      END IF
      IF (XL.LT.0 .OR. XR.LT.0 .OR. YB.LT.0 .OR. YT.LT.0) THEN
        CALL MSGDMP('E','SLMGNZ','MARGIN PARAMETER IS LESS THAN ZERO.')
      END IF
      IF (XL+XR.GE.1.0 .OR. YB+YT.GE.1.0) THEN
        CALL MSGDMP('E','SLMGNZ',
     +    'SUM OF MARGIN PARAMETERS IS LARGER THAN 1.0.')
      END IF

      XAMIN=XAMIN+WXA*XL
      XAMAX=XAMAX-WXA*XR
      YAMIN=YAMIN+WYA*YB
      YAMAX=YAMAX-WYA*YT

      END
