*-----------------------------------------------------------------------
*     SLPWWR : PLOT WORKSTATION-WINDOW RECTANGLE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SLPWWR(INDEX)


      IF (INDEX.LE.0) THEN
        IF (INDEX.EQ.0) THEN
          CALL MSGDMP('M','SLPWWR','LINE INDEX IS ZERO / DO NOTHING.')
          RETURN
        ELSE
          CALL MSGDMP('E','SLPWWR','LINE INDEX IS LESS THAN ZERO.')
        END IF
      END IF

      CALL STQWTR(RXMIN,RXMAX,RYMIN,RYMAX,WXMIN,WXMAX,WYMIN,WYMAX,ITR)

      CALL SZSLTI(1,INDEX)

      CALL SZOPLR
      CALL SZMVLR(RXMIN,RYMIN)
      CALL SZPLLR(RXMAX,RYMIN)
      CALL SZPLLR(RXMAX,RYMAX)
      CALL SZPLLR(RXMIN,RYMAX)
      CALL SZPLLR(RXMIN,RYMIN)
      CALL SZCLLR

      END
