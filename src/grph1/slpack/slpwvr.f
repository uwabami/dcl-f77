*-----------------------------------------------------------------------
*     SLPWWR : PLOT MAXIMUM-DRAWING-REGION RECTANGLE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SLPWVR(INDEX)


      IF (INDEX.LE.0) THEN
        IF (INDEX.EQ.0) THEN
          CALL MSGDMP('M','SLPWVR','LINE INDEX IS ZERO / DO NOTHING.')
          RETURN
        ELSE
          CALL MSGDMP('E','SLPWVR','LINE INDEX IS LESS THAN ZERO.')
        END IF
      END IF

      CALL STQWTR(RXMN,RXMX,RYMN,RYMX,WXMN,WXMX,WYMN,WYMX,ITR)
      IF (ITR.EQ.1) THEN
        CALL STQWRC(WXMIN,WXMAX,WYMIN,WYMAX)
      ELSE
        CALL STQWRC(WXMIN,WXMAX,WYMAX,WYMIN)
      END IF

      CALL SZQCLL(CXMIN,CXMAX,CYMIN,CYMAX,0)

      CALL STIWTR(WXMIN,WYMIN,RXMIN,RYMIN)
      CALL STIWTR(WXMAX,WYMAX,RXMAX,RYMAX)

      CALL SZSCLL(RXMIN,RXMAX,RYMIN,RYMAX,0)
      CALL SZSLTI(1,INDEX)

      CALL SZOPLR
      CALL SZMVLR(RXMIN,RYMIN)
      CALL SZPLLR(RXMAX,RYMIN)
      CALL SZPLLR(RXMAX,RYMAX)
      CALL SZPLLR(RXMIN,RYMAX)
      CALL SZPLLR(RXMIN,RYMIN)
      CALL SZCLLR

      CALL SZSCLL(CXMIN,CXMAX,CYMIN,CYMAX,0)

      END
