*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SLTLCV(CTLIN,CTLOUT,LOUT)

      CHARACTER CTLIN*(*),CTLOUT*(*)

      LOGICAL   LCHREQ,LPARA
      CHARACTER CST*4,CTMP*16

      EXTERNAL  LCHREQ,LENC


      ITL=LENC(CTLIN)
      IN=0
      IC=0
   10 CONTINUE
        IN=IN+1
        IC=IC+1
        IF (CTLIN(IN:IN).EQ.'#') THEN
          CST=CTLIN(IN+1:IN+4)
          IF (LCHREQ(CST,'PAGE')) THEN
            CTMP='##'
            CALL SGIGET('NPAGE',IPAGE)
            CALL CHNGI(CTMP,'##',IPAGE,'(I2)')
            LPARA=.TRUE.
          ELSE IF (LCHREQ(CST,'DATE')) THEN
            CTMP='YY/MM/DD'
            CALL DATEQ3(IY,IM,ID)
            CALL DATEC3(CTMP,IY,IM,ID)
            LPARA=.TRUE.
          ELSE IF (LCHREQ(CST,'TIME')) THEN
            CTMP='HH:MM:SS'
            CALL TIMEQ3(JH,JM,JS)
            CALL TIMEC3(CTMP,JH,JM,JS)
            LPARA=.TRUE.
          ELSE IF (LCHREQ(CST,'PROG')) THEN
            CALL OSGARG(0,CTMP)
            LPARA=.TRUE.
          ELSE IF (LCHREQ(CST,'USER')) THEN
            CALL OSGENV('USER',CTMP)
            LPARA=.TRUE.
          ELSE
            LPARA=.FALSE.
          END IF
          IF (LPARA) THEN
            NP=LENC(CTMP)-1
            CTLOUT(IC:IC+NP)=CTMP
            IN=IN+4
            IC=IC+NP
          ELSE
            CTLOUT(IC:IC)='#'
          END IF
        ELSE
          CTLOUT(IC:IC)=CTLIN(IN:IN)
        END IF
      IF (.NOT.(IN.GE.ITL)) GO TO 10
      LOUT=IC

      END
