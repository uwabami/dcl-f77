*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGNTRF(UX, UY, VX, VY)


      CALL MSGDMP('M','SGNTRF','THIS IS OLD INTERFACE - USE STFTRF !')

      CALL STFTRF(UX, UY, VX, VY)

      END
