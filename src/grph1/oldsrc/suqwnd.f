*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SUQWND(UXMIN,UXMAX,UYMIN,UYMAX,INTRF)


      CALL MSGDMP('M','SUQWND','THIS IS OLD INTERFACE - USE SGQWND !')

      CALL SGQWND(UXMIN,UXMAX,UYMIN,UYMAX)
      CALL SGQTRN(INTRF)

      END
