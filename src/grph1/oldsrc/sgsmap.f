*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGSMAP(SIMFAC, VXOFF, VYOFF, PLX, PLY, PLROT)


      CALL MSGDMP('M','SGSMAP',
     +     'THIS IS OLD INTERFACE - USE SGSSIM/SGSMPL !')

      CALL SGSSIM(SIMFAC, VXOFF, VYOFF)
      CALL SGSMPL(PLX, PLY, PLROT)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGQMAP(SIMFAC, VXOFF, VYOFF, PLX, PLY, PLROT)

      CALL MSGDMP('M','SGQMAP',
     +     'THIS IS OLD INTERFACE - USE SGQSIM/SGQMPL !')

      CALL SGQSIM(SIMFAC, VXOFF, VYOFF)
      CALL SGQMPL(PLX, PLY, PLROT)

      RETURN
      END
