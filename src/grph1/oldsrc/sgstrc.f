*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGSTRC(CTR)

      CHARACTER CTR*(*)

      EXTERNAL  ISGTRC


      CALL MSGDMP('M','SGSTRC',
     +     'THIS IS OLD INTERFACE - USE SGSTRN(ISGTRC) !')

      CALL SGSTRN(ISGTRC(CTR))

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGQTRC(CTR)

      CALL MSGDMP('M','SGQTRC','THIS IS OLD INTERFACE - USE SGTRNL !')

      CALL SGIGET('ITR',ITR)
      CALL SGTRNL(ITR,CTR)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGQTRA(CTR)

      CALL MSGDMP('M','SGQTRA','THIS IS OLD INTERFACE - USE SGTRNS !')

      CALL SGIGET('ITR',ITR)
      CALL SGTRNS(ITR,CTR)

      RETURN
      END
