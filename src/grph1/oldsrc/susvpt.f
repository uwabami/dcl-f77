*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SUSVPT(VXMIN,VXMAX,VYMIN,VYMAX)


      CALL MSGDMP('M','SUSVPT','THIS IS OLD INTERFACE - USE SGSVPT !')

      CALL SGSVPT(VXMIN, VXMAX, VYMIN, VYMAX)
      CALL SGSTRF

      END
