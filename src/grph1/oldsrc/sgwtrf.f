*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGWTRF(VX,VY,WX,WY)


      CALL MSGDMP('M','SGWTRF','THIS IS OLD INTERFACE - USE STFWTR !')

      CALL STFWTR(VX,VY,WX,WY)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGSWTR(VXMIN,VXMAX,VYMIN,VYMAX,
     +             WXMIN,WXMAX,WYMIN,WYMAX,IWTRF)

      CALL MSGDMP('M','SGSWTR','THIS IS OLD INTERFACE - USE STSWTR !')

      CALL STSWTR(VXMIN,VXMAX,VYMIN,VYMAX,
     +            WXMIN,WXMAX,WYMIN,WYMAX,IWTRF)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGQWTR(VXMIN,VXMAX,VYMIN,VYMAX,
     +             WXMIN,WXMAX,WYMIN,WYMAX,IWTRF)

      CALL MSGDMP('M','SGQWTR','THIS IS OLD INTERFACE - USE STQWTR !')

      CALL STQWTR(VXMIN,VXMAX,VYMIN,VYMAX,
     +            WXMIN,WXMAX,WYMIN,WYMAX,IWTRF)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGSWRC(WSXMN,WSXMX,WSYMN,WSYMX)

      CALL MSGDMP('M','SGSWRC','THIS IS OLD INTERFACE - USE STSWRC !')

      CALL STSWRC(WSXMN,WSXMX,WSYMN,WSYMX)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGQWRC(WSXMN,WSXMX,WSYMN,WSYMX)

      CALL MSGDMP('M','SGQWRC','THIS IS OLD INTERFACE - USE STQWRC !')

      CALL STQWRC(WSXMN,WSXMX,WSYMN,WSYMX)

      RETURN
      END
