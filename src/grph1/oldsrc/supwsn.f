*-----------------------------------------------------------------------
*     SUPACK
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SUPWSN(IU)


      CALL MSGDMP('M','SUPWSN','THIS IS OLD INTERFACE - USE SGPWSN !')

      CALL GLIGET('MSGUNIT',IUX)
      CALL GLISET('MSGUNIT',IU)

      CALL SGPWSN

      CALL GLISET('MSGUNIT',IUX)

      END
