*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGSNTR(UXMIN, UXMAX, UYMIN, UYMAX,
     +                  VXMIN, VXMAX, VYMIN, VYMAX, ITR)


      CALL MSGDMP('M','SGSNTR','THIS IS OLD INTERFACE.')

      IF (.NOT.(1.LE.ITR .AND. ITR.LE.4))
     +     CALL MSGDMP('W', 'SGSNTR', 'INVALID TRANSFORMATION NUMBER.')

      CALL SGSWND(UXMIN, UXMAX, UYMIN, UYMAX)
      CALL SGSVPT(VXMIN, VXMAX, VYMIN, VYMAX)
      CALL SGSTRN(ITR)
      CALL SGSTRF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGQNTR(UXMIN, UXMAX, UYMIN, UYMAX,
     +             VXMIN, VXMAX, VYMIN, VYMAX, ITR)


      CALL MSGDMP('M','SGQNTR','THIS IS OLD INTERFACE.')

      CALL SGQTRN(ITR)
      IF (.NOT.(1.LE.ITR .AND. ITR.LE.4))
     +     CALL MSGDMP('W', 'SGQNTR', 'INVALID TRANSFORMATION NUMBER.')

      CALL SGQWND(UXMIN, UXMAX, UYMIN, UYMAX)
      CALL SGQVPT(VXMIN, VXMAX, VYMIN, VYMAX)

      RETURN
      END
