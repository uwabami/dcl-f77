*-----------------------------------------------------------------------
*     SLPACK
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SLPGET(CP,IPARA)

      CHARACTER CP*(*)


      CALL MSGDMP('M','SLPGET','THIS IS OLD INTERFACE - USE SGPGET !')

      CALL SGPGET(CP,IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SLPSET(CP,IPARA)

      CALL MSGDMP('M','SLPSET','THIS IS OLD INTERFACE - USE SGPSET !')

      CALL SGPSET(CP,IPARA)

      RETURN
      END
