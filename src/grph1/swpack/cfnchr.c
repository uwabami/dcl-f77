/*
 *      Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
 */
#include <string.h>

void cfnchr (char *ch1, char *ch2, int lch)
{
    int nc;
    char *nl;

    strncpy (ch1, ch2, lch);
    for (nc = lch; nc >= 1; --nc) {
	nl = ch1 + nc - 1;
	if (*nl != '\0' && *nl != ' ')
	    break;
    }
    *(nl + 1) = '\0';
}
