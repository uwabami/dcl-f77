*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SWCQNP(NCP)

      CHARACTER CP*(*), CVAL*(*)
      PARAMETER (NPARA = 17)
      LOGICAL   LCHREQ, LFIRST
      CHARACTER CPARAS(NPARA)*8
      CHARACTER CPARAL(NPARA)*40
      CHARACTER CX(NPARA)*80, CMSG*80

      EXTERNAL  LCHREQ, LENC

      SAVE

      DATA      CPARAS( 1)/'WSNAME01'/, CX( 1)/'DISP    '/
      DATA      CPARAS( 2)/'WSNAME02'/, CX( 2)/'FILES   '/
      DATA      CPARAS( 3)/'FNAME   '/, CX( 3)/'dcl     '/
      DATA      CPARAS( 4)/'TITLE   '/, CX( 4)/'*       '/
      DATA      CPARAS( 5)/'CLRMAP  '/, CX( 5)/'colormap'/
      DATA      CPARAS( 6)/'BITMAP  '/, CX( 6)/'bitmap  '/
      DATA      CPARAS( 7)/'CL2TN   '/, CX( 7)/'cl2tnmap'/
      DATA      CPARAS( 8)/'FONT1   '/, CX( 8)/'font1u  '/
      DATA      CPARAS( 9)/'FONT2   '/, CX( 9)/'font2u  '/
      DATA      CPARAS( 10)/'LPR     '/, CX(10)/'lpr     '/
      DATA      CPARAS( 11)/'CIMGFMT '/, CX(11)/'png     '/
      DATA      CPARAS( 12)/'CMAPLIST'/, CX(12)/'cmaplist'/
      DATA      CPARAS( 13)/'FLNAME01'/, CX(13)/'PNG     '/
      DATA      CPARAS( 14)/'FLNAME02'/, CX(14)/'EPS     '/
      DATA      CPARAS( 15)/'FLNAME03'/, CX(15)/'SVG     '/
      DATA      CPARAS( 16)/'FLNAME04'/, CX(16)/'PDF     '/
      DATA      CPARAS( 17)/'FONTNAME'/, CX(17)/'        '/


      DATA      CPARAL( 1)/'DEVICE_NAME01'/
      DATA      CPARAL( 2)/'DEVICE_NAME02'/
      DATA      CPARAL( 3)/'FRAME_NAME'/
      DATA      CPARAL( 4)/'TITLE'/
      DATA      CPARAL( 5)/'COLORMAP'/
      DATA      CPARAL( 6)/'BITMAP'/
      DATA      CPARAL( 7)/'COLOR_TO_TONE_MAP'/
      DATA      CPARAL( 8)/'FONT1'/
      DATA      CPARAL( 9)/'FONT2'/
      DATA      CPARAL(10)/'LPR'/
      DATA      CPARAL(11)/'IMAGE_FORMAT'/
      DATA      CPARAL(12)/'COLORMAP_LIST'/
      DATA      CPARAL(13)/'FILE_NAME01'/
      DATA      CPARAL(14)/'FILE_NAME02'/
      DATA      CPARAL(15)/'FILE_NAME03'/
      DATA      CPARAL(16)/'FILE_NAME04'/
      DATA      CPARAL(17)/'FONTNAME'/

      DATA      LFIRST / .TRUE. /


      NCP = NPARA

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWCQID(CP, IDX)

      DO 10 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IDX = N
          RETURN
        END IF
   10 CONTINUE
      CMSG = 'PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E', 'SWCQID', CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWCQCP(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAS(IDX)
      ELSE
        CALL MSGDMP('E', 'SWCQCP', 'IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWCQCL(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAL(IDX)
      ELSE
        CALL MSGDMP('E', 'SWCQCL', 'IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWCQVL(IDX, CVAL)

      IF (LFIRST) THEN
        CALL RTCGET('SW', CPARAS, CX, NPARA)
        CALL RLCGET(CPARAL, CX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CVAL = CX(IDX)
      ELSE
        CALL MSGDMP('E', 'SWCQVL', 'IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWCSVL(IDX, CVAL)

      IF (LFIRST) THEN
        CALL RTCGET('SW', CPARAS, CX, NPARA)
        CALL RLCGET(CPARAL, CX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CX(IDX) = CVAL
      ELSE
        CALL MSGDMP('E', 'SWCSVL', 'IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWCQIN(CP, IN)

      DO 20 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IN = N
          RETURN
        END IF
   20 CONTINUE

      IN = 0

      RETURN
      END
