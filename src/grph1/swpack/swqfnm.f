**-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SWQFNM(CPARA,CFNAME)

      CHARACTER CPARA*(*),CFNAME*(*)

      PARAMETER (MAXP=3,MAXF=3)

      LOGICAL   LCHREQ
      CHARACTER CPLIST(MAXP)*1024,CFLIST(MAXF)*1024,CPX*1024,
     +          CX11*4,CPSX*4,CMSG*1024

      EXTERNAL  LENC,LCHREQ

      CPLIST(1)=' '
      CALL GLCGET('DUPATH',CPLIST(2))
      CALL GLCGET('DSPATH',CPLIST(3))
      NP=3

      CALL SWCGET(CPARA,CPX)
      LCP=LENC(CPX)
      CALL SWIGET('IWS',IWS)
      CX11='.X11'
      CALL CLOWER(CX11)
      CPSX='.PSX'
      CALL CLOWER(CPSX)

      IF (      LCHREQ(CPARA,'FONT1')
     +     .OR. LCHREQ(CPARA,'FONT2')
     +     .OR. LCHREQ(CPARA,'CL2TN')
     +     .OR. LCHREQ(CPARA,'CMAPLIST')) THEN
        CFLIST(1)=CPX(1:LCP)
        NF=1
      ELSE IF ( LCHREQ(CPARA,'CLRMAP')
     +     .OR. LCHREQ(CPARA,'BITMAP')) THEN
        IF (IWS.EQ.2 .OR. IWS.EQ.3) THEN
          CFLIST(1)=CPX(1:LCP)//CPSX
          CFLIST(2)=CPX(1:LCP)
          CFLIST(3)=CPX(1:LCP)//CX11
        ELSE
          CFLIST(1)=CPX(1:LCP)//CX11
          CFLIST(2)=CPX(1:LCP)
          CFLIST(3)=CPX(1:LCP)//CPSX
        END IF
        NF=3
      ELSE
        CMSG='PARAMETER NAME <'//CPARA(1:LENC(CPARA))//'> IS INVALID.'
        CALL MSGDMP('E','SWQFNM',CMSG)
      END IF
      CALL CFSRCH(CPLIST,NP,CFLIST,NF,CFNAME)

      END
