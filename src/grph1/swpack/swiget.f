*-----------------------------------------------------------------------
*     SWIGET / SWISET / SWISTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SWIGET(CP, IPARA)

      CHARACTER CP*(*)

      CHARACTER CX*8
      CHARACTER CL*40


      CALL SWIQID(CP, IDX)
      CALL SWIQVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWISET(CP, IPARA)

      CALL SWIQID(CP, IDX)
      CALL SWISVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWISTX(CP, IPARA)

      IP = IPARA
      CALL SWIQID(CP, IDX)

*     / SHORT NAME /

      CALL SWIQCP(IDX, CX)
      CALL RTIGET('SW', CX, IP, 1)

*     / LONG NAME /

      CALL SWIQCL(IDX, CL)
      CALL RLIGET(CL, IP, 1)

      CALL SWISVL(IDX,IP)

      RETURN
      END
