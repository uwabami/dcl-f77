*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SWCGET(CP, CPARA)

      CHARACTER CP*(*), CPARA*(*)

      CHARACTER CX*40, CPVAL*80


      CALL SWCQID(CP, IDX)
      CALL SWCQVL(IDX, CPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWCSET(CP, CPARA)

      CALL SWCQID(CP, IDX)
      CALL SWCSVL(IDX, CPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWCSTX(CP, CPARA)

      CPVAL=CPARA

      CALL SWCQID(CP, IDX)

      CALL SWCQCP(IDX, CX)
      CALL RTCGET('SW', CX, CPVAL, 1)

      CALL SWCQCL(IDX, CX)
      CALL RLCGET(CX, CPVAL, 1)

      CALL SWCSVL(IDX, CPVAL)

      RETURN
      END
