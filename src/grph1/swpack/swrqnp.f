*-----------------------------------------------------------------------
*     REAL PARAMETER CONTROL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SWRQNP(NCP)

      CHARACTER CP*(*)

      PARAMETER (NPARA = 2)

      REAL      RX(NPARA)
      LOGICAL   LCHREQ, LFIRST
      CHARACTER CPARAS(NPARA)*8
      CHARACTER CPARAL(NPARA)*40
      CHARACTER CMSG*80

      EXTERNAL  LCHREQ,LENC

      SAVE

*     / SHORT NAME /

      DATA      CPARAS( 1) / 'RIMGCMP ' / , RX( 1) / -1.0 /
      DATA      CPARAS( 2) / 'RLWFACT ' / , RX( 2) / 1.0 /

*    / LONG NAME /

      DATA      CPARAL( 1) / 'IMAGE_COMPRESSION'/
      DATA      CPARAL( 2) / 'LINE_WIDTH_FACTOR'/

      DATA      LFIRST / .TRUE. /


      NCP = NPARA

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWRQID(CP, IDX)

      DO 10 N = 1, NPARA
         IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
            IDX = N
            RETURN
         END IF
 10   CONTINUE
      CMSG = 'PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E','SWRQID',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWRQCP(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
         CP = CPARAS(IDX)
      ELSE
         CALL MSGDMP('E','SWRQCP','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWRQCL(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
         CP = CPARAL(IDX)
      ELSE
         CALL MSGDMP('E','SWRQCL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWRQVL(IDX, RPARA)

      IF (LFIRST) THEN
         CALL RTRGET('SW', CPARAS, RX, NPARA)
         CALL RLRGET(CPARAL, RX, NPARA)
         LFIRST = .FALSE.
      END IF

      if (1.LE.IDX .AND. IDX.LE.NPARA) THEN
         RPARA = RX(IDX)
      ELSE
         CALL MSGDMP('E','SWRQVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWRSVL(IDX, RPARA)

      IF (LFIRST) THEN
         CALL RTRGET('SW', CPARAS, RX, NPARA)
         CALL RLRGET(CPARAL, RX, NPARA)
         LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
         RX(IDX) = RPARA
      ELSE
         CALL MSGDMP('E','SWRSVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWRQIN(CP, IN)

      DO 20 N = 1, NPARA
         IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
            IN = N
            RETURN
         END IF
 20   CONTINUE

      IN = 0

      RETURN
      END
