*-----------------------------------------------------------------------
*     COLORMAP LIST LOADING
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SWCMLL

      CHARACTER CTF*(*),CTD*(*)

      PARAMETER (NMAX=99)

      LOGICAL   LFIRST
      CHARACTER CTR1(NMAX)*80,CTR2(NMAX)*80,CFILE*1024,CMSG*80

      EXTERNAL  IUFOPN

      DATA      LFIRST/.TRUE./

      SAVE


      IF (.NOT.LFIRST) RETURN

*     / OPEN LIST FILE : MAYBE SYSTEM DEPENDENT /

      CALL SWQFNM('CMAPLIST',CFILE)
      IF (CFILE.EQ.' ') THEN
        CALL MSGDMP('E','SWCMLL','COLORMAP LIST DOES NOT EXIST.')
      END IF

      IU=IUFOPN()
      OPEN(IU,FILE=CFILE,FORM='FORMATTED')
      REWIND(IU)

*     / LOAD COLORMAP LIST /

      READ(IU,*,IOSTAT=IOS) NC
      IF (IOS.NE.0) THEN
        CALL MSGDMP('E','SWCMLL','COLORMAP LIST IS INVALID')
      END IF
      IF (NC.GT.NMAX) THEN
        CALL MSGDMP('E','SWCMLL','COLORMAP LIST OVERFLOWS')
      END IF

      DO 10 N=1,NC
        READ(IU,*,IOSTAT=IOS) CTR1(N),CTR2(N)
        IF (IOS.NE.0) THEN
          CALL MSGDMP('E','SWCMLL','COLORMAP LIST IS INVALID')
        END IF
   10 CONTINUE
      NNMAX=NC
      LFIRST=.FALSE.

*     / CLOSE TABLE FILE /

      CLOSE(IU)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWQCMN(NN)

      IF (LFIRST) THEN
        CALL MSGDMP('E','SWQCMN',
     +       'COLORMAP LIST FILE SHOULD BE READ FIRST USING "SWCMLL"')
      END IF

      NN=NNMAX

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWQCMF(NTX,CTF)

      IF (LFIRST) THEN
        CALL MSGDMP('E','SWQCMF',
     +       'COLORMAP LIST FILE SHOULD BE READ FIRST USING "SWCMLL"')
      END IF

      IF (1.LE.NTX .AND. NTX.LE.NNMAX) THEN
        CTF=CTR1(NTX)
      ELSE
        CMSG='COLORMAP NUMBER <##> IS OUT OF RANGE.'
        CALL CHNGI(CMSG,'##',NTX,'(I2)')
        CALL MSGDMP('E','SWQCMF',CMSG)
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWQCMD(NTX,CTD)

      IF (LFIRST) THEN
        CALL MSGDMP('E','SWQCMD',
     +       'COLORMAP LIST FILE SHOULD BE READ FIRST USING "SWCMLL"')
      END IF

      IF (1.LE.NTX .AND. NTX.LE.NNMAX) THEN
        CTD=CTR2(NTX)
      ELSE
        CMSG='COLORMAP NUMBER <##> IS OUT OF RANGE.'
        CALL CHNGI(CMSG,'##',NTX,'(I2)')
        CALL MSGDMP('E','SWQCMD',CMSG)
      END IF

      RETURN
      END
