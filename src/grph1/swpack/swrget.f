*-----------------------------------------------------------------------
*     SWRGET / SWRSET / SWRSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SWRGET(CP, RPARA)

      CHARACTER CP*(*)

      CHARACTER CX*8
      CHARACTER CL*40


      CALL SWRQID(CP, IDX)
      CALL SWRQVL(IDX, RPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWRSET(CP, RPARA)

      CALL SWRQID(CP, IDX)
      CALL SWRSVL(IDX, RPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWRSTX(CP, RPARA)

      RP = RPARA
      CALL SWRQID(CP, IDX)

*     / SHORT NAME /

      CALL SWRQCP(IDX, CX)
      CALL RTRGET('SW', CX, RP, 1)

*     / LONG NAME /

      CALL SWRQCL(IDX, CL)
      CALL RLRGET(CL, RP, 1)

      CALL SWRSVL(IDX,RP)

      RETURN
      END
