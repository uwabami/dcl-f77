*-----------------------------------------------------------------------
*     COMMON SETTING FOR LINE SEGMENT
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZSLTI(ITYPE,INDEX)

      COMMON    /SZBLS1/ LLNINT,LGCINT,RDXR,RDYR
      LOGICAL   LLNINT,LGCINT
      COMMON    /SZBLS2/ LCLIP
      LOGICAL   LCLIP
      COMMON    /SZBLS3/ LCHAZ
      LOGICAL   LCHAZ

      SAVE


      CALL SGLGET('LLNINT',LLNINT)
      CALL SGLGET('LGCINT',LGCINT)
      CALL SGRGET('RDX',RDX)
      CALL SGRGET('RDY',RDY)
      CALL SGLGET('LCLIP',LCLIP)
      CALL SGLGET('LCHAR',LCHAZ)
      CALL STFRAD(RDX, RDY, RDXR, RDYR)

      CALL SZSIDX(INDEX)
      CALL SZSTYP(ITYPE)

      END
