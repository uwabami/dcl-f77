*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZPLZR(N,RPX,RPY)

      REAL      RPX(*),RPY(*)

      LOGICAL   LFLAG,LCLIPZ

      COMMON    /SZBPL1/ LMISS,RMISS
      LOGICAL   LMISS
      COMMON    /SZBLS2/ LCLIP
      LOGICAL   LCLIP

      SAVE


      LCLIPZ=LCLIP
      LCLIP=.FALSE.
      CALL STEPR2

      CALL SZOPLV

      IF (.NOT.LMISS) THEN
        CALL SZMVLV(RPX(1),RPY(1))
        DO 10 I=2,N
          CALL SZPLLV(RPX(I),RPY(I))
   10   CONTINUE
      ELSE
        LFLAG=.FALSE.
        DO 20 I=1,N
          IF (RPX(I).EQ.RMISS .OR. RPY(I).EQ.RMISS) THEN
            LFLAG=.FALSE.
          ELSE IF (LFLAG) THEN
            CALL SZPLLV(RPX(I),RPY(I))
          ELSE
            CALL SZMVLV(RPX(I),RPY(I))
            LFLAG=.TRUE.
          END IF
   20   CONTINUE
      END IF

      CALL SZCLLV

      LCLIP=LCLIPZ
      CALL STRPR2

      END
