*-----------------------------------------------------------------------
*     TONE ROUTINE ON UC
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZOPTU

      LOGICAL   LFIRST, LCONT, LMAP

      SAVE


      CALL STQTRF(LMAP)

      IF (LMAP) THEN
        CALL SZOPTT
      ELSE
        CALL SZOPTV
      END IF

      LFIRST = .TRUE.

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZSTTU(UX,UY)

      CALL STFRAD(UX, UY, UX1, UY1)

      IF (LFIRST) THEN
        LFIRST = .FALSE.
        UX0 = UX1
        UY0 = UY1
      END IF

      CALL SZPIPL(UX0, UY0, UX1, UY1, 1)
   10 CONTINUE
        CALL SZGIPL(XX, YY, LCONT)
        IF (LMAP) THEN
          CALL STFROT(XX, YY, TX, TY)
          CALL SZSTTT(TX, TY)
        ELSE
          CALL STFTRN(XX, YY , VX, VY)
          CALL SZSTTV(VX, VY)
        END IF
      IF (LCONT) GO TO 10

      UX0 = UX1
      UY0 = UY1

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZCLTU

      IF (LMAP) THEN
        CALL SZCLTT
      ELSE
        CALL SZCLTV
      END IF

      RETURN
      END
