*-----------------------------------------------------------------------
*     TONE PATTERN ATTRIBUTE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZSTNI(ITPAT)

      PARAMETER (MAXCLI=100)

      INTEGER   ICPAT(MAXCLI)
      LOGICAL   LFRST,LTNATR,LCLATR,LSOFTF,LHARD,LCL2TN,LMONOF,LMSG
      CHARACTER CMSG*80

      SAVE

      DATA      LFRST/.TRUE./, LMSG/.TRUE./


*     / INITIALIZE /

      ITPATZ=ITPAT
      CALL SGIGET('IBGCLI',IBGCLI)
      CALL SGIGET('IFCIDX',IFCIDX)

      IF (LFRST) THEN

        CALL SWQTNC(LTNATR)
        CALL SWQCLC(LCLATR)

        CALL SGLGET('LCL2TN',LCL2TN)

        IF (LCL2TN) THEN
          LMONOF=.TRUE.
        ELSE
          IF (LCLATR) THEN
            LMONOF=.FALSE.
          ELSE
            LMONOF=.TRUE.
          END IF
        END IF

        IF (LMONOF) THEN
          CALL SZCLTN(ICPAT, MAXCLI)
          CALL MSGDMP('M','SZSTNI',
     +         'COLOR TO TONE CONVERSION TABLE IS USED.')
        END IF

        LFRST=.FALSE.

      END IF

*     / SOFT FILL OR HARD FILL (FORCE TO SOFT FILL) /

      CALL SGLGET('LSOFTF',LSOFTF)

      LHARD=.FALSE.
      IF (LSOFTF) THEN
      ELSE
        LSOFTF=.TRUE.
        LHARD=.FALSE.
        IF (LMSG) THEN
          CALL MSGDMP('W','SZSTNI','HARD FILL IS OBSOLETE.SOFT FILLED')
          LMSG=.FALSE.
        END IF
      END IF

*     / COLOR TO TONE CONVERSION /

      IF (LMONOF) THEN
        LP=MOD(ITPATZ,1000)
        LC=ITPATZ/1000
        IF (LP.EQ.999 .AND. LC.NE.IBGCLI) THEN
          IF (1.LE.LC .AND. LC.LE.MAXCLI) THEN
            IF (ICPAT(LC).GE.0) THEN
              ITPATZ=ICPAT(LC)
            ELSE
              CMSG='COLOR NUMBER ## IS NOT DEFINED IN CL2TNMAP.'
              CALL CHNGI(CMSG,'##',LC,'(I2)')
              CALL MSGDMP('W','SWGTON',CMSG)
            END IF
          ELSE IF(LC.EQ.999)THEN
          ELSE
            CMSG='COLOR NUMBER ## IS NOT DEFINED IN CL2TNMAP.'
            CALL CHNGI(CMSG,'##',LC,'(I2)')
            CALL MSGDMP('W','SWGTON',CMSG)
          END IF
        END IF
      END IF

      ITCLIY=ITPATZ/1000
      ITPNMY=MOD(ITPATZ,1000)
      IF (ITCLIY.EQ.IBGCLI) THEN
        ITPATY=ITPNMY
      ELSE
        IF (ITCLIY.EQ.0) THEN
          ITCLIY=1
        END IF
        ITPATY=ITPNMY+ITCLIY*1000
      END IF

      IF ((ITPNMY .EQ. IFCIDX).OR.(ITPNMY .EQ. 999)) THEN
        LHARD=.TRUE.
      END IF
      CALL SZSTMD(LHARD)

      IF (LHARD) THEN
        CALL SZTITZ(ITPATZ)
      ELSE
        CALL SZTITS(ITPATY)
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZQTNI(ITPAT)

      ITPAT=ITPATY

      RETURN
      END
