*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZPMZR(N,RPX,RPY)

      REAL      RPX(*),RPY(*)

      LOGICAL   LFLAG,LCLIPZ

      COMMON    /SZBPM1/ LMISS,RMISS,NPM
      LOGICAL   LMISS
      COMMON    /SZBPM2/ CMARK
      CHARACTER CMARK*1
      COMMON    /SZBTX3/ LCLIP
      LOGICAL   LCLIP

      SAVE


      LCLIPZ=LCLIP
      LCLIP=.FALSE.
      CALL STEPR2

      DO 10 I=1,N,NPM
        LFLAG=LMISS .AND. (RPX(I).EQ.RMISS .OR. RPY(I).EQ.RMISS)
        IF (.NOT.LFLAG) THEN
          CALL SZTXZV(RPX(I),RPY(I),CMARK)
        END IF
   10 CONTINUE

      LCLIP=LCLIPZ
      CALL STRPR2

      END
