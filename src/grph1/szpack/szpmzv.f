*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZPMZV(N,VPX,VPY)

      REAL      VPX(*),VPY(*)

      LOGICAL   LFLAG

      COMMON    /SZBPM1/ LMISS,RMISS,NPM
      LOGICAL   LMISS
      COMMON    /SZBPM2/ CMARK
      CHARACTER CMARK*1

      SAVE


      DO 10 I=1,N,NPM
        LFLAG=LMISS .AND. (VPX(I).EQ.RMISS .OR. VPY(I).EQ.RMISS)
        IF (.NOT.LFLAG) THEN
          CALL SZTXZV(VPX(I),VPY(I),CMARK)
        END IF
   10 CONTINUE

      END
