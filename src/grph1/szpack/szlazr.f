*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZLAZR(RX1,RY1,RX2,RY2)

      LOGICAL   LCLPLZ,LCLPTZ

      COMMON    /SZBLA1/ LARRWZ,LPROPZ,AFACTZ,CONSTZ,ANGLEZ,
     +                   LATONZ,LUARWZ,CONSMZ,RDUNIT
      LOGICAL   LARRWZ,LPROPZ,LATONZ,LUARWZ
      COMMON    /SZBLS2/ LCLIPL
      LOGICAL   LCLIPL
      COMMON    /SZBTX3/ LCLIPT
      LOGICAL   LCLIPT

      EXTERNAL  RFPI

      SAVE


      R=SQRT((RX2-RX1)**2+(RY2-RY1)**2)
      IF (R.EQ.0) RETURN

      PI=RFPI()

      LCLPLZ=LCLIPL
      LCLPTZ=LCLIPT
      LCLIPL=.FALSE.
      LCLIPT=.FALSE.
      CALL STEPR2

      CALL SZOPLV
      CALL SZMVLV(RX1,RY1)
      CALL SZPLLV(RX2,RY2)
      CALL SZCLLV

      IF (.NOT.LARRWZ) GO TO 100

      IF (LPROPZ) THEN
        AR=R*AFACTZ
      ELSE
        AR=CONSTZ
      END IF

      XE=(RX2-RX1)/R*AR
      YE=(RY2-RY1)/R*AR
      CALL CR2C(-(PI-ANGLEZ*RDUNIT),XE,YE,XA1,YA1)
      CALL CR2C(-(PI+ANGLEZ*RDUNIT),XE,YE,XA2,YA2)

      IF (LATONZ) THEN
        CALL SZOPTV
        CALL SZSTTV(RX2+XA1,RY2+YA1)
        CALL SZSTTV(RX2,RY2)
        CALL SZSTTV(RX2+XA2,RY2+YA2)
        CALL SZCLTV
      ELSE
        CALL SZOPLV
        CALL SZMVLV(RX2+XA1,RY2+YA1)
        CALL SZPLLV(RX2,RY2)
        CALL SZPLLV(RX2+XA2,RY2+YA2)
        CALL SZCLLV
      END IF

  100 CONTINUE

      LCLIPL=LCLPLZ
      LCLIPT=LCLPTZ
      CALL STRPR2

      END
