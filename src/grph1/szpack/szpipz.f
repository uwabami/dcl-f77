*-----------------------------------------------------------------------
*     INTERPOLATION FOR ARROW PRIMITIVE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZPIPZ(UXP0,UYP0,UXP1,UYP1,NN)

      PARAMETER (NMAX=100)

      REAL      XZ(NMAX),YZ(NMAX)
      LOGICAL   LFRST,LMAP,LCONTU,LCONTT

      EXTERNAL  RFPI

      SAVE

      DATA      LFRST/.TRUE./


      PI=RFPI()

      CALL STQTRF(LMAP)

      CALL STFRAD(UXP0,UYP0,UX0,UY0)
      CALL STFRAD(UXP1,UYP1,UX1,UY1)

      DXX=UX1-UX0
      IF (LMAP) THEN
        IF (DXX.GT.PI) THEN
          UX1=UX1-2*PI
        ELSE IF (DXX.LT.-PI) THEN
          UX1=UX1+2*PI
        END IF
      END IF

      XX1=UX0
      YY1=UY0
      NC=1
      XZ(1)=UX0
      YZ(1)=UY0
      CALL SZPIPL(UX0,UY0,UX1,UY1,2)
   10 CONTINUE
        XX0=XX1
        YY0=YY1
        CALL SZGIPL(XX1,YY1,LCONTU)
        CALL SZPIPT(XX0,YY0,XX1,YY1,2)
   20   CONTINUE
          NC=NC+1
          IF (NC.GT.NMAX) THEN
            IF (LFRST) THEN
              CALL MSGDMP('M','SGSIPU','WORKING AREA OVERFLOW.')
              LFRST=.FALSE.
            END IF
            XZ(NMAX-1)=XZ(NMAX)
            YZ(NMAX-1)=YZ(NMAX)
            NC=NMAX
          END IF
          CALL SZGIPT(XZ(NC),YZ(NC),LCONTT)
        IF (LCONTT) GO TO 20
      IF (LCONTU) GO TO 10

      NN=NC-1

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZGIPZ(UXG0,UYG0,UXG1,UYG1,N0)

      N1=N0+1
      CALL STIRAD(XZ(N0),YZ(N0),UXG0,UYG0)
      CALL STIRAD(XZ(N1),YZ(N1),UXG1,UYG1)

      RETURN
      END
