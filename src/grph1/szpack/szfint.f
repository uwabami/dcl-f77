*-----------------------------------------------------------------------
*     FONT LOADING
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZFINT(NCNT)

      INTEGER   JPOSX(*)
      REAL      WX1(*),WX2(*)
      CHARACTER CLX(*)*1,CLY(*)*1

      PARAMETER (NCHAR=256,LEN=6000)
      PARAMETER (MAXFS=2)
      PARAMETER (WUNIT=24,WUNIT2=WUNIT/2,PAD=0)

      INTEGER   IPOSX(NCHAR)
      REAL      VX1(NCHAR),VX2(NCHAR)
      LOGICAL   LPROP,LPROPZ
      CHARACTER CKX(LEN)*1,CKY(LEN)*1
      CHARACTER CPARA*8,CDSN*1024

      EXTERNAL  IUFOPN

      SAVE

      DATA      NCNTZ/0/
      DATA      IFONTZ/0/
      DATA      LPROPZ/.FALSE./


*     / CHECK FONT NUMBER & LFPROP OPTION /

      CALL SGIGET('IFONT',IFONT)
      CALL SGLGET('LFPROP',LPROP)

      IF (.NOT.(1.LE.IFONT .AND. IFONT.LE.MAXFS)) THEN
        CALL MSGDMP('E','SZINIT','FONT NUMBER IS OUT OF RANGE.')
      END IF

      IF ((IFONT.EQ.IFONTZ) .AND. (LPROPZ.EQV.LPROP)) THEN
        NCNT=NCNTZ
        RETURN
      ELSE
        NCNTZ=NCNTZ+1
        NCNT=NCNTZ
      END IF

*     / FONT LOADING /

      IF (IFONT.NE.IFONTZ) THEN

*       / INQUIRE FONT FILE NAMES /

        CPARA='FONT#'
        CALL CHNGI(CPARA,'#',IFONT,'(I1)')
        CALL SWQFNM(CPARA,CDSN)
        IF (CDSN.EQ.' ') THEN
          CALL MSGDMP('E','SZFONT','FONT FILE DOES NOT EXIST.')
        END IF

*       / OPEN FONT FILE : MAYBE SYSTEM DEPENDENT /

        IU=IUFOPN()
        OPEN(IU,FILE=CDSN,FORM='UNFORMATTED')
        REWIND(IU)

*       / LOAD TEXT FONT /

        READ(IU) IPOSX
        READ(IU) CKX
        READ(IU) CKY

*       / CLOSE FONT FILE /

        CLOSE(IU)

        IFONTZ=IFONT

      END IF

*     / FONT WIDTH /

      IF (LPROP) THEN

        DO 10 N=1,NCHAR
          IPS=IPOSX(N)
          VX1(N)=ICHAR(CKX(IPS))-64-PAD
          VX2(N)=ICHAR(CKY(IPS))-64+PAD
  10    CONTINUE

      ELSE

        CALL RSET0(VX1,NCHAR,1,-WUNIT2)
        CALL RSET0(VX2,NCHAR,1,+WUNIT2)

      END IF

      LPROPZ=LPROP

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZQFNT(JPOSX,CLX,CLY)

      IF (NCNTZ.EQ.0) THEN
        CALL MSGDMP('E','SZQFNT','FONT FILE HAS NOT BEEN LOADED.')
      END IF

      CALL VISET0(IPOSX,JPOSX,NCHAR,1,1)
      DO 20 N=1,LEN
        CLX(N)=CKX(N)
        CLY(N)=CKY(N)
   20 CONTINUE

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZQFNW(WX1,WX2)

      IF (NCNTZ.EQ.0) THEN
        CALL MSGDMP('E','SZQFNW','FONT FILE HAS NOT BEEN LOADED.')
      END IF

      CALL VRSET0(VX1,WX1,NCHAR,1,1)
      CALL VRSET0(VX2,WX2,NCHAR,1,1)

      RETURN
      END
