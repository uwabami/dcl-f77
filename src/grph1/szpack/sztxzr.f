*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZTXZR(RX,RY,CHARS)

      CHARACTER CHARS*(*)

      LOGICAL   LCLIPZ

      COMMON    /SZBTX3/ LCLIP
      LOGICAL   LCLIP

      SAVE


      CALL SGLGET('LCLIP',LCLIPZ)
      CALL SGLSET('LCLIP',.FALSE.)
      LCLIP=.FALSE.
      CALL STEPR2

      CALL SZTXWV(RX,RY,CHARS)
      CALL SGLSET('LCLIP',LCLIPZ)
      LCLIP=LCLIPZ
      CALL STRPR2

      END
