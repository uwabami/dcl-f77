*-----------------------------------------------------------------------
*     PLOT ROUTINE ON VC (PERSPECTIVE TRANSFORMATION)
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZOPLP

      CALL SZOPLR

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZMVLP(VX, VY)

      CALL STFPR2(VX, VY, RX, RY)
      CALL SZMVLR(RX, RY)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZPLLP(VX, VY)

      CALL STFPR2(VX, VY, RX, RY)
      CALL SZPLLR(RX, RY)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZCLLP

      CALL SZCLLR

      RETURN
      END
