*-----------------------------------------------------------------------
*     PLOT ROUTINE ON RC (SOLID LINE)
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZOPLZ

      CALL SWGOPN

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZMVLZ(RX, RY)

      CALL STFWTR(RX, RY, WX, WY)
      CALL SWGMOV(WX, WY)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZPLLZ(RX, RY)

      CALL STFWTR(RX, RY, WX, WY)
      CALL SWGPLT(WX, WY)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZCLLZ

      CALL SWGCLS

      RETURN
      END
