*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZT3ZU(UPX,UPY,UPZ)

      REAL      UPX(3), UPY(3), UPZ(3), RX(3), RY(3)

      COMMON    /SZBTN2/ IRMODE, IRMODR
      COMMON    /SZBTN4/ ITPT1, ITPT2

      SAVE

      DATA      ITPATZ /0/


      DO 10 I=1, 3
        CALL STFTR3 (UPX(I), UPY(I), UPZ(I), VX, VY, VZ)
        CALL STFPR3 (VX, VY, VZ, RX(I), RY(I))
   10 CONTINUE

      ROT = (RX(2)-RX(1))*(RY(3)-RY(1)) - (RX(3)-RX(1))*(RY(2)-RY(1))

      IF (ROT .GT. 0) THEN
        IRMODR = MOD(IRMODE, 2)
        ITPAT = ITPT1
      ELSE IF (ROT .LT. 0) THEN
        IRMODR = MOD(IRMODE+1, 2)
        ITPAT = ITPT2
      ELSE
        RETURN
      END IF

      IF (ITPAT.NE.ITPATZ) THEN
        CALL SZSTNI(ITPAT)
        ITPATZ = ITPAT
      END IF

      CALL SZOPTR
      DO 20 I=1,3
        CALL SZSTTR(RX(I),RY(I))
   20 CONTINUE
      CALL SZSTTR(RX(1),RY(1))
      CALL SZCLTR

      END
