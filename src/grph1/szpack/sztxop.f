*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZTXOP(RSIZE,IROTA,ICENT,INDEX)

      PARAMETER (WIDE=24)

      CHARACTER COBJ*80

      COMMON    /SZBTX1/ QSIZE,CT,ST,ICENTZ
      COMMON    /SZBTX2/ LCNTL,JSUP,JSUB,JRST,SMALL,SHIFT
      LOGICAL   LCNTL
      COMMON    /SZBTX3/ LCLIP
      LOGICAL   LCLIP

      SAVE


      CALL SGIGET('IFONT',IFONT)

      QSIZE=RSIZE/WIDE
      THETA=RD2R(REAL(IROTA))
      CT=QSIZE*COS(THETA)
      ST=QSIZE*SIN(THETA)

      ICENTZ=ICENT

      CALL SGLGET('LCNTL',LCNTL)
      CALL SGIGET('ISUP',ISUP)
      CALL SGIGET('ISUB',ISUB)
      CALL SGIGET('IRST',IRST)
      CALL SGRGET('SMALL',SMALL)
      CALL SGRGET('SHIFT',SHIFT)
      CALL SGLGET('LCLIP',LCLIP)
      CALL SGIGET('IWS',IWS)

      JSUP=ISUP+1
      JSUB=ISUB+1
      JRST=IRST+1

      CALL SZQIDX(INDEXZ)
      CALL SZSIDX(INDEX)

      WRITE(COBJ,'(F8.5,5I8)') RSIZE,IROTA,ICENT,INDEX,IFONT,IWS
*     WRITE(COBJ,'(F8.5,5I8)') RSIZE,IROTA,ICENT,INDEX,IWS
      CALL CDBLK(COBJ)
      CALL SWOOPN('SZTX',COBJ)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZTXCL

      CALL SZSIDX(INDEXZ)

      CALL SWOCLS('SZTX')

      RETURN
      END
