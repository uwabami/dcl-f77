*-----------------------------------------------------------------------
*     TONE ROUTINE ON RC (HARD FILL - BUFFERING)
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZOPTZ

      PARAMETER (NMAX = 16384)

      REAL      WX(NMAX), WY(NMAX)

      SAVE


      NN = 0

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZSTTZ(RX, RY)

      NN = NN +1
      IF (NN.GT.NMAX)
     +     CALL MSGDMP('E', 'SZSTTZ', 'WORKING AREA OVER FLOW')

      CALL STFWTR(RX, RY, WX(NN), WY(NN))

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZCLTZ

      CALL SWGTON(NN, WX, WY, ITPATZ)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZTITZ(ITPAT)

      ITPATZ = ITPAT

      RETURN
      END
