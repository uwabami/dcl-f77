*-----------------------------------------------------------------------
*     TONE PRIMITIVE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZTNOP(ITPAT)

      CHARACTER COBJ*80

      COMMON    /SZBLS1/ LLNINT,LGCINT,RDXR,RDYR
      LOGICAL   LLNINT,LGCINT
      COMMON    /SZBTN2/ IRMODE, IRMODR
      COMMON    /SZBTN3/ LCLIP
      LOGICAL   LCLIP

      SAVE


      CALL SGLGET('LLNINT',LLNINT)
      CALL SGLGET('LGCINT',LGCINT)
      CALL SGRGET('RDX',RDX)
      CALL SGRGET('RDY',RDY)
      CALL SGIGET('IRMODE',IRMODE)
      CALL SGLGET('LCLIP',LCLIP)

      CALL STFPR2(0., 0., RX0, RY0)
      CALL STFPR2(0., 1., RX1, RY1)
      CALL STFPR2(1., 0., RX2, RY2)

      ROT = (RX2-RX0)*(RY1-RY0) - (RY2-RY0)*(RX1-RX0)

      IRMODE = MOD(IRMODE, 2)
      IF (ROT.GT.0) THEN
        IRMODR = IRMODE
      ELSE
        IRMODR = MOD(IRMODE+1, 2)
      END IF

      CALL STFRAD(RDX, RDY, RDXR, RDYR)

      WRITE(COBJ,'(I8)') ITPAT
      CALL CDBLK(COBJ)
      CALL SWOOPN('SZTN',COBJ)

      CALL SZSTNI(ITPAT)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZTNCL

      CALL SWOCLS('SZTN')

      RETURN
      END
