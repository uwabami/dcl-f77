*-----------------------------------------------------------------------
*     POLYMARKER PRIMITIVE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZPMOP(ITYPE,INDEX,RSIZE)

      CHARACTER CSGI*1,COBJ*80

      COMMON    /SZBPM1/ LMISS,RMISS,NPM
      COMMON    /SZBPM2/ CMARK
      CHARACTER CMARK*1

      EXTERNAL  CSGI

      SAVE


      CALL GLLGET('LMISS',LMISS)
      CALL GLRGET('RMISS',RMISS)
      CALL SGRGET('PMFACT',PMF)
      CALL SGIGET('NPMSKIP',NPM)

      CMARK=CSGI(ITYPE)

      WRITE(COBJ,'(2I8,F8.5)') ITYPE,INDEX,RSIZE
      CALL CDBLK(COBJ)
      CALL SWOOPN('SZPM',COBJ)

      CALL SZTXOP(RSIZE*PMF,0,0,INDEX)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZPMCL

      CALL SZTXCL

      CALL SWOCLS('SZPM')

      RETURN
      END
