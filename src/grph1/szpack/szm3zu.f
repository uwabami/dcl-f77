*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZM3ZU(N,UPX,UPY,UPZ)

      REAL      UPX(*),UPY(*),UPZ(*)

      LOGICAL   LFLAG,LCLIPZ

      COMMON    /SZBPM1/ LMISS,RMISS,NPM
      LOGICAL   LMISS
      COMMON    /SZBPM2/ CMARK
      CHARACTER CMARK*1
      COMMON    /SZBTX3/ LCLIP
      LOGICAL   LCLIP

      SAVE


      LCLIPZ=LCLIP
      LCLIP=.FALSE.
      CALL STEPR2

      DO 10 I=1,N,NPM
        LFLAG=LMISS .AND. 
     +      (UPX(I).EQ.RMISS .OR. UPY(I).EQ.RMISS .OR. UPZ(I).EQ.RMISS)
        IF (.NOT.LFLAG) THEN
          CALL STFTR3(UPX(I), UPY(I), UPZ(I), VX, VY, VZ)
          CALL STFPR3(VX, VY, VZ, RX, RY)
          CALL SZTXZV(RX,RY,CMARK)
        END IF
   10 CONTINUE

      LCLIP=LCLIPZ
      CALL STRPR2

      END
