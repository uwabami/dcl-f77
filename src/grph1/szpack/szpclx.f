*-----------------------------------------------------------------------
*     CLIPPING ON TC - X
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZPCLX(TX0, TY0, TX1, TY1, LVALID, LBOUND)

      PARAMETER (EPSIL=1.E-5)

      REAL      XB(2)
      LOGICAL   LCONT, LMOVE, LPOLE, LBRDR, LINNR, LVALID,
     +          LREQA, LRNEA, LBOUND

      EXTERNAL  RFPI, LREQA, LRNEA

      SAVE

      LBRDR (X) = LREQA(XB(1), X, EPSIL) .OR. LREQA(X, XB(2), EPSIL)
      LINNR (X) = XB(1).LT. X  .AND. X .LT. XB(2)
      LPOLE (Y) = LREQA(ABS(Y), PI2, EPSIL)


      PI = RFPI()
      PI2 = PI/2

      PX0 = TX0
      PY0 = TY0
      PX1 = TX1
      PY1 = TY1

      DXX = SZXMOD(PX1 - PX0)
      PXX = PX0 + DXX

      IF ( LBRDR(PX0) .AND. LBOUND ) THEN
        IF ( LBRDR(PX1) ) THEN
          IF ( LRNEA(PXX, PX1, EPSIL)) THEN
            NMOD = 2
            NEND = 2
          ELSE
            NMOD = 3
            NEND = 3
          END IF
        ELSE IF ( LINNR(PX1) ) THEN
          NMOD = 3
          NEND = 3
          IF ( LRNEA(PXX, PX1, EPSIL) ) NMOD = 2
        ELSE
          LVALID = .FALSE.
          RETURN
        END IF
      ELSE IF ( LINNR(PX0) .AND. .NOT. LBRDR(PX0) ) THEN
        IF ( LBRDR(PX1) .AND. LBOUND) THEN
          NMOD = 1
          NEND = 1
          IF ( LRNEA(PXX, PX1, EPSIL) ) NEND = 2
        ELSE IF ( LINNR(PX1) .AND. .NOT.LBRDR(PX1) ) THEN
          NMOD = 3
          NEND = 3
          IF ( LRNEA(PXX, PX1, EPSIL) ) NMOD = 1
        ELSE
          NMOD = 1
          NEND = 1
        END IF
      ELSE
        IF (LBRDR(PX1) .AND. LBOUND ) THEN
          NMOD = 2
          NEND = 2
        ELSE IF ( LINNR(PX1) ) THEN
          NMOD = 2
          NEND = 3
        ELSE
          LVALID = .FALSE.
          RETURN
        END IF
      END IF
      LVALID = .TRUE.

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZGCLX(TX, TY, LCONT, LMOVE)

*     NMOD 1: PLOT TO BOUNDARY
*          2: MOVE TO BOUNDARY
*          3: PLOT TO (TX1, TY1)

      LMOVE = NMOD.EQ.2
      IF(NMOD.EQ.3 ) THEN
        TX = PX1
        TY = PY1
      ELSE IF(NMOD.EQ.1 .AND. LBRDR(PX1)) THEN
        TX = PXX
        TY = PY1
      ELSE
        IF (NMOD.EQ.1) THEN
          CXX = PX0+DXX/2
        ELSE IF (NMOD.EQ.2) THEN
          CXX = PX1-DXX/2
        END IF

        IF (ABS(CXX-XB(1)).LT.ABS(CXX-XB(2))) THEN
          TX = XB(1)
        ELSE
          TX = XB(2)
        END IF

        IF (LPOLE(PY1)) THEN
          TY = PY1
        ELSE IF (LPOLE(PY0)) THEN
          TY = PY0
        ELSE IF (LREQA(DXX, 0., EPSIL*10)) THEN
          TY = PY1
        ELSE IF (LREQA(ABS(DXX), PI, EPSIL*10)) THEN
          TY = SIGN(PI2, PY0+PY1)
        ELSE
          CALL SZSGCL(PX0, PY0, PX1, PY1)
          CALL SZQGCY(TX, TY)
        END IF
      END IF

      NMOD = NMOD+1
      LCONT = NMOD.LE.NEND

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZSCLX(XBND1, XBND2)

      XB(1) = XBND1
      XB(2) = XBND2

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZQCLX(XBND1, XBND2)

      XBND1 = XB(1)
      XBND2 = XB(2)

      RETURN
      END
