*-----------------------------------------------------------------------
*     LINE INDEX ATTRIBUTE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZSIDX(INDEX)

      LOGICAL   LFRST,LWDATR,LCLATR,LFCATR

      SAVE

      DATA      LFRST/.TRUE./


      INDEXZ=INDEX

      IF (LFRST) THEN
        CALL SWQWDC(LWDATR)
        CALL SWQCLC(LCLATR)
        LFRST=.FALSE.
      END IF

      CALL SGIGET('IBGCLI',IBGCLI)
      CALL SGIGET('IFCIDX',IFCIDX)

      IWDIDX=MOD(INDEXZ,10)
      ICLIDX=INDEXZ/10

      IF (ICLIDX.EQ.IBGCLI) THEN
        ICLIDX=0
      ELSE
        ICLIDX=MOD(ICLIDX,1000)
        IF (ICLIDX.EQ.0) ICLIDX=1
      END IF

      IF (.NOT.LCLATR .AND. IWDIDX.EQ.0) THEN
        IWDIDX=ICLIDX
      ELSE IF (.NOT.LWDATR .AND. ICLIDX.EQ.0) THEN
        ICLIDX=IWDIDX
      END IF

      CALL SWSWDI(IWDIDX)
      CALL SWQFCC(LFCATR)
      CALL SWSCLI(ICLIDX,.NOT.LFCATR)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZQIDX(INDEX)

      INDEX=INDEXZ

      RETURN
      END
