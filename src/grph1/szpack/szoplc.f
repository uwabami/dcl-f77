*-----------------------------------------------------------------------
*     PLOT ROUTINE ON VC (ENCODING TEXT)
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZOPLC

      CHARACTER CH*(*)

      PARAMETER (MAXTMP=400)

      REAL      VXT(MAXTMP),VYT(MAXTMP)
      LOGICAL   LCHAR,LCSET,LROT,LBUFF,LCURV,LCPUT
      CHARACTER CHZ*1024

      COMMON    /SZBLS3/ LCHAZ
      LOGICAL   LCHAZ

      EXTERNAL  LENC,RR2D

      SAVE

      DATA      LCSET/.FALSE./


*     / OPEN DASH LINE & CHARACTER SEGMENT /

      LCHAR=LCHAZ.AND.LCSET

      IF (LCHAR) THEN

        CALL SGLGET('LROT',LROT)
        CALL SGIGET('IROT',IROT)
        CALL SGRGET('FWC',FWC)
        CALL SGRGET('CWL',CWL)
        CALL SGRGET('FFCT',FFCT)
        CALL SGIGET('INDEXC',INDEX)
        CALL SGLGET('LBUFF',LBUFF)
        CALL SGIGET('NBUFF',NBUFF)
        CALL SGRGET('RBUFF',RBUFF)
        CALL SGLGET('LCURV',LCURV)
        CALL SGRGET('RCURV',RCURV)

        IF (.NOT.(FWC.GE.1.0)) THEN
          CALL MSGDMP('E','SZOPLC','PARAMETER ''FWC'' IS LESS THAN 1.')
        END IF
        IF (.NOT.(CWL.GT.0.0)) THEN
          CALL MSGDMP('E','SZOPLC','PARAMETER ''CWL'' IS LESS THAN 0.')
        END IF
        IF (.NOT.(0.0.LT.FFCT .AND. FFCT.LT.1.0)) THEN
          CALL MSGDMP('E','SZOPLC',
     +         'PARAMETER ''FFCT'' IS NOT IN THE RANGE OF (0,1).')
        END IF
        IF (.NOT.(1.LE.NBUFF .AND. NBUFF.LE.MAXTMP)) THEN
          CALL MSGDMP('E','SZOPLC',
     +         'PARAMETER ''NBUFF'' IS NOT IN THE RANGE OF (1,MAXTMP).')
        END IF
        IF (.NOT.(0.0.LT.RBUFF .AND. RBUFF.LT.1.0)) THEN
          CALL MSGDMP('E','SZOPLC',
     +         'PARAMETER ''RBUFF'' IS NOT IN THE RANGE OF (0,1).')
        END IF
        IF (.NOT.(0.0.LT.RCURV .AND. RCURV.LT.FWC)) THEN
          CALL MSGDMP('E','SZOPLC',
     +         'PARAMETER ''RCURV'' IS NOT IN THE RANGE OF (0,FWC).')
        END IF

        CALL SZQTXW(CHZ,NCHZ,WXCH,WYCH)

        FC=WXCH*FWC
        FL=CWL

        WC=HZ*FC
        WL=HZ*FL
        XW=WL+WC
        XI=WL*FFCT

      END IF

      IF (LBUFF) NT=0

      CALL SZOPLD

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZMVLC(VX,VY)

*     / PEN-UP MOVE /

      IF (LBUFF .AND. NT.NE.0) THEN
        DO 5 N=1,NT
          CALL SZPLLD(VXT(N),VYT(N))
    5   CONTINUE
        NT=0
      END IF

      CALL SZMVLD(VX,VY)

      VX0=VX
      VY0=VY
      XL0=XI

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZPLLC(VX,VY)

*     / PEN-DOWN MOVE /

      IF (.NOT.LCHAR) THEN

*       / THERE IS NO TEXT IN THE CYCLE /

        CALL SZPLLD(VX,VY)

      ELSE

*       / THERE EXISTS A TEXT IN THE CYCLE /

   10   CONTINUE

          R=SQRT((VX-VX0)**2+(VY-VY0)**2)
          IF (R.EQ.0) RETURN

          XL=XL0+R

          IF (XL.LT.WL) THEN

*           / CURRENET POSITION IS IN THE LINE PART /

            CALL SZPLLD(VX,VY)

            VX0=VX
            VY0=VY
            XL0=XL

          ELSE IF (XL.LT.XW) THEN

*           / CURRENT POSITION IS IN THE CHARCTER PART /

            IF (XL0.LT.WL) THEN

*             / LAST POSITION WAS IN THE LINE PART /

              VX1=VX0+(VX-VX0)*(WL-XL0)/R
              VY1=VY0+(VY-VY0)*(WL-XL0)/R

              CALL SZPLLD(VX1,VY1)

            END IF

            VX0=VX
            VY0=VY
            XL0=XL

            IF (LBUFF) THEN

*           / BUFFERRING /

              NT=NT+1
              VXT(NT)=VX
              VYT(NT)=VY

              IF (NT.EQ.NBUFF) THEN

                DO 15 N=1,NT
                  CALL SZPLLD(VXT(N),VYT(N))
   15           CONTINUE
                XL0=WL*RBUFF
                NT=0

              END IF

            END IF

          ELSE

*           / CURRENT POSITION IS IN THE NEXT CYCLE /

            IF (XL0.LT.WL) THEN

*             / LAST POSITION WAS IN THE LINE PART /

              VX1=VX0+(VX-VX0)*(WL-XL0)/R
              VY1=VY0+(VY-VY0)*(WL-XL0)/R

              CALL SZPLLD(VX1,VY1)

            END IF

*           / POSITION OF TEXT /

            VX0=VX0+(VX-VX0)*(XW-XL0)/R
            VY0=VY0+(VY-VY0)*(XW-XL0)/R
            VXC=(VX0+VX1)/2
            VYC=(VY0+VY1)/2

            LCPUT=.TRUE.

            IF (LBUFF.AND.LCURV) THEN

*             / CHECK CURVATURE (RUN PATH) /

              XLC=SQRT((VX1-VX0)**2+(VY1-VY0)**2)
              IF (XLC.LE.HZ*WXCH*RCURV) THEN
                DO 20 N=1,NT
                  CALL SZPLLD(VXT(N),VYT(N))
   20           CONTINUE
                XL0=WL*RBUFF
                NT=0
                CALL SZPLLD(VX,VY)
                VX0=VX
                VY0=VY
                LCPUT=.FALSE.
              END IF

            END IF

            IF (LCPUT) THEN

*             / ROTATION OPTION /

              IF (LROT) THEN
                ITH=IROT
              ELSE
                ITH=MOD(RR2D(ATAN2(VY0-VY1,VX0-VX1))+270,180.0)-90
              END IF

*             / WRITE TEXT /

              CALL SZQIDX(INDEXZ)
              IF (INDEX.EQ.0) THEN
                INDEXY=INDEXZ
              ELSE
                INDEXY=INDEX
              END IF

              CALL SZCLLD

*             CALL SGTXZV(VXC,VYC,CHZ,HZ,ITH,0,INDEXY)
              CALL SZTXOP(HZ,ITH,0,INDEXY)
              CALL SZTXZV(VXC,VYC,CHZ)
              CALL SZTXCL

              CALL SZSIDX(INDEXZ)

              CALL SZOPLD
              CALL SZMVLD(VX0,VY0)

              XL0=0
              NT=0

            END IF

          END IF

        IF (.NOT.(XL.LT.XW)) GO TO 10

      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZCLLC

*     / CLOSE DASHCHAR SEGMENT /

      IF (LBUFF .AND. NT.NE.0) THEN
        DO 25 N=1,NT
          CALL SZPLLD(VXT(N),VYT(N))
   25   CONTINUE
      END IF

      CALL SZCLLD

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZSCHZ(CH,H)

      NC  = LENC(CH)
      CHZ = CH(1:NC)
      HZ  = H
      LCSET = .TRUE.

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZQCHZ(CH,H)

      IF (.NOT.LCSET) THEN
        CALL MSGDMP('E','SZQCHZ','TEXT HAS NOT BEEN SET YET.')
      END IF

      CH = CHZ
      H  = HZ

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZCRST

      LCSET=.FALSE.

      RETURN
      END
