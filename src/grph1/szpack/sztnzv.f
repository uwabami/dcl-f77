*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZTNZV(N,VPX,VPY)

      REAL      VPX(*),VPY(*)


      CALL SZOPTV
      DO 10 I=1,N
        CALL SZSTTV(VPX(I),VPY(I))
   10 CONTINUE
      CALL SZSTTV(VPX(1),VPY(1))
      CALL SZCLTV

      END
