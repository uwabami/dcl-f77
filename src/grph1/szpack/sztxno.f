*-----------------------------------------------------------------------
*     CONTROL CHARACTER CONVERT TO OLD TYPE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZTXNO(CHARS,CHARZ,LCNTLZ)

      CHARACTER CHARS*(*)
      CHARACTER CHARZ*(*)

      PARAMETER (NCHARS=2048,LEN=6000)
      PARAMETER (WUNIT=24,ZUNIT=24)

      CHARACTER CHARX*(NCHARS)

      COMMON    /SZBTX1/ QSIZE,CT,ST,ICENTZ
      COMMON    /SZBTX2/ LCNTL,JSUP,JSUB,JRST,SMALL,SHIFT
      LOGICAL   LCNTL, LMARK
      INTEGER   ISUPZ,ISUBZ,IRSTZ
      INTEGER   ISUP,ISUB,IRST
      CHARACTER CSGI*1,CSUP*1,CSUB*1,CRST*1
      INTEGER   IMODE, IDEST, IOCMDE
      LOGICAL   LCNTLZ

      EXTERNAL  LENC,ISGC

      SAVE

      DATA      NCNTZ/0/

      LCNTLZ=LCNTL
      CALL SGIGET('ISUP',ISUP)
      CALL SGIGET('ISUB',ISUB)
      CALL SGIGET('IRST',IRST)
      ISUPZ=29
      ISUBZ=30
      IRSTZ=31
      CSUP=CSGI(ISUPZ)
      CSUB=CSGI(ISUBZ)
      CRST=CSGI(IRSTZ)

*     / Remove '\x' /
      DO 20 I=1,NCHARS
        CHARX(I:I)=' '
   20 CONTINUE
      NCS=LENC(CHARS)
      IDEST=1
      IMODE=0
      DO 10 I=1,NCS
        IF (CHARS(I:I+1).EQ.'\x')THEN
          IMODE=1
        ELSE IF(IMODE.EQ.1)THEN
          IMODE=0
        ELSE
          CHARX(IDEST:IDEST)=CHARS(I:I)
          IDEST=IDEST+1
        ENDIF
   10 CONTINUE

*     / CONVERT NEW TYPE MARKUPS TO OLD ONE /
      NCS=LENC(CHARX)
      NCHAR=LENC(CHARZ)
      DO 1 N=1,NCHAR
        CHARZ=' '
    1 CONTINUE
      NSKIP=0
      IDEST=1
      IMODE=0
      IOCMDE=0
      LMARK=.TRUE.
      DO 3 N=1,NCS
        IDX0=ISGC(CHARX(N:N))
        IF(IDX0.EQ.ISUPZ .OR. IDX0.EQ.ISUBZ .OR. IDX0.EQ.IRSTZ)THEN
          CHARX(N:N)=CSGI(32)
          IDX0=ISGC(CHARX(N:N))
        END IF
        IF(IMODE.EQ.1)THEN
          IF (IDX0.EQ.94)THEN
            LCNTLZ=.TRUE.
            CHARZ(IDEST:IDEST)=CSUP
            IDEST=IDEST+1
            IMODE=2
          ELSE IF (IDX0.EQ.95)THEN
            LCNTLZ=.TRUE.
            CHARZ(IDEST:IDEST)=CSUB
            IDEST=IDEST+1
            IMODE=2
          ELSE IF (IDX0.EQ.111)THEN
C           180: UP ARROW
            CHARZ(IDEST:IDEST)=CSGI(180)
            IDEST=IDEST+1
            CALL MSGDMP('W','SZTXWV',
     *        'TOPBAR IS NOT SUPPORT IN STROKEFONT')
            IMODE=2
            LMARK=.FALSE.
          ELSE IF (IDX0.EQ.117)THEN
C           182: DOWN ARROW
            CHARZ(IDEST:IDEST)=CSGI(182)
            IDEST=IDEST+1
            CALL MSGDMP('W','SZTXWV',
     *        'UNDERBAR IS NOT SUPPORT IN STROKEFONT')
            IMODE=2
            LMARK=.FALSE.
          ELSE IF(IDX0.EQ.92)THEN
            CHARZ(IDEST:IDEST)=CSGI(92)
            IDEST=IDEST+1
            IMODE=0
            LCNTLZ=.TRUE.
          ELSE IF(IDX0.EQ.120) THEN
            IMODE=0
          ELSE
            IMODE=0
            CHARZ(IDEST:IDEST)=CHARX(N:N)
            IDEST=IDEST+1
            IF(LMARK) THEN
              CHARZ(IDEST:IDEST)=CRST
              IDEST=IDEST+1
            ELSE
              LMARK=.TRUE.
            END IF
          END IF
        ELSE IF (IMODE.EQ.2) THEN
          IF(IDX0.EQ.123) THEN
            IMODE=3
          ELSE
            IMODE=0
            CHARZ(IDEST:IDEST)=CHARX(N:N)
            IDEST=IDEST+1
            CHARZ(IDEST:IDEST)=CRST
            IDEST=IDEST+1
          ENDIF
        ELSE IF(IMODE.EQ.3)THEN
          IF(ISGC(CHARX(N:N)).EQ.125)THEN
            IMODE=0
            IF(LMARK)THEN
              CHARZ(IDEST:IDEST)=CRST
              IDEST=IDEST+1
            ELSE
              LMARK=.TRUE.
            END IF
          ELSE
            CHARZ(IDEST:IDEST)=CHARX(N:N)
            IDEST=IDEST+1
          END IF
*       92:\ 94:^ 95:_ 111:o 117:u 123:{ 125:} 120:x
        ELSE IF (IDX0.EQ.92) THEN
            IMODE=1
*OLD TYPE CONTROL
        ELSE IF (IDX0.EQ.ISUP .AND. LCNTL) THEN
          CHARZ(IDEST:IDEST)=CSGI(ISUP)
          IOCMDE=IDEST
          IDEST=IDEST+1
        ELSE IF (IDX0.EQ.ISUB .AND. LCNTL) THEN
          CHARZ(IDEST:IDEST)=CSGI(ISUB)
          IOCMDE=-IDEST
          IDEST=IDEST+1
        ELSE IF (IDX0.EQ.IRST .AND. LCNTL) THEN
          IF(IOCMDE.GT.0)THEN
            CHARZ(IOCMDE:IOCMDE)=CSUP
            CHARZ(IDEST:IDEST)=CRST
            IDEST=IDEST+1
            IOCMOD=0
          ELSE IF(IOCMDE.LT.0)THEN
            CHARZ(-IOCMDE:-IOCMDE)=CSUB
            CHARZ(IDEST:IDEST)=CRST
            IDEST=IDEST+1
            IOCMOD=0
          ELSE
            CHARZ(IDEST:IDEST)=CSGI(IRST)
            IOCMDE=0
            IDEST=IDEST+1
          END IF
        ELSE
          CHARZ(IDEST:IDEST)=CHARX(N:N)
          IDEST=IDEST+1
        END IF
    3 CONTINUE

      IF (IDEST.LE.NCS)THEN
        NCS=IDEST-1
      END IF
*  / END CONVERT MARKUPS /
      LCNTLZ=.TRUE.
      END
