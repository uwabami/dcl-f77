*-----------------------------------------------------------------------
*     TEXT WIDTH AND HEIGHT INQUIRY
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZQTXW(CHARS,NC,WXCH,WYCH)

      CHARACTER CHARS*(*)

      PARAMETER (NWX=256)
      PARAMETER (WUNIT=24)
      PARAMETER (NCHARZ=2048)

      REAL      WX1(NWX),WX2(NWX)
      LOGICAL   LCNTL, LSYSFNT, LFPROP, LMRKFNT,LCNTLZ
      CHARACTER CSGI*1,CSUP*1,CSUB*1,CRST*1
      CHARACTER FONTNAME*70
      REAL      SMALL, SHIFT
      INTEGER   ISUBZ, ISUPZ, IRSTZ, IFONT, INDEX
      CHARACTER CHARZ*(NCHARZ)

      EXTERNAL  LENC,INDXCF,NINDXC,CSGI,ISGC

      SAVE

      DATA      NCNTZ/0/


*     / GET FONT INFORMATION /

      CALL SZFINT(NCNT)
      IF (NCNTZ.NE.NCNT) THEN
        CALL SZQFNW(WX1,WX2)
        NCNTZ=NCNT
      END IF

      CALL SZTXNO(CHARS,CHARZ,LCNTLZ)
      NCHZ=LENC(CHARZ)

*     / FOR SYSTEM FONTS /
*     / PROCCESS IF NOT MARKER /
      CALL SWLGET("LSYSFNT", LSYSFNT)
      IF(LSYSFNT) THEN
      CALL SZQIDX(INDEX)
      CALL SWCGET("FONTNAME",FONTNAME)
*     / WHEN FONTNAME IS NOT GIVEN /
      IF (LENZ(FONTNAME) == 0) THEN
        CALL SGLGET('LFPROP', LFPROP)
        IF (LFPROP) THEN
          CALL SGIGET('IFONT', IFONT)
          IF(IFONT.EQ.1)THEN
            FONTNAME = 'Sans'
          ELSE IF (IFONT.EQ.2)THEN
            FONTNAME = 'Serif'
          END IF
        ELSE
          FONTNAME = 'Monospace'
        END IF
        CALL SWFTFC(FONTNAME)
        CALL SWSFW(MAX(MOD(INDEX,10),1))
      ELSE
        CALL SWFTFC(FONTNAME)
      END IF

      IDX=ISGC(CHARZ(1:1))
      CALL SGLGET('LMRKFNT',LMRKFNT)
      IF (IDX.GT.18 .OR. LMRKFNT) THEN
        IF (IDX.LT.18) THEN
          CALL SWSFW(MAX(MOD(INDEX,10),1))
        END IF
        CALL SWQTXW(CHARZ,NCHZ,WXCH,WYCH)
        GO TO 40
      END IF
      END IF


*     / CHECK LCNTL OPTION /

      CALL SGLGET('LCNTL',LCNTL)
      LCNTLZ=.TRUE.

      IF (LCNTLZ) THEN

*       / CONTROL CHARACTERS ARE EFFECTIVE /

*        CALL SGIGET('ISUP',ISUP)
*        CALL SGIGET('ISUB',ISUB)
*        CALL SGIGET('IRST',IRST)
        CALL SGRGET('SMALL',SMALL)
        CALL SGRGET('SHIFT',SHIFT)

        ISUPZ=29
        ISUBZ=30
        IRSTZ=31
        CSUP=CSGI(ISUPZ)
        CSUB=CSGI(ISUBZ)
        CRST=CSGI(IRSTZ)

        NC=0
        NCTL=0
        NSUP=0
        NSUB=0
        ICHK=0

        MSUP=NINDXC(CHARZ,NCHZ,1,CSUP)
        MSUB=NINDXC(CHARZ,NCHZ,1,CSUB)
        MRST=NINDXC(CHARZ,NCHZ,1,CRST)
        IF (MRST.NE.(MSUP+MSUB)) THEN
          CALL MSGDMP('E','SZQTXW',
     +      'CONTROL CHARACTERS ARE NOT IN GOOD AGREEMENT.')
        END IF

   10   CONTINUE
          ID1=ICHK+1
          ID2=NCHZ
          NCHAR=ID2-ID1+1
          IDXRST=INDXCF(CHARZ(ID1:ID2),NCHAR,1,CRST)
          IF (IDXRST.GT.0) THEN
            ID3=ID1+IDXRST-1
            NDXSUP=NINDXC(CHARZ(ID1:ID3),IDXRST,1,CSUP)
            NDXSUB=NINDXC(CHARZ(ID1:ID3),IDXRST,1,CSUB)
            IF (NDXSUP.EQ.1 .NEQV. NDXSUB.EQ.1) THEN
              NCTL=NCTL+1
              IF (NDXSUP.EQ.1) THEN
                IDXSUP=INDXCF(CHARZ(ID1:ID3),IDXRST,1,CSUP)
                NCH=IDXRST-IDXSUP-1
                IF (NCH.GE.1) THEN
                  NSUP=NSUP+NCH
                ELSE
                  CALL MSGDMP('E','SZQTXW','NO VALID SUP CHARACTER.')
                END IF
              ELSE IF (NDXSUB.EQ.1) THEN
                IDXSUB=INDXCF(CHARZ(ID1:ID3),IDXRST,1,CSUB)
                NCH=IDXRST-IDXSUB-1
                IF (NCH.GE.1) THEN
                  NSUB=NSUB+NCH
                ELSE
                  CALL MSGDMP('E','SZQTXW','NO VALID SUB CHARACTER.')
                END IF
              END IF
              ICHK=ICHK+IDXRST
            ELSE
              CALL MSGDMP('E','SZQTXW',
     +          'CONTROL CHARACTERS ARE NOT IN GOOD AGREEMENT.')
            END IF
          END IF
        IF (.NOT.(IDXRST.EQ.0 .OR. ICHK.EQ.NCHZ)) GO TO 10

        DZ=MAX(0.5,SHIFT+SMALL/2)-0.5

        RFACT=1
        WXCH=0
        DO 20 N=1,NCHZ
          ICH=ISGC(CHARZ(N:N))
          IDX=ICH+1
          IF (ICH.EQ.ISUPZ .OR. ICH.EQ.ISUBZ) THEN
            RFACT=SMALL
            GO TO 20
          ELSE IF (ICH.EQ.IRSTZ) THEN
            RFACT=1
            GO TO 20
          END IF
          WXCH=WXCH+(WX2(IDX)-WX1(IDX))/WUNIT*RFACT
  20    CONTINUE

        WYCH=1
        IF (NSUP.GE.1) THEN
          WYCH=WYCH+DZ
        END IF
        IF (NSUB.GE.1) THEN
          WYCH=WYCH+DZ
        END IF

      ELSE

*       / CONTROL CHARACTERS ARE NOT EFFECTIVE /

        WXCH=0
        DO 30 N=1,NCHZ
          IDX=ISGC(CHARZ(N:N))+1
          WXCH=WXCH+(WX2(IDX)-WX1(IDX))/WUNIT
  30    CONTINUE
        WYCH=1

      END IF

  40  CONTINUE

      END
