*-----------------------------------------------------------------------
*     PLOT ROUTINE ON VC (CLIPPING)
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZOPLV

      LOGICAL   LVALID, LCONT, LMOVE

      COMMON    /SZBLS2/ LCLIP
      LOGICAL   LCLIP

      SAVE


      CALL SZOPLC

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZMVLV(VX, VY)

      IF (LCLIP) THEN
        CALL SZPCLL(VX, VY, VX, VY, LVALID, 1)
        IF (LVALID) THEN
          CALL SZMVLC(VX, VY)
        END IF
        VX0=VX
        VY0=VY
      ELSE
        CALL SZMVLC(VX, VY)
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZPLLV(VX, VY)

      IF (LCLIP) THEN
        CALL SZPCLL(VX0, VY0, VX, VY, LVALID, 1)
        IF (LVALID) THEN
   10     CONTINUE
            CALL SZGCLL(XX, YY, LCONT, LMOVE, 1)
            IF (LMOVE) THEN
              CALL SZMVLC(XX, YY)
            ELSE
              CALL SZPLLC(XX, YY)
            END IF
          IF (LCONT) GO TO 10
        END IF
        VX0=VX
        VY0=VY
      ELSE
        CALL SZPLLC(VX, VY)
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZCLLV

      CALL SZCLLC

      RETURN
      END
