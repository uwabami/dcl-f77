*-----------------------------------------------------------------------
*     PLOT ROUTINE ON UC (LINEAR INTERPOLATION)
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZOPLU

      LOGICAL   LCONT, LMAP

      SAVE


      CALL STQTRF(LMAP)

      IF (LMAP) THEN
        CALL SZOPLT
      ELSE
        CALL SZOPLV
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZMVLU(UX, UY)

      CALL STFRAD(UX, UY, UX1, UY1)
      IF (LMAP) THEN
        CALL STFROT(UX1, UY1, TX, TY)
        CALL SZMVLT(TX, TY)
      ELSE
        CALL STFTRN(UX1, UY1, VX, VY)
        CALL SZMVLV(VX, VY)
      END IF

      UX0 = UX1
      UY0 = UY1

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZPLLU(UX, UY)

      CALL STFRAD(UX, UY, UX1, UY1)

      CALL SZPIPL(UX0, UY0, UX1, UY1, 0)
   10 CONTINUE
        CALL SZGIPL(XX, YY, LCONT)
        IF (LMAP) THEN
          CALL STFROT(XX, YY, TX, TY)
          CALL SZPLLT(TX, TY)
        ELSE
          CALL STFTRN(XX, YY, VX, VY)
          CALL SZPLLV(VX, VY)
        END IF
      IF (LCONT) GO TO 10

      UX0 = UX1
      UY0 = UY1

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZCLLU

      IF (LMAP) THEN
        CALL SZCLLT
      ELSE
        CALL SZCLLV
      END IF

      RETURN
      END
