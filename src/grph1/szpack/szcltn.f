*-----------------------------------------------------------------------
*     COLOR TO TONE CONVERSION TABLE LOADING
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZCLTN(ICPAT,MAXCLI)

      INTEGER   ICPAT(*)

      CHARACTER CFILE*1024

      EXTERNAL  IUFOPN

      SAVE


*     / INITIALIZE /

      DO 10 I=1,MAXCLI
        ICPAT(I)=-1
   10 CONTINUE

*     / OPEN TABLE FILE : MAYBE SYSTEM DEPENDENT /

      CALL SWQFNM('CL2TN',CFILE)
      IF (CFILE.EQ.' ') THEN
        CALL MSGDMP('E','SZCLTN',
     +       'COLOR TO TONE CONVERSION TABLE DOES NOT EXIST.')
      END IF

      IU=IUFOPN()
      OPEN(IU,FILE=CFILE,FORM='FORMATTED')
      REWIND(IU)

*     / LOAD CONVERSION TABLE /

   20 CONTINUE
        READ(IU,*,IOSTAT=IOS) ICI,IPAT
        IF (ICI.GT.MAXCLI .OR. ICI.LE.0) THEN
          CALL MSGDMP('E','SZCLTN','COLOR INDEX IS INVALID')
        END IF
        ICPAT(ICI)=IPAT
      IF (IOS.EQ.0) GO TO 20

*     / CLOSE TABLE FILE /

      CLOSE(IU)

      END
