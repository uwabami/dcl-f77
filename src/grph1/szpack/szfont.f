*-----------------------------------------------------------------------
*     FONT LOADING
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZFONT(IFONT,IPOSX,NCHAR,CKX,CKY,LEN)

      INTEGER   IPOSX(NCHAR)
      CHARACTER CKX(LEN)*1,CKY(LEN)*1

      PARAMETER (MAXFS=2)

      CHARACTER CPARA*8,CDSN*1024

      EXTERNAL  IUFOPN

      SAVE


*     / CHECK FONT NUMBER /

      IF (.NOT.(1.LE.IFONT .AND. IFONT.LE.MAXFS)) THEN
        CALL MSGDMP('E','SZFONT','FONT NUMBER IS OUT OF RANGE.')
      END IF

*     / INQUIRE FONT FILE NAMES /

      CPARA='FONT#'
      CALL CHNGI(CPARA,'#',IFONT,'(I1)')
      CALL SWQFNM(CPARA,CDSN)
      IF (CDSN.EQ.' ') THEN
        CALL MSGDMP('E','SZFONT','FONT FILE DOES NOT EXIST.')
      END IF

*     / OPEN FONT FILE : MAYBE SYSTEM DEPENDENT /

      IU=IUFOPN()
      OPEN(IU,FILE=CDSN,FORM='UNFORMATTED')
      REWIND(IU)

*     / LOAD TEXT FONT /

      READ(IU) IPOSX
      READ(IU) CKX
      READ(IU) CKY

*     / CLOSE FONT FILE /

      CLOSE(IU)

      END
