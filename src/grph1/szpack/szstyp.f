*-----------------------------------------------------------------------
*     LINE TYPE ATTRIBUTE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZSTYP(ITYPE)

      PARAMETER (MAXTYP=4)

      INTEGER   IPAT(MAXTYP)
      LOGICAL   LFRST
      CHARACTER CPAT(MAXTYP)*32

      SAVE

      DATA      CPAT(1)/'11111111111111111111111111111111'/
      DATA      CPAT(2)/'11111111111100001111111111110000'/
      DATA      CPAT(3)/'11001100110011001100110011001100'/
      DATA      CPAT(4)/'11111111100010001111111110001000'/

      DATA      IDASHZ/0/,LFRST/.TRUE./


      IF (LFRST) THEN
        DO 10 N=1,MAXTYP
          CALL BITPCI(CPAT(N),IPAT(N))
   10   CONTINUE
        LFRST=.FALSE.
      END IF

      ITYPEZ=ITYPE
      IF (1.LE.ITYPEZ .AND. ITYPEZ.LE.MAXTYP) THEN
        IDASHZ=IPAT(ITYPEZ)
      ELSE
        IDASHZ=ITYPEZ
      END IF

      CALL SZSTYZ(IDASHZ)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZQTYP(ITYPE)

      ITYPE=ITYPEZ

      RETURN
      END
