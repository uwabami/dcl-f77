*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      REAL FUNCTION SZXMOD(X)

      EXTERNAL  RFPI


      PI = RFPI()
      IF (X .GT. PI) THEN
        SZXMOD = X - 2*PI
      ELSE IF (X.LE. -PI) THEN
        SZXMOD = X + 2*PI
      ELSE
        SZXMOD = X
      END IF

      END
