*-----------------------------------------------------------------------
*     PLOT ROUTINE ON TC (GREAT CIRCLE INTERPOLATION)
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZOPLT

      PARAMETER (EPSIL = 1.E-5)

      LOGICAL   LFIRST, LCONT1, LCONT2, LCONT3,
     +          LMOVEX, LVLDX, LVLDY, LREQA

      EXTERNAL  XMPLON, LREQA

      SAVE


      CALL SZOPLV

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZMVLT(TX, TY)

      TX0 = XMPLON(TX)
      TY0 = TY
      LFIRST = .TRUE.

      CALL SZPCLY(TX0, TY0, TX0, TY0, LVLDY, .TRUE.)
      CALL SZPCLX(TX0, TY0, TX0, TY0, LVLDX, .TRUE.)
      IF (LVLDX .AND. LVLDY) THEN
        CALL STFTRN(TX0, TY0, VX, VY)
        CALL SZMVLV(VX, VY)
        LFIRST = .FALSE.
      END IF

      BX0 = TX0
      BY0 = TY0
      CX0 = TX0
      CY0 = TY0

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZPLLT(TX, TY)

      TX1 = XMPLON(TX)
      TY1 = TY
      IF (.NOT. LFIRST .AND.
     +     LREQA(TX1, TX0, EPSIL) .AND. LREQA(TY1, TY0, EPSIL)) RETURN

      CALL SZPIPT(TX0, TY0, TX1, TY1, 0)
   10 CONTINUE
        CALL SZGIPT(BX1, BY1, LCONT1)
        CALL SZPCLY(BX0, BY0, BX1, BY1, LVLDY, .TRUE.)
        IF (LVLDY) THEN
   20     CONTINUE
            CALL SZGCLY(CX1, CY1, LCONT2)
            CX1 = XMPLON(CX1)
            CALL SZPCLX(CX0, CY0, CX1, CY1, LVLDX, .TRUE.)
            IF (LVLDX) THEN
   30         CONTINUE
                CALL SZGCLX(XX, YY, LCONT3, LMOVEX)
                CALL STFTRN(XX, YY, VX, VY)
                IF (LMOVEX.OR.LCONT2.OR.LFIRST) THEN
                  CALL SZMVLV(VX, VY)
                  LFIRST = .FALSE.
                ELSE
                  CALL SZPLLV(VX, VY)
                END IF
              IF (LCONT3) GO TO 30
            END IF
            CX0 = CX1
            CY0 = CY1
          IF (LCONT2) GO TO 20
        END IF
        BX0 = BX1
        BY0 = BY1
      IF (LCONT1) GO TO 10

      TX0=TX1
      TY0=TY1

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZCLLT

      CALL SZCLLV

      RETURN
      END
