*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZPLZV(N,VPX,VPY)

      REAL      VPX(*),VPY(*)

      LOGICAL   LFLAG

      COMMON    /SZBPL1/ LMISS,RMISS
      LOGICAL   LMISS

      SAVE


      CALL SZOPLV

      IF (.NOT.LMISS) THEN
        CALL SZMVLV(VPX(1),VPY(1))
        DO 10 I=2,N
          CALL SZPLLV(VPX(I),VPY(I))
   10   CONTINUE
      ELSE
        LFLAG=.FALSE.
        DO 20 I=1,N
          IF (VPX(I).EQ.RMISS .OR. VPY(I).EQ.RMISS) THEN
            LFLAG=.FALSE.
          ELSE IF (LFLAG) THEN
            CALL SZPLLV(VPX(I),VPY(I))
          ELSE
            CALL SZMVLV(VPX(I),VPY(I))
            LFLAG=.TRUE.
          END IF
   20   CONTINUE
      END IF

      CALL SZCLLV

      END
