*-----------------------------------------------------------------------
*     PLOT ROUTINE ON RC (CLIPPING)
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZOPLR

      LOGICAL   LVALID, LCONT, LMOVE

      SAVE


      CALL SZOPLZ

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZMVLR(RX, RY)

      CALL SZPCLL(RX, RY, RX, RY, LVALID, 0)
      IF (LVALID) THEN
        CALL SZMVLZ(RX, RY)
      END IF

      RX0=RX
      RY0=RY

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZPLLR(RX, RY)

      CALL SZPCLL(RX0, RY0, RX, RY, LVALID, 0)
      IF (LVALID) THEN
   10   CONTINUE
          CALL SZGCLL(XX, YY, LCONT, LMOVE, 0)
          IF (LMOVE) THEN
            CALL SZMVLZ(XX, YY)
          ELSE
            CALL SZPLLZ(XX, YY)
          END IF
        IF (LCONT) GO TO 10
      END IF

      RX0=RX
      RY0=RY

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZCLLR

      CALL SZCLLZ

      RETURN
      END
