*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZLAZU(UX1,UY1,UX2,UY2)

      LOGICAL   LMAP

      COMMON    /SZBLA1/ LARRWZ,LPROPZ,AFACTZ,CONSTZ,ANGLEZ,
     +                   LATONZ,LUARWZ,CONSMZ,RDUNIT
      LOGICAL   LARRWZ,LPROPZ,LATONZ,LUARWZ

      EXTERNAL  RFPI

      SAVE


      IF (UX1.EQ.UX2 .AND. UY1.EQ.UY2) RETURN

      CALL SZOPLU
      CALL SZMVLU(UX1,UY1)
      CALL SZPLLU(UX2,UY2)
      CALL SZCLLU

      IF (.NOT.LARRWZ) RETURN

      PI=RFPI()

      CALL STQTRF(LMAP)

      CALL SZPIPZ(UX1,UY1,UX2,UY2,NN)
      IF (LMAP) THEN
        CALL STFRAD(UX1,PI/RDUNIT/2-UY1,PH1,TH1)
        CALL STFRAD(UX2,PI/RDUNIT/2-UY2,PH2,TH2)
        CALL CR3S(TH2,PH2,0.0,TH1,PH1,THX,PHX)
        RV=ABS(THX)
        CALL SZGIPZ(RX1,RY1,RX2,RY2,NN)
        CALL STFTRF(RX1,RY1,VX1,VY1)
        CALL STFTRF(RX2,RY2,VX2,VY2)
      ELSE
        RV=0
        DO 10 N=1,NN
          CALL SZGIPZ(RX1,RY1,RX2,RY2,N)
          CALL STFTRF(RX1,RY1,VX1,VY1)
          CALL STFTRF(RX2,RY2,VX2,VY2)
          RV=RV+SQRT((VX2-VX1)**2+(VY2-VY1)**2)
   10   CONTINUE
      END IF

      IF (LPROPZ.AND.LUARWZ) THEN
        AR=RV*AFACTZ
      ELSE
        IF (LMAP.AND.LUARWZ) THEN
          AR=CONSMZ*RDUNIT
        ELSE
          AR=CONSTZ
        END IF
      END IF

      IF (LMAP.AND.LUARWZ) THEN

        CALL STFRAD(RX1,PI/RDUNIT/2-RY1,PH1,TH1)
        CALL STFRAD(RX2,PI/RDUNIT/2-RY2,PH2,TH2)
        CALL CR3S(TH2,PH2,0.0,TH1,PH1,THX,PHX)

        CALL CR3S(-TH2,0.0,-PH2,AR,PHX+ANGLEZ*RDUNIT,THF1,PHF1)
        CALL CR3S(-TH2,0.0,-PH2,AR,PHX-ANGLEZ*RDUNIT,THF2,PHF2)

        CALL STIRAD(PHF1,THF1,UXA,UYA)
        CALL STIRAD(PHF2,THF2,UXB,UYB)
        UYA=PI/RDUNIT/2-UYA
        UYB=PI/RDUNIT/2-UYB

        IF (LATONZ) THEN
          CALL SZOPTU
          CALL SZSTTU(UXA,UYA)
          CALL SZSTTU(UX2,UY2)
          CALL SZSTTU(UXB,UYB)
          CALL SZCLTU
        ELSE
          CALL SZOPLU
          CALL SZMVLU(UXA,UYA)
          CALL SZPLLU(UX2,UY2)
          CALL SZPLLU(UXB,UYB)
          CALL SZCLLU
        END IF

      ELSE

        RVX=SQRT((VX2-VX1)**2+(VY2-VY1)**2)
        XE=(VX2-VX1)/RVX*AR
        YE=(VY2-VY1)/RVX*AR
        CALL CR2C(-(PI-ANGLEZ*RDUNIT),XE,YE,XT1,YT1)
        CALL CR2C(-(PI+ANGLEZ*RDUNIT),XE,YE,XT2,YT2)
        VXA=VX2+XT1
        VYA=VY2+YT1
        VXB=VX2+XT2
        VYB=VY2+YT2

        IF (LATONZ) THEN
          CALL SZOPTV
          CALL SZSTTV(VXA,VYA)
          CALL SZSTTV(VX2,VY2)
          CALL SZSTTV(VXB,VYB)
          CALL SZCLTV
        ELSE
          CALL SZOPLV
          CALL SZMVLV(VXA,VYA)
          CALL SZPLLV(VX2,VY2)
          CALL SZPLLV(VXB,VYB)
          CALL SZCLLV
        END IF

      END IF

      END
