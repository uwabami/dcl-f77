*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZTNZR(N,RPX,RPY)

      REAL      RPX(*),RPY(*)

      LOGICAL   LCLIPZ

      COMMON    /SZBTN3/ LCLIP
      LOGICAL   LCLIP

      SAVE


      LCLIPZ=LCLIP
      LCLIP=.FALSE.
      CALL STEPR2

      CALL SZOPTV
      DO 10 I=1,N
        CALL SZSTTV(RPX(I),RPY(I))
   10 CONTINUE
      CALL SZSTTV(RPX(1),RPY(1))
      CALL SZCLTV

      LCLIP=LCLIPZ
      CALL STRPR2

      END
