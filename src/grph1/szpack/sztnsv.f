*-----------------------------------------------------------------------
*     FILL AREA PRIMITIVE (SOFT FILL - BASIC)
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZTNSV(N,VPX,VPY,IROTA,RSPCE,ITYPE,INDEX)

      REAL      VPX(*),VPY(*)

      PARAMETER (NCMAX=200)

      REAL      SX(NCMAX)
      LOGICAL   LSX,LSY

      EXTERNAL  IMOD,IGUS,INDXRF,RMAX0,RMIN0,RD2R

      SAVE


      CALL SGRGET('BITLEN',BITLEN)
      CALL SGIGET('NBITS',NBITS)
      CALL SGRGET('RFAROT',ROT)

      CYCLE=BITLEN*NBITS

      ROTA=IMOD(IROTA+90,180)-90+ROT
      XCOS=COS(RD2R(ROTA))
      XSIN=SIN(RD2R(ROTA))

      DB1=+XCOS*VPX(1)+XSIN*VPY(1)
      DBMIN=DB1
      DBMAX=DB1
      DO 10 I=2,N
        DBI=+XCOS*VPX(I)+XSIN*VPY(I)
        IF (DBI.LT.DBMIN) THEN
          DBMIN=DBI
        END IF
        IF (DBI.GT.DBMAX) THEN
          DBMAX=DBI
        END IF
   10 CONTINUE
      DBMIN=IGUS(DBMIN/CYCLE)*CYCLE-CYCLE
      DBMAX=IGUS(DBMAX/CYCLE)*CYCLE+CYCLE

      DA1=-XSIN*VPX(1)+XCOS*VPY(1)
      DAMIN=DA1
      DAMAX=DA1
      DO 15 I=1,N
        DAI=-XSIN*VPX(I)+XCOS*VPY(I)
        IF (DAI.LT.DAMIN) THEN
          DAMIN=DAI
        END IF
        IF (DAI.GT.DAMAX) THEN
          DAMAX=DAI
        END IF
   15 CONTINUE
      DAMIN=IGUS(DAMIN/RSPCE)*RSPCE-RSPCE
      DAMAX=IGUS(DAMAX/RSPCE)*RSPCE+RSPCE

      CALL SGIGET('MOVE',MOVEZ)
      CALL SZQIDX(INDEXZ)
      CALL SZQTYP(ITYPEZ)

      CALL SGISET('MOVE',1)
      CALL SZSIDX(INDEX)
      CALL SZSTYP(ITYPE)

*     DO 30 DA=DAMIN,DAMAX,RSPCE
      NCNT=MAX(INT((DAMAX-DAMIN+RSPCE)/RSPCE),0)
      DO 30 IDA=1,NCNT
        DA=DAMIN+RSPCE*(IDA-1)
        XA=-XSIN*DA
        YA=+XCOS*DA
        X1=XA+XCOS*DBMIN
        Y1=YA+XSIN*DBMIN
        X2=XA+XCOS*DBMAX
        Y2=YA+XSIN*DBMAX
        NP=0
        DO 20 I=1,N
          IB=IMOD(I-2,N)+1
          IC=I
          IA=IMOD(I,N)+1
          X3=VPX(IC)
          Y3=VPY(IC)
          X4=VPX(IA)
          Y4=VPY(IA)
          D=-(X2-X1)*(Y4-Y3)+(Y2-Y1)*(X4-X3)
          IF (D.NE.0) THEN
            SXX=(-(Y4-Y3)*(X3-X1)+(X4-X3)*(Y3-Y1))/D
            TXX=(-(Y2-Y1)*(X3-X1)+(X2-X1)*(Y3-Y1))/D
            LSX=0.LT.SXX .AND. SXX.LT.1
            LSY=0.LT.TXX .AND. TXX.LT.1
*           IF (0.LT.SXX .AND. SXX.LT.1 .AND.
*    +          0.LT.TXX .AND. TXX.LT.1 ) THEN
            IF (LSX .AND. LSY) THEN
              NP=NP+1
              SX(NP)=SXX
            ELSE IF (TXX.EQ.0) THEN
              DAIB=-XSIN*VPX(IB)+XCOS*VPY(IB)
              DAIA=-XSIN*VPX(IA)+XCOS*VPY(IA)
              IF ((DAIB-DA)*(DAIA-DA).LT.0) THEN
                NP=NP+1
                SX(NP)=SXX
              END IF
            END IF
          END IF
   20   CONTINUE

        IF (NP.GT.NCMAX) THEN
          CALL MSGDMP('E','SZTNSV','WORKING AREA OVERFLOW.')
        END IF

        IF (NP.GT.0) THEN
          SZMAX=RMAX0(SX,NP,1)
          CALL SZOPLV
          CALL SZMVLV(X1,Y1)
          DO 25 J=1,NP
            SZ=RMIN0(SX,NP,1)
            IZ=INDXRF(SX,NP,1,SZ)
            XS=X1+SZ*(X2-X1)
            YS=Y1+SZ*(Y2-Y1)
            IF (MOD(J,2).NE.0) THEN
              CALL SZMVLV(XS,YS)
            ELSE
              CALL SZPLLV(XS,YS)
            END IF
            SX(IZ)=SZMAX
   25     CONTINUE
          CALL SZCLLV
        END IF
   30 CONTINUE

      CALL SGISET('MOVE',MOVEZ)
      CALL SZSIDX(INDEXZ)
      CALL SZSTYP(ITYPEZ)

      END
