*-----------------------------------------------------------------------
*     POLYLINE PRIMITIVE (3-D)
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZL3OP(INDEX)

      CHARACTER COBJ*80

      COMMON    /SZBPL1/ LMISS,RMISS
      LOGICAL   LMISS

      SAVE


      CALL GLLGET('LMISS',LMISS)
      CALL GLRGET('RMISS',RMISS)

      WRITE(COBJ,'(2I8)') INDEX
      CALL CDBLK(COBJ)
      CALL SWOOPN('SZL3',COBJ)

      CALL SZSIDX(INDEX)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZL3CL

      CALL SWOCLS('SZL3')

      RETURN
      END
