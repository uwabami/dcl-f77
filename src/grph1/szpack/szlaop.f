*-----------------------------------------------------------------------
*     ARROW SUBPRIMITIVE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZLAOP(ITYPE,INDEX)

      LOGICAL   LDEG
      CHARACTER COBJ*80

      COMMON    /SZBLA1/ LARRWZ,LPROPZ,AFACTZ,CONSTZ,ANGLEZ,
     +                   LATONZ,LUARWZ,CONSMZ,RDUNIT
      LOGICAL   LARRWZ,LPROPZ,LATONZ,LUARWZ

      EXTERNAL  RFPI

      SAVE


      LARRWZ=.TRUE.

      CALL SGLGET('LPROP',LPROPZ)
      CALL SGRGET('AFACT',AFACTZ)
      CALL SGRGET('CONST',CONSTZ)
      CALL SGRGET('ANGLE',ANGLEZ)
      CALL SGLGET('LATONE',LATONZ)
      CALL SGIGET('IATONE',IATONZ)
      CALL SGLGET('LUARW',LUARWZ)
      CALL SGRGET('CONSTM',CONSMZ)
      CALL SGLGET('LDEG',LDEG)

      IF (LDEG) THEN
        RDUNIT=RFPI()/180
      ELSE
        RDUNIT=1
      END IF

      WRITE(COBJ,'(2I8)') ITYPE,INDEX
      CALL CDBLK(COBJ)
      CALL SWOOPN('SZLA',COBJ)

      CALL SZSLTI(ITYPE,INDEX)

      IF (LATONZ) THEN
        CALL SZQIDX(INDEX)
        IF (IATONZ/1000.EQ.0) IATONE=IATONZ+(INDEX/10)*1000
        CALL SZTNOP(IATONE)
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZLACL

      LARRWZ=.FALSE.

      CALL SZPLCL

      IF (LATONZ) CALL SZTNCL

      CALL SWOCLS('SZLA')

      RETURN
      END
