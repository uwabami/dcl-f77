*-----------------------------------------------------------------------
*     CLIPPING ON VC
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZPCLL(VX0,VY0,VX1,VY1,LVALID,II)

      LOGICAL   LVALID,LCONT,LMOVE

      INTEGER   ISTART(0:1),INDX(4)
      REAL      XLIM(2,0:1),YLIM(2,0:1),VX(2,0:1),VY(2,0:1),XJ(4),YJ(4)

      SAVE


      IF (VX0.LT.XLIM(1,II)) THEN
        KX0=0
      ELSE IF (VX0.GT.XLIM(2,II)) THEN
        KX0=2
      ELSE
        KX0=1
      END IF
      IF (VY0.LT.YLIM(1,II)) THEN
        KY0=0
      ELSE IF (VY0.GT.YLIM(2,II)) THEN
        KY0=2
      ELSE
        KY0=1
      END IF
      K0=KX0+3*KY0

      IF (VX1.LT.XLIM(1,II)) THEN
        KX1=0
      ELSE IF (VX1.GT.XLIM(2,II)) THEN
        KX1=2
      ELSE
        KX1=1
      END IF
      IF (VY1.LT.YLIM(1,II)) THEN
        KY1=0
      ELSE IF (VY1.GT.YLIM(2,II)) THEN
        KY1=2
      ELSE
        KY1=1
      END IF
      K1=KX1+3*KY1

      LVALID=.TRUE.

      IF (K1.EQ.4 .AND. K0.EQ.4) THEN

*       / VISIBLE /

        ISTART(II)=2
        VX(2,II)=VX1
        VY(2,II)=VY1
        RETURN

      ELSE IF (K1.EQ.4 .OR. K0.EQ.4) THEN

*       / HALF VISIBLE /

        IF (KX1.EQ.1 .AND. KX0.EQ.1) THEN

*         / CROSSES VERTICAL EDGE /

          KYL=MAX(KY1,KY0)/2+1
          YI=YLIM(KYL,II)
          XI=VX0+(YI-VY0)/(VY1-VY0)*(VX1-VX0)

        ELSE IF (KY1.EQ.1 .AND. KY0.EQ.1) THEN

*         / CROSSES HORIZONTAL EDGE /

          KXL=MAX(KX1,KX0)/2+1
          XI=XLIM(KXL,II)
          YI=VY0+(XI-VX0)/(VX1-VX0)*(VY1-VY0)

        ELSE

*         / CROSSES DIAGONAL CORNERS /

          KXL=MAX(KX1,KX0)/2+1
          KYL=MAX(KY1,KY0)/2+1
          DYDX=(VY1-VY0)/(VX1-VX0)
          YC=VY1-DYDX*VX1
          XI=XLIM(KXL,II)
          YI=YC+DYDX*XI
          IF (YI.LT.YLIM(1,II) .OR. YI.GT.YLIM(2,II)) THEN
            YI=YLIM(KYL,II)
            XI=(YI-YC)/DYDX
          END IF
        END IF

        IF (K1.EQ.4) THEN
          ISTART(II)=1
          VX(1,II)=XI
          VY(1,II)=YI
          VX(2,II)=VX1
          VY(2,II)=VY1
        ELSE
          ISTART(II)=2
          VX(2,II)=XI
          VY(2,II)=YI
        END IF

      ELSE IF ((ABS(KX1-KX0)+ABS(KY1-KY0)).GE.2) THEN

*       / LINE GOES THROUGH WINDOWED SECTION, BUT STARTS AND ENDS
*         ELSEWHERE.  TEST TO SEE IF VERTICAL OR HORIZONTAL. /

        ISTART(II)=1
        IF (VX1.EQ.VX0) THEN
          VX(1,II)=VX1
          VY(1,II)=YLIM(1,II)
          VX(2,II)=VX1
          VY(2,II)=YLIM(2,II)
          IF (MOD(KX0,2).EQ.0) LVALID=.FALSE.
        ELSE IF (VY1.EQ.VY0) THEN
          VX(1,II)=XLIM(1,II)
          VY(1,II)=VY1
          VX(2,II)=XLIM(2,II)
          VY(2,II)=VY1
          IF (MOD(KY0,2).EQ.0) LVALID=.FALSE.
        ELSE
          DYDX=(VY1-VY0)/(VX1-VX0)
          DXDY=1.0/DYDX
          YC=VY1-DYDX*VX1
          YJ(1)=YLIM(2,II)
          XJ(1)=DXDY*(YJ(1)-YC)
          XJ(2)=XLIM(1,II)
          YJ(2)=YC+DYDX*XJ(2)
          XJ(3)=XLIM(2,II)
          YJ(3)=YC+DYDX*XJ(3)
          YJ(4)=YLIM(1,II)
          XJ(4)=DXDY*(YJ(4)-YC)
          KK=0
          IF (XJ(1).GE.XLIM(1,II) .AND. XJ(1).LE.XLIM(2,II)) THEN
            KK=KK+1
            INDX(KK)=1
          END IF
          IF (YJ(2).GE.YLIM(1,II) .AND. YJ(2).LE.YLIM(2,II)) THEN
            KK=KK+1
            INDX(KK)=2
          END IF
          IF (YJ(3).GE.YLIM(1,II) .AND. YJ(3).LE.YLIM(2,II)) THEN
            KK=KK+1
            INDX(KK)=3
          END IF
          IF (XJ(4).GE.XLIM(1,II) .AND. XJ(4).LE.XLIM(2,II)) THEN
            KK=KK+1
            INDX(KK)=4
          END IF
          IF (KK.GE.1) THEN
            I1=INDX(1)
            I2=INDX(KK)
            VX(1,II)=XJ(I1)
            VY(1,II)=YJ(I1)
            VX(2,II)=XJ(I2)
            VY(2,II)=YJ(I2)
          ELSE
            LVALID=.FALSE.
          END IF
        END IF
      ELSE
        LVALID=.FALSE.
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZGCLL(XX, YY, LCONT, LMOVE, II)

      XX=VX(ISTART(II),II)
      YY=VY(ISTART(II),II)
      LCONT=ISTART(II).EQ.1
      LMOVE=ISTART(II).EQ.1
      ISTART(II)=ISTART(II)+1

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZSCLL(VXMIN,VXMAX,VYMIN,VYMAX,II)

      XLIM(1,II)=VXMIN
      XLIM(2,II)=VXMAX
      YLIM(1,II)=VYMIN
      YLIM(2,II)=VYMAX

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZQCLL(VXMIN,VXMAX,VYMIN,VYMAX,II)

      VXMIN=XLIM(1,II)
      VXMAX=XLIM(2,II)
      VYMIN=YLIM(1,II)
      VYMAX=YLIM(2,II)

      RETURN
      END
