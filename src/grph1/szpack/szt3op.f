*-----------------------------------------------------------------------
*     TONE PRIMITIVE (3-D)
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZT3OP(ITPAT1,ITPAT2)

      CHARACTER COBJ*80

      COMMON    /SZBTN2/ IRMODE, IRMODR
      COMMON    /SZBTN4/ ITPT1, ITPT2

      SAVE


      CALL SGIGET('IRMODE',IRMODE)
      IRMODE = MOD(IRMODE,2)

      WRITE(COBJ,'(2I8)') ITPAT1, ITPAT2
      CALL CDBLK(COBJ)
      CALL SWOOPN('SZT3',COBJ)

      ITPT1 = ITPAT1
      ITPT2 = ITPAT2

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZT3CL

      CALL SWOCLS('SZT3')

      RETURN
      END
