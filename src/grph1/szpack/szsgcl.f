*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZSGCL(TX0, TY0, TX1, TY1)

      PARAMETER (EPSIL = 1.E-5)

      LOGICAL   LMER, LREQA

      EXTERNAL  RFPI, LREQA

      SAVE


      PI = RFPI()

      TXX = TX0
      TYY = TY0

      IF(LREQA(ABS(TY0), PI/2, EPSIL) .OR.
     +   LREQA(ABS(TY1), PI/2, EPSIL)) THEN
        LMER = .TRUE.
      ELSE
        TAN0 = TAN(TY0)
        TAN1 = TAN(TY1)

        IF (TAN0.EQ.0 .AND. TAN1.EQ.0) THEN
          XLA = 0
        ELSE
          XLA = ATAN2(TAN1*COS(TX0)-TAN0*COS(TX1),
     +                TAN0*SIN(TX1)-TAN1*SIN(TX0))
        END IF

        DX1 = SZXMOD(TX1-TX0)
        XLM = SZXMOD(TX0+DX1/2 - XLA)

        CS0 = COS(TX0-XLA)
        CS1 = COS(TX1-XLA)

        IF (ABS(CS0).GE.ABS(CS1)) THEN
          ALPHA = TAN0/CS0
          LMER = .FALSE.
        ELSE IF (ABS(CS1).NE.0) THEN
          ALPHA = TAN1/CS1
          LMER = .FALSE.
        ELSE
          LMER = .TRUE.
        END IF
      END IF

      IF(LMER) THEN
        TYY = SIGN(PI/2, (TY0+TY1))
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZQGCY(TX, TY)

      IF (LMER) THEN
        TY = TYY
      ELSE
        TY = ATAN(ALPHA*COS(TX-XLA))
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZQGCX(TY, TX)

      IF (LMER) THEN
        TX = TXX
      ELSE
        IF(ALPHA.EQ.0) THEN
          TX = TXX
        ELSE
          TT = TAN(TY)/ALPHA
          IF (ABS(TT).GE.1) THEN
            TX = SIGN(PI, XLM) + XLA
          ELSE
            TX = SIGN(ACOS(TT), XLM) + XLA
          END IF
        END IF
      END IF

      RETURN
      END
