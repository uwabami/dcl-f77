*-----------------------------------------------------------------------
*     PLOT ROUTINE ON THE 3-D COORDINATE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZOPL3

      CALL SZOPLR

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZMVL3(SX, SY, SZ)

      CALL STFPR3(SX, SY, SZ, RX, RY)
      CALL SZMVLR(RX, RY)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZPLL3(SX, SY, SZ)

      CALL STFPR3(SX, SY, SZ, RX, RY)
      CALL SZPLLR(RX, RY)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZCLL3

      CALL SZCLLR

      RETURN
      END
