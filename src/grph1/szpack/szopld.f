*-----------------------------------------------------------------------
*     PLOT ROUTINE ON VC (DASH LINE)
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZOPLD

      PARAMETER (MAXNB=32)

      INTEGER   M(MAXNB)
      REAL      A(MAXNB)
      LOGICAL   LDASH,LOPEN

      EXTERNAL  ISHIFT

      SAVE

      DATA      LDASH/.FALSE./


*     / OPEN DASH LINE SEGMENT /

      LOPEN=.TRUE.
      CALL SZOPLP

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZMVLD(VX,VY)

*     / PEN-UP MOVE /

      CALL SZMVLP(VX,VY)

      IF (LDASH) THEN

*       / DASH /

        IF (LOPEN) THEN

*         / BEGINNING OF DASH LINE /

          T=0.0
          LOPEN=.FALSE.

        ELSE

          S=SQRT((VX-VXM)**2+(VY-VYM)**2)
          IF (MOVE.EQ.1) THEN
            T=MOD(S+T,C)
          ELSE IF (MOVE.EQ.-1) THEN
            T=0.0
          END IF

        END IF

      END IF

      VXM=VX
      VYM=VY

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZPLLD(VX,VY)

*     / PEN-DOWN MOVE /

      IF (.NOT.LDASH) THEN

*       / SOLID /

        CALL SZPLLP(VX,VY)

      ELSE

*       / DASH /

        S=SQRT((VX-VXM)**2+(VY-VYM)**2)

        IF (S.EQ.0) RETURN

        DX=(VX-VXM)/S
        DY=(VY-VYM)/S

*       / FIND CURRENT POSITION INDEX /

        IX=0
   10   CONTINUE
          IX=IX+1
        IF (A(IX).LT.T) GO TO 10

*       / CHECK LENGTH OF LINE /

        IF (T+S.LE.C) THEN

*         / FIT IN THE CURRENT CYCLE /

          I=IX
   15     IF (T+S.LE.A(I)) GO TO 20
            IF (M(I).EQ.0) THEN
              CALL SZMVLP(VXM+DX*(A(I)-T),VYM+DY*(A(I)-T))
            ELSE IF (M(I).EQ.1) THEN
              CALL SZPLLP(VXM+DX*(A(I)-T),VYM+DY*(A(I)-T))
            END IF
            I=I+1
            GO TO 15
   20     CONTINUE
          IF (M(I).EQ.0) THEN
            CALL SZMVLP(VX,VY)
          ELSE IF (M(I).EQ.1) THEN
            CALL SZPLLP(VX,VY)
          END IF
          T=T+S

        ELSE

*         / CONTINUE TO THE NEXT CYCLE /

          DO 25 I=IX,NA
            IF (M(I).EQ.0) THEN
              CALL SZMVLP(VXM+DX*(A(I)-T),VYM+DY*(A(I)-T))
            ELSE IF (M(I).EQ.1) THEN
              CALL SZPLLP(VXM+DX*(A(I)-T),VYM+DY*(A(I)-T))
            END IF
   25     CONTINUE

          VXM=VXM+DX*(C-T)
          VYM=VYM+DY*(C-T)
          S=S-(C-T)
          T=0
          L=S/C

*         / PROCESS CYCLES /

          DO 35 J=1,L
            DO 30 I=1,NA
              IF (M(I).EQ.0) THEN
                CALL SZMVLP(VXM+DX*A(I),VYM+DY*A(I))
              ELSE IF (M(I).EQ.1) THEN
                CALL SZPLLP(VXM+DX*A(I),VYM+DY*A(I))
              END IF
   30       CONTINUE
            VXM=VXM+DX*C
            VYM=VYM+DY*C
            S=S-C
   35     CONTINUE

*         / FIND LAST POSITON INDEX /

          IX=0
   40     CONTINUE
            IX=IX+1
          IF (A(IX).LT.S) GO TO 40

*         / PROCESS REMAINING CYCLE /

          DO 45 I=1,IX-1
            IF (M(I).EQ.0) THEN
              CALL SZMVLP(VXM+DX*A(I),VYM+DY*A(I))
            ELSE IF (M(I).EQ.1) THEN
              CALL SZPLLP(VXM+DX*A(I),VYM+DY*A(I))
            END IF
   45     CONTINUE
          IF (M(IX).EQ.0) THEN
            CALL SZMVLP(VX,VY)
          ELSE IF (M(I).EQ.1) THEN
            CALL SZPLLP(VX,VY)
          END IF
          T=S

        END IF

      END IF

      VXM=VX
      VYM=VY

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZCLLD

*     / CLOSE DASHLINE SEGMENT /

      CALL SZCLLP

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZSTYZ(IPAT)

      CALL SGRGET('BITLEN',BL)
      CALL SGIGET('MOVE',MOVE)
      CALL SGIGET('NBITS',NB)

*     / GENERATE SOLID PATTERN /

      ISOLID=0
      DO 50 I=1,NB
        ISOLID=IOR(ISHIFT(ISOLID,1),1)
   50 CONTINUE

*     / CHECK IPAT /

      JPAT=IAND(ISOLID,IPAT)
      IF (JPAT.EQ.0 .OR. JPAT.EQ.ISOLID) THEN

*       / SOLID /

        LDASH=.FALSE.

      ELSE

*       / DASH /

        LDASH=.TRUE.

        NA=1
        MODE1=IAND(ISHIFT(JPAT,1-NB),1)
        A(1)=BL
        M(1)=MODE1
        MODEX=MODE1

        DO 55 I=2,NB
          MODEI=IAND(ISHIFT(JPAT,I-NB),1)
          IF (MODEI.NE.MODEX) THEN
            NA=NA+1
            A(NA)=A(NA-1)
            M(NA)=MODEI
            MODEX=MODEI
          END IF
          A(NA)=A(NA)+BL
   55   CONTINUE

*       / INITIALIZATION /

        C=A(NA)

      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZQTYZ(IPAT)

      IPAT=JPAT

      RETURN
      END
