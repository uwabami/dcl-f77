*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZPLZU(N,UPX,UPY)

      REAL      UPX(*),UPY(*)

      LOGICAL   LFLAG

      COMMON    /SZBPL1/ LMISS,RMISS
      LOGICAL   LMISS

      SAVE


      CALL SZOPLU

      IF (.NOT.LMISS) THEN
        CALL SZMVLU(UPX(1),UPY(1))
        DO 10 I=2,N
          CALL SZPLLU(UPX(I),UPY(I))
   10   CONTINUE
      ELSE
        LFLAG=.FALSE.
        DO 20 I=1,N
          IF (UPX(I).EQ.RMISS .OR. UPY(I).EQ.RMISS) THEN
            LFLAG=.FALSE.
          ELSE IF (LFLAG) THEN
            CALL SZPLLU(UPX(I),UPY(I))
          ELSE
            CALL SZMVLU(UPX(I),UPY(I))
            LFLAG=.TRUE.
          END IF
   20   CONTINUE
      END IF

      CALL SZCLLU

      END
