*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SCTNZV(VPX,VPY,VPZ,ITPAT1,ITPAT2)

      REAL      VPX(*),VPY(*),VPZ(*)


      IF (ITPAT1.EQ.0 .OR. ITPAT2.EQ.0) THEN
        CALL MSGDMP('M','SCTNZV','TONE PAT. INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (ITPAT1.LT.0 .OR. ITPAT2.LE.0) THEN
        CALL MSGDMP('E','SCTNZV','TONE PAT. INDEX IS LESS THAN 0.')
      END IF

      CALL SZT3OP(ITPAT1, ITPAT2)
      CALL SZT3ZV(VPX,VPY,VPZ)
      CALL SZT3CL

      END
