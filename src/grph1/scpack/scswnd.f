*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SCSWND(UXMIN, UXMAX, UYMIN, UYMAX, UZMIN, UZMAX)


      CALL SGRSET('UXMIN3',UXMIN)
      CALL SGRSET('UXMAX3',UXMAX)
      CALL SGRSET('UYMIN3',UYMIN)
      CALL SGRSET('UYMAX3',UYMAX)
      CALL SGRSET('UZMIN3',UZMIN)
      CALL SGRSET('UZMAX3',UZMAX)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SCQWND(UXMIN, UXMAX, UYMIN, UYMAX, UZMIN, UZMAX)

      CALL SGRGET('UXMIN3',UXMIN)
      CALL SGRGET('UXMAX3',UXMAX)
      CALL SGRGET('UYMIN3',UYMIN)
      CALL SGRGET('UYMAX3',UYMAX)
      CALL SGRGET('UZMIN3',UZMIN)
      CALL SGRGET('UZMAX3',UZMAX)

      RETURN
      END
