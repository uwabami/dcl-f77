*-----------------------------------------------------------------------
*     3-D NORMALIZATION TRANSFORMATION
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SCSVPT(VXMIN, VXMAX, VYMIN, VYMAX, VZMIN, VZMAX)


      CALL SGRSET('VXMIN3',VXMIN)
      CALL SGRSET('VXMAX3',VXMAX)
      CALL SGRSET('VYMIN3',VYMIN)
      CALL SGRSET('VYMAX3',VYMAX)
      CALL SGRSET('VZMIN3',VZMIN)
      CALL SGRSET('VZMAX3',VZMAX)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SCQVPT(VXMIN, VXMAX, VYMIN, VYMAX, VZMIN, VZMAX)

      CALL SGRGET('VXMIN3',VXMIN)
      CALL SGRGET('VXMAX3',VXMAX)
      CALL SGRGET('VYMIN3',VYMIN)
      CALL SGRGET('VYMAX3',VYMAX)
      CALL SGRGET('VZMIN3',VZMIN)
      CALL SGRGET('VZMAX3',VZMAX)

      RETURN
      END
