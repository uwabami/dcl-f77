*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SCSPRJ

      LOGICAL LDEG, LPRJCT, LSYSFNT

      EXTERNAL RFPI


      CALL SCQOBJ(XOBJ, YOBJ, ZOBJ)
      CALL SCQEYE(XEYE, YEYE, ZEYE)

      CALL SGIGET('ITR3',ITR3)
      CALL SGRGET('XOFF3',XOFF)
      CALL SGRGET('YOFF3',YOFF)
      CALL SGRGET('TILT3',TILT)
      CALL SGRGET('ANGLE3',ANGLE)
      CALL SGLGET('LDEG',LDEG)

      IF (LDEG) THEN
        CP = RFPI()/180
      ELSE
        CP = 1
      END IF

      RZ  = SQRT((XEYE-XOBJ)**2 + (YEYE-YOBJ)**2 )
      RR  = SQRT((XEYE-XOBJ)**2 + (YEYE-YOBJ)**2 + (ZEYE-ZOBJ)**2)

      THE = ATAN2(RZ, ZEYE-ZOBJ)
      PHI = ATAN2(YEYE - YOBJ, XEYE - XOBJ)
      PSI = RFPI()/2 -  CP*TILT

      CALL STQWTR(RXMIN, RXMAX, RYMIN, RYMAX,
     +            WXMIN, WXMAX, WYMIN, WYMAX, IWTRF)
      X0 = (RXMIN+RXMAX)/2 + XOFF
      Y0 = (RYMIN+RYMAX)/2 + YOFF

      IF (ANGLE.EQ.0) THEN
        CALL MSGDMP('E', 'SCSPRJ', 'ANGLE MUST NOT BE ZERO.')
      ELSE
        FAC = ABS(.5/(RR*SIN(CP*ANGLE/2.)))
      END IF

      IF (ANGLE.LE.0) RR = -RR

      CALL STSPR3(XOBJ, YOBJ, ZOBJ, THE, PHI, PSI, FAC, RR, X0, Y0)

*     / 2-D PROJECTION /

      CALL SGLGET('L2TO3',LPRJCT)
      IF (LPRJCT) THEN
        CALL SGIGET('IXC3',IXC3)
        CALL SGIGET('IYC3',IYC3)
        CALL SGRGET('SEC3',SEC3)
        CALL STSPR2(IXC3, IYC3, SEC3)
      ELSE
        CALL STSPR2(0, 0, 0.)
      END IF

      CALL SWLGET('LSYSFNT',LSYSFNT)
      IF (LSYSFNT) THEN
        CALL MSGDMP('W', 'SCSPRJ',
     +    'SYSTEM FONT IS NOT SUPPORTED IN SCPACK.')
        CALL MSGDMP('W', 'SCSPRJ',
     +    'NOTE : LSYSFNT IS SET TO FALSE.')
        CALL SWLSET('LSYSFNT',.FALSE.)
      END IF

      END
