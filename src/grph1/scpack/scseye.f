*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SCSEYE(XEYE3, YEYE3, ZEYE3)


      CALL SGRSET('XEYE3',XEYE3)
      CALL SGRSET('YEYE3',YEYE3)
      CALL SGRSET('ZEYE3',ZEYE3)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SCQEYE(XEYE3, YEYE3, ZEYE3)

      CALL SGRGET('XEYE3',XEYE3)
      CALL SGRGET('YEYE3',YEYE3)
      CALL SGRGET('ZEYE3',ZEYE3)

      RETURN
      END
