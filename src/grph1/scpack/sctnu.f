*-----------------------------------------------------------------------
*     TONE PRIMITIVE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SCTNU(UPX,UPY,UPZ)

      REAL      UPX(*),UPY(*),UPZ(*),VPX(*),VPY(*),VPZ(*)

      SAVE

      DATA      ITPT1Z, ITPT2Z /1, 1/


      IF (ITPT1Z.EQ.0 .OR. ITPT2Z.EQ.0) THEN
        CALL MSGDMP('M','SCTNU','TONE PAT. INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (ITPT1Z.LT.0 .OR. ITPT2Z.LT.0) THEN
        CALL MSGDMP('E','SCTNU','TONE PAT. INDEX IS LESS THAN 0.')
      END IF

      CALL SZT3OP(ITPT1Z, ITPT2Z)
      CALL SZT3ZU(UPX,UPY,UPZ)
      CALL SZT3CL

      RETURN
*-----------------------------------------------------------------------
      ENTRY SCTNV(VPX,VPY,VPZ)

      IF (ITPT1Z.EQ.0 .OR. ITPT2Z.EQ.0) THEN
        CALL MSGDMP('M','SCTNV','TONE PAT. INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (ITPT1Z.LT.0 .OR. ITPT2Z.LT.0) THEN
        CALL MSGDMP('E','SCTNV','TONE PAT. INDEX IS LESS THAN 0.')
      END IF

      CALL SZT3OP(ITPT1Z, ITPT2Z)
      CALL SZT3ZV(VPX,VPY,VPZ)
      CALL SZT3CL

      RETURN
*-----------------------------------------------------------------------
      ENTRY SCSTNP(ITPAT1, ITPAT2)

      ITPT1Z=ITPAT1
      ITPT2Z=ITPAT2

      RETURN
*-----------------------------------------------------------------------
      ENTRY SCQTNP(ITPAT1, ITPAT2)

      ITPAT1=ITPT1Z
      ITPAT2=ITPT2Z

      RETURN
      END
