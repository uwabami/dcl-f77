*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SCSTRF

      LOGICAL   LDEG, LXLOG, LYLOG, LZLOG


      CALL SGIGET('ITR3',ITR3)
      CALL SGLGET('LDEG',LDEG)
      CALL SCQLOG(LXLOG, LYLOG, LZLOG)

      IF (ITR3.EQ.1) THEN

*       / LINEAR AND LOG /

        CALL SCQVPT(VXMIN, VXMAX, VYMIN, VYMAX, VZMIN, VZMAX)

        IF (.NOT.(VXMIN.LT.VXMAX .AND. VYMIN.LT.VYMAX
     +                           .AND. VZMIN.LT.VZMAX)) THEN
          CALL MSGDMP('E','SCSTRF','VIEWPORT DEFINITION IS INVALID.')
        END IF

        CALL SCQWND(UXMIN, UXMAX, UYMIN, UYMAX, UZMIN, UZMAX)

        IF (.NOT. LXLOG) THEN
          CX  = (VXMAX-VXMIN) / (UXMAX-UXMIN)
          VX0 = VXMIN - CX*UXMIN
        ELSE
          IF (UXMIN*UXMAX.LE.0) THEN
            CALL MSGDMP('E','SCSTRF',
     +           'THE REGION STRADDLES 0 FOR LOG TRANSFORMATION (X).')
          END IF
          CX = (VXMAX-VXMIN) / LOG10(UXMAX/UXMIN)
          VX0 = VXMIN - CX*LOG10(ABS(UXMIN))
        END IF

        IF (.NOT. LYLOG) THEN
          CY = (VYMAX-VYMIN) / (UYMAX-UYMIN)
          VY0 = VYMIN - CY*UYMIN
        ELSE
          IF (UYMIN*UYMAX.LE.0) THEN
            CALL MSGDMP('E','SCSTRF',
     +           'THE REGION STRADDLES 0 FOR LOG TRANSFORMATION (Y).')
          END IF
          CY = (VYMAX-VYMIN) / LOG10(UYMAX/UYMIN)
          VY0 = VYMIN - CY*LOG10(ABS(UYMIN))
        END IF

        IF (.NOT. LZLOG) THEN
          CZ = (VZMAX-VZMIN) / (UZMAX-UZMIN)
          VZ0 = VZMIN - CZ*UZMIN
        ELSE
          IF (UZMIN*UZMAX.LE.0) THEN
            CALL MSGDMP('E','SCSTRF',
     +           'THE REGION STRADDLES 0 FOR LOG TRANSFORMATION (Z).')
          END IF
          CZ = (VZMAX-VZMIN) / LOG10(UZMAX/UZMIN)
          VZ0 = VZMIN - CZ*LOG10(ABS(UZMIN))
        END IF

        CALL STSTR3(ITR3, CX, CY, CZ, VX0, VY0, VZ0)
        CALL STSLG3(LXLOG, LYLOG, LZLOG)
        CALL STSRD3(.FALSE., .FALSE., .FALSE.)

      ELSE IF (ITR3.EQ.2) THEN

*       / CYLINDRICAL /

        CALL SGRGET('SIMFAC3', FAC)
        CALL SGRGET('VXORG3', VX0)
        CALL SGRGET('VYORG3', VY0)
        CALL SGRGET('VZORG3', VZ0)

        CALL STSTR3(ITR3, FAC, FAC, FAC, VX0, VY0, VZ0)
        CALL STSLG3(.FALSE., .FALSE., .FALSE.)
        CALL STSRD3(.FALSE., LDEG, .FALSE.)

      ELSE IF (ITR3.EQ.3) THEN

*       / SPHERICAL /

        CALL SGRGET('SIMFAC3', FAC)
        CALL SGRGET('VXORG3', VX0)
        CALL SGRGET('VYORG3', VY0)
        CALL SGRGET('VZORG3', VZ0)

        CALL STSTR3(ITR3, FAC, FAC, FAC, VX0, VY0, VZ0)
        CALL STSLG3(.FALSE., .FALSE., .FALSE.)
        CALL STSRD3(.FALSE., LDEG, LDEG)

      ELSE

        CALL MSGDMP('E','SCSTRF',
     +       'TRANSFORMATION FUNCTION NUMBER IS INVALID.')

      END IF

      END
