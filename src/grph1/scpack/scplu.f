*-----------------------------------------------------------------------
*     POLYLINE PRIMITIVE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SCPLU(N,UPX,UPY,UPZ)

      REAL      UPX(*),UPY(*),UPZ(*),VPX(*),VPY(*),VPZ(*)

      SAVE

      DATA      INDEXZ/1/


      IF (N.LT.2) THEN
        CALL MSGDMP('E','SCPLU','NUMBER OF POINTS IS LESS THAN 2.')
      END IF
      IF (INDEXZ.EQ.0) THEN
        CALL MSGDMP('M','SCPLU','POLYLINE INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEXZ.LT.0) THEN
        CALL MSGDMP('E','SCPLU','POLYLINE INDEX IS LESS THAN 0.')
      END IF

      CALL SZL3OP(INDEXZ)
      CALL SZL3ZU(N,UPX,UPY,UPZ)
      CALL SZL3CL

      RETURN
*-----------------------------------------------------------------------
      ENTRY SCPLV(N,VPX,VPY,VPZ)

      IF (N.LT.2) THEN
        CALL MSGDMP('E','SCPLV','NUMBER OF POINTS IS LESS THAN 2.')
      END IF
      IF (INDEXZ.EQ.0) THEN
        CALL MSGDMP('M','SCPLV','POLYLINE INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEXZ.LT.0) THEN
        CALL MSGDMP('E','SCPLV','POLYLINE INDEX IS LESS THAN 0.')
      END IF

      CALL SZL3OP(INDEXZ)
      CALL SZL3ZV(N,VPX,VPY,VPZ)
      CALL SZL3CL

      RETURN
*-----------------------------------------------------------------------
      ENTRY SCSPLI(INDEX)

      INDEXZ=INDEX

      RETURN
*-----------------------------------------------------------------------
      ENTRY SCQPLI(INDEX)

      INDEX=INDEXZ

      RETURN
      END
