*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SCSPLN(IXAX, IYAX, SECT)


      CALL SGISET('IXC3',IXAX)
      CALL SGISET('IYC3',IYAX)
      CALL SGRSET('SEC3',SECT)
      CALL SGLSET('L2TO3', .TRUE.)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SCQPLN(IXAX, IYAX, SECT)

      CALL SGIGET('IXC3',IXAX)
      CALL SGIGET('IYC3',IYAX)
      CALL SGRGET('SEC3',SECT)

      RETURN
      END
