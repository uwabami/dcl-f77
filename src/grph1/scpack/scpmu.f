*-----------------------------------------------------------------------
*     POLYMARKER PRIMITIVE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SCPMU(N,UPX,UPY,UPZ)

      REAL      UPX(*),UPY(*),UPZ(*),VPX(*),VPY(*),VPZ(*)

      SAVE

      DATA      ITYPEZ/1/,INDEXZ/1/,RSIZEZ/0.01/


      IF (N.LT.1) THEN
        CALL MSGDMP('E','SCPMU','NUMBER OF POINTS IS LESS THAN 1.')
      END IF
      IF (ITYPEZ.EQ.0) THEN
        CALL MSGDMP('M','SCPMU','MARKER TYPE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEXZ.EQ.0) THEN
        CALL MSGDMP('M','SCPMU','POLYMARKER INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEXZ.LT.0) THEN
        CALL MSGDMP('E','SCPMU','POLYMARKER INDEX IS LESS THAN 0.')
      END IF
      IF (RSIZEZ.EQ.0) THEN
        CALL MSGDMP('M','SCPMU','MARKER SIZE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (RSIZEZ.LT.0) THEN
        CALL MSGDMP('E','SCPMU','MARKER SIZE IS LESS THAN ZERO.')
      END IF

      CALL SZM3OP(ITYPEZ,INDEXZ,RSIZEZ)
      CALL SZM3ZU(N,UPX,UPY,UPZ)
      CALL SZM3CL

      RETURN
*-----------------------------------------------------------------------
      ENTRY SCPMV(N,VPX,VPY,VPZ)

      IF (N.LT.1) THEN
        CALL MSGDMP('E','SCPMV','NUMBER OF POINTS IS LESS THAN 1.')
      END IF
      IF (ITYPEZ.EQ.0) THEN
        CALL MSGDMP('M','SCPMV','MARKER TYPE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEXZ.EQ.0) THEN
        CALL MSGDMP('M','SCPMV','POLYMARKER INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEXZ.LT.0) THEN
        CALL MSGDMP('E','SCPMV','POLYMARKER INDEX IS LESS THAN 0.')
      END IF
      IF (RSIZEZ.EQ.0) THEN
        CALL MSGDMP('M','SCPMV','MARKER SIZE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (RSIZEZ.LT.0) THEN
        CALL MSGDMP('E','SCPMV','MARKER SIZE IS LESS THAN ZERO.')
      END IF

      CALL SZM3OP(ITYPEZ,INDEXZ,RSIZEZ)
      CALL SZM3ZV(N,VPX,VPY,VPZ)
      CALL SZM3CL

      RETURN
*-----------------------------------------------------------------------
      ENTRY SCSPMT(ITYPE)

      ITYPEZ=ITYPE

      RETURN
*-----------------------------------------------------------------------
      ENTRY SCQPMT(ITYPE)

      ITYPE=ITYPEZ

      RETURN
*-----------------------------------------------------------------------
      ENTRY SCSPMI(INDEX)

      INDEXZ=INDEX

      RETURN
*-----------------------------------------------------------------------
      ENTRY SCQPMI(INDEX)

      INDEX=INDEXZ

      RETURN
*-----------------------------------------------------------------------
      ENTRY SCSPMS(RSIZE)

      RSIZEZ=RSIZE

      RETURN
*-----------------------------------------------------------------------
      ENTRY SCQPMS(RSIZE)

      RSIZE=RSIZEZ

      RETURN
      END
