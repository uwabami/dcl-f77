*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SCPMZU(N,UPX,UPY,UPZ,ITYPE,INDEX,RSIZE)

      REAL      UPX(*),UPY(*),UPZ(*)


      IF (N.LT.1) THEN
        CALL MSGDMP('E','SCPMZU','NUMBER OF POINTS IS LESS THAN 1.')
      END IF
      IF (ITYPE.EQ.0) THEN
        CALL MSGDMP('M','SCPMZU','MARKER TYPE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.EQ.0) THEN
        CALL MSGDMP('M','SCPMZU','POLYMARKER INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.LT.0) THEN
        CALL MSGDMP('E','SCPMZU','POLYMARKER INDEX IS LESS THAN 0.')
      END IF
      IF (RSIZE.EQ.0) THEN
        CALL MSGDMP('M','SCPMZU','MARKER SIZE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (RSIZE.LT.0) THEN
        CALL MSGDMP('E','SCPMZU','MARKER SIZE IS LESS THAN ZERO.')
      END IF

      CALL SZM3OP(ITYPE,INDEX,RSIZE)
      CALL SZM3ZU(N,UPX,UPY,UPZ)
      CALL SZM3CL

      END
