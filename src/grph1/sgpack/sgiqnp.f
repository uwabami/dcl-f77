*-----------------------------------------------------------------------
*     INTEGER PARAMATER CONTROL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGIQNP(NCP)

      CHARACTER CP*(*)

      PARAMETER (NPARA = 24)

      INTEGER   IX(NPARA)
      LOGICAL   LCHREQ, LFIRST
      CHARACTER CPARAS(NPARA)*8
      CHARACTER CPARAL(NPARA)*40
      CHARACTER CMSG*80

      EXTERNAL  LCHREQ,LENC

      SAVE

*     / SHORT NAME /

      DATA      CPARAS( 1)/'IRMODE  '/, IX( 1) / 0 /
      DATA      CPARAS( 2)/'MOVE    '/, IX( 2) / 1 /
      DATA      CPARAS( 3)/'NBITS   '/, IX( 3) / 16 /
      DATA      CPARAS( 4)/'ISUP    '/, IX( 4) / 124 /
      DATA      CPARAS( 5)/'ISUB    '/, IX( 5) / 95 /
      DATA      CPARAS( 6)/'IRST    '/, IX( 6) / 34 /
      DATA      CPARAS( 7)/'NPMSKIP '/, IX( 7) / 1 /
      DATA      CPARAS( 8)/'IFONT   '/, IX( 8) / 1 /
      DATA      CPARAS( 9)/'ICLRMAP '/, IX( 9) / 1 /
      DATA      CPARAS(10)/'IROT    '/, IX(10) / 0 /
      DATA      CPARAS(11)/'INDEXC  '/, IX(11) / 0 /
      DATA      CPARAS(12)/'NBUFF   '/, IX(12) / 200 /
      DATA      CPARAS(13)/'IATONE  '/, IX(13) / 999 /
      DATA      CPARAS(14)/'IWS     '/, IX(14) / 0 /
      DATA      CPARAS(15)/'ITR     '/, IX(15) / 1 /
      DATA      CPARAS(16)/'IBGCLI  '/, IX(16) / 999 /
      DATA      CPARAS(17)/'NFRAME  '/, IX(17) / 0 /
      DATA      CPARAS(18)/'NPAGE   '/, IX(18) / 0 /
      DATA      CPARAS(19)/'NLEVEL  '/, IX(19) / 0 /
      DATA      CPARAS(20)/'INDEX   '/, IX(20) / 1 /
      DATA      CPARAS(21)/'ITR3    '/, IX(21) / 1 /
      DATA      CPARAS(22)/'IXC3    '/, IX(22) / 1 /
      DATA      CPARAS(23)/'IYC3    '/, IX(23) / 2 /
      DATA      CPARAS(24)/'IFCIDX  '/, IX(24) / 998 /

*     / LONG  NAME /

      DATA      CPARAL( 1) / 'BOUNDARY_DIRECTION' /
      DATA      CPARAL( 2) / 'LINE_RESUME_MODE' /
      DATA      CPARAL( 3) / 'PATTERN_BIT_LENGTH' /
      DATA      CPARAL( 4) / 'BEGIN_SUPERSCRIPT' /
      DATA      CPARAL( 5) / 'BEGIN_SUBSCRIPT' /
      DATA      CPARAL( 6) / 'END_SCRIPT' /
      DATA      CPARAL( 7) / 'POLIMARKER_INTERVAL' /
      DATA      CPARAL( 8) / 'FONT_NUMBER' /
      DATA      CPARAL( 9) / 'COLORMAP_NUMBER' /
      DATA      CPARAL(10) / 'LINE_LABEL_ROTATION_ANGLE' /
      DATA      CPARAL(11) / 'LABEL_CHAR_INDEX' /
      DATA      CPARAL(12) / 'LINE_BUFFERING_LENGTH' /
      DATA      CPARAL(13) / 'ARROWHEAD_SHADE_PATTERN' /
      DATA      CPARAL(14) / '++++IWS     ' /
      DATA      CPARAL(15) / '++++ITR     ' /
      DATA      CPARAL(16) / 'BACKGROUND_COLOR_INDEX' /
      DATA      CPARAL(17) / '****NFRAME  ' /
      DATA      CPARAL(18) / '****NPAGE   ' /
      DATA      CPARAL(19) / '****NLEVEL  ' /
      DATA      CPARAL(20) / 'CORNERMARK_INDEX' /
      DATA      CPARAL(21) / '++++ITR3    ' /
      DATA      CPARAL(22) / '++++IXC3    ' /
      DATA      CPARAL(23) / '++++IYC3    ' /
      DATA      CPARAL(24) / 'FULLCOLOR_INDEX' /

      DATA      LFIRST / .TRUE. /


      NCP = NPARA

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGIQID(CP, IDX)

      DO 10 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IDX = N
          RETURN
        END IF
   10 CONTINUE
      CMSG = 'PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E','SGIQID',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGIQCP(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAS(IDX)
      ELSE
        CALL MSGDMP('E','SGIQCP','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGIQCL(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAL(IDX)
      ELSE
        CALL MSGDMP('E','SGIQCL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGIQVL(IDX, IPARA)

      IF (LFIRST) THEN
        CALL RTIGET('SG', CPARAS, IX, NPARA)
        CALL RLIGET(CPARAL, IX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        IPARA = IX(IDX)
      ELSE
        CALL MSGDMP('E','SGIQVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGISVL(IDX, IPARA)

      IF (LFIRST) THEN
        CALL RTIGET('SG', CPARAS, IX, NPARA)
        CALL RLIGET(CPARAL, IX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        IX(IDX) = IPARA
      ELSE
        CALL MSGDMP('E','SGISVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGIQIN(CP, IN)

      DO 20 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IN = N
          RETURN
        END IF
   20 CONTINUE

      IN = 0

      RETURN
      END
