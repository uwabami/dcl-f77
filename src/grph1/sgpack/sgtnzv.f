*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGTNZV(N,VPX,VPY,ITPAT)

      REAL      VPX(*),VPY(*)


      IF (N.LT.3) THEN
        CALL MSGDMP('E','SGTNZV','NUMBER OF POINTS IS LESS THAN 3.')
      END IF
      IF (ITPAT.EQ.0) THEN
        CALL MSGDMP('M','SGTNZV','TONE PAT. INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (ITPAT.LT.0) THEN
        CALL MSGDMP('E','SGTNZV','TONE PAT. INDEX IS LESS THAN 0.')
      END IF

      CALL SZTNOP(ITPAT)
      CALL SZTNZV(N,VPX,VPY)
      CALL SZTNCL

      END
