*-----------------------------------------------------------------------
*     POLYMARKER PRIMITIVE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGPMU(N,UPX,UPY)

      REAL      UPX(*),UPY(*),VPX(*),VPY(*),RPX(*),RPY(*)

      SAVE

      DATA      ITYPEZ/1/,INDEXZ/1/,RSIZEZ/0.01/


      IF (N.LT.1) THEN
        CALL MSGDMP('E','SGPMU','NUMBER OF POINTS IS LESS THAN 1.')
      END IF
      IF (ITYPEZ.EQ.0) THEN
        CALL MSGDMP('M','SGPMU','MARKER TYPE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEXZ.EQ.0) THEN
        CALL MSGDMP('M','SGPMU','POLYMARKER INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEXZ.LT.0) THEN
        CALL MSGDMP('E','SGPMU','POLYMARKER INDEX IS LESS THAN 0.')
      END IF
      IF (RSIZEZ.EQ.0) THEN
        CALL MSGDMP('M','SGPMU','MARKER SIZE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (RSIZEZ.LT.0) THEN
        CALL MSGDMP('E','SGPMU','MARKER SIZE IS LESS THAN ZERO.')
      END IF

      CALL SZPMOP(ITYPEZ,INDEXZ,RSIZEZ)
      CALL SZPMZU(N,UPX,UPY)
      CALL SZPMCL

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGPMV(N,VPX,VPY)

      IF (N.LT.1) THEN
        CALL MSGDMP('E','SGPMV','NUMBER OF POINTS IS LESS THAN 1.')
      END IF
      IF (ITYPEZ.EQ.0) THEN
        CALL MSGDMP('M','SGPMV','MARKER TYPE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEXZ.EQ.0) THEN
        CALL MSGDMP('M','SGPMV','POLYMARKER INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEXZ.LT.0) THEN
        CALL MSGDMP('E','SGPMV','POLYMARKER INDEX IS LESS THAN 0.')
      END IF
      IF (RSIZEZ.EQ.0) THEN
        CALL MSGDMP('M','SGPMV','MARKER SIZE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (RSIZEZ.LT.0) THEN
        CALL MSGDMP('E','SGPMV','MARKER SIZE IS LESS THAN ZERO.')
      END IF

      CALL SZPMOP(ITYPEZ,INDEXZ,RSIZEZ)
      CALL SZPMZV(N,VPX,VPY)
      CALL SZPMCL

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGPMR(N,RPX,RPY)

      IF (N.LT.1) THEN
        CALL MSGDMP('E','SGPMR','NUMBER OF POINTS IS LESS THAN 1.')
      END IF
      IF (ITYPEZ.EQ.0) THEN
        CALL MSGDMP('M','SGPMR','MARKER TYPE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEXZ.EQ.0) THEN
        CALL MSGDMP('M','SGPMR','POLYMARKER INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEXZ.LT.0) THEN
        CALL MSGDMP('E','SGPMR','POLYMARKER INDEX IS LESS THAN 0.')
      END IF
      IF (RSIZEZ.EQ.0) THEN
        CALL MSGDMP('M','SGPMR','MARKER SIZE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (RSIZEZ.LT.0) THEN
        CALL MSGDMP('E','SGPMR','MARKER SIZE IS LESS THAN ZERO.')
      END IF

      CALL SZPMOP(ITYPEZ,INDEXZ,RSIZEZ)
      CALL SZPMZR(N,RPX,RPY)
      CALL SZPMCL

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGSPMT(ITYPE)

      ITYPEZ=ITYPE

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGQPMT(ITYPE)

      ITYPE=ITYPEZ

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGSPMI(INDEX)

      INDEXZ=INDEX

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGQPMI(INDEX)

      INDEX=INDEXZ

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGSPMS(RSIZE)

      RSIZEZ=RSIZE

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGQPMS(RSIZE)

      RSIZE=RSIZEZ

      RETURN
      END
