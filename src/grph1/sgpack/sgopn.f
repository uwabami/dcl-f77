*-----------------------------------------------------------------------
*     CONTROL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGOPN(IWS)

      LOGICAL   LFIRST,LFULL,LPAGE

      SAVE


*     / SET WORKSTATION NUMBER /

      CALL SGISET('IWS',IWS)

      CONTINUE
*-----------------------------------------------------------------------
      ENTRY SGINIT

*     / SET INITIALIZATION FLAG /

      LFIRST=.TRUE.

*     / CHECK WORKSTATION NUMBER /

      CALL SGIGET('IWS',IWSX)
      CALL SWIGET('MAXWNU',NW)
      JWS=ABS(IWSX)
      IF (.NOT.(1.LE.JWS .AND. JWS.LE.NW)) THEN
        CALL MSGDMP('E','SGINIT','WORKSTATION NUMBER IS INVALID.')
      END IF

*     / OPEN WORKSTATION /

      CALL SWISET('IWS',JWS)
      CALL SWDOPN

*     / GET RANGE OF WORKSTATION RECTANGLE AND SCALING FACTOR /

      CALL SWQRCT(WSXMN,WSXMX,WSYMN,WSYMX,FACT)

*     / SET WORKSTATION RECTANGLE /

      IF (IWSX.GE.0) THEN
        WX0=WSXMX-WSXMN
        WY0=WSYMX-WSYMN
        ITR=1
      ELSE
        WX0=WSYMX-WSYMN
        WY0=WSXMX-WSXMN
        ITR=2
      END IF

      CALL STSWRC(WSXMN,WSXMX,WSYMN,WSYMX)
      CALL SWSROT(ITR)

*     / INITIALIZATION FOR LAYOUT /

      CALL SLINIT(WX0,WY0,FACT)

*     / SET DUMMY TRANSFORMATION /

      RXM=MIN(1.0,WX0/WY0)
      RYM=MIN(WY0/WX0,1.0)
      CALL STSWTR(0.0,RXM,0.0,RYM,0.0,WX0,0.0,WY0,ITR)
      CALL SGSVPT(0.0,RXM,0.0,RYM)
      CALL SGSWND(0.0,1.0,0.0,1.0)
      CALL SGSTRN(1)
      CALL SGSTRF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGFRM

*     / INQUIRE LAYOUT STATUS  /

      CALL SGIGET('NLEVEL',LEV)
      CALL SGIGET('NFRAME',IFR)
      CALL SGIGET('NPAGE' ,IPG)

*     / SET NEW FRAME /

      IFR=IFR+1
      JFR=IFR
      CALL SGISET('NFRAME',IFR)

*     / INQUIRE PAGE NO. /

      CALL SLPAGE(LEV,JFR,IPAGE)
      LPAGE=IPAGE.NE.IPG

      IF (LPAGE) THEN

*       / CLOSE PAGE /

        IF (LFIRST) THEN
          LFIRST=.FALSE.
        ELSE
          CALL SWPCLS
        END IF

*       / NEXT PAGE /

        CALL SGISET('NPAGE',IPAGE)

        CALL SWPOPN

*       / PLOT CORNER MARKS & TITLES /

        CALL SLQRCT(1,1,XAMIN,XAMAX,YAMIN,YAMAX)
        WXL=XAMAX-XAMIN
        WYL=YAMAX-YAMIN
        RXM=MIN(1.0,WXL/WYL)
        RYM=MIN(WYL/WXL,1.0)
        CALL STSWTR(0.0,RXM,0.0,RYM,XAMIN,XAMAX,YAMIN,YAMAX,ITR)

        CALL SLPCNR
        CALL SLPTTL

      END IF

*     / GET WORKSTATION RECTANGLE /

      CALL SLQRCT(LEV,IFR,XAMIN,XAMAX,YAMIN,YAMAX)
      WXL=XAMAX-XAMIN
      WYL=YAMAX-YAMIN

*     / SET WORKSTATION TRANSFORMATION /

      CALL SGLGET('LFULL', LFULL)
      IF (LFULL) THEN
        RXM=MIN(1.0,WXL/WYL)
        RYM=MIN(WYL/WXL,1.0)
        CALL STSWTR(0.0,RXM,0.0,RYM,XAMIN,XAMAX,YAMIN,YAMAX,ITR)
      ELSE
        CALL STSWTR(0.0,1.0,0.0,1.0,XAMIN,XAMAX,YAMIN,YAMAX,ITR)
      END IF

*     / SET NORMALIZATION TRANSFORMATION /

      CALL STQWTR(RXMIN,RXMAX,RYMIN,RYMAX,WXMIN,WXMAX,WYMIN,WYMAX,JTR)
      CALL SGSVPT(RXMIN,RXMAX,RYMIN,RYMAX)
      CALL SGSWND(RXMIN,RXMAX,RYMIN,RYMAX)
      CALL SGSTRN(1)
      CALL SGSTRF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGCLS

*     / CLOSE SGKS /

      CALL SWPCLS
      CALL SWDCLS

      RETURN
      END
