*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGLNZR(RX1,RY1,RX2,RY2,INDEX)


      IF (INDEX.EQ.0) THEN
        CALL MSGDMP('M','SGLNZR','LINE INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.LT.0) THEN
        CALL MSGDMP('E','SGLNZR','LINE INDEX IS LESS THAN 0.')
      END IF

      CALL SZLNOP(INDEX)
      CALL SZLNZR(RX1,RY1,RX2,RY2)
      CALL SZLNCL

      END
