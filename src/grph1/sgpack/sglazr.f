*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGLAZR(RX1,RY1,RX2,RY2,ITYPE,INDEX)


      IF (ITYPE.EQ.0) THEN
        CALL MSGDMP('M','SGLAZR','LINETYPE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.EQ.0) THEN
        CALL MSGDMP('M','SGLAZR','LINE INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.LT.0) THEN
        CALL MSGDMP('E','SGLAZR','LINE INDEX IS INVALID.')
      END IF

      CALL SZLAOP(ITYPE,INDEX)
      CALL SZLAZR(RX1,RY1,RX2,RY2)
      CALL SZLACL

      END
