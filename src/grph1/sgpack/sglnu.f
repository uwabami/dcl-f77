*-----------------------------------------------------------------------
*     LINE SUBPRIMITIVE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGLNU(UX1,UY1,UX2,UY2)

      SAVE

      DATA      INDEXZ/1/


      IF (INDEXZ.EQ.0) THEN
        CALL MSGDMP('M','SGLNU','LINE INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEXZ.LT.0) THEN
        CALL MSGDMP('E','SGLNU','LINE INDEX IS LESS THAN 0.')
      END IF

      CALL SZLNOP(INDEXZ)
      CALL SZLNZU(UX1,UY1,UX2,UY2)
      CALL SZLNCL

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGLNV(VX1,VY1,VX2,VY2)

      IF (INDEXZ.EQ.0) THEN
        CALL MSGDMP('M','SGLNV','LINE INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEXZ.LT.0) THEN
        CALL MSGDMP('E','SGLNV','LINE INDEX IS LESS THAN 0.')
      END IF

      CALL SZLNOP(INDEXZ)
      CALL SZLNZV(VX1,VY1,VX2,VY2)
      CALL SZLNCL

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGLNR(RX1,RY1,RX2,RY2)

      IF (INDEXZ.EQ.0) THEN
        CALL MSGDMP('M','SGLNR','LINE INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEXZ.LT.0) THEN
        CALL MSGDMP('E','SGLNR','LINE INDEX IS LESS THAN 0.')
      END IF

      CALL SZLNOP(INDEXZ)
      CALL SZLNZR(RX1,RY1,RX2,RY2)
      CALL SZLNCL

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGSLNI(INDEX)

      INDEXZ=INDEX

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGQLNI(INDEX)

      INDEX=INDEXZ

      RETURN
      END
