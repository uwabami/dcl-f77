*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGSCWD(CXMIN,CXMAX,CYMIN,CYMAX)


      CALL SGRSET('CXMIN',CXMIN)
      CALL SGRSET('CXMAX',CXMAX)
      CALL SGRSET('CYMIN',CYMIN)
      CALL SGRSET('CYMAX',CYMAX)

      CALL SGQVPT(VXMIN, VXMAX, VYMIN, VYMAX)
      CX  = (VXMAX-VXMIN) / (CXMAX-CXMIN)
      CY  = (VYMAX-VYMIN) / (CYMAX-CYMIN)
      VX0 = VXMIN - CX * CXMIN
      VY0 = VYMIN - CY * CYMIN
      SIMFAC = CX
      VXOFF = VX0 - (VXMAX + VXMIN)/2
      VYOFF = VY0 - (VYMAX + VYMIN)/2
      CALL SGRSET('SIMFAC',SIMFAC)
      CALL SGRSET('VXOFF' ,VXOFF)
      CALL SGRSET('VYOFF' ,VYOFF)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGQCWD(CXMIN,CXMAX,CYMIN,CYMAX)

      CALL SGRGET('CXMIN',CXMIN)
      CALL SGRGET('CXMAX',CXMAX)
      CALL SGRGET('CYMIN',CYMIN)
      CALL SGRGET('CYMAX',CYMAX)

      RETURN
      END
