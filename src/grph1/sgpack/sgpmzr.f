*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGPMZR(N,RPX,RPY,ITYPE,INDEX,RSIZE)

      REAL      RPX(*),RPY(*)


      IF (N.LT.1) THEN
        CALL MSGDMP('E','SGPMZR','NUMBER OF POINTS IS LESS THAN 1.')
      END IF
      IF (ITYPE.EQ.0) THEN
        CALL MSGDMP('M','SGPMZR','MARKER TYPE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.EQ.0) THEN
        CALL MSGDMP('M','SGPMZR','POLYMARKER INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.LT.0) THEN
        CALL MSGDMP('E','SGPMZR','POLYMARKER INDEX IS LESS THAN 0.')
      END IF
      IF (RSIZE.EQ.0) THEN
        CALL MSGDMP('M','SGPMZR','MARKER SIZE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (RSIZE.LT.0) THEN
        CALL MSGDMP('E','SGPMZR','MARKER SIZE IS LESS THAN ZERO.')
      END IF

      CALL SZPMOP(ITYPE,INDEX,RSIZE)
      CALL SZPMZR(N,RPX,RPY)
      CALL SZPMCL

      END
