*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGTRQF(NTX,LTR)

      LOGICAL   LTR
      CHARACTER CTS*(*),CTL*(*)

      PARAMETER (NMAX=29)

      INTEGER   NTR(NMAX)
      LOGICAL   LCHREQ
      CHARACTER CTR1(NMAX)*3,CTR2(NMAX)*20,CMSG*80

      EXTERNAL  LCHREQ,LENC

      SAVE

      DATA CTR1( 1)/'U-U'/, CTR2( 1)/'UNI-UNI           '/, NTR( 1)/ 1/
      DATA CTR1( 2)/'U-L'/, CTR2( 2)/'UNI-LOG           '/, NTR( 2)/ 2/
      DATA CTR1( 3)/'L-U'/, CTR2( 3)/'LOG-UNI           '/, NTR( 3)/ 3/
      DATA CTR1( 4)/'L-L'/, CTR2( 4)/'LOG-LOG           '/, NTR( 4)/ 4/
      DATA CTR1( 5)/'POL'/, CTR2( 5)/'POLAR             '/, NTR( 5)/ 5/
      DATA CTR1( 6)/'BPL'/, CTR2( 6)/'BIPOLAR           '/, NTR( 6)/ 6/
      DATA CTR1( 7)/'ELP'/, CTR2( 7)/'ELLIPTIC          '/, NTR( 7)/ 7/
      DATA CTR1( 8)/'CYL'/, CTR2( 8)/'EQDST. CYLINDRICAL'/, NTR( 8)/10/
      DATA CTR1( 9)/'MER'/, CTR2( 9)/'MERCATOR          '/, NTR( 9)/11/
      DATA CTR1(10)/'MWD'/, CTR2(10)/'MOLLWEIDE         '/, NTR(10)/12/
      DATA CTR1(11)/'HMR'/, CTR2(11)/'HAMMER            '/, NTR(11)/13/
      DATA CTR1(12)/'EK6'/, CTR2(12)/'ECKERT 6          '/, NTR(12)/14/
      DATA CTR1(13)/'KTD'/, CTR2(13)/'KITADA            '/, NTR(13)/15/
      DATA CTR1(14)/'CON'/, CTR2(14)/'PTOLEMAIC CONICAL '/, NTR(14)/20/
      DATA CTR1(15)/'COA'/, CTR2(15)/'EQ.-AREA CONICAL  '/, NTR(15)/21/
      DATA CTR1(16)/'COC'/, CTR2(16)/'CONFORMAL CONICAL '/, NTR(16)/22/
      DATA CTR1(17)/'BON'/, CTR2(17)/'BONNE             '/, NTR(17)/23/
      DATA CTR1(18)/'OTG'/, CTR2(18)/'ORTHOGRAPHIC      '/, NTR(18)/30/
      DATA CTR1(19)/'PST'/, CTR2(19)/'STEREOGRAPHIC     '/, NTR(19)/31/
      DATA CTR1(20)/'AZM'/, CTR2(20)/'EQUIDST. AZIMUTHAL'/, NTR(20)/32/
      DATA CTR1(21)/'AZA'/, CTR2(21)/'AZIMUTHAL EQ. AREA'/, NTR(21)/33/
      DATA CTR1(22)/'GTR'/, CTR2(22)/'GRIDBASE TRANSFORM'/, NTR(22)/51/
      DATA CTR1(23)/'USR'/, CTR2(23)/'USER              '/, NTR(23)/99/
      DATA CTR1(24)/'MIL'/, CTR2(24)/'MILLER            '/, NTR(24)/16/
      DATA CTR1(25)/'RBS'/, CTR2(25)/'ROBINSON          '/, NTR(25)/17/
      DATA CTR1(26)/'SIN'/, CTR2(26)/'SANSON/SINUSOIDAL '/, NTR(26)/18/
      DATA CTR1(27)/'VDG'/, CTR2(27)/'VAN DER GRINTEN   '/, NTR(27)/19/
      DATA CTR1(28)/'PLC'/, CTR2(28)/'POLYCONIC         '/, NTR(28)/24/
      DATA CTR1(29)/'GNO'/, CTR2(29)/'GNOMONIC          '/, NTR(29)/34/

      LTR=INDXIF(NTR,NMAX,1,NTX).NE.0
      
      RETURN
*-----------------------------------------------------------------------
      ENTRY SGTRSL(CTS,CTL)

      DO 10 N=1,NMAX
        IF (LCHREQ(CTS,CTR1(N))) THEN
          CTL=CTR2(N)
          RETURN
        END IF
   10 CONTINUE

      NCP=LENC(CTS)
      CMSG='TRANSFORMATION NAME <'//CTS(1:NCP)//'> IS NOT DEFINED.'
      CALL MSGDMP('E','SGTRSL',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGTRSN(CTS,NTX)

      DO 20 N=1,NMAX
        IF (LCHREQ(CTS,CTR1(N))) THEN
          NTX=NTR(N)
          RETURN
        END IF
   20 CONTINUE

      NCP=LENC(CTS)
      CMSG='TRANSFORMATION NAME <'//CTS(1:NCP)//'> IS NOT DEFINED.'
      CALL MSGDMP('E','SGTRSN',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGTRLS(CTL,CTS)

      DO 30 N=1,NMAX
        IF (LCHREQ(CTL,CTR2(N))) THEN
          CTS=CTR1(N)
          RETURN
        END IF
   30 CONTINUE

      NCP=LENC(CTL)
      CMSG='TRANSFORMATION NAME <'//CTL(1:NCP)//'> IS NOT DEFINED.'
      CALL MSGDMP('E','SGTRLS',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGTRLN(CTL,NTX)

      DO 40 N=1,NMAX
        IF (LCHREQ(CTL,CTR2(N))) THEN
          NTX=NTR(N)
          RETURN
        END IF
   40 CONTINUE

      NCP=LENC(CTL)
      CMSG='TRANSFORMATION NAME <'//CTL(1:NCP)//'> IS NOT DEFINED.'
      CALL MSGDMP('E','SGTRLN',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGTRNS(NTX,CTS)

      DO 50 N=1,NMAX
        IF (NTX.EQ.NTR(N)) THEN
          CTS=CTR1(N)
          RETURN
        END IF
   50 CONTINUE

      CMSG='TRANSFORMATION NUMBER <##> IS NOT DEFINED.'
      CALL CHNGI(CMSG,'##',NTX,'(I2)')
      CALL MSGDMP('E','SGTRNS',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGTRNL(NTX,CTL)

      DO 60 N=1,NMAX
        IF (NTX.EQ.NTR(N)) THEN
          CTL=CTR2(N)
          RETURN
        END IF
   60 CONTINUE

      CMSG='TRANSFORMATION NUMBER <##> IS NOT DEFINED.'
      CALL CHNGI(CMSG,'##',NTX,'(I2)')
      CALL MSGDMP('E','SGTRNL',CMSG)

      RETURN
      END
