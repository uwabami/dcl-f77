*-----------------------------------------------------------------------
*     LOGICAL PARAMETER CONTROL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGLQNP(NCP)

      LOGICAL   LPARA
      CHARACTER CP*(*)

      PARAMETER (NPARA = 26)

      LOGICAL   LX(NPARA)
      LOGICAL   LCHREQ, LFIRST
      CHARACTER CPARAS(NPARA)*8
      CHARACTER CPARAL(NPARA)*40
      CHARACTER CMSG*80

      EXTERNAL  LCHREQ,LENC

      SAVE

*     / SHORT NAME /

      DATA      CPARAS( 1)/'LCLIP   '/, LX( 1) / .FALSE. /
      DATA      CPARAS( 2)/'LCHAR   '/, LX( 2) / .FALSE. /
      DATA      CPARAS( 3)/'LLNINT  '/, LX( 3) / .FALSE. /
      DATA      CPARAS( 4)/'LGCINT  '/, LX( 4) / .TRUE. /
      DATA      CPARAS( 5)/'LSOFTF  '/, LX( 5) / .TRUE. /
      DATA      CPARAS( 6)/'LCL2TN  '/, LX( 6) / .FALSE. /
      DATA      CPARAS( 7)/'LCNTL   '/, LX( 7) / .FALSE. /
      DATA      CPARAS( 8)/'LFULL   '/, LX( 8) / .FALSE. /
      DATA      CPARAS( 9)/'LFPROP  '/, LX( 9) / .TRUE. /
      DATA      CPARAS(10)/'LROT    '/, LX(10) / .FALSE. /
      DATA      CPARAS(11)/'LBUFF   '/, LX(11) / .TRUE. /
      DATA      CPARAS(12)/'LCURV   '/, LX(12) / .TRUE. /
      DATA      CPARAS(13)/'LPROP   '/, LX(13) / .TRUE. /
      DATA      CPARAS(14)/'LUARW   '/, LX(14) / .TRUE. /
      DATA      CPARAS(15)/'LARROW  '/, LX(15) / .FALSE. /
      DATA      CPARAS(16)/'LATONE  '/, LX(16) / .FALSE. /
      DATA      CPARAS(17)/'LDEG    '/, LX(17) / .TRUE. /
      DATA      CPARAS(18)/'LCORNER '/, LX(18) / .FALSE. /
      DATA      CPARAS(19)/'LTITLE  '/, LX(19) / .TRUE. /
      DATA      CPARAS(20)/'LWIDE   '/, LX(20) / .FALSE. /
      DATA      CPARAS(21)/'LFGBG   '/, LX(21) / .FALSE. /
      DATA      CPARAS(22)/'L2TO3   '/, LX(22) / .FALSE. /
      DATA      CPARAS(23)/'LXLOG3  '/, LX(23) / .FALSE. /
      DATA      CPARAS(24)/'LYLOG3  '/, LX(24) / .FALSE. /
      DATA      CPARAS(25)/'LZLOG3  '/, LX(25) / .FALSE. /
      DATA      CPARAS(26)/'LMRKFNT '/, LX(26) / .TRUE. /

*     / LONG  NAME /

      DATA      CPARAL( 1) / 'ENABLE_CLIPPING' /
      DATA      CPARAL( 2) / 'ENABLE_LINE_LABELING' /
      DATA      CPARAL( 3) / 'ENABLE_LINEAR_INTERPOLATION' /
      DATA      CPARAL( 4) / 'ENABLE_GREATCIRCLE_INTERPOLATION' /
      DATA      CPARAL( 5) / 'ENABLE_SOFTFILL' /
      DATA      CPARAL( 6) / 'ENABLE_COLOR_SUBSTITUTION' /
      DATA      CPARAL( 7) / 'ENABLE_CONTROL_CHAR' /
      DATA      CPARAL( 8) / 'USE_FULL_WINDOW' /
      DATA      CPARAL( 9) / 'ENABLE_PROPORTIONAL_FONT' /
      DATA      CPARAL(10) / 'ENABLE_LINE_LABEL_ROTATION' /
      DATA      CPARAL(11) / 'ENABLE_LINE_BUFFERING' /
      DATA      CPARAL(12) / 'DISABLE_LINE_LABEL_AT_CURVE' /
      DATA      CPARAL(13) / 'FIX_ARROWHEAD_PROPORTION' /
      DATA      CPARAL(14) / 'ENABLE_ARROWHEAD_MAPPING' /
      DATA      CPARAL(15) / '----LARROW  ' /
      DATA      CPARAL(16) / 'ENABLE_ARROWHEAD_SHADE' /
      DATA      CPARAL(17) / 'USE_DEGREE' /
      DATA      CPARAL(18) / 'DRAW_CORNERMARK' /
      DATA      CPARAL(19) / 'DRAW_PAGE_TITLE' /
      DATA      CPARAL(20) / '****LWIDE   ' /
      DATA      CPARAL(21) / '****LFGBG   ' /
      DATA      CPARAL(22) / '****L2TO3   ' /
      DATA      CPARAL(23) / '++++LXLOG3  ' /
      DATA      CPARAL(24) / '++++LYLOG3  ' /
      DATA      CPARAL(25) / '++++LZLOG3  ' /
      DATA      CPARAL(26) / 'USE_DCLMARKER_FONT' /

      DATA      LFIRST/.TRUE./

      NCP = NPARA

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGLQID(CP, IDX)

      DO 10 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IDX = N
          RETURN
        END IF
   10 CONTINUE
      CMSG = 'PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E','SGLQID',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGLQCP(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAS(IDX)
      ELSE
        CALL MSGDMP('E','SGLQCP','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGLQCL(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAL(IDX)
      ELSE
        CALL MSGDMP('E','SGLQCL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGLQVL(IDX, LPARA)

      IF (LFIRST) THEN
        CALL RTLGET('SG', CPARAS, LX, NPARA)
        CALL RLLGET(CPARAL, LX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        LPARA = LX(IDX)
      ELSE
        CALL MSGDMP('E','SGLQVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGLSVL(IDX, LPARA)

      IF (LFIRST) THEN
        CALL RTLGET('SG', CPARAS, LX, NPARA)
        CALL RLLGET(CPARAL, LX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        LX(IDX) = LPARA
      ELSE
        CALL MSGDMP('E','SGLSVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGLQIN(CP, IN)

      DO 20 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IN = N
          RETURN
        END IF
   20 CONTINUE

      IN = 0

      RETURN
      END
