*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGSTXY(TXMIN, TXMAX, TYMIN, TYMAX)


      CALL SGRSET('TXMIN',TXMIN)
      CALL SGRSET('TXMAX',TXMAX)
      CALL SGRSET('TYMIN',TYMIN)
      CALL SGRSET('TYMAX',TYMAX)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGQTXY(TXMIN, TXMAX, TYMIN, TYMAX)

      CALL SGRGET('TXMIN',TXMIN)
      CALL SGRGET('TXMAX',TXMAX)
      CALL SGRGET('TYMIN',TYMIN)
      CALL SGRGET('TYMAX',TYMAX)

      RETURN
      END
