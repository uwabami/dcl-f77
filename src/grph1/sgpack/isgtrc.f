*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      INTEGER FUNCTION ISGTRC(CTR)

      CHARACTER CTR*(*)


      NC=LENC(CTR)
      IF (NC.EQ.3) THEN
        CALL SGTRSN(CTR,NTX)
      ELSE
        CALL SGTRLN(CTR,NTX)
      END IF
      ISGTRC=NTX

      END
