*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGPMZV(N,VPX,VPY,ITYPE,INDEX,RSIZE)

      REAL      VPX(*),VPY(*)


      IF (N.LT.1) THEN
        CALL MSGDMP('E','SGPMZV','NUMBER OF POINTS IS LESS THAN 1.')
      END IF
      IF (ITYPE.EQ.0) THEN
        CALL MSGDMP('M','SGPMZV','MARKER TYPE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.EQ.0) THEN
        CALL MSGDMP('M','SGPMZV','POLYMARKER INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.LT.0) THEN
        CALL MSGDMP('E','SGPMZV','POLYMARKER INDEX IS LESS THAN 0.')
      END IF
      IF (RSIZE.EQ.0) THEN
        CALL MSGDMP('M','SGPMZV','MARKER SIZE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (RSIZE.LT.0) THEN
        CALL MSGDMP('E','SGPMZV','MARKER SIZE IS LESS THAN ZERO.')
      END IF

      CALL SZPMOP(ITYPE,INDEX,RSIZE)
      CALL SZPMZV(N,VPX,VPY)
      CALL SZPMCL

      END
