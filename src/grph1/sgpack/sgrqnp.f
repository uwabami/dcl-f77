*-----------------------------------------------------------------------
*     SGRQNP / SGRQID / SGRQCP / SGRQVL / SGRSVL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGRQNP(NCP)

      CHARACTER CP*(*)

      PARAMETER (NPARA = 73)

      REAL      RX(NPARA)
      LOGICAL   LCHREQ, LFIRST
      CHARACTER CPARAS(NPARA)*8
      CHARACTER CPARAL(NPARA)*40
      CHARACTER CMSG*80

      EXTERNAL  LCHREQ,LENC

      SAVE

*     / SHORT NAME /

      DATA      CPARAS( 1) / 'RDX     ' / , RX( 1) / 5.0 /
      DATA      CPARAS( 2) / 'RDY     ' / , RX( 2) / 5.0 /
      DATA      CPARAS( 3) / 'BITLEN  ' / , RX( 3) / 0.003 /
      DATA      CPARAS( 4) / 'SMALL   ' / , RX( 4) / 0.7 /
      DATA      CPARAS( 5) / 'SHIFT   ' / , RX( 5) / 0.3 /
      DATA      CPARAS( 6) / 'PMFACT  ' / , RX( 6) / 2.0 /
      DATA      CPARAS( 7) / 'RFAROT  ' / , RX( 7) / 0.01 /
      DATA      CPARAS( 8) / 'TNBLEN  ' / , RX( 8) / 0.001 /
      DATA      CPARAS( 9) / 'FWC     ' / , RX( 9) / 1.25 /
      DATA      CPARAS(10) / 'CWL     ' / , RX(10) / 30.0 /
      DATA      CPARAS(11) / 'FFCT    ' / , RX(11) / 0.5 /
      DATA      CPARAS(12) / 'RBUFF   ' / , RX(12) / 0.99 /
      DATA      CPARAS(13) / 'RCURV   ' / , RX(13) / 1.0 /
      DATA      CPARAS(14) / 'AFACT   ' / , RX(14) / 0.33 /
      DATA      CPARAS(15) / 'CONST   ' / , RX(15) / 0.01 /
      DATA      CPARAS(16) / 'CONSTM  ' / , RX(16) / 5.0 /
      DATA      CPARAS(17) / 'ANGLE   ' / , RX(17) / 20.0 /
      DATA      CPARAS(18) / 'VXMIN   ' / , RX(18) / 0.0 /
      DATA      CPARAS(19) / 'VXMAX   ' / , RX(19) / 1.0 /
      DATA      CPARAS(20) / 'VYMIN   ' / , RX(20) / 0.0 /
      DATA      CPARAS(21) / 'VYMAX   ' / , RX(21) / 1.0 /
      DATA      CPARAS(22) / 'UXMIN   ' / , RX(22) / 0.0 /
      DATA      CPARAS(23) / 'UXMAX   ' / , RX(23) / 1.0 /
      DATA      CPARAS(24) / 'UYMIN   ' / , RX(24) / 0.0 /
      DATA      CPARAS(25) / 'UYMAX   ' / , RX(25) / 1.0 /
      DATA      CPARAS(26) / 'VXOFF   ' / , RX(26) / 0.0 /
      DATA      CPARAS(27) / 'VYOFF   ' / , RX(27) / 0.0 /
      DATA      CPARAS(28) / 'PLX     ' / , RX(28) / 0.0 /
      DATA      CPARAS(29) / 'PLY     ' / , RX(29) / 90.0 /
      DATA      CPARAS(30) / 'PLROT   ' / , RX(30) / 0.0 /
      DATA      CPARAS(31) / 'SIMFAC  ' / , RX(31) / 0.0 /
      DATA      CPARAS(32) / 'STLAT1  ' / , RX(32) / 0.0 /
      DATA      CPARAS(33) / 'STLAT2  ' / , RX(33) / 0.0 /
      DATA      CPARAS(34) / 'TXMIN   ' / , RX(34) / -180.0 /
      DATA      CPARAS(35) / 'TXMAX   ' / , RX(35) / +180.0 /
      DATA      CPARAS(36) / 'TYMIN   ' / , RX(36) / -90.0 /
      DATA      CPARAS(37) / 'TYMAX   ' / , RX(37) / +90.0 /
      DATA      CPARAS(38) / 'CXMIN   ' / , RX(38) / 0.0 /
      DATA      CPARAS(39) / 'CXMAX   ' / , RX(39) / 1.0 /
      DATA      CPARAS(40) / 'CYMIN   ' / , RX(40) / 0.0 /
      DATA      CPARAS(41) / 'CYMAX   ' / , RX(41) / 1.0 /
      DATA      CPARAS(42) / 'RSAT    ' / , RX(42) / 0. /
      DATA      CPARAS(43) / 'FACTOR  ' / , RX(43) / 1.0 /
      DATA      CPARAS(44) / 'CORNER  ' / , RX(44) / 0.01 /
      DATA      CPARAS(45) / 'XPAD    ' / , RX(45) / 1.0 /
      DATA      CPARAS(46) / 'YPAD    ' / , RX(46) / 1.0 /
      DATA      CPARAS(47) / 'TILT3   ' / , RX(47) / 0. /
      DATA      CPARAS(48) / 'ANGLE3  ' / , RX(48) / 30. /
      DATA      CPARAS(49) / 'XOFF3   ' / , RX(49) / 0. /
      DATA      CPARAS(50) / 'YOFF3   ' / , RX(50) / 0. /
      DATA      CPARAS(51) / 'XOBJ3   ' / , RX(51) / 0.5 /
      DATA      CPARAS(52) / 'YOBJ3   ' / , RX(52) / 0.5 /
      DATA      CPARAS(53) / 'ZOBJ3   ' / , RX(53) / 0. /
      DATA      CPARAS(54) / 'XEYE3   ' / , RX(54) / -0.8 /
      DATA      CPARAS(55) / 'YEYE3   ' / , RX(55) / -1.5 /
      DATA      CPARAS(56) / 'ZEYE3   ' / , RX(56) / +1.5 /
      DATA      CPARAS(57) / 'SEC3    ' / , RX(57) / 0. /
      DATA      CPARAS(58) / 'VXMIN3  ' / , RX(58) / 0. /
      DATA      CPARAS(59) / 'VXMAX3  ' / , RX(59) / 1. /
      DATA      CPARAS(60) / 'VYMIN3  ' / , RX(60) / 0. /
      DATA      CPARAS(61) / 'VYMAX3  ' / , RX(61) / 1. /
      DATA      CPARAS(62) / 'VZMIN3  ' / , RX(62) / 0. /
      DATA      CPARAS(63) / 'VZMAX3  ' / , RX(63) / 1. /
      DATA      CPARAS(64) / 'UXMIN3  ' / , RX(64) / 0. /
      DATA      CPARAS(65) / 'UXMAX3  ' / , RX(65) / 1. /
      DATA      CPARAS(66) / 'UYMIN3  ' / , RX(66) / 0. /
      DATA      CPARAS(67) / 'UYMAX3  ' / , RX(67) / 1. /
      DATA      CPARAS(68) / 'UZMIN3  ' / , RX(68) / 0. /
      DATA      CPARAS(69) / 'UZMAX3  ' / , RX(69) / 1. /
      DATA      CPARAS(70) / 'VXORG3  ' / , RX(70) / 0. /
      DATA      CPARAS(71) / 'VYORG3  ' / , RX(71) / 0. /
      DATA      CPARAS(72) / 'VZORG3  ' / , RX(72) / 0. /
      DATA      CPARAS(73) / 'SIMFAC3 ' / , RX(73) / 0.0 /

*     / LONG  NAME /

      DATA      CPARAL( 1) / 'INTERPOLATION_ANGLE_X' /
      DATA      CPARAL( 2) / 'INTERPOLATION_ANGLE_Y' /
      DATA      CPARAL( 3) / 'LINE_BIT_LENGTH' /
      DATA      CPARAL( 4) / 'SCRIPT_HEIGHT' /
      DATA      CPARAL( 5) / 'SCRIPT_SHIFT' /
      DATA      CPARAL( 6) / 'POLIMARKER_HEIGHT' /
      DATA      CPARAL( 7) / '****RFAROT  ' /
      DATA      CPARAL( 8) / 'TONE_CYCLE_LENGTH' /
      DATA      CPARAL( 9) / 'LINE_LABEL_SPACE' /
      DATA      CPARAL(10) / 'LINE_CYCLE_LENGTH' /
      DATA      CPARAL(11) / 'LINE_START_POSITION' /
      DATA      CPARAL(12) / 'RESUME_POSITION_AFTER_BUFFERING' /
      DATA      CPARAL(13) / 'LINE_LABEL_THRESHOLD_CURVATURE' /
      DATA      CPARAL(14) / 'ARROWHEAD_PROPOTION' /
      DATA      CPARAL(15) / 'ARROWHEAD_SIZE' /
      DATA      CPARAL(16) / 'ARROWHEAD_SIZE_MAP' /
      DATA      CPARAL(17) / 'ARROWHEAD_ANGLE' /
      DATA      CPARAL(18) / '++++VXMIN   ' /
      DATA      CPARAL(19) / '++++VXMAX   ' /
      DATA      CPARAL(20) / '++++VYMIN   ' /
      DATA      CPARAL(21) / '++++VYMAX   ' /
      DATA      CPARAL(22) / '++++UXMIN   ' /
      DATA      CPARAL(23) / '++++UXMAX   ' /
      DATA      CPARAL(24) / '++++UYMIN   ' /
      DATA      CPARAL(25) / '++++UYMAX   ' /
      DATA      CPARAL(26) / '++++VXOFF   ' /
      DATA      CPARAL(27) / '++++VYOFF   ' /
      DATA      CPARAL(28) / '++++PLX     ' /
      DATA      CPARAL(29) / '++++PLY     ' /
      DATA      CPARAL(30) / '++++PLROT   ' /
      DATA      CPARAL(31) / '++++SIMFAC  ' /
      DATA      CPARAL(32) / '++++STLAT1  ' /
      DATA      CPARAL(33) / '++++STLAT2  ' /
      DATA      CPARAL(34) / '++++TXMIN   ' /
      DATA      CPARAL(35) / '++++TXMAX   ' /
      DATA      CPARAL(36) / '++++TYMIN   ' /
      DATA      CPARAL(37) / '++++TYMAX   ' /
      DATA      CPARAL(38) / '++++CXMIN   ' /
      DATA      CPARAL(39) / '++++CXMAX   ' /
      DATA      CPARAL(40) / '++++CYMIN   ' /
      DATA      CPARAL(41) / '++++CYMAX   ' /
      DATA      CPARAL(42) / 'SATELLITE_ALTITUDE' /
      DATA      CPARAL(43) / '****FACTOR  ' /
      DATA      CPARAL(44) / 'CORNERMARK_LENGTH' /
      DATA      CPARAL(45) / '****XPAD    ' /
      DATA      CPARAL(46) / '****YPAD    ' /
      DATA      CPARAL(47) / '++++TILT3   ' /
      DATA      CPARAL(48) / '++++ANGLE3  ' /
      DATA      CPARAL(49) / '++++XOFF3   ' /
      DATA      CPARAL(50) / '++++YOFF3   ' /
      DATA      CPARAL(51) / '++++XOBJ3   ' /
      DATA      CPARAL(52) / '++++YOBJ3   ' /
      DATA      CPARAL(53) / '++++ZOBJ3   ' /
      DATA      CPARAL(54) / '++++XEYE3   ' /
      DATA      CPARAL(55) / '++++YEYE3   ' /
      DATA      CPARAL(56) / '++++ZEYE3   ' /
      DATA      CPARAL(57) / '++++SEC3    ' /
      DATA      CPARAL(58) / '++++VXMIN3  ' /
      DATA      CPARAL(59) / '++++VXMAX3  ' /
      DATA      CPARAL(60) / '++++VYMIN3  ' /
      DATA      CPARAL(61) / '++++VYMAX3  ' /
      DATA      CPARAL(62) / '++++VZMIN3  ' /
      DATA      CPARAL(63) / '++++VZMAX3  ' /
      DATA      CPARAL(64) / '++++UXMIN3  ' /
      DATA      CPARAL(65) / '++++UXMAX3  ' /
      DATA      CPARAL(66) / '++++UYMIN3  ' /
      DATA      CPARAL(67) / '++++UYMAX3  ' /
      DATA      CPARAL(68) / '++++UZMIN3  ' /
      DATA      CPARAL(69) / '++++UZMAX3  ' /
      DATA      CPARAL(70) / '++++VXORG3  ' /
      DATA      CPARAL(71) / '++++VYORG3  ' /
      DATA      CPARAL(72) / '++++VZORG3  ' /
      DATA      CPARAL(73) / '++++SIMFAC3 ' /

      DATA      LFIRST / .TRUE. /


      NCP = NPARA

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGRQID(CP, IDX)

      DO 10 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IDX = N
          RETURN
        END IF
   10 CONTINUE
      CMSG = 'PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E','SGRQID',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGRQCP(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAS(IDX)
      ELSE
        CALL MSGDMP('E','SGRQCP','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGRQCL(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAL(IDX)
      ELSE
        CALL MSGDMP('E','SGRQCL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGRQVL(IDX, RPARA)

      IF (LFIRST) THEN
        CALL RTRGET('SG', CPARAS, RX, NPARA)
        CALL RLRGET(CPARAL, RX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        RPARA = RX(IDX)
      ELSE
        CALL MSGDMP('E','SGRQVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGRSVL(IDX, RPARA)

      IF (LFIRST) THEN
        CALL RTRGET('SG', CPARAS, RX, NPARA)
        CALL RLRGET(CPARAL, RX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        RX(IDX) = RPARA
      ELSE
        CALL MSGDMP('E','SGRSVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGRQIN(CP, IN)

      DO 20 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IN = N
          RETURN
        END IF
   20 CONTINUE

      IN = 0

      RETURN
      END
