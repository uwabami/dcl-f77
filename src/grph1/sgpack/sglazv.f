*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGLAZV(VX1,VY1,VX2,VY2,ITYPE,INDEX)


      IF (ITYPE.EQ.0) THEN
        CALL MSGDMP('M','SGLAZV','LINETYPE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.EQ.0) THEN
        CALL MSGDMP('M','SGLAZV','LINE INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.LT.0) THEN
        CALL MSGDMP('E','SGLAZV','LINE INDEX IS INVALID.')
      END IF

      CALL SZLAOP(ITYPE,INDEX)
      CALL SZLAZV(VX1,VY1,VX2,VY2)
      CALL SZLACL

      END
