*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGLNZV(VX1,VY1,VX2,VY2,INDEX)


      IF (INDEX.EQ.0) THEN
        CALL MSGDMP('M','SGLNZV','LINE INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.LT.0) THEN
        CALL MSGDMP('E','SGLNZV','LINE INDEX IS LESS THAN 0.')
      END IF

      CALL SZLNOP(INDEX)
      CALL SZLNZV(VX1,VY1,VX2,VY2)
      CALL SZLNCL

      END
