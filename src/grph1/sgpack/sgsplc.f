*-----------------------------------------------------------------------
*     CHARACTER SETTING FOR 'LCHAR'=.TRUE.
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGSPLC(CHARX)

      CHARACTER CHARX*(*)

      PARAMETER (MAXLEN=32)

      CHARACTER CHARXZ*(MAXLEN),CSGI*1

      SAVE

      EXTERNAL  LENC,CSGI

      DATA      CHARXZ/'A'/, RSIZEZ/0.02/


      NC=LENC(CHARX)
      IF (NC.GE.MAXLEN) THEN
        CALL MSGDMP('E','SGSPLC','TEXT LENGTH IS TOO LONG.')
      END IF

      CHARXZ=CHARX
      CALL SZSCHZ(CHARXZ,RSIZEZ)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGQPLC(CHARX)

      CHARX=CHARXZ

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGSPLS(RSIZE)

      IF (RSIZE.EQ.0) THEN
        CALL MSGDMP('M','SGSPLS','TEXT HEIGHT IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (RSIZE.LT.0) THEN
        CALL MSGDMP('E','SGSPLS','TEXT HEIGHT IS LESS THAN 0.')
      END IF

      RSIZEZ=RSIZE
      CALL SZSCHZ(CHARXZ,RSIZEZ)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGQPLS(RSIZE)

      RSIZE=RSIZEZ

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGNPLC

      NC=LENC(CHARXZ)
      CHARXZ(NC:NC)=CSGI(ISGC(CHARXZ(NC:NC))+1)
      CALL SZSCHZ(CHARXZ,RSIZEZ)

      RETURN
      END
