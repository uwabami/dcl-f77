*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGLNZU(UX1,UY1,UX2,UY2,INDEX)


      IF (INDEX.EQ.0) THEN
        CALL MSGDMP('M','SGLNZU','LINE INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.LT.0) THEN
        CALL MSGDMP('E','SGLNZU','LINE INDEX IS LESS THAN 0.')
      END IF

      CALL SZLNOP(INDEX)
      CALL SZLNZU(UX1,UY1,UX2,UY2)
      CALL SZLNCL

      END
