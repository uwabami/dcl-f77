*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGCLST(ccfile)

      parameter (maxcli = 256)

      character ccfile*(*)
      character cmsg*64

      INTEGER ir(maxcli),ig(maxcli),ib(maxcli)

      iu = iufopn()
      open(iu, file=ccfile)
      read(iu, '(i3)') max_pal
      if (max_pal.gt. maxcli) then
        max_pal = maxcli
        cmsg='color numbers greater than xx are ignored.'
        write(cmsg(28:29),'(i2)') nplmax
        call msgdmp('w', 'zmdopn', cmsg)
      endif

      ns = 2**8
      do i=1, max_pal
        read(iu, '(3i6)') ir0, ig0, ib0
        ir(i) = ir0 / ns
        ig(i) = ig0 / ns
        ib(i) = ib0 / ns
      end do

      close(iu)

      RETURN

*------------------------------------------------------------------------
      ENTRY SGQCL(NUM,NR,NG,NB)

      NR=IR(NUM+1)
      NG=IG(NUM+1)
      NB=IB(NUM+1)

      RETURN

      END
