*-----------------------------------------------------------------------
*     SGRGET / SGRSET / SGRSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGRGET(CP, RPARA)

      CHARACTER CP*(*)

      CHARACTER CX*8
      CHARACTER CL*40


      CALL SGRQID(CP, IDX)
      CALL SGRQVL(IDX, RPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGRSET(CP, RPARA)

      CALL SGRQID(CP, IDX)
      CALL SGRSVL(IDX, RPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGRSTX(CP, RPARA)

      RP = RPARA
      CALL SGRQID(CP, IDX)

*     / SHORT NAME /

      CALL SGRQCP(IDX, CX)
      CALL RTRGET('SG', CX, RP, 1)

*     / LONG NAME /

      CALL SGRQCL(IDX, CL)
      CALL RLRGET(CL, RP, 1)

      CALL SGRSVL(IDX,RP)

      RETURN
      END
