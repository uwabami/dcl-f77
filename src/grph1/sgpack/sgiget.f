*-----------------------------------------------------------------------
*     SGIGET / SGISET / SGISTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGIGET(CP, IPARA)

      CHARACTER CP*(*)

      CHARACTER CX*8
      CHARACTER CL*40


      CALL SGIQID(CP, IDX)
      CALL SGIQVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGISET(CP, IPARA)

      CALL SGIQID(CP, IDX)
      CALL SGISVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGISTX(CP, IPARA)

      IP = IPARA
      CALL SGIQID(CP, IDX)

*     / SHORT NAME /

      CALL SGIQCP(IDX, CX)
      CALL RTIGET('SG', CX, IP, 1)

*     / LONG NAME /

      CALL SGIQCL(IDX, CL)
      CALL RLIGET(CL, IP, 1)

      CALL SGISVL(IDX,IP)

      RETURN
      END
