*-----------------------------------------------------------------------
*     PARAMETER CONTROL (GENERIC)
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGPQNP(NCP)

      CHARACTER CP*(*)

      PARAMETER (NPARA = 122)

      INTEGER   ITYPE(NPARA)
      CHARACTER CPARAS(NPARA)*8
      CHARACTER CPARAL(NPARA)*40
      CHARACTER CMSG*80

      LOGICAL   LCHREQ

      EXTERNAL  LCHREQ,LENC

      SAVE

*     / SHORT NAME /

      DATA      CPARAS(  1)/'RDX     '/, ITYPE(  1) / 3 /
      DATA      CPARAS(  2)/'RDY     '/, ITYPE(  2) / 3 /
      DATA      CPARAS(  3)/'LCLIP   '/, ITYPE(  3) / 2 /
      DATA      CPARAS(  4)/'LCHAR   '/, ITYPE(  4) / 2 /
      DATA      CPARAS(  5)/'LLNINT  '/, ITYPE(  5) / 2 /
      DATA      CPARAS(  6)/'LGCINT  '/, ITYPE(  6) / 2 /
      DATA      CPARAS(  7)/'LSOFTF  '/, ITYPE(  7) / 2 /
      DATA      CPARAS(  8)/'LCL2TN  '/, ITYPE(  8) / 2 /
      DATA      CPARAS(  9)/'IRMODE  '/, ITYPE(  9) / 1 /
      DATA      CPARAS( 10)/'BITLEN  '/, ITYPE( 10) / 3 /
      DATA      CPARAS( 11)/'MOVE    '/, ITYPE( 11) / 1 /
      DATA      CPARAS( 12)/'NBITS   '/, ITYPE( 12) / 1 /
      DATA      CPARAS( 13)/'LCNTL   '/, ITYPE( 13) / 2 /
      DATA      CPARAS( 14)/'SMALL   '/, ITYPE( 14) / 3 /
      DATA      CPARAS( 15)/'SHIFT   '/, ITYPE( 15) / 3 /
      DATA      CPARAS( 16)/'ISUP    '/, ITYPE( 16) / 1 /
      DATA      CPARAS( 17)/'ISUB    '/, ITYPE( 17) / 1 /
      DATA      CPARAS( 18)/'IRST    '/, ITYPE( 18) / 1 /
      DATA      CPARAS( 19)/'LFULL   '/, ITYPE( 19) / 2 /
      DATA      CPARAS( 20)/'PMFACT  '/, ITYPE( 20) / 3 /
      DATA      CPARAS( 21)/'NPMSKIP '/, ITYPE( 21) / 1 /
      DATA      CPARAS( 22)/'RFAROT  '/, ITYPE( 22) / 3 /
      DATA      CPARAS( 23)/'TNBLEN  '/, ITYPE( 23) / 3 /
      DATA      CPARAS( 24)/'IFONT   '/, ITYPE( 24) / 1 /
      DATA      CPARAS( 25)/'ICLRMAP '/, ITYPE( 25) / 1 /
      DATA      CPARAS( 26)/'LFPROP  '/, ITYPE( 26) / 2 /
      DATA      CPARAS( 27)/'LROT    '/, ITYPE( 27) / 2 /
      DATA      CPARAS( 28)/'IROT    '/, ITYPE( 28) / 1 /
      DATA      CPARAS( 29)/'FWC     '/, ITYPE( 29) / 3 /
      DATA      CPARAS( 30)/'CWL     '/, ITYPE( 30) / 3 /
      DATA      CPARAS( 31)/'FFCT    '/, ITYPE( 31) / 3 /
      DATA      CPARAS( 32)/'INDEXC  '/, ITYPE( 32) / 1 /
      DATA      CPARAS( 33)/'LBUFF   '/, ITYPE( 33) / 2 /
      DATA      CPARAS( 34)/'NBUFF   '/, ITYPE( 34) / 1 /
      DATA      CPARAS( 35)/'RBUFF   '/, ITYPE( 35) / 3 /
      DATA      CPARAS( 36)/'LCURV   '/, ITYPE( 36) / 2 /
      DATA      CPARAS( 37)/'RCURV   '/, ITYPE( 37) / 3 /
      DATA      CPARAS( 38)/'LPROP   '/, ITYPE( 38) / 2 /
      DATA      CPARAS( 39)/'LUARW   '/, ITYPE( 39) / 2 /
      DATA      CPARAS( 40)/'AFACT   '/, ITYPE( 40) / 3 /
      DATA      CPARAS( 41)/'CONST   '/, ITYPE( 41) / 3 /
      DATA      CPARAS( 42)/'CONSTM  '/, ITYPE( 42) / 3 /
      DATA      CPARAS( 43)/'ANGLE   '/, ITYPE( 43) / 3 /
      DATA      CPARAS( 44)/'LARROW  '/, ITYPE( 44) / 2 /
      DATA      CPARAS( 45)/'LATONE  '/, ITYPE( 45) / 2 /
      DATA      CPARAS( 46)/'IATONE  '/, ITYPE( 46) / 1 /
      DATA      CPARAS( 47)/'IWS     '/, ITYPE( 47) / 0 /
      DATA      CPARAS( 48)/'ITR     '/, ITYPE( 48) / 1 /
      DATA      CPARAS( 49)/'IBGCLI  '/, ITYPE( 49) / 1 /
      DATA      CPARAS( 50)/'VXMIN   '/, ITYPE( 50) / 3 /
      DATA      CPARAS( 51)/'VXMAX   '/, ITYPE( 51) / 3 /
      DATA      CPARAS( 52)/'VYMIN   '/, ITYPE( 52) / 3 /
      DATA      CPARAS( 53)/'VYMAX   '/, ITYPE( 53) / 3 /
      DATA      CPARAS( 54)/'UXMIN   '/, ITYPE( 54) / 3 /
      DATA      CPARAS( 55)/'UXMAX   '/, ITYPE( 55) / 3 /
      DATA      CPARAS( 56)/'UYMIN   '/, ITYPE( 56) / 3 /
      DATA      CPARAS( 57)/'UYMAX   '/, ITYPE( 57) / 3 /
      DATA      CPARAS( 58)/'VXOFF   '/, ITYPE( 58) / 3 /
      DATA      CPARAS( 59)/'VYOFF   '/, ITYPE( 59) / 3 /
      DATA      CPARAS( 60)/'PLX     '/, ITYPE( 60) / 3 /
      DATA      CPARAS( 61)/'PLY     '/, ITYPE( 61) / 3 /
      DATA      CPARAS( 62)/'PLROT   '/, ITYPE( 62) / 3 /
      DATA      CPARAS( 63)/'SIMFAC  '/, ITYPE( 63) / 3 /
      DATA      CPARAS( 64)/'STLAT1  '/, ITYPE( 64) / 3 /
      DATA      CPARAS( 65)/'STLAT2  '/, ITYPE( 65) / 3 /
      DATA      CPARAS( 66)/'TXMIN   '/, ITYPE( 66) / 3 /
      DATA      CPARAS( 67)/'TXMAX   '/, ITYPE( 67) / 3 /
      DATA      CPARAS( 68)/'TYMIN   '/, ITYPE( 68) / 3 /
      DATA      CPARAS( 69)/'TYMAX   '/, ITYPE( 69) / 3 /
      DATA      CPARAS( 70)/'CXMIN   '/, ITYPE( 70) / 3 /
      DATA      CPARAS( 71)/'CXMAX   '/, ITYPE( 71) / 3 /
      DATA      CPARAS( 72)/'CYMIN   '/, ITYPE( 72) / 3 /
      DATA      CPARAS( 73)/'CYMAX   '/, ITYPE( 73) / 3 /
      DATA      CPARAS( 74)/'RSAT    '/, ITYPE( 74) / 3 /
      DATA      CPARAS( 75)/'LDEG    '/, ITYPE( 75) / 2 /
      DATA      CPARAS( 76)/'NFRAME  '/, ITYPE( 76) / 1 /
      DATA      CPARAS( 77)/'NPAGE   '/, ITYPE( 77) / 1 /
      DATA      CPARAS( 78)/'NLEVEL  '/, ITYPE( 78) / 1 /
      DATA      CPARAS( 79)/'FACTOR  '/, ITYPE( 79) / 3 /
      DATA      CPARAS( 80)/'INDEX   '/, ITYPE( 80) / 1 /
      DATA      CPARAS( 81)/'LCORNER '/, ITYPE( 81) / 2 /
      DATA      CPARAS( 82)/'LTITLE  '/, ITYPE( 82) / 2 /
      DATA      CPARAS( 83)/'CORNER  '/, ITYPE( 83) / 3 /
      DATA      CPARAS( 84)/'XPAD    '/, ITYPE( 84) / 3 /
      DATA      CPARAS( 85)/'YPAD    '/, ITYPE( 85) / 3 /
      DATA      CPARAS( 86)/'LWIDE   '/, ITYPE( 86) / 2 /
      DATA      CPARAS( 87)/'ITR3    '/, ITYPE( 87) / 1 /
      DATA      CPARAS( 88)/'TILT3   '/, ITYPE( 88) / 3 /
      DATA      CPARAS( 89)/'ANGLE3  '/, ITYPE( 89) / 3 /
      DATA      CPARAS( 90)/'XOFF3   '/, ITYPE( 90) / 3 /
      DATA      CPARAS( 91)/'YOFF3   '/, ITYPE( 91) / 3 /
      DATA      CPARAS( 92)/'XOBJ3   '/, ITYPE( 92) / 3 /
      DATA      CPARAS( 93)/'YOBJ3   '/, ITYPE( 93) / 3 /
      DATA      CPARAS( 94)/'ZOBJ3   '/, ITYPE( 94) / 3 /
      DATA      CPARAS( 95)/'XEYE3   '/, ITYPE( 95) / 3 /
      DATA      CPARAS( 96)/'YEYE3   '/, ITYPE( 96) / 3 /
      DATA      CPARAS( 97)/'ZEYE3   '/, ITYPE( 97) / 3 /
      DATA      CPARAS( 98)/'IXC3    '/, ITYPE( 98) / 1 /
      DATA      CPARAS( 99)/'IYC3    '/, ITYPE( 99) / 1 /
      DATA      CPARAS(100)/'SEC3    '/, ITYPE(100) / 3 /
      DATA      CPARAS(101)/'LFGBG   '/, ITYPE(101) / 2 /
      DATA      CPARAS(102)/'L2TO3   '/, ITYPE(102) / 2 /
      DATA      CPARAS(103)/'VXMIN3  '/, ITYPE(103) / 3 /
      DATA      CPARAS(104)/'VXMAX3  '/, ITYPE(104) / 3 /
      DATA      CPARAS(105)/'VYMIN3  '/, ITYPE(105) / 3 /
      DATA      CPARAS(106)/'VYMAX3  '/, ITYPE(106) / 3 /
      DATA      CPARAS(107)/'VZMIN3  '/, ITYPE(107) / 3 /
      DATA      CPARAS(108)/'VZMAX3  '/, ITYPE(108) / 3 /
      DATA      CPARAS(109)/'UXMIN3  '/, ITYPE(109) / 3 /
      DATA      CPARAS(110)/'UXMAX3  '/, ITYPE(110) / 3 /
      DATA      CPARAS(111)/'UYMIN3  '/, ITYPE(111) / 3 /
      DATA      CPARAS(112)/'UYMAX3  '/, ITYPE(112) / 3 /
      DATA      CPARAS(113)/'UZMIN3  '/, ITYPE(113) / 3 /
      DATA      CPARAS(114)/'UZMAX3  '/, ITYPE(114) / 3 /
      DATA      CPARAS(115)/'VXORG3  '/, ITYPE(115) / 3 /
      DATA      CPARAS(116)/'VYORG3  '/, ITYPE(116) / 3 /
      DATA      CPARAS(117)/'VZORG3  '/, ITYPE(117) / 3 /
      DATA      CPARAS(118)/'SIMFAC3 '/, ITYPE(118) / 3 /
      DATA      CPARAS(119)/'LXLOG3  '/, ITYPE(119) / 2 /
      DATA      CPARAS(120)/'LYLOG3  '/, ITYPE(120) / 2 /
      DATA      CPARAS(121)/'LZLOG3  '/, ITYPE(121) / 2 /
      DATA      CPARAS(122)/'LMRKFNT '/, ITYPE(122) / 2 /

*     / LONG  NAME /

      DATA      CPARAL(  1) / 'INTERPOLATION_ANGLE_X' /
      DATA      CPARAL(  2) / 'INTERPOLATION_ANGLE_Y' /
      DATA      CPARAL(  3) / 'ENABLE_CLIPPING' /
      DATA      CPARAL(  4) / 'ENABLE_LINE_LABELING' /
      DATA      CPARAL(  5) / 'ENABLE_LINEAR_INTERPOLATION' /
      DATA      CPARAL(  6) / 'ENABLE_GREATCIRCLE_INTERPOLATION' /
      DATA      CPARAL(  7) / 'ENABLE_SOFTFILL' /
      DATA      CPARAL(  8) / 'ENABLE_COLOR_SUBSTITUTION' /
      DATA      CPARAL(  9) / 'BOUNDARY_DIRECTION' /
      DATA      CPARAL( 10) / 'LINE_BIT_LENGTH' /
      DATA      CPARAL( 11) / 'LINE_RESUME_MODE' /
      DATA      CPARAL( 12) / 'PATTERN_BIT_LENGTH' /
      DATA      CPARAL( 13) / 'ENABLE_CONTROL_CHAR' /
      DATA      CPARAL( 14) / 'SCRIPT_HEIGHT' /
      DATA      CPARAL( 15) / 'SCRIPT_SHIFT' /
      DATA      CPARAL( 16) / 'BEGIN_SUPERSCRIPT' /
      DATA      CPARAL( 17) / 'BEGIN_SUBSCRIPT' /
      DATA      CPARAL( 18) / 'END_SCRIPT' /
      DATA      CPARAL( 19) / 'USE_FULL_WINDOW' /
      DATA      CPARAL( 20) / 'POLIMARKER_HEIGHT' /
      DATA      CPARAL( 21) / 'POLIMARKER_INTERVAL' /
      DATA      CPARAL( 22) / '****RFAROT  ' /
      DATA      CPARAL( 23) / 'TONE_CYCLE_LENGTH' /
      DATA      CPARAL( 24) / 'FONT_NUMBER' /
      DATA      CPARAL( 25) / 'COLORMAP_NUMBER' /
      DATA      CPARAL( 26) / 'ENABLE_PROPORTINAL_FONT' /
      DATA      CPARAL( 27) / 'ENABLE_LINE_LABEL_ROTATION' /
      DATA      CPARAL( 28) / 'LINE_LABEL_ROTATION_ANGLE' /
      DATA      CPARAL( 29) / 'LINE_LABEL_SPACE' /
      DATA      CPARAL( 30) / 'LINE_CYCLE_LENGTH' /
      DATA      CPARAL( 31) / 'LINE_START_POSITION' /
      DATA      CPARAL( 32) / 'LABEL_CHAR_INDEX' /
      DATA      CPARAL( 33) / 'ENABLE_LINE_BUFFERING' /
      DATA      CPARAL( 34) / 'LINE_BUFFERING_LENGTH' /
      DATA      CPARAL( 35) / 'RESUME_POSITION_AFTER_BUFFERING' /
      DATA      CPARAL( 36) / 'DISABLE_LINE_LABEL_AT_CURVE' /
      DATA      CPARAL( 37) / 'LINE_LABEL_THRESHOLD_CURVATURE' /
      DATA      CPARAL( 38) / 'FIX_ARROWHEAD_PROPORTION' /
      DATA      CPARAL( 39) / 'ENABLE_ARROWHEAD_MAPPING' /
      DATA      CPARAL( 40) / 'ARROWHEAD_PROPOTION' /
      DATA      CPARAL( 41) / 'ARROWHEAD_SIZE' /
      DATA      CPARAL( 42) / 'ARROWHEAD_SIZE_MAP' /
      DATA      CPARAL( 43) / 'ARROWHEAD_ANGLE' /
      DATA      CPARAL( 44) / '----LARROW  ' /
      DATA      CPARAL( 45) / 'ENABLE_ARROWHEAD_SHADE' /
      DATA      CPARAL( 46) / 'ARROWHEAD_SHADE_PATTERN' /
      DATA      CPARAL( 47) / '++++IWS     ' /
      DATA      CPARAL( 48) / '++++ITR     ' /
      DATA      CPARAL( 49) / 'BACKGROUND_COLOR_INDEX' /
      DATA      CPARAL( 50) / '++++VXMIN   ' /
      DATA      CPARAL( 51) / '++++VXMAX   ' /
      DATA      CPARAL( 52) / '++++VYMIN   ' /
      DATA      CPARAL( 53) / '++++VYMAX   ' /
      DATA      CPARAL( 54) / '++++UXMIN   ' /
      DATA      CPARAL( 55) / '++++UXMAX   ' /
      DATA      CPARAL( 56) / '++++UYMIN   ' /
      DATA      CPARAL( 57) / '++++UYMAX   ' /
      DATA      CPARAL( 58) / '++++VXOFF   ' /
      DATA      CPARAL( 59) / '++++VYOFF   ' /
      DATA      CPARAL( 60) / '++++PLX     ' /
      DATA      CPARAL( 61) / '++++PLY     ' /
      DATA      CPARAL( 62) / '++++PLROT   ' /
      DATA      CPARAL( 63) / '++++SIMFAC  ' /
      DATA      CPARAL( 64) / '++++STLAT1  ' /
      DATA      CPARAL( 65) / '++++STLAT2  ' /
      DATA      CPARAL( 66) / '++++TXMIN   ' /
      DATA      CPARAL( 67) / '++++TXMAX   ' /
      DATA      CPARAL( 68) / '++++TYMIN   ' /
      DATA      CPARAL( 69) / '++++TYMAX   ' /
      DATA      CPARAL( 70) / '++++CXMIN   ' /
      DATA      CPARAL( 71) / '++++CXMAX   ' /
      DATA      CPARAL( 72) / '++++CYMIN   ' /
      DATA      CPARAL( 73) / '++++CYMAX   ' /
      DATA      CPARAL( 74) / 'SATELLITE_ALTITUDE' /
      DATA      CPARAL( 75) / 'USE_DEGREE' /
      DATA      CPARAL( 76) / '****NFRAME  ' /
      DATA      CPARAL( 77) / '****NPAGE   ' /
      DATA      CPARAL( 78) / '****NLEVEL  ' /
      DATA      CPARAL( 79) / '****FACTOR  ' /
      DATA      CPARAL( 80) / 'CORNERMARK_INDEX' /
      DATA      CPARAL( 81) / 'DRAW_CORNERMARK' /
      DATA      CPARAL( 82) / 'DRAW_PAGE_TITLE' /
      DATA      CPARAL( 83) / 'CORNERMARK_LENGTH' /
      DATA      CPARAL( 84) / '****XPAD    ' /
      DATA      CPARAL( 85) / '****YPAD    ' /
      DATA      CPARAL( 86) / '****LWIDE   ' /
      DATA      CPARAL( 87) / '++++ITR3    ' /
      DATA      CPARAL( 88) / '++++TILT3   ' /
      DATA      CPARAL( 89) / '++++ANGLE3  ' /
      DATA      CPARAL( 90) / '++++XOFF3   ' /
      DATA      CPARAL( 91) / '++++YOFF3   ' /
      DATA      CPARAL( 92) / '++++XOBJ3   ' /
      DATA      CPARAL( 93) / '++++YOBJ3   ' /
      DATA      CPARAL( 94) / '++++ZOBJ3   ' /
      DATA      CPARAL( 95) / '++++XEYE3   ' /
      DATA      CPARAL( 96) / '++++YEYE3   ' /
      DATA      CPARAL( 97) / '++++ZEYE3   ' /
      DATA      CPARAL( 98) / '++++IXC3    ' /
      DATA      CPARAL( 99) / '++++IYC3    ' /
      DATA      CPARAL(100) / '++++SEC3    ' /
      DATA      CPARAL(101) / '****LFGBG   ' /
      DATA      CPARAL(102) / '****L2TO3   ' /
      DATA      CPARAL(103) / '++++VXMIN3  ' /
      DATA      CPARAL(104) / '++++VXMAX3  ' /
      DATA      CPARAL(105) / '++++VYMIN3  ' /
      DATA      CPARAL(106) / '++++VYMAX3  ' /
      DATA      CPARAL(107) / '++++VZMIN3  ' /
      DATA      CPARAL(108) / '++++VZMAX3  ' /
      DATA      CPARAL(109) / '++++UXMIN3  ' /
      DATA      CPARAL(110) / '++++UXMAX3  ' /
      DATA      CPARAL(111) / '++++UYMIN3  ' /
      DATA      CPARAL(112) / '++++UYMAX3  ' /
      DATA      CPARAL(113) / '++++UZMIN3  ' /
      DATA      CPARAL(114) / '++++UZMAX3  ' /
      DATA      CPARAL(115) / '++++VXORG3  ' /
      DATA      CPARAL(116) / '++++VYORG3  ' /
      DATA      CPARAL(117) / '++++VZORG3  ' /
      DATA      CPARAL(118) / '++++SIMFAC3 ' /
      DATA      CPARAL(119) / '++++LXLOG3  ' /
      DATA      CPARAL(120) / '++++LYLOG3  ' /
      DATA      CPARAL(121) / '++++LZLOG3  ' /
      DATA      CPARAL(122) / 'USE_DCLMARKER_FONT ' /


      NCP = NPARA

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGPQID(CP, IDX)

      DO 10 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IDX = N
          RETURN
        END IF
   10 CONTINUE
      CMSG = 'PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E','SGPQID',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGPQCP(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAS(IDX)
      ELSE
        CALL MSGDMP('E','SGPQCP','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGPQCL(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAL(IDX)
      ELSE
        CALL MSGDMP('E','SGPQCL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGPQIT(IDX, ITP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        ITP = ITYPE(IDX)
      ELSE
        CALL MSGDMP('E','SGPQIT','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGPQVL(IDX, IPARA)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        IF (ITYPE(IDX) .EQ. 1) THEN
          CALL SGIQID(CPARAS(IDX), ID)
          CALL SGIQVL(ID, IPARA)
        ELSE IF (ITYPE(IDX) .EQ. 2) THEN
          CALL SGLQID(CPARAS(IDX), ID)
          CALL SGLQVL(ID, IPARA)
        ELSE IF (ITYPE(IDX) .EQ. 3) THEN
          CALL SGRQID(CPARAS(IDX), ID)
          CALL SGRQVL(ID, IPARA)
        END IF
      ELSE
        CALL MSGDMP('E','SGPQVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGPSVL(IDX, IPARA)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        IF (ITYPE(IDX) .EQ. 1) THEN
          CALL SGIQID(CPARAS(IDX), ID)
          CALL SGISVL(ID, IPARA)
        ELSE IF (ITYPE(IDX) .EQ. 2) THEN
          CALL SGLQID(CPARAS(IDX), ID)
          CALL SGLSVL(ID, IPARA)
        ELSE IF (ITYPE(IDX) .EQ. 3) THEN
          CALL SGRQID(CPARAS(IDX), ID)
          CALL SGRSVL(ID, IPARA)
        END IF
      ELSE
        CALL MSGDMP('E','SGPSVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGPQIN(CP, IN)

      DO 20 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IN = N
          RETURN
        END IF
   20 CONTINUE

      IN = 0

      RETURN
      END
