*-----------------------------------------------------------------------
      FUNCTION ISGRGB(IR, IG, IB)
      INTEGER ISGRGB

      ISGRGB = IOR(ISHFT(IR,16), IOR(ISHFT(IG,8), IB))

      END
