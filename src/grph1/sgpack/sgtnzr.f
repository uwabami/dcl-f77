*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGTNZR(N,RPX,RPY,ITPAT)

      REAL      RPX(*),RPY(*)


      IF (N.LT.3) THEN
        CALL MSGDMP('E','SGTNZR','NUMBER OF POINTS IS LESS THAN 3.')
      END IF
      IF (ITPAT.EQ.0) THEN
        CALL MSGDMP('M','SGTNZR','TONE PAT. INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (ITPAT.LT.0) THEN
        CALL MSGDMP('E','SGTNZR','TONE PAT. INDEX IS LESS THAN 0.')
      END IF

      CALL SZTNOP(ITPAT)
      CALL SZTNZR(N,RPX,RPY)
      CALL SZTNCL

      END
