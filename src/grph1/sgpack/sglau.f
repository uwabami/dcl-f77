*-----------------------------------------------------------------------
*     ARROW SUBPRIMITIVE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGLAU(UX1,UY1,UX2,UY2)

      SAVE

      DATA      ITYPEZ/1/,INDEXZ/1/


      IF (ITYPEZ.EQ.0) THEN
        CALL MSGDMP('M','SGLAU','LINETYPE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEXZ.EQ.0) THEN
        CALL MSGDMP('M','SGLAU','LINE INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEXZ.LT.0) THEN
        CALL MSGDMP('E','SGLAU','LINE INDEX IS INVALID.')
      END IF

      CALL SZLAOP(ITYPEZ,INDEXZ)
      CALL SZLAZU(UX1,UY1,UX2,UY2)
      CALL SZLACL

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGLAV(VX1,VY1,VX2,VY2)

      IF (ITYPEZ.EQ.0) THEN
        CALL MSGDMP('M','SGLAV','LINETYPE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEXZ.EQ.0) THEN
        CALL MSGDMP('M','SGLAV','LINE INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEXZ.LT.0) THEN
        CALL MSGDMP('E','SGLAV','LINE INDEX IS INVALID.')
      END IF

      CALL SZLAOP(ITYPEZ,INDEXZ)
      CALL SZLAZV(VX1,VY1,VX2,VY2)
      CALL SZLACL

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGLAR(RX1,RY1,RX2,RY2)

      IF (ITYPEZ.EQ.0) THEN
        CALL MSGDMP('M','SGLAR','LINETYPE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEXZ.EQ.0) THEN
        CALL MSGDMP('M','SGLAR','LINE INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEXZ.LT.0) THEN
        CALL MSGDMP('E','SGLAR','LINE INDEX IS INVALID.')
      END IF

      CALL SZLAOP(ITYPEZ,INDEXZ)
      CALL SZLAZR(RX1,RY1,RX2,RY2)
      CALL SZLACL

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGSLAT(ITYPE)

      ITYPEZ=ITYPE

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGQLAT(ITYPE)

      ITYPE=ITYPEZ

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGSLAI(INDEX)

      INDEXZ=INDEX

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGQLAI(INDEX)

      INDEX=INDEXZ

      RETURN
      END
