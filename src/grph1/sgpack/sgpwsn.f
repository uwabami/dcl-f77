*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGPWSN

      CHARACTER LINE*100,CWSN*8,CWNM*8

      EXTERNAL  LENC


      CALL GLIGET('MSGUNIT',IU)
      CALL SWIGET('MAXWNU',MAXWNU)

      N=0
      LINE=' '
      DO 10 I=1,MAXWNU
        WRITE(CWNM,'(A6,I2.2)') 'WSNAME',I
        CALL SWCGET(CWNM,CWSN)
        NC=LENC(CWSN)
        IF (I.LT.10) THEN
          NX=4+NC
          WRITE(LINE(N+1:N+NX),'(TR1,I1,A1,A,A1)') I,':',CWSN(1:NC),','
        ELSE
          NX=5+NC
          WRITE(LINE(N+1:N+NX),'(TR1,I2,A1,A,A1)') I,':',CWSN(1:NC),','
        END IF
        N=N+NX
   10 CONTINUE
      WRITE(LINE(N:N+1),'(A2)') ' ;'
      WRITE(IU,'(A)') LINE(1:N+1)

      END
