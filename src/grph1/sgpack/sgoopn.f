*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGOOPN(CPRC, COM)

      CHARACTER CPRC*(*), COM*(*)


      CALL PRCOPN(CPRC)
      CALL SWOOPN(CPRC, COM)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGOCLS(CPRC)

      CALL PRCCLS(CPRC)
      CALL SWOCLS(CPRC)

      RETURN
      END
