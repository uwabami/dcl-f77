*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGTNZU(N,UPX,UPY,ITPAT)

      REAL      UPX(*),UPY(*)

      IF (N.LT.3) THEN
        CALL MSGDMP('E','SGTNZU','NUMBER OF POINTS IS LESS THAN 3.')
      END IF
      IF (ITPAT.EQ.0) THEN
        CALL MSGDMP('M','SGTNZU','TONE PAT. INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (ITPAT.LT.0) THEN
        CALL MSGDMP('E','SGTNZU','TONE PAT. INDEX IS LESS THAN 0.')
      END IF

      CALL SZTNOP(ITPAT)
      CALL SZTNZU(N,UPX,UPY)
      CALL SZTNCL

      END
