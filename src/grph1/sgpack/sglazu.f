*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGLAZU(UX1,UY1,UX2,UY2,ITYPE,INDEX)


      IF (ITYPE.EQ.0) THEN
        CALL MSGDMP('M','SGLAZU','LINETYPE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.EQ.0) THEN
        CALL MSGDMP('M','SGLAZU','LINE INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.LT.0) THEN
        CALL MSGDMP('E','SGLAZU','LINE INDEX IS INVALID.')
      END IF

      CALL SZLAOP(ITYPE,INDEX)
      CALL SZLAZU(UX1,UY1,UX2,UY2)
      CALL SZLACL

      END
