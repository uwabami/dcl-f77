*-----------------------------------------------------------------------
*     SGPGET / SGPSET / SGPSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGPGET(CP, IPARA)

      CHARACTER CP*(*)

      CHARACTER CX*8, CL*40


      CALL SGPQID(CP, IDX)
      CALL SGPQVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGPSET(CP, IPARA)

      CALL SGPQID(CP, IDX)
      CALL SGPSVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGPSTX(CP, IPARA)

      IP = IPARA
      CALL SGPQID(CP, IDX)
      CALL SGPQIT(IDX, IT)
      CALL SGPQCP(IDX, CX)
      CALL SGPQCL(IDX, CL)
      IF (IT .EQ. 1) THEN
        CALL RTIGET('SG', CX, IP, 1)
        CALL RLIGET(CL, IP, 1)
        CALL SGIQID(CP, IDX)
        CALL SGISVL(IDX, IP)
      ELSE IF (IT .EQ. 2) THEN
        CALL RTLGET('SG', CX, IP, 1)
        CALL RLLGET(CL, IP, 1)
        CALL SGLQID(CP, IDX)
        CALL SGLSVL(IDX, IP)
      ELSE IF (IT .EQ. 3) THEN
        CALL RTRGET('SG', CX, IP, 1)
        CALL RLRGET(CL, IP, 1)
        CALL SGRQID(CP, IDX)
        CALL SGRSVL(IDX, IP)
      ENDIF

      RETURN
      END
