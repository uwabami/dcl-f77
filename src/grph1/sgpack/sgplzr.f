*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGPLZR(N,RPX,RPY,ITYPE,INDEX)

      REAL      RPX(*),RPY(*)


      IF (N.LT.2) THEN
        CALL MSGDMP('E','SGPLZR','NUMBER OF POINTS IS LESS THAN 2.')
      END IF
      IF (ITYPE.EQ.0) THEN
        CALL MSGDMP('M','SGPLZR','LINETYPE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.EQ.0) THEN
        CALL MSGDMP('M','SGPLZR','POLYLINE INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.LT.0) THEN
        CALL MSGDMP('E','SGPLZR','POLYLINE INDEX IS LESS THAN 0.')
      END IF

      CALL SZPLOP(ITYPE,INDEX)
      CALL SZPLZR(N,RPX,RPY)
      CALL SZPLCL

      END
