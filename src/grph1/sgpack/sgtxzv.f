*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGTXZV(VX,VY,CHARS,RSIZE,IROTA,ICENT,INDEX)

      CHARACTER CHARS*(*)


      IF (RSIZE.EQ.0) THEN
        CALL MSGDMP('M','SGTXZV','TEXT HEIGHT IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (RSIZE.LT.0) THEN
        CALL MSGDMP('E','SGTXZV','TEXT HEIGHT IS LESS THAN ZERO.')
      END IF
      IF (.NOT.(-1.LE.ICENT .AND. ICENT.LE.1)) THEN
        CALL MSGDMP('E','SGTXZV','CENTERING OPTION IS INVALID.')
      END IF
      IF (INDEX.EQ.0) THEN
        CALL MSGDMP('M','SGTXZV','TEXT INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.LT.0) THEN
        CALL MSGDMP('E','SGTXZV','TEXT INDEX IS LESS THAN 0.')
      END IF

      CALL SZTXOP(RSIZE,IROTA,ICENT,INDEX)
      CALL SZTXZV(VX,VY,CHARS)
      CALL SZTXCL

      END
