*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGSSIM(SIMFAC, VXOFF, VYOFF)


      CALL SGRSET('SIMFAC',SIMFAC)
      CALL SGRSET('VXOFF' ,VXOFF)
      CALL SGRSET('VYOFF' ,VYOFF)

      CALL SGQVPT(VXMIN, VXMAX, VYMIN, VYMAX)
      CX  = SIMFAC
      CY  = SIMFAC
      VX0 = (VXMAX + VXMIN)/2 + VXOFF
      VY0 = (VYMAX + VYMIN)/2 + VYOFF
      CXMIN = ( VXMIN - VX0 ) / CX
      CXMAX = CXMIN + (VXMAX-VXMIN) / CX
      CYMIN = ( VYMIN - VY0 ) / CY
      CYMAX = CYMIN + (VYMAX-VYMIN) / CY
      CALL SGRSET('CXMIN',CXMIN)
      CALL SGRSET('CXMAX',CXMAX)
      CALL SGRSET('CYMIN',CYMIN)
      CALL SGRSET('CYMAX',CYMAX)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGQSIM(SIMFAC, VXOFF, VYOFF)

      CALL SGRGET('SIMFAC',SIMFAC)
      CALL SGRGET('VXOFF' ,VXOFF)
      CALL SGRGET('VYOFF' ,VYOFF)

      RETURN
      END
