*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGPLZV(N,VPX,VPY,ITYPE,INDEX)

      REAL      VPX(*),VPY(*)


      IF (N.LT.2) THEN
        CALL MSGDMP('E','SGPLZV','NUMBER OF POINTS IS LESS THAN 2.')
      END IF
      IF (ITYPE.EQ.0) THEN
        CALL MSGDMP('M','SGPLZV','LINETYPE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.EQ.0) THEN
        CALL MSGDMP('M','SGPLZV','POLYLINE INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.LT.0) THEN
        CALL MSGDMP('E','SGPLZV','POLYLINE INDEX IS LESS THAN 0.')
      END IF

      CALL SZPLOP(ITYPE,INDEX)
      CALL SZPLZV(N,VPX,VPY)
      CALL SZPLCL

      END
