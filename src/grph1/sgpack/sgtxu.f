*-----------------------------------------------------------------------
*     TEXT PRIMITIVE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGTXU(UX,UY,CHARS)

      CHARACTER CHARS*(*)

      SAVE

      DATA      RSIZEZ/0.05/,IROTAZ/0/,ICENTZ/0/,INDEXZ/1/


      IF (RSIZEZ.EQ.0) THEN
        CALL MSGDMP('M','SGTXU','TEXT HEIGHT IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (RSIZEZ.LT.0) THEN
        CALL MSGDMP('E','SGTXU','TEXT HEIGHT IS LESS THAN ZERO.')
      END IF
      IF (.NOT.(-1.LE.ICENTZ .AND. ICENTZ.LE.1)) THEN
        CALL MSGDMP('E','SGTXU','CENTERING OPTION IS INVALID.')
      END IF
      IF (INDEXZ.EQ.0) THEN
        CALL MSGDMP('M','SGTXU','TEXT INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEXZ.LT.0) THEN
        CALL MSGDMP('E','SGTXU','TEXT INDEX IS LESS THAN 0.')
      END IF

      CALL SZTXOP(RSIZEZ,IROTAZ,ICENTZ,INDEXZ)
      CALL SZTXZU(UX,UY,CHARS)
      CALL SZTXCL

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGTXV(VX,VY,CHARS)

      IF (RSIZEZ.EQ.0) THEN
        CALL MSGDMP('M','SGTXV','TEXT HEIGHT IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (RSIZEZ.LT.0) THEN
        CALL MSGDMP('E','SGTXV','TEXT HEIGHT IS LESS THAN ZERO.')
      END IF
      IF (.NOT.(-1.LE.ICENTZ .AND. ICENTZ.LE.1)) THEN
        CALL MSGDMP('E','SGTXV','CENTERING OPTION IS INVALID.')
      END IF
      IF (INDEXZ.EQ.0) THEN
        CALL MSGDMP('M','SGTXV','TEXT INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEXZ.LT.0) THEN
        CALL MSGDMP('E','SGTXV','TEXT INDEX IS LESS THAN 0.')
      END IF

      CALL SZTXOP(RSIZEZ,IROTAZ,ICENTZ,INDEXZ)
      CALL SZTXZV(VX,VY,CHARS)
      CALL SZTXCL

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGTXR(RX,RY,CHARS)

      IF (RSIZEZ.EQ.0) THEN
        CALL MSGDMP('M','SGTXR','TEXT HEIGHT IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (RSIZEZ.LT.0) THEN
        CALL MSGDMP('E','SGTXR','TEXT HEIGHT IS LESS THAN ZERO.')
      END IF
      IF (.NOT.(-1.LE.ICENTZ .AND. ICENTZ.LE.1)) THEN
        CALL MSGDMP('E','SGTXR','CENTERING OPTION IS INVALID.')
      END IF
      IF (INDEXZ.EQ.0) THEN
        CALL MSGDMP('M','SGTXR','TEXT INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEXZ.LT.0) THEN
        CALL MSGDMP('E','SGTXR','TEXT INDEX IS LESS THAN 0.')
      END IF

      CALL SZTXOP(RSIZEZ,IROTAZ,ICENTZ,INDEXZ)
      CALL SZTXZR(RX,RY,CHARS)
      CALL SZTXCL

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGSTXS(RSIZE)

      RSIZEZ=RSIZE

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGQTXS(RSIZE)

      RSIZE=RSIZEZ

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGSTXR(IROTA)

      IROTAZ=IROTA

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGQTXR(IROTA)

      IROTA=IROTAZ

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGSTXC(ICENT)

      ICENTZ=ICENT

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGQTXC(ICENT)

      ICENT=ICENTZ

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGSTXI(INDEX)

      INDEXZ=INDEX

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGQTXI(INDEX)

      INDEX=INDEXZ

      RETURN
      END
