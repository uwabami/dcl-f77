*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGPLZU(N,UPX,UPY,ITYPE,INDEX)

      REAL      UPX(*),UPY(*)


      IF (N.LT.2) THEN
        CALL MSGDMP('E','SGPLZU','NUMBER OF POINTS IS LESS THAN 2.')
      END IF
      IF (ITYPE.EQ.0) THEN
        CALL MSGDMP('M','SGPLZU','LINETYPE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.EQ.0) THEN
        CALL MSGDMP('M','SGPLZU','POLYLINE INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.LT.0) THEN
        CALL MSGDMP('E','SGPLZU','POLYLINE INDEX IS LESS THAN 0.')
      END IF

      CALL SZPLOP(ITYPE,INDEX)
      CALL SZPLZU(N,UPX,UPY)
      CALL SZPLCL

      END
