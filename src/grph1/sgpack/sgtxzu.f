*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGTXZU(UX,UY,CHARS,RSIZE,IROTA,ICENT,INDEX)

      REAL WXR,WYR,Q2SIZE
      PARAMETER (WIDE=24)

      CHARACTER CHARS*(*)

      IF (RSIZE.EQ.0) THEN
        CALL MSGDMP('M','SGTXZU','TEXT HEIGHT IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (RSIZE.LT.0) THEN
        CALL MSGDMP('E','SGTXZU','TEXT HEIGHT IS LESS THAN ZERO.')
      END IF
      IF (.NOT.(-1.LE.ICENT .AND. ICENT.LE.1)) THEN
        CALL MSGDMP('E','SGTXZU','CENTERING OPTION IS INVALID.')
      END IF
      IF (INDEX.EQ.0) THEN
        CALL MSGDMP('M','SGTXZU','TEXT INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.LT.0) THEN
        CALL MSGDMP('E','SGTXZU','TEXT INDEX IS LESS THAN 0.')
      END IF

      CALL STFTRF(UX,UY,VX,VY)
      CALL STFWTR(VX,VY,WX,WY)
      CALL STFWTR(VX+RSIZE,VY+RSIZE,WXR,WYR)
      QSIZE=WXR-WX
      Q2SIZE=WYR-WY

      IF(Q2SIZE>QSIZE)THEN
        QSIZE=Q2SIZE
      END IF

      THETA=RD2R(REAL(IROTA))
      CT=QSIZE*COS(THETA)
      ST=QSIZE*SIN(THETA)

      CALL SZTXOP(RSIZE,IROTA,ICENT,INDEX)
      CALL SZTXZU(UX,UY,CHARS)
      CALL SZTXCL

      END
