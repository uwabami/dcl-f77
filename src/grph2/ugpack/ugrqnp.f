*-----------------------------------------------------------------------
*     UGRQNP / UGRQID / UGRQCP / UGRQVL / UGRSVL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UGRQNP(NCP)

      CHARACTER CP*(*)

      PARAMETER (NPARA  = 20)
      PARAMETER (RUNDEF = -999.)

      REAL      RX(NPARA)
      LOGICAL   LCHREQ, LFIRST
      CHARACTER CPARAS(NPARA)*8
      CHARACTER CPARAL(NPARA)*40
      CHARACTER CMSG*80

      EXTERNAL  LCHREQ,LENC

      SAVE

*     / SHORT NAME /

      DATA      CPARAS( 1) / 'XFACT1  ' /, RX( 1) / 1.0 /
      DATA      CPARAS( 2) / 'YFACT1  ' /, RX( 2) / 1.0 /
      DATA      CPARAS( 3) / 'XFACT2  ' /, RX( 3) / RUNDEF /
      DATA      CPARAS( 4) / 'YFACT2  ' /, RX( 4) / RUNDEF /
      DATA      CPARAS( 5) / 'RSMALL  ' /, RX( 5) / 0.001 /
      DATA      CPARAS( 6) / 'RSIZEM  ' /, RX( 6) / 0.01 /
      DATA      CPARAS( 7) / 'RSIZET  ' /, RX( 7) / RUNDEF /
      DATA      CPARAS( 8) / 'XTTL    ' /, RX( 8) / 0.0 /
      DATA      CPARAS( 9) / 'VXUOFF  ' /, RX( 9) / 0.02 /
      DATA      CPARAS(10) / 'VYUOFF  ' /, RX(10) / 0. /
      DATA      CPARAS(11) / 'VXULOC  ' /, RX(11) / RUNDEF /
      DATA      CPARAS(12) / 'VYULOC  ' /, RX(12) / RUNDEF /
      DATA      CPARAS(13) / 'UXUNIT  ' /, RX(13) / RUNDEF /
      DATA      CPARAS(14) / 'UYUNIT  ' /, RX(14) / RUNDEF /
      DATA      CPARAS(15) / 'VXUNIT  ' /, RX(15) / 0.05 /
      DATA      CPARAS(16) / 'VYUNIT  ' /, RX(16) / 0.05 /
      DATA      CPARAS(17) / 'RSIZEUT ' /, RX(17) / RUNDEF /
      DATA      CPARAS(18) / 'VUTOFF  ' /, RX(18) / 0.005 /
      DATA      CPARAS(19) / 'RHFACT  ' /, RX(19) / 1.5 /
      DATA      CPARAS(20) / 'RUNDEF  ' /, RX(20) / RUNDEF /

*     / LONG NAME /

      DATA      CPARAL(1) / 'VECTOR_SCALING_FACTOR_X' /
      DATA      CPARAL(2) / 'VECTOR_SCALING_FACTOR_Y' /
      DATA      CPARAL(3) / '****XFACT2  ' /
      DATA      CPARAL(4) / '****YFACT2  ' /
      DATA      CPARAL(5) / 'SMALL_VECTOR_THRESHOLD' /
      DATA      CPARAL(6) / 'MISS_VECTOR_MARKER_SIZE' /
      DATA      CPARAL(7) / 'VECTOR_MESSAGE_HEIGHT' /
      DATA      CPARAL(8) / 'VECTOR_MESSAGE_POSITION' /
      DATA      CPARAL( 9) / 'UNIT_VECTOR_X_OFFSET' /
      DATA      CPARAL(10) / 'UNIT_VECTOR_Y_OFFSET' /
      DATA      CPARAL(11) / 'UNIT_VECTOR_X_LOCATION' /
      DATA      CPARAL(12) / 'UNIT_VECTOR_Y_LOCATION' /
      DATA      CPARAL(13) / 'UNIT_VECTOR_X_LENGTH' /
      DATA      CPARAL(14) / 'UNIT_VECTOR_Y_LENGTH' /
      DATA      CPARAL(15) / 'UNIT_VECTOR_X_NORMALIZED_LENGTH' /
      DATA      CPARAL(16) / 'UNIT_VECTOR_Y_NORMALIZED_LENGTH' /
      DATA      CPARAL(17) / 'UNIT_VECTOR_TITLE_HEIGHT' /
      DATA      CPARAL(18) / '****VUTOFF  ' /
      DATA      CPARAL(19) / '****RHFACT  ' /
      DATA      CPARAL(20) / '----RUNDEF  ' /

      DATA      LFIRST / .TRUE. /


      NCP = NPARA

      RETURN
*-----------------------------------------------------------------------
      ENTRY UGRQID(CP, IDX)

      DO 10 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IDX = N
          RETURN
        END IF
   10 CONTINUE
      CMSG = 'PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E','UGRQID',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UGRQCP(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAS(IDX)
      ELSE
        CALL MSGDMP('E','UGRQCP','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UGRQCL(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAL(IDX)
      ELSE
        CALL MSGDMP('E','UGRQCL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UGRQVL(IDX, RPARA)

      IF (LFIRST) THEN
        CALL RTRGET('UG', CPARAS, RX, NPARA)
        CALL RLRGET(CPARAL, RX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        RPARA = RX(IDX)
        IF ((IDX.EQ.7 .OR. IDX.EQ.17) .AND. RX(IDX).EQ.RUNDEF) THEN
          CALL UZRGET('RSIZEL1',RPARA)
        END IF
      ELSE
        CALL MSGDMP('E','UGRQVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UGRSVL(IDX, RPARA)

      IF (LFIRST) THEN
        CALL RTRGET('UG', CPARAS, RX, NPARA)
        CALL RLRGET(CPARAL, RX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        RX(IDX) = RPARA
      ELSE
        CALL MSGDMP('E','UGRSVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UGRQIN(CP, IN)

      DO 20 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IN = N
          RETURN
        END IF
   20 CONTINUE

      IN = 0

      RETURN
      END
