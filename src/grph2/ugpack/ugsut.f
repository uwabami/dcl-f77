*-----------------------------------------------------------------------
*     UGSUT / UGDUT
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UGSUT(CSIDE,CTTL)

      CHARACTER CSIDE*(*),CTTL*(*)

      PARAMETER (MAXTTL=10,MAXLEN=32)

      REAL      RSZ(MAXTTL)
      CHARACTER CSIDEZ(MAXTTL)*1,CTTLZ(MAXTTL)*(MAXLEN),CS1*1

      SAVE


*     / CHECK INPUT ARGUMENTS /

      CS1=CSIDE(1:1)
      CALL CUPPER(CS1)
      IF (.NOT.(CS1.EQ.'X' .OR. CS1.EQ.'Y')) THEN
        CALL MSGDMP('W','UGSUT ','SIDE PARAMETER IS INVALID.')
        CALL MSGDMP('M','-CNT.-','DO NOTHING.')
        RETURN
      END IF
      IF (LENZ(CTTL).GT.MAXLEN) THEN
        CALL MSGDMP('W','UGSUT ','LENGTH OF TITLE IS TOO LONG.')
        CALL MSGDMP('M','-CNT.-','DO NOTHING.')
        RETURN
      END IF

      CALL UGRGET('RUNDEF',RUNDEF)
      CALL UGIGET('IUNTTL',NTTL)
      CALL UGRGET('RHFACT',RHFACT)

      NTTL=NTTL+1
      IF (NTTL.EQ.1) THEN
        CALL UGRGET('VXUOFF',VXUOFF)
        CALL UGRGET('VYUOFF',VYUOFF)
        CALL UGRGET('VUTOFF',VUTOFF)
        XTOFF=VXUOFF+VUTOFF
        YTOFF=VYUOFF+VUTOFF
      ELSE
        CALL UGRGET('VXUOFF',XTOFF)
        CALL UGRGET('VYUOFF',YTOFF)
      END IF
      CSIDEZ(NTTL)=CS1
      CTTLZ(NTTL)=CTTL
      CALL UGRGET('RSIZEUT',RSIZE)
      IF (RSIZE.EQ.RUNDEF) THEN
        CALL UZRGET('RSIZEC2',RSZ(NTTL))
        CALL UGRSET('RSIZEUT',RSZ(NTTL))
      ELSE
        RSZ(NTTL)=RSIZE
      END IF

      IF (CS1.EQ.'X') THEN
        YTOFF=YTOFF+RSZ(NTTL)*RHFACT
      ELSE IF (CS1.EQ.'Y') THEN
        XTOFF=XTOFF+RSZ(NTTL)*RHFACT
      END IF

      CALL UGISET('IUNTTL',NTTL)
      CALL UGRSET('VXUOFF',XTOFF)
      CALL UGRSET('VYUOFF',YTOFF)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UGDUT

*     / DRAW TITLES FOR UNIT VECTOR /

      CALL UGIGET('IUNTTL',NTTL)
      CALL UGRGET('RHFACT',RHFACT)

      IF (NTTL.LE.0) THEN
        RETURN
      ELSE
        CALL UGRGET('VXULOC',VXULOC)
        CALL UGRGET('VYULOC',VYULOC)
        CALL UGRGET('VXUNIT',VXUNIT)
        CALL UGRGET('VYUNIT',VYUNIT)
        CALL UGRGET('UXUNIT',UXUNIT)
        CALL UGRGET('UYUNIT',UYUNIT)
        CALL UGIGET('IUINDX',IUINDX)
        CALL UGRGET('VUTOFF',VUTOFF)
        XOFF=VXULOC-VUTOFF
        YOFF=VYULOC-VUTOFF
        DO 10 ITTL=1,NTTL
          CALL SGSTXS(RSZ(ITTL))
          IF (CSIDEZ(ITTL).EQ.'X') THEN
            XX=VXULOC+VXUNIT*0.5
            YY=YOFF-RSZ(ITTL)*RHFACT/2
            YOFF=YOFF-RSZ(ITTL)*RHFACT
            CALL UGIGET('IUTXRO',IUTXRO)
            IF (IUTXRO.NE.0 .AND. LENZ(CTTLZ(ITTL)).GE.2) THEN
              CALL MSGDMP('W','UGDUT ',
     &          'LENGTH OF TITLE IS TOO LONG TO CHANGE ROT')
              CALL SGSTXR(0)
            ELSE
              CALL SGSTXR(IUTXRO)
            END IF
          ELSE IF (CSIDEZ(ITTL).EQ.'Y') THEN
            XX=XOFF-RSZ(ITTL)*RHFACT/2
            YY=VYULOC+VYUNIT*0.5
            XOFF=XOFF-RSZ(ITTL)*RHFACT
            CALL UGIGET('IUTYRO',IUTYRO)
            IF (IUTYRO.NE.90 .AND. LENZ(CTTLZ(ITTL)).GE.2) THEN
              CALL MSGDMP('W','UGDUT ',
     &          'LENGTH OF TITLE IS TOO LONG TO CHANGE ROT')
              CALL SGSTXR(90)
            ELSE
              CALL SGSTXR(IUTYRO)
            END IF
          END IF
          CALL SGSTXI(IUINDX)
          CALL SGTXV(XX,YY,CTTLZ(ITTL))
   10   CONTINUE
      END IF

      CALL UGISET('IUNTTL',0)
      CALL UGRSET('VXUOFF',VXUOFF)
      CALL UGRSET('VYUOFF',VYUOFF)

      RETURN
      END
