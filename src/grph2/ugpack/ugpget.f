*-----------------------------------------------------------------------
*     UGPGET / UGPSET / UGPSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UGPGET(CP, IPARA)

      CHARACTER CP*(*)

      CHARACTER CX*8, CL*40


      CALL UGPQID(CP, IDX)
      CALL UGPQVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UGPSET(CP, IPARA)

      CALL UGPQID(CP, IDX)
      CALL UGPSVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UGPSTX(CP, IPARA)

      IP = IPARA
      CALL UGPQID(CP, IDX)
      CALL UGPQIT(IDX, IT)
      CALL UGPQCP(IDX, CX)
      CALL UGPQCL(IDX, CL)
      IF (IT .EQ. 1) THEN
        CALL RTIGET('UG', CX, IP, 1)
        CALL RLIGET(CL, IP, 1)
        CALL UGIQID(CP, IDX)
        CALL UGISVL(IDX, IP)
      ELSE IF (IT .EQ. 2) THEN
        CALL RTLGET('UG', CX, IP, 1)
        CALL RLLGET(CL, IP, 1)
        CALL UGLQID(CP, IDX)
        CALL UGLSVL(IDX, IP)
      ELSE IF (IT .EQ. 3) THEN
        CALL RTRGET('UG', CX, IP, 1)
        CALL RLRGET(CL, IP, 1)
        CALL UGRQID(CP, IDX)
        CALL UGRSVL(IDX, IP)
      END IF

      RETURN
      END
