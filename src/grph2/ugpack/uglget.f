*-----------------------------------------------------------------------
*     UGLGET / UGLSET / UGLSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UGLGET(CP, LPARA)

      LOGICAL   LPARA
      CHARACTER CP*(*)

      LOGICAL   LP
      CHARACTER CX*8
      CHARACTER CL*40


      CALL UGLQID(CP, IDX)
      CALL UGLQVL(IDX, LPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UGLSET(CP, LPARA)

      CALL UGLQID(CP, IDX)
      CALL UGLSVL(IDX, LPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UGLSTX(CP, LPARA)

      LP = LPARA
      CALL UGLQID(CP, IDX)

*     / SHORT NAME /

      CALL UGLQCP(IDX, CX)
      CALL RTLGET('UG', CX, LP, 1)

*     / LONG NAME /

      CALL UGLQCL(IDX, CL)
      CALL RLLGET(CL, LP, 1)

      CALL UGLSVL(IDX,LP)

      RETURN
      END
