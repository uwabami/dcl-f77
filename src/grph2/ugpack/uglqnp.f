*-----------------------------------------------------------------------
*     UGLQNP / UGLQID / UGLQCP / UGLQVL / UGLSVL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UGLQNP(NCP)

      LOGICAL   LPARA
      CHARACTER CP*(*)

      PARAMETER (NPARA = 7)

      LOGICAL   LX(NPARA)
      LOGICAL   LCHREQ, LFIRST
      CHARACTER CPARAS(NPARA)*8
      CHARACTER CPARAL(NPARA)*40
      CHARACTER CMSG*80

      EXTERNAL  LCHREQ,LENC

      SAVE

*     / SHORT NAME /

      DATA      CPARAS(1) / 'LNRMAL  ' /, LX(1) / .TRUE. /
      DATA      CPARAS(2) / 'LEQRAT  ' /, LX(2) / .TRUE. /
      DATA      CPARAS(3) / 'LMSG    ' /, LX(3) / .TRUE. /
      DATA      CPARAS(4) / 'LMISSP  ' /, LX(4) / .FALSE. /
      DATA      CPARAS(5) / 'LSMALL  ' /, LX(5) / .FALSE. /
      DATA      CPARAS(6) / 'LUNIT   ' /, LX(6) / .FALSE. /
      DATA      CPARAS(7) / 'LUMSG   ' /, LX(7) / .TRUE. /

*     / LONG NAME /

      DATA      CPARAL(1) / 'ENABLE_VECTOR_NORMALIZING' /
      DATA      CPARAL(2) / 'FIX_VECTOR_PROPORTION' /
      DATA      CPARAL(3) / 'ENABLE_VECTOR_MESSAGE' /
      DATA      CPARAL(4) / 'ENABLE_MISS_VECTOR_MARKER' /
      DATA      CPARAL(5) / 'ENABLE_SMALL_VECTOR_MARKER' /
      DATA      CPARAL(6) / 'DRAW_UNIT_VECTOR' /
      DATA      CPARAL(7) / 'DRAW_UNIT_VECTOR_MESSAGE' /

      DATA      LFIRST / .TRUE. /


      NCP = NPARA

      RETURN
*-----------------------------------------------------------------------
      ENTRY UGLQID(CP, IDX)

      DO 10 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IDX = N
          RETURN
        END IF
   10 CONTINUE
      CMSG = 'PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E','UGLQID',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UGLQCP(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAS(IDX)
      ELSE
        CALL MSGDMP('E','UGLQCP','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UGLQCL(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAL(IDX)
      ELSE
        CALL MSGDMP('E','UGLQCL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UGLQVL(IDX, LPARA)

      IF (LFIRST) THEN
        CALL RTLGET('UG', CPARAS, LX, NPARA)
        CALL RLLGET(CPARAL, LX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        LPARA = LX(IDX)
      ELSE
        CALL MSGDMP('E','UGLQVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UGLSVL(IDX, LPARA)

      IF (LFIRST) THEN
        CALL RTLGET('UG', CPARAS, LX, NPARA)
        CALL RLLGET(CPARAL, LX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        LX(IDX) = LPARA
      ELSE
        CALL MSGDMP('E','UGLSVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UGLQIN(CP, IN)

      DO 20 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IN = N
          RETURN
        END IF
   20 CONTINUE

      IN = 0

      RETURN
      END
