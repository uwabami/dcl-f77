*-----------------------------------------------------------------------
*     UGVECT
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UGVECT(U,MU,V,MV,NX,NY)

      REAL      U(MU,*),V(MV,*)

      CHARACTER CMSG*80
      LOGICAL   LMISS,LNRMAL,LMADA,LOK,LMSG,LEQRAT,
     +          LMISSP,LSMALL,LUNIT,LUMSG

      EXTERNAL  RUWGX,RUWGY

      SAVE


*     / GET INTERNAL PARAMETERS /

      CALL GLLGET('LMISS   ',LMISS )
      CALL GLRGET('RMISS   ',RMISS )
      CALL UGIGET('INDEX   ',INDEX )
      CALL UGLGET('LNRMAL  ',LNRMAL)
      CALL UGLGET('LEQRAT  ',LEQRAT)
      CALL UGLGET('LMSG    ',LMSG  )
      CALL UGLGET('LUNIT   ',LUNIT )
      CALL UGLGET('LUMSG   ',LUMSG )
      CALL UGIGET('ICENT   ',ICENT )
      CALL UGLGET('LMISSP  ',LMISSP)
      CALL UGIGET('ITYPE1  ',ITYPE1)
      CALL UGLGET('LSMALL  ',LSMALL)
      CALL UGRGET('RSMALL  ',RSMALL)
      CALL UGIGET('ITYPE2  ',ITYPE2)
      CALL UGRGET('RSIZEM  ',RSIZEM)
      CALL UGRGET('RSIZET  ',RSIZET)
      CALL UGRGET('XTTL    ',XTTL  )
      CALL UGIGET('IXINT   ',IXINT )
      CALL UGIGET('IYINT   ',IYINT )

*     / SET GRID ATTRIBUTE IF IT HAS NOT BEEN SET YET /

      CALL UWDFLT(NX,NY)

*     / CHECK INPUT DATA /

      LMADA=.TRUE.

      DO 10 J=1,NY,IYINT
        DO 15 I=1,NX,IXINT

          LOK=.NOT.(LMISS .AND. (U(I,J).EQ.RMISS .OR. V(I,J).EQ.RMISS))

*         / CHECK MIN & MAX /

          IF (LMADA) THEN
            IF (LOK) THEN
              RMAXU=U(I,J)
              RMINU=U(I,J)
              RMAXV=V(I,J)
              RMINV=V(I,J)
              LMADA=.FALSE.
            END IF
          ELSE
            IF (LOK) THEN
              IF (U(I,J).GT.RMAXU) THEN
                RMAXU=U(I,J)
              ELSE IF (U(I,J).LT.RMINU) THEN
                RMINU=U(I,J)
              END IF
              IF (V(I,J).GT.RMAXV) THEN
                RMAXV=V(I,J)
              ELSE IF (V(I,J).LT.RMINV) THEN
                RMINV=V(I,J)
              END IF
            END IF
          END IF

   15   CONTINUE
   10 CONTINUE

*     / WRITE MESSAGE IF MISSING OR ZERO FIELD /

      IF (LMADA .OR. (RMINU.EQ.0 .AND. RMAXU.EQ.0
     +          .AND. RMINV.EQ.0 .AND. RMAXV.EQ.0)) THEN

        IF (LMADA) THEN
          CMSG='MISSING FIELD.'
        ELSE
          CMSG='ZERO FIELD.'
        END IF
        CALL MSGDMP('W','UGVECT',CMSG)
        IF (LMSG) THEN
          CALL UZRGET('RSIZEC2',RSIZEZ)
          CALL UZRSET('RSIZEC2',RSIZET)
          CALL UXPTTL('B',2,'  ',XTTL)
          CALL UXPTTL('B',2,CMSG,XTTL)
          CALL UZRSET('RSIZEC2',RSIZEZ)
        END IF
        GO TO 100

      END IF

*     / CALCULATE NORMALIZATION FACTOR /

      IF (LNRMAL) THEN

        CALL SGQVPT(VXMN,VXMX,VYMN,VYMX)
        UE=(VXMX-VXMN)/(NX/IXINT)
        VE=(VYMX-VYMN)/(NY/IYINT)
        AU=RGNLE(UE/MAX(ABS(RMINU),ABS(RMAXU)))
        AV=RGNLE(VE/MAX(ABS(RMINV),ABS(RMAXV)))
        IF (LEQRAT) THEN
          AU=MIN(AU,AV)
          AV=MIN(AU,AV)
        END IF

      ELSE

        CALL UGRGET('XFACT1  ',XFACT1)
        CALL UGRGET('YFACT1  ',YFACT1)
        AU=XFACT1
        AV=YFACT1

      END IF

      CALL UGRSET('XFACT2  ',AU)
      CALL UGRSET('YFACT2  ',AV)

*     / DRAW UNIT VECTOR IF LUNIT /

      IF (LUNIT) THEN

        CALL UGUNIT

*       / WRITE UNIT VALUE IF LMUSG /

        IF (LUMSG) THEN

          CALL UGRGET('UXUNIT  ',UXUNIT)
          CALL UGRGET('UYUNIT  ',UYUNIT)
          CMSG='XUNIT =##########, YUNIT =##########'
          WRITE(CMSG( 8:17),'(1P,E10.3)') UXUNIT
          WRITE(CMSG(27:36),'(1P,E10.3)') UYUNIT
          CALL UZRGET('RSIZEC2',RSIZEZ)
          CALL UZRSET('RSIZEC2',RSIZET)
          CALL UXPTTL('B',2,'  ',XTTL)
          CALL UXPTTL('B',2,CMSG,XTTL)
          CALL UZRSET('RSIZEC2',RSIZEZ)

        END IF

*       / DRAW TITLE FOR UNIT VECTOR /

        CALL UGDUT

*     / WRITE SCALING FACTOR IF LMSG /

      ELSE IF (LMSG) THEN

        CMSG='XFACT =##########, YFACT =##########'
        WRITE(CMSG( 8:17),'(1P,E10.3)') AU
        WRITE(CMSG(27:36),'(1P,E10.3)') AV
        CALL UZRGET('RSIZEC2',RSIZEZ)
        CALL UZRSET('RSIZEC2',RSIZET)
        CALL UXPTTL('B',2,'  ',XTTL)
        CALL UXPTTL('B',2,CMSG,XTTL)
        CALL UZRSET('RSIZEC2',RSIZEZ)

      END IF

*     / DRAW VECTORS /

      CALL SZLAOP(1,INDEX)

      DO 25 J=1,NY,IYINT
        DO 20 I=1,NX,IXINT

          LOK=.NOT.(LMISS .AND. (U(I,J).EQ.RMISS .OR. V(I,J).EQ.RMISS))
          CALL STFTRF(RUWGX(I),RUWGY(J),VX0,VY0)

          IF (LOK) THEN

            VX1=VX0-U(I,J)*(ICENT+1)/2.0*AU
            VX2=VX0-U(I,J)*(ICENT-1)/2.0*AU
            VY1=VY0-V(I,J)*(ICENT+1)/2.0*AV
            VY2=VY0-V(I,J)*(ICENT-1)/2.0*AV
            R=SQRT((VX2-VX1)**2+(VY2-VY1)**2)
            IF (LSMALL .AND. R.LE.RSMALL) THEN
              CALL SGPMZV(1,VX0,VY0,ITYPE2,INDEX,RSIZEM)
            ELSE
              CALL SZLAZV(VX1,VY1,VX2,VY2)
            END IF

          ELSE

            IF (LMISSP) THEN
              CALL SGPMZV(1,VX0,VY0,ITYPE1,INDEX,RSIZEM)
            END IF

          END IF

   20   CONTINUE
   25 CONTINUE

      CALL SZLACL

  100 CONTINUE

      END
