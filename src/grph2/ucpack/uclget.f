*-----------------------------------------------------------------------
*     UCLGET / UCLSET / UCLSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UCLGET(CP, LPARA)

      LOGICAL   LPARA
      CHARACTER CP*(*)

      LOGICAL   LP
      CHARACTER CX*8
      CHARACTER CL*40


      CALL UCLQID(CP, IDX)
      CALL UCLQVL(IDX, LPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UCLSET(CP, LPARA)

      CALL UCLQID(CP, IDX)
      CALL UCLSVL(IDX, LPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UCLSTX(CP, LPARA)

      LP = LPARA
      CALL UCLQID(CP, IDX)

*     / SHORT NAME /

      CALL UCLQCP(IDX, CX)
      CALL RTLGET('UC', CX, LP, 1)

*     / LONG NAME /

      CALL UCLQCL(IDX, CL)
      CALL RLLGET(CL, LP, 1)

      CALL UCLSVL(IDX,LP)

      RETURN
      END
