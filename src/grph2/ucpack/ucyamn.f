*-----------------------------------------------------------------------
*     UCYAMN : PLOT MONTH AXIS
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UCYAMN(CSIDE,JD0,ND)

      CHARACTER CSIDE*1

      PARAMETER (N=50)

      REAL      UY(N)
      CHARACTER CH(N)*9,CMON*9
      LOGICAL   LABEL,LUYCHK,LBTWN


      IF (.NOT.LUYCHK(CSIDE)) THEN
        CALL MSGDMP('E','UCYAMN','SIDE PARAMETER IS INVALID.')
      END IF
      IF (JD0.LT.0) THEN
        CALL MSGDMP('E','UCYAMN','FIRST DATE IS LESS THAN 0.')
      END IF
      IF (ND.LE.0) THEN
        CALL MSGDMP('E','UCYAMN','DATE LENGTH IS LESS THAN 0.')
      END IF

      NC=NUCCHR('Y',ND)
      MC=ABS(NC)
      IF (MC.EQ.0) THEN
        CALL MSGDMP('W','UCYAMN','NO MONTH-AXIS.')
        RETURN
      END IF

      CALL UYPAXS(CSIDE,2)

      NN=1
      UY(NN)=0
      CALL DATE13(JD0,IY0,MO0,ID0)

      DO 10 I=1,ND
        CALL DATEF3(I,IY0,MO0,ID0,IYI,MOI,IDI)
        CALL DATE32(IYI,MOI,IDI,ITDI)
        IML=NDMON(IYI,MOI)
        IF (IDI.EQ.IML .OR. I.EQ.ND) THEN
          NN=NN+1
          IF (NN.GT.N) THEN
            CALL MSGDMP('E','UCYAMN','WORKING AREA IS NOT ENOUGH.')
          END IF
          UY(NN)=I
          CH(NN-1)=' '
          CH(NN-1)(1:MC)=CMON(MOI)
          IF (NC.LT.0 .AND. MC.GE.2) THEN
            CALL CLOWER(CH(NN-1)(2:MC))
          END IF
        END IF
   10 CONTINUE

      CALL UZIGET('ICENTY'//CSIDE,ICENT)
      CALL UZIGET('IROTLY'//CSIDE,IROTA)
      CALL UZIGET('IROTCY'//CSIDE,IROTC)
      CALL UZLGET('LBTWN',LBTWN)

      CALL UZISET('ICENTY'//CSIDE,0)
      CALL UZISET('IROTLY'//CSIDE,IROTC)
      CALL UZLSET('LBTWN',.TRUE.)

      CALL UYPTMK(CSIDE,2,UY,NN)
      CALL UZLGET('LABELY'//CSIDE,LABEL)
      IF (LABEL) THEN
        CALL UYPLBL(CSIDE,2,UY,CH,9,NN)
      END IF

      CALL UZISET('ICENTY'//CSIDE,ICENT)
      CALL UZISET('IROTLY'//CSIDE,IROTA)
      CALL UZLSET('LBTWN',LBTWN)

      END
