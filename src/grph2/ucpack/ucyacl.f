*-----------------------------------------------------------------------
*     UCYACL : PLOT CALENDAR AXIS
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UCYACL(CSIDE,JD0,ND)

      CHARACTER CSIDE*1

      LOGICAL   LUYCHK


      IF (.NOT.LUYCHK(CSIDE)) THEN
        CALL MSGDMP('E','UCYACL','SIDE PARAMETER IS INVALID.')
      END IF
      IF (JD0.LT.0) THEN
        CALL MSGDMP('E','UCYACL','FIRST DATE IS LESS THAN 0.')
      END IF
      IF (ND.LE.0) THEN
        CALL MSGDMP('E','UCYACL','DATE LENGTH IS LESS THAN 0.')
      END IF

      CALL UYPAXS(CSIDE,2)

      CALL UCYADY(CSIDE,JD0,ND)
      CALL UCYAMN(CSIDE,JD0,ND)
      CALL UCYAYR(CSIDE,JD0,ND)

      END
