*-----------------------------------------------------------------------
*     UCXADY : PLOT DATE AXIS
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UCXADY(CSIDE,JD0,ND)

      CHARACTER CSIDE*1

      PARAMETER (N=100)

      REAL      UX(N)
      CHARACTER CH(N)*2
      LOGICAL   LABEL,LBTWN,LUXCHK


      IF (.NOT.LUXCHK(CSIDE)) THEN
        CALL MSGDMP('E','UCXADY','SIDE PARAMETER IS INVALID.')
      END IF
      IF (JD0.LT.0) THEN
        CALL MSGDMP('E','UCXADY','FIRST DATE IS LESS THAN 0.')
      END IF
      IF (ND.LE.0) THEN
        CALL MSGDMP('E','UCXADY','DATE LENGTH IS LESS THAN 0.')
      END IF

      CALL UXPAXS(CSIDE,2)

      ID=NUCDAY('X',ND)
      IF (ID.LE.0) THEN
        CALL MSGDMP('W','UCXADY','NO DAY-AXIS.')
        RETURN
      END IF

      CALL DATE12(JD0,IY0,IT0)
      NN=0
      DO 10 I=0,ND
        CALL DATEF2(I,IY0,IT0,IYI,ITI)
        CALL DATE23(IYI,MOI,IDI,ITI)
        IML=NDMON(IYI,MOI)
        IF ((IDI.LT.IML-ID/2 .AND. MOD(IDI,ID).EQ.0)
     +     .OR. IDI.EQ.IML) THEN
          NN=NN+1
          IF (NN.GT.N) THEN
            CALL MSGDMP('E','UCXADY','WORKING AREA IS NOT ENOUGH.')
          END IF
          UX(NN)=I
          WRITE(CH(NN)(1:2),'(I2)') IDI
          CALL CLADJ(CH(NN)(1:2))
        END IF
   10 CONTINUE

      IF (NN.EQ.0) THEN
        CALL MSGDMP('W','UCXADY','THERE IS NO TICKMARK / LABEL.')
        RETURN
      END IF

      CALL UZIGET('ICENTX'//CSIDE,ICENT)
      CALL UZIGET('IROTLX'//CSIDE,IROTA)
      CALL UZIGET('IROTCX'//CSIDE,IROTC)
      CALL UZLGET('LBTWN',LBTWN)

      CALL UZISET('ICENTX'//CSIDE,0)
      CALL UZISET('IROTLX'//CSIDE,IROTC)
      CALL UZLSET('LBTWN',.FALSE.)

      CALL UXPTMK(CSIDE,1,UX,NN)
      CALL UZLGET('LABELX'//CSIDE,LABEL)
      IF (LABEL) THEN
        CALL UXPLBL(CSIDE,1,UX,CH,2,NN)
      END IF

      CALL UZISET('ICENTX'//CSIDE,ICENT)
      CALL UZISET('IROTLX'//CSIDE,IROTA)
      CALL UZLSET('LBTWN',LBTWN)

      END
