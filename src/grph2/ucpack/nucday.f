*-----------------------------------------------------------------------
*     NUCDAY
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      INTEGER FUNCTION NUCDAY(CS,ND)

      CHARACTER CS*(*)

      PARAMETER (NX=5)

      INTEGER   NDX(NX)

      DATA      NDX/  1,  2,  5, 10, 15/


      CALL UCIGET('IUNDEF',IUNDEF)
      CALL UCIGET('NDAY  ',NDAY  )

      IF (NDAY.NE.IUNDEF) THEN
        NUCDAY=NDAY
      ELSE
        CALL UCRGET('DFACT  ',DFACT)
        CALL UZRGET('RSIZEL1',RSIZE)
        CALL SGQVPT(VX1,VX2,VY1,VY2)
        IF (CS(1:1).EQ.'X') THEN
          WD=VX2-VX1
        ELSE IF (CS(1:1).EQ.'Y') THEN
          WD=VY2-VY1
        END IF
        MAXL=WD/(RSIZE*2*DFACT)
        DO 10 N=1,NX
          IF (ND/NDX(N).LE.MAXL) THEN
            NUCDAY=NDX(N)
            RETURN
          END IF
   10   CONTINUE
        NUCDAY=0
      END IF

      END
