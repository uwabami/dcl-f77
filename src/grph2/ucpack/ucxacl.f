*-----------------------------------------------------------------------
*     UCXACL : PLOT CALENDAR AXIS
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UCXACL(CSIDE,JD0,ND)

      CHARACTER CSIDE*1

      LOGICAL   LUXCHK


      IF (.NOT.LUXCHK(CSIDE)) THEN
        CALL MSGDMP('E','UCXACL','SIDE PARAMETER IS INVALID.')
      END IF
      IF (JD0.LT.0) THEN
        CALL MSGDMP('E','UCXACL','FIRST DATE IS LESS THAN 0.')
      END IF
      IF (ND.LE.0) THEN
        CALL MSGDMP('E','UCXACL','DATE LENGTH IS LESS THAN 0.')
      END IF

      CALL UXPAXS(CSIDE,2)

      CALL UCXADY(CSIDE,JD0,ND)
      CALL UCXAMN(CSIDE,JD0,ND)
      CALL UCXAYR(CSIDE,JD0,ND)

      END
