*-----------------------------------------------------------------------
*     UCPGET / UCPSET / UCPSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UCPGET(CP, IPARA)

      CHARACTER CP*(*)

      CHARACTER CX*8, CL*40


      CALL UCPQID(CP, IDX)
      CALL UCPQVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UCPSET(CP, IPARA)

      CALL UCPQID(CP, IDX)
      CALL UCPSVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UCPSTX(CP, IPARA)

      IP = IPARA
      CALL UCPQID(CP, IDX)
      CALL UCPQIT(IDX, IT)
      CALL UCPQCP(IDX, CX)
      CALL UCPQCL(IDX, CL)
      IF (IT .EQ. 1) THEN
        CALL RTIGET('UC', CX, IP, 1)
        CALL RLIGET(CL, IP, 1)
        CALL UCIQID(CP, IDX)
        CALL UCISVL(IDX, IP)
      ELSE IF (IT .EQ. 2) THEN
        CALL RTLGET('UC', CX, IP, 1)
        CALL RLLGET(CL, IP, 1)
        CALL UCLQID(CP, IDX)
        CALL UCLSVL(IDX, IP)
      ELSE IF (IT .EQ. 3) THEN
        CALL RTRGET('UC', CX, IP, 1)
        CALL RLRGET(CL, IP, 1)
        CALL UCRQID(CP, IDX)
        CALL UCRSVL(IDX, IP)
      END IF

      RETURN
      END
