*-----------------------------------------------------------------------
*     UCXAMN : PLOT MONTH AXIS
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UCXAMN(CSIDE,JD0,ND)

      CHARACTER CSIDE*1

      PARAMETER (N=50)

      REAL      UX(N)
      CHARACTER CH(N)*9,CMON*9
      LOGICAL   LABEL,LUXCHK,LBTWN


      IF (.NOT.LUXCHK(CSIDE)) THEN
        CALL MSGDMP('E','UCXAMN','SIDE PARAMETER IS INVALID.')
      END IF
      IF (JD0.LT.0) THEN
        CALL MSGDMP('E','UCXAMN','FIRST DATE IS LESS THAN 0.')
      END IF
      IF (ND.LE.0) THEN
        CALL MSGDMP('E','UCXAMN','DATE LENGTH IS LESS THAN 0.')
      END IF

      NC=NUCCHR('X',ND)
      MC=ABS(NC)
      IF (MC.EQ.0) THEN
        CALL MSGDMP('W','UCXAMN','NO MONTH-AXIS.')
        RETURN
      END IF

      CALL UXPAXS(CSIDE,2)

      NN=1
      UX(NN)=0
      CALL DATE13(JD0,IY0,MO0,ID0)

      DO 10 I=1,ND
        CALL DATEF3(I,IY0,MO0,ID0,IYI,MOI,IDI)
        CALL DATE32(IYI,MOI,IDI,ITDI)
        IML=NDMON(IYI,MOI)
        IF (IDI.EQ.IML .OR. I.EQ.ND) THEN
          NN=NN+1
          IF (NN.GT.N) THEN
            CALL MSGDMP('E','UCXAMN','WORKING AREA IS NOT ENOUGH.')
          END IF
          UX(NN)=I
          CH(NN-1)=' '
          CH(NN-1)(1:MC)=CMON(MOI)
          IF (NC.LT.0 .AND. MC.GE.2) THEN
            CALL CLOWER(CH(NN-1)(2:MC))
          END IF
        END IF
   10 CONTINUE

      CALL UZIGET('ICENTX'//CSIDE,ICENT)
      CALL UZIGET('IROTLX'//CSIDE,IROTA)
      CALL UZIGET('IROTCX'//CSIDE,IROTC)
      CALL UZLGET('LBTWN',LBTWN)

      CALL UZISET('ICENTX'//CSIDE,0)
      CALL UZISET('IROTLX'//CSIDE,IROTC)
      CALL UZLSET('LBTWN',.TRUE.)

      CALL UXPTMK(CSIDE,2,UX,NN)
      CALL UZLGET('LABELX'//CSIDE,LABEL)
      IF (LABEL) THEN
        CALL UXPLBL(CSIDE,2,UX,CH,9,NN)
      END IF

      CALL UZISET('ICENTX'//CSIDE,ICENT)
      CALL UZISET('IROTLX'//CSIDE,IROTA)
      CALL UZLSET('LBTWN',LBTWN)

      END
