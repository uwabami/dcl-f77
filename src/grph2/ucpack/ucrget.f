*-----------------------------------------------------------------------
*     UCRGET / UCRSET / UCRSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UCRGET(CP, RPARA)

      CHARACTER CP*(*)

      CHARACTER CX*8
      CHARACTER CL*40


      CALL UCRQID(CP, IDX)
      CALL UCRQVL(IDX, RPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UCRSET(CP, RPARA)

      CALL UCRQID(CP, IDX)
      CALL UCRSVL(IDX, RPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UCRSTX(CP, RPARA)

      RP = RPARA
      CALL UCRQID(CP, IDX)

*     / SHORT NAME /

      CALL UCRQCP(IDX, CX)
      CALL RTRGET('UC', CX, RP, 1)

*     / LONG NAME /

      CALL UCRQCL(IDX, CL)
      CALL RLRGET(CL, RP, 1)

      CALL UCRSVL(IDX,RP)

      RETURN
      END
