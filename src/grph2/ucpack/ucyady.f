*-----------------------------------------------------------------------
*     UCYADY : PLOT DATE AXIS
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UCYADY(CSIDE,JD0,ND)

      CHARACTER CSIDE*1

      PARAMETER (N=100)

      REAL      UY(N)
      CHARACTER CH(N)*2
      LOGICAL   LABEL,LUYCHK,LBTWN


      IF (.NOT.LUYCHK(CSIDE)) THEN
        CALL MSGDMP('E','UCYADY','SIDE PARAMETER IS INVALID.')
      END IF
      IF (JD0.LT.0) THEN
        CALL MSGDMP('E','UCYADY','FIRST DATE IS LESS THAN 0.')
      END IF
      IF (ND.LE.0) THEN
        CALL MSGDMP('E','UCYADY','DATE LENGTH IS LESS THAN 0.')
      END IF

      CALL UYPAXS(CSIDE,2)

      ID=NUCDAY('Y',ND)
      IF (ID.LE.0) THEN
        CALL MSGDMP('W','UCYADY','NO DAY-AXIS.')
        RETURN
      END IF

      CALL DATE12(JD0,IY0,IT0)
      NN=0
      DO 10 I=0,ND
        CALL DATEF2(I,IY0,IT0,IYI,ITI)
        CALL DATE23(IYI,MOI,IDI,ITI)
        IML=NDMON(IYI,MOI)
        IF ((IDI.LT.IML-ID/2 .AND. MOD(IDI,ID).EQ.0)
     +     .OR. IDI.EQ.IML) THEN
          NN=NN+1
          IF (NN.GT.N) THEN
            CALL MSGDMP('E','UCYADY','WORKING AREA IS NOT ENOUGH.')
          END IF
          UY(NN)=I
          WRITE(CH(NN)(1:2),'(I2)') IDI
          CALL CLADJ(CH(NN)(1:2))
        END IF
   10 CONTINUE

      IF (NN.EQ.0) THEN
        CALL MSGDMP('W','UCYADY','THERE IS NO TICKMARK / LABEL.')
        RETURN
      END IF

      CALL UZIGET('ICENTY'//CSIDE,ICENT)
      CALL UZIGET('IROTLY'//CSIDE,IROTA)
      CALL UZIGET('IROTCY'//CSIDE,IROTC)
      CALL UZLGET('LBTWN',LBTWN)

      CALL UZISET('ICENTY'//CSIDE,0)
      CALL UZISET('IROTLY'//CSIDE,IROTC)
      CALL UZLSET('LBTWN',.FALSE.)

      CALL UYPTMK(CSIDE,1,UY,NN)
      CALL UZLGET('LABELY'//CSIDE,LABEL)
      IF (LABEL) THEN
        CALL UYPLBL(CSIDE,1,UY,CH,2,NN)
      END IF

      CALL UZISET('ICENTY'//CSIDE,ICENT)
      CALL UZISET('IROTLY'//CSIDE,IROTA)
      CALL UZLSET('LBTWN',LBTWN)

      END
