*-----------------------------------------------------------------------
*     UCIGET / UCISET / UCISTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UCIGET(CP, IPARA)

      CHARACTER CP*(*)

      CHARACTER CX*8
      CHARACTER CL*40


      CALL UCIQID(CP, IDX)
      CALL UCIQVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UCISET(CP, IPARA)

      CALL UCIQID(CP, IDX)
      CALL UCISVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UCISTX(CP, IPARA)

      IP = IPARA
      CALL UCIQID(CP, IDX)

*     / SHORT NAME /

      CALL UCIQCP(IDX, CX)
      CALL RTIGET('UC', CX, IP, 1)

*     / LONG NAME /

      CALL UCIQCL(IDX, CL)
      CALL RLIGET(CL, IP, 1)

      CALL UCISVL(IDX,IP)

      RETURN
      END
