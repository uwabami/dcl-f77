*-----------------------------------------------------------------------
*     NUCCHR
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      INTEGER FUNCTION NUCCHR(CS,ND)

      CHARACTER CS*(*)

      PARAMETER (NX=4)

      INTEGER   NCX(NX)
      LOGICAL   LOWER

      DATA      NCX/  1,  2,  3,  9/


      CALL UCIGET('IUNDEF',IUNDEF)
      CALL UCIGET('NCHAR ',NCHAR )

      IF (NCHAR.NE.IUNDEF) THEN
        NUCCHR=NCHAR
      ELSE
        CALL UCLGET('LOWER  ',LOWER)
        CALL UZRGET('RSIZEL2',RSIZE)
        CALL SGQVPT(VX1,VX2,VY1,VY2)
        IF (CS(1:1).EQ.'X') THEN
          WD=VX2-VX1
        ELSE IF (CS(1:1).EQ.'Y') THEN
          WD=VY2-VY1
        END IF
        MAXC=WD/RSIZE
        NMON=ND/28+1
        DO 10 N=NX,1,-1
          IF (NMON*NCX(N).LE.MAXC) THEN
            IF (LOWER) THEN
              NUCCHR=-NCX(N)
            ELSE
              NUCCHR=+NCX(N)
            END IF
            RETURN
          END IF
   10   CONTINUE
        NUCCHR=0
      END IF

      END
