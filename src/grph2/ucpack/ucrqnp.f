*-----------------------------------------------------------------------
*     UCRQNP / UCRQID / UCRQCP / UCRQVL / UCRSVL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UCRQNP(NCP)

      CHARACTER CP*(*)

      PARAMETER (NPARA = 1)

      REAL      RX(NPARA)
      LOGICAL   LCHREQ, LFIRST
      CHARACTER CPARAS(NPARA)*8
      CHARACTER CPARAL(NPARA)*40
      CHARACTER CMSG*80

      EXTERNAL  LCHREQ,LENC

      SAVE

      DATA      CPARAS( 1) / 'DFACT   ' /, RX( 1) / 1.2 /

      DATA      CPARAL( 1) / '****DFACT   ' /

      DATA      LFIRST / .TRUE. /


      NCP = NPARA

      RETURN
*-----------------------------------------------------------------------
      ENTRY UCRQID(CP, IDX)

      DO 10 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IDX = N
          RETURN
        END IF
   10 CONTINUE
      CMSG = 'PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E','UCRQID',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UCRQCP(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAS(IDX)
      ELSE
        CALL MSGDMP('E','UCRQCP','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UCRQCL(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAL(IDX)
      ELSE
        CALL MSGDMP('E','UCRQCL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UCRQVL(IDX, RPARA)

      IF (LFIRST) THEN
        CALL RTRGET('UC', CPARAS, RX, NPARA)
        CALL RLRGET(CPARAL, RX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        RPARA = RX(IDX)
      ELSE
        CALL MSGDMP('E','UCRQVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UCRSVL(IDX, RPARA)

      IF (LFIRST) THEN
        CALL RTRGET('UC', CPARAS, RX, NPARA)
        CALL RLRGET(CPARAL, RX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        RX(IDX) = RPARA
      ELSE
        CALL MSGDMP('E','UCRSVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UCRQIN(CP, IN)

      DO 20 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IN = N
          RETURN
        END IF
   20 CONTINUE

      IN = 0

      RETURN
      END
