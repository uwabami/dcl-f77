*-----------------------------------------------------------------------
*     UCXAYR : PLOT YEAR AXIS
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UCXAYR(CSIDE,JD0,ND)

      CHARACTER CSIDE*1

      PARAMETER (N=50)

      REAL      UX(N)
      CHARACTER CH(N)*4
      LOGICAL   LABEL,LUXCHK,LBTWN


      IF (.NOT.LUXCHK(CSIDE)) THEN
        CALL MSGDMP('E','UCXAYR','SIDE PARAMETER IS INVALID.')
      END IF
      IF (JD0.LT.0) THEN
        CALL MSGDMP('E','UCXAYR','FIRST DATE IS LESS THAN 0.')
      END IF
      IF (ND.LE.0) THEN
        CALL MSGDMP('E','UCXAYR','DATE LENGTH IS LESS THAN 0.')
      END IF

      CALL UXPAXS(CSIDE,2)

      NN=1
      UX(NN)=0
      CALL DATE13(JD0,IY0,MO0,ID0)

      DO 10 I=1,ND
        CALL DATEF3(I,IY0,MO0,ID0,IYI,MOI,IDI)
        CALL DATE32(IYI,MOI,IDI,ITDI)
        IYL=NDYEAR(IYI)
        IF (ITDI.EQ.IYL .OR. I.EQ.ND) THEN
          NN=NN+1
          IF (NN.GT.N) THEN
            CALL MSGDMP('E','UCXAYR','WORKING AREA IS NOT ENOUGH.')
          END IF
          UX(NN)=I
          WRITE(CH(NN-1),'(I4)') IYI
          CALL CLADJ(CH(NN-1)(1:4))
        END IF
   10 CONTINUE

      CALL SGQVPT(VX1,VX2,VY1,VY2)
      CALL UZRGET('RSIZEL2',RSIZE)
      NCHX=(VX2-VX1)/RSIZE

      CALL UZIGET('ICENTX'//CSIDE,ICENT)
      CALL UZIGET('IROTLX'//CSIDE,IROTA)
      CALL UZIGET('IROTCX'//CSIDE,IROTC)
      CALL UZLGET('LBTWN',LBTWN)

      CALL UZISET('ICENTX'//CSIDE,0)
      IF (NN*4.LE.NCHX) THEN
        CALL UZISET('IROTLX'//CSIDE,IROTC)
      ELSE
        CALL UZISET('IROTLX'//CSIDE,+1)
      END IF
      CALL UZLSET('LBTWN',.TRUE.)

      CALL UZRGET('RSIZET2',RTICK2)
      CALL UZRSET('RSIZET2',RTICK2*1.5)
      CALL UXPTMK(CSIDE,2,UX,NN)
      CALL UZRSET('RSIZET2',RTICK2)

      CALL UZLGET('LABELX'//CSIDE,LABEL)
      IF (LABEL) THEN
        CALL UXPLBL(CSIDE,2,UX,CH,4,NN)
      END IF

      CALL UZISET('ICENTX'//CSIDE,ICENT)
      CALL UZISET('IROTLX'//CSIDE,IROTA)
      CALL UZLSET('LBTWN',LBTWN)

      END
