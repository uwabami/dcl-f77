*-----------------------------------------------------------------------
*     UXAXDV : PLOT X-AXIS (SPECIFY DIVISION)
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UXAXDV(CSIDE,DX1,DX2)

      PARAMETER (N=200)

      REAL      UX(N)
      CHARACTER CSIDE*1

      LOGICAL   LABEL,LEPSL,LREQ,LRLE,LUXCHK


*     / CHECK ARGUMENTS /

      IF (.NOT.(LUXCHK(CSIDE))) THEN
        CALL MSGDMP('E','UXAXDV','SIDE PARAMETER IS INVALID.')
      END IF
      IF (DX1.LE.0) THEN
        CALL MSGDMP('E','UXAXDV','MINOR DIVISION IS LESS THAN 0.')
      END IF
      IF (DX2.LE.0) THEN
        CALL MSGDMP('E','UXAXDV','MAJOR DIVISION IS LESS THAN 0.')
      END IF
      IF (DX1.LE.DX2) THEN
        DZ1=DX1
        DZ2=DX2
      ELSE
        CALL MSGDMP('W','UXAXDV',
     +    'MINOR DIVISION IS GREATER THAN MAJOR DIVISION.')
        DZ1=DX2
        DZ2=DX1
        CALL MSGDMP('M','-CNT.-','DX1 AND DX2 WERE SWITCHED.')
      END IF
      FRC=DZ2/DZ1-NINT(DZ2/DZ1)
      IF (ABS(FRC).GT.0.0001) THEN
        CALL MSGDMP('W','UXAXDV',
     +    'MAJOR DIVISION IS NOT MULTIPLE OF MINOR DIVISION.')
      END IF

*     / GET & SET INTERNAL PARAMETERS /

      CALL GLLGET('LEPSL',LEPSL)
      CALL GLRGET('REPSL',REPSL)
      CALL GLLSET('LEPSL',.TRUE.)

*     / SET SCALING FOR OFFSET /

      CALL UXSOFF

*     / INQUIRE NORMALIZATION TRANSFORMATION /

      CALL SGQWND(UXMN,UXMX,UYMN,UYMX)

      UXMNW=MIN(UXMN,UXMX)
      UXMXW=MAX(UXMN,UXMX)

*     / PLOT HOLIZONTAL AXIS /

      CALL UXPAXS(CSIDE,2)

*     / GENERATE NUMBERS FOR MINOR TICKMARKS /

      NN=0
      RX=IRLE(UXMNW/DZ1)*DZ1
      IF (LREQ(UXMNW,RX)) THEN
        X=RX
      ELSE
        X=RX+DZ1
      END IF
   11 IF (.NOT.LRLE(X,UXMXW)) GO TO 10
        NN=NN+1
        IF (ABS(X).LT.DZ1*REPSL*NN) THEN
          X=0
        END IF
        UX(NN)=X
        X=X+DZ1
        GO TO 11
   10 CONTINUE

      IF (NN.EQ.0) THEN
        CALL MSGDMP('W','UXAXDV','THERE IS NO TICKMARK / LABEL.')
        GO TO 100
      ELSE IF (NN.GT.N) THEN
        CALL MSGDMP('E','UXAXDV','WORKING AREA IS NOT ENOUGH.')
      END IF

*     / IF DZ1.NE.DZ2 THEN PLOT MINOR TICKMARKS
*       ELSE SKIP TO THE PROCESS FOR MAJOR TICKMARKS /

      IF (DZ2.EQ.DZ1) GO TO 20

      CALL UXPTMK(CSIDE,1,UX,NN)

*     / GENERATE NUMBERS FOR MAJOR TICKMARKS AND LABELS /

      NN=0
      RX=IRLE(UXMNW/DZ2)*DZ2
      IF (LREQ(UXMNW,RX)) THEN
        X=RX
      ELSE
        X=RX+DZ2
      END IF
   16 IF (.NOT.LRLE(X,UXMXW)) GO TO 15
        NN=NN+1
        IF (ABS(X).LT.DZ2*REPSL*NN) THEN
          X=0
        END IF
        UX(NN)=X
        X=X+DZ2
        GO TO 16
   15 CONTINUE

   20 CONTINUE
      CALL UXPTMK(CSIDE,2,UX,NN)
      CALL UZLGET('LABELX'//CSIDE,LABEL)
      IF (LABEL) THEN
        CALL UXPNUM(CSIDE,1,UX,NN)
      END IF

*     / RESET INTERNAL PARAMETER /

  100 CALL GLLSET('LEPSL',LEPSL)

*     / RESET SCALING FOR OFFSET /

      CALL UXROFF

      END
