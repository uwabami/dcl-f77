*-----------------------------------------------------------------------
*     UXAXNM : PLOT X-AXIS (SPECIFY NUMBERS)
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UXAXNM(CSIDE,UX1,N1,UX2,N2)

      REAL      UX1(*),UX2(*)
      CHARACTER CSIDE*1

      LOGICAL   LABEL,LUXCHK


      IF (.NOT.(LUXCHK(CSIDE))) THEN
        CALL MSGDMP('E','UXAXNM','SIDE PARAMETER IS INVALID.')
      END IF

      CALL UXSOFF

      CALL UXPAXS(CSIDE,2)

      IF (N1.GE.1) THEN
        CALL UXPTMK(CSIDE,1,UX1,N1)
      END IF

      IF (N2.GE.1) THEN
        CALL UXPTMK(CSIDE,2,UX2,N2)
        CALL UZLGET('LABELX'//CSIDE,LABEL)
        IF (LABEL) THEN
          CALL UXPNUM(CSIDE,1,UX2,N2)
        END IF
      END IF

      CALL UXROFF

      END
