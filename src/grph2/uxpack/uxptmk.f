*-----------------------------------------------------------------------
*     UXPTMK : PLOT TICKMARKS
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UXPTMK(CSIDE,ISLCT,UX,N)

      REAL      UX(*)
      CHARACTER CSIDE*1

      LOGICAL   LUXCHK,LCHREQ
      CHARACTER CSLCT*1


      IF (.NOT.(LUXCHK(CSIDE))) THEN
        CALL MSGDMP('E','UXPTMK','SIDE PARAMETER IS INVALID.')
      END IF
      IF (.NOT.(0.LE.ISLCT .AND. ISLCT.LE.2)) THEN
        CALL MSGDMP('E','UXPTMK','''ISLCT'' IS INVALID.')
      END IF
      IF (N.LE.0) THEN
        CALL MSGDMP('E','UXPTMK','NUMBER OF POINTS IS INVALID.')
      END IF

      WRITE(CSLCT,'(I1)') ISLCT

      CALL UZRGET('ROFFX'//CSIDE,ROFFX)
      CALL UZRGET('ROFGX'//CSIDE,ROFGX)
      CALL UZIGET('INDEXT'//CSLCT,INDEX)
      CALL UZRGET('RSIZET'//CSLCT,RSIZE)
      CALL UZIGET('INNER',INNER)
      JSGN=SIGN(1,INNER)

      IF (.NOT.LCHREQ(CSIDE,'U')) THEN
        CALL SGQWND(UXMN,UXMX,UYMN,UYMX)
        IF (LCHREQ(CSIDE,'B')) THEN
          POSY=UYMN
          IFLAG=-1
        ELSE
          POSY=UYMX
          IFLAG=+1
        END IF
      ELSE
        CALL UZRGET('UYUSER',POSY)
        CALL UZIGET('IFLAG',IFLAG)
        IFLAG=SIGN(1,IFLAG)
      END IF

      RTICK=-RSIZE*JSGN*IFLAG

      IF (IFLAG.GE.0) THEN
        ROFFX=MAX(ROFGX+RTICK,ROFFX)
      ELSE
        ROFFX=MIN(ROFGX+RTICK,ROFFX)
      END IF

      CALL UXPTMZ(UX,N,POSY,ROFGX,RTICK,INDEX)

      CALL UZRSET('ROFFX'//CSIDE,ROFFX)

      END
