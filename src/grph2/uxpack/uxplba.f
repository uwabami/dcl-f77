*-----------------------------------------------------------------------
*     UXPLBA : PLOT LABELS ( AT THE POINTS )
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UXPLBA(UX,CH,NC,N,UPY,ROFFX,RSIZE,IROTA,ICENT,INDEX)

      REAL      UX(*)
      CHARACTER CH(*)*(*)

      LOGICAL   LCLIPZ


      IF (NC.LE.0) THEN
        CALL MSGDMP('E','UXPLBA',
     +       'CHARACTER LENGTH IS LESS THAN OR EQUAL TO ZERO.')
      END IF
      IF (N.LE.0) THEN
        CALL MSGDMP('E','UXPLBA','NUMBER OF POINTS IS INVALID.')
      END IF
      IF (RSIZE.LE.0) THEN
        CALL MSGDMP('E','UXPLBA','TEXT HEIGHT IS LESS THAN ZERO.')
      END IF
      IF (.NOT.(-1.LE.ICENT .AND. ICENT.LE.1)) THEN
        CALL MSGDMP('E','UXPLBA','CENTERING OPTION IS INVALID.')
      END IF
      IF (INDEX.LE.0) THEN
        CALL MSGDMP('E','UXPLBA','TEXT INDEX IS INVALID.')
      END IF

      CALL SGLGET('LCLIP',LCLIPZ)
      CALL SGLSET('LCLIP',.FALSE.)

      CALL SZTXOP(RSIZE,IROTA*90,ICENT,INDEX)
      DO 10 I=1,N
        LC = LENC(CH(I))
        CALL STFTRF(UX(I),UPY,VPX,VPY)
        VPY=VPY+ROFFX
        CALL SZTXZV(VPX,VPY,CH(I)(1:LC))
   10 CONTINUE
      CALL SZTXCL

      CALL SGLSET('LCLIP',LCLIPZ)

      END
