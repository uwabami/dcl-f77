*-----------------------------------------------------------------------
*     UXMTTL : PLOT MAIN (LARGE) TITLE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UXMTTL(CSIDE,CTTL,PX)

      CHARACTER CSIDE*1,CTTL*(*)

      LOGICAL   LUXCHK


      IF (.NOT.(LUXCHK(CSIDE))) THEN
        CALL MSGDMP('E','UXMTTL','SIDE PARAMETER IS INVALID.')
      END IF

      CALL UXPTTL(CSIDE,2,CTTL,PX)

      END
