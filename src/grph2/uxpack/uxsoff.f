*-----------------------------------------------------------------------
*     UXSOFF / UXROFF
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UXSOFF

      LOGICAL   LCALL,LOFF

      SAVE

      DATA      LCALL/.FALSE./

      FX(X)=XFCT*X+XOFF


*     / SCALING FACTOR FOR OFFSET /

      CALL UZLGET('LOFFSET',LOFF)
      IF (LOFF) THEN
        CALL UZRGET('XOFFSET',XOFF)
        CALL UZRGET('XFACT  ',XFCT)
      ELSE
        XOFF=0.0
        XFCT=1.0
      END IF

*     / INQUIRE NORMALIZATION TRANSFORMATION /

      CALL SGQWND(UXMN,UXMX,UYMN,UYMX)

      FXMN=FX(UXMN)
      FXMX=FX(UXMX)

*     / SET NORMALIZATION TRANSFORMATION /

      CALL SGSWND(FXMN,FXMX,UYMN,UYMX)
      CALL SGSTRF

      LCALL=.TRUE.

      RETURN
*-----------------------------------------------------------------------
      ENTRY UXROFF

      IF (.NOT.LCALL) THEN
        CALL MSGDMP('E','UXROFF','UXSOFF HAS NOT BEEN CALLED.')
      END IF

*     / RESET NORMALIZATION TRANSFORMATION /

      CALL SGSWND(UXMN,UXMX,UYMN,UYMX)
      CALL SGSTRF

      RETURN
      END
