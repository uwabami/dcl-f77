*-----------------------------------------------------------------------
*     UXSAXS : OFFSET FOR NEW AXIS
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UXSAXS(CSIDE)

      CHARACTER CSIDE*1

      LOGICAL   LUXCHK,LCHREQ


      IF (.NOT.LUXCHK(CSIDE)) THEN
        CALL MSGDMP('E','UXSAXS','SIDE PARAMETER IS INVALID.')
      END IF

      CALL UZRGET('ROFFX'//CSIDE,ROFFX)
      CALL UZRGET('RSIZET2',RSIZET)
      CALL UZRGET('RSIZEC2',RSIZEC)
      CALL UZRGET('PAD2',PAD)
      CALL UZIGET('INNER',INNER)
      JSGN=SIGN(1,INNER)

      IF (.NOT.LCHREQ(CSIDE,'U')) THEN
        IF (LCHREQ(CSIDE,'B')) THEN
          IFLAG=-1
        ELSE
          IFLAG=+1
        END IF
      ELSE
        CALL UZIGET('IFLAG',IFLAG)
        IFLAG=SIGN(1,IFLAG)
      END IF

      RTICK=-RSIZET*JSGN*IFLAG

      IF (IFLAG.GE.0) THEN
        ROFFX=MAX(ROFFX-RTICK,ROFFX)+RSIZEC*PAD
      ELSE
        ROFFX=MIN(ROFFX-RTICK,ROFFX)-RSIZEC*PAD
      END IF

      CALL UXSAXZ(CSIDE,ROFFX)

      END
