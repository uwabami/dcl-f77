*-----------------------------------------------------------------------
*     UXPLBB : PLOT LABELS ( BETWEEN THE POINTS )
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UXPLBB(UX,CH,NC,N,UPY,ROFFX,RSIZE,IROTA,ICENT,INDEX,
     +                  RBTWN,LBOUND,LBMSG)

      REAL      UX(*)
      LOGICAL   LBOUND,LBMSG
      CHARACTER CH(*)*(*)

      LOGICAL   LCLIPZ


      IF (NC.LE.0) THEN
        CALL MSGDMP('E','UXPLBB',
     +       'CHARACTER LENGTH IS LESS THAN OR EQUAL TO ZERO.')
      END IF
      IF (N.LE.1) THEN
        CALL MSGDMP('E','UXPLBB','NUMBER OF POINTS IS INVALID.')
      END IF
      IF (RSIZE.LE.0) THEN
        CALL MSGDMP('E','UXPLBB','TEXT HEIGHT IS LESS THAN ZERO.')
      END IF
      IF (.NOT.(-1.LE.ICENT .AND. ICENT.LE.1)) THEN
        CALL MSGDMP('E','UXPLBB','CENTERING OPTION IS INVALID.')
      END IF
      IF (INDEX.LE.0) THEN
        CALL MSGDMP('E','UXPLBB','TEXT INDEX IS INVALID.')
      END IF

      CALL SGLGET('LCLIP',LCLIPZ)
      CALL SGLSET('LCLIP',.FALSE.)

      CALL SZTXOP(RSIZE,IROTA*90,ICENT,INDEX)
      DO 10 I=1,N-1
        LC = LENC(CH(I))
        CALL SZQTXW(CH(I),LCZ,WXCH,WYCH)
        CALL STFTRF(UX(I),UPY,VPX1,VPY)
        CALL STFTRF(UX(I+1),UPY,VPX2,VPY)
        VWX=ABS(VPX2-VPX1)
        JROTA=MOD(IROTA,2)
        IF (JROTA.EQ.0) THEN
          CWX=RSIZE*WXCH
        ELSE
          CWX=RSIZE*WYCH
        END IF
        IF (CWX.GT.VWX .AND. LBOUND) THEN
          IF (LBMSG) THEN
            CALL MSGDMP('W','UXPLBB','SPACE FOR LABEL IS NOT ENOUGH.')
          END IF
          GO TO 10
        END IF
        VPX=(VPX1+VPX2)/2+(VWX-CWX)/2*RBTWN
        VPY=VPY+ROFFX
        CALL SZTXZV(VPX,VPY,CH(I)(1:LC))
   10 CONTINUE
      CALL SZTXCL

   15 CONTINUE

      CALL SGLSET('LCLIP',LCLIPZ)

      END
