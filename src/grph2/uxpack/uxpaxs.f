*-----------------------------------------------------------------------
*     LOWER LEVEL APPLICATIONS
*-----------------------------------------------------------------------
*     UXPAXS : PLOT HOLIZONTAL LINE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UXPAXS(CSIDE,ISLCT)

      CHARACTER CSIDE*1

      LOGICAL   LUXCHK,LCHREQ,LCLIPZ
      CHARACTER CSLCT*1


      IF (.NOT.(LUXCHK(CSIDE))) THEN
        CALL MSGDMP('E','UXPAXS','SIDE PARAMETER IS INVALID.')
      END IF
      IF (.NOT.(0.LE.ISLCT .AND. ISLCT.LE.2)) THEN
        CALL MSGDMP('E','UXPAXS','''ISLCT'' IS INVALID')
      END IF

      CALL UZRGET('ROFFX'//CSIDE,ROFFX)
      CALL UZRGET('ROFGX'//CSIDE,ROFGX)
      IF (ROFFX.NE.ROFGX) RETURN

      WRITE(CSLCT,'(I1)') ISLCT

      CALL SGLGET('LCLIP',LCLIPZ)
      CALL SGLSET('LCLIP',.FALSE.)
      CALL UZIGET('INDEXT'//CSLCT,INDEX)

      CALL SGQWND(UXMN,UXMX,UYMN,UYMX)
      IF (.NOT.LCHREQ(CSIDE,'U')) THEN
        IF (LCHREQ(CSIDE,'B')) THEN
          POSY=UYMN
        ELSE
          POSY=UYMX
        END IF
      ELSE
        CALL UZRGET('UYUSER',POSY)
      END IF

      CALL STFTRF(UXMN,POSY,VX1,VY1)
      CALL STFTRF(UXMX,POSY,VX2,VY2)
      CALL SGLNZV(VX1,VY1+ROFGX,VX2,VY2+ROFGX,INDEX)

      CALL SGLSET('LCLIP',LCLIPZ)

      END
