*-----------------------------------------------------------------------
*     UXSTTL : PLOT SUB (SMALL) TITLE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UXSTTL(CSIDE,CTTL,PX)

      CHARACTER CSIDE*1,CTTL*(*)

      LOGICAL   LUXCHK


      IF (.NOT.(LUXCHK(CSIDE))) THEN
        CALL MSGDMP('E','UXSTTL','SIDE PARAMETER IS INVALID.')
      END IF

      CALL UXPTTL(CSIDE,1,CTTL,PX)

      END
