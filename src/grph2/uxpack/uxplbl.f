*-----------------------------------------------------------------------
*     UXPLBL : PLOT LABELS
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UXPLBL(CSIDE,ISLCT,UX,CH,NC,N)

      REAL      UX(*)
      CHARACTER CSIDE*1,CH(*)*(*)

      LOGICAL   LBTWN,LUXCHK,LCHREQ
      CHARACTER CSLCT*1


      IF (.NOT.(LUXCHK(CSIDE))) THEN
        CALL MSGDMP('E','UXPLBL','SIDE PARAMETER IS INVALID.')
      END IF
      IF (.NOT.(0.LE.ISLCT .AND. ISLCT.LE.2)) THEN
        CALL MSGDMP('E','UXPLBL','''ISLCT'' IS INVALID.')
      END IF
      IF (NC.LE.0) THEN
        CALL MSGDMP('E','UXPLBL',
     +       'CHARACTER LENGTH IS LESS THAN OR EQUAL TO ZERO.')
      END IF
      IF (N.LE.0) THEN
        CALL MSGDMP('E','UXPLBL','NUMBER OF POINTS IS INVALID.')
      END IF

      WRITE(CSLCT,'(I1)') ISLCT

      CALL UZRGET('ROFFX'//CSIDE,ROFFX)
      CALL UZRGET('RSIZEL'//CSLCT,RSIZE)
      CALL UZIGET('ICENTX'//CSIDE,ICENT)
      CALL UZIGET('IROTLX'//CSIDE,IROTA)
      CALL UZIGET('INDEXL'//CSLCT,INDEX)
      CALL UZRGET('PAD1',PAD)
      CALL UZLGET('LBTWN',LBTWN)
      IF (LBTWN) THEN
        CALL UZRGET('RBTWN',RBTWN)
        NCMAX=N-1
      ELSE
        NCMAX=N
      END IF

      IF (.NOT.LCHREQ(CSIDE,'U')) THEN
        CALL SGQWND(UXMN,UXMX,UYMN,UYMX)
        IF (LCHREQ(CSIDE,'B')) THEN
          POSY=UYMN
          IFLAG=-1
        ELSE
          POSY=UYMX
          IFLAG=+1
        END IF
      ELSE
        CALL UZRGET('UYUSER',POSY)
        CALL UZIGET('IFLAG',IFLAG)
        IFLAG=SIGN(1,IFLAG)
      END IF

      JROTA=MOD(IROTA+2,4)-2
      IF (JROTA.EQ.-2) THEN
        JROTA=0
      END IF

      RLC=1
      DO 10 I=1,NCMAX
        CALL SZQTXW(CH(I),LCW,WXCH,WYCH)
        IF (JROTA.EQ.0 .AND. WYCH.GT.RLC) THEN
          RLC=WYCH
        ELSE IF (JROTA.NE.0 .AND. WXCH.GT.RLC) THEN
          RLC=WXCH
        END IF
   10 CONTINUE

      IC=JROTA*ICENT*IFLAG
      ROFFZ=ROFFX+RSIZE*(PAD+RLC*(1+IC)*0.5)*IFLAG
      ROFFX=ROFFX+RSIZE*(PAD+RLC)*IFLAG

      IF (LBTWN) THEN
        CALL UXPLBB(UX,CH,NC,N,POSY,ROFFZ,RSIZE,IROTA,ICENT,INDEX,
     +              RBTWN,.TRUE.,.FALSE.)
      ELSE
        CALL UXPLBA(UX,CH,NC,N,POSY,ROFFZ,RSIZE,IROTA,ICENT,INDEX)
      END IF

      CALL UZRSET('ROFFX'//CSIDE,ROFFX)

      END
