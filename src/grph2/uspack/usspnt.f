*-----------------------------------------------------------------------
*     USPACK SET POINTS                             S.Sakai    95/03/02
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USSPNT(N, X, Y)
      REAL  X(*), Y(*)
      REAL  XMIND(2), XMAXD(2), YMIND(2), YMAXD(2)

      CALL GLRGET('RUNDEF', RUNDEF)

*---------------------------- XMAX/XMIN --------------------------------

      IF( X(1) .NE. RUNDEF ) THEN
        CALL USRGET('XDTMIN', XMIND(1))
        CALL USRGET('XDTMAX', XMAXD(1))
        XMIND(2) = RMIN(X,N,1)
        XMAXD(2) = RMAX(X,N,1)

        IF(XMIND(1).NE.RUNDEF) THEN
          XDTMIN = RMIN(XMIND,2,1)
          XDTMAX = RMAX(XMAXD,2,1)
        ELSE
          XDTMIN = XMIND(2)
          XDTMAX = XMAXD(2)
        ENDIF
        CALL USRSET('XDTMIN', XDTMIN)
        CALL USRSET('XDTMAX', XDTMAX)
      ENDIF

*---------------------------- YMAX/YMIN --------------------------------

      IF( Y(1) .NE. RUNDEF ) THEN
        CALL USRGET('YDTMIN', YMIND(1))
        CALL USRGET('YDTMAX', YMAXD(1))
        YMIND(2) = RMIN(Y,N,1)
        YMAXD(2) = RMAX(Y,N,1)

        IF(YMIND(1).NE.RUNDEF) THEN
          YDTMIN = RMIN(YMIND,2,1)
          YDTMAX = RMAX(YMAXD,2,1)
        ELSE
          YDTMIN = YMIND(2)
          YDTMAX = YMAXD(2)
        ENDIF
        CALL USRSET('YDTMIN', YDTMIN)
        CALL USRSET('YDTMAX', YDTMAX)
      ENDIF

      END
