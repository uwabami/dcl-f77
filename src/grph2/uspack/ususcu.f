*-----------------------------------------------------------------------
*     USPACK AUTO SCALING ROUTINE (UNIFORM)           DCL 5.0  95/09/04
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USUSCU(CAXIS, UMIN,  UMAX,  VMIN,  VMAX,  MODE)

      PARAMETER(NS1=4, NS2=4)
      REAL        SC1(NS1), SC2(NS2)
      CHARACTER   CAXIS*1,  CFMT*8
      CHARACTER*8 CDUT, CDUL, COFF, CFAC, CMXD, CCFMT
      LOGICAL     LEPSL

      DATA SC1 / 1., 2., 5., 10./
     &     SC2 / 1., 2., 4., 10./

*------------------------- ARGUMENT CHECK ------------------------------

      IF(CAXIS.NE.'X' .AND. CAXIS.NE.'Y')
     &   CALL MSGDMP('E', 'USUSCU', 'INVALID CAXIS')

      IF(VMIN.GE.VMAX)
     &   CALL MSGDMP('E', 'USUSCU', 'VMIN>VMAX')

*-------------------------- PARAMETERS ---------------------------------

      CALL GLRGET('RUNDEF' , RUNDEF)
      CALL UZRGET('RSIZEL1', CW)

      IF(MODE.EQ.0) THEN
        CALL USIGET('NBLANK1'  , NB)
      ELSE
        CALL USIGET('NBLANK2'  , NB)
      ENDIF

*------------------------------- DUT -----------------------------------
      CDUT = 'D'//CAXIS//'T'
      CALL USRGET(CDUT, DUT)

      IF(DUT.EQ.RUNDEF) THEN
        RMIN = MIN(UMIN, UMAX)
        RMAX = MAX(UMIN, UMAX)
        CALL USURDT(RMIN, RMAX, VMIN, VMAX, DUT)
        CALL USRSET(CDUT, DUT)
      ENDIF

*------------------------------- DUL -----------------------------------

      CALL GLLGET('LEPSL', LEPSL)
      CALL GLLSET('LEPSL', .TRUE.)
      CALL GNSAVE
      CALL GNSBLK(SC1, NS1)
      CALL GNLE(DUT, BX, IEXP)

      RMIN = MIN(UMIN, UMAX)
      RMAX = MAX(UMIN, UMAX)
      U2V  = (VMAX-VMIN) / (RMAX-RMIN)

      CDUL = 'D'//CAXIS//'L'
      COFF = CAXIS//'OFF'
      CFAC = CAXIS//'FAC'
      CMXD = 'MXDGT'//CAXIS

      CALL USRGET(CDUL, DUL)
      CALL USRGET(COFF, UOFF)
      CALL USRGET(CFAC, UFAC)
      CALL USIGET(CMXD, MAXDGT)

      IF(DUL.EQ.RUNDEF) THEN
        DUL0 = DUT*SC1(2)
        DO 100 NTRY=1, 2
          IF(MODE.EQ.0) THEN
            UOFF0 = UOFF
            UFAC0 = UFAC
            CALL USZDGT(RMIN,  RMAX,  DUL0, MAXDGT,
     &                  UOFF0, UFAC0, NDGT, LDGT)
          ELSE
            NDGT=1
          ENDIF

          FAC = CW*(NDGT+NB)/(U2V*DUT)

          IF(BX.EQ.5.) THEN
            CALL GNSBLK(SC2, NS2)
            FAC = RGNGE(FAC)
          ELSE
            CALL GNSBLK(SC1, NS1)
            FAC = RGNGE(FAC)
          ENDIF
          IF(FAC.LT.2.) FAC = 2.
          IFAC = INT(FAC+0.1)
          DUL  = DUT * IFAC
          DUL  = MAX(DUL, DUL0)
          IF(IFAC.LT.10) GOTO 200
          DUL0 = DUL/2.
  100   CONTINUE
  200   CONTINUE

        CALL USRSET(CDUL, DUL)
      ENDIF

*------------------------------ CFMT ----------------------------------

      CCFMT = 'C'//CAXIS//'FMT'
      CALL USCGET(CCFMT, CFMT)

      IF (CFMT.EQ.' ') THEN
        DVL = DUL*U2V
        IF(MODE.EQ.0) THEN
          MAXD = MIN(MAXDGT, INT(DVL/CW) - NB)
        ELSE
          MAXD = MAXDGT
        ENDIF

        CALL USZDGT(RMIN, RMAX, DUL,  MAXD,
     &              UOFF, UFAC, NDGT, LDGT)

        IF(LDGT.EQ.0) THEN
          WRITE(CFMT, 500) NDGT
        ELSE
          WRITE(CFMT, 510) NDGT, LDGT
        ENDIF
  500   FORMAT('(I', I1, ')')
  510   FORMAT('(F', I1, '.', I1, ')')

        CALL USRSET(COFF, UOFF)
        CALL USRSET(CFAC, UFAC)
        CALL USCSET(CCFMT, CFMT)
      ENDIF

      CALL GNRSET
      CALL GLLSET('LEPSL', LEPSL)

      RETURN
      END
