*-----------------------------------------------------------------------
*     USINIT                                          DCL 5.0  95/09/04
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USINIT

      CALL GLRGET('RUNDEF', RUNDEF)
      CALL GLIGET('IUNDEF', IUNDEF)
      CALL USIGET('IRESET', IRESET)

      IF(IRESET.LT.0 .OR. IRESET.GT.2)
     +   CALL MSGDMP('E', 'USINIT', 'INVALID VALUE OF ''IRESET''.')

*-----------------------------------------------------------------------

      CALl UZRGET( 'RSIZEL1',  SIZEL)
      OFFSET = SIZEL*0.86

      CALL USRSET( 'SOFFXTR',  OFFSET)
      CALL USRSET( 'SOFFXBR',  OFFSET)
      CALL USRSET( 'SOFFXUR',  OFFSET)

      CALL USRSET( 'SOFFXTL', -OFFSET)
      CALL USRSET( 'SOFFXBL', -OFFSET)
      CALL USRSET( 'SOFFXUL', -OFFSET)

      CALL USRSET( 'SOFFYRT',  OFFSET)
      CALL USRSET( 'SOFFYLT',  OFFSET)
      CALL USRSET( 'SOFFYUT',  OFFSET)

      CALL USRSET( 'SOFFYRB', -OFFSET)
      CALL USRSET( 'SOFFYLB', -OFFSET)
      CALL USRSET( 'SOFFYUB', -OFFSET)

      CALL USRSET( 'ROFFXT', 0.)
      CALL USRSET( 'ROFFXB', 0.)
      CALL USRSET( 'ROFFYR', 0.)
      CALL USRSET( 'ROFFYL', 0.)

      CALL USRSTX( 'XOFF',     RUNDEF)
      CALL USRSTX( 'YOFF',     RUNDEF)
      CALL USRSTX( 'XFAC',     RUNDEF)
      CALL USRSTX( 'YFAC',     RUNDEF)
      CALL USRSTX( 'DXT',      RUNDEF)
      CALL USRSTX( 'DYT',      RUNDEF)
      CALL USRSTX( 'DXL',      RUNDEF)
      CALL USRSTX( 'DYL',      RUNDEF)

      CALL USISTX( 'NLBLX',    IUNDEF)
      CALL USISTX( 'NLBLY',    IUNDEF)
      CALL USISTX( 'NTICKSX',  IUNDEF)
      CALL USISTX( 'NTICKSY',  IUNDEF)
      CALL USISTX( 'ITYPEX',   IUNDEF)
      CALL USISTX( 'ITYPEY',   IUNDEF)

      CALL USRSET( 'XDTMIN',   RUNDEF)
      CALL USRSET( 'XDTMAX',   RUNDEF)
      CALL USRSET( 'YDTMIN',   RUNDEF)
      CALL USRSET( 'YDTMAX',   RUNDEF)

      CALL USCSTX('CXFMT', ' ')
      CALL USCSTX('CYFMT', ' ')

      IF(IRESET.GE.1) THEN
        CALL USCSTX('CXTTL', ' ')
        CALL USCSTX('CYTTL', ' ')
        CALL USCSTX('CXUNIT', ' ')
        CALL USCSTX('CYUNIT', ' ')
      ENDIF

      IF(IRESET.GE.2) THEN
        CALL USCSTX('CXSIDE', 'BT')
        CALL USCSTX('CYSIDE', 'LR')
        CALL USCSTX('CXSPOS', 'R ')
        CALL USCSTX('CYSPOS', 'T ')
        CALL USCSTX('CBLKT ', '()')

*-------------------- PARAMETERS FOR USDAXS ----------------------------

        CALL USLSTX( 'LXINV',   .FALSE.)
        CALL USLSTX( 'LYINV',   .FALSE.)
        CALL USLSTX( 'LMATCH',  .FALSE.)
        CALL USRSTX( 'RMRGN',    9.524)

*-------------------- PARAMETERS FOR USUSCU ----------------------------

        CALL USRSTX( 'TFACT',    2.)
        CALL USISTX( 'MXDGTX',   4)
        CALL USISTX( 'MXDGTY',   4)
        CALL USISTX( 'NBLANK1',  1)
        CALL USISTX( 'NBLANK2',  2)

*---------------- PARAMETERS FOR USXSUB & USYSUB -----------------------

        CALL USISTX( 'MXDGTSX',  6)
        CALL USISTX( 'MXDGTSY',  6)
        CALL USLSTX( 'LPRTCT',  .FALSE.)
      ENDIF

      END
