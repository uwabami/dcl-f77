*-----------------------------------------------------------------------
*     USPACK DRAW Y-AXIS (LOG)                        DCL 5.0  95/09/04
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USYAXL(CYS)
      CHARACTER CYS*(*), CUNIT*32, CYSUB*32, CSBLBL*32, CPOS*1, CP*8
      LOGICAL LAB1
      EXTERNAL CSBLBL, LENZ

      CALL USIGET('NLBLY'  , NLBL)
      CALL USIGET('NTICKSY', NTICKS)
      CALL USIGET('ITYPEY' , ITYPEY)
      CALL USRGET('YFAC'   , YFAC)
      CALL USCGET('CYUNIT' , CUNIT)

      CALL ULIGET('IYTYPE', IT)
      CALL ULISET('IYTYPE', ITYPEY)

      CALL SGQTRN(ITR)
      IF(.NOT.(ITR.EQ.2 .OR. ITR.EQ.4))
     &   CALL MSGDMP('E', 'USXAXL', 'INVALID TRANSFORMATION NUMBER.')
      CALL SGQWND(XMIN, XMAX, YMIN, YMAX)

      YMINA = YMIN*YFAC
      YMAXA = YMAX*YFAC
      CALL SGSWND(XMIN, XMAX, YMINA, YMAXA)
      CALL SGSTRF

*---------------------------- Y-AXIS -----------------------------------

      NYS = MIN(LEN(CYS), 2)
      DO 100 I=1, NYS
        CALL ULYLOG(CYS(I:I), NLBL, NTICKS)
        CP = 'LABELY'//CYS(I:I)
        CALL UZLGET(CP, LAB1)
        IF(LAB1) THEN
          CYSUB = CSBLBL(YFAC, 0., CUNIT)
          IF(LENZ(CYSUB).NE.0) THEN
            CALL USCGET('CYSPOS', CPOS)
            CALL USYSUB(CYS(I:I), CPOS, CYSUB, 3.)
          ENDIF
        ENDIF
  100 CONTINUE

*-----------------------------------------------------------------------

      CALL SGSWND(XMIN, XMAX, YMIN, YMAX)
      CALL SGSTRF
      CALL ULISET('IYTYPE', IT)

      RETURN
      END
