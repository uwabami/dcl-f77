*-----------------------------------------------------------------------
*     USPACK OFFSET CHEK (Y-AXIS)                    S. Sakai  90/08/18
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USYOFF(CYS)
      CHARACTER*1 CYS, CP*8

      IF(CYS.EQ.'T' .OR. CYS.EQ.'B') THEN
        CP = 'ROFFY'//CYS
        CALL USRGET(CP, SOFF)
        CALL UZRGET(CP, ROFF)
        ROFF = MAX(ROFF, SOFF)
        CALL UZRGET(CP, ROFF)
      ELSE
        CALL MSGDMP('E', 'USYOFF', 'INVALID CYS')
      ENDIF

      RETURN
      END
