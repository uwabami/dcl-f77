*-----------------------------------------------------------------------
*    USPACK SETTING TITLE                             S.Sakai  92/02/29
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USSTTL(CXTTL, CXUNIT, CYTTL, CYUNIT)
      CHARACTER*(*) CXTTL, CXUNIT, CYTTL, CYUNIT

      CALL USCSTX('CXTTL' , CXTTL)
      CALL USCSTX('CXUNIT', CXUNIT)
      CALL USCSTX('CYTTL' , CYTTL)
      CALL USCSTX('CYUNIT', CYUNIT)

      RETURN
      END
