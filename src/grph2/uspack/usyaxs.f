*-----------------------------------------------------------------------
*     USPACK DRAW Y-AXIS                              S.Sakai  99/10/09
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USYAXS(CSIDE)
      CHARACTER  CSIDE*(*), CS*1
      EXTERNAL   LENZ

      NCS = LENZ(CSIDE)
      DO 200 I=1, NCS
        CS = CSIDE(I:I)
        CALL CUPPER(CS)
        IF(CS.EQ.'U') CS='V'
        CALL USAXSC(CS)
  200 CONTINUE

      END
