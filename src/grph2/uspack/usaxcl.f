*-----------------------------------------------------------------------
*     USPACK AXIS (CALENDAR)                          S.Sakai  99/10/05
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USAXCL(CSIDE, JD0, CTYPE, ND)

      CHARACTER  CSIDE*(*), CTYPE*(*), CS, CT
      EXTERNAL   LENZ
      LOGICAL    LOFF

      NCS = LENZ(CSIDE)

      CALL UZLGET('LOFFSET', LOFF)
      CALL UZLSET('LOFFSET', .FALSE.)

      DO 100 I=1, NCS
        CS = CSIDE(I:I)
        CALL CUPPER(CS)

        IF(CS.EQ.'T' .OR. CS.EQ.'B' .OR. CS.EQ.'H') THEN
          IF(CS.EQ.'H') CS='U'

          IF(ND.EQ.0) THEN
            CALl SGQWND(XMIN,XMAX,YMIN,YMAX)
            ND0 = ABS(NINT(XMAX-XMIN))
          ELSE
            ND0 = ND
          END IF

          CALL USXINZ(CS,FACTOT,OFFSET)
          NTYP = LENZ(CTYPE)
          DO 200 J=1, NTYP
            CT = CTYPE(J:J)
            CALL CUPPER(CT)
            IF(CT.EQ.'Y')  CALL UCXAYR(CS, JD0, ND0)
            IF(CT.EQ.'M')  CALL UCXAMN(CS, JD0, ND0)
            IF(CT.EQ.'D')  CALL UCXADY(CS, JD0, ND0)
 200      CONTINUE
          CALL USXTLZ

        ELSEIF(CS.EQ.'L' .OR. CS.EQ.'R' .OR. CS.EQ.'V') THEN
          IF(CS.EQ.'V') CS='U'

          IF(ND.EQ.0) THEN
            CALl SGQWND(XMIN,XMAX,YMIN,YMAX)
            ND0 = ABS(NINT(YMAX-YMIN))
          ELSE
            ND0 = ND
          END IF

          CALL USYINZ(CS, FACTOR, OFFSET)
          NTYP = LENZ(CTYPE)
          DO 250 J=1, NTYP
            CT = CTYPE(J:J)
            CALL CUPPER(CT)
            IF(CT.EQ.'Y')  CALL UCYAYR(CS, JD0, ND0)
            IF(CT.EQ.'M')  CALL UCYAMN(CS, JD0, ND0)
            IF(CT.EQ.'D')  CALL UCYADY(CS, JD0, ND0)
 250      CONTINUE
          CALL USYTLZ
        ENDIF

  100 CONTINUE
      CALL UZLSET('LOFFSET', LOFF)

      END
