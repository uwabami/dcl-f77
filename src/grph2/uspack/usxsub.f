*-----------------------------------------------------------------------
*     USPACK SUB-LABEL (X)                            S.Sakai  90/08/18
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USXSUB(CXA, CYA, CLABEL, RLBL)

      CHARACTER CLABEL*(*), CXS*1, CYS*1, CXA*1, CYA*1, CP*8
      LOGICAL   LPRTCT,LCLIPZ


      CXS = CXA
      CYS = CYA
      CALL CUPPER(CXS)
      CALL CUPPER(CYS)

      IF(CYS.NE.'L' .AND. CYS.NE.'R')
     #   CALL MSGDMP('E', 'USXSUB', 'INVALID CYS')
      IF(CXS.NE.'T' .AND. CXS.NE.'B'.AND. CXS.NE.'U')
     #   CALL MSGDMP('E', 'USXSUB', 'INVALID CXS')

      CP = 'ROFFX' //CXS
      CALL UZRGET(CP, ROFFX )
      CP = 'ICENTX'//CXS
      CALL UZIGET(CP, ICENTX)
      CP = 'IROTLX'//CXS
      CALL UZIGET(CP, IROTLX)
      CALL UZRGET('RSIZEL1', RSIZEL)
      CALL UZRGET('RSIZET2', RSIZET)
      CALL UZIGET('INDEXL1', INDEX )
      CALL UZIGET('INNER'  , INNER )
      CALL UZRGET('PAD1'   , PAD1  )

      CALL SZQTXW(CLABEL, NCH, WSUB, HSUB)
      CALL SGQVPT(VXMIN, VXMAX, VYMIN, VYMAX)

      IROTLY = MOD(IROTLY, 4)
      WSUB = WSUB*RSIZEL
      WLBL = RLBL*RSIZEL
      WPAD = PAD1*RSIZEL

      IC = 1
      IF(IROTLX.GE.2) IC = -1
      ICX = IC*ICENTX

*======================= < HORIZONTAL LABEL > ==========================

      IF(IROTLX.EQ.0 .OR. IROTLX.EQ.2) THEN
        IF(CYS.EQ.'R') THEN
          ICENTX = IC
          POSX = VXMAX - WLBL/2*(ICX-1)
        ELSE
          ICENTX = -IC
          POSX = VXMIN - WLBL/2*(ICX+1)
        ENDIF

        IF(CXS.EQ.'B') THEN
          POSY  = VYMIN
          IFLAG = -1
        ELSEIF(CXS.EQ.'T') THEN
          POSY  = VYMAX
          IFLAG = +1
        ELSE
          CALL UZRGET('UYUSER', UYUSR)
          CALL UZIGET('IFLAG', IFLAG)
          IFLAG = SIGN(1, IFLAG)
          CALL STFTRF(1., UYUSR, VX, POSY)
        ENDIF
        ROFFX = ROFFX + RSIZEL*IFLAG*(1.+PAD1)
        POSY  = POSY  + ROFFX - RSIZEL*IFLAG/2.
        CP = 'ROFFX' //CXS
        CALL UZRSET(CP, ROFFX )

*======================= < VERTICAL LABEL > ============================

      ELSE
        CALL USIGET('MXDGTSX', MXDGT )
        CALL USLGET('LPRTCT' , LPRTCT)
        CP = 'SOFFX'//CXS//CYS
        CALL USRGET(CP, RSOFF )
        IF(LPRTCT .AND. CXS.EQ.'U')
     #    CALL MSGDMP('M', 'USXSUB', 'Y-LABEL REGION IS NOT PROTECTED.')

        IF(CYS .EQ. 'R') THEN
          POSX  = VXMAX + RSOFF + RSIZEL*(0.5+PAD1)
          RSOFF = RSOFF + RSIZEL*(1.+PAD1)
        ELSE
          POSX  = VXMIN + RSOFF - RSIZEL*(0.5+PAD1)
          RSOFF = RSOFF - RSIZEL*(1.+PAD1)
        ENDIF

        CP = 'SOFFX'//CXS//CYS
        CALL USRSET(CP, RSOFF)

        CP = 'ROFFY'//CYS
        CALL USRGET(CP, ROFFY)

        ROFFY = MAX(RSOFF, ROFFY)

*-----------------------------------------------------------------------
*       IFLAG =  1 : LABEL IS RIGHT SIDE OF THE AXIS.
*       IFLAG = -1 : LABEL IS LEFT  SIDE OF THE AXIS.

        CP = 'ROFGX'//CXS
        CALL UZRGET(CP , ROFG)
        IF(INNER.LT.0) WPAD = WPAD - INNER*RSIZET
        IF(CXS.EQ.'B') THEN
          IFLAG = -1
          VYP  = -(VYMIN+ROFG)
        ELSEIF(CXS.EQ.'T') THEN
          IFLAG = 1
          VYP  = VYMAX+ROFG
        ELSEIF(CXS.EQ.'U') THEN
          CALL UZRGET('UYUSER', UYUSR)
          CALL UZIGET('IFLAG', IFLAG)
          CALL STFTRF(1., UYUSR, VX, VYP)
          VYP = VYP + ROFG
          CP = 'ROFFY'//CYS
          CALL USRSET(CP, ROFFY)
        ENDIF

        POSY = VYP + WPAD + WLBL*(ICX*IFLAG+1)/2.
        YMIN = VYP + WPAD
        YMAX = VYP + WPAD + MXDGT*RSIZEL

*------------------------ boundary check -------------------------------

        YLMIN = POSY - WSUB*(ICX*IFLAG+1)/2.
        IF(YLMIN.LT.YMIN) THEN
          ICX    = -IFLAG
          POSY   = YMIN
        ENDIF

        YLMAX = POSY - WSUB*(ICX*IFLAG-1)/2.
        IF(.NOT.LPRTCT .AND. YLMAX.GT.YMAX) THEN
          ICX    = IFLAG
          POSY   = YMAX
        ENDIF

        YLMIN = POSY - WSUB*(ICX*IFLAG+1)/2.
        IF(YLMIN.LT.VYP) THEN
          IF(CXS.EQ.'U') THEN
            ICX = 0
            POSY = (VYP+YMAX)/2.
          ELSE
            CP = 'ROFFY'//CYS
            CALL USRSET(CP, ROFFY)
          ENDIF
        ENDIF

        ICENTX = ICX/IC
        POSY = ABS(POSY)
      ENDIF

      CALL SGLGET('LCLIP',LCLIPZ)
      CALL SGLSET('LCLIP',.FALSE.)
      CALL SGTXZV(POSX, POSY, CLABEL, RSIZEL, IROTLX*90, ICENTX, INDEX)
      CALL SGLSET('LCLIP',LCLIPZ)

      RETURN
      END
