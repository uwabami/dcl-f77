*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USYINZ(CSA, FACA, OFFA)
      CHARACTER CSA, CS, CPOS
      CHARACTER*32  CSBLBL, CTTL, CTITLE, CUNIT, CSUB
      CHARACTER*16  CMIN, CMAX, CFMT0, CFMT1
      LOGICAL LOFF, LSUB, LABEL

      SAVE OFF0, FAC0, OFF1, FAC1, CFMT0, CFMT1, CS, FACTOR, OFFSET

      CALL GLRGET('RUNDEF',  RUNDEF)

      CS = CSA
      CALL UZRGET('ROFFY'//CS,ROFF)
      CALL UZRGET('ROFGY'//CS,ROFG)
      IF (ROFF.NE.ROFG)  CALL UYSAXS(CS)

      CALl UZRGET('RSIZEL1', SIZEL)
      CALL USRSET('SOFFY'//CS//'T',  SIZEL*0.86)
      CALL USRSET('SOFFY'//CS//'B', -SIZEL*0.86)

      CALL UZLGET('LOFFSET', LOFF)
      IF(LOFF) THEN
        CALL UZRGET('YOFFSET', OFF0)
        CALL UZRGET('YFACT',   FAC0)
      ELSE
        OFF0 = 0.
        FAC0 = 1.
      END IF

      CALL USRGET('YOFF',  OFF1)
      CALL USRGET('YFAC',  FAC1)
      IF(OFF1.EQ.RUNDEF) OFF1 = 0.
      IF(FAC1.EQ.RUNDEF) FAC1 = 1.
      OFFA = OFF1
      FACA = FAC1

      FACTOR = FAC0/FAC1
      OFFSET = (OFF0-OFF1)/FAC1
      CALL UZLSET('LOFFSET', .TRUE.)
      CALL UZRSET('YOFFSET', OFFSET)
      CALL UZRSET('YFACT',   FACTOR)

      CALL UZCGET('CYFMT', CFMT0)
      CALL USCGET('CYFMT', CFMT1)
      IF(CFMT1 .EQ. ' ') CFMT1 = CFMT0
      CALL UZCSET('CYFMT', CFMT1)

      RETURN
*-------------------------------------------------------------
      ENTRY USYTLZ

      CALL SGQWND(XMIN,XMAX,YMIN,YMAX)

      CALL UZLGET('LABELY'//CS, LABEL)
      CALL USCGET('CYUNIT', CUNIT)
      CALL USCGET('CYTTL ', CTTL)

      IF(LABEL) THEN
        CSUB = CSBLBL(FAC1, OFF1, CUNIT)
        CALL USLGET('LYSUB', LSUB)
        IF(LENZ(CSUB).NE.0) THEN
          IF(LSUB) THEN
            CALL CHVAL(CFMT1,YMIN*FACTOR+OFFSET,CMIN)
            CALL CHVAL(CFMT1,YMAX*FACTOR+OFFSET,CMAX)
            RLEN = MAX(LENZ(CMIN), LENZ(CMAX))

            CALL USCGET('CYSPOS', CPOS)
            CALL USYSUB(CS, CPOS, CSUB, RLEN)
            CTITLE = CTTL
          ELSE
            NLT = LENZ(CTTL)
            CTITLE = CTTL(1:NLT+1) // CSUB
          ENDIF
        ELSE
          CTITLE = CTTL
        ENDIF

        CALL CLADJ(CTITLE)
        IF(LENZ(CTITLE).NE.0) CALL UYSTTL(CS, CTITLE, 0.)
      ENDIF

      CALL UZRSET('YOFFSET', OFF0)
      CALL UZRSET('YFACT',   FAC0)
      CALL UZCSET('CYFMT',   CFMT0)

      END
