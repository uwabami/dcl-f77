*-----------------------------------------------------------------------
*     USRQNP / USRQID / USRQCP / USRQVL / USRSVL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USRQNP(NCP)

      CHARACTER CP*(*)

      PARAMETER (NPARA  = 30)
      PARAMETER (RUNDEF = -999.)

      REAL      RX(NPARA)
      LOGICAL   LCHREQ, LFIRST
      CHARACTER CPARAS(NPARA)*8
      CHARACTER CPARAL(NPARA)*40
      CHARACTER CMSG*80

      EXTERNAL  LCHREQ, LENC

      SAVE

* ---- SHORT NAME ----
*     / PARAMETERS FOR USDAXS /
      DATA      CPARAS( 1) / 'RMRGN   ' /, RX( 1) /  9.524 /
*     / PARAMETERS FOR USUSCU /
      DATA      CPARAS( 2) / 'XOFF    ' /, RX( 2) / RUNDEF /
      DATA      CPARAS( 3) / 'YOFF    ' /, RX( 3) / RUNDEF /
      DATA      CPARAS( 4) / 'XFAC    ' /, RX( 4) / RUNDEF /
      DATA      CPARAS( 5) / 'YFAC    ' /, RX( 5) / RUNDEF /
      DATA      CPARAS( 6) / 'DXT     ' /, RX( 6) / RUNDEF /
      DATA      CPARAS( 7) / 'DYT     ' /, RX( 7) / RUNDEF /
      DATA      CPARAS( 8) / 'DXL     ' /, RX( 8) / RUNDEF /
      DATA      CPARAS( 9) / 'DYL     ' /, RX( 9) / RUNDEF /
      DATA      CPARAS(10) / 'TFACT   ' /, RX(10) / 2. /
*     / FOLLOWING 20 PARAMETERS ARE RESET BY USINIZ /
      DATA      CPARAS(11) / 'SOFFXTR ' /, RX(11) /  0.018 /
      DATA      CPARAS(12) / 'SOFFXBR ' /, RX(12) /  0.018 /
      DATA      CPARAS(13) / 'SOFFXUR ' /, RX(13) /  0.018 /
      DATA      CPARAS(14) / 'SOFFXTL ' /, RX(14) / -0.018 /
      DATA      CPARAS(15) / 'SOFFXBL ' /, RX(15) / -0.018 /
      DATA      CPARAS(16) / 'SOFFXUL ' /, RX(16) / -0.018 /
      DATA      CPARAS(17) / 'SOFFYRT ' /, RX(17) /  0.018 /
      DATA      CPARAS(18) / 'SOFFYLT ' /, RX(18) /  0.018 /
      DATA      CPARAS(19) / 'SOFFYUT ' /, RX(19) /  0.018 /
      DATA      CPARAS(20) / 'SOFFYRB ' /, RX(20) / -0.018 /
      DATA      CPARAS(21) / 'SOFFYLB ' /, RX(21) / -0.018 /
      DATA      CPARAS(22) / 'SOFFYUB ' /, RX(22) / -0.018 /
      DATA      CPARAS(23) / 'ROFFXT  ' /, RX(23) /  0. /
      DATA      CPARAS(24) / 'ROFFXB  ' /, RX(24) /  0. /
      DATA      CPARAS(25) / 'ROFFYR  ' /, RX(25) /  0. /
      DATA      CPARAS(26) / 'ROFFYL  ' /, RX(26) /  0. /

      DATA      CPARAS(27) / 'XDTMIN  ' /, RX(27) /  RUNDEF /
      DATA      CPARAS(28) / 'XDTMAX  ' /, RX(28) /  RUNDEF /
      DATA      CPARAS(29) / 'YDTMIN  ' /, RX(29) /  RUNDEF /
      DATA      CPARAS(30) / 'YDTMAX  ' /, RX(30) /  RUNDEF /

* ---- LONG NAME ----
*     / PARAMETERS FOR USDAXS /
      DATA      CPARAL( 1) / 'MARGIN_WIDTH' /
*     / PARAMETERS FOR USUSCU /
      DATA      CPARAL( 2) / 'X_LABEL_OFFSET' /
      DATA      CPARAL( 3) / 'Y_LABEL_OFFSET' /
      DATA      CPARAL( 4) / 'X_LABEL_FACTOR' /
      DATA      CPARAL( 5) / 'Y_LABEL_FACTOR' /
      DATA      CPARAL( 6) / 'X_TICK_INTERVAL' /
      DATA      CPARAL( 7) / 'Y_TICK_INTERVAL' /
      DATA      CPARAL( 8) / 'X_LABEL_INTERVAL' /
      DATA      CPARAL( 9) / 'Y_LABEL_INTERVAL' /
      DATA      CPARAL(10) / 'MAX_TICK_INTERVAL' /
*     / FOLLOWING 20 PARAMETERS ARE RESET BY USINIZ /
      DATA      CPARAL(11) / '****SOFFXTR ' /
      DATA      CPARAL(12) / '****SOFFXBR ' /
      DATA      CPARAL(13) / '****SOFFXUR ' /
      DATA      CPARAL(14) / '****SOFFXTL ' /
      DATA      CPARAL(15) / '****SOFFXBL ' /
      DATA      CPARAL(16) / '****SOFFXUL ' /
      DATA      CPARAL(17) / '****SOFFYRT ' /
      DATA      CPARAL(18) / '****SOFFYLT ' /
      DATA      CPARAL(19) / '****SOFFYUT ' /
      DATA      CPARAL(20) / '****SOFFYRB ' /
      DATA      CPARAL(21) / '****SOFFYLB ' /
      DATA      CPARAL(22) / '****SOFFYUB ' /
      DATA      CPARAL(23) / '****ROFFXT  ' /
      DATA      CPARAL(24) / '****ROFFXB  ' /
      DATA      CPARAL(25) / '****ROFFYR  ' /
      DATA      CPARAL(26) / '****ROFFYL  ' /

      DATA      CPARAL(27) / '****XDTMIN  ' /
      DATA      CPARAL(28) / '****XDTMAX  ' /
      DATA      CPARAL(29) / '****YDTMIN  ' /
      DATA      CPARAL(30) / '****YDTMAX  ' /

      DATA      LFIRST / .TRUE. /


      NCP = NPARA

      RETURN
*-----------------------------------------------------------------------
      ENTRY USRQID(CP, IDX)

      DO 10 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IDX = N
          RETURN
        END IF
   10 CONTINUE
      CMSG = 'PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E','USRQID',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY USRQCP(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAS(IDX)
      ELSE
        CALL MSGDMP('E','USRQCP','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY USRQCL(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAL(IDX)
      ELSE
        CALL MSGDMP('E','USRQCL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY USRQVL(IDX, RPARA)

      IF (LFIRST) THEN
        CALL RTRGET('US', CPARAS, RX, NPARA)
        CALL RLRGET(CPARAL, RX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        RPARA = RX(IDX)
      ELSE
        CALL MSGDMP('E','USRQVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY USRSVL(IDX, RPARA)

      IF (LFIRST) THEN
        CALL RTRGET('US', CPARAS, RX, NPARA)
        CALL RLRGET(CPARAL, RX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        RX(IDX) = RPARA
      ELSE
        CALL MSGDMP('E','USRSVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY USRQIN(CP, IN)

      DO 20 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IN = N
          RETURN
        END IF
   20 CONTINUE

      IN = 0

      RETURN
      END
