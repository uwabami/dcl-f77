*-----------------------------------------------------------------------
*     USPACK ROUND UMIN AND UMAX (LOG)               S. Sakai  90/03/04
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USURDL(UMIN, UMAX, VMIN, VMAX)

      LOGICAL LEPSL

      REAL      SC(4)
      DATA      SC / 1., 2., 5., 10./

*------------------------- ARGUMENT CHECK ------------------------------

      IF(UMIN.LE.0. .OR. UMAX.LE.0.)
     &   CALL MSGDMP('E', 'USURDL', 'NEGATIVE UMIN OR UMAX.')

      IF(UMIN.GT.UMAX)
     &   CALL MSGDMP('E', 'USURDL', 'UMIN > UMAX.')

*-----------------------------------------------------------------------

      CALL GLLGET('LEPSL', LEPSL)
      CALL GLLSET('LEPSL', .TRUE.)

      RLMAX = LOG10(UMAX)
      RLMIN = LOG10(UMIN)
      NLOG  = IRGE(RLMAX) - IRLE(RLMIN)

      IF(NLOG.GE.4) THEN
        UMAX = 1.D1**IRGE(RLMAX)
        UMIN = 1.D1**IRLE(RLMIN)
      ELSE
        CALL GNSAVE
        CALL GNSBLK(SC, 4)
        CALL GNGE(UMAX, BUMAX, IPMAX)
        CALL GNLE(UMIN, BUMIN, IPMIN)
        UMAX = BUMAX*1.D1**IPMAX
        UMIN = BUMIN*1.D1**IPMIN
        CALL GNRSET
      ENDIF

      CALL GLLSET('LEPSL', LEPSL)

      RETURN
      END
