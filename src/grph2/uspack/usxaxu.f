*-----------------------------------------------------------------------
*     USPACK DRAW X-AXIS (UNIFORM)                    DCL 5.0  95/09/04
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USXAXU(CXS)
      CHARACTER CXS*(*), CXFMT*8, CXUNIT*32
      CHARACTER CFMT0*8, CPOS*1, CXSUB*32, CSBLBL*32, CP*8
      LOGICAL   LAB1
      EXTERNAL  CSBLBL, LENZ

*---------------------------- PARAMETERS -------------------------------

      CALL USRGET('DXT',  DXT )
      CALL USRGET('DXL',  DXL )
      CALL USRGET('XFAC', XFAC)
      CALL USRGET('XOFF', XOFF)
      CALL USCGET('CXFMT',  CXFMT)
      CALL USCGET('CXUNIT', CXUNIT)

      IF(DXT.LE.0 .OR. DXL.LE.0)
     #   CALL MSGDMP('E', 'USXAXU', 'DXT OR DXL IS NEGATIVE.')

*-----------------------------------------------------------------------

      CALL SGQTRN(ITR)
      IF(.NOT.(ITR.EQ.1 .OR. ITR.EQ.2))
     #   CALL MSGDMP('E', 'USXAXU', 'INVALID TRANSFORMATION NUMBER.')
      CALL SGQWND(XMIN, XMAX, YMIN, YMAX)

      DXTA = DXT/XFAC
      DXLA = DXL/XFAC
      XMINA = (XMIN - XOFF) * XFAC
      XMAXA = (XMAX - XOFF) * XFAC
      CALL SGSWND(XMINA, XMAXA, YMIN, YMAX)
      CALL SGSTRF

*---------------------------- X-AXIS -----------------------------------

      CALL UXQFMT(CFMT0)
      CALL UXSFMT(CXFMT)
      NXS = MIN(LENZ(CXS), 2)

      DO 100 I=1, NXS
        CALL UXAXDV(CXS(I:I), DXTA, DXLA)
        CP = 'LABELX'//CXS(I:I)
        CALL UZLGET(CP, LAB1)
        IF(LAB1) THEN
          CXSUB = CSBLBL(XFAC, XOFF, CXUNIT)
          IF(LENZ(CXSUB).NE.0) THEN
            CALL USCGET('CXSPOS', CPOS)
            READ(CXFMT, '(T3,I1)') NDGT
            CALL USXSUB(CXS(I:I), CPOS, CXSUB, REAL(NDGT))
          ENDIF
        ENDIF
  100 CONTINUE

      CALL SGSWND(XMIN,  XMAX,  YMIN,  YMAX)
      CALL SGSTRF
      CALL UXSFMT(CFMT0)

      RETURN
      END
