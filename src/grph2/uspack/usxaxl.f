*-----------------------------------------------------------------------
*     USPACK DRAW X-AXIS (LOG)                         DCL 5.0 95/09/04
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USXAXL(CXS)
      CHARACTER CXS*(*), CUNIT*32, CXSUB*32, CSBLBL*32, CPOS*1, CP*8
      LOGICAL LAB1
      EXTERNAL CSBLBL, LENZ

      CALL USIGET('NLBLX'  , NLBL)
      CALL USIGET('NTICKSX', NTICKS)
      CALL USIGET('ITYPEX' , ITYPEX)
      CALL USRGET('XFAC'   , XFAC)
      CALL USCGET('CXUNIT' , CUNIT)

      CALL ULIGET('IXTYPE', IT)
      CALL ULISET('IXTYPE', ITYPEX)

      CALL SGQTRN(ITR)
      IF(.NOT.(ITR.EQ.3 .OR. ITR.EQ.4))
     &   CALL MSGDMP('E', 'USXAXL', 'INVALID TRANSFORMATION NUMBER.')
      CALL SGQWND(XMIN, XMAX, YMIN, YMAX)

      XMINA = XMIN*XFAC
      XMAXA = XMAX*XFAC
      CALL SGSWND(XMINA, XMAXA, YMIN, YMAX)
      CALL SGSTRF

*---------------------------- X-AXIS -----------------------------------

      NXS = MIN(LEN(CXS), 2)
      DO 100 I=1, NXS
        CALL ULXLOG(CXS(I:I), NLBL, NTICKS)
        CP = 'LABELX'//CXS(I:I)
        CALL UZLGET(CP, LAB1)
        IF(LAB1) THEN
          CXSUB = CSBLBL(XFAC, 0., CUNIT)
          IF(LENZ(CXSUB).NE.0) THEN
            CALL USCGET('CXSPOS', CPOS)
            CALL USXSUB(CXS(I:I), CPOS, CXSUB, 3.)
          ENDIF
        ENDIF
  100 CONTINUE

*------------------------------ ----------------------------------------

      CALL SGSWND(XMIN, XMAX, YMIN, YMAX)
      CALL SGSTRF
      CALL ULISET('IXTYPE', IT)

      RETURN
      END
