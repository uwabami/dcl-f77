*-----------------------------------------------------------------------
*     USPACK SUB-LABEL (Y)                            S.Sakai  90/08/18
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USYSUB(CYA, CXA, CLABEL, RLBL)

      CHARACTER CLABEL*(*), CXS*1, CYS*1, CXA*1, CYA*1, CP*8
      LOGICAL   LPRTCT,LCLIPZ


      CXS = CXA
      CYS = CYA
      CALL CUPPER(CXS)
      CALL CUPPER(CYS)

      IF(CXS.NE.'T' .AND. CXS.NE.'B')
     #   CALL MSGDMP('E', 'USYSUB', 'INVALID CXS')
      IF(CYS.NE.'L' .AND. CYS.NE.'R'.AND. CYS.NE.'U')
     #   CALL MSGDMP('E', 'USYSUB', 'INVALID CYS')

      CP = 'ROFFY' //CYS
      CALL UZRGET(CP, ROFFY )
      CP = 'ICENTY'//CYS
      CALL UZIGET(CP, ICENTY)
      CP = 'IROTLY'//CYS
      CALL UZIGET(CP, IROTLY)
      CALL UZRGET('RSIZEL1', RSIZEL)
      CALL UZRGET('RSIZET2', RSIZET)
      CALL UZIGET('INDEXL1', INDEX )
      CALL UZIGET('INNER'  , INNER )
      CALL UZRGET('PAD1'   , PAD1  )

      CALL SZQTXW(CLABEL, NCH, WSUB, HSUB)
      CALL SGQVPT(VXMIN, VXMAX, VYMIN, VYMAX)

      IROTLY = MOD(IROTLY, 4)
      WSUB = WSUB*RSIZEL
      WLBL = RLBL*RSIZEL
      WPAD = PAD1*RSIZEL

      IC = 1
      IF(IROTLY.GE.2) IC = -1
      ICY = IC*ICENTY

*======================== < VERTICAL LABEL > ===========================

      IF(IROTLY.EQ.1 .OR. IROTLY.EQ.3) THEN
        IF(CXS.EQ.'T') THEN
          ICENTY = IC
          POSY = VYMAX - WLBL/2*(ICY-1)
        ELSE
          ICENTY = -IC
          POSY = VYMIN - WLBL/2*(ICY+1)
        ENDIF

        IF(CYS.EQ.'L') THEN
          POSX  = VXMIN
          IFLAG = -1
        ELSEIF(CYS.EQ.'R') THEN
          POSX  = VXMAX
          IFLAG = +1
        ELSE
          CALL UZRGET('UXUSER', UXUSR)
          CALL UZIGET('IFLAG', IFLAG)
          IFLAG = SIGN(1, IFLAG)
          CALL STFTRF(UXUSR, 1., POSX, VY)
        ENDIF
        ROFFY = ROFFY + RSIZEL*IFLAG*(1.+PAD1)
        POSX  = POSX  + ROFFY - RSIZEL*IFLAG/2.
        CP = 'ROFFY' //CYS
        CALL UZRSET(CP, ROFFY )

*====================== < HORIZONTAL LABEL > ===========================

      ELSE
        CALL USIGET('MXDGTSY', MXDGT )
        CALL USLGET('LPRTCT' , LPRTCT)
        CP = 'SOFFY'//CYS//CXS
        CALL USRGET(CP, RSOFF )
        IF(LPRTCT .AND. CYS.EQ.'U')
     #    CALL MSGDMP('M', 'USYSUB', 'X-LABEL REGION IS NOT PROTECTED.')

        IF(CXS .EQ. 'T') THEN
          POSY  = VYMAX + RSOFF + RSIZEL*(0.5+PAD1)
          RSOFF = RSOFF + RSIZEL*(1.+PAD1)
        ELSE
          POSY  = VYMIN + RSOFF - RSIZEL*(0.5+PAD1)
          RSOFF = RSOFF - RSIZEL*(1.+PAD1)
        ENDIF

        CP = 'SOFFY'//CYS//CXS
        CALL USRSET(CP, RSOFF)

        CP = 'ROFFX'//CXS
        CALL USRGET(CP, ROFFX)

        ROFFX = MAX(RSOFF, ROFFX)

*-----------------------------------------------------------------------
*       IFLAG =  1 : LABEL IS RIGHT SIDE OF THE AXIS.
*       IFLAG = -1 : LABEL IS LEFT  SIDE OF THE AXIS.

        CP = 'ROFGY'//CYS
        CALL UZRGET(CP , ROFG)
        IF(INNER.LT.0) WPAD = WPAD - INNER*RSIZET
        IF(CYS.EQ.'L') THEN
          IFLAG = -1
          VXP  = -(VXMIN+ROFG)
        ELSEIF(CYS.EQ.'R') THEN
          IFLAG = 1
          VXP  = VXMAX+ROFG
        ELSEIF(CYS.EQ.'U') THEN
          CALL UZRGET('UXUSER', UXUSR)
          CALL UZIGET('IFLAG', IFLAG)
          CALL STFTRF(UXUSR, 1., VXP, VY)
          VXP = VXP + ROFG
          CP = 'ROFFX'//CXS
          CALL USRSET(CP, ROFFX)
        ENDIF

        POSX = VXP + WPAD + WLBL*(ICY*IFLAG+1)/2.
        XMIN = VXP + WPAD
        XMAX = VXP + WPAD + MXDGT*RSIZEL

*------------------------ boundary check -------------------------------

        XLMIN = POSX - WSUB*(ICY*IFLAG+1)/2.
        IF(XLMIN.LT.XMIN) THEN
          ICY    = -IFLAG
          POSX   = XMIN
        ENDIF

        XLMAX = POSX - WSUB*(ICY*IFLAG-1)/2.
        IF(.NOT.LPRTCT .AND. XLMAX.GT.XMAX) THEN
          ICY    = IFLAG
          POSX   = XMAX
        ENDIF

        XLMIN = POSX - WSUB*(ICY*IFLAG+1)/2.
        IF(XLMIN.LT.VXP) THEN
          IF(CYS.EQ.'U') THEN
            ICY = 0
            POSX = (VXP+XMAX)/2.
          ELSE
            CP = 'ROFFX'//CXS
            CALL USRSET(CP, ROFFX)
          ENDIF
        ENDIF

        ICENTY = ICY/IC
        POSX = ABS(POSX)
      ENDIF

      CALL SGLGET('LCLIP',LCLIPZ)
      CALL SGLSET('LCLIP',.FALSE.)
      CALL SGTXZV(POSX, POSY, CLABEL, RSIZEL, IROTLY*90, ICENTY, INDEX)
      CALL SGLSET('LCLIP',LCLIPZ)

      RETURN
      END
