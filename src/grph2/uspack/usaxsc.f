*-----------------------------------------------------------------------
*     USPACK AXIS (AUTO SCALING)                      S.Sakai  99/10/07
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USAXSC(CSIDE)

      CHARACTER  CSIDE*(*), CS, CP*8
      EXTERNAL   LENZ
      LOGICAL    LMATCH, LOFF

      IF(CSIDE.EQ.' ') RETURN

      CALL SGQTRN(ITR)
      CALL SGQVPT(VXMIN, VXMAX, VYMIN, VYMAX)
      CALL SGQWND(UXMIN0, UXMAX0, UYMIN0, UYMAX0)

      CALL UZRGET('XFACT' , XFACT  )
      CALL UZRGET('XOFFSET', XOFFSET)
      CALL UZRGET('YFACT' , YFACT  )
      CALL UZRGET('YOFFSET', YOFFSET)
      UXMIN = XFACT*UXMIN0 + XOFFSET
      UXMAX = XFACT*UXMAX0 + XOFFSET
      UYMIN = YFACT*UYMIN0 + YOFFSET
      UYMAX = YFACT*UYMAX0 + YOFFSET

      NCS = LENZ(CSIDE)
      CALL UZLGET('LOFFSET', LOFF)
      CALL UZLSET('LOFFSET', .TRUE.)

      DO 100 I=1, NCS
        CS = CSIDE(I:I)
        CALL CUPPER(CS)

        IF(CS.EQ.'T' .OR. CS.EQ.'B' .OR. CS.EQ.'H') THEN
          IF(CS.EQ.'H') CS='U'

          IF(ITR.EQ.1 .OR. ITR.EQ.2) THEN
            CP = 'IROTLX'//CS
            CALL UZIGET(CP, IROTA)
            CALL USLGET('LMATCH' , LMATCH)
            MODE = MOD(IROTA,2)
            IF(LMATCH) MODE = 0

            CALL USUSCU('X', UXMIN, UXMAX, VXMIN, VXMAX, MODE)
            CALL USRGET('DXT', DXT)
            CALL USRGET('DXL', DXL)
            IF(CS.EQ.'U') CS='H'
            CALL USAXDV(CS, DXT, DXL)

          ELSEIF(ITR.EQ.3 .OR. ITR.EQ.4) THEN
            CALL USUSCL('X', UXMIN, UXMAX, VXMIN, VXMAX)
            CALL USIGET('NLBLX'  , NLBL  )
            CALL USIGET('NTICKSX', NTICKS)
            CALL USIGET('ITYPEX' , ITYPEX)

            CALL ULIGET('IXTYPE', IT    )
            CALL ULISET('IXTYPE', ITYPEX)
            CALL USAXLG(CS, NLBL, NTICKS)
            CALL ULISET('IXTYPE', IT    )
          ELSE
            CALL MSGDMP('E', 'USXAXS', 'INVALID TRANSFORMATION NUMBER.')
          ENDIF


        ELSEIF(CS.EQ.'L' .OR. CS.EQ.'R' .OR. CS.EQ.'V') THEN
          IF(CS.EQ.'V') CS='U'

          IF(ITR.EQ.1 .OR. ITR.EQ.3) THEN
            CP = 'IROTLY'//CS
            CALL UZIGET(CP, IROTA)
            CALL USLGET('LMATCH' , LMATCH)
            MODE = MOD(IROTA+1,2)
            IF(LMATCH) MODE = 0

            CALL USUSCU('Y', UYMIN, UYMAX, VYMIN, VYMAX, MODE)
            CALL USRGET('DYT', DYT)
            CALL USRGET('DYL', DYL)
            IF(CS.EQ.'U') CS='V'
            CALL USAXDV(CS, DYT, DYL)

          ELSEIF(ITR.EQ.2 .OR. ITR.EQ.4) THEN
            CALL USUSCL('Y', UYMIN, UYMAX, VYMIN, VYMAX)
            CALL USIGET('NLBLY'  , NLBL  )
            CALL USIGET('NTICKSY', NTICKS)
            CALL USIGET('ITYPEY' , ITYPEY)

            CALL ULIGET('IYTYPE', IT    )
            CALL ULISET('IYTYPE', ITYPEY)
            CALL USAXLG(CS, NLBL, NTICKS)
            CALL ULISET('IYTYPE', IT    )

          ELSE
            CALL MSGDMP('E', 'USYAXS', 'INVALID TRANSFORMATION NUMBER.')
          ENDIF

        ENDIF

  100 CONTINUE

      CALL UZLSET('LOFFSET', LOFF)
      END
