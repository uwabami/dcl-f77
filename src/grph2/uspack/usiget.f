*-----------------------------------------------------------------------
*     USIGET / USISET / USISTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USIGET(CP, IPARA)

      CHARACTER CP*(*)

      CHARACTER CX*8
      CHARACTER CL*40


      CALL USIQID(CP, IDX)
      CALL USIQVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY USISET(CP, IPARA)

      CALL USIQID(CP, IDX)
      CALL USISVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY USISTX(CP, IPARA)

      IP = IPARA
      CALL USIQID(CP, IDX)

*     / SHORT NAME /

      CALL USIQCP(IDX, CX)
      CALL RTIGET('US', CX, IP, 1)

*     / LONG NAME /

      CALL USIQCL(IDX, CL)
      CALL RLIGET(CL, IP, 1)

      CALL USISVL(IDX,IP)

      RETURN
      END
