*-----------------------------------------------------------------------
*     USPACK DRAW Y-AXIS (UNIFORM)                     DCL 5.0 95/09/04
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USYAXU(CYS)
      CHARACTER CYS*(*), CYFMT*8, CYUNIT*32
      CHARACTER CFMT0*8, CPOS*1, CYSUB*32, CSBLBL*32, CP*8
      LOGICAL   LAB1
      EXTERNAL  CSBLBL, LENZ

*---------------------------- PARAMETERS -------------------------------

      CALL USRGET('DYT',  DYT )
      CALL USRGET('DYL',  DYL )
      CALL USRGET('YFAC', YFAC)
      CALL USRGET('YOFF', YOFF)
      CALL USCGET('CYFMT',  CYFMT)
      CALL USCGET('CYUNIT', CYUNIT)

      IF(DYT.LE.0 .OR. DYL.LE.0)
     #   CALL MSGDMP('E', 'USYAXU', 'DYT OR DYL IS NEGATIVE.')

*-----------------------------------------------------------------------

      CALL SGQTRN(ITR)
      IF(.NOT.(ITR.EQ.1 .OR. ITR.EQ.3))
     #   CALL MSGDMP('E', 'USYAXU', 'INVALID TRANSFORMATION NUMBER.')
      CALL SGQWND(XMIN, XMAX, YMIN, YMAX)

      DYTA = DYT/YFAC
      DYLA = DYL/YFAC
      YMINA = (YMIN - YOFF) * YFAC
      YMAXA = (YMAX - YOFF) * YFAC
      CALL SGSWND(XMIN, XMAX, YMINA, YMAXA)
      CALL SGSTRF

*---------------------------- Y-AXIS -----------------------------------

      CALL UYQFMT(CFMT0)
      CALL UYSFMT(CYFMT)
      NYS = MIN(LENZ(CYS), 2)

      DO 100 I=1, NYS
        CALL UYAXDV(CYS(I:I), DYTA, DYLA)
        CP = 'LABELY'//CYS(I:I)
        CALL UZLGET(CP, LAB1)
        IF(LAB1) THEN
          CYSUB = CSBLBL(YFAC, YOFF, CYUNIT)
          IF(LENZ(CYSUB).NE.0) THEN
            CALL USCGET('CYSPOS', CPOS)
            READ(CYFMT, '(T3,I1)') NDGT
            CALL USYSUB(CYS(I:I), CPOS, CYSUB, REAL(NDGT))
          ENDIF
        ENDIF
  100 CONTINUE

      CALL SGSWND(XMIN,  XMAX,  YMIN,  YMAX)
      CALL SGSTRF
      CALL UYSFMT(CFMT0)

      RETURN
      END
