*-----------------------------------------------------------------------
*     USLQNP / USLQID / USLQCP / USLQVL / USLSVL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USLQNP(NCP)

      LOGICAL   LPARA
      CHARACTER CP*(*)

      PARAMETER (NPARA = 6)

      LOGICAL   LX(NPARA)
      LOGICAL   LCHREQ, LFIRST
      CHARACTER CPARAS(NPARA)*8
      CHARACTER CPARAL(NPARA)*40
      CHARACTER CMSG*80

      EXTERNAL  LCHREQ, LENC

      SAVE

* ---- SHORT NAME ----
*     / PARAMETERS FOR USDAXS /
      DATA      CPARAS(1) / 'LXINV   ' /, LX(1) / .FALSE. /
      DATA      CPARAS(2) / 'LYINV   ' /, LX(2) / .FALSE. /
      DATA      CPARAS(3) / 'LMATCH  ' /, LX(3) / .FALSE. /
*     / PARAMETERS FOR USXSUB & USYSUB /
      DATA      CPARAS(4) / 'LPRTCT  ' /, LX(4) / .FALSE. /
      DATA      CPARAS(5) / 'LXSUB   ' /, LX(5) / .TRUE. /
      DATA      CPARAS(6) / 'LYSUB   ' /, LX(6) / .TRUE. /

* ---- LONG NAME ----
*     / PARAMETERS FOR USDAXS /
      DATA      CPARAL(1) / 'INV_X_AXIS' /
      DATA      CPARAL(2) / 'INV_Y_AXIS' /
      DATA      CPARAL(3) / 'ENABLE_XY_MATCHING' /
*     / PARAMETERS FOR USXSUB & USYSUB /
      DATA      CPARAL(4) / 'PROTECT_REGION' /
      DATA      CPARAL(5) / 'DRAW_X_SUBLABEL' /
      DATA      CPARAL(6) / 'DRAW_Y_SUBLABEL' /

      DATA      LFIRST / .TRUE. /


      NCP = NPARA

      RETURN
*-----------------------------------------------------------------------
      ENTRY USLQID(CP, IDX)

      DO 10 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IDX = N
          RETURN
        END IF
   10 CONTINUE
      CMSG = 'PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E','USLQID',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY USLQCP(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAS(IDX)
      ELSE
        CALL MSGDMP('E','USLQCP','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY USLQCL(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAL(IDX)
      ELSE
        CALL MSGDMP('E','USLQCL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY USLQVL(IDX, LPARA)

      IF (LFIRST) THEN
        CALL RTLGET('US', CPARAS, LX, NPARA)
        CALL RLLGET(CPARAL, LX, NPARA)
        LFIRST=.FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        LPARA = LX(IDX)
      ELSE
        CALL MSGDMP('E','USLQVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY USLSVL(IDX, LPARA)

      IF (LFIRST) THEN
        CALL RTLGET('US', CPARAS, LX, NPARA)
        CALL RLLGET(CPARAL, LX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        LX(IDX) = LPARA
      ELSE
        CALL MSGDMP('E','USLSVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY USLQIN(CP, IN)

      DO 20 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IN = N
          RETURN
        END IF
   20 CONTINUE

      IN = 0

      RETURN
      END
