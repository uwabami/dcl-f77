*-----------------------------------------------------------------------
*     USPACK OFFSET CHEK (X-AXIS)                    S. Sakai  90/08/18
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USXOFF(CXS)
      CHARACTER*1 CXS, CP*8

      IF(CXS.EQ.'T' .OR. CXS.EQ.'B') THEN
        CP = 'ROFFX'//CXS
        CALL USRGET(CP, SOFF)
        CALL UZRGET(CP, ROFF)
        ROFF = MAX(ROFF, SOFF)
        CALL UZRSET(CP, ROFF)
      ELSE
        CALL MSGDMP('E', 'USXOFF', 'INVALID CXS')
      ENDIF

      RETURN
      END
