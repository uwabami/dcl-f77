*-----------------------------------------------------------------------
*     USPACK SET PARAMETER
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USPFIT
      LOGICAL LXINV, LYINV, LOFF
      CHARACTER  CXSIDE*2, CYSIDE*2

      CALL GLRGET('RUNDEF', RUNDEF)

      CALL USCGET('CXSIDE' ,  CXSIDE)
      CALL USCGET('CYSIDE' ,  CYSIDE)

      CALL CUPPER(CXSIDE)
      CALL CUPPER(CYSIDE)

      IF(INDEX(CYSIDE,'U') .NE. 0) THEN
        CALL UZRGET('UXUSER', UXUSER)
        CALL USSPNT(1, UXUSER, RUNDEF)
      ENDIF

      IF(INDEX(CXSIDE,'U') .NE. 0) THEN
        CALL UZRGET('UYUSER', UYUSER)
        CALL USSPNT(1, RUNDEF, UYUSER)
      ENDIF

      CALL SGIGET('ITR'    , ITR)
      CALL USLGET('LXINV'  , LXINV)
      CALL USLGET('LYINV'  , LYINV)
      CALL UZLGET('LOFFSET', LOFF)

*--------------------------- VIEW PORT ---------------------------------

      CALL SGRGET('VXMIN'  , VXMIN)
      CALL SGRGET('VXMAX'  , VXMAX)
      CALL SGRGET('VYMIN'  , VYMIN)
      CALL SGRGET('VYMAX'  , VYMAX)

      CALL USRGET('RMRGN'  , RMRGN)
      CALL UZRGET('RSIZEL1', CW)
      RMC = RMRGN*CW

      CALL STQWTR(VXMIN0, VXMAX0, VYMIN0, VYMAX0,
     #            WXMIN , WXMAX,  WYMIN,  WYMAX, ITRW)

      IF(VXMIN.EQ.RUNDEF)  VXMIN = VXMIN0 + RMC
      IF(VXMAX.EQ.RUNDEF)  VXMAX = VXMAX0 - RMC
      IF(VYMIN.EQ.RUNDEF)  VYMIN = VYMIN0 + RMC
      IF(VYMAX.EQ.RUNDEF)  VYMAX = VYMAX0 - RMC

*----------------------------- X-AXIS ----------------------------------

      CALL SGRGET('UXMAX' , XMAX)
      CALL SGRGET('UXMIN' , XMIN)

      IF(XMIN.NE.RUNDEF .AND. XMAX.NE.RUNDEF .AND. XMIN.GT.XMAX) THEN
        LXINV = .TRUE.
        CALL USWAPZ(XMIN, XMAX, 1)
      ENDIF

      CALL USRGET('XDTMAX', UXMAX)
      CALL USRGET('XDTMIN', UXMIN)
      IF(XMIN.NE.RUNDEF) UXMIN = XMIN
      IF(XMAX.NE.RUNDEF) UXMAX = XMAX

      IF(UXMIN.EQ.RUNDEF .OR. UXMAX.EQ.RUNDEF) THEN
        CALL MSGDMP('E', 'USPFIT', 'XMIN OR XMAX IS NOT DEFINED.')
      END IF

      IF(LOFF) THEN
        CALL UZRGET('XOFFSET', XOFF)
        CALL UZRGET('XFACT'  , XFAC)
        UXMIN = XFAC*UXMIN + XOFF
        UXMAX = XFAC*UXMAX + XOFF
      END IF

      IF(ITR.EQ.1 .OR. ITR.EQ.2) THEN
        CALL USURDT(UXMIN, UXMAX, VXMIN, VXMAX, DT)
      ELSEIF(ITR.EQ.3. .OR. ITR.EQ.4) THEN
        CALL USURDL(UXMIN, UXMAX, VXMIN, VXMAX)
      ELSE
        CALL MSGDMP('E', 'USPFIT', 'INVALID ITR')
      ENDIF

      IF(XMIN.NE.RUNDEF) UXMIN = XMIN
      IF(XMAX.NE.RUNDEF) UXMAX = XMAX

*----------------------------- Y-AXIS ----------------------------------

      CALL SGRGET('UYMAX', YMAX)
      CALL SGRGET('UYMIN', YMIN)

      IF(YMIN.NE.RUNDEF .AND. YMAX.NE.RUNDEF .AND. YMIN.GT.YMAX) THEN
        LYINV = .TRUE.
        CALL USWAPZ(YMIN, YMAX, 1)
      ENDIF

      CALL USRGET('YDTMAX', UYMAX)
      CALL USRGET('YDTMIN', UYMIN)
      IF(YMIN.NE.RUNDEF) UYMIN = YMIN
      IF(YMAX.NE.RUNDEF) UYMAX = YMAX

      IF(UYMIN.EQ.RUNDEF .OR. UYMAX.EQ.RUNDEF) THEN
        CALL MSGDMP('E', 'USPFIT', 'YMIN OR YMAX IS NOT DEFINED.')
      END IF

      IF(LOFF) THEN
        CALL UZRGET('YOFFSET', YOFF)
        CALL UZRGET('YFACT'  , YFAC)
        UYMIN = YFAC*UYMIN + YOFF
        UYMAX = YFAC*UYMAX + YOFF
      END IF

      IF(ITR.EQ.1 .OR. ITR.EQ.3) THEN
        CALL USURDT(UYMIN, UYMAX, VYMIN, VYMAX, DT)
      ELSE
        CALL USURDL(UYMIN, UYMAX, VYMIN, VYMAX)
      ENDIF

      IF(YMIN.NE.RUNDEF) UYMIN = YMIN
      IF(YMAX.NE.RUNDEF) UYMAX = YMAX

*-----------------------------------------------------------------------

      IF(LXINV) CALL USWAPZ(UXMIN, UXMAX, 1)
      IF(LYINV) CALL USWAPZ(UYMIN, UYMAX, 1)

      CALL SGSWND(UXMIN, UXMAX, UYMIN, UYMAX)
      CALL SGSVPT(VXMIN, VXMAX, VYMIN, VYMAX)
      CALL SGSTRN(ITR)

      END
