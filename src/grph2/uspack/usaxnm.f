*-----------------------------------------------------------------------
*     USPACK AXIS (SPECIFIED MARK)                    S.Sakai  99/10/05
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USAXNM(CSIDE, DTICK, N1, DLABEL, N2)
      PARAMETER (N0 = 200)

      REAL       DTICK(N1), DLABEL(N2), DTZ(N0),DLZ(N0)
      CHARACTER  CSIDE*(*), CS
      EXTERNAL   LENZ
      LOGICAL    LOFF

      CALL UZLGET('LOFFSET', LOFF)
      CALL UZLSET('LOFFSET', .TRUE.)

      NCS = LENZ(CSIDE)
      DO 100 I=1, NCS
        CS = CSIDE(I:I)
        CALL CUPPER(CS)

        IF(CS.EQ.'T' .OR. CS.EQ.'B' .OR. CS.EQ.'H') THEN
          IF(CS.EQ.'H') CS='U'
          CALL USXINZ(CS,FACTOR,OFFSET)

          DO 200 N=1, N1
            DTZ(N) = (DTICK(N)-OFFSET)/FACTOR
  200     CONTINUE
          DO 210 N=1, N2
            DLZ(N) = (DLABEL(N)-OFFSET)/FACTOR
  210     CONTINUE

          CALL UXAXNM(CS, DTZ, N1, DLZ, N2)
          CALL USXTLZ

        ELSEIF(CS.EQ.'L' .OR. CS.EQ.'R' .OR. CS.EQ.'V') THEN
          IF(CS.EQ.'V') CS='U'
          CALL USYINZ(CS,FACTOR,OFFSET)

          DO 300 N=1, N1
            DTZ(N) = (DTICK(N)-OFFSET)/FACTOR
  300     CONTINUE
          DO 310 N=1, N2
            DLZ(N) = (DLABEL(N)-OFFSET)/FACTOR
  310     CONTINUE

          CALL UYAXNM(CS, DTZ, N1, DLZ, N2)
          CALL USYTLZ
        ENDIF

  100 CONTINUE
      CALL UZLSET('LOFFSET', LOFF)

      END
