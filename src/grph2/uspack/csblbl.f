*-----------------------------------------------------------------------
*     USPACK GET CHARACTER FOR SUBLABEL               S.Sakai  90/02/24
*                                                              92/02/27
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      FUNCTION CSBLBL(UFAC, UOFF, CUNIT)

      CHARACTER CSBLBL*(*), CUNIT*(*), CSUB*32, CBLKT*2,
     #          CHFAC*16,   CHOFF*16,  USGI*3

      EXTERNAL  LENZ

      CALL USCGET('CBLKT'             , CBLKT)

*----------------------- OFFSET AND FACTOR -----------------------------

      CHOFF = ' '
      IF(UOFF.NE.0.) THEN
        CALL USCHVL(UOFF, CHOFF)
      ENDIF

      IF(UFAC .NE. 1.)  THEN
        CALL USCHVL(UFAC, CHFAC)
        CSUB = USGI(194) // CHFAC(2:LENC(CHFAC)) // ' ' // CHOFF
      ELSEIF(UOFF.NE.0.) THEN
        CSUB = CHOFF
      ELSE
        CSUB = ' '
      ENDIF

*--------------------------- UNIT etc. ---------------------------------

      NLF = LENZ(CSUB)
      NLS = LENZ(CUNIT)
      CSBLBL = CSUB

      IF(NLF.NE.0) THEN
        IF(NLS.NE.0) THEN
          CSUB = CBLKT(1:1)//CSBLBL(1:NLF+1)//CUNIT(1:NLS)//CBLKT(2:2)
        ELSE
          CSUB = CBLKT(1:1)//CSBLBL(1:NLF)//CBLKT(2:2)
        ENDIF
      ELSE
        IF(NLS.NE.0) THEN
          CSUB = CBLKT(1:1)//CUNIT(1:NLS)//CBLKT(2:2)
        ELSE
          CSUB = ' '
        ENDIF
      ENDIF
      CALL CLADJ(CSUB)
      CSBLBL = CSUB

      RETURN
      END
