*-----------------------------------------------------------------------
*     USPQNP / USPQID / USPQCP / USPQVL / USPSVL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USPQNP(NCP)

      CHARACTER CP*(*)

      PARAMETER (NPARA = 49)

      INTEGER   ITYPE(NPARA)
      CHARACTER CPARAS(NPARA)*8
      CHARACTER CPARAL(NPARA)*40
      CHARACTER CMSG*80

      LOGICAL   LCHREQ

      EXTERNAL  LCHREQ,LENC

      SAVE

* ---- SHORT NAME ----

*     / CONTROL PARAMETERS /

      DATA      CPARAS( 1) / 'IRESET  ' /, ITYPE( 1) / 1 /

*     / PARAMETERS FOR USDAXS /

      DATA      CPARAS( 2) / 'LXINV   ' /, ITYPE( 2) / 2 /
      DATA      CPARAS( 3) / 'LYINV   ' /, ITYPE( 3) / 2 /
      DATA      CPARAS( 4) / 'LMATCH  ' /, ITYPE( 4) / 2 /
      DATA      CPARAS( 5) / 'RMRGN   ' /, ITYPE( 5) / 3 /

*     / PARAMETERS FOR USUSCU /

      DATA      CPARAS( 6) / 'XOFF    ' /, ITYPE( 6) / 3 /
      DATA      CPARAS( 7) / 'YOFF    ' /, ITYPE( 7) / 3 /
      DATA      CPARAS( 8) / 'XFAC    ' /, ITYPE( 8) / 3 /
      DATA      CPARAS( 9) / 'YFAC    ' /, ITYPE( 9) / 3 /
      DATA      CPARAS(10) / 'DXT     ' /, ITYPE(10) / 3 /
      DATA      CPARAS(11) / 'DYT     ' /, ITYPE(11) / 3 /
      DATA      CPARAS(12) / 'DXL     ' /, ITYPE(12) / 3 /
      DATA      CPARAS(13) / 'DYL     ' /, ITYPE(13) / 3 /
      DATA      CPARAS(14) / 'TFACT   ' /, ITYPE(14) / 3 /
      DATA      CPARAS(15) / 'MXDGTX  ' /, ITYPE(15) / 1 /
      DATA      CPARAS(16) / 'MXDGTY  ' /, ITYPE(16) / 1 /
      DATA      CPARAS(17) / 'NBLANK1 ' /, ITYPE(17) / 1 /
      DATA      CPARAS(18) / 'NBLANK2 ' /, ITYPE(18) / 1 /

*     / PARAMETERS FOR USUSCL /

      DATA      CPARAS(19) / 'NLBLX   ' /, ITYPE(19) / 1 /
      DATA      CPARAS(20) / 'NLBLY   ' /, ITYPE(20) / 1 /
      DATA      CPARAS(21) / 'NTICKSX ' /, ITYPE(21) / 1 /
      DATA      CPARAS(22) / 'NTICKSY ' /, ITYPE(22) / 1 /
      DATA      CPARAS(23) / 'ITYPEX  ' /, ITYPE(23) / 1 /
      DATA      CPARAS(24) / 'ITYPEY  ' /, ITYPE(24) / 1 /

*     / PARAMETERS FOR USXSUB & USYSUB /

      DATA      CPARAS(25) / 'MXDGTSX ' /, ITYPE(25) / 1 /
      DATA      CPARAS(26) / 'MXDGTSY ' /, ITYPE(26) / 1 /
      DATA      CPARAS(27) / 'LPRTCT  ' /, ITYPE(27) / 2 /
      DATA      CPARAS(28) / 'LXSUB   ' /, ITYPE(28) / 2 /
      DATA      CPARAS(29) / 'LYSUB   ' /, ITYPE(29) / 2 /

*     / FOLLOWING 20 PARAMETERS ARE RESET BY USINIZ /

      DATA      CPARAS(30) / 'SOFFXTR ' /, ITYPE(30) / 3 /
      DATA      CPARAS(31) / 'SOFFXBR ' /, ITYPE(31) / 3 /
      DATA      CPARAS(32) / 'SOFFXUR ' /, ITYPE(32) / 3 /
      DATA      CPARAS(33) / 'SOFFXTL ' /, ITYPE(33) / 3 /
      DATA      CPARAS(34) / 'SOFFXBL ' /, ITYPE(34) / 3 /
      DATA      CPARAS(35) / 'SOFFXUL ' /, ITYPE(35) / 3 /
      DATA      CPARAS(36) / 'SOFFYRT ' /, ITYPE(36) / 3 /
      DATA      CPARAS(37) / 'SOFFYLT ' /, ITYPE(37) / 3 /
      DATA      CPARAS(38) / 'SOFFYUT ' /, ITYPE(38) / 3 /
      DATA      CPARAS(39) / 'SOFFYRB ' /, ITYPE(39) / 3 /
      DATA      CPARAS(40) / 'SOFFYLB ' /, ITYPE(40) / 3 /
      DATA      CPARAS(41) / 'SOFFYUB ' /, ITYPE(41) / 3 /
      DATA      CPARAS(42) / 'ROFFXT  ' /, ITYPE(42) / 3 /
      DATA      CPARAS(43) / 'ROFFXB  ' /, ITYPE(43) / 3 /
      DATA      CPARAS(44) / 'ROFFYR  ' /, ITYPE(44) / 3 /
      DATA      CPARAS(45) / 'ROFFYL  ' /, ITYPE(45) / 3 /

      DATA      CPARAS(46) / 'XDTMIN  ' /, ITYPE(46) / 3 /
      DATA      CPARAS(47) / 'XDTMAX  ' /, ITYPE(47) / 3 /
      DATA      CPARAS(48) / 'YDTMIN  ' /, ITYPE(48) / 3 /
      DATA      CPARAS(49) / 'YDTMAX  ' /, ITYPE(49) / 3 /

* ---- LONG NAME ----

*     / CONTROL PARAMETERS /

      DATA      CPARAL( 1) / '****IRESET  ' /

*     / PARAMETERS FOR USDAXS /

      DATA      CPARAL( 2) / 'INV_X_AXIS' /
      DATA      CPARAL( 3) / 'INV_Y_AXIS' /
      DATA      CPARAL( 4) / 'ENABLE_XY_MATCHING' /
      DATA      CPARAL( 5) / 'MARGIN_WIDTH' /

*     / PARAMETERS FOR USUSCU /

      DATA      CPARAL( 6) / 'X_LABEL_OFFSET' /
      DATA      CPARAL( 7) / 'Y_LABEL_OFFSET' /
      DATA      CPARAL( 8) / 'X_LABEL_FACTOR' /
      DATA      CPARAL( 9) / 'Y_LABEL_FACTOR' /
      DATA      CPARAL(10) / 'X_TICK_INTERVAL' /
      DATA      CPARAL(11) / 'Y_TICK_INTERVAL' /
      DATA      CPARAL(12) / 'X_LABEL_INTERVAL' /
      DATA      CPARAL(13) / 'Y_LABEL_INTERVAL' /
      DATA      CPARAL(14) / 'MAX_TICK_INTERVAL' /
      DATA      CPARAL(15) / 'X_LABEL_MAX_CHAR' /
      DATA      CPARAL(16) / 'X_LABEL_MAX_CHAR' /
      DATA      CPARAL(17) / 'LABEL_GAP_PARALLEL' /
      DATA      CPARAL(18) / 'LABEL_GAP_RIGHT_ANGLE' /

*     / PARAMETERS FOR USUSCL /

      DATA      CPARAL(19) / 'LOG_X_LABEL_NUMBER' /
      DATA      CPARAL(20) / 'LOG_Y_LABEL_NUMBER' /
      DATA      CPARAL(21) / 'LOG_X_TICKS_NUMBER' /
      DATA      CPARAL(22) / 'LOG_X_TICKS_NUMBER' /
      DATA      CPARAL(23) / 'LOG_X_LABEL_FORMAT' /
      DATA      CPARAL(24) / 'LOG_X_LABEL_FORMAT' /

*     / PARAMETERS FOR USXSUB & USYSUB /

      DATA      CPARAL(25) / 'X_SUBLABEL_MAX_CHAR' /
      DATA      CPARAL(26) / 'Y_SUBLABEL_MAX_CHAR' /
      DATA      CPARAL(27) / 'PROTECT_REGION' /
      DATA      CPARAL(28) / 'DRAW_X_SUBLABEL' /
      DATA      CPARAL(29) / 'DRAW_Y_SUBLABEL' /

*     / FOLLOWING 20 PARAMETERS ARE RESET BY USINIZ /

      DATA      CPARAL(30) / '****SOFFXTR ' /
      DATA      CPARAL(31) / '****SOFFXBR ' /
      DATA      CPARAL(32) / '****SOFFXUR ' /
      DATA      CPARAL(33) / '****SOFFXTL ' /
      DATA      CPARAL(34) / '****SOFFXBL ' /
      DATA      CPARAL(35) / '****SOFFXUL ' /
      DATA      CPARAL(36) / '****SOFFYRT ' /
      DATA      CPARAL(37) / '****SOFFYLT ' /
      DATA      CPARAL(38) / '****SOFFYUT ' /
      DATA      CPARAL(39) / '****SOFFYRB ' /
      DATA      CPARAL(40) / '****SOFFYLB ' /
      DATA      CPARAL(41) / '****SOFFYUB ' /
      DATA      CPARAL(42) / '****ROFFXT  ' /
      DATA      CPARAL(43) / '****ROFFXB  ' /
      DATA      CPARAL(44) / '****ROFFYR  ' /
      DATA      CPARAL(45) / '****ROFFYL  ' /

      DATA      CPARAL(46) / '****XDTMIN  ' /
      DATA      CPARAL(47) / '****XDTMAX  ' /
      DATA      CPARAL(48) / '****YDTMIN  ' /
      DATA      CPARAL(49) / '****YDTMAX  ' /


      NCP = NPARA

      RETURN
*-----------------------------------------------------------------------
      ENTRY USPQID(CP, IDX)

      DO 10 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IDX = N
          RETURN
        END IF
   10 CONTINUE
      CMSG = 'PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E','USPQID',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY USPQCP(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAS(IDX)
      ELSE
        CALL MSGDMP('E','USPQCP','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY USPQCL(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAL(IDX)
      ELSE
        CALL MSGDMP('E','USPQCL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY USPQIT(IDX, ITP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        ITP = ITYPE(IDX)
      ELSE
        CALL MSGDMP('E','USPQIT','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY USPQVL(IDX, IPARA)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        IF (ITYPE(IDX) .EQ. 1) THEN
          CALL USIQID(CPARAS(IDX), ID)
          CALL USIQVL(ID, IPARA)
        ELSE IF (ITYPE(IDX) .EQ. 2) THEN
          CALL USLQID(CPARAS(IDX), ID)
          CALL USLQVL(ID, IPARA)
        ELSE IF (ITYPE(IDX) .EQ. 3) THEN
          CALL USRQID(CPARAS(IDX), ID)
          CALL USRQVL(ID, IPARA)
        END IF
      ELSE
        CALL MSGDMP('E','USPQVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY USPSVL(IDX, IPARA)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        IF (ITYPE(IDX) .EQ. 1) THEN
          CALL USIQID(CPARAS(IDX), ID)
          CALL USISVL(ID, IPARA)
        ELSE IF (ITYPE(IDX) .EQ. 2) THEN
          CALL USLQID(CPARAS(IDX), ID)
          CALL USLSVL(ID, IPARA)
        ELSE IF (ITYPE(IDX) .EQ. 3) THEN
          CALL USRQID(CPARAS(IDX), ID)
          CALL USRSVL(ID, IPARA)
        END IF
      ELSE
        CALL MSGDMP('E','USPSVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY USPQIN(CP, IN)

      DO 20 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IN = N
          RETURN
        END IF
   20 CONTINUE

      IN = 0

      RETURN
      END
