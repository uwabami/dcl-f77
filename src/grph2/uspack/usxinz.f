*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USXINZ(CSA, FACA, OFFA)
      CHARACTER CSA, CS, CPOS
      CHARACTER*32  CSBLBL, CTTL, CTITLE, CUNIT, CSUB
      CHARACTER*16  CMIN, CMAX, CFMT0, CFMT1
      LOGICAL LOFF, LSUB, LABEL

      SAVE OFF0, FAC0, OFF1, FAC1, CFMT0, CFMT1, CS, FACTOR, OFFSET

      CALL GLRGET('RUNDEF',  RUNDEF)

      CS = CSA
      CALL UZRGET('ROFFX'//CS,ROFF)
      CALL UZRGET('ROFGX'//CS,ROFG)
      IF (ROFF.NE.ROFG)  CALL UXSAXS(CS)

      CALl UZRGET('RSIZEL1', SIZEL)
      CALL USRSET('SOFFX'//CS//'R',  SIZEL*0.86)
      CALL USRSET('SOFFX'//CS//'L', -SIZEL*0.86)

      CALL UZLGET('LOFFSET', LOFF)
      IF(LOFF) THEN
        CALL UZRGET('XOFFSET', OFF0)
        CALL UZRGET('XFACT',   FAC0)
      ELSE
        OFF0 = 0.
        FAC0 = 1.
      END IF

      CALL USRGET('XOFF',  OFF1)
      CALL USRGET('XFAC',  FAC1)
      IF(OFF1.EQ.RUNDEF) OFF1 = 0.
      IF(FAC1.EQ.RUNDEF) FAC1 = 1.
      OFFA = OFF1
      FACA = FAC1

      FACTOR = FAC0/FAC1
      OFFSET = (OFF0-OFF1)/FAC1
      CALL UZLSET('LOFFSET', .TRUE.)
      CALL UZRSET('XOFFSET', OFFSET)
      CALL UZRSET('XFACT',   FACTOR)

      CALL UZCGET('CXFMT', CFMT0)
      CALL USCGET('CXFMT', CFMT1)
      IF(CFMT1 .EQ. ' ') CFMT1 = CFMT0
      CALL UZCSET('CXFMT', CFMT1)

      RETURN
*-------------------------------------------------------------
      ENTRY USXTLZ

      CALL SGQWND(XMIN,XMAX,YMIN,YMAX)

      CALL UZLGET('LABELX'//CS, LABEL)
      CALL USCGET('CXUNIT', CUNIT)
      CALL USCGET('CXTTL ', CTTL)

      IF(LABEL) THEN
        CSUB = CSBLBL(FAC1, OFF1, CUNIT)
        CALL USLGET('LXSUB', LSUB)
        IF(LENZ(CSUB).NE.0) THEN
          IF(LSUB) THEN
            CALL CHVAL(CFMT1,XMIN*FACTOR+OFFSET,CMIN)
            CALL CHVAL(CFMT1,XMAX*FACTOR+OFFSET,CMAX)
            RLEN = MAX(LENZ(CMIN), LENZ(CMAX))

            CALL USCGET('CXSPOS', CPOS)
            CALL USXSUB(CS, CPOS, CSUB, RLEN)
            CTITLE = CTTL
          ELSE
            NLT = LENZ(CTTL)
            CTITLE = CTTL(1:NLT+1) // CSUB
          ENDIF
        ELSE
          CTITLE = CTTL
        ENDIF

        CALL CLADJ(CTITLE)
        IF(LENZ(CTITLE).NE.0) CALL UXSTTL(CS, CTITLE, 0.)
      ENDIF

      CALL UZRSET('XOFFSET', OFF0)
      CALL UZRSET('XFACT',   FAC0)
      CALL UZCSET('CXFMT',   CFMT0)

      END
