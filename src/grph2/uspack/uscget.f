*-----------------------------------------------------------------------
*     USCGET / USCSET / USCSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USCGET(CP,CPARA)

      CHARACTER CP*(*), CPARA*(*)

      CHARACTER CX*40, CPVAL*1024


      CALL USCQID(CP,IDX)
      CALL USCQVL(IDX,CPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY USCSET(CP,CPARA)

      CALL USCQID(CP,IDX)
      CALL USCSVL(IDX,CPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY USCSTX(CP, CPARA)

      CPVAL = CPARA

      CALL USCQID(CP, IDX)

      CALL USCQCP(IDX, CX)
      CALL RTCGET('US', CX, CPVAL, 1)

      CALL USCQCL(IDX, CX)
      CALL RLCGET(CX, CPVAL, 1)

      CALL USCSVL(IDX, CPVAL)

      RETURN
      END
