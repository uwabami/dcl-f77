*-----------------------------------------------------------------------
*     USPACK DRAW X-AXIS                              S.Sakai  99/10/09
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USXAXS(CSIDE)
      CHARACTER  CSIDE*(*), CS*1
      EXTERNAL   LENZ

      NCS = LENZ(CSIDE)
      DO 100 I=1, NCS
        CS = CSIDE(I:I)
        CALL CUPPER(CS)
        IF(CS.EQ.'U') CS='H'
        CALL USAXSC(CS)
  100 CONTINUE

      END
