*-----------------------------------------------------------------------
*     USLGET / USLSET / USLSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USLGET(CP, LPARA)

      LOGICAL   LPARA
      CHARACTER CP*(*)

      LOGICAL   LP
      CHARACTER CX*8
      CHARACTER CL*40


      CALL USLQID(CP, IDX)
      CALL USLQVL(IDX, LPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY USLSET(CP, LPARA)

      CALL USLQID(CP, IDX)
      CALL USLSVL(IDX, LPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY USLSTX(CP, LPARA)

      LP = LPARA
      CALL USLQID(CP, IDX)

*     / SHORT NAME /

      CALL USLQCP(IDX, CX)
      CALL RTLGET('US', CX, LP, 1)

*     / LONG NAME /

      CALL USLQCL(IDX, CL)
      CALL RLLGET(CL, LP, 1)

      CALL USLSVL(IDX,LP)

      RETURN
      END
