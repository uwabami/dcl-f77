*-----------------------------------------------------------------------
*     USPACK AXIS (UNIFORM TICK MARK)                 S.Sakai  99/09/30
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USAXDV(CSIDE, DTICK, DLBL)

      CHARACTER  CSIDE*(*), CS
      EXTERNAL   LENZ
      LOGICAL    LOFF

      CALL UZLGET('LOFFSET', LOFF)
      CALL UZLSET('LOFFSET', .TRUE.)

      NCS = LENZ(CSIDE)
      DO 100 I=1, NCS
        CS = CSIDE(I:I)
        CALL CUPPER(CS)

        IF(CS.EQ.'T' .OR. CS.EQ.'B' .OR. CS.EQ.'H') THEN
          IF(CS.EQ.'H') CS='U'
          CALL USXINZ(CS, FACTOR, OFFSET)
          CALL UXAXDV(CS, DTICK/FACTOR, DLBL/FACTOR)
          CALL USXTLZ

        ELSEIF(CS.EQ.'L' .OR. CS.EQ.'R' .OR. CS.EQ.'V') THEN
          IF(CS.EQ.'V') CS='U'
          CALL USYINZ(CS, FACTOR, OFFSET)
          CALL UYAXDV(CS, DTICK/FACTOR, DLBL/FACTOR)
          CALL USYTLZ
        ELSE
          CALL MSGDMP('E', 'USAXDV', 'INVALID SIDE NAME.')
        ENDIF

  100 CONTINUE
      CALL UZLSET('LOFFSET', LOFF)

      END
