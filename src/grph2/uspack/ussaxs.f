*-----------------------------------------------------------------------
*     USPACK SHIFT AXIS                               S.Sakai  99/10/13
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USSAXS(CSIDE)

      CHARACTER  CSIDE*(*), CS
      EXTERNAL   LENZ


      NCS = LENZ(CSIDE)
      DO 100 I=1, NCS
        CS = CSIDE(I:I)
        CALL CUPPER(CS)

        IF(CS.EQ.'T' .OR. CS.EQ.'B' .OR. CS.EQ.'H') THEN
          IF(CS.EQ.'H') CS='U'
          CALL UXSAXS(CS)
        ELSEIF(CS.EQ.'L' .OR. CS.EQ.'R' .OR. CS.EQ.'V') THEN
          IF(CS.EQ.'V') CS='U'
          CALL UYSAXS(CS)
        ENDIF

  100 CONTINUE

      END
