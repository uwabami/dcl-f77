*-----------------------------------------------------------------------
*     USPACK EASY GRAPH
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USGRPH(N, X, Y)

      REAL      X(*), Y(*)



      CALL USSPNT(N, X, Y)
      CALL USPFIT
      CALL GRSTRF
      CALL USDAXS
      CALL UULIN(N, X, Y)

      END
