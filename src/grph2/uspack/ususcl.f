*-----------------------------------------------------------------------
*     USPACK AUTO SCALING ROUTINE (LOG)                DCL 5.0 95/09/04
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USUSCL(CAXIS, UMIN,  UMAX,  VMIN,  VMAX)

      REAL        SC(4)
      CHARACTER   CAXIS*1, CP*8
      DATA        SC / 1., 2., 5., 10./

*----------------------- ARGUMENT CHECK --------------------------------

      IF(CAXIS.NE.'X' .AND. CAXIS.NE.'Y')
     #   CALL MSGDMP('E', 'USUSCL', 'INVALID CAXIS')

      IF(VMIN.GE.VMAX)
     #   CALL MSGDMP('E', 'USUSCL', 'VMIN>VMAX')

*--------------------------- PARAMETERS --------------------------------

      CP = 'MXDGT'//CAXIS
      CALL USIGET(CP, MXDGT)

      CALL GLRGET('RUNDEF' , RUNDEF)
      CALL GLIGET('IUNDEF' , IUNDEF)
      CALL UZRGET('RSIZEL1', CW)

*--------------------------- ITYPE & UFAC ------------------------------

      CALL GNSAVE
      CALL GNSBLK(SC, 4)
      CALL GNGE(MAX(UMAX,UMIN), BUMAX, IPMAX)
      CALL GNLE(MIN(UMIN,UMAX), BUMIN, IPMIN)
      CALL GNRSET

      IF(IPMAX.LE.IPMIN+1) THEN
        ITYPE = 1
!        IF(IPMAX+1.GT.MXDGT .OR. 2-IPMIN.GT.MXDGT) THEN
!          UFAC = 1.D1**IPMIN
!        ELSE
!        ENDIF
      ELSE
        ITYPE = 2
      ENDIF
      UFAC = 1.
!
      CP = CAXIS//'FAC'
      CALL USRGET(CP, UFACA)
      IF(UFACA .EQ.RUNDEF) CALL USRSET(CP, UFAC)

      CP = 'ITYPE'//CAXIS
      CALL USIGET(CP, ITYPEA)
      IF(ITYPEA.EQ.IUNDEF) CALL USISET(CP, ITYPE)

*--------------------------- NLBL & NTISCKS ----------------------------

      DV = (VMAX-VMIN) / ABS(LOG10(UMAX)-LOG10(UMIN)) / CW
      IF(DV.GE.10.) THEN
        NLBL   = 3
        NTICKS = 9
      ELSEIF(DV.GE.5.) THEN
        NLBL   = 1
        NTICKS = 9
      ELSEIF(DV.GE.2.5) THEN
        NLBL   = 1
        NTICKS = 5
      ELSE
        NLBL   = 1
        NTICKS = 2
      ENDIF

      CP = 'NLBL'//CAXIS
      CALL USIGET(CP, NLBLA)
      IF(NLBLA .EQ.IUNDEF) CALL USISET(CP, NLBL)

      CP = 'NTICKS'//CAXIS
      CALL USIGET(CP, NTICKA)
      IF(NTICKA.EQ.IUNDEF) CALL USISET(CP, NTICKS)

      RETURN
      END
