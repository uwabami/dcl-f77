*-----------------------------------------------------------------------
*     UEIGET / UEISET / UEISTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UEIGET(CP, IPARA)

      CHARACTER CP*(*)

      CHARACTER CX*8
      CHARACTER CL*40


      CALL UEIQID(CP, IDX)
      CALL UEIQVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UEISET(CP, IPARA)

      CALL UEIQID(CP, IDX)
      CALL UEISVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UEISTX(CP, IPARA)

      IP = IPARA
      CALL UEIQID(CP, IDX)

*     / SHORT NAME /

      CALL UEIQCP(IDX, CX)
      CALL RTIGET('UE', CX, IP, 1)

*     / LONG NAME /

      CALL UEIQCL(IDX, CL)
      CALL RLIGET(CL, IP, 1)

      CALL UEISVL(IDX,IP)

      RETURN
      END
