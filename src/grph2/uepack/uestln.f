*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UESTLN(TLEVN,IPATN,NTON)

      PARAMETER (MAXNT=100)

      INTEGER   IPATN(*)
      REAL      TLEVN(*)

      CHARACTER CMSG*80


*     / CHECK NUMBER OF TONE /

      CALL UEQNTL(NT)
      IF (NT+NTON.GT.MAXNT) THEN
        CMSG='NUMBER OF TONE IS IN EXCESS OF MAXIMUM (###).'
        WRITE(CMSG(41:43),'(I3)') MAXNT
        CALL MSGDMP('E','UESTLN',CMSG)
      END IF

      DO 10 IT=1,NTON

*       / CHECK IPAT /

        IF (IPATN(IT).LT.0) THEN
          CMSG='TONE PATTERN NUMBER IS LESS THAN ZERO.'
          CALL MSGDMP('E','UESTLN',CMSG)
        END IF

        CALL UESTLV(TLEVN(IT),TLEVN(IT+1),IPATN(IT))

   10 CONTINUE

      END
