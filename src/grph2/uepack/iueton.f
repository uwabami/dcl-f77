*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      INTEGER FUNCTION IUETON(ZLEV)

      COMMON    /UEBLK1/ TL1,TL2,IPT,NT,LASCND
      PARAMETER (MAXNT=100)
      LOGICAL   LASCND
      INTEGER   IPT(MAXNT)
      REAL      TL1(MAXNT),TL2(MAXNT)

      SAVE

      IF (LASCND) THEN
        ITBEG = 1
        ITEND = NT
        ITSTEP = 1
      ELSE
        ITBEG = NT
        ITEND = 1
        ITSTEP = -1
      END IF
      IF (ZLEV .LT. TL1(1)) THEN
        IT = 1
      ELSE IF (ZLEV .GT. TL2(NT)) THEN
        IT = NT
      ELSE
        DO 10 IT = ITBEG, ITEND, ITSTEP
          IF (TL1(IT) .LE. ZLEV .AND. ZLEV .LE. TL2(IT)) GO TO 20
 10   CONTINUE
 20   CONTINUE

      END IF
      IUETON = IPT(IT)

      END
