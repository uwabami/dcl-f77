*-----------------------------------------------------------------------
*     UERGET / UERSET / UERSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UERGET(CP, RPARA)

      CHARACTER CP*(*)

      CHARACTER CX*8
      CHARACTER CL*40


      CALL UERQID(CP, IDX)
      CALL UERQVL(IDX, RPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UERSET(CP, RPARA)

      CALL UERQID(CP, IDX)
      CALL UERSVL(IDX, RPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UERSTX(CP, RPARA)

      RP = RPARA
      CALL UERQID(CP, IDX)

*     / SHORT NAME /

      CALL UERQCP(IDX, CX)
      CALL RTRGET('UE', CX, RP, 1)

*     / LONG NAME /

      CALL UERQCL(IDX, CL)
      CALL RLRGET(CL, RP, 1)

      CALL UERSVL(IDX,RP)

      RETURN
      END
