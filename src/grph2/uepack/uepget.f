*-----------------------------------------------------------------------
*     UEPGET / UEPSET / UEPSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UEPGET(CP, IPARA)

      CHARACTER CP*(*)

      CHARACTER CX*8, CL*40


      CALL UEPQID(CP, IDX)
      CALL UEPQVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UEPSET(CP, IPARA)

      CALL UEPQID(CP, IDX)
      CALL UEPSVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UEPSTX(CP, IPARA)

      IP = IPARA
      CALL UEPQID(CP, IDX)
      CALL UEPQIT(IDX, IT)
      CALL UEPQCP(IDX, CX)
      CALL UEPQCL(IDX, CL)
      IF (IT .EQ. 1) THEN
        CALL RTIGET('UE', CX, IP, 1)
        CALL RLIGET(CL, IP, 1)
        CALL UEIQID(CP, IDX)
        CALL UEISVL(IDX, IP)
      ELSE IF (IT .EQ. 2) THEN
        CALL RTLGET('UE', CX, IP, 1)
        CALL RLLGET(CL, IP, 1)
        CALL UELQID(CP, IDX)
        CALL UELSVL(IDX, IP)
      ELSE IF (IT .EQ. 3) THEN
        CALL RTRGET('UE', CX, IP, 1)
        CALL RLRGET(CL, IP, 1)
        CALL UERQID(CP, IDX)
        CALL UERSVL(IDX, IP)
      END IF

      RETURN
      END
