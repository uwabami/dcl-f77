*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USGRPY(N, Y, XMIN, XMAX)

      REAL      Y(*)


      CALL MSGDMP('W','USGRPY','THIS IS OLD INTERFACE - USE USGRPH !')

      CALL GLRGET('RUNDEF', RUNDEF)
      CALL SGRSET('UXMIN', XMIN)
      CALL SGRSET('UXMAX', XMAX)

      CALL USSPNT(N, RUNDEF, Y)
      CALL USAXIS
      CALL UULIN(N, RUNDEF, Y)

      END
