*-----------------------------------------------------------------------
*     UBPACK
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UBPLU(N,UPX,UPY)

      REAL      UPX(*),UPY(*),VPX(*),VPY(*)
      CHARACTER CHARX*(*)

      LOGICAL   LCHAR


      CALL MSGDMP('M','UBPLU','THIS IS OLD INTERFACE - USE SGPLU !')

      CALL SGLGET('LCHAR',LCHAR)
      CALL SGLSET('LCHAR',.TRUE.)

      CALL SGPLU(N,UPX,UPY)

      CALL SGLSET('LCHAR',LCHAR)

      CALL SGNPLC

      RETURN
*-----------------------------------------------------------------------
      ENTRY UBPLV(N,VPX,VPY)

      CALL MSGDMP('M','UBPLV','THIS IS OLD INTERFACE - USE SGPLV !')

      CALL SGLGET('LCHAR',LCHAR)
      CALL SGLSET('LCHAR',.TRUE.)

      CALL SGPLV(N,VPX,VPY)

      CALL SGLSET('LCHAR',LCHAR)

      CALL SGNPLC

      RETURN
*-----------------------------------------------------------------------
      ENTRY UBSPLT(ITYPE)

      CALL MSGDMP('M','UBSPLT','THIS IS OLD INTERFACE - USE SGSPLT !')

      CALL SGSPLT(ITYPE)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UBQPLT(ITYPE)

      CALL MSGDMP('M','UBQPLT','THIS IS OLD INTERFACE - USE SGQPLT !')

      CALL SGQPLT(ITYPE)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UBSPLI(INDEX)

      CALL MSGDMP('M','UBSPLI','THIS IS OLD INTERFACE - USE SGSPLI !')

      CALL SGSPLI(INDEX)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UBQPLI(INDEX)

      CALL MSGDMP('M','UBQPLI','THIS IS OLD INTERFACE - USE SGQPLI !')

      CALL SGQPLI(INDEX)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UBSPLC(CHARX)

      CALL MSGDMP('M','UBSPLC','THIS IS OLD INTERFACE - USE SGSPLC !')

      CALL SGSPLC(CHARX)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UBQPLC(CHARX)

      CALL MSGDMP('M','UBQPLC','THIS IS OLD INTERFACE - USE SGQPLC !')

      CALL SGQPLC(CHARX)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UBSPLS(RSIZE)

      CALL MSGDMP('M','UBSPLS','THIS IS OLD INTERFACE - USE SGSPLS !')

      CALL SGSPLS(RSIZE)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UBQPLS(RSIZE)

      CALL MSGDMP('M','UBQPLS','THIS IS OLD INTERFACE - USE SGQPLS !')

      CALL SGQPLS(RSIZE)

      RETURN
      END
