*-----------------------------------------------------------------------
*     SET TRANSFORMATION
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UMSTRF


      CALL MSGDMP('W','UMSTRF',
     +     'THIS IS OLD INTERFACE - USE UMPFIT & GRSTRF !')

      CALL UMPFIT
      CALL GRSTRF

      END
