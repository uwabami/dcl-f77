*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UBPGET(CP,IPARA)

      CHARACTER CP*(*)


      CALL MSGDMP('M','UBPGET','THIS IS OLD INTERFACE - USE SGPGET !')

      CALL SGPGET(CP,IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UBPSET(CP,IPARA)

      CALL MSGDMP('M','UBPSET','THIS IS OLD INTERFACE - USE SGPSET !')

      CALL SGPSET(CP,IPARA)

      RETURN
      END
