*-----------------------------------------------------------------------
*     UGSGXA / UGSGXB
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UGSGXA(XP,NX)

      REAL      XP(*)
      LOGICAL   LSETX


      CALL MSGDMP('M','UGSGXA','THIS IS OLD INTERFACE - USE UWSGXA !')

      CALL UWSGXA(XP,NX)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UGQGXA(XP,NX)

      CALL MSGDMP('M','UGQGXA','THIS IS OLD INTERFACE - USE UWQGXA !')

      CALL UWQGXA(XP,NX)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UGSGXB(UXMIN,UXMAX,NX)

      CALL MSGDMP('M','UGSGXB','THIS IS OLD INTERFACE - USE UWSGXB !')

      CALL UWSGXB(UXMIN,UXMAX,NX)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UGQGXB(UXMIN,UXMAX,NX)

      CALL MSGDMP('M','UGQGXB','THIS IS OLD INTERFACE - USE UWQGXB !')

      CALL UWQGXB(UXMIN,UXMAX,NX)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UGSGXZ(LSETX)

      CALL MSGDMP('M','UGSGXZ','THIS IS OLD INTERFACE - USE UWSGXZ !')

      CALL UWSGXZ(LSETX)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UGQGXZ(LSETX)

      CALL MSGDMP('M','UGQGXZ','THIS IS OLD INTERFACE - USE UWQGXZ !')

      CALL UWQGXZ(LSETX)

      RETURN
      END
