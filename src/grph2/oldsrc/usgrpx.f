*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USGRPX(N, X, YMIN, YMAX)

      REAL      X(*)


      CALL MSGDMP('W','USGRPX','THIS IS OLD INTERFACE - USE USGRPH !')

      CALL GLRGET('RUNDEF', RUNDEF)
      CALL SGRSET('UYMIN', YMIN)
      CALL SGRSET('UYMAX', YMAX)

      CALL USSPNT(N, X, RUNDEF)
      CALL USAXIS
      CALL UULIN(N, X, RUNDEF)

      END
