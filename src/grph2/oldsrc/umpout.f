*-----------------------------------------------------------------------
*     DRAW CONTINENTAL OUTLINES
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UMPOUT


      CALL MSGDMP('M','UMPOUT','THIS IS OLD INTERFACE - USE UMPMAP !')

      CALL UMIGET('IGROUP',IG)

      IF (IG.EQ.1) THEN
        CALL UMPMAP('COAST_WORLD')
      ELSE IF (IG.EQ.2) THEN
        CALL UMPMAP('COAST_WORLD')
        CALL UMPMAP('BORDER_WORLD')
      ELSE
        CALL MSGDMP('M','UMPOUT','INVALID GROUP NUMBER / DO NOTHING.')
        RETURN
      END IF

      END
