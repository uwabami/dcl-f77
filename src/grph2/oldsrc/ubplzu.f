*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UBPLZU(N,UPX,UPY,ITYPE,INDEX,CHARX,RSIZE)

      REAL      UPX(*),UPY(*)
      CHARACTER CHARX*(*)

      LOGICAL   LCHAR


      CALL MSGDMP('M','UBPLZU','THIS IS OLD INTERFACE - USE SGPLZU !')

      CALL SGLGET('LCHAR',LCHAR)
      CALL SGLSET('LCHAR',.TRUE.)

      CALL SGSPLC(CHARX)
      CALL SGSPLS(RSIZE)

      CALL SGPLZU(N,UPX,UPY,ITYPE,INDEX)

      CALL SGLSET('LCHAR',LCHAR)

      END
