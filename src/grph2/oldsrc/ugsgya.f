*-----------------------------------------------------------------------
*     UGSGYA / UGSGYB
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UGSGYA(YP,NY)

      REAL      YP(*)
      LOGICAL   LSETY


      CALL MSGDMP('M','UGSGYA','THIS IS OLD INTERFACE - USE UWSGYA !')

      CALL UWSGYA(YP,NY)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UGQGYA(YP,NY)

      CALL MSGDMP('M','UGQGYA','THIS IS OLD INTERFACE - USE UWQGYA !')

      CALL UWQGYA(YP,NY)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UGSGYB(UYMIN,UYMAX,NY)

      CALL MSGDMP('M','UGSGYB','THIS IS OLD INTERFACE - USE UWSGYB !')

      CALL UWSGYB(UYMIN,UYMAX,NY)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UGQGYB(UYMIN,UYMAX,NY)

      CALL MSGDMP('M','UGQGYB','THIS IS OLD INTERFACE - USE UWQGYB !')

      CALL UWQGYB(UYMIN,UYMAX,NY)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UGSGYZ(LSETY)

      CALL MSGDMP('M','UGSGYZ','THIS IS OLD INTERFACE - USE UWSGYZ !')

      CALL UWSGYZ(LSETY)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UGQGYZ(LSETY)

      CALL MSGDMP('M','UGQGYZ','THIS IS OLD INTERFACE - USE UWQGYZ !')

      CALL UWQGYZ(LSETY)

      RETURN
      END
