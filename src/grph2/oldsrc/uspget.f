*-----------------------------------------------------------------------
*     USPGET / USPSET (OLD INTERFACE)
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USPGET(CP,IPARA)

      CHARACTER CP*(*)

      CHARACTER CX*8, CMSG*80, CPP*12

*     ----------------------
      ENTRY USIGET(CP,IPARA)
      ENTRY USRGET(CP,IPARA)
      ENTRY USLGET(CP,IPARA)
*     ----------------------

      IF(CP.EQ.'RUNDEF' .OR. CP.EQ.'IUNDEF') THEN
        CALL GLPGET(CP, IPARA)
        CMSG = 'THIS IS OLD INTERFACE ('//CP//') - USE GLPGET.'
        CALL MSGDMP('W', 'USPGET', CMSG)
      ELSEIF (CP.EQ.'VXMIN' .OR. CP.EQ.'VYMIN' .OR.
     #        CP.EQ.'VXMAX' .OR. CP.EQ.'VYMAX' .OR. CP.EQ.'ITR')  THEN
        CALL SGPGET(CP, IPARA)
        CMSG = 'THIS IS OLD INTERFACE ('//CP//') - USE SGPGET.'
        CALL MSGDMP('W', 'USPGET', CMSG)
      ELSEIF (CP.EQ.'XMIN' .OR. CP.EQ.'YMIN' .OR.
     #        CP.EQ.'XMAX' .OR. CP.EQ.'YMAX')  THEN
        CPP = 'U'//CP
        CALL SGPGET(CPP, IPARA)
        CMSG = 'THIS IS OLD INTERFACE ('//CP//
     #               ') - USE SGPGET (U'//CP//').'
        CALL MSGDMP('W', 'USPGET', CMSG)
      ELSE
        CALL USPQID(CP,IDX)
        CALL USPQVL(IDX,IPARA)
      ENDIF

      RETURN
*-----------------------------------------------------------------------
      ENTRY USPSET(CP,IPARA)

*     ----------------------
      ENTRY USISET(CP,IPARA)
      ENTRY USRSET(CP,IPARA)
      ENTRY USLSET(CP,IPARA)
*     ----------------------

      IF(CP.EQ.'RUNDEF' .OR. CP.EQ.'IUNDEF') THEN
        CALL GLPSET(CP, IPARA)
        CMSG = 'THIS IS OLD INTERFACE ('//CP//') - USE GLPSET.'
        CALL MSGDMP('W', 'USPSET', CMSG)
      ELSEIF (CP.EQ.'VXMIN' .OR. CP.EQ.'VYMIN' .OR.
     #        CP.EQ.'VXMAX' .OR. CP.EQ.'VYMAX' .OR. CP.EQ.'ITR')  THEN
        CALL SGPSET(CP, IPARA)
        CMSG = 'THIS IS OLD INTERFACE ('//CP//') - USE SGPSET.'
        CALL MSGDMP('W', 'USPSET', CMSG)
      ELSEIF (CP.EQ.'XMIN' .OR. CP.EQ.'YMIN' .OR.
     #        CP.EQ.'XMAX' .OR. CP.EQ.'YMAX')  THEN
        CPP = 'U'//CP
        CALL SGPSET(CPP, IPARA)
        CMSG = 'THIS IS OLD INTERFACE ('//CP//
     #         ') - USE SGPSET (U'//CP//').'
        CALL MSGDMP('W', 'USPSET', CMSG)
      ELSE
        CALL USPQID(CP,IDX)
        CALL USPSVL(IDX,IPARA)
      ENDIF

      RETURN
*-----------------------------------------------------------------------
      ENTRY USPSTX(CP,IPARA)

*     ----------------------
      ENTRY USISTX(CP,IPARA)

      CX=CP
      IP=IPARA
      CALL RTIGET('US',CX,IP,1)
      GO TO 10
*
      ENTRY USRSTX(CP,IPARA)

      CX=CP
      IP=IPARA
      CALL RTRGET('US',CX,IP,1)
      GO TO 10
*
      ENTRY USLSTX(CP,IPARA)

      CX=CP
      IP=IPARA
      CALL RTLGET('US',CX,IP,1)
*     ----------------------

 10   CONTINUE
      CALL USPQID(CX,IDX)
      CALL USPSVL(IDX,IP)

      RETURN
      END
