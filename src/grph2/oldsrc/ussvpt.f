*-----------------------------------------------------------------------
*     USPACK VIEW PORT                                S.Sakai  92/02/27
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USSVPT(VXMIN, VXMAX, VYMIN, VYMAX)


      CALL MSGDMP('W','USSVPT','THIS IS OLD INTERFACE - USE SGSVPT !')

      CALL SGRSET('VXMIN', VXMIN)
      CALL SGRSET('VXMAX', VXMAX)
      CALL SGRSET('VYMIN', VYMIN)
      CALL SGRSET('VYMAX', VYMAX)

      END
