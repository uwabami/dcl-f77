*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UBPLZV(N,VPX,VPY,ITYPE,INDEX,CHARX,RSIZE)

      REAL      VPX(*),VPY(*)
      CHARACTER CHARX*(*)

      LOGICAL   LCHAR


      CALL MSGDMP('M','UBPLZV','THIS IS OLD INTERFACE - USE SGPLZV !')

      CALL SGLGET('LCHAR',LCHAR)
      CALL SGLSET('LCHAR',.TRUE.)

      CALL SGSPLC(CHARX)
      CALL SGSPLS(RSIZE)

      CALL SGPLZV(N,VPX,VPY,ITYPE,INDEX)

      CALL SGLSET('LCHAR',LCHAR)

      END
