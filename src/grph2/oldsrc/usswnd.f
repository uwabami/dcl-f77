*-----------------------------------------------------------------------
*     USPACK SETTING WINDOW                           S.Sakai  92/02/29
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USSWND(XMIN, XMAX, YMIN, YMAX)


      CALL MSGDMP('W','USSWND','THIS IS OLD INTERFACE - USE SGSWND !')

      CALL SGRSET('UXMIN', XMIN)
      CALL SGRSET('UXMAX', XMAX)
      CALL SGRSET('UYMIN', YMIN)
      CALL SGRSET('UYMAX', YMAX)

      END
