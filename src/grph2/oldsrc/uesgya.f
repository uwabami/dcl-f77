*-----------------------------------------------------------------------
*     UESGYA / UESGYB
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UESGYA(YP,NY)

      REAL      YP(*)
      LOGICAL   LSETY


      CALL MSGDMP('M','UESGYA','THIS IS OLD INTERFACE - USE UWSGYA !')

      CALL UWSGYA(YP,NY)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UEQGYA(YP,NY)

      CALL MSGDMP('M','UEQGYA','THIS IS OLD INTERFACE - USE UWQGYA !')

      CALL UWQGYA(YP,NY)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UESGYB(UYMIN,UYMAX,NY)

      CALL MSGDMP('M','UESGYB','THIS IS OLD INTERFACE - USE UWSGYB !')

      CALL UWSGYB(UYMIN,UYMAX,NY)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UEQGYB(UYMIN,UYMAX,NY)

      CALL MSGDMP('M','UEQGYB','THIS IS OLD INTERFACE - USE UWQGYB !')

      CALL UWQGYB(UYMIN,UYMAX,NY)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UESGYZ(LSETY)

      CALL MSGDMP('M','UESGYZ','THIS IS OLD INTERFACE - USE UWSGYZ !')

      CALL UWSGYZ(LSETY)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UEQGYZ(LSETY)

      CALL MSGDMP('M','UEQGYZ','THIS IS OLD INTERFACE - USE UWQGYZ !')

      CALL UWQGYZ(LSETY)

      RETURN
      END
