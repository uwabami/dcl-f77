*-----------------------------------------------------------------------
*     NORMALIZATION TRANSFORMATION
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE GRSVPT(VXMIN, VXMAX, VYMIN, VYMAX)


      CALL SGRSTX('VXMIN',VXMIN)
      CALL SGRSTX('VXMAX',VXMAX)
      CALL SGRSTX('VYMIN',VYMIN)
      CALL SGRSTX('VYMAX',VYMAX)

      END
