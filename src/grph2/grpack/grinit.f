*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE GRINIT


      CALL GLRGET('RUNDEF', RUNDEF)

      CALL GRSVPT(RUNDEF, RUNDEF, RUNDEF, RUNDEF)
      CALL GRSWND(RUNDEF, RUNDEF, RUNDEF, RUNDEF)
      CALL GRSSIM(RUNDEF, RUNDEF, RUNDEF)
      CALL GRSMPL(RUNDEF, RUNDEF, RUNDEF)
      CALL GRSTXY(RUNDEF, RUNDEF, RUNDEF, RUNDEF)
      CALL GRSCWD(RUNDEF, RUNDEF, RUNDEF, RUNDEF)

      CALL SGRSTX('RSAT',   RUNDEF)
      CALL SGRSTX('STLAT1', RUNDEF)
      CALL SGRSTX('STLAT2', RUNDEF)

      END
