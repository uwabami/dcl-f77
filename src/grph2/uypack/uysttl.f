*-----------------------------------------------------------------------
*     UYSTTL : PLOT SUB (SMALL) TITLE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UYSTTL(CSIDE,CTTL,PX)

      CHARACTER CSIDE*1,CTTL*(*)

      LOGICAL   LUYCHK


      IF (.NOT.(LUYCHK(CSIDE))) THEN
        CALL MSGDMP('E','UYSTTL','SIDE PARAMETER IS INVALID.')
      END IF

      CALL UYPTTL(CSIDE,1,CTTL,PX)

      END
