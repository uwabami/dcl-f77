*-----------------------------------------------------------------------
*     UYAXDV : PLOT Y-AXIS (SPECIFY DIVISION)
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UYAXDV(CSIDE,DY1,DY2)

      PARAMETER (N=200)

      REAL      UY(N)
      CHARACTER CSIDE*1

      LOGICAL   LABEL,LEPSL,LREQ,LRLE,LUYCHK


*     / CHECK ARGUMENTS /

      IF (.NOT.(LUYCHK(CSIDE))) THEN
        CALL MSGDMP('E','UYAXDV','SIDE PARAMETER IS INVALID.')
      END IF
      IF (DY1.LE.0) THEN
        CALL MSGDMP('E','UYAXDV','MINOR DIVISION IS LESS THAN 0.')
      END IF
      IF (DY2.LE.0) THEN
        CALL MSGDMP('E','UYAXDV','MAJOR DIVISION IS LESS THAN 0.')
      END IF
      IF (DY1.LE.DY2) THEN
        DZ1=DY1
        DZ2=DY2
      ELSE
        CALL MSGDMP('W','UYAXDV',
     +    'MINOR DIVISION IS GREATER THAN MAJOR DIVISION.')
        DZ1=DY2
        DZ2=DY1
        CALL MSGDMP('M','-CNT.-','DY1 AND DY2 WERE SWITCHED.')
      END IF
      FRC=DZ2/DZ1-NINT(DZ2/DZ1)
      IF (ABS(FRC).GT.0.0001) THEN
        CALL MSGDMP('W','UYAXDV',
     +    'MAJOR DIVISION IS NOT MULTIPLE OF MINOR DIVISION.')
      END IF

*     / GET & SET INTERNAL PARAMETERS /

      CALL GLLGET('LEPSL',LEPSL)
      CALL GLRGET('REPSL',REPSL)
      CALL GLLSET('LEPSL',.TRUE.)

*     / SET SCALING FOR OFFSET /

      CALL UYSOFF

*     / INQUIRE NORMALIZATION TRANSFORMATION /

      CALL SGQWND(UXMN,UXMX,UYMN,UYMX)

      UYMNW=MIN(UYMN,UYMX)
      UYMXW=MAX(UYMN,UYMX)

*     / PLOT VERTICAL AXIS /

      CALL UYPAXS(CSIDE,2)

*     / GENERATE NUMBERS FOR MINOR TICKMARKS /

      NN=0
      RY=IRLE(UYMNW/DZ1)*DZ1
      IF (LREQ(UYMNW,RY)) THEN
        Y=RY
      ELSE
        Y=RY+DZ1
      END IF
   11 IF (.NOT.LRLE(Y,UYMXW)) GO TO 10
        NN=NN+1
        IF (ABS(Y).LT.DZ1*REPSL*NN) THEN
          Y=0
        END IF
        UY(NN)=Y
        Y=Y+DZ1
        GO TO 11
   10 CONTINUE

      IF (NN.EQ.0) THEN
        CALL MSGDMP('W','UYAXDV','THERE IS NO TICKMARK / LABEL.')
        GO TO 100
      ELSE IF (NN.GT.N) THEN
        CALL MSGDMP('E','UYAXDV','WORKING AREA IS NOT ENOUGH.')
      END IF

*     / IF DZ1.NE.DZ2 THEN PLOT MINOR TICKMARKS
*       ELSE SKIP TO THE PROCESS FOR MAJOR TICKMARKS /

      IF (DZ2.EQ.DZ1) GO TO 20

      CALL UYPTMK(CSIDE,1,UY,NN)

*     / GENERATE NUMBERS FOR MAJOR TICKMARKS AND LABELS /

      NN=0
      RY=IRLE(UYMNW/DZ2)*DZ2
      IF (LREQ(UYMNW,RY)) THEN
        Y=RY
      ELSE
        Y=RY+DZ2
      END IF
   16 IF (.NOT.LRLE(Y,UYMXW)) GO TO 15
        NN=NN+1
        IF (ABS(Y).LT.DZ2*REPSL*NN) THEN
          Y=0
        END IF
        UY(NN)=Y
        Y=Y+DZ2
        GO TO 16
   15 CONTINUE

   20 CONTINUE
      CALL UYPTMK(CSIDE,2,UY,NN)
      CALL UZLGET('LABELY'//CSIDE,LABEL)
      IF (LABEL) THEN
        CALL UYPNUM(CSIDE,1,UY,NN)
      END IF

*     / RESET INTERNAL PARAMETER /

  100 CALL GLLSET('LEPSL',LEPSL)

*     / RESET SCALING FOR OFFSET /

      CALL UYROFF

      END
