*-----------------------------------------------------------------------
*     UYPNUM : PLOT NUMBERS
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UYPNUM(CSIDE,ISLCT,UY,N)

      PARAMETER (NCH=40,LCH=12)

      REAL      UY(*)
      CHARACTER CSIDE*1

      LOGICAL   LUYCHK
      CHARACTER CFMTZ*16,CH(NCH)*(LCH)


*     / CHECK ARGUMENTS /

      IF (.NOT.(LUYCHK(CSIDE))) THEN
        CALL MSGDMP('E','UYPNUM','SIDE PARAMETER IS INVALID.')
      END IF
      IF (.NOT.(0.LE.ISLCT .AND. ISLCT.LE.2)) THEN
        CALL MSGDMP('E','UYPNUM','''ISLCT'' IS INVALID.')
      END IF
      IF (N.LE.0) THEN
        CALL MSGDMP('E','UYPNUM','NUMBER OF POINTS IS INVALID.')
      ELSE IF (N.GT.NCH) THEN
        CALL MSGDMP('E','UYPNUM','WORKING AREA IS NOT ENOUGH.')
      END IF

*     / GENERATE CHARACTERS /

      CALL UZCGET('CYFMT', CFMTZ)
      DO 10 I=1,N
        CALL CHVAL(CFMTZ,UY(I),CH(I))
   10 CONTINUE

*     / UYPLBL CALL /

      CALL UYPLBL(CSIDE,ISLCT,UY,CH,LCH,N)

      END
