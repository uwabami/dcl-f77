*-----------------------------------------------------------------------
*     UYSOFF / UYROFF
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UYSOFF

      LOGICAL   LCALL,LOFF

      SAVE

      DATA      LCALL/.FALSE./

      FY(Y)=YFCT*Y+YOFF


*     / SCALING FACTOR FOR OFFSET /

      CALL UZLGET('LOFFSET',LOFF)
      IF (LOFF) THEN
        CALL UZRGET('YOFFSET',YOFF)
        CALL UZRGET('YFACT  ',YFCT)
      ELSE
        YOFF=0.0
        YFCT=1.0
      END IF

*     / INQUIRE NORMALIZATION TRANSFORMATION /

      CALL SGQWND(UXMN,UXMX,UYMN,UYMX)

      FYMN=FY(UYMN)
      FYMX=FY(UYMX)

*     / SET NORMALIZATION TRANSFORMATION /

      CALL SGSWND(UXMN,UXMX,FYMN,FYMX)
      CALL SGSTRF

      LCALL=.TRUE.

      RETURN
*-----------------------------------------------------------------------
      ENTRY UYROFF

      IF (.NOT.LCALL) THEN
        CALL MSGDMP('E','UYROFF','UYSOFF HAS NOT BEEN CALLED.')
      END IF

*     / RESET NORMALIZATION TRANSFORMATION /

      CALL SGSWND(UXMN,UXMX,UYMN,UYMX)
      CALL SGSTRF

      RETURN
      END
