*-----------------------------------------------------------------------
*     UYPLBB : PLOT LABELS ( BETWEEN THE POINTS )
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UYPLBB(UY,CH,NC,N,UPX,ROFFY,RSIZE,IROTA,ICENT,INDEX,
     +                  RBTWN,LBOUND,LBMSG)

      REAL      UY(*)
      LOGICAL   LBOUND,LBMSG
      CHARACTER CH(*)*(*)

      LOGICAL   LCLIPZ


      IF (NC.LE.0) THEN
        CALL MSGDMP('E','UYPLBB',
     +       'CHARACTER LENGTH IS LESS THAN OR EQUAL TO ZERO.')
      END IF
      IF (N.LE.1) THEN
        CALL MSGDMP('E','UYPLBB','NUMBER OF POINTS IS INVALID.')
      END IF
      IF (RSIZE.LE.0) THEN
        CALL MSGDMP('E','UYPLBB','TEXT HEIGHT IS LESS THAN ZERO.')
      END IF
      IF (.NOT.(-1.LE.ICENT .AND. ICENT.LE.1)) THEN
        CALL MSGDMP('E','UYPLBB','CENTERING OPTION IS INVALID.')
      END IF
      IF (INDEX.LE.0) THEN
        CALL MSGDMP('E','UYPLBB','TEXT INDEX IS INVALID.')
      END IF

      CALL SGLGET('LCLIP',LCLIPZ)
      CALL SGLSET('LCLIP',.FALSE.)

      CALL SZTXOP(RSIZE,IROTA*90,ICENT,INDEX)
      DO 10 I=1,N-1
        LC = LENC(CH(I))
        CALL SZQTXW(CH(I),LCZ,WXCH,WYCH)
        CALL STFTRF(UPX,UY(I),VPX,VPY1)
        CALL STFTRF(UPX,UY(I+1),VPX,VPY2)
        VWY=ABS(VPY2-VPY1)
        JROTA=MOD(IROTA,2)
        IF (JROTA.NE.0) THEN
          CWY=RSIZE*WXCH
        ELSE
          CWY=RSIZE*WYCH
        END IF
        IF (CWY.GT.VWY .AND. LBOUND) THEN
          IF (LBMSG) THEN
            CALL MSGDMP('W','UYPLBB','SPACE FOR LABEL IS NOT ENOUGH.')
          END IF
          GO TO 10
        END IF
        VPY=(VPY1+VPY2)/2+(VWY-CWY)/2*RBTWN
        VPX=VPX+ROFFY
        CALL SZTXZV(VPX,VPY,CH(I)(1:LC))
   10 CONTINUE
      CALL SZTXCL

   15 CONTINUE

      CALL SGLSET('LCLIP',LCLIPZ)

      END
