*-----------------------------------------------------------------------
*     BASIC ROUTINES
*-----------------------------------------------------------------------
*     UYPTMZ : PLOT TICKMARKS
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UYPTMZ(UY,N,UPX,ROFFY,RTICK,INDEX)

      REAL      UY(*)

      LOGICAL   LCLIPZ


      IF (N.LE.0) THEN
        CALL MSGDMP('E','UYPTMZ','NUMBER OF POINTS IS INVALID.')
      END IF
      IF (INDEX.LE.0) THEN
        CALL MSGDMP('E','UYPTMZ','LINE INDEX IS INVALID.')
      END IF

      CALL SGLGET('LCLIP',LCLIPZ)
      CALL SGLSET('LCLIP',.FALSE.)

      CALL SZLNOP(INDEX)
      DO 10 I=1,N
        CALL STFTRF(UPX,UY(I),VPX,VPY)
        VPX=VPX+ROFFY
        CALL SZLNZV(VPX,VPY,VPX+RTICK,VPY)
   10 CONTINUE
      CALL SZLNCL

      CALL SGLSET('LCLIP',LCLIPZ)

      END
