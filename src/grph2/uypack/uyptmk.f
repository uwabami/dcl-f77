*-----------------------------------------------------------------------
*     UYPTMK : PLOT TICKMARKS
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UYPTMK(CSIDE,ISLCT,UY,N)

      REAL      UY(*)
      CHARACTER CSIDE*1

      LOGICAL   LUYCHK,LCHREQ
      CHARACTER CSLCT*1


      IF (.NOT.(LUYCHK(CSIDE))) THEN
        CALL MSGDMP('E','UYPTMK','SIDE PARAMETER IS INVALID.')
      END IF
      IF (.NOT.(0.LE.ISLCT .AND. ISLCT.LE.2)) THEN
        CALL MSGDMP('E','UYPTMK','''ISLCT'' IS INVALID.')
      END IF
      IF (N.LE.0) THEN
        CALL MSGDMP('E','UYPTMK','NUMBER OF POINTS IS INVALID.')
      END IF

      WRITE(CSLCT,'(I1)') ISLCT

      CALL UZRGET('ROFFY'//CSIDE,ROFFY)
      CALL UZRGET('ROFGY'//CSIDE,ROFGY)
      CALL UZIGET('INDEXT'//CSLCT,INDEX)
      CALL UZRGET('RSIZET'//CSLCT,RSIZE)
      CALL UZIGET('INNER',INNER)
      JSGN=SIGN(1,INNER)

      IF (.NOT.LCHREQ(CSIDE,'U')) THEN
        CALL SGQWND(UXMN,UXMX,UYMN,UYMX)
        IF (LCHREQ(CSIDE,'L')) THEN
          POSX=UXMN
          IFLAG=-1
        ELSE
          POSX=UXMX
          IFLAG=+1
        END IF
      ELSE
        CALL UZRGET('UXUSER',POSX)
        CALL UZIGET('IFLAG',IFLAG)
        IFLAG=SIGN(1,IFLAG)
      END IF

      RTICK=-RSIZE*JSGN*IFLAG

      IF (IFLAG.GE.0) THEN
        ROFFY=MAX(ROFGY+RTICK,ROFFY)
      ELSE
        ROFFY=MIN(ROFGY+RTICK,ROFFY)
      END IF

      CALL UYPTMZ(UY,N,POSX,ROFGY,RTICK,INDEX)

      CALL UZRSET('ROFFY'//CSIDE,ROFFY)

      END
