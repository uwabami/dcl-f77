*-----------------------------------------------------------------------
*     UYPLBL : PLOT LABELS
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UYPLBL(CSIDE,ISLCT,UY,CH,NC,N)

      REAL      UY(*)
      CHARACTER CSIDE*1,CH(*)*(*)

      LOGICAL   LBTWN,LUYCHK,LCHREQ
      CHARACTER CSLCT*1


      IF (.NOT.(LUYCHK(CSIDE))) THEN
        CALL MSGDMP('E','UYPLBL','SIDE PARAMETER IS INVALID.')
      END IF
      IF (.NOT.(0.LE.ISLCT .AND. ISLCT.LE.2)) THEN
        CALL MSGDMP('E','UYPLBL','''ISLCT'' IS INVALID.')
      END IF
      IF (NC.LE.0) THEN
        CALL MSGDMP('E','UYPLBL',
     +       'CHARACTER LENGTH IS LESS THAN OR EQUAL TO ZERO.')
      END IF
      IF (N.LE.0) THEN
        CALL MSGDMP('E','UYPLBL','NUMBER OF POINTS IS INVALID.')
      END IF

      WRITE(CSLCT,'(I1)') ISLCT

      CALL UZRGET('ROFFY'//CSIDE,ROFFY)
      CALL UZRGET('RSIZEL'//CSLCT,RSIZE)
      CALL UZIGET('ICENTY'//CSIDE,ICENT)
      CALL UZIGET('IROTLY'//CSIDE,IROTA)
      CALL UZIGET('INDEXL'//CSLCT,INDEX)
      CALL UZRGET('PAD1',PAD)
      CALL UZLGET('LBTWN',LBTWN)
      IF (LBTWN) THEN
        CALL UZRGET('RBTWN',RBTWN)
        NCMAX=N-1
      ELSE
        NCMAX=N
      END IF

      IF (.NOT.LCHREQ(CSIDE,'U')) THEN
        CALL SGQWND(UXMN,UXMX,UYMN,UYMX)
        IF (LCHREQ(CSIDE,'L')) THEN
          POSX=UXMN
          IFLAG=-1
        ELSE
          POSX=UXMX
          IFLAG=+1
        END IF
      ELSE
        CALL UZRGET('UXUSER',POSX)
        CALL UZIGET('IFLAG',IFLAG)
        IFLAG=SIGN(1,IFLAG)
      END IF

      JROTA=MOD(IROTA+3,4)-2
      IF (JROTA.EQ.-2) THEN
        JROTA=0
      END IF

      RLC=1
      DO 10 I=1,NCMAX
        CALL SZQTXW(CH(I),LCW,WXCH,WYCH)
        IF (JROTA.EQ.0 .AND. WYCH.GT.RLC) THEN
          RLC=WYCH
        ELSE IF (JROTA.NE.0 .AND. WXCH.GT.RLC) THEN
          RLC=WXCH
        END IF
   10 CONTINUE

      IC=JROTA*ICENT*IFLAG
      ROFFZ=ROFFY+RSIZE*(PAD+RLC*(1+IC)*0.5)*IFLAG
      ROFFY=ROFFY+RSIZE*(PAD+RLC)*IFLAG

      IF (LBTWN) THEN
        CALL UYPLBB(UY,CH,NC,N,POSX,ROFFZ,RSIZE,IROTA,ICENT,INDEX,
     +              RBTWN,.TRUE.,.FALSE.)
      ELSE
        CALL UYPLBA(UY,CH,NC,N,POSX,ROFFZ,RSIZE,IROTA,ICENT,INDEX)
      END IF

      CALL UZRSET('ROFFY'//CSIDE,ROFFY)

      END
