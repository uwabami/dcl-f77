*-----------------------------------------------------------------------
*     CONTROL ROUTINES
*-----------------------------------------------------------------------
*     UYSAXZ : OFFSET FOR AXIS
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UYSAXZ(CSIDE,ROFFY)

      CHARACTER CSIDE*1

      LOGICAL   LUYCHK


      IF (.NOT.(LUYCHK(CSIDE))) THEN
        CALL MSGDMP('E','UYSAXZ','SIDE PARAMETER IS INVALID.')
      END IF

      CALL UZRSET('ROFFY'//CSIDE,ROFFY)
      CALL UZRSET('ROFGY'//CSIDE,ROFFY)

      END
