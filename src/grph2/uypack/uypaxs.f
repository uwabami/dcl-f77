*-----------------------------------------------------------------------
*     LOWER LEVEL APPLICATIONS
*-----------------------------------------------------------------------
*     UYPAXS : PLOT VERTICAL LINE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UYPAXS(CSIDE,ISLCT)

      CHARACTER CSIDE*1

      LOGICAL   LUYCHK,LCHREQ,LCLIPZ
      CHARACTER CSLCT*1


      IF (.NOT.(LUYCHK(CSIDE))) THEN
        CALL MSGDMP('E','UYPAXS','SIDE PARAMETER IS INVALID.')
      END IF
      IF (.NOT.(0.LE.ISLCT .AND. ISLCT.LE.2)) THEN
        CALL MSGDMP('E','UYPAXS','''ISLCT'' IS INVALID')
      END IF

      CALL UZRGET('ROFFY'//CSIDE,ROFFY)
      CALL UZRGET('ROFGY'//CSIDE,ROFGY)
      IF (ROFFY.NE.ROFGY) RETURN

      WRITE(CSLCT,'(I1)') ISLCT

      CALL SGLGET('LCLIP',LCLIPZ)
      CALL SGLSET('LCLIP',.FALSE.)

      CALL UZIGET('INDEXT'//CSLCT,INDEX)

      CALL SGQWND(UXMN,UXMX,UYMN,UYMX)
      IF (.NOT.LCHREQ(CSIDE,'U')) THEN
        IF (LCHREQ(CSIDE,'L')) THEN
          POSX=UXMN
        ELSE
          POSX=UXMX
        END IF
      ELSE
        CALL UZRGET('UXUSER',POSX)
      END IF

      CALL STFTRF(POSX,UYMN,VX1,VY1)
      CALL STFTRF(POSX,UYMX,VX2,VY2)
      CALL SGLNZV(VX1+ROFGY,VY1,VX2+ROFGY,VY2,INDEX)

      CALL SGLSET('LCLIP',LCLIPZ)

      END
