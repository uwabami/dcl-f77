*-----------------------------------------------------------------------
*     LUYCHK : CHECK SIDE PARAMETER
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      LOGICAL FUNCTION LUYCHK(CSIDE)

      CHARACTER CSIDE*(*)

      CHARACTER CSX*1


      CSX=CSIDE(1:1)
      CALL CUPPER(CSX)
      LUYCHK=CSX.EQ.'L' .OR. CSX.EQ.'R' .OR. CSX.EQ.'U'

      END
