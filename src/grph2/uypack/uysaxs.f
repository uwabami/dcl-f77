*-----------------------------------------------------------------------
*     UYSAXS : OFFSET FOR NEW AXIS
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UYSAXS(CSIDE)

      CHARACTER CSIDE*1

      LOGICAL   LUYCHK,LCHREQ


      IF (.NOT.(LUYCHK(CSIDE))) THEN
        CALL MSGDMP('E','UYSAXS','SIDE PARAMETER IS INVALID.')
      END IF

      CALL UZRGET('ROFFY'//CSIDE,ROFFY)
      CALL UZRGET('RSIZET2',RSIZET)
      CALL UZRGET('RSIZEC2',RSIZEC)
      CALL UZRGET('PAD2',PAD)
      CALL UZIGET('INNER',INNER)
      JSGN=SIGN(1,INNER)

      IF (.NOT.LCHREQ(CSIDE,'U')) THEN
        IF (LCHREQ(CSIDE,'L')) THEN
          IFLAG=-1
        ELSE
          IFLAG=+1
        END IF
      ELSE
        CALL UZIGET('IFLAG',IFLAG)
        IFLAG=SIGN(1,IFLAG)
      END IF

      RTICK=-RSIZET*JSGN*IFLAG

      IF (IFLAG.GE.0) THEN
        ROFFY=MAX(ROFFY-RTICK,ROFFY)+RSIZEC*PAD
      ELSE
        ROFFY=MIN(ROFFY-RTICK,ROFFY)-RSIZEC*PAD
      END IF

      CALL UYSAXZ(CSIDE,ROFFY)

      END
