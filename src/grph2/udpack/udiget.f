*-----------------------------------------------------------------------
*     UDIGET / UDISET / UDISTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UDIGET(CP, IPARA)

      CHARACTER CP*(*)

      CHARACTER CX*8
      CHARACTER CL*40


      CALL UDIQID(CP, IDX)
      CALL UDIQVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDISET(CP, IPARA)

      CALL UDIQID(CP, IDX)
      CALL UDISVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDISTX(CP, IPARA)

      IP = IPARA
      CALL UDIQID(CP, IDX)

*     / SHORT NAME /

      CALL UDIQCP(IDX, CX)
      CALL RTIGET('UD', CX, IP, 1)

*     / LONG NAME /

      CALL UDIQCL(IDX, CL)
      CALL RLIGET(CL, IP, 1)

      CALL UDISVL(IDX,IP)

      RETURN
      END
