*-----------------------------------------------------------------------
*     UDGRDN
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UDGRDN(MODE,IX,IY,K,JX,JY,JK)

      INTEGER JX(0:3),JY(0:3),JK(0:3)


      KM=1-K
      IF (MODE.EQ.1) THEN
        JX(0)=IX
        JX(1)=IX+KM
        JX(2)=IX+K
        JX(3)=IX
        JY(0)=IY
        JY(1)=IY+K
        JY(2)=IY+KM
        JY(3)=IY
        JK(0)=K
        JK(1)=KM
        JK(2)=K
        JK(3)=KM
      ELSE
        JX(0)=IX
        JX(1)=IX-K
        JX(2)=IX-K
        JX(3)=IX+1-2*K
        JY(0)=IY
        JY(1)=IY-KM
        JY(2)=IY-KM
        JY(3)=IY+1-2*KM
        JK(0)=K
        JK(1)=KM
        JK(2)=K
        JK(3)=KM
      END IF

      END
