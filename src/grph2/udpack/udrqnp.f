*-----------------------------------------------------------------------
*     UDRQNP / UDRQID / UDRQCP / UDRQVL / UDRSVL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UDRQNP(NCP)

      CHARACTER CP*(*)

      PARAMETER (NPARA  = 3)
      PARAMETER (RUNDEF = -999.)

      REAL      RX(NPARA)
      LOGICAL   LCHREQ, LFIRST
      CHARACTER CPARAS(NPARA)*8
      CHARACTER CPARAL(NPARA)*40
      CHARACTER CMSG*80

      EXTERNAL  LCHREQ,LENC

      SAVE

*     / SHORT NAME /

      DATA      CPARAS(1) / 'RSIZEL  ' /, RX(1) / RUNDEF /
      DATA      CPARAS(2) / 'RSIZET  ' /, RX(2) / RUNDEF /
      DATA      CPARAS(3) / 'XTTL    ' /, RX(3) / 0.0 /

*     / LONG NAME /

      DATA      CPARAL(1) / 'CONTOUR_LABEL_HEIGHT' /
      DATA      CPARAL(2) / 'CONTOUR_MESSAGE_HEIGHT' /
      DATA      CPARAL(3) / 'CONTOUR_MESSAGE_POSITION' /

      DATA      LFIRST / .TRUE. /


      NCP = NPARA

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDRQID(CP, IDX)

      DO 10 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IDX = N
          RETURN
        END IF
   10 CONTINUE
      CMSG = 'PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E','UDRQID',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDRQCP(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAS(IDX)
      ELSE
        CALL MSGDMP('E','UDRQCP','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDRQCL(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAL(IDX)
      ELSE
        CALL MSGDMP('E','UDRQCL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDRQVL(IDX, RPARA)

      IF (LFIRST) THEN
        CALL RTRGET('UD', CPARAS, RX, NPARA)
        CALL RLRGET(CPARAL, RX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        RPARA = RX(IDX)
        IF ((IDX.EQ.1 .OR. IDX.EQ.2) .AND. RPARA.EQ.RUNDEF) THEN
          CALL UZRGET('RSIZEL1', RPARA)
        END IF
      ELSE
        CALL MSGDMP('E','UDRQVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDRSVL(IDX, RPARA)

      IF (LFIRST) THEN
        CALL RTRGET('UD', CPARAS, RX, NPARA)
        CALL RLRGET(CPARAL, RX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        RX(IDX) = RPARA
      ELSE
        CALL MSGDMP('E','UDRSVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDRQIN(CP, IN)

      DO 20 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IN = N
          RETURN
        END IF
   20 CONTINUE

      IN = 0

      RETURN
      END
