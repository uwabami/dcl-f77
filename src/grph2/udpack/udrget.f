*-----------------------------------------------------------------------
*     UDRGET / UDRSET / UDRSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UDRGET(CP, RPARA)

      CHARACTER CP*(*)

      CHARACTER CX*8
      CHARACTER CL*40


      CALL UDRQID(CP, IDX)
      CALL UDRQVL(IDX, RPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDRSET(CP, RPARA)

      CALL UDRQID(CP, IDX)
      CALL UDRSVL(IDX, RPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDRSTX(CP, RPARA)

      RP = RPARA
      CALL UDRQID(CP, IDX)

*     / SHORT NAME /

      CALL UDRQCP(IDX, CX)
      CALL RTRGET('UD', CX, RP, 1)

*     / LONG NAME /

      CALL UDRQCL(IDX, CL)
      CALL RLRGET(CL, RP, 1)

      CALL UDRSVL(IDX,RP)

      RETURN
      END
