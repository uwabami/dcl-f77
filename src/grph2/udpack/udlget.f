*-----------------------------------------------------------------------
*     UDLGET / UDLSET / UDLSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UDLGET(CP, LPARA)

      CHARACTER CP*(*)

      CHARACTER CX*8
      CHARACTER CL*40
      LOGICAL   LPARA, LP

      CALL UDLQID(CP, IDX)
      CALL UDLQVL(IDX, LPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDLSET(CP, LPARA)

      CALL UDLQID(CP, IDX)
      CALL UDLSVL(IDX, LPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDLSTX(CP, LPARA)

      LP = LPARA
      CALL UDLQID(CP, IDX)

*     / SHORT NAME /

      CALL UDLQCP(IDX, CX)
      CALL RTLGET('UD', CX, LP, 1)

*     / LONG NAME /

      CALL UDLQCL(IDX, CL)
      CALL RLLGET(CL, LP, 1)

      CALL UDLSVL(IDX,LP)

      RETURN
      END
