*-----------------------------------------------------------------------
*     UDLQNP / UDLQID / UDLQCP / UDLQVL / UDLSVL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UDLQNP(NCP)

      INTEGER   NCP
      LOGICAL   LPARA
      CHARACTER CP*(*)

      PARAMETER (NPARA = 4)

      LOGICAL   LX(NPARA)
      LOGICAL   LCHREQ, LFIRST
      CHARACTER CPARAS(NPARA)*8
      CHARACTER CPARAL(NPARA)*40
      CHARACTER CMSG*80

      EXTERNAL  LCHREQ,LENC

      SAVE

*     / SHORT NAME /

      DATA      CPARAS(1) / 'LDASH   ' /, LX(1) / .TRUE. /
      DATA      CPARAS(2) / 'LABEL   ' /, LX(2) / .TRUE. /
      DATA      CPARAS(3) / 'LMSG    ' /, LX(3) / .TRUE. /
      DATA      CPARAS(4) / 'LCENT   ' /, LX(4) / .TRUE. /

*     / LONG NAME /

      DATA      CPARAL(1) / 'ENABLE_NEGATIVE_CONTOUR' /
      DATA      CPARAL(2) / 'ENABLE_LABELED_CONTOUR' /
      DATA      CPARAL(3) / 'ENABLE_CONTOUR_MESSAGE' /
      DATA      CPARAL(4) / 'ENABLE_MESSAGE_CENTERING' /

      DATA      LFIRST / .TRUE. /

      NCP = NPARA

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDLQID(CP, IDX)

      DO 10 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IDX = N
          RETURN
        END IF
   10 CONTINUE
      CMSG = 'PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E','UDLQID',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDLQCP(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAS(IDX)
      ELSE
        CALL MSGDMP('E','UDLQCP','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDLQCL(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAL(IDX)
      ELSE
        CALL MSGDMP('E','UDLQCL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDLQVL(IDX, LPARA)

      IF (LFIRST) THEN
        CALL RTLGET('UD', CPARAS, LX, NPARA)
        CALL RLLGET(CPARAL, LX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        LPARA = LX(IDX)
      ELSE
        CALL MSGDMP('E','UDLQVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDLSVL(IDX, LPARA)

      IF (LFIRST) THEN
        CALL RTLGET('UD', CPARAS, LX, NPARA)
        CALL RLLGET(CPARAL, LX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        LX(IDX) = LPARA
      ELSE
        CALL MSGDMP('E','UDLSVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDLQIN(CP, IN)

      DO 20 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IN = N
          RETURN
        END IF
   20 CONTINUE

      IN = 0

      RETURN
      END
