*-----------------------------------------------------------------------
*     UDLABL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UDLABL(VAL,CVAL)

      CHARACTER CVAL*(*),CFMT*(*)

      CHARACTER CFMTZ*16

      SAVE

      DATA      CFMTZ/'D'/


      CALL CHVAL(CFMTZ,VAL,CVAL)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDSFMT(CFMT)

      CFMTZ=CFMT

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDQFMT(CFMT)

      CFMT=CFMTZ

      RETURN
      END
