*-----------------------------------------------------------------------
*     UDPQNP / UDPQID / UDPQCP / UDPQVL / UDPSVL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UDPQNP(NCP)

      CHARACTER CP*(*)

      PARAMETER (NPARA = 14)

      INTEGER   ITYPE(NPARA)
      LOGICAL   LCHREQ
      CHARACTER CPARAS(NPARA)*8
      CHARACTER CPARAL(NPARA)*40
      CHARACTER CMSG*80

      EXTERNAL  LCHREQ,LENC

      SAVE

*     / SHORT NAME /

      DATA      CPARAS( 1) / 'INDXMJ  ' /, ITYPE( 1) / 1 /
      DATA      CPARAS( 2) / 'INDXMN  ' /, ITYPE( 2) / 1 /
      DATA      CPARAS( 3) / 'ISOLID  ' /, ITYPE( 3) / 1 /
      DATA      CPARAS( 4) / 'IDASH   ' /, ITYPE( 4) / 1 /
      DATA      CPARAS( 5) / 'LDASH   ' /, ITYPE( 5) / 2 /
      DATA      CPARAS( 6) / 'LABEL   ' /, ITYPE( 6) / 2 /
      DATA      CPARAS( 7) / 'ICYCLE  ' /, ITYPE( 7) / 1 /
      DATA      CPARAS( 8) / 'NLEV    ' /, ITYPE( 8) / 1 /
      DATA      CPARAS( 9) / 'RSIZEL  ' /, ITYPE( 9) / 3 /
      DATA      CPARAS(10) / 'RSIZET  ' /, ITYPE(10) / 3 /
      DATA      CPARAS(11) / 'XTTL    ' /, ITYPE(11) / 3 /
      DATA      CPARAS(12) / 'LMSG    ' /, ITYPE(12) / 2 /
      DATA      CPARAS(13) / 'LCENT   ' /, ITYPE(13) / 2 /
      DATA      CPARAS(14) / 'IUNDEF  ' /, ITYPE(14) / 1 /

*     / LONG NAME /

      DATA      CPARAL( 1) / 'MAJOR_CONTOUR_INDEX' /
      DATA      CPARAL( 2) / 'MINOR_CONTOUR_INDEX' /
      DATA      CPARAL( 3) / 'POSITIVE_CONTOUR_TYPE' /
      DATA      CPARAL( 4) / 'NEGATIVE_CONTOUR_TYPE' /
      DATA      CPARAL( 5) / 'ENABLE_NEGATIVE_CONTOUR' /
      DATA      CPARAL( 6) / 'ENABLE_LABELED_CONTOUR' /
      DATA      CPARAL( 7) / 'MAJOR_CONTOUR_CYCLE' /
      DATA      CPARAL( 8) / 'CONTOUR_LEVEL_NUMBER' /
      DATA      CPARAL( 9) / 'CONTOUR_LABEL_HEIGHT' /
      DATA      CPARAL(10) / 'CONTOUR_MESSAGE_HEIGHT' /
      DATA      CPARAL(11) / 'CONTOUR_MESSAGE_POSITION' /
      DATA      CPARAL(12) / 'ENABLE_CONTOUR_MESSAGE' /
      DATA      CPARAL(13) / 'ENABLE_MESSAGE_CENTERING' /
      DATA      CPARAL(14) / '----IUNDEF  ' /


      NCP = NPARA

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDPQID(CP, IDX)

      DO 10 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IDX = N
          RETURN
        END IF
   10 CONTINUE
      CMSG = 'PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E','UDPQID',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDPQCP(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAS(IDX)
      ELSE
        CALL MSGDMP('E','UDPQCP','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDPQCL(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAL(IDX)
      ELSE
        CALL MSGDMP('E','UDPQCL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDPQIT(IDX, ITP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        ITP = ITYPE(IDX)
      ELSE
        CALL MSGDMP('E','UDPQIT','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDPQVL(IDX, IPARA)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        IF (ITYPE(IDX) .EQ. 1) THEN
          CALL UDIQID(CPARAS(IDX), ID)
          CALL UDIQVL(ID, IPARA)
        ELSE IF (ITYPE(IDX) .EQ. 2) THEN
          CALL UDLQID(CPARAS(IDX), ID)
          CALL UDLQVL(ID, IPARA)
        ELSE IF (ITYPE(IDX) .EQ. 3) THEN
          CALL UDRQID(CPARAS(IDX), ID)
          CALL UDRQVL(ID, IPARA)
        END IF
      ELSE
        CALL MSGDMP('E','UDPQVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDPSVL(IDX, IPARA)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        IF (ITYPE(IDX) .EQ. 1) THEN
          CALL UDIQID(CPARAS(IDX), ID)
          CALL UDISVL(ID, IPARA)
        ELSE IF (ITYPE(IDX) .EQ. 2) THEN
          CALL UDLQID(CPARAS(IDX), ID)
          CALL UDLSVL(ID, IPARA)
        ELSE IF (ITYPE(IDX) .EQ. 3) THEN
          CALL UDRQID(CPARAS(IDX), ID)
          CALL UDRSVL(ID, IPARA)
        END IF
      ELSE
        CALL MSGDMP('E','UDPSVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDPQIN(CP, IN)

      DO 20 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IN = N
          RETURN
        END IF
   20 CONTINUE

      IN = 0

      RETURN
      END
