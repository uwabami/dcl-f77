*-----------------------------------------------------------------------
*     RUDLEV
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      REAL FUNCTION RUDLEV(NLEV)

      CHARACTER CLV1*8,CLV2*8


      CALL UDQCLN(NLEVZ)
      IF (.NOT.(2.LE.NLEVZ)) THEN
        CALL MSGDMP('W','IUDLEV',
     +       'NO. OF CONTOUR LEVELS IS LESS THAN 2.')
        RUDLEV=0
        RETURN
      END IF
      IF (.NOT.(1.LE.NLEV .AND. NLEV.LE.NLEVZ-1)) THEN
        CALL MSGDMP('W','IUDLEV','NLEV IS OUT OF RANGE.')
        RUDLEV=0
        RETURN
      END IF

      CALL UDQCLV(ZLEV1,INDX1,ITYP1,CLV1,HL1,NLEV  )
      CALL UDQCLV(ZLEV2,INDX2,ITYP2,CLV2,HL2,NLEV+1)
      RUDLEV=ZLEV2-ZLEV1

      END
