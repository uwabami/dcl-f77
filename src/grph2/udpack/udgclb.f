*-----------------------------------------------------------------------
*     UDGCLB
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UDGCLB(Z,MX,NX,NY,DX)

      REAL      Z(MX,*)

      INTEGER   NS(2),NP(2),NQ(2)

      EXTERNAL  RVMIN,RVMAX,RGNGE,IRLE,IRGE


      CALL GLLGET('LMISS',LMISS)
      CALL GLRGET('RMISS',RMISS)

      NS(1)=MX
      NS(2)=NY
      NP(1)=1
      NP(2)=1
      NQ(1)=NX
      NQ(2)=NY
      XMIN=RVMIN(Z,NS,NP,NQ,2)
      XMAX=RVMAX(Z,NS,NP,NQ,2)

      IF (XMIN.EQ.XMAX) RETURN

      IF (DX.GT.0) THEN
        DZ=DX
      ELSE IF (DX.EQ.0) THEN
        CALL UDIGET('NLEV',NLEV)
        DZ=RGNGE((XMAX-XMIN)/NLEV)
      ELSE
        NL=MAX(1,NINT(ABS(DX)))
        DZ=RGNGE((XMAX-XMIN)/NL)
      END IF
      ZMIN=IRLE(XMIN/DZ)*DZ
      ZMAX=IRGE(XMAX/DZ)*DZ

      CALL UDGCLA(ZMIN,ZMAX,DZ)

      END
