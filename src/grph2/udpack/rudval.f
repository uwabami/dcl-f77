*-----------------------------------------------------------------------
*     RUDVAL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      REAL FUNCTION RUDVAL(ZZ,CZ)

      LOGICAL   LFST

      SAVE

      DATA      LFST/.TRUE./


      IF (LFST) THEN
        CALL GLRGET('REPSL',REPSL)
        LFST=.FALSE.
      END IF

      IF (ZZ.EQ.CZ) THEN
        IF (ZZ.EQ.0) THEN
          RUDVAL=REPSL**2
        ELSE
          RUDVAL=ZZ*(1+REPSL)
        END IF
      ELSE
        RUDVAL=ZZ
      END IF

      END
