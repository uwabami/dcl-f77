*-----------------------------------------------------------------------
*     UDICLV / UDSCLV / UDDCLV
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UDICLV

      CHARACTER CLV*(*)
      LOGICAL   LSET

      LOGICAL   LSETZ
      CHARACTER CMSG*80

      COMMON    /UDBLK2/ NL,CZL,IDX,ITY,HLV
      COMMON    /UDBLK3/ CLAB
      PARAMETER (NLX=50)
      INTEGER   IDX(NLX),ITY(NLX)
      REAL      CZL(NLX),HLV(NLX)
      CHARACTER CLAB(NLX)*8

      SAVE

      DATA      LSETZ/.FALSE./


      LSETZ=.FALSE.
      NL=0

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDSCLV(ZLEV,INDX,ITYP,CLV,HL)

      IP=INDXRF(CZL,NL,1,ZLEV)
      IF (IP.NE.0) THEN
        NN=IP
      ELSE
        IF (NL.GE.NLX) THEN
          CMSG='NUMBER OF CONTOUR LEVELS IS IN EXCESS OF MAXIMUM (##)'
          WRITE(CMSG(51:52),'(I2)') NLX
          CALL MSGDMP('E','UDSCLV',CMSG)
        END IF
        NL=NL+1
        NN=NL
      END IF

      CZL(NN)=ZLEV
      IDX(NN)=INDX
      ITY(NN)=ITYP
      HLV(NN)=HL
      CLAB(NN)=CLV
      LSETZ=.TRUE.

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDQCLV(ZLEV,INDX,ITYP,CLV,HL,NLEV)

      IF (.NOT.(1.LE.NLEV .AND. NLEV.LE.NL)) THEN
        CMSG='LEVEL NUMBER (##) IS OUT OF RANGE (1-##).'
        WRITE(CMSG(15:16),'(I2)') NLEV
        WRITE(CMSG(38:39),'(I2)') NL
        CALL MSGDMP('E','UDQCLV',CMSG)
      END IF

      ZLEV=CZL(NLEV)
      INDX=IDX(NLEV)
      ITYP=ITY(NLEV)
      HL=HLV(NLEV)
      CLV=CLAB(NLEV)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDQCLN(NLEV)

      NLEV=NL

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDDCLV(ZLEV)

      IP=INDXRF(CZL,NL,1,ZLEV)
      IF (IP.NE.0) THEN
        DO 10 I=IP,NL-1
          CZL(I)=CZL(I+1)
          IDX(I)=IDX(I+1)
          ITY(I)=ITY(I+1)
          HLV(I)=HLV(I+1)
          CLAB(I)=CLAB(I+1)
   10   CONTINUE
        NL=NL-1
        IF (NL.EQ.0) THEN
          LSETZ=.FALSE.
        END IF
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDSCLZ(LSET)

      LSETZ=LSET

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDQCLZ(LSET)

      LSET=LSETZ

      RETURN
      END
