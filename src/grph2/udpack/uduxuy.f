*-----------------------------------------------------------------------
*     UDUXUY
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UDUXUY(Z,MX,IX,IY,K,CX,UX,UY)

      REAL      Z(MX,*)

      EXTERNAL  RUWGX,RUWGY


      IX1=IX
      IX2=IX+1-K
      IY1=IY
      IY2=IY+K

      XD1=RUWGX(IX1)
      XD2=RUWGX(IX2)
      YD1=RUWGY(IY1)
      YD2=RUWGY(IY2)

      IF (Z(IX1,IY1).EQ.CX) THEN
        ZZ1=RUDVAL(Z(IX1,IY1),CX)
      ELSE
        ZZ1=Z(IX1,IY1)
      END IF
      IF (Z(IX2,IY2).EQ.CX) THEN
        ZZ2=RUDVAL(Z(IX2,IY2),CX)
      ELSE
        ZZ2=Z(IX2,IY2)
      END IF
      CC=(CX-ZZ1)/(ZZ2-ZZ1)

      UX=(XD2-XD1)*CC+XD1
      UY=(YD2-YD1)*CC+YD1

      END
