*-----------------------------------------------------------------------
*     ULRQNP / ULRQID / ULRQCP / ULRQVL / ULRSVL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE ULRQNP(NCP)

      CHARACTER CP*(*)

      CHARACTER CMSG*80

      SAVE


      NCP = 0

      RETURN
*-----------------------------------------------------------------------
      ENTRY ULRQID(CP, IDX)

      IDX = 0

      CMSG = 'PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E','ULRQID',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY ULRQCP(IDX, CP)

      CALL MSGDMP('E','ULRQCP','IDX IS OUT OF RANGE.')

      RETURN
*-----------------------------------------------------------------------
      ENTRY ULRQCL(IDX, CP)

      CALL MSGDMP('E','ULRQCL','IDX IS OUT OF RANGE.')

      RETURN
*-----------------------------------------------------------------------
      ENTRY ULRQVL(IDX, RPARA)

      RPARA = 0

      CALL MSGDMP('E','ULRQVL','IDX IS OUT OF RANGE.')

      RETURN
*-----------------------------------------------------------------------
      ENTRY ULRSVL(IDX, RPARA)

      CALL MSGDMP('E','ULRSVL','IDX IS OUT OF RANGE.')

      RETURN
*-----------------------------------------------------------------------
      ENTRY ULRQIN(CP, IN)

      IN = 0

      RETURN
      END
