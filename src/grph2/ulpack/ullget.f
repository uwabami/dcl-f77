*-----------------------------------------------------------------------
*     ULLGET / ULLSET / ULLSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE ULLGET(CP, LPARA)

      LOGICAL   LPARA
      CHARACTER CP*(*)

      LOGICAL   LP
      CHARACTER CX*8
      CHARACTER CL*40


      CALL ULLQID(CP, IDX)
      CALL ULLQVL(IDX, LPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY ULLSET(CP, LPARA)

      CALL ULLQID(CP, IDX)
      CALL ULLSVL(IDX, LPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY ULLSTX(CP, LPARA)

      LP = LPARA
      CALL ULLQID(CP, IDX)

*     / SHORT NAME /

      CALL ULLQCP(IDX, CX)
      CALL RTLGET('UL', CX, LP, 1)

*     / LONG NAME /

      CALL ULLQCL(IDX, CL)
      CALL RLLGET(CL, LP, 1)

      CALL ULLSVL(IDX,LP)

      RETURN
      END
