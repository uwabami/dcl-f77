*-----------------------------------------------------------------------
*     ULRGET / ULRSET / ULRSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE ULRGET(CP, RPARA)

      CHARACTER CP*(*)

      CHARACTER CX*8
      CHARACTER CL*40


      CALL ULRQID(CP, IDX)
      CALL ULRQVL(IDX, RPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY ULRSET(CP, RPARA)

      CALL ULRQID(CP, IDX)
      CALL ULRSVL(IDX, RPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY ULRSTX(CP, RPARA)

      RP = RPARA
      CALL ULRQID(CP, IDX)

*     / SHORT NAME /

      CALL ULRQCP(IDX, CX)
      CALL RTRGET('UL', CX, RP, 1)

*     / LONG NAME /

      CALL ULRQCL(IDX, CL)
      CALL RLRGET(CL, RP, 1)

      CALL ULRSVL(IDX,RP)

      RETURN
      END
