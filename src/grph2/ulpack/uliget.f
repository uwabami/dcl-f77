*-----------------------------------------------------------------------
*     ULIGET / ULISET / ULISTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE ULIGET(CP, IPARA)

      CHARACTER CP*(*)

      CHARACTER CX*8
      CHARACTER CL*40


      CALL ULIQID(CP, IDX)
      CALL ULIQVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY ULISET(CP, IPARA)

      CALL ULIQID(CP, IDX)
      CALL ULISVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY ULISTX(CP, IPARA)

      IP = IPARA
      CALL ULIQID(CP, IDX)

*     / SHORT NAME /

      CALL ULIQCP(IDX, CX)
      CALL RTIGET('UL', CX, IP, 1)

*     / LONG NAME /

      CALL ULIQCL(IDX, CL)
      CALL RLIGET(CL, IP, 1)

      CALL ULISVL(IDX,IP)

      RETURN
      END
