*-----------------------------------------------------------------------
*     ULIQNP / ULIQID / ULIQCP / ULIQVL / ULISVL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE ULIQNP(NCP)

      CHARACTER CP*(*)

      PARAMETER (NPARA = 4)

      INTEGER   IX(NPARA)
      LOGICAL   LCHREQ, LFIRST
      CHARACTER CPARAS(NPARA)*8
      CHARACTER CPARAL(NPARA)*40
      CHARACTER CMSG*80

      EXTERNAL  LCHREQ, LENC

      SAVE

*     / SHORT NAME /

      DATA      CPARAS(1) / 'IXCHR   ' /, IX(1) / 195 /
      DATA      CPARAS(2) / 'IYCHR   ' /, IX(2) / 195 /
      DATA      CPARAS(3) / 'IXTYPE  ' /, IX(3) / 1 /
      DATA      CPARAS(4) / 'IYTYPE  ' /, IX(4) / 1 /

*     / LONG NAME /

      DATA      CPARAL(1) / 'LOG_X_LABEL_CHAR' /
      DATA      CPARAL(2) / 'LOG_Y_LABEL_CHAR' /
      DATA      CPARAL(3) / '****IXTYPE  ' /
      DATA      CPARAL(4) / '****IYTYPE  ' /

*     IXCHR /IYCHR  : CHARACTER NUMBER OF * FOR 5*10E1 IN X/Y-AXIS
*     IXTYPE/IXTYPE : 1-4  1 ... 10|2" 2*10|2" 5*10|2" 10|3"  ETC
*                          2 ... 10|2" 2       5       10|3"  ETC
*                          3 ... 100   200     500     1000   ETC
*                          4 ... 100   2       5       1000   ETC
*               FORMAT FOR 3 OR 4 DEPENDS ON THAT SET BY ULSFMT

      DATA      LFIRST / .TRUE. /


      NCP = NPARA

      RETURN
*-----------------------------------------------------------------------
      ENTRY ULIQID(CP, IDX)

      DO 10 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IDX = N
          RETURN
        END IF
   10 CONTINUE
      CMSG = 'PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E','ULIQID',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY ULIQCP(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAS(IDX)
      ELSE
        CALL MSGDMP('E','ULIQCP','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY ULIQCL(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAL(IDX)
      ELSE
        CALL MSGDMP('E','ULIQCL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY ULIQVL(IDX, IPARA)

      IF (LFIRST) THEN
        CALL RTIGET('UL', CPARAS, IX, NPARA)
        CALL RLIGET(CPARAL, IX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        IPARA = IX(IDX)
      ELSE
        CALL MSGDMP('E','ULIQVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY ULISVL(IDX, IPARA)

      IF (LFIRST) THEN
        CALL RTIGET('UL', CPARAS, IX, NPARA)
        CALL RLIGET(CPARAL, IX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        IX(IDX) = IPARA
      ELSE
        CALL MSGDMP('E','ULISVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY ULIQIN(CP, IN)

      DO 20 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IN = N
          RETURN
        END IF
   20 CONTINUE

      IN = 0

      RETURN
      END
