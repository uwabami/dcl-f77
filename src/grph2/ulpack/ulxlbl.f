*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE ULXLBL ( BL, NBL, INUM )

*     LABEL BUFFER FOR X-AXIS

      DIMENSION XBL(10,4),NXBL(4),BL(*)

      SAVE

      DATA XBL/1.,10.,0.,0.,0.,0.,0.,0.,0.,0.,
     &         1.,2.,10.,0.,0.,0.,0.,0.,0.,0.,
     &         1.,2.,5.,10.,0.,0.,0.,0.,0.,0.,
     &         1.,2.,3.,4.,5.,6.,7.,8.,9.,10./
      DATA NXBL/1,2,3,9/


      DO 10 IBL=1,NXBL(INUM)+1
        BL(IBL)=XBL(IBL,INUM)
   10 CONTINUE
      NBL=NXBL(INUM)

      RETURN
*-----------------------------------------------------------------------
      ENTRY ULSXBL ( BL, NBL )

      NXBL(4)=NBL
      DO 20 IBL=1,NBL
        XBL(IBL,4)=BL(IBL)
   20 CONTINUE
      XBL(NBL+1,4)=10.

      RETURN
*-----------------------------------------------------------------------
      ENTRY ULQXBL ( BL, NBL )

      NBL=NXBL(4)
      DO 30 IBL=1,NBL
        BL(IBL)=XBL(IBL,4)
   30 CONTINUE

      RETURN
      END
