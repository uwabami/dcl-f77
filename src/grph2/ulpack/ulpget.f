*-----------------------------------------------------------------------
*     ULPGET / ULPSET / ULPSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE ULPGET(CP,IPARA)

      CHARACTER CP*(*)

      CHARACTER CX*8, CL*40


      CALL ULPQID(CP, IDX)
      CALL ULPQVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY ULPSET(CP, IPARA)

      CALL ULPQID(CP, IDX)
      CALL ULPSVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY ULPSTX(CP, IPARA)

      IP = IPARA
      CALL ULPQID(CP, IDX)
      CALL ULPQIT(IDX, IT)
      CALL ULPQCP(IDX, CX)
      CALL ULPQCL(IDX, CL)
      IF (IT .EQ. 1) THEN
        CALL RTIGET('UL', CX, IP, 1)
        CALL RLIGET(CL, IP, 1)
        CALL ULIQID(CP, IDX)
        CALL ULISVL(IDX, IP)
      ELSE IF (IT .EQ. 2) THEN
        CALL RTLGET('UL', CX, IP, 1)
        CALL RLLGET(CL, IP, 1)
        CALL ULLQID(CP, IDX)
        CALL ULLSVL(IDX, IP)
      ELSE IF (IT .EQ. 3) THEN
        CALL RTRGET('UL', CX, IP, 1)
        CALL RLRGET(CL, IP, 1)
        CALL ULRQID(CP, IDX)
        CALL ULRSVL(IDX, IP)
      END IF

      RETURN
      END
