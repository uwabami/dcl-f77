*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UVBRLZ(N,UPX,UPY,ITYPE,INDEX,RSIZE)

      REAL      UPX(*),UPY(*)

      LOGICAL   LFLAG, LMISS, LXUNI
      CHARACTER COBJ*80

      COMMON    /SZBLS2/ LCLIP
      LOGICAL   LCLIP


      IF (N.LT.2) THEN
        CALL MSGDMP('E','UVBRLZ','NUMBER OF POINTS IS LESS THAN 2.')
      END IF
      IF (ITYPE.EQ.0) THEN
        CALL MSGDMP('M','UVBRLZ','LINE TYPE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.EQ.0) THEN
        CALL MSGDMP('M','UVBRLZ','LINE INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.LT.0) THEN
        CALL MSGDMP('E','UVBRLZ','LINE INDEX IS LESS THAN 0.')
      END IF
      IF (RSIZE.EQ.0) THEN
        CALL MSGDMP('M','UVBRLZ','BAR SIZE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (RSIZE.LT.0) THEN
        CALL MSGDMP('E','UVBRLZ','BAR SIZE IS LESS THAN ZERO.')
      END IF

      CALL SGLGET('LCLIP' , LCLIP )
      CALL GLRGET('RUNDEF', RUNDEF)
      CALL GLRGET('RMISS' , RMISS)
      CALL GLLGET('LMISS' , LMISS)

      IF (UPY(1).EQ.RUNDEF) THEN
        CALL MSGDMP('E', 'UVBRLZ', 'RUNDEF CAN NOT BE UESED FOR UPY.')
      END IF

      WRITE(COBJ,'(2I8)') ITYPE,INDEX
      CALL CDBLK(COBJ)
      CALL SWOOPN('UVBRLZ',COBJ)

      CALL SZSLTI(ITYPE,INDEX)
      CALL SZOPLU

      LXUNI = UPX(1).EQ.RUNDEF

      IF (LXUNI) THEN
        CALL UUQIDV(UXMIN, UXMAX)
        IF (UXMIN.EQ.RUNDEF) CALL SGRGET('UXMIN', UXMIN)
        IF (UXMAX.EQ.RUNDEF) CALL SGRGET('UXMAX', UXMAX)
        DX = (UXMAX-UXMIN)/(N-1)
      END IF

      CALL SZOPLV

      LFLAG=.FALSE.
      DO 20 I=1,N
        IF (LXUNI) THEN
          UXX = UXMIN + DX*(I-1)
        ELSE
          UXX = UPX(I)
        END IF

        IF ((UXX.EQ.RMISS .OR. UPY(I).EQ.RMISS) .AND. LMISS) THEN
          LFLAG=.FALSE.
        ELSE
          CALL STFTRF(UXX, UPY(I), VXX, VYY)
          IF (LFLAG) THEN
            CALL SZPLLV(VXX-RSIZE/2.,VYY)
            CALL SZPLLV(VXX+RSIZE/2.,VYY)
          ELSE
            CALL SZMVLV(VXX-RSIZE/2.,VYY)
            CALL SZPLLV(VXX+RSIZE/2.,VYY)
            LFLAG=.TRUE.
          END IF
        END IF
   20 CONTINUE

      CALL SZCLLV
      CALL SWOCLS('UVBRLZ')

      END
