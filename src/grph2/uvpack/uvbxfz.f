*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UVBXFZ(N,UPX,UPY1,UPY2,ITYPE,INDEX)

      REAL      UPX(*),UPY1(*),UPY2(*)

      LOGICAL   LMISS, LXUNI, LYC1, LYC2
      CHARACTER COBJ*80

      COMMON    /SZBLS2/ LCLIP
      LOGICAL   LCLIP


      IF (N.LT.1) THEN
        CALL MSGDMP('E','UVBXFZ','NUMBER OF POINTS IS LESS THAN 1.')
      END IF
      IF (ITYPE.EQ.0) THEN
        CALL MSGDMP('M','UVBXFZ','LINE TYPE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.EQ.0) THEN
        CALL MSGDMP('M','UVBXFZ','LINE INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.LT.0) THEN
        CALL MSGDMP('E','UVBXFZ','LINE INDEX IS LESS THAN 0.')
      END IF

      CALL SGLGET('LCLIP' , LCLIP )
      CALL GLRGET('RUNDEF', RUNDEF)
      CALL GLRGET('RMISS' , RMISS)
      CALL GLLGET('LMISS' , LMISS)

      WRITE(COBJ,'(2I8)') ITYPE, INDEX
      CALL CDBLK(COBJ)
      CALL SWOOPN('UVBXFZ',COBJ)

      CALL SZSLTI(ITYPE,INDEX)

      LXUNI = UPX(1).EQ.RUNDEF
      LYC1  = UPY1(1).EQ.RUNDEF
      LYC2  = UPY2(1).EQ.RUNDEF

      IF (LXUNI) THEN
        CALL UUQIDV(UXMIN, UXMAX)
        IF (UXMIN.EQ.RUNDEF) CALL SGRGET('UXMIN', UXMIN)
        IF (UXMAX.EQ.RUNDEF) CALL SGRGET('UXMAX', UXMAX)
        DX = (UXMAX-UXMIN)/N
      END IF

      IF (LYC1 .OR. LYC2) THEN
        CALL UURGET('UREF', UREF)
      END IF

      DO 20 I=1,N
        IF (LXUNI) THEN
          UX1 = UXMIN + DX*(I-1)
          UX2 = UXMIN + DX*I
        ELSE
          UX1 = UPX(I)
          UX2 = UPX(I+1)
        END IF

        IF (LYC1) THEN
          UY1 = UREF
        ELSE
          UY1 = UPY1(I)
        END IF

        IF (LYC2) THEN
          UY2 = UREF
        ELSE
          UY2 = UPY2(I)
        END IF

        IF (.NOT.
     #    ((UX1.EQ.RMISS .OR. UX1.EQ.RMISS .OR.
     #      UY1.EQ.RMISS .OR. UY2.EQ.RMISS) .AND. LMISS)) THEN

          CALL SZOPLU
          CALL SZMVLU(UX1, UY2)
          CALL SZPLLU(UX2, UY2)
          CALL SZPLLU(UX2, UY1)
          CALL SZPLLU(UX1, UY1)
          CALL SZPLLU(UX1, UY2)
          CALL SZCLLU

        END IF
   20 CONTINUE

      CALL SWOCLS('UVBXFZ')

      END
