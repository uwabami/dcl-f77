*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UVERB(N,UPX,UPY1,UPY2)

      REAL      UPX(*),UPY1(*),UPY2(*)


      CALL UUQEBT(ITYPE)
      CALL UUQEBI(INDEX)
      CALL UUQEBS(RSIZE)

      CALL UVERBZ(N,UPX,UPY1,UPY2,ITYPE,INDEX,RSIZE)

      END
