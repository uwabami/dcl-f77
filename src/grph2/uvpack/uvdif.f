*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UVDIF(N,UPX,UPY1,UPY2)

      REAL      UPX(*),UPY1(*),UPY2(*)


      CALL UUQARP(ITPAT1, ITPAT2)

      CALL UVDIFZ(N,UPX,UPY1,UPY2,ITPAT1,ITPAT2)

      END
