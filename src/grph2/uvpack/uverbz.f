*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UVERBZ(N,UPX,UPY1,UPY2,ITYPE,INDEX,RSIZE)

      REAL      UPX(*),UPY1(*),UPY2(*)

      LOGICAL   LMISS, LXUNI
      CHARACTER COBJ*80

      COMMON    /SZBLS2/ LCLIP
      COMMON    /SZBTX3/ LCLIPT
      LOGICAL   LCLIP, LCLIPT


      IF (N.LT.1) THEN
        CALL MSGDMP('E','UVERBZ','NUMBER OF POINTS IS LESS THAN 1.')
      END IF
      IF (ITYPE.EQ.0) THEN
        CALL MSGDMP('M','UVERBZ','LINE TYPE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.EQ.0) THEN
        CALL MSGDMP('M','UVERBZ','LINE INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.LT.0) THEN
        CALL MSGDMP('E','UVERBZ','LINE INDEX IS LESS THAN 0.')
      END IF
      IF (RSIZE.EQ.0) THEN
        CALL MSGDMP('M','UVERBZ','MARKER SIZE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (RSIZE.LT.0) THEN
        CALL MSGDMP('E','UVERBZ','MARKER SIZE IS LESS THAN ZERO.')
      END IF

      CALL SGLGET('LCLIP' , LCLIP )
      LCLIPT = LCLIP
      CALL GLRGET('RUNDEF', RUNDEF)
      CALL GLRGET('RMISS' , RMISS)
      CALL GLLGET('LMISS' , LMISS)

      IF (UPY1(1).EQ.RUNDEF .OR. UPY2(1).EQ.RUNDEF) THEN
        CALL MSGDMP('E', 'UVERBZ',
     #       'RUNDEF CAN NOT BE UESED FOR UPY1 OR UPY2')
      END IF

      WRITE(COBJ,'(2I8,F8.5)') ITYPE, INDEX, RSIZE
      CALL CDBLK(COBJ)
      CALL SWOOPN('UVERBZ',COBJ)

      CALL SZSLTI(ITYPE,INDEX)

      LXUNI = UPX(1).EQ.RUNDEF

      IF (LXUNI) THEN
        CALL UUQIDV(UXMIN, UXMAX)
        IF (UXMIN.EQ.RUNDEF) CALL SGRGET('UXMIN', UXMIN)
        IF (UXMAX.EQ.RUNDEF) CALL SGRGET('UXMAX', UXMAX)
        DX = (UXMAX-UXMIN)/(N-1)
      END IF

      DO 20 I=1,N
        IF (LXUNI) THEN
          UXX = UXMIN + DX*(I-1)
        ELSE
          UXX = UPX(I)
        END IF

        IF (.NOT.
     #     ((UXX.EQ.RMISS .OR. UPY1(I).EQ.RMISS .OR. UPY2(I).EQ.RMISS)
     #     .AND. LMISS)) THEN

          CALL STFTRF(UXX, UPY1(I), VXX, VY1)
          CALL STFTRF(UXX, UPY2(I), VXX, VY2)

          CALL SZOPLV
          CALL SZMVLV(VXX, VY1)
          CALL SZPLLV(VXX, VY2)
          CALL SZCLLV

          CALL SZOPSV
          CALL SZMVSV(VXX-RSIZE/2., VY2)
          CALL SZPLSV(VXX+RSIZE/2., VY2)
          CALL SZMVSV(VXX-RSIZE/2., VY1)
          CALL SZPLSV(VXX+RSIZE/2., VY1)
          CALL SZCLSV
        END IF
   20 CONTINUE

      CALL SWOCLS('UVERBZ')

      END
