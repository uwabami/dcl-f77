*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UVBRL(N,UPX,UPY)

      REAL      UPX(*),UPY(*)


      CALL UUQLNT(ITYPE)
      CALL UUQLNI(INDEX)
      CALL UUQBRS(RSIZE)

      CALL UVBRLZ(N,UPX,UPY,ITYPE,INDEX,RSIZE)

      END
