*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UVBRFZ(N,UPX,UPY1,UPY2,ITYPE,INDEX,RSIZE)

      REAL      UPX(*),UPY1(*),UPY2(*)

      LOGICAL   LMISS, LXUNI, LYC1, LYC2
      CHARACTER COBJ*80

      COMMON    /SZBLS2/ LCLIP
      LOGICAL   LCLIP


      IF (N.LT.1) THEN
        CALL MSGDMP('E','UVBRFZ','NUMBER OF POINTS IS LESS THAN 1.')
      END IF
      IF (ITYPE.EQ.0) THEN
        CALL MSGDMP('M','UVBRFZ','LINE TYPE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.EQ.0) THEN
        CALL MSGDMP('M','UVBRFZ','LINE INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.LT.0) THEN
        CALL MSGDMP('E','UVBRFZ','LINE INDEX IS LESS THAN 0.')
      END IF
      IF (RSIZE.EQ.0) THEN
        CALL MSGDMP('M','UVBRFZ','MARKER SIZE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (RSIZE.LT.0) THEN
        CALL MSGDMP('E','UVBRFZ','MARKER SIZE IS LESS THAN ZERO.')
      END IF

      CALL SGLGET('LCLIP' , LCLIP )
      CALL GLRGET('RUNDEF', RUNDEF)
      CALL GLRGET('RMISS' , RMISS )
      CALL GLLGET('LMISS' , LMISS )

      WRITE(COBJ,'(2I8,F8.5)') ITYPE, INDEX, RSIZE
      CALL CDBLK(COBJ)
      CALL SWOOPN('UVBRFZ',COBJ)

      CALL SZSLTI(ITYPE,INDEX)

      LXUNI = UPX(1).EQ.RUNDEF
      LYC1  = UPY1(1).EQ.RUNDEF
      LYC2  = UPY2(1).EQ.RUNDEF

      IF (LXUNI) THEN
        CALL UUQIDV(UXMIN, UXMAX)
        IF (UXMIN.EQ.RUNDEF) CALL SGRGET('UXMIN', UXMIN)
        IF (UXMAX.EQ.RUNDEF) CALL SGRGET('UXMAX', UXMAX)
        DX = (UXMAX-UXMIN)/(N-1)
      END IF

      IF (LYC1 .OR. LYC2) THEN
        CALL UURGET('UREF', UREF)
      END IF

      DO 20 I=1,N
        IF (LXUNI) THEN
          UXX = UXMIN + DX*(I-1)
        ELSE
          UXX = UPX(I)
        END IF

        IF (LYC1) THEN
          UYY1 = UREF
        ELSE
          UYY1 = UPY1(I)
        END IF

        IF (LYC2) THEN
          UYY2 = UREF
        ELSE
          UYY2 = UPY2(I)
        END IF

        IF (.NOT.
     #    ((UXX.EQ.RMISS .OR. UYY1.EQ.RMISS .OR. UYY2.EQ.RMISS)
     #     .AND. LMISS)) THEN

          CALL STFTRF(UXX, UYY1, VXX, VY1)
          CALL STFTRF(UXX, UYY2, VXX, VY2)

          CALL SZOPLV
          CALL SZMVLV(VXX-RSIZE/2., VY2)
          CALL SZPLLV(VXX+RSIZE/2., VY2)
          CALL SZPLLV(VXX+RSIZE/2., VY1)
          CALL SZPLLV(VXX-RSIZE/2., VY1)
          CALL SZPLLV(VXX-RSIZE/2., VY2)
          CALL SZCLLV
        END IF
   20 CONTINUE

      CALL SWOCLS('UVBRFZ')

      END
