*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UVBXF(N,UPX,UPY1,UPY2)

      REAL      UPX(*),UPY1(*),UPY2(*)


      CALL UUQFRT(ITYPE)
      CALL UUQFRI(INDEX)

      CALL UVBXFZ(N,UPX,UPY1,UPY2,ITYPE,INDEX)

      END
