*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UVBRAZ(N,UPX,UPY1,UPY2,ITPAT1,ITPAT2,RSIZE)

      REAL      UPX(*),UPY1(*),UPY2(*)

      LOGICAL   LMISS, LXUNI, LYC1, LYC2
      CHARACTER COBJ*80

      COMMON    /SZBTN2/ IRMODE, IRMODR
      COMMON    /SZBTN3/ LCLIP
      LOGICAL   LCLIP


      IF (N.LT.1) THEN
        CALL MSGDMP('E','UVBRAZ','NUMBER OF POINTS IS LESS THAN 1.')
      END IF
      IF (ITPAT1.EQ.0 .OR. ITPAT2.EQ.0) THEN
        CALL MSGDMP('M','UVBRAZ','TONE PAT. INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (ITPAT1.LT.0 .OR. ITPAT2.LT.0) THEN
        CALL MSGDMP('E','UVBRAZ','TONE PAT. INDEX IS LESS THAN 0.')
      END IF
      IF (RSIZE.EQ.0) THEN
        CALL MSGDMP('M','UVBRAZ','BAR SIZE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (RSIZE.LT.0) THEN
        CALL MSGDMP('E','UVBRAZ','BAR SIZE IS LESS THAN ZERO.')
      END IF

      CALL SGLGET('LCLIP' , LCLIP)
      CALL GLRGET('RUNDEF', RUNDEF)
      CALL GLRGET('RMISS' , RMISS)
      CALL GLLGET('LMISS' , LMISS)

      CALL STFPR2(0., 0., RX0, RY0)
      CALL STFPR2(0., 1., RX1, RY1)
      CALL STFPR2(1., 0., RX2, RY2)

      ROT = (RX2-RX0)*(RY1-RY0) - (RY2-RY0)*(RX1-RX0)

      IRMODE = 0
      IF (ROT.GT.0) THEN
        IRMODR = IRMODE
      ELSE
        IRMODR = MOD(IRMODE+1, 2)
      END IF

      WRITE(COBJ,'(2I8,F8.5)') ITPAT1, ITPAT2, RSIZE
      CALL CDBLK(COBJ)
      CALL SWOOPN('UVBRAZ',COBJ)

      LXUNI = UPX(1).EQ.RUNDEF
      LYC1  = UPY1(1).EQ.RUNDEF
      LYC2  = UPY2(1).EQ.RUNDEF

      IF (LXUNI) THEN
        CALL UUQIDV(UXMIN, UXMAX)
        IF (UXMIN.EQ.RUNDEF) CALL SGRGET('UXMIN', UXMIN)
        IF (UXMAX.EQ.RUNDEF) CALL SGRGET('UXMAX', UXMAX)
        DX = (UXMAX-UXMIN)/(N-1)
      END IF

      IF (LYC1 .OR. LYC2) THEN
        CALL UURGET('UREF', UREF)
      END IF

      DO 20 I=1,N
        IF (LXUNI) THEN
          UXX = UXMIN + DX*(I-1)
        ELSE
          UXX = UPX(I)
        END IF

        IF (LYC1) THEN
          UYY1 = UREF
        ELSE
          UYY1 = UPY1(I)
        END IF

        IF (LYC2) THEN
          UYY2 = UREF
        ELSE
          UYY2 = UPY2(I)
        END IF

        IF (.NOT.
     #    ((UXX.EQ.RMISS .OR. UYY1.EQ.RMISS .OR. UYY2.EQ.RMISS)
     #     .AND. LMISS)) THEN

          CALL STFTRF(UXX, UYY1, VXX, VY1)
          CALL STFTRF(UXX, UYY2, VXX, VY2)

          IF (UYY2 .GT. UYY1) THEN
            CALL SZSTNI(ITPAT1)
          ELSE
            CALL SZSTNI(ITPAT2)
          END IF

          IF (VY1 .GT. VY2) THEN
            VYY = VY1
            VY1 = VY2
            VY2 = VYY
          END IF

          CALL SZOPTV
          CALL SZSTTV(VXX-RSIZE/2., VY1)
          CALL SZSTTV(VXX+RSIZE/2., VY1)
          CALL SZSTTV(VXX+RSIZE/2., VY2)
          CALL SZSTTV(VXX-RSIZE/2., VY2)
          CALL SZSTTV(VXX-RSIZE/2., VY1)
          CALL SZCLTV
        END IF
   20 CONTINUE

      CALL SWOCLS('UVBRAZ')

      END
