*-----------------------------------------------------------------------
*     UWQGXI / UWIGXI
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UWQGXI(UX,IUX,FRAC)

      LOGICAL   LMAP, LDEG, LASCND

      EXTERNAL  RFPI, RMOD

      COMMON    /UWBLKX/ LEQDXZ,NXZ,UXMINZ,UXMAXZ,DXZ,XPZ
      PARAMETER (NW=@MAXNGRID)
      LOGICAL   LEQDXZ
      REAL      XPZ(NW)

      SAVE

      DATA      IX /1/


      IF (LMAP) THEN
        UXX = RMOD(UX-UXMINA, UMOD) + UXMINA
      ELSE
        UXX = UX
      END IF

      IF (.NOT.(UXMINA.LE.UXX .AND. UXX.LE.UXMAXA)) THEN
        IUX = IUNDEF
        FRAC = 0
        RETURN
      END IF

      IF (LEQDXZ) THEN
        XNORM = (UXX-UXMINZ)/DXZ
        IUX = MIN(INT(XNORM)+1, NXZ-1)
        FRAC = XNORM-IUX+1
      ELSE
        IF (LASCND) THEN
          IF (UXX.GT.XPZ(IX)) THEN
            DO 10 I=IX, NXZ-2
              IF (UXX.LE.XPZ(I+1)) GO TO 30
   10       CONTINUE
          ELSE
            DO 20 I=IX, 2, -1
              IF (UXX.GT.XPZ(I)) GO TO 30
   20       CONTINUE
          END IF
   30     CONTINUE
        ELSE
          IF (UXX.GT.XPZ(IX)) THEN
            DO 110 I=IX-1, 1, -1
              IF (UXX.LE.XPZ(I)) GO TO 130
  110       CONTINUE
          ELSE
            DO 120 I=IX, NXZ-2
              IF (UXX.GT.XPZ(I+1)) GO TO 130
  120       CONTINUE
          END IF
  130     CONTINUE
        END IF
        IX = I
        IUX = I
        FRAC = (UXX-XPZ(I))/(XPZ(I+1)-XPZ(I))
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UWIGXI

      IX = 1

      CALL GLIGET('IUNDEF', IUNDEF)
      CALL SGLGET('LDEG', LDEG)
      CALL STQTRF(LMAP)

      IF (LDEG) THEN
        UMOD = 360
      ELSE
        UMOD = RFPI()*2
      END IF

      IF (.NOT.LEQDXZ) THEN
        LASCND = XPZ(NXZ).GT.XPZ(1)
      END IF

      UXMAXA = MAX(UXMINZ, UXMAXZ)
      UXMINA = MIN(UXMINZ, UXMAXZ)

      RETURN
      END
