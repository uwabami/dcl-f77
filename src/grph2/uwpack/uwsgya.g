*-----------------------------------------------------------------------
*     UWSGYA / UWSGYB
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UWSGYA(YP,NY)

      REAL      YP(*)
      LOGICAL   LSETY

      LOGICAL   LSETYZ

      COMMON    /UWBLKY/ LEQDYZ,NYZ,UYMINZ,UYMAXZ,DYZ,YPZ
      PARAMETER (NW=@MAXNGRID)
      LOGICAL   LEQDYZ
      REAL      YPZ(NW)

      SAVE

      DATA      LSETYZ/.FALSE./


      IF (NY.LT.2) THEN
        CALL MSGDMP('E','UWSGYA','NUMBER OF POINTS IS INVALID.')
      END IF
      IF (NY.GT.NW) THEN
        CALL MSGDMP('E','UWSGYA','WORKING AREA IS NOT ENOUGH.')
      END IF

      LEQDYZ=.FALSE.
      NYZ=NY
      CALL VRSET(YP,YPZ,NY,1,1)
      UYMINZ=YP(1)
      UYMAXZ=YP(NY)
      LSETYZ=.TRUE.

      CALL UWIGYI

      RETURN
*-----------------------------------------------------------------------
      ENTRY UWQGYA(YP,NY)

      NY=NYZ
      CALL VRSET(YPZ,YP,NY,1,1)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UWSGYB(UYMIN,UYMAX,NY)

      IF (UYMIN.EQ.UYMAX) THEN
        CALL MSGDMP('E','UWSGYB','UYMIN = UYMAX.')
      END IF

      LEQDYZ=.TRUE.
      NYZ=NY
      UYMINZ=UYMIN
      UYMAXZ=UYMAX
      DYZ=(UYMAX-UYMIN)/(NY-1)
      LSETYZ=.TRUE.

      CALL UWIGYI

      RETURN
*-----------------------------------------------------------------------
      ENTRY UWQGYB(UYMIN,UYMAX,NY)

      NY=NYZ
      UYMIN=UYMINZ
      UYMAX=UYMAXZ

      RETURN
*-----------------------------------------------------------------------
      ENTRY UWSGYZ(LSETY)

      LSETYZ=LSETY

      RETURN
*-----------------------------------------------------------------------
      ENTRY UWQGYZ(LSETY)

      LSETY=LSETYZ

      RETURN
      END
