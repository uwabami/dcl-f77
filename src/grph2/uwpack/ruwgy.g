*-----------------------------------------------------------------------
*     RUWGY
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      REAL FUNCTION RUWGY(IY)

      COMMON    /UWBLKY/ LEQDYZ,NYZ,UYMINZ,UYMAXZ,DYZ,YPZ
      PARAMETER (NW=@MAXNGRID)
      LOGICAL   LEQDYZ
      REAL      YPZ(NW)


*     IF (.NOT.(1.LE.IY .AND. IY.LE.NYZ)) THEN
*       CALL MSGDMP('E','RUWGY','IY IS OUT OF RANGE.')
*     END IF

      IF (LEQDYZ) THEN
        RUWGY = UYMINZ+DYZ*(IY-1)
      ELSE
        RUWGY = YPZ(IY)
      END IF

      END
