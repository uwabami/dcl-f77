*-----------------------------------------------------------------------
*     RUWGX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      REAL FUNCTION RUWGX(IX)

      COMMON    /UWBLKX/ LEQDXZ,NXZ,UXMINZ,UXMAXZ,DXZ,XPZ
      PARAMETER (NW=@MAXNGRID)
      LOGICAL   LEQDXZ
      REAL      XPZ(NW)


*     IF (.NOT.(1.LE.IX .AND. IX.LE.NXZ)) THEN
*       CALL MSGDMP('E','RUWGX','IX IS OUT OF RANGE.')
*     END IF

      IF (LEQDXZ) THEN
        RUWGX = UXMINZ+DXZ*(IX-1)
      ELSE
        RUWGX = XPZ(IX)
      END IF

      END
