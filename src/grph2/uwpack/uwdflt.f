*-----------------------------------------------------------------------
*     UWDFLT
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UWDFLT(NX,NY)

      LOGICAL   LSET


*     / SET GRID ATTRIBUTE IF IT HAS NOT BEEN SET YET /

      CALL SGQWND(UXMN,UXMX,UYMN,UYMX)

      CALL UWQGXZ(LSET)
      IF (.NOT.LSET) THEN
        CALL UWSGXB(UXMN,UXMX,NX)
        CALL UWSGXZ(.FALSE.)
      END IF

      CALL UWQGYZ(LSET)
      IF (.NOT.LSET) THEN
        CALL UWSGYB(UYMN,UYMX,NY)
        CALL UWSGYZ(.FALSE.)
      END IF

      END
