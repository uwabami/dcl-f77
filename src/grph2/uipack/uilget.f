*-----------------------------------------------------------------------
*     UILGET / UILSET / UILSTX
*-----------------------------------------------------------------------
      SUBROUTINE UILGET(CP, LPARA)

      LOGICAL   LPARA
      CHARACTER CP*(*)

      LOGICAL   LP
      CHARACTER CX*8
      CHARACTER CL*40


      CALL UILQID(CP, IDX)
      CALL UILQVL(IDX, LPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UILSET(CP, LPARA)

      CALL UILQID(CP, IDX)
      CALL UILSVL(IDX, LPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UILSTX(CP, LPARA)

      LP = LPARA
      CALL UILQID(CP, IDX)

*     / SHORT NAME /

      CALL UILQCP(IDX, CX)
      CALL RTLGET('UE', CX, LP, 1)

*     / LONG NAME /

      CALL UILQCL(IDX, CL)
      CALL RLLGET(CL, LP, 1)

      CALL UILSVL(IDX,LP)

      RETURN
      END
