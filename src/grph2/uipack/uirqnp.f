*-----------------------------------------------------------------------
*     UIRQNP / UIRQID / UIRQCP / UIRQVL / UIRSVL
*-----------------------------------------------------------------------
      SUBROUTINE UIRQNP(NCP)

      CHARACTER CP*(*)

      PARAMETER (NPARA = 5)

      REAL      RX(NPARA)
      LOGICAL   LCHREQ, LFIRST
      CHARACTER CPARAS(NPARA)*9
      CHARACTER CPARAL(NPARA)*40
      CHARACTER CMSG*80

      EXTERNAL  LCHREQ,LENC

      SAVE

*     / SHORT NAME /

      DATA      CPARAS(1) / 'HEIGHT   ' /, RX(1) / 1.0  /
      DATA      CPARAS(2) / 'DENSITY  ' /, RX(2) / 0.8  /
      DATA      CPARAS(3) / 'DIRECTION' /, RX(3) / 45.0 /
      DATA      CPARAS(4) / 'XLON     ' /, RX(4) / 90.0 /
      DATA      CPARAS(5) / 'YLAT     ' /, RX(5) / 45.0 /

*     / LONG NAME /

      DATA      CPARAL(1) / 'EMBOSSMENT_HEIGHT ' /
      DATA      CPARAL(2) / 'EMBOSSMENT_DENSITY' /
      DATA      CPARAL(3) / 'LIGHT_DIRECTION' /
      DATA      CPARAL(4) / 'LIGHT_LONGITUDE' /
      DATA      CPARAL(5) / 'LIGHT_LATITUDE ' /

      DATA      LFIRST / .TRUE. /


      NCP = NPARA

      RETURN
*-----------------------------------------------------------------------
      ENTRY UIRQID(CP, IDX)

      DO 10 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IDX = N
          RETURN
        END IF
   10 CONTINUE
      CMSG = 'PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E','UIRQID',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UIRQCP(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAS(IDX)
      ELSE
        CALL MSGDMP('E','UIRQCP','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UIRQCL(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAL(IDX)
      ELSE
        CALL MSGDMP('E','UIRQCL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UIRQVL(IDX, RPARA)

      IF (LFIRST) THEN
        CALL RTRGET('UI', CPARAS, RX, NPARA)
        CALL RLRGET(CPARAL, RX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        RPARA = RX(IDX)
      ELSE
        CALL MSGDMP('E','UIRQVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UIRSVL(IDX, RPARA)

      IF (LFIRST) THEN
        CALL RTRGET('UI', CPARAS, RX, NPARA)
        CALL RLRGET(CPARAL, RX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        RX(IDX) = RPARA
      ELSE
        CALL MSGDMP('E','UIRSVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UIRQIN(CP, IN)

      DO 20 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IN = N
          RETURN
        END IF
   20 CONTINUE

      IN = 0

      RETURN
      END
