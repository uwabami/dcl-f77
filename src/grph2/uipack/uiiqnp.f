*-----------------------------------------------------------------------
*     UIIQNP / UIIQID / UIIQCP / UIIQVL / UIISVL
*-----------------------------------------------------------------------
      SUBROUTINE UIIQNP(NCP)

      CHARACTER CP*(*)

      PARAMETER (NPARA = 3)

      INTEGER   IX(NPARA)
      LOGICAL   LCHREQ, LFIRST
      CHARACTER CPARAS(NPARA)*8
      CHARACTER CPARAL(NPARA)*40
      CHARACTER CMSG*80

      EXTERNAL  LCHREQ, LENC

      SAVE

*     / SHORT NAME /

      DATA      CPARAS(1) / 'ICOLOR1 ' /, IX(1) / Z'FFFFFF' /
      DATA      CPARAS(2) / 'ICOLOR2 ' /, IX(2) / Z'FFFFFF' /
      DATA      CPARAS(3) / 'ICOLOR3 ' /, IX(3) / Z'FFFFFF' /

*     / LONG NAME /

      DATA      CPARAL(1) / 'COLOR_FOR_OUT_OF_DOMAIN' /
      DATA      CPARAL(2) / 'COLOR_FOR_OUT_OF_GRID' /
      DATA      CPARAL(3) / 'COLOR_FOR_MISSING_VALUE' /

      DATA      LFIRST / .TRUE. /


      NCP = NPARA

      RETURN
*-----------------------------------------------------------------------
      ENTRY UIIQID(CP, IDX)

      DO 10 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IDX = N
          RETURN
        END IF
   10 CONTINUE
      CMSG = 'PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E','UIIQID',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UIIQCP(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAS(IDX)
      ELSE
        CALL MSGDMP('E','UIIQCP','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UIIQCL(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAL(IDX)
      ELSE
        CALL MSGDMP('E','UIIQCL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UIIQVL(IDX, IPARA)

      IF (LFIRST) THEN
        CALL RTIGET('UI', CPARAS, IX, NPARA)
        CALL RLIGET(CPARAL, IX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        IPARA = IX(IDX)
      ELSE
        CALL MSGDMP('E','UIIQVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UIISVL(IDX, IPARA)

      IF (LFIRST) THEN
        CALL RTIGET('UI', CPARAS, IX, NPARA)
        CALL RLIGET(CPARAL, IX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        IX(IDX) = IPARA
      ELSE
        CALL MSGDMP('E','UIISVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UIIQIN(CP, IN)

      DO 20 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IN = N
          RETURN
        END IF
   20 CONTINUE

      IN = 0

      RETURN
      END
