      SUBROUTINE UIC3D(U, V, W,IRGB)

      LOGICAL LSET

      DATA IR1, IR2 / Z'00', Z'FF'/
      DATA IG1, IG2 / Z'00', Z'FF'/
      DATA IB1, IB2 / Z'00', Z'FF'/

      DATA LSET /.FALSE./

      SAVE

      JR = IR1 - U * (IR1 -IR2)
      JG = IG1 - V * (IG1 -IG2)
      JB = IB1 - W * (IB1 -IB2)

      CALL UIFPAC(JR, JG, JB, IRGB)


      RETURN

*-----------------------------------------------------------------------
      ENTRY UI3INI(UMIN, UMAX, VMIN, VMAX, WMIN, WMAX)

      IF(.NOT.LSET) THEN
        U1 = UMIN
        U2 = UMAX
        V1 = VMIN
        V2 = VMAX
        W1 = WMIN
        W2 = WMAX
      END IF

      DU = U2-U1
      DV = V2-V1
      DW = W2-W1

      RETURN

      END
