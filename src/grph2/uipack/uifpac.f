*-----------------------------------------------------------------------
      SUBROUTINE UIFPAC(IR, IG, IB, IRGB)

      IRGB = IOR(ISHFT(IR,16), IOR(ISHFT(IG,8), IB))

      RETURN
*-----------------------------------------------------------------------
      ENTRY UIIPAC(IRGB, IR, IG, IB)

      IR  = IAND(ISHFT(IRGB, -16), 255)
      IG  = IAND(ISHFT(IRGB,  -8), 255)
      IB  = IAND(      IRGB,       255)

      END
