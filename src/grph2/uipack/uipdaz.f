*-----------------------------------------------------------------------
      SUBROUTINE UIPDAZ(Z, MX, NX, NY, IMAGE,MAXPXL)

      PARAMETER (PI = 3.1415, CPI =PI/180.)
      REAL      Z(MX,*)
      INTEGER   IMAGE(MAXPXL)
      INTEGER   NS(2), NP(2), NQ(2)
      LOGICAL   LMISS,LIMC,LFCC,LCX,LCY,LCNR,LSHDW,LDEG,LMAP,LSPHE
      DATA      NP /1,1/

*     / CHECK IMAGE CAPABILITY /

      CALL SWQIMC(LIMC)
      IF (.NOT.LIMC) THEN
        CALL MSGDMP('E','UIPDAT','NO IMAGE CAPABILITY.')
      END IF

      CALL SWQFCC(LFCC)
      IF (.NOT.LFCC) THEN
        CALL MSGDMP('E','UIPDAT','NO FULL COLOR CAPABILITY.')
      END IF

*     / GET INTERNAL PARAMETERS /

      CALL GLRGET('RUNDEF  ', RUNDEF)
      CALL GLIGET('IUNDEF  ', IUNDEF)
      CALL GLLGET('LMISS   ', LMISS )
      CALL GLRGET('RMISS   ', RMISS )

      CALL UIIGET('ICOLOR1 ', ICOLOR1)
      CALL UIIGET('ICOLOR2 ', ICOLOR2)
      CALL UIIGET('ICOLOR3 ', ICOLOR3)
      CALL UILGET('LCELLX  ', LCX)
      CALL UILGET('LCELLY  ', LCY)
      CALL UILGET('LCORNER ', LCNR)
      CALL UILGET('LEMBOSS ', LSHDW)
      CALL UILGET('LSPHERE ', LSPHE)
      CALL UIRGET('DENSITY ', DENSTY)

*     / SET GRID ATTRIBUTE IF IT HAS NOT BEEN SET YET /

      NGX = NX
      NGY = NY
      IF(LCX) NGX = NGX + 1
      IF(LCY) NGY = NGY + 1
      CALL UWDFLT(NGX, NGY)

*     / INITIALIZE /


      CALL SGQVPT(VXMIN, VXMAX, VYMIN, VYMAX)

      CALL STFPR2(VXMIN, VYMIN, RX, RY)
      CALL STFWTR(RX, RY, WX1, WY1)
      CALL SWFINT(WX1, WY1, IX1, IY1)

      CALL STFPR2(VXMAX, VYMIN, RX, RY)
      CALL STFWTR(RX, RY, WX2, WY2)
      CALL SWFINT(WX2, WY2, IX2, IY2)

      CALL STFPR2(VXMAX, VYMAX, RX, RY)
      CALL STFWTR(RX, RY, WX3, WY3)
      CALL SWFINT(WX3, WY3, IX3, IY3)

      CALL STFPR2(VXMIN, VYMAX, RX, RY)
      CALL STFWTR(RX, RY, WX4, WY4)
      CALL SWFINT(WX4, WY4, IX4, IY4)

      IXMIN  = MIN(IX1, IX2, IX3, IX4)
      IYMIN  = MIN(IY1, IY2, IY3, IY4)
      IXMAX  = MAX(IX1, IX2, IX3, IX4)
      IYMAX  = MAX(IY1, IY2, IY3, IY4)
      IWIDTH = IXMAX-IXMIN+1
      IHIGHT = IYMAX-IYMIN+1
      
      CALL SWIOPN(IXMIN, IYMIN, IWIDTH, IHIGHT, 
     +            WX1, WY1, WX2, WY2, WX3, WY3, WX4, WY4)

      NS(1) = MX
      NS(2) = NY
      NQ(1) = NX
      NQ(2) = NY

      ZMIN = RVMIN(Z, NS, NP, NQ, 2)
      ZMAX = RVMAX(Z, NS, NP, NQ, 2)
      CALL UICINI(ZMIN, ZMAX)

      IF(LSHDW) THEN
        CALL SGIGET('ITR ', ITR )
        CALL SGLGET('LDEG', LDEG)
        LMAP = 10.LE.ITR .AND. ITR.LE.39

        AVE = 0.
        NN = 0.
        NXINT = MAX(1, NX/500)
        NYINT = MAX(1, NY/500)
        DO 11 IX=1, NX-1, NXINT
          DO 10 IY=1, NY-1, NYINT
            IF (.NOT. LMISS .OR. 
     +         (Z(IX,IY  ).NE.RMISS .AND. Z(IX+1,IY  ).NE.RMISS .AND.
     +          Z(IX,IY+1).NE.RMISS .AND. Z(IX+1,IY+1).NE.RMISS)) THEN
              Y1 = RUWGY(IY  )
              Y2 = RUWGY(IY+1)
              DX = (RUWGX(IX+1) - RUWGX(IX))
              DY = (Y2 - Y1)
              IF(LMAP)THEN
                YY = (Y1+Y2)/2.
                IF(LDEG) YY = CPI*YY
                DX = DX*COS(YY)
              END IF
              ZX = (Z(IX+1, IY) - Z(IX, IY)) / DX
              ZY = (Z(IX, IY+1) - Z(IX, IY)) / DY
              AVE = AVE + ABS(ZX) + ABS(ZY)
              NN  = NN +2
            END IF
 10       CONTINUE
 11     CONTINUE
        CALL UIRGET('EMBOSSMENT_HEIGHT', HEIGHT)
        AVE = AVE/NN*2/HEIGHT

        IF(LSPHE) THEN
          CALL UIRGET('LIGHT_LONGITUDE', PHI)
          CALL UIRGET('LIGHT_LATITUDE ', THE)
          EX0 = COS(THE*CPI)*COS(PHI*CPI)
          EY0 = COS(THE*CPI)*SIN(PHI*CPI)
          EZ0 = SIN(THE*CPI)
        ELSE
          CALL UIRGET('LIGHT_DIRECTION', THETA)
          EX =  SIN(THETA*CPI)
          EY = -COS(THETA*CPI)
          EZ = 0.
        END IF
      END IF

*     / LOOP FOR EACH PIXEL /

      DO 30 J=1, IHIGHT
        DO 20 I=1, IWIDTH

          CALL SWIINT(I+IXMIN-1, J+IYMIN-1, WX, WY)
          CALL STIWTR(WX, WY, RX, RY)
          CALL STIPR2(RX, RY, VX, VY)
          CALL STITRF(VX, VY, UX, UY)

          IF (UX.EQ.RUNDEF) THEN
            IMAGE(I) = ICOLOR1
          ELSE
            CALL UWQGXI(UX,IX,FX)
            CALL UWQGYI(UY,IY,FY)

            IF(LSHDW) THEN
              Y1 = RUWGY(IY  )
              Y2 = RUWGY(IY+1)
              X1 = RUWGX(IX  )
              X2 = RUWGX(IX+1)
              DX = (X2 - X1)*AVE
              DY = (Y2 - Y1)*AVE
              IF(LMAP)THEN
                XX = (X1+X2)/2.
                YY = (Y1+Y2)/2.
                IF(LDEG) THEN
                  XX = 3.141592/180.*XX
                  YY = 3.141592/180.*YY
                END IF
                DX = DX*COS(YY)
                IF(LSPHE) THEN
                  CALL CR3C(-YY+PI/2.,+XX,-PI/2.,EX0,EY0,EZ0,EX,EY,EZ)
                END IF
              END IF
            END IF

            IF (IX.EQ.IUNDEF .OR. IY.EQ.IUNDEF) THEN
              IMAGE(I) = ICOLOR2
            ELSE
              IF(LCX .AND. LCY) THEN
                IF (LMISS .AND. Z(IX, IY).EQ.RMISS) THEN
                  IMAGE(I) = ICOLOR3
                ELSE
                  ZZ =  Z(IX, IY  )
                  CALL UICRGB(ZZ, 0., IMAGE(I))
                END IF

              ELSE IF(LCX .AND. .NOT.LCY) THEN
                IF (LMISS .AND. 
     +             (Z(IX,IY).EQ.RMISS .OR. Z(IX,IY+1).EQ.RMISS)) THEN
                  IMAGE(I) = ICOLOR3
                ELSE
                  ZZ = Z(IX, IY  )*(1-FY) + Z(IX  ,IY+1)*FY
                  CALL UICRGB(ZZ, 0., IMAGE(I))
                END IF

              ELSE IF(.NOT.LCX .AND. LCY) THEN
                IF (LMISS .AND. 
     +             (Z(IX,IY).EQ.RMISS .OR. Z(IX+1,IY).EQ.RMISS)) THEN
                  IMAGE(I) = ICOLOR3
                ELSE
                  ZZ = Z(IX, IY  )*(1-FX) + Z(IX+1,IY  )*FX
                  CALL UICRGB(ZZ, 0., IMAGE(I))
                END IF

              ELSE IF(.NOT.LCX .AND. .NOT.LCY) THEN
                IF (.NOT. LMISS .OR. 
     +           (Z(IX,IY  ).NE.RMISS .AND. Z(IX+1,IY  ).NE.RMISS .AND.
     +            Z(IX,IY+1).NE.RMISS .AND. Z(IX+1,IY+1).NE.RMISS)) THEN
                  ZZ = (Z(IX, IY  )*(1-FX) + Z(IX+1,IY  )*FX)*(1-FY)
     +               + (Z(IX, IY+1)*(1-FX) + Z(IX+1,IY+1)*FX)*FY
                  
                  IF(LSHDW) THEN
                    ZX = ((Z(IX+1, IY  ) - Z(IX,   IY  ))*(1.-FY)
     +                 +  (Z(IX+1, IY+1) - Z(IX,   IY+1))*FY)/DX
                    ZY = ((Z(IX,   IY+1) - Z(IX,   IY  ))*(1.-FX)
     +                 +  (Z(IX+1, IY+1) - Z(IX+1, IY  ))*FX)/DY
                    CALL UISHDW(ZX, ZY, EX, EY, EZ, CMSK)
                  ELSE
                    CMSK = 0.
                  END IF
                  CALL UICRGB(ZZ, CMSK*DENSTY, IMAGE(I))

                ELSE IF(LCNR .AND. (FX+FY).LE.1 .AND.
     +           (Z(IX,IY  ).NE.RMISS .AND. Z(IX+1,IY  ).NE.RMISS .AND.
     +            Z(IX,IY+1).NE.RMISS .AND. Z(IX+1,IY+1).EQ.RMISS)) THEN
                  ZZ = Z(IX,  IY  )*(1-FX-FY) 
     +               + Z(IX+1,IY )*FX + Z(IX, IY+1)*FY

                  IF(LSHDW) THEN
                    ZX = (Z(IX+1, IY  ) - Z(IX,   IY  ))/DX
                    ZY = (Z(IX,   IY+1) - Z(IX,   IY  ))/DY
                    CALL UISHDW(ZX, ZY, EX, EY, EZ, CMSK)
                  ELSE
                    CMSK = 0.
                  END IF
                  CALL UICRGB(ZZ, CMSK*DENSTY, IMAGE(I))

                ELSE IF(LCNR .AND. (1-FX+FY).LE.1 .AND.
     +           (Z(IX,IY  ).NE.RMISS .AND. Z(IX+1,IY  ).NE.RMISS .AND.
     +            Z(IX,IY+1).EQ.RMISS .AND. Z(IX+1,IY+1).NE.RMISS)) THEN
                  ZZ = Z(IX+1,IY  )*(FX-FY) 
     +               + Z(IX  ,IY )*(1-FX) + Z(IX+1, IY+1)*FY

                  IF(LSHDW) THEN
                    ZX = (Z(IX+1, IY  ) - Z(IX,   IY  ))/DX
                    ZY = (Z(IX+1, IY+1) - Z(IX+1, IY  ))/DY
                    CALL UISHDW(ZX, ZY, EX, EY, EZ, CMSK)
                  ELSE
                    CMSK = 0.
                  END IF
                  CALL UICRGB(ZZ, CMSK*DENSTY, IMAGE(I))

                ELSE IF(LCNR .AND. (1+FX-FY).LE.1 .AND.
     +           (Z(IX,IY  ).NE.RMISS .AND. Z(IX+1,IY  ).EQ.RMISS .AND.
     +            Z(IX,IY+1).NE.RMISS .AND. Z(IX+1,IY+1).NE.RMISS)) THEN
                  ZZ = Z(IX  ,IY+1)*(FY-FX) 
     +               + Z(IX+1,IY+1)*FX + Z(IX, IY)*(1-FY)

                  IF(LSHDW) THEN
                    ZX = (Z(IX+1, IY+1) - Z(IX,   IY+1))/DX
                    ZY = (Z(IX,   IY+1) - Z(IX,   IY  ))/DY
                    CALL UISHDW(ZX, ZY, EX, EY, EZ, CMSK)
                  ELSE
                    CMSK = 0.
                  END IF
                  CALL UICRGB(ZZ, CMSK*DENSTY, IMAGE(I))

                ELSE IF(LCNR .AND. (2-FX-FY).LE.1 .AND.
     +           (Z(IX,IY  ).EQ.RMISS .AND. Z(IX+1,IY  ).NE.RMISS .AND.
     +            Z(IX,IY+1).NE.RMISS .AND. Z(IX+1,IY+1).NE.RMISS)) THEN
                  ZZ = Z(IX+1,IY+1)*(FX+FY-1) 
     +               + Z(IX  ,IY+1)*(1-FX) + Z(IX+1, IY)*(1-FY)

                  IF(LSHDW) THEN
                    ZX = (Z(IX+1, IY+1) - Z(IX,   IY+1))/DX
                    ZY = (Z(IX+1, IY+1) - Z(IX+1, IY  ))/DY
                    CALL UISHDW(ZX, ZY, EX, EY, EZ, CMSK)
                  ELSE
                    CMSK = 0.
                  END IF
                  CALL UICRGB(ZZ, CMSK*DENSTY, IMAGE(I))

                ELSE
                  IMAGE(I) = ICOLOR3
                END IF
              END IF

            END IF
          END IF
   20   CONTINUE

        CALL SWICLR(IMAGE, IWIDTH)
   30 CONTINUE

      CALL SWICLS

      END
*-----------------------------------------------------------------------
      SUBROUTINE UISHDW(DX, DY, EX, EY, EZ, CC)

        IF(DX .NE. 0. .AND. DY.NE.0.) THEN
          A  = 1./DX
          B  = 1./DY
          AB = SQRT(A**2+B**2)

          C  = ABS(A*B)/AB
          CS = SQRT(1.+C**2)

          VX = SIGN(B,A)/AB/CS
          VY = SIGN(A,B)/AB/CS
          VZ = C/CS
        ELSE IF(DX .NE. 0.) THEN
          A  = 1./DX
          CS = SQRT(1.+A**2)

          VX = SIGN(1.,A)/CS
          VY = 0.
          VZ = ABS(A)/CS
        ELSE IF(DY .NE. 0.) THEN
          B  = 1./DY
          CS = SQRT(1.+B**2)

          VX = 0.
          VY = SIGN(1.,B)/CS
          VZ = ABS(B)/CS
        ELSE
          VX = 0.
          VY = 0.
          VZ = 1.
        END IF

        CC = VX*EX + VY*EY + VZ*EZ

      END
