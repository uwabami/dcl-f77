*-----------------------------------------------------------------------
      SUBROUTINE UIPDA2(U, V, MX, NX, NY)

      PARAMETER (MAXPXL=4000)
      REAL      U(MX, *), V(MX, *)
      INTEGER   IMAGE(MAXPXL)

      CALL UIPD2Z(U, V, MX, NX, NY, IMAGE, MAXPXL)

      END
*-----------------------------------------------------------------------
      SUBROUTINE UIPD2Z(U, V, MX, NX, NY, IMAGE, MAXPXL)

      REAL      U(MX, *), V(MX, *)
      INTEGER   IMAGE(MAXPXL)
      INTEGER   NS(2), NP(2), NQ(2)
      LOGICAL   LMISS,LIMC,LFCC,LCX,LCY,LCNR
      DATA      NP /1,1/


*     / CHECK IMAGE CAPABILITY /

      CALL SWQIMC(LIMC)
      IF (.NOT.LIMC) THEN
        CALL MSGDMP('E','UIPGR2','NO IMAGE CAPABILITY.')
      END IF

      CALL SWQFCC(LFCC)
      IF (.NOT.LFCC) THEN
        CALL MSGDMP('E','UIPGR2','NO FULL COLOR CAPABILITY.')
      END IF

*     / GET INTERNAL PARAMETERS /

      CALL GLRGET('RUNDEF  ', RUNDEF)
      CALL GLIGET('IUNDEF  ', IUNDEF)
      CALL GLLGET('LMISS   ', LMISS )
      CALL GLRGET('RMISS   ', RMISS )

      CALL UIIGET('ICOLOR1 ', ICOLOR1)
      CALL UIIGET('ICOLOR2 ', ICOLOR2)
      CALL UIIGET('ICOLOR3 ', ICOLOR3)
      CALL UILGET('LCELLX  ', LCX)
      CALL UILGET('LCELLY  ', LCY)
      CALL UILGET('LCORNER ', LCNR)

*     / SET GRID ATTRIBUTE IF IT HAS NOT BEEN SET YET /

      NGX = NX
      NGY = NY
      IF(LCX) NGX = NGX + 1
      IF(LCY) NGY = NGY + 1
      CALL UWDFLT(NGX, NGY)

*     / INITIALIZE /

      CALL SGQVPT(VXMIN, VXMAX, VYMIN, VYMAX)

      CALL STFPR2(VXMIN, VYMIN, RX, RY)
      CALL STFWTR(RX, RY, WX1, WY1)
      CALL SWFINT(WX1, WY1, IX1, IY1)

      CALL STFPR2(VXMAX, VYMIN, RX, RY)
      CALL STFWTR(RX, RY, WX2, WY2)
      CALL SWFINT(WX2, WY2, IX2, IY2)

      CALL STFPR2(VXMAX, VYMAX, RX, RY)
      CALL STFWTR(RX, RY, WX3, WY3)
      CALL SWFINT(WX3, WY3, IX3, IY3)

      CALL STFPR2(VXMIN, VYMAX, RX, RY)
      CALL STFWTR(RX, RY, WX4, WY4)
      CALL SWFINT(WX4, WY4, IX4, IY4)

      IXMIN  = MIN(IX1, IX2, IX3, IX4)
      IYMIN  = MIN(IY1, IY2, IY3, IY4)
      IXMAX  = MAX(IX1, IX2, IX3, IX4)
      IYMAX  = MAX(IY1, IY2, IY3, IY4)
      IWIDTH = IXMAX-IXMIN+1
      IHIGHT = IYMAX-IYMIN+1

      CALL SWIOPN(IXMIN, IYMIN, IWIDTH, IHIGHT, 
     +            WX1, WY1, WX2, WY2, WX3, WY3, WX4, WY4)

      NS(1) = MX
      NS(2) = NY
      NQ(1) = NX
      NQ(2) = NY

      UMIN = RVMIN(U, NS, NP, NQ, 2)
      UMAX = RVMAX(U, NS, NP, NQ, 2)
      VMIN = RVMIN(V, NS, NP, NQ, 2)
      VMAX = RVMAX(V, NS, NP, NQ, 2)

      CALL UI2INI(UMIN, UMAX, VMIN, VMAX)

*     / LOOP FOR EACH PIXEL /

      DO 30 J=1, IHIGHT
        DO 20 I=1, IWIDTH

          CALL SWIINT(I+IXMIN-1, J+IYMIN-1, WX, WY)
          CALL STIWTR(WX, WY, RX, RY)
          CALL STIPR2(RX, RY, VX, VY)
          CALL STITRF(VX, VY, UX, UY)

          IF (UX.EQ.RUNDEF) THEN
            IMAGE(I) = ICOLOR1
          ELSE
            CALL UWQGXI(UX,IX,FX)
            CALL UWQGYI(UY,IY,FY)

            IF (IX.EQ.IUNDEF .OR. IY.EQ.IUNDEF) THEN
              IMAGE(I) = ICOLOR2
            ELSE
              IF(LCX .AND. LCY) THEN
                IF (LMISS .AND. 
     +             (U(IX,IY).EQ.RMISS.OR.V(IX,IY).EQ.RMISS)) THEN
                  IMAGE(I) = ICOLOR3
                ELSE
                  CALL UIC2D(U(IX,IY), V(IX,IY),IMAGE(I))
                END IF

              ELSE IF(LCX .AND. .NOT.LCY) THEN
                IF (LMISS .AND. 
     +             (U(IX,IY).EQ.RMISS .OR. U(IX,IY+1).EQ.RMISS .OR.
     +              V(IX,IY).EQ.RMISS .OR. V(IX,IY+1).EQ.RMISS)) THEN
                  IMAGE(I) = ICOLOR3
                ELSE
                  UU = U(IX, IY  )*(1-FY) + U(IX  ,IY+1)*FY
                  VV = V(IX, IY  )*(1-FY) + V(IX  ,IY+1)*FY
                  CALL UIC2D(UU, VV,IMAGE(I))
                END IF

              ELSE IF(.NOT.LCX .AND. LCY) THEN
                IF (LMISS .AND. 
     +             (U(IX,IY).EQ.RMISS .OR. U(IX+1,IY).EQ.RMISS .OR.
     +              V(IX,IY).EQ.RMISS .OR. V(IX+1,IY).EQ.RMISS)) THEN
                  IMAGE(I) = ICOLOR3
                ELSE
                  UU = U(IX, IY  )*(1-FX) + U(IX+1,IY  )*FX
                  VV = V(IX, IY  )*(1-FX) + V(IX+1,IY  )*FX
                  CALL UIC2D(UU, VV,IMAGE(I))
                END IF

              ELSE IF(.NOT.LCX .AND. .NOT.LCY) THEN
                IF (.NOT. LMISS .OR. 
     +           (U(IX,IY  ).NE.RMISS .AND. U(IX+1,IY  ).NE.RMISS .AND.
     +            U(IX,IY+1).NE.RMISS .AND. U(IX+1,IY+1).NE.RMISS .AND.
     +            V(IX,IY  ).NE.RMISS .AND. V(IX+1,IY  ).NE.RMISS .AND.
     +            V(IX,IY+1).NE.RMISS .AND. V(IX+1,IY+1).NE.RMISS)) THEN

                  UU = (U(IX, IY  )*(1-FX) + U(IX+1,IY  )*FX)*(1-FY)
     +               + (U(IX, IY+1)*(1-FX) + U(IX+1,IY+1)*FX)*FY
                  
                  VV = (V(IX, IY  )*(1-FX) + V(IX+1,IY  )*FX)*(1-FY)
     +               + (V(IX, IY+1)*(1-FX) + V(IX+1,IY+1)*FX)*FY
                  
                  CALL UIC2D(UU, VV,IMAGE(I))

                ELSE IF(LCNR .AND. (FX+FY).LE.1 .AND.
     +           (U(IX,IY  ).NE.RMISS .AND. U(IX+1,IY  ).NE.RMISS .AND.
     +            U(IX,IY+1).NE.RMISS .AND.
     +            U(IX,IY  ).NE.RMISS .AND. U(IX+1,IY  ).NE.RMISS .AND.
     +            U(IX,IY+1).NE.RMISS                            )) THEN

                  UU = U(IX,  IY  )*(1-FX-FY) 
     +               + U(IX+1,IY )*FX + U(IX, IY+1)*FY

                  VV = V(IX,  IY  )*(1-FX-FY) 
     +               + V(IX+1,IY )*FX + V(IX, IY+1)*FY

                  CALL UIC2D(UU, VV,IMAGE(I))

                ELSE IF(LCNR .AND. (1-FX+FY).LE.1 .AND.
     +           (U(IX,IY  ).NE.RMISS .AND. U(IX+1,IY  ).NE.RMISS .AND.
     +                                      U(IX+1,IY+1).NE.RMISS .AND.
     +            V(IX,IY  ).NE.RMISS .AND. V(IX+1,IY  ).NE.RMISS .AND.
     +                                      V(IX+1,IY+1).NE.RMISS)) THEN

                  UU = U(IX+1,IY  )*(FX-FY) 
     +               + U(IX  ,IY )*(1-FX) + U(IX+1, IY+1)*FY

                  VV = V(IX+1,IY  )*(FX-FY) 
     +               + V(IX  ,IY )*(1-FX) + V(IX+1, IY+1)*FY

                  CALL UIC2D(UU, VV,IMAGE(I))

                ELSE IF(LCNR .AND. (1+FX-FY).LE.1 .AND.
     +           (U(IX,IY  ).NE.RMISS .AND.
     +            U(IX,IY+1).NE.RMISS .AND. U(IX+1,IY+1).NE.RMISS .AND.
     +            V(IX,IY  ).NE.RMISS .AND.
     +            V(IX,IY+1).NE.RMISS .AND. V(IX+1,IY+1).NE.RMISS)) THEN

                  UU = U(IX  ,IY+1)*(FY-FX) 
     +               + U(IX+1,IY+1)*FX + U(IX, IY)*(1-FY)

                  VV = V(IX  ,IY+1)*(FY-FX) 
     +               + V(IX+1,IY+1)*FX + V(IX, IY)*(1-FY)

                  CALL UIC2D(UU, VV,IMAGE(I))

                ELSE IF(LCNR .AND. (2-FX-FY).LE.1 .AND.
     +           (                          U(IX+1,IY  ).NE.RMISS .AND.
     +            U(IX,IY+1).NE.RMISS .AND. U(IX+1,IY+1).NE.RMISS .AND.
     +                                      V(IX+1,IY  ).NE.RMISS .AND.
     +            V(IX,IY+1).NE.RMISS .AND. V(IX+1,IY+1).NE.RMISS)) THEN

                  UU = U(IX+1,IY+1)*(FX+FY-1) 
     +               + U(IX  ,IY+1)*(1-FX) + U(IX+1, IY)*(1-FY)

                  VV = V(IX+1,IY+1)*(FX+FY-1) 
     +               + V(IX  ,IY+1)*(1-FX) + V(IX+1, IY)*(1-FY)

                  CALL UIC2D(UU, VV,IMAGE(I))

                ELSE
                  IMAGE(I) = ICOLOR3
                END IF
              END IF

            END IF
          END IF
   20   CONTINUE

        CALL SWICLR(IMAGE, IWIDTH)

   30 CONTINUE

      CALL SWICLS

      END

