*-----------------------------------------------------------------------
*     UIIGET / UIISET / UIISTX
*-----------------------------------------------------------------------
      SUBROUTINE UIIGET(CP, IPARA)

      CHARACTER CP*(*)

      CHARACTER CX*8
      CHARACTER CL*40


      CALL UIIQID(CP, IDX)
      CALL UIIQVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UIISET(CP, IPARA)

      CALL UIIQID(CP, IDX)
      CALL UIISVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UIISTX(CP, IPARA)

      IP = IPARA
      CALL UIIQID(CP, IDX)

*     / SHORT NAME /

      CALL UIIQCP(IDX, CX)
      CALL RTIGET('UI', CX, IP, 1)

*     / LONG NAME /

      CALL UIIQCL(IDX, CL)
      CALL RLIGET(CL, IP, 1)

      CALL UIISVL(IDX,IP)

      RETURN
      END
