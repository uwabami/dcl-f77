*=======================================================================
      SUBROUTINE UIFLAB(X, Y, Z, UL, A, B)

      PARAMETER (XN=0.9804, YN=1., ZN=1.1812 )
      PARAMETER (THRS=0.008856, C=16./116.)

      X0 = X/XN
      Y0 = Y/YN
      Z0 = Z/ZN

      IF(X0.LE.THRS) THEN
        XX = 7.787*X0 + C
      ELSE
        XX = X0**(1./3.)
      END IF

      IF(Y0.LE.THRS) THEN
        YY = 7.787*Y0 + C
      ELSE
        YY = Y0**(1./3.)
      END IF

      IF(Z0.LE.THRS) THEN
        ZZ = 7.787*Z0 + C
      ELSE
        ZZ = Z0**(1./3.)
      ENDIF

      UL = 116.*YY - 16.
      A  = 500.*(XX-YY)
      B  = 200.*(YY-ZZ)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UIILAB(UL, A, B, X, Y, Z)

      YY = (UL+16.)/116.
      XX =  A/500. + YY
      ZZ = -B/200. + YY

      X0 = XX**3
      Y0 = YY**3
      Z0 = ZZ**3

      IF(X0.LE.THRS) X0 = (XX-C)/7.787
      IF(Y0.LE.THRS) Y0 = (YY-C)/7.787
      IF(Z0.LE.THRS) Z0 = (ZZ-C)/7.787

      X = X0*XN
      Y = Y0*YN
      Z = Z0*ZN

      END
*=======================================================================
      SUBROUTINE UIFLUV(X, Y, Z, UL, U, V)
      PARAMETER (THRS=0.008856, C=16./116.)

      IF(Y.LE.THRS) THEN
        YY = 7.787*Y + C
      ELSE
        YY = Y**(1./3.)
      END IF
      UL = 116.*YY - 16.

      XYZ = X+15*Y+3*Z
      U = 4*X / XYZ
      V = 9*Y / XYZ

      RETURN
*-----------------------------------------------------------------------
      ENTRY UIILUV(UL, U, V, X, Y, Z)

      YY = (UL+16.)/116.
      Y = YY**3
      IF(Y.LE.THRS) Y = (YY-C)/7.787

      X = (9.*U*Y)/(4.*V)
      Z = (12.-3.*U -20.*V)*Y/(4.*V)

      END
*=======================================================================
      SUBROUTINE UIFRGB(X, Y, Z, R, G, B)
      REAL X2R(3,3), R2X(3,3)

      DATA X2R /  1.9106, -0.5326, -0.2883,
     +           -0.9843,  1.9984, -0.0283,
     +            0.0584, -0.1185,  0.8985 /

      DATA R2X /  0.6067,  0.1736,  0.2001,
     +            0.2988,  0.5868,  0.1144,
     +            0.0000,  0.0661,  1.1150 /

      R = X2R(1,1)*X + X2R(2,1)*Y + X2R(3,1)*Z
      G = X2R(1,2)*X + X2R(2,2)*Y + X2R(3,2)*Z
      B = X2R(1,3)*X + X2R(2,3)*Y + X2R(3,3)*Z

      RETURN
*-----------------------------------------------------------------------
      ENTRY UIIRGB(R, G, B, X, Y, Z)

      X = R2X(1,1)*R + R2X(2,1)*G + R2X(3,1)*B
      Y = R2X(1,2)*R + R2X(2,2)*G + R2X(3,2)*B
      Z = R2X(1,3)*R + R2X(2,3)*G + R2X(3,3)*B

      END
*=======================================================================
      SUBROUTINE UIFYXY(X, Y, Z, YU, XL, YL)
     
      XYZ = X+Y+Z

      IF(XYZ.GT.0) THEN
        YU = Y
        XL = X/(X+Y+Z)
        YL = Y/(X+Y+Z)
      ELSE
        YU = 0.
        XL = 0.3
        YL = 0.3
      ENDIF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UIIYXY(YU, XL, YL, X, Y, Z)
     
      XYZ = YU/YL

      X = XL*XYZ
      Y = YU
      Z = (1-XL-YL)*XYZ

      END
*=======================================================================
      SUBROUTINE UIENCD(R, G, B, IRGB)

      DATA GAMMA /2.2/

      GI = 1./GAMMA

      RR = R**GI
      GG = G**GI
      BB = B**GI

      IR = MAX(MIN(NINT(RR*255), 255), 0)
      IG = MAX(MIN(NINT(GG*255), 255), 0)
      IB = MAX(MIN(NINT(BB*255), 255), 0)

      IRGB = IOR(ISHFT(IR,16), IOR(ISHFT(IG,8), IB))

      RETURN

*-----------------------------------------------------------------------
      ENTRY UIDECD(IRGB, R, G, B)

      RR = REAL(IAND(ISHFT(IRGB, -16), 255))/255.
      GG = REAL(IAND(ISHFT(IRGB,  -8), 255))/255.
      BB = REAL(IAND(      IRGB      , 255))/255.

      R = RR**GAMMA
      G = GG**GAMMA
      B = BB**GAMMA

      END
