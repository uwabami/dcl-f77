*-----------------------------------------------------------------------
      SUBROUTINE UICRGB(VAL,CMASK,IRGB)
      PARAMETER (NMAX=256)

      DIMENSION RC(NMAX), RC0(NMAX), IR(NMAX), IG(NMAX), IB(NMAX)
      DIMENSION RS(NMAX), RS0(NMAX), SD(NMAX)

      DIMENSION ICOLOR(*), CLRLVL(*)
      DIMENSION SHADE(*),  SHDLVL(*)

      CHARACTER CDSNX*1024,CMSG*80
      CHARACTER CDSN*(*)
      LOGICAL   LCYCLE, LSHADE, LCSET, LSSET

      SAVE

      EXTERNAL  IUFOPN

      DATA NLEV,  NSHADE       / 7, 4 /
      DATA LCSET, LSSET        /.FALSE., .FALSE./

      DATA IR / Z'FF', Z'00', Z'00', Z'00', Z'FF', Z'FF', Z'FF', 249*0/
      DATA IG / Z'00', Z'00', Z'FF', Z'FF', Z'FF', Z'00', Z'00', 249*0/
      DATA IB / Z'FF', Z'FF', Z'FF', Z'00', Z'00', Z'00', Z'FF', 249*0/
      DATA RC0/ 0.000, .1667, .3333, .5000, .6667, .8333, 1.000, 249*0./
      DATA RCMIN, RCMAX, RSMIN, RSMAX / 999.0, 999.0, 999.0, 999.0/

      DATA SD /-0.2, -0.2,  0.2,  0.2, 252*0./
      DATA RS0/ 0.0,  0.5,  0.5,  1.0, 252*0./

      IF(LCYCLE) THEN
        RANGE = RC(NLEV)-RC(1)
        VC = MOD(MOD(VAL-RC(1),RANGE)+RANGE,RANGE)+RC(1)
      ELSE
        VC = VAL
      END IF

      IF(VC.LE.RC(1)) THEN
        JR = IR(1)
        JG = IG(1)
        JB = IB(1)
        GOTO 100
      END IF

      DO 10 I=2, NLEV
        IF(VC.LE.RC(I)) THEN
           X0 = RC(I) - RC(I-1)
           X1 = RC(I) - VC
           X2 = VC   - RC(I-1)
          JR = (X2*IR(I) + X1*IR(I-1))/X0
          JG = (X2*IG(I) + X1*IG(I-1))/X0
          JB = (X2*IB(I) + X1*IB(I-1))/X0
          GOTO 100
        END IF
  10  CONTINUE

      JR = IR(NLEV)
      JG = IG(NLEV)
      JB = IB(NLEV)

  100 CONTINUE

      IF(LSHADE) THEN
        RANGE = RS(NSHADE)-RS(1)
        VM = MOD(MOD(VAL-RS(1),RANGE)+RANGE,RANGE)+RS(1)
        DO 110 I=2, NSHADE
          IF(VM.LE.RS(I)) THEN
            X0 = RS(I) - RS(I-1)
            X1 = RS(I) - VM
            X2 = VM    - RS(I-1)
            CC = (X2*SD(I) + X1*SD(I-1))/X0
            GOTO 120
          END IF
 110    CONTINUE
 120    CONTINUE
      ELSE
        CC = CMASK
      END IF

      IF(CC.GT.0) THEN
        KR = JR + (255-JR)*CC
        KG = JG + (255-JG)*CC
        KB = JB + (255-JB)*CC
      ELSE
        KR = JR*ABS(1.+CC)
        KG = JG*ABS(1.+CC)
        KB = JB*ABS(1.+CC)
      END IF

      CALL UIFPAC(KR, KG, KB, IRGB)

      RETURN

*-----------------------------------------------------------------------
      ENTRY UICINI(ZMIN, ZMAX)

      CALL UILGET('LCYCLE', LCYCLE)
      CALL UILGET('LMASK', LSHADE )

      IF(.NOT.LCSET) THEN
        RCMIN = ZMIN
        RCMAX = ZMAX
      END IF

      IF(.NOT.LSSET) THEN
        RSMIN = RCMIN
        RSMAX = RCMIN+(RCMAX-RCMIN)/10.
      END IF

      DO 200 I=1, NLEV
        RC(I) = RC0(I)*(RCMAX-RCMIN) + RCMIN
  200 CONTINUE

      DO 210 I=1, NSHADE
        RS(I) = RS0(I)*(RSMAX-RSMIN) + RSMIN
  210 CONTINUE

      RETURN
*-----------------------------------------------------------------------
      ENTRY UISCRG(ZMIN, ZMAX)

        IF(ZMIN.GE.ZMAX) THEN
          CALL MSGDMP('E','UISCRG','ZMIN > ZMAX.')
        END IF

        RCMIN = ZMIN
        RCMAX = ZMAX
        LCSET = .TRUE.
      RETURN

*-----------------------------------------------------------------------
      ENTRY UISMRG(ZMIN, ZMAX)

        IF(ZMIN.GE.ZMAX) THEN
          CALL MSGDMP('E','UISMRG','ZMIN > ZMAX.')
        END IF

        RSMIN = ZMIN
        RSMAX = ZMAX
        LSSET = .TRUE.
      RETURN

*-----------------------------------------------------------------------
      ENTRY UIQCRG(ZMIN, ZMAX)

        ZMIN = RCMIN
        ZMAX = RCMAX

      RETURN

*-----------------------------------------------------------------------
      ENTRY UIQMRG(ZMIN, ZMAX)

        ZMIN = RSMIN 
        ZMAX = RSMAX 

      RETURN

*-----------------------------------------------------------------------
      ENTRY UISCSQ(CLRLVL,  ICOLOR, N)

      IF(N.GT.NMAX) THEN
        CALL MSGDMP('M','UISCSQ','TOO MANY LEVEL (N).')
      END IF

      NLEV = MIN(N, NMAX)
      DO 20 I=1, N
        RC0(I) = (CLRLVL(I)-CLRLVL(1))/(CLRLVL(N)-CLRLVL(1))
        CALL UIIPAC(ICOLOR(I), IR(I), IG(I), IB(I))
  20  CONTINUE

      RETURN

*-----------------------------------------------------------------------
      ENTRY UISCFL(CDSN)

*     / INQUIRE OUTLINE FILE /

      CALL UIQFNM(CDSN,CDSNX)
      IF (CDSNX.EQ.' ') THEN
        CMSG='COLOR MAP FILE = '//CDSN(1:LENC(CDSN))
        CALL MSGDMP('M','UISCFL',CMSG)
        CALL MSGDMP('E','UISCFL','COLOR MAP FILE DOES NOT EXIST.')
      END IF

*     / OPEN OUTLINE FILE /

      IU=IUFOPN()
      OPEN(IU,FILE=CDSNX,FORM='FORMATTED',STATUS='OLD')

      READ(IU, '(I3)') NN

      IF(NN.GT.NMAX) THEN
        CALL MSGDMP('M','UISCFL','TOO MANY LEVEL.')
      END IF

      NLEV = MIN(NN, NMAX)

      DO 30 I=1, NLEV
        READ(IU, '(G6.4, Z8)',IOSTAT=IOST) XLEVEL, ICLR
        IF(IOST.NE.0) CALL MSGDMP('E','UISCFL','ERROR IN FILE.')

        RC0(I) = XLEVEL
        CALL UIIPAC(ICLR, IR(I), IG(I), IB(I))
  30  CONTINUE

      RMIN = RC0(1)
      RMAX = RC0(NLEV)
      DO 40 I=1, NLEV
        RC0(I) = (RC0(I)-RMIN)/(RMAX-RMIN)
  40  CONTINUE

      CLOSE(IU)
      RETURN
*-----------------------------------------------------------------------
      ENTRY UISMSQ(SHDLVL,  SHADE, N)

      IF(N.GT.NMAX) THEN
        CALL MSGDMP('M','UISMSQ','TOO MANY LEVEL (N).')
      END IF

      NSHADE = MIN(N,NMAX)
      DO 50 I=1, N
        RS0(I) = (SHDLVL(I)-SHDLVL(1))/(SHDLVL(N)-SHDLVL(1))
        SD (I) = SHADE(I)
  50  CONTINUE

      RETURN

*-----------------------------------------------------------------------
      ENTRY UISMFL(CDSN)

*     / INQUIRE OUTLINE FILE /

      CALL UIQFNM(CDSN,CDSNX)
      IF (CDSNX.EQ.' ') THEN
        CMSG='COLOR MAP FILE = '//CDSN(1:LENC(CDSN))
        CALL MSGDMP('M','UISMFL',CMSG)
        CALL MSGDMP('E','UISMFL','MASK FILE DOES NOT EXIST.')
      END IF

*     / OPEN OUTLINE FILE /

      IU=IUFOPN()
      OPEN(IU,FILE=CDSNX,FORM='FORMATTED',STATUS='OLD')

      READ(IU, '(I3)') NN
      IF(NN.GT.NMAX) THEN
        CALL MSGDMP('M','UISMFL','TOO MANY LEVEL.')
      END IF

      NSHADE = MIN(NN, NMAX)

      DO 60 I=1, NSHADE
        READ(IU, '(F6.4,F8.3)',IOSTAT=IOST) RS0(I), SD(I)
        IF(IOST.NE.0) CALL MSGDMP('E','UISMFL','ERROR IN FILE.')
  60  CONTINUE

      RMIN = RS0(1)
      RMAX = RS0(NSHADE)
      DO 70 I=1, NSHADE
        RS0(I) = (RS0(I)-RMIN)/(RMAX-RMIN)
  70  CONTINUE

      CLOSE(IU)
      END
