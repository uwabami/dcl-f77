*-----------------------------------------------------------------------
*     UIRGET / UIRSET / UIRSTX
*-----------------------------------------------------------------------
      SUBROUTINE UIRGET(CP, RPARA)

      CHARACTER CP*(*)

      CHARACTER CX*8
      CHARACTER CL*40


      CALL UIRQID(CP, IDX)
      CALL UIRQVL(IDX, RPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UIRSET(CP, RPARA)

      CALL UIRQID(CP, IDX)
      CALL UIRSVL(IDX, RPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UIRSTX(CP, RPARA)

      RP = RPARA
      CALL UIRQID(CP, IDX)

*     / SHORT NAME /

      CALL UIRQCP(IDX, CX)
      CALL RTRGET('UI', CX, RP, 1)

*     / LONG NAME /

      CALL UIRQCL(IDX, CL)
      CALL RLRGET(CL, RP, 1)

      CALL UIRSVL(IDX,RP)

      RETURN
      END
