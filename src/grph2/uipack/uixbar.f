*---------------------------------------------------------------
      SUBROUTINE UIXBAR(XMIN,XMAX,YMIN,YMAX,ZMIN,ZMAX,CPOS)
      CHARACTER CPOS*(*)
      REAL  Z(2,2)
      LOGICAL LCX, LCY,LEM

      CALL GRFIG
      CALL SGSTRN( 1 )
      CALL SGSVPT( XMIN,  XMAX, YMIN,  YMAX )
      CALL SGSWND( ZMIN,  ZMAX,  0.,  1. )
      CALL SGSTRF

      Z(1,1) = ZMIN
      Z(1,2) = ZMIN
      Z(2,1) = ZMAX
      Z(2,2) = ZMAX

      CALL UILGET('CELL_MODE_X', LCX)
      CALL UILGET('CELL_MODE_Y', LCY)
      CALL UILGET('EMBOSS     ', LEM)

      CALL UILSET('CELL_MODE_X', .FALSE.)
      CALL UILSET('CELL_MODE_Y', .FALSE.)
      CALL UILSET('EMBOSS     ', .FALSE.)

      CALL UWSGXB(ZMIN, ZMAX, 2)
      CALL UWSGYB(0.,     1., 2)
      CALL UIPDAT(Z,2,2,2)
      CALL SLPVPR(3)
      CALL USAXSC(CPOS)

      CALL UILSET('CELL_MODE_X', LCX)
      CALL UILSET('CELL_MODE_Y', LCY)
      CALL UILSET('EMBOSS     ', LEM)

      END
