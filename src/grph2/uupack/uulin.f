*-----------------------------------------------------------------------
*     POLYLINE PRIMITIVE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UULIN(N,UPX,UPY)

      REAL      UPX(*),UPY(*)


      CALL UUQLNT(ITYPE)
      CALL UUQLNI(INDEX)

      CALL UULINZ(N,UPX,UPY,ITYPE,INDEX)

      END
