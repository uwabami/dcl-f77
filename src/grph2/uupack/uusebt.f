*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UUSEBT(ITYPE)

      SAVE

      DATA      ITYPEZ/1/, INDEXZ/1/, RSIZEZ/0.02/


      ITYPEZ=ITYPE

      RETURN
*-----------------------------------------------------------------------
      ENTRY UUQEBT(ITYPE)

      ITYPE=ITYPEZ

      RETURN
*-----------------------------------------------------------------------
      ENTRY UUSEBI(INDEX)

      INDEXZ=INDEX

      RETURN
*-----------------------------------------------------------------------
      ENTRY UUQEBI(INDEX)

      INDEX=INDEXZ

      RETURN
*-----------------------------------------------------------------------
      ENTRY UUSEBS(RSIZE)

      RSIZEZ=RSIZE

      RETURN
*-----------------------------------------------------------------------
      ENTRY UUQEBS(RSIZE)

      RSIZE=RSIZEZ

      RETURN
      END
