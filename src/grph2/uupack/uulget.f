*-----------------------------------------------------------------------
*     UULGET / UULSET / UULSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UULGET(CP, LPARA)

      LOGICAL   LPARA
      CHARACTER CP*(*)

      LOGICAL   LP
      CHARACTER CX*8
      CHARACTER CL*40


      CALL UULQID(CP, IDX)
      CALL UULQVL(IDX, LPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UULSET(CP, LPARA)

      CALL UULQID(CP, IDX)
      CALL UULSVL(IDX, LPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UULSTX(CP, LPARA)

      LP = LPARA
      CALL UULQID(CP, IDX)

*     / SHORT NAME /

      CALL UULQCP(IDX, CX)
      CALL RTLGET('UU', CX, LP, 1)

*     / LONG NAME /

      CALL UULQCL(IDX, CL)
      CALL RLLGET(CL, LP, 1)

      CALL UULSVL(IDX,LP)

      RETURN
      END
