*-----------------------------------------------------------------------
*     UUSIDV
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UUSIDV(UMIN, UMAX)


      CALL UURSET('UMIN', UMIN)
      CALL UURSET('UMAX', UMAX)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UUQIDV(UMIN, UMAX)

      CALL UURGET('UMIN', UMIN)
      CALL UURGET('UMAX', UMAX)

      RETURN
      END
