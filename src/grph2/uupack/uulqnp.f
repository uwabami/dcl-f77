*-----------------------------------------------------------------------
*     UULQNP / UULQID / UULQCP / UULQVL / UULSVL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UULQNP(NCP)

      CHARACTER CP*(*)
      LOGICAL   LPARA

      CHARACTER CMSG*80


      NCP = 0

      RETURN
*-----------------------------------------------------------------------
      ENTRY UULQID(CP, IDX)

      IDX = 0

      CMSG = 'PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E','UULQID',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UULQCP(IDX, CP)

      CALL MSGDMP('E','UULQCP','IDX IS OUT OF RANGE.')

      RETURN
*-----------------------------------------------------------------------
      ENTRY UULQCL(IDX, CP)

      CALL MSGDMP('E','UULQCL','IDX IS OUT OF RANGE.')

      RETURN
*-----------------------------------------------------------------------
      ENTRY UULQVL(IDX, LPARA)

      LPARA = .FALSE.

      CALL MSGDMP('E','UULQVL','IDX IS OUT OF RANGE.')

      RETURN
*-----------------------------------------------------------------------
      ENTRY UULSVL(IDX, LPARA)

      CALL MSGDMP('E','UULSVL','IDX IS OUT OF RANGE.')

      RETURN
*-----------------------------------------------------------------------
      ENTRY UULQIN(CP, IN)

      IN = 0

      RETURN
      END
