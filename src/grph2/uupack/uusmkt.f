*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UUSMKT(ITYPE)

      SAVE

      DATA      ITYPEZ/1/, INDEXZ/1/, RSIZEZ/0.01/


      ITYPEZ=ITYPE

      RETURN
*-----------------------------------------------------------------------
      ENTRY UUQMKT(ITYPE)

      ITYPE=ITYPEZ

      RETURN
*-----------------------------------------------------------------------
      ENTRY UUSMKI(INDEX)

      INDEXZ=INDEX

      RETURN
*-----------------------------------------------------------------------
      ENTRY UUQMKI(INDEX)

      INDEX=INDEXZ

      RETURN
*-----------------------------------------------------------------------
      ENTRY UUSMKS(RSIZE)

      RSIZEZ=RSIZE

      RETURN
*-----------------------------------------------------------------------
      ENTRY UUQMKS(RSIZE)

      RSIZE=RSIZEZ

      RETURN
      END
