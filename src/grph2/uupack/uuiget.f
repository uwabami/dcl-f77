*-----------------------------------------------------------------------
*     UUIGET / UUISET / UUISTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UUIGET(CP, IPARA)

      CHARACTER CP*(*)

      CHARACTER CX*8
      CHARACTER CL*40


      CALL UUIQID(CP, IDX)
      CALL UUIQVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UUISET(CP, IPARA)

      CALL UUIQID(CP, IDX)
      CALL UUISVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UUISTX(CP, IPARA)

      IP = IPARA
      CALL UUIQID(CP, IDX)

*     / SHORT NAME /

      CALL UUIQCP(IDX, CX)
      CALL RTIGET('UU', CX, IP, 1)

*     / LONG NAME /

      CALL UUIQCL(IDX, CL)
      CALL RLIGET(CL, IP, 1)

      CALL UUISVL(IDX,IP)

      RETURN
      END
