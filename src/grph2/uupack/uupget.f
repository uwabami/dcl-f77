*-----------------------------------------------------------------------
*     UUPGET / UUPSET / UUPSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UUPGET(CP,IPARA)

      CHARACTER CP*(*)

      CHARACTER CX*8, CL*40


      CALL UUPQID(CP, IDX)
      CALL UUPQVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UUPSET(CP, IPARA)

      CALL UUPQID(CP, IDX)
      CALL UUPSVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UUPSTX(CP, IPARA)

      IP = IPARA
      CALL UUPQID(CP, IDX)
      CALL UUPQIT(IDX, IT)
      CALL UUPQCP(IDX, CX)
      CALL UUPQCL(IDX, CL)
      IF (IT .EQ. 1) THEN
        CALL RTIGET('UU', CX, IP, 1)
        CALL RLIGET(CL, IP, 1)
        CALL UUIQID(CP, IDX)
        CALL UUISVL(IDX, IP)
      ELSE IF (IT .EQ. 2) THEN
        CALL RTLGET('UU', CX, IP, 1)
        CALL RLLGET(CL, IP, 1)
        CALL UULQID(CP, IDX)
        CALL UULSVL(IDX, IP)
      ELSE IF (IT .EQ. 3) THEN
        CALL RTRGET('UU', CX, IP, 1)
        CALL RLRGET(CL, IP, 1)
        CALL UURQID(CP, IDX)
        CALL UURSVL(IDX, IP)
      END IF

      RETURN
      END
