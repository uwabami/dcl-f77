*-----------------------------------------------------------------------
*     UUIQNP / UUIQID / UUIQCP / UUIQVL / UUISVL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UUIQNP(NCP)

      CHARACTER CP*(*)

      CHARACTER CMSG*80


      NCP = 0

      RETURN
*-----------------------------------------------------------------------
      ENTRY UUIQID(CP, IDX)

      IDX = 0

      CMSG = 'PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E','UUIQID',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UUIQCP(IDX, CP)

      CALL MSGDMP('E','UUIQCP','IDX IS OUT OF RANGE.')

      RETURN
*-----------------------------------------------------------------------
      ENTRY UUIQCL(IDX, CP)

      CALL MSGDMP('E','UUIQCL','IDX IS OUT OF RANGE.')

      RETURN
*-----------------------------------------------------------------------
      ENTRY UUIQVL(IDX, IPARA)

      IPARA = 0

      CALL MSGDMP('E','UUIQVL','IDX IS OUT OF RANGE.')

      RETURN
*-----------------------------------------------------------------------
      ENTRY UUISVL(IDX, IPARA)

      CALL MSGDMP('E','UUISVL','IDX IS OUT OF RANGE.')

      RETURN
*-----------------------------------------------------------------------
      ENTRY UUIQIN(CP, IN)

      IN = 0

      RETURN
      END
