*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UULINZ(N,UPX,UPY,ITYPE,INDEX)

      REAL      UPX(*),UPY(*)

      LOGICAL   LFLAG, LMISS, LXUNI, LYUNI
      CHARACTER COBJ*80

      COMMON    /SZBLS2/ LCLIP
      LOGICAL   LCLIP


      IF (N.LT.2) THEN
        CALL MSGDMP('E','UULINZ','NUMBER OF POINTS IS LESS THAN 2.')
      END IF
      IF (ITYPE.EQ.0) THEN
        CALL MSGDMP('M','UULINZ','LINETYPE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.EQ.0) THEN
        CALL MSGDMP('M','UULINZ','LINE INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.LT.0) THEN
        CALL MSGDMP('E','UULINZ','LINE INDEX IS LESS THAN 0.')
      END IF

      CALL SGLGET('LCLIP' , LCLIP )
      CALL GLRGET('RUNDEF', RUNDEF)
      CALL GLRGET('RMISS' , RMISS)
      CALL GLLGET('LMISS' , LMISS)

      WRITE(COBJ,'(2I8)') ITYPE,INDEX
      CALL CDBLK(COBJ)
      CALL SWOOPN('UULINZ',COBJ)

      CALL SZSLTI(ITYPE,INDEX)
      CALL SZOPLU

      LXUNI = UPX(1).EQ.RUNDEF
      LYUNI = UPY(1).EQ.RUNDEF

      IF (LXUNI) THEN
        CALL UUQIDV(UXMIN, UXMAX)
        IF(UXMIN.EQ.RUNDEF) CALL SGRGET('UXMIN', UXMIN)
        IF(UXMAX.EQ.RUNDEF) CALL SGRGET('UXMAX', UXMAX)
        DX = (UXMAX-UXMIN)/(N-1)
      END IF

      IF (LYUNI) THEN
        CALL UUQIDV(UYMIN, UYMAX)
        IF(UYMIN.EQ.RUNDEF) CALL SGRGET('UYMIN', UYMIN)
        IF(UYMAX.EQ.RUNDEF) CALL SGRGET('UYMAX', UYMAX)
        DY = (UYMAX-UYMIN)/(N-1)
      END IF


      LFLAG=.FALSE.
      DO 20 I=1,N
        IF (LXUNI) THEN
          UXX = UXMIN + DX*(I-1)
        ELSE
          UXX = UPX(I)
        END IF

        IF (LYUNI) THEN
          UYY = UYMIN + DY*(I-1)
        ELSE
          UYY = UPY(I)
        END IF

        IF ((UXX.EQ.RMISS .OR. UYY.EQ.RMISS) .AND. LMISS) THEN
          LFLAG=.FALSE.
        ELSE IF (LFLAG) THEN
          CALL SZPLLU(UXX,UYY)
        ELSE
          CALL SZMVLU(UXX,UYY)
          LFLAG=.TRUE.
        END IF
   20 CONTINUE

      CALL SZCLLU
      CALL SWOCLS('UULINZ')

      END
