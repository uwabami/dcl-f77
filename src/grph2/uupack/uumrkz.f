*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UUMRKZ(N,UPX,UPY,ITYPE,INDEX,RSIZE)

      REAL      UPX(*),UPY(*)

      LOGICAL   LFLAG, LMISS, LXUNI, LYUNI
      CHARACTER CMARK*1, CSGI*1, COBJ*80

      COMMON    /SZBTX3/ LCLIP
      LOGICAL   LCLIP


      IF (N.LT.1) THEN
        CALL MSGDMP('E','UUMRKZ','NUMBER OF POINTS IS LESS THAN 1.')
      END IF
      IF (ITYPE.EQ.0) THEN
        CALL MSGDMP('M','UUMRKZ','MARKER TYPE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.EQ.0) THEN
        CALL MSGDMP('M','UUMRKZ','MARKER INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.LT.0) THEN
        CALL MSGDMP('E','UUMRKZ','MARKER INDEX IS LESS THAN 0.')
      END IF
      IF (RSIZE.EQ.0) THEN
        CALL MSGDMP('M','UUMRKZ','MARKER SIZE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (RSIZE.LT.0) THEN
        CALL MSGDMP('E','UUMRKZ','MARKER SIZE IS LESS THAN ZERO.')
      END IF

      CALL SGLGET('LCLIP' , LCLIP )
      CALL GLRGET('RUNDEF', RUNDEF)
      CALL GLLGET('LMISS' , LMISS)
      CALL GLRGET('RMISS' , RMISS)
      CALL SGRGET('PMFACT', PMF)
      CALL SGIGET('NPMSKIP',NPM)

      CMARK=CSGI(ITYPE)

      WRITE(COBJ,'(2I8,F8.5)') ITYPE,INDEX,RSIZE
      CALL CDBLK(COBJ)
      CALL SWOOPN('UUMRKZ',COBJ)

      CALL SZTXOP(RSIZE*PMF,0,0,INDEX)

      LXUNI = UPX(1).EQ.RUNDEF
      LYUNI = UPY(1).EQ.RUNDEF

      IF (LXUNI) THEN
        CALL UUQIDV(UXMIN, UXMAX)
        IF(UXMIN.EQ.RUNDEF) CALL SGRGET('UXMIN', UXMIN)
        IF(UXMAX.EQ.RUNDEF) CALL SGRGET('UXMAX', UXMAX)
        DX = (UXMAX-UXMIN)/(N-1)
      END IF

      IF (LYUNI) THEN
        CALL UUQIDV(UYMIN, UYMAX)
        IF(UYMIN.EQ.RUNDEF) CALL SGRGET('UYMIN', UYMIN)
        IF(UYMAX.EQ.RUNDEF) CALL SGRGET('UYMAX', UYMAX)
        DY = (UYMAX-UYMIN)/(N-1)
      END IF

      DO 10 I=1,N,NPM
        IF (LXUNI) THEN
          UXX = UXMIN + DX*(I-1)
        ELSE
          UXX = UPX(I)
        END IF

        IF (LYUNI) THEN
          UYY = UYMIN + DY*(I-1)
        ELSE
          UYY = UPY(I)
        END IF

        LFLAG=LMISS .AND. (UXX.EQ.RMISS .OR. UYY.EQ.RMISS)
        IF (.NOT.LFLAG) THEN
          CALL SZTXZU(UXX,UYY,CMARK)
        END IF
   10 CONTINUE

      CALL SZTXCL
      CALL SWOCLS('UUMRKZ')

      END
