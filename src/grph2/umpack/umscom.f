*-----------------------------------------------------------------------
*     UMSCOM : COMMON BLOCK SETTING
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UMSCOM

      LOGICAL   LDEG

      EXTERNAL  RFPI

      COMMON    /UMWK1/ITR, RUNDEF, IUNDEF, PI, CPR, CPD, CP

      SAVE


      CALL GLRGET('RUNDEF', RUNDEF)
      CALL GLIGET('IUNDEF', IUNDEF)
      CALL SGLGET('LDEG'  , LDEG  )

      PI = RFPI()
      CP = PI/180
      IF (LDEG) THEN
        CPD =1
        CPR = 1/CP
      ELSE
        CPD = CP
        CPR = 1
      END IF

*----------------------------- ITR (CHECK)------------------------------

      CALL SGQTRN(ITR)
      IF (.NOT.(10.LE.ITR .AND. ITR.LE.24 .OR.
     +          30.LE.ITR .AND. ITR.LE.34 ) ) THEN
        CALL MSGDMP('E', 'UMSCOM', 'INVALID TRANSFORMATION NUMBER.')
      END IF

      END
