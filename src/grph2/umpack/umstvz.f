*-----------------------------------------------------------------------
*     UMSTVZ : SET VIEW PORT AND T-WINDOW
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UMSTVZ

      LOGICAL   LPRJ

      COMMON    /UMWK1/ITR, RUNDEF, IUNDEF, PI, CPR, CPD, CP


      CALL SGQVPT(VXMIN, VXMAX, VYMIN, VYMAX)
      CALL SGLGET('L2TO3',LPRJ)

      IF (LPRJ) THEN
        RXMIN = 0.
        RXMAX = 1.
        RYMIN = 0.
        RYMAX = 1.
      ELSE
        CALL STQWTR(RXMIN, RXMAX, RYMIN, RYMAX,
     +              WXMIN, WXMAX, WYMIN, WYMAX, IWTRF)
      END IF

      IF (VXMIN .EQ. RUNDEF) VXMIN = RXMIN
      IF (VXMAX .EQ. RUNDEF) VXMAX = RXMAX
      IF (VYMIN .EQ. RUNDEF) VYMIN = RYMIN
      IF (VYMAX .EQ. RUNDEF) VYMAX = RYMAX

      CALL SGSVPT(VXMIN, VXMAX, VYMIN, VYMAX)

*------------------------------- T-WINDOW ------------------------------

      CALL SGQTXY(TXMIN, TXMAX, TYMIN, TYMAX)

      IF (TXMIN .EQ. RUNDEF) TXMIN = -CPD*180
      IF (TXMAX .EQ. RUNDEF) TXMAX =  CPD*180
      IF (TYMAX .EQ. RUNDEF) TYMAX =  CPD*90
      IF (TYMIN .EQ. RUNDEF) THEN
        IF (ITR.EQ.30) THEN
          CALL SGRGET('RSAT',RSAT)
          IF (RSAT.EQ.0.) THEN
            TYMIN = CPD*0.
          ELSE
            TYMIN = CPR*ASIN(1./RSAT)
          END IF
        ELSE
          TYMIN =  -CPD*90.
        END IF
      END IF

      CALL SGSTXY(TXMIN, TXMAX, TYMIN, TYMAX)

      END
