*-----------------------------------------------------------------------
*     UMRQNP / UMRQID / UMRQCP / UMRQVL / UMRSVL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UMRQNP(NCP)

      CHARACTER CP*(*)

      PARAMETER (NPARA = 4)

      REAL      RX(NPARA)
      LOGICAL   LCHREQ, LFIRST
      CHARACTER CPARAS(NPARA)*8
      CHARACTER CPARAL(NPARA)*40
      CHARACTER CMSG*80

      EXTERNAL  LCHREQ, LENC

      SAVE

*     / SHORT NAME /

      DATA      CPARAS(1) / 'DGRIDMJ ' /, RX(1) / 30. /
      DATA      CPARAS(2) / 'DGRIDMN ' /, RX(2) / 10. /
      DATA      CPARAS(3) / 'DGRPLMJ ' /, RX(3) / 0. /
      DATA      CPARAS(4) / 'DGRPLMN ' /, RX(4) / 0. /

*     / LONG NAME /

      DATA      CPARAL(1) / 'MAP_MAJOR_LINE_INTERVAL' /
      DATA      CPARAL(2) / 'MAP_MINOR_LINE_INTERVAL' /
      DATA      CPARAL(3) / 'MAP_MAJOR_LINE_POLAR_LIMIT' /
      DATA      CPARAL(4) / 'MAP_MINOR_LINE_POLAR_LIMIT' /

      DATA      LFIRST / .TRUE. /


      NCP = NPARA

      RETURN
*-----------------------------------------------------------------------
      ENTRY UMRQID(CP, IDX)

      DO 10 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IDX = N
          RETURN
        END IF
   10 CONTINUE
      CMSG = 'PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E','UMRQID',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UMRQCP(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAS(IDX)
      ELSE
        CALL MSGDMP('E','UMRQCP','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UMRQCL(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAL(IDX)
      ELSE
        CALL MSGDMP('E','UMRQCL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UMRQVL(IDX, RPARA)

      IF (LFIRST) THEN
        CALL RTRGET('UM', CPARAS, RX, NPARA)
        CALL RLRGET(CPARAL, RX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        RPARA = RX(IDX)
      ELSE
        CALL MSGDMP('E','UMRQVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UMRSVL(IDX, RPARA)

      IF (LFIRST) THEN
        CALL RTRGET('UM', CPARAS, RX, NPARA)
        CALL RLRGET(CPARAL, RX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        RX(IDX) = RPARA
      ELSE
        CALL MSGDMP('E','UMRSVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UMRQIN(CP, IN)

      DO 20 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IN = N
          RETURN
        END IF
   20 CONTINUE

      IN = 0

      RETURN
      END
