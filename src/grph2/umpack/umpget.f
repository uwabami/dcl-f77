*-----------------------------------------------------------------------
*     UMPGET / UMPSET / UMPSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UMPGET(CP,IPARA)

      CHARACTER CP*(*)

      CHARACTER CX*8, CL*40


      CALL UMPQID(CP, IDX)
      CALL UMPQVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UMPSET(CP, IPARA)

      CALL UMPQID(CP, IDX)
      CALL UMPSVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UMPSTX(CP, IPARA)

      IP = IPARA
      CALL UMPQID(CP, IDX)
      CALL UMPQIT(IDX, IT)
      CALL UMPQCP(IDX, CX)
      CALL UMPQCL(IDX, CL)
      IF (IT .EQ. 1) THEN
        CALL RTIGET('UM', CX, IP, 1)
        CALL RLIGET(CL, IP, 1)
        CALL UMIQID(CP, IDX)
        CALL UMISVL(IDX, IP)
      ELSE IF (IT .EQ. 2) THEN
        CALL RTLGET('UM', CX, IP, 1)
        CALL RLLGET(CL, IP, 1)
        CALL UMLQID(CP, IDX)
        CALL UMLSVL(IDX, IP)
      ELSE IF (IT .EQ. 3) THEN
        CALL RTRGET('UM', CX, IP, 1)
        CALL RLRGET(CL, IP, 1)
        CALL UMRQID(CP, IDX)
        CALL UMRSVL(IDX, IP)
      END IF

      RETURN
      END
