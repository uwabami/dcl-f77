*-----------------------------------------------------------------------
*     SET MAPPING PARAMETERS
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UMPFIT

      LOGICAL   LGLOBE

      EXTERNAL  STFTRF, STFTRN, SGQWND, UMQTXY


      CALL GLRGET('RUNDEF', RUNDEF)

      CALL UMSCOM
      CALL SGQVPT(VXMIN, VXMAX, VYMIN, VYMAX)
      CALL SGQSIM(RADIUS, VXOFF, VYOFF)

*----------------------- SET EARTH'S POSITION---------------------------

      CALL UMSPCT
      CALL UMSPCW
      CALL UMSPWD
      CALL UMSPPT
      CALL UMSPDF

      CALL SGRGET('RSAT',RSAT)
      IF (RSAT .EQ. RUNDEF) RSAT = 0.
      CALL SGRSET('RSAT',RSAT)

*--------------------- SET TENTATIVE VIEW PORT--------------------------

      CALL UMSTVZ
      CALL SGSSIM(1., 0., 0.)
      CALL SGSTRF

*--------------------------- RESCALING ---------------------------------

      CALL UMLGET('LGLOBE', LGLOBE)
      CALL SGQVPT(VXMINZ, VXMAXZ, VYMINZ, VYMAXZ)
      CALL UMQTXY(TXMINZ, TXMAXZ, TYMINZ, TYMAXZ)

      XMIN = RUNDEF

      IF (.NOT.LGLOBE) THEN
        CALL UMBNDC(XMIN,  XMAX,  YMIN,  YMAX  )
        CALL UMBNDR(STFTRF, SGQWND,
     +              XMIN,  XMAX,  YMIN,  YMAX  )
        CALL UMBNDP(XMIN,  XMAX,  YMIN,  YMAX  )
      END IF
      IF (XMIN .EQ. RUNDEF) THEN
        CALL UMBNDR(STFTRN, UMQTXY,
     +               XMIN,   XMAX,   YMIN,   YMAX  )
      END IF

*-----------------------------------------------------------------------

      IF (RADIUS.EQ.RUNDEF) 
     +     RADIUS = MIN((VXMAXZ-VXMINZ)/(XMAX-XMIN),
     +                  (VYMAXZ-VYMINZ)/(YMAX-YMIN))

      IF (VXOFF.EQ.RUNDEF)
     +     VXOFF = ((VXMAXZ + VXMINZ) - (XMAX  + XMIN )) /2. * RADIUS
      IF (VYOFF.EQ.RUNDEF)
     +     VYOFF = ((VYMAXZ + VYMINZ) - (YMAX  + YMIN )) /2. * RADIUS
      CALL SGSSIM(RADIUS, VXOFF, VYOFF)

*------------------------- RESET VIEW PORT -----------------------------

      WX = (XMAX-XMIN)*RADIUS
      WY = (YMAX-YMIN)*RADIUS

      IF (VXMIN .EQ. RUNDEF .AND. VXMAX .EQ. RUNDEF) THEN
        VXMIN = (VXMINZ+VXMAXZ-WX)/2.
        VXMAX = (VXMINZ+VXMAXZ+WX)/2.
      ELSE IF (VXMIN .EQ. RUNDEF) THEN
        VXMIN = VXMAX - WX
      ELSE IF (VXMAX .EQ. RUNDEF) THEN
        VXMAX = VXMIN + WX
      END IF

      IF (VYMIN .EQ. RUNDEF .AND. VYMAX .EQ. RUNDEF) THEN
        VYMIN = (VYMINZ+VYMAXZ-WY)/2.
        VYMAX = (VYMINZ+VYMAXZ+WY)/2.
      ELSE IF (VYMIN .EQ. RUNDEF) THEN
        VYMIN = VYMAX - WY
      ELSE IF (VYMAX .EQ. RUNDEF) THEN
        VYMAX = VYMIN + WY
      END IF

      CALL SGSVPT(VXMIN, VXMAX, VYMIN, VYMAX)

      END
