*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UMBNDP(VXMIN, VXMAX, VYMIN, VYMAX)


      CALL GLRGET('RUNDEF', RUNDEF)

      CALL UMQPTN(NDATA)
      IF (NDATA .EQ. 0 ) RETURN
      IF (VXMIN .EQ. RUNDEF) THEN
        CALL UMQPNT(1, UXZ, UYZ)
        CALL STFTRF(UXZ, UYZ, VXMIN, VYMIN)
        VXMAX = VXMIN
        VYMAX = VYMIN
      END IF

      DO 40 I=1, NDATA
        CALL UMQPNT(I, UXZ, UYZ)
        CALL STFTRF(UXZ, UYZ, VX, VY)
        VXMIN = MIN (VXMIN, VX)
        VXMAX = MAX (VXMAX, VX)
        VYMIN = MIN (VYMIN, VY)
        VYMAX = MAX (VYMAX, VY)
   40 CONTINUE

      END
