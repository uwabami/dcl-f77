*-----------------------------------------------------------------------
*     DRAW MAP (GEOGRAPHIC LINES)
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UMPMAP(CDSN)

      CHARACTER CDSN*(*)

      PARAMETER (NPMAX=8192)

      REAL      XLAT(NPMAX),XLON(NPMAX)
      REAL      XLATSG(NPMAX),XLONSG(NPMAX)
      CHARACTER CDSNX*1024,CMSG*80
      LOGICAL   LITRONE, LLAST

      EXTERNAL  IUFOPN

      LITRONE = .FALSE.

*     / GET INTERNAL PARAMETERS /

      CALL UMIGET('INDEXOUT',INDEX)
      CALL UMIGET('ITYPEOUT',ITYPE)
      CALL SGQWND(UX0,UX1,UY0,UY1)
      CALL SGQTRN(ITR)

      IF (ITR .EQ. 1) THEN
        LITRONE = .TRUE.
      ELSE IF (1 .LT. ITR .AND. ITR .LT. 10) THEN
        CALL MSGDMP('E','UMPMAP','INVALID TRANSFORMATION NUMBER.')
      END IF

*     / INQUIRE OUTLINE FILE /
      CALL UMQFNM(CDSN,CDSNX)
      IF (CDSNX.EQ.' ') THEN
        CMSG='OUTLINE FILE = '//CDSN(1:LENC(CDSN))
        CALL MSGDMP('M','UMPMAP',CMSG)
        CALL MSGDMP('E','UMPMAP','OUTLINE FILE DOES NOT EXIST.')
      END IF

*     / OPEN OUTLINE FILE /

      IU=IUFOPN()
      OPEN(IU,FILE=CDSNX,FORM='UNFORMATTED',STATUS='OLD')
      REWIND(IU)

*     / READ OUTLINE DATA AND PLOT /

      CALL SZPLOP(ITYPE,INDEX)
   10 CONTINUE
        READ(IU,IOSTAT=IOS) NPTS,IGID,XLATMX,XLATMN,XLONMX,XLONMN,
     +     (XLAT(I),XLON(I),I=1,NPTS/2)
        IF (IOS.EQ.0) THEN
          IF (NPTS.GE.2) THEN
            IF (LITRONE) THEN
              DO 15 N=1, NPTS/2
                IF (UX0 .GT. XLON(N)) XLON(N) = XLON(N) + 360.0
                IF (UX1 .LT. XLON(N)) XLON(N) = XLON(N) - 360.0
   15         CONTINUE
              N0 = 1
              LLAST = .FALSE.
   20         CONTINUE
              DO 25 N = N0+1, NPTS/2
                IF (ABS(XLON(N)-XLON(N-1)) .GT. 300.0) GO TO 30
   25         CONTINUE
   30         CONTINUE
              NM = N
              IF (NM .GT. NPTS/2) THEN
                LLAST = .TRUE.
                NM = NPTS/2
              END IF
              DO 35 N = 1, NM-N0+1
                XLATSG(N) = XLAT(N+N0-1)
   35         CONTINUE
              DO 40 N = 1, NM-N0
                XLONSG(N) = XLON(N+N0-1)
   40         CONTINUE
              IF (LLAST) THEN
                XLONSG(NM-N0+1) = XLON(NM)
              ELSE
                IF (XLON(NM) .GT. XLON(NM-1)) THEN
                  XLONSG(NM-N0+1) = XLON(NM)-360.0
                  XLON(NM-1) = XLON(NM-1)+360.0
                ELSE
                  XLONSG(NM-N0+1) = XLON(NM)+360.0
                  XLON(NM-1) = XLON(NM-1)-360.0
                END IF
              END IF
              CALL SZPLZU(NM-N0+1,XLONSG,XLATSG)
              N0 = NM-1
              IF (NM .EQ. NPTS/2) GO TO 45
              GO TO 20
            ELSE
              CALL SZPLZU(NPTS/2,XLON,XLAT)
            END IF
   45       CONTINUE
          END IF
        END IF
      IF (IOS.EQ.0) GO TO 10
   50 CONTINUE
      CALL SZPLCL

      CLOSE(IU)

      END
