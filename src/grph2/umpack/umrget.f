*-----------------------------------------------------------------------
*     UMRGET / UMRSET / UMRSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UMRGET(CP, RPARA)

      CHARACTER CP*(*)

      CHARACTER CX*8
      CHARACTER CL*40


      CALL UMRQID(CP, IDX)
      CALL UMRQVL(IDX, RPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UMRSET(CP, RPARA)

      CALL UMRQID(CP, IDX)
      CALL UMRSVL(IDX, RPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UMRSTX(CP, RPARA)

      RP = RPARA
      CALL UMRQID(CP, IDX)

*     / SHORT NAME /

      CALL UMRQCP(IDX, CX)
      CALL RTRGET('UM', CX, RP, 1)

*     / LONG NAME /

      CALL UMRQCL(IDX, CL)
      CALL RLRGET(CL, RP, 1)

      CALL UMRSVL(IDX,RP)

      RETURN
      END
