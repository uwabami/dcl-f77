*-----------------------------------------------------------------------
*     UMSPPT : SET MAP POLE (POINTS)
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UMSPPT

      COMMON    /UMWK1/ITR, RUNDEF, IUNDEF, PI, CPR, CPD, CP

      SAVE


      CALL SGQMPL(PLX, PLY, PLROT)
      IF (PLX.NE.RUNDEF .AND. PLY.NE. RUNDEF .AND.
     +    PLROT.NE.RUNDEF ) RETURN

      CALL UMQPTN(NDATA)
      IF (NDATA.EQ.0) RETURN

      X2 = 0
      Y2 = 0
      CALL UMQPNT(1, UXZ, UYZ)
      DO 20 I=1, NDATA
        CALL UMQPNT(I, UXZ, UYZ)
        YMAX = MAX(YMAX, UYZ)
        YMIN = MIN(YMIN, UYZ)
        UXR = UXZ/CPR
        UYR = UYZ/CPR
        X2 = X2 + COS(UXR)
        Y2 = Y2 + SIN(UXR)
        CALL CT3SC(1., PI/2.-UYR, UXR, X, Y, Z)
        X3 = X3+X
        Y3 = Y3+Y
        Z3 = Z3+Z
   20 CONTINUE
      UXC = ATAN2(Y2,X2)*CPR
      UYC = (YMAX + YMIN)/2.

      IF (10.LE.ITR .AND. ITR.LE.19) THEN
        PLX = UXC
        PLY = 90.*CPD
      ELSE IF (20.LE.ITR .AND. ITR.LE.24) THEN
        PLX = UXC
        PLY = 90.*CPD

        IF (UYC .EQ. 0) CALL MSGDMP ('E', 'UMSPPT',
     +    'INVALID WINDOW FOR CONICAL PROJECTION.')
        CALL SGRGET('STLAT1',STLAT1)
        CALL SGRGET('STLAT2',STLAT2)

        IF (ITR .EQ. 22) THEN
          IF (STLAT1 .EQ. RUNDEF) STLAT1 = MAX(YMIN, -89.*CPD)
          IF (STLAT2 .EQ. RUNDEF) STLAT2 = MIN(YMAX,  89.*CPD)
          CALL SGRSET('STLAT1', STLAT1)
          CALL SGRSET('STLAT2', STLAT2)
        ELSE
          IF (STLAT1 .EQ. RUNDEF) STLAT1 =  UYC
          CALL SGRSET('STLAT1',STLAT1)
        END IF
      ELSE IF (30.LE.ITR .AND. ITR.LE.34) THEN
        CALL CT3CS(X3, Y3, Z3, R, TH, UXR)
        PLX = UXR*CPR
        PLY = (PI/2.-TH)*CPR
      END IF

      CALL SGSMPL(PLX, PLY, 0.)

      END
