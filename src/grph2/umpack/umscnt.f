*-----------------------------------------------------------------------
*     CONTACT POINTS
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UMSCNT(XCNT, YCNT, ROT)

      SAVE


      XCNTZ = XCNT
      YCNTZ = YCNT
      ROTZ  = ROT

      RETURN
*-----------------------------------------------------------------------
      ENTRY UMQCNT(XCNT, YCNT, ROT)

      XCNT = XCNTZ
      YCNT = YCNTZ
      ROT  = ROTZ

      RETURN
      END
