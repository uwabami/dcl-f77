*-----------------------------------------------------------------------
*     FILL PATTERN ON MAP (GEOGRAPHIC COORDINATE)
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UMFMAP(CDSN)

      CHARACTER CDSN*(*)

      PARAMETER (NPMAX=8192)

      REAL      XLAT(NPMAX),XLON(NPMAX)
      CHARACTER CDSNX*1024,CMSG*80
      LOGICAL   LFLAKE

      EXTERNAL  IUFOPN


*     / SET INTERNAL PARAMETERS /

      CALL SGISET('IRMODE', 0)

*     / GET INTERNAL PARAMETERS /

      CALL SGIGET('IBGCLI',IBGCLI)
      CALL UMIGET('IPATLAND',IPLAND)
      CALL UMIGET('IPATLAKE',IPLAKE)
      CALL UMLGET('LFILLAKE',LFLAKE)
      IF (.NOT. LFLAKE) IPLAKE = IBGCLI*1000+999

*     / INQUIRE OUTLINE FILE /
      CALL UMQFNM(CDSN,CDSNX)
      IF (CDSNX.EQ.' ') THEN
        CMSG='OUTLINE FILE = '//CDSN(1:LENC(CDSN))
        CALL MSGDMP('M','UMPMAP',CMSG)
        CALL MSGDMP('E','UMPMAP','OUTLINE FILE DOES NOT EXIST.')
      END IF

*     / OPEN OUTLINE FILE /

      IU=IUFOPN()
      OPEN(IU,FILE=CDSNX,FORM='UNFORMATTED',STATUS='OLD')
      REWIND(IU)

*     / READ OUTLINE DATA AND PLOT /

   10 CONTINUE
        READ(IU,IOSTAT=IOS) NPTS,IGID,XLATMX,XLATMN,XLONMX,XLONMN,
     +     (XLAT(I),XLON(I),I=1,NPTS/2)
        IF (IOS.EQ.0) THEN
          IF (NPTS .GT. 10 .OR.
     +      (XLON(1).EQ.XLON(NPTS/2) .AND. XLAT(1).EQ.XLAT(NPTS/2)))
     +        THEN
            IF (IGID.EQ.1) THEN
             CALL SGTNZU(NPTS/2,XLON,XLAT,IPLAND)
            ELSE IF (IGID.EQ.2) THEN
              CALL SGTNZU(NPTS/2,XLON,XLAT,IPLAKE)
            END IF
          END IF
        END IF
      IF (IOS.EQ.0) GO TO 10
   20 CONTINUE

      CLOSE(IU)

      END
