*-----------------------------------------------------------------------
*     UMIGET / UMISET / UMISTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UMIGET(CP, IPARA)

      CHARACTER CP*(*)

      CHARACTER CX*8
      CHARACTER CL*40


      CALL UMIQID(CP, IDX)
      CALL UMIQVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UMISET(CP, IPARA)

      CALL UMIQID(CP, IDX)
      CALL UMISVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UMISTX(CP, IPARA)

      IP = IPARA
      CALL UMIQID(CP, IDX)

*     / SHORT NAME /

      CALL UMIQCP(IDX, CX)
      CALL RTIGET('UM', CX, IP, 1)

*     / LONG NAME /

      CALL UMIQCL(IDX, CL)
      CALL RLIGET(CL, IP, 1)

      CALL UMISVL(IDX,IP)

      RETURN
      END
