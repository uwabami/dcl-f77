*-----------------------------------------------------------------------
*     UMSPCW : CIRCLE WINDOW
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UMSPCW

      COMMON    /UMWK1/ITR, RUNDEF, IUNDEF, PI, CPR, CPD, CP

      SAVE


      CALL SGQMPL(PLX, PLY, PLROT)
      IF (PLX.NE.RUNDEF .AND. PLY.NE. RUNDEF .AND.
     +    PLROT.NE.RUNDEF ) RETURN

      CALL UMQCWD (XCNTR, YCNTR, R)
      IF (XCNTR .EQ. RUNDEF .OR. YCNTR.EQ.RUNDEF .OR.
     +        R .EQ. RUNDEF ) RETURN

      IF (10.LE.ITR .AND. ITR.LE.19) THEN
        PLX = XCNTR
        PLY = 90.*CPD
      ELSE IF (20.LE.ITR .AND. ITR.LE.24) THEN
        PLX = XCNTR
        PLY = 90.*CPD

        IF (YCNTR .EQ. 0) CALL MSGDMP ('E', 'UMSPCW',
     +    'INVALID WINDOW FOR CONICAL PROJECTION.')
        CALL SGRGET('STLAT1',STLAT1)
        CALL SGRGET('STLAT2',STLAT2)

        IF (ITR .EQ. 22) THEN
          IF (STLAT1 .EQ. RUNDEF) STLAT1 = MAX(YCNTR-R, -89.*CPD)
          IF (STLAT2 .EQ. RUNDEF) STLAT2 = MIN(YCNTR+R,  89.*CPD)
          CALL SGRSET('STLAT1', STLAT1)
          CALL SGRSET('STLAT2', STLAT2)
        ELSE
          IF (STLAT1 .EQ. RUNDEF) STLAT1 =  YCNTR
          CALL SGRSET('STLAT1',STLAT1)
        END IF
      ELSE IF (30.LE.ITR .AND. ITR.LE.34) THEN
        PLX = XCNTR
        PLY = YCNTR
      END IF

      CALL SGSMPL(PLX, PLY, 0.)

      END
