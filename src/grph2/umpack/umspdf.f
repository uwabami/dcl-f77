*-----------------------------------------------------------------------
*     UMSPDF : DEFAULT
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UMSPDF

      COMMON    /UMWK1/ITR, RUNDEF, IUNDEF, PI, CPR, CPD, CP

      SAVE


      CALL SGQMPL(PLX, PLY, PLROT)
      IF (PLX.NE.RUNDEF .AND. PLY.NE. RUNDEF .AND.
     +    PLROT.NE.RUNDEF ) RETURN

      CALL SGSMPL(0., CPD*90., 0.)

*------------------------ STANDARD LATITUDE ---------------------------

      CALL SGRGET('STLAT1',STLAT1)
      CALL SGRGET('STLAT2',STLAT2)

      IF (ITR.EQ.20 .OR. ITR.EQ.21 .OR. ITR.EQ.23) THEN
        IF (STLAT1 .EQ. RUNDEF) STLAT1 = CPD*35
        CALL SGRSET('STLAT1',STLAT1)
      ELSE IF (ITR .EQ. 22) THEN
        IF (STLAT1 .EQ. RUNDEF) STLAT1 = CPD*35
        IF (STLAT2 .EQ. RUNDEF) STLAT2 = CPD*45
        CALL SGRSET('STLAT1', STLAT1)
        CALL SGRSET('STLAT2', STLAT2)
      END IF

      END
