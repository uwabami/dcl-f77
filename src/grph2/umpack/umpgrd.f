*-----------------------------------------------------------------------
*     DRAW LAT-LON GRIDS OF MAP
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UMPGRD

      PARAMETER (NP=4)

      REAL      X(NP),Y(NP)
      LOGICAL   LEPSL,LLNINT,LGCINT,LGRDMJ,LGRDMN,LRNE

      EXTERNAL  LRNE,IRGT,IRLT


*     / GET INTERNAL PARAMETERS /

      CALL GLLGET('LEPSL' ,LEPSL)
      CALL SGLGET('LLNINT',LLNINT)
      CALL SGLGET('LGCINT',LGCINT)

      CALL UMSGRD

      CALL UMRGET('DGRIDMJ',DGMJ)
      CALL UMRGET('DGRIDMN',DGMN)
      CALL UMRGET('DGRPLMJ',DPMJ)
      CALL UMRGET('DGRPLMN',DPMN)
      CALL UMLGET('LGRIDMJ',LGRDMJ)
      CALL UMLGET('LGRIDMN',LGRDMN)
      CALL UMIGET('INDEXMJ',IDMJ)
      CALL UMIGET('INDEXMN',IDMN)
      CALL UMIGET('ITYPEMJ',ITMJ)
      CALL UMIGET('ITYPEMN',ITMN)

      IF (.NOT.LGRDMJ .AND. .NOT.LGRDMN) RETURN

*     / SET INTERNAL PARAMETER /

      CALL GLLSET('LEPSL' ,.TRUE.)
      CALL SGLSET('LLNINT',.TRUE.)
      CALL SGLSET('LGCINT',.TRUE.)

*     / MAJOR LINES /

      IF (LGRDMJ) THEN

        RN=360/DGMJ
        NMJ=NINT(RN)
        IF (LRNE(RN,REAL(NMJ))) THEN
          CALL MSGDMP('E','UMPGRD',
     +         'MAJOR DIVISION IS NOT A COMMON MEASURE OF 360.')
        END IF

        CALL SZPLOP(ITMJ,IDMJ)

        DO 20 M=1,NMJ
          DO 10 N=1,NP
            X(N)=-180.0+(M-1)*DGMJ
            Y(N)=(N-1)*(180.0-DPMJ*2)/(NP-1)-(90.0-DPMJ)
   10     CONTINUE
          CALL SZPLZU(NP,X,Y)
   20   CONTINUE

        M1=IRGT(-90/DGMJ)
        M2=IRLT(+90/DGMJ)
        DO 40 M=M1,M2
          DO 30 N=1,NP
            X(N)=(N-1)*360.0/(NP-1)-180.0
            Y(N)=DGMJ*M
   30     CONTINUE
          CALL SZPLZU(NP,X,Y)
   40   CONTINUE

        CALL SZPLCL

      END IF

*     / MINOR LINES /

      IF (LGRDMN) THEN

        RN=360/DGMN
        NMN=NINT(RN)
        IF (LRNE(RN,REAL(NMN))) THEN
          CALL MSGDMP('E','UMPGRD',
     +         'MINOR DIVISION IS NOT A COMMON MEASURE OF 360.')
        END IF
        IF (LGRDMJ) THEN
          IF (MOD(NMN,NMJ).NE.0) THEN
            CALL MSGDMP('E','UMPGRD',
     +           'MAJOR DIVISION IS NOT MULTIPLE OF MINOR DIVISION.')
          END IF
          NDX=NMN/NMJ
        ELSE
          NDX=1
        END IF

        CALL SZPLOP(ITMN,IDMN)

        DO 60 M=1,NMN
          IF (LGRDMJ .AND. MOD(M-1,NDX).EQ.0) GO TO 60
          DO 50 N=1,NP
            X(N)=-180.0+(M-1)*DGMN
            Y(N)=(N-1)*(180.0-DPMN*2)/(NP-1)-(90.0-DPMN)
   50     CONTINUE
          CALL SZPLZU(NP,X,Y)
   60   CONTINUE

        M1=IRGT(-90/DGMN)
        M2=IRLT(+90/DGMN)
        DO 80 M=M1,M2
          IF (LGRDMJ .AND. MOD(M,NDX).EQ.0) GO TO 80
          DO 70 N=1,NP
            X(N)=(N-1)*360.0/(NP-1)-180.0
            Y(N)=DGMN*M
   70     CONTINUE
          CALL SZPLZU(NP,X,Y)
   80   CONTINUE

        CALL SZPLCL

      END IF

*     / RESET INTERNAL PARAMETER /

      CALL GLLSET('LEPSL', LEPSL)
      CALL SGLSET('LLNINT',LLNINT)
      CALL SGLSET('LGCINT',LGCINT)

      END
