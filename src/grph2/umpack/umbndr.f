*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UMBNDR(FUNC, FTR,
     +                  XMIN,  XMAX,  YMIN,  YMAX )

      PARAMETER (NMAX = 360)

      EXTERNAL  FUNC, FTR


      CALL GLRGET('RUNDEF', RUNDEF)

      CALL FTR(UXMIN, UXMAX, UYMIN, UYMAX)

      IF (UXMIN.EQ.RUNDEF .OR. UXMAX.EQ.RUNDEF .OR.
     +    UYMIN.EQ.RUNDEF .OR. UYMAX.EQ.RUNDEF ) RETURN

      DX = (UXMAX - UXMIN) / NMAX
      DY = (UYMAX - UYMIN) / NMAX

      IF (XMIN.EQ.RUNDEF) THEN
        CALL FUNC(UXMIN, UYMIN, XMIN, YMIN)
        XMAX = XMIN
        YMAX = YMIN
      END IF

      DO 10 I=0, NMAX
        X = UXMIN + DX*I
        Y = UYMIN + DY*I

        CALL FUNC(X, UYMIN, X1, Y1)
        CALL FUNC(X, UYMAX, X2, Y2)
        CALL FUNC(UXMIN, Y, X3, Y3)
        CALL FUNC(UXMAX, Y, X4, Y4)

        XMAX = MAX (XMAX, X1, X2, X3, X4)
        XMIN = MIN (XMIN, X1, X2, X3, X4)
        YMAX = MAX (YMAX, Y1, Y2, Y3, Y4)
        YMIN = MIN (YMIN, Y1, Y2, Y3, Y4)

   10 CONTINUE

      END
