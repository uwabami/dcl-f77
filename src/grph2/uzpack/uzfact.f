*-----------------------------------------------------------------------
*     UZFACT
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UZFACT(RFACT)


      IF (RFACT.LE.0) THEN
        CALL MSGDMP('E','UZFACT','FACTOR IS LESS THAN ZERO.')
      END IF

      CALL UZRGET('RSIZET1',RSIZT1)
      CALL UZRGET('RSIZET2',RSIZT2)
      CALL UZRGET('RSIZEL1',RSIZL1)
      CALL UZRGET('RSIZEL2',RSIZL2)
      CALL UZRGET('RSIZEC1',RSIZC1)
      CALL UZRGET('RSIZEC2',RSIZC2)

      CALL UZRSET('RSIZET1',RSIZT1*RFACT)
      CALL UZRSET('RSIZET2',RSIZT2*RFACT)
      CALL UZRSET('RSIZEL1',RSIZL1*RFACT)
      CALL UZRSET('RSIZEL2',RSIZL2*RFACT)
      CALL UZRSET('RSIZEC1',RSIZC1*RFACT)
      CALL UZRSET('RSIZEC2',RSIZC2*RFACT)

      END
