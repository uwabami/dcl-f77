*-----------------------------------------------------------------------
*     UZRQNP / UZRQID / UZRQCP / UZRQVL / UZRSVL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UZRQNP(NCP)

      CHARACTER CP*(*)

      PARAMETER (NPARA  = 31)
      PARAMETER (RUNDEF = -999.)

      REAL      RX(NPARA)
      LOGICAL   LCHREQ, LFIRST
      CHARACTER CPARAS(NPARA)*8
      CHARACTER CPARAL(NPARA)*40
      CHARACTER CMSG*80

      EXTERNAL  LCHREQ, LENC

      SAVE

*     / SHORT NAME /

      DATA      CPARAS( 1) / 'UXUSER  ' /, RX( 1) / RUNDEF /
      DATA      CPARAS( 2) / 'UYUSER  ' /, RX( 2) / RUNDEF /
      DATA      CPARAS( 3) / 'ROFFXB  ' /, RX( 3) / 0. /
      DATA      CPARAS( 4) / 'ROFFXT  ' /, RX( 4) / 0. /
      DATA      CPARAS( 5) / 'ROFFXU  ' /, RX( 5) / 0. /
      DATA      CPARAS( 6) / 'ROFFYL  ' /, RX( 6) / 0. /
      DATA      CPARAS( 7) / 'ROFFYR  ' /, RX( 7) / 0. /
      DATA      CPARAS( 8) / 'ROFFYU  ' /, RX( 8) / 0. /
      DATA      CPARAS( 9) / 'ROFGXB  ' /, RX( 9) / 0. /
      DATA      CPARAS(10) / 'ROFGXT  ' /, RX(10) / 0. /
      DATA      CPARAS(11) / 'ROFGXU  ' /, RX(11) / 0. /
      DATA      CPARAS(12) / 'ROFGYL  ' /, RX(12) / 0. /
      DATA      CPARAS(13) / 'ROFGYR  ' /, RX(13) / 0. /
      DATA      CPARAS(14) / 'ROFGYU  ' /, RX(14) / 0. /
      DATA      CPARAS(15) / 'RSIZET0 ' /, RX(15) / RUNDEF /
      DATA      CPARAS(16) / 'RSIZET1 ' /, RX(16) / 0.007 /
      DATA      CPARAS(17) / 'RSIZET2 ' /, RX(17) / 0.014 /
      DATA      CPARAS(18) / 'RSIZEL0 ' /, RX(18) / RUNDEF /
      DATA      CPARAS(19) / 'RSIZEL1 ' /, RX(19) / 0.021 /
      DATA      CPARAS(20) / 'RSIZEL2 ' /, RX(20) / 0.028 /
      DATA      CPARAS(21) / 'RSIZEC0 ' /, RX(21) / RUNDEF /
      DATA      CPARAS(22) / 'RSIZEC1 ' /, RX(22) / 0.028 /
      DATA      CPARAS(23) / 'RSIZEC2 ' /, RX(23) / 0.035 /
      DATA      CPARAS(24) / 'XOFFSET ' /, RX(24) / 0.0 /
      DATA      CPARAS(25) / 'YOFFSET ' /, RX(25) / 0.0 /
      DATA      CPARAS(26) / 'XFACT   ' /, RX(26) / 1.0 /
      DATA      CPARAS(27) / 'YFACT   ' /, RX(27) / 1.0 /
      DATA      CPARAS(28) / 'PAD1    ' /, RX(28) / 0.7 /
      DATA      CPARAS(29) / 'PAD2    ' /, RX(29) / 1.5 /
      DATA      CPARAS(30) / 'RBTWN   ' /, RX(30) / 0.0 /
      DATA      CPARAS(31) / 'RUNDEF  ' /, RX(31) / RUNDEF /

*     / LONG NAME /

      DATA      CPARAL( 1) / 'X_INTERSECTION  ' /
      DATA      CPARAL( 2) / 'Y_INTERSECTION  ' /
      DATA      CPARAL( 3) / '****ROFFXB  ' /
      DATA      CPARAL( 4) / '****ROFFXT  ' /
      DATA      CPARAL( 5) / '****ROFFXU  ' /
      DATA      CPARAL( 6) / '****ROFFYL  ' /
      DATA      CPARAL( 7) / '****ROFFYR  ' /
      DATA      CPARAL( 8) / '****ROFFYU  ' /
      DATA      CPARAL( 9) / '****ROFGXB  ' /
      DATA      CPARAL(10) / '****ROFGXT  ' /
      DATA      CPARAL(11) / '****ROFGXU  ' /
      DATA      CPARAL(12) / '****ROFGYL  ' /
      DATA      CPARAL(13) / '****ROFGYR  ' /
      DATA      CPARAL(14) / '****ROFGYU  ' /
      DATA      CPARAL(15) / 'TICK_LENGTH0' /
      DATA      CPARAL(16) / 'TICK_LENGTH1' /
      DATA      CPARAL(17) / 'TICK_LENGTH2' /
      DATA      CPARAL(18) / 'LABEL_HEIGHT0' /
      DATA      CPARAL(19) / 'LABEL_HEIGHT1' /
      DATA      CPARAL(20) / 'LABEL_HEIGHT2' /
      DATA      CPARAL(21) / 'TITLE_HEIGHT0' /
      DATA      CPARAL(22) / 'TITLE_HEIGHT1' /
      DATA      CPARAL(23) / 'TITLE_HEIGHT2' /
      DATA      CPARAL(24) / 'X_AXIS_OFFSET' /
      DATA      CPARAL(25) / 'Y_AXIS_OFFSET' /
      DATA      CPARAL(26) / 'X_AXIS_FACTOR' /
      DATA      CPARAL(27) / 'Y_AXIS_FACTOR' /
      DATA      CPARAL(28) / '****PAD1    ' /
      DATA      CPARAL(29) / '****PAD2    ' /
      DATA      CPARAL(30) / 'SPAN_LABELING_CENTERING' /
      DATA      CPARAL(31) / '----RUNDEF  ' /

      DATA      LFIRST / .TRUE. /


      NCP = NPARA

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZRQID(CP, IDX)

      DO 10 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IDX = N
          RETURN
        END IF
   10 CONTINUE
      CMSG = 'PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E','UZRQID',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZRQCP(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAS(IDX)
      ELSE
        CALL MSGDMP('E','UZRQCP','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZRQCL(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAL(IDX)
      ELSE
        CALL MSGDMP('E','UZRQCL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZRQVL(IDX, RPARA)

      IF (LFIRST) THEN
        CALL RTRGET('UZ', CPARAS, RX, NPARA)
        CALL RLRGET(CPARAL, RX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        RPARA = RX(IDX)
      ELSE
        CALL MSGDMP('E','UZRQVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZRSVL(IDX, RPARA)

      IF (LFIRST) THEN
        CALL RTRGET('UZ', CPARAS, RX, NPARA)
        CALL RLRGET(CPARAL, RX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        RX(IDX) = RPARA
      ELSE
        CALL MSGDMP('E','UZRSVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZRQIN(CP, IN)

      DO 20 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IN = N
          RETURN
        END IF
   20 CONTINUE

      IN = 0

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZRSAV(IU)

      WRITE(UNIT=IU,IOSTAT=IOS) RX
      IF (IOS.NE.0) THEN
        CALL MSGDMP('E','UZRSAV','IOSTAT IS NOT ZERO.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZRRST(IU)

      READ(UNIT=IU,IOSTAT=IOS) RX
      IF (IOS.NE.0) THEN
        CALL MSGDMP('E','UZRRST','IOSTAT IS NOT ZERO.')
      END IF

      RETURN
      END
