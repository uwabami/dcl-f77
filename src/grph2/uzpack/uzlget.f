*-----------------------------------------------------------------------
*     UZLGET / UZLSET / UZLSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UZLGET(CP, LPARA)

      LOGICAL   LPARA
      CHARACTER CP*(*)

      LOGICAL   LP
      CHARACTER CX*8
      CHARACTER CL*40


      CALL UZLQID(CP, IDX)
      CALL UZLQVL(IDX, LPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZLSET(CP, LPARA)

      CALL UZLQID(CP, IDX)
      CALL UZLSVL(IDX, LPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZLSTX(CP, LPARA)

      LP = LPARA
      CALL UZLQID(CP, IDX)

*     / SHORT NAME /

      CALL UZLQCP(IDX, CX)
      CALL RTLGET('UZ', CX, LP, 1)

*     / LONG NAME /

      CALL UZLQCL(IDX, CL)
      CALL RLLGET(CL, LP, 1)

      CALL UZLSVL(IDX,LP)

      RETURN
      END
