*-----------------------------------------------------------------------
*     UZPSAV / UZPRST
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UZPSAV

      EXTERNAL  IUFOPN

      SAVE


      IU=IUFOPN()
      OPEN(UNIT=IU,FORM='UNFORMATTED',STATUS='SCRATCH')
      REWIND(IU)

      CALL UZCSAV(IU)
      CALL UZISAV(IU)
      CALL UZLSAV(IU)
      CALL UZRSAV(IU)

      REWIND(IU)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZPRST

      CALL UZCRST(IU)
      CALL UZIRST(IU)
      CALL UZLRST(IU)
      CALL UZRRST(IU)

      REWIND(IU)

      RETURN
      END
