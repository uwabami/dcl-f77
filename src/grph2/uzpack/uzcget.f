*-----------------------------------------------------------------------
*     UZCGET / UZCSET / UZCSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UZCGET(CP,CPARA)

      CHARACTER CP*(*), CPARA*(*)

      CHARACTER CX*40, CPVAL*1024


      CALL UZCQID(CP,IDX)
      CALL UZCQVL(IDX,CPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZCSET(CP,CPARA)

      CALL UZCQID(CP,IDX)
      CALL UZCSVL(IDX,CPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZCSTX(CP, CPARA)

      CPVAL = CPARA

      CALL UZCQID(CP, IDX)

      CALL UZCQCP(IDX, CX)
      CALL RTCGET('UZ', CX, CPVAL, 1)

      CALL UZCQCL(IDX, CX)
      CALL RLCGET(CX, CPVAL, 1)

      CALL UZCSVL(IDX,CPVAL)

      RETURN
      END
