*-----------------------------------------------------------------------
*     UZIQNP / UZIQID / UZIQCP / UZIQVL / UZISVL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UZIQNP(NCP)

      CHARACTER CP*(*)

      PARAMETER (NPARA  = 27)
      PARAMETER (IUNDEF = -999)

      INTEGER   IX(NPARA)
      LOGICAL   LCHREQ, LFIRST
      CHARACTER CPARAS(NPARA)*8
      CHARACTER CPARAL(NPARA)*40
      CHARACTER CMSG*80

      EXTERNAL  LCHREQ, LENC

      SAVE

*     / SHORT NAME /

      DATA      CPARAS( 1) / 'IROTLXB ' /, IX( 1) / 0 /
      DATA      CPARAS( 2) / 'IROTLXT ' /, IX( 2) / 0 /
      DATA      CPARAS( 3) / 'IROTLXU ' /, IX( 3) / 0 /
      DATA      CPARAS( 4) / 'IROTLYL ' /, IX( 4) / 0 /
      DATA      CPARAS( 5) / 'IROTLYR ' /, IX( 5) / 0 /
      DATA      CPARAS( 6) / 'IROTLYU ' /, IX( 6) / 0 /

      DATA      CPARAS( 7) / 'IROTCXB ' /, IX( 7) / 0 /
      DATA      CPARAS( 8) / 'IROTCXT ' /, IX( 8) / 0 /
      DATA      CPARAS( 9) / 'IROTCXU ' /, IX( 9) / 0 /
      DATA      CPARAS(10) / 'IROTCYL ' /, IX(10) / +1 /
      DATA      CPARAS(11) / 'IROTCYR ' /, IX(11) / +1 /
      DATA      CPARAS(12) / 'IROTCYU ' /, IX(12) / +1 /

      DATA      CPARAS(13) / 'ICENTXB ' /, IX(13) / 0 /
      DATA      CPARAS(14) / 'ICENTXT ' /, IX(14) / 0 /
      DATA      CPARAS(15) / 'ICENTXU ' /, IX(15) / 0 /
      DATA      CPARAS(16) / 'ICENTYL ' /, IX(16) / +1 /
      DATA      CPARAS(17) / 'ICENTYR ' /, IX(17) / +1 /
      DATA      CPARAS(18) / 'ICENTYU ' /, IX(18) / +1 /

      DATA      CPARAS(19) / 'INDEXT0 ' /, IX(19) / IUNDEF /
      DATA      CPARAS(20) / 'INDEXT1 ' /, IX(20) / 1 /
      DATA      CPARAS(21) / 'INDEXT2 ' /, IX(21) / 3 /

      DATA      CPARAS(22) / 'INDEXL0 ' /, IX(22) / IUNDEF /
      DATA      CPARAS(23) / 'INDEXL1 ' /, IX(23) / 3 /
      DATA      CPARAS(24) / 'INDEXL2 ' /, IX(24) / 3 /

      DATA      CPARAS(25) / 'IFLAG   ' /, IX(25) / -1 /
      DATA      CPARAS(26) / 'INNER   ' /, IX(26) / +1 /

      DATA      CPARAS(27) / 'IUNDEF  ' /, IX(27) / IUNDEF /

*     / LONG NAME /

      DATA      CPARAL( 1) / 'BOTTOM_LABEL_ANGLE' /
      DATA      CPARAL( 2) / 'TOP_LABEL_ANGLE' /
      DATA      CPARAL( 3) / 'HORIZONTAL_LABEL_ANGLE' /
      DATA      CPARAL( 4) / 'LEFT_LABEL_ANGLE' /
      DATA      CPARAL( 5) / 'RIGHT_LABEL_ANGLE' /
      DATA      CPARAL( 6) / 'VERTICAL_LABEL_ANGLE' /

      DATA      CPARAL( 7) / 'BOTTOM_TITLE_ANGLE' /
      DATA      CPARAL( 8) / 'TOP_TITLE_ANGLE' /
      DATA      CPARAL( 9) / 'HORIZONTAL_TITLE_ANGLE' /
      DATA      CPARAL(10) / 'LEFT_TITLE_ANGLE' /
      DATA      CPARAL(11) / 'RIGHT_TITLE_ANGLE' /
      DATA      CPARAL(12) / 'VERTICAL_TITLE_ANGLE' /

      DATA      CPARAL(13) / 'BOTTOM_LABEL_CENTERING' /
      DATA      CPARAL(14) / 'TOP_LABEL_CENTERING' /
      DATA      CPARAL(15) / 'HORIZONTAL_LABEL_CENTERING' /
      DATA      CPARAL(16) / 'LEFT_LABEL_CENTERING' /
      DATA      CPARAL(17) / 'RIGHT_LABEL_CENTERING' /
      DATA      CPARAL(18) / 'VERTICAL_LABEL_CENTERING' /

      DATA      CPARAL(19) / 'AXIS_LINE_INDEX0' /
      DATA      CPARAL(20) / 'AXIS_LINE_INDEX1' /
      DATA      CPARAL(21) / 'AXIS_LINE_INDEX2' /

      DATA      CPARAL(22) / 'LABEL_INDEX0' /
      DATA      CPARAL(23) / 'LABEL_INDEX1' /
      DATA      CPARAL(24) / 'LABEL_INDEX2' /

      DATA      CPARAL(25) / 'LABEL_SIDE_FOR_USER_AXIS' /
      DATA      CPARAL(26) / 'TICKMARK_SIDE' /

      DATA      CPARAL(27) / '----IUNDEF  ' /

      DATA      LFIRST / .TRUE. /


      NCP = NPARA

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZIQID(CP, IDX)

      DO 10 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IDX = N
          RETURN
        END IF
   10 CONTINUE
      CMSG = 'PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E','UZIQID',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZIQCP(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAS(IDX)
      ELSE
        CALL MSGDMP('E','UZIQCP','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZIQCL(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAL(IDX)
      ELSE
        CALL MSGDMP('E','UZIQCL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZIQVL(IDX, IPARA)

      IF (LFIRST) THEN
        CALL RTIGET('UZ', CPARAS, IX, NPARA)
        CALL RLIGET(CPARAL, IX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        IPARA = IX(IDX)
      ELSE
        CALL MSGDMP('E','UZIQVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZISVL(IDX, IPARA)

      IF (LFIRST) THEN
        CALL RTIGET('UZ', CPARAS, IX, NPARA)
        CALL RLIGET(CPARAL, IX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        IX(IDX) = IPARA
      ELSE
        CALL MSGDMP('E','UZISVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZIQIN(CP, IN)

      DO 20 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IN = N
          RETURN
        END IF
   20 CONTINUE

      IN = 0

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZISAV(IU)

      WRITE(UNIT=IU,IOSTAT=IOS) IX
      IF (IOS.NE.0) THEN
        CALL MSGDMP('E','UZISAV','IOSTAT IS NOT ZERO.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZIRST(IU)

      READ(UNIT=IU,IOSTAT=IOS) IX
      IF (IOS.NE.0) THEN
        CALL MSGDMP('E','UZIRST','IOSTAT IS NOT ZERO.')
      END IF

      RETURN
      END
