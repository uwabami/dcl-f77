*-----------------------------------------------------------------------
*     UZPGET / UZPSET / UZPSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UZPGET(CP,IPARA)

      CHARACTER CP*(*)

      CHARACTER CX*8, CL*40


      CALL UZPQID(CP, IDX)
      CALL UZPQVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZPSET(CP, IPARA)

      CALL UZPQID(CP, IDX)
      CALL UZPSVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZPSTX(CP, IPARA)

      IP = IPARA
      CALL UZPQID(CP, IDX)
      CALL UZPQIT(IDX, IT)
      CALL UZPQCP(IDX, CX)
      CALL UZPQCL(IDX, CL)
      IF (IT .EQ. 1) THEN
        CALL RTIGET('UZ', CX, IP, 1)
        CALL RLIGET(CL, IP, 1)
        CALL UZIQID(CP, IDX)
        CALL UZISVL(IDX, IP)
      ELSE IF (IT .EQ. 2) THEN
        CALL RTLGET('UZ', CX, IP, 1)
        CALL RLLGET(CL, IP, 1)
        CALL UZLQID(CP, IDX)
        CALL UZLSVL(IDX, IP)
      ELSE IF (IT .EQ. 3) THEN
        CALL RTRGET('UZ', CX, IP, 1)
        CALL RLRGET(CL, IP, 1)
        CALL UZRQID(CP, IDX)
        CALL UZRSVL(IDX, IP)
      END IF

      RETURN
      END
