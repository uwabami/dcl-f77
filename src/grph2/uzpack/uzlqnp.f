*-----------------------------------------------------------------------
*     UZLQNP / UZLQID / UZLQCP / UZLQVL / UZLSVL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UZLQNP(NCP)

      LOGICAL   LPARA
      CHARACTER CP*(*)

      PARAMETER (NPARA = 10)

      LOGICAL   LX(NPARA)
      LOGICAL   LCHREQ, LFIRST
      CHARACTER CPARAS(NPARA)*8
      CHARACTER CPARAL(NPARA)*40
      CHARACTER CMSG*80

      EXTERNAL  LCHREQ, LENC

      SAVE

*     / SHORT NAME /
      DATA      CPARAS( 1) / 'LABELXB ' /, LX( 1) / .TRUE. /
      DATA      CPARAS( 2) / 'LABELXT ' /, LX( 2) / .FALSE. /
      DATA      CPARAS( 3) / 'LABELXU ' /, LX( 3) / .TRUE. /
      DATA      CPARAS( 4) / 'LABELYL ' /, LX( 4) / .TRUE. /
      DATA      CPARAS( 5) / 'LABELYR ' /, LX( 5) / .FALSE. /
      DATA      CPARAS( 6) / 'LABELYU ' /, LX( 6) / .TRUE. /
      DATA      CPARAS( 7) / 'LOFFSET ' /, LX( 7) / .FALSE. /
      DATA      CPARAS( 8) / 'LBTWN   ' /, LX( 8) / .FALSE. /
      DATA      CPARAS( 9) / 'LBOUND  ' /, LX( 9) / .FALSE. /
      DATA      CPARAS(10) / 'LBMSG   ' /, LX(10) / .TRUE. /

*     / LONG NAME /

      DATA      CPARAL( 1) / 'DRAW_BOTTOM_LABEL' /
      DATA      CPARAL( 2) / 'DRAW_TOP_LABEL' /
      DATA      CPARAL( 3) / 'DRAW_HORIZONTAL_LABEL' /
      DATA      CPARAL( 4) / 'DRAW_LEFT_LABEL' /
      DATA      CPARAL( 5) / 'DRAW_RIGHT_LABEL' /
      DATA      CPARAL( 6) / 'DRAW_VERTICAL_LABEL' /
      DATA      CPARAL( 7) / 'ENABLE_LINEAR_OFFSET' /
      DATA      CPARAL( 8) / 'ENABLE_SPAN_LABELING' /
      DATA      CPARAL( 9) / 'TITLE_OVER_VIEWPORT' /
      DATA      CPARAL(10) / 'TITLE_OVER_VIEWPORT_MESSAGE' /

      DATA      LFIRST / .TRUE. /


      NCP = NPARA

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZLQID(CP, IDX)

      DO 10 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IDX = N
          RETURN
        END IF
 10   CONTINUE
      CMSG = 'PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E','UZLQID',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZLQCP(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAS(IDX)
      ELSE
        CALL MSGDMP('E','UZLQCP','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZLQCL(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAL(IDX)
      ELSE
        CALL MSGDMP('E','UZLQCL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZLQVL(IDX, LPARA)

      IF (LFIRST) THEN
        CALL RTLGET('UZ', CPARAS, LX, NPARA)
        CALL RLLGET(CPARAL, LX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        LPARA = LX(IDX)
      ELSE
        CALL MSGDMP('E','UZLQVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZLSVL(IDX, LPARA)

      IF (LFIRST) THEN
        CALL RTLGET('UZ', CPARAS, LX, NPARA)
        CALL RLLGET(CPARAL, LX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        LX(IDX) = LPARA
      ELSE
        CALL MSGDMP('E','UZLSVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZLQIN(CP, IN)

      DO 20 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IN = N
          RETURN
        END IF
   20 CONTINUE

      IN = 0

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZLSAV(IU)

      WRITE(UNIT=IU,IOSTAT=IOS) LX
      IF (IOS.NE.0) THEN
        CALL MSGDMP('E','UZLSAV','IOSTAT IS NOT ZERO.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZLRST(IU)

      READ(UNIT=IU,IOSTAT=IOS) LX
      IF (IOS.NE.0) THEN
        CALL MSGDMP('E','UZLRST','IOSTAT IS NOT ZERO.')
      END IF

      RETURN
      END
