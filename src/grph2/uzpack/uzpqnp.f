*-----------------------------------------------------------------------
*     UZPQNP / UZPQID / UZPQCP / UZPQVL / UZPSVL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UZPQNP(NCP)

      CHARACTER CP*(*)

      PARAMETER (NPARA  = 68)

      INTEGER   ITYPE(NPARA)
      CHARACTER CPARAS(NPARA)*8
      CHARACTER CPARAL(NPARA)*40
      CHARACTER CMSG*80

      LOGICAL   LCHREQ

      EXTERNAL  LCHREQ,LENC

      SAVE

*     / SHORT NAME /

      DATA      CPARAS( 1) / 'UXUSER  ' /, ITYPE( 1) / 3 /
      DATA      CPARAS( 2) / 'UYUSER  ' /, ITYPE( 2) / 3 /

      DATA      CPARAS( 3) / 'ROFFXB  ' /, ITYPE( 3) / 3 /
      DATA      CPARAS( 4) / 'ROFFXT  ' /, ITYPE( 4) / 3 /
      DATA      CPARAS( 5) / 'ROFFXU  ' /, ITYPE( 5) / 3 /
      DATA      CPARAS( 6) / 'ROFFYL  ' /, ITYPE( 6) / 3 /
      DATA      CPARAS( 7) / 'ROFFYR  ' /, ITYPE( 7) / 3 /
      DATA      CPARAS( 8) / 'ROFFYU  ' /, ITYPE( 8) / 3 /

      DATA      CPARAS( 9) / 'ROFGXB  ' /, ITYPE( 9) / 3 /
      DATA      CPARAS(10) / 'ROFGXT  ' /, ITYPE(10) / 3 /
      DATA      CPARAS(11) / 'ROFGXU  ' /, ITYPE(11) / 3 /
      DATA      CPARAS(12) / 'ROFGYL  ' /, ITYPE(12) / 3 /
      DATA      CPARAS(13) / 'ROFGYR  ' /, ITYPE(13) / 3 /
      DATA      CPARAS(14) / 'ROFGYU  ' /, ITYPE(14) / 3 /

      DATA      CPARAS(15) / 'LABELXB ' /, ITYPE(15) / 2 /
      DATA      CPARAS(16) / 'LABELXT ' /, ITYPE(16) / 2 /
      DATA      CPARAS(17) / 'LABELXU ' /, ITYPE(17) / 2 /
      DATA      CPARAS(18) / 'LABELYL ' /, ITYPE(18) / 2 /
      DATA      CPARAS(19) / 'LABELYR ' /, ITYPE(19) / 2 /
      DATA      CPARAS(20) / 'LABELYU ' /, ITYPE(20) / 2 /
      DATA      CPARAS(21) / 'IROTLXB ' /, ITYPE(21) / 1 /
      DATA      CPARAS(22) / 'IROTLXT ' /, ITYPE(22) / 1 /
      DATA      CPARAS(23) / 'IROTLXU ' /, ITYPE(23) / 1 /
      DATA      CPARAS(24) / 'IROTLYL ' /, ITYPE(24) / 1 /
      DATA      CPARAS(25) / 'IROTLYR ' /, ITYPE(25) / 1 /
      DATA      CPARAS(26) / 'IROTLYU ' /, ITYPE(26) / 1 /

      DATA      CPARAS(27) / 'IROTCXB ' /, ITYPE(27) / 1 /
      DATA      CPARAS(28) / 'IROTCXT ' /, ITYPE(28) / 1 /
      DATA      CPARAS(29) / 'IROTCXU ' /, ITYPE(29) / 1 /
      DATA      CPARAS(30) / 'IROTCYL ' /, ITYPE(30) / 1 /
      DATA      CPARAS(31) / 'IROTCYR ' /, ITYPE(31) / 1 /
      DATA      CPARAS(32) / 'IROTCYU ' /, ITYPE(32) / 1 /

      DATA      CPARAS(33) / 'ICENTXB ' /, ITYPE(33) / 1 /
      DATA      CPARAS(34) / 'ICENTXT ' /, ITYPE(34) / 1 /
      DATA      CPARAS(35) / 'ICENTXU ' /, ITYPE(35) / 1 /
      DATA      CPARAS(36) / 'ICENTYL ' /, ITYPE(36) / 1 /
      DATA      CPARAS(37) / 'ICENTYR ' /, ITYPE(37) / 1 /
      DATA      CPARAS(38) / 'ICENTYU ' /, ITYPE(38) / 1 /

      DATA      CPARAS(39) / 'INDEXT0 ' /, ITYPE(39) / 1 /
      DATA      CPARAS(40) / 'INDEXT1 ' /, ITYPE(40) / 1 /
      DATA      CPARAS(41) / 'INDEXT2 ' /, ITYPE(41) / 1 /

      DATA      CPARAS(42) / 'INDEXL0 ' /, ITYPE(42) / 1 /
      DATA      CPARAS(43) / 'INDEXL1 ' /, ITYPE(43) / 1 /
      DATA      CPARAS(44) / 'INDEXL2 ' /, ITYPE(44) / 1 /

      DATA      CPARAS(45) / 'RSIZET0 ' /, ITYPE(45) / 3 /
      DATA      CPARAS(46) / 'RSIZET1 ' /, ITYPE(46) / 3 /
      DATA      CPARAS(47) / 'RSIZET2 ' /, ITYPE(47) / 3 /

      DATA      CPARAS(48) / 'RSIZEL0 ' /, ITYPE(48) / 3 /
      DATA      CPARAS(49) / 'RSIZEL1 ' /, ITYPE(49) / 3 /
      DATA      CPARAS(50) / 'RSIZEL2 ' /, ITYPE(50) / 3 /

      DATA      CPARAS(51) / 'RSIZEC0 ' /, ITYPE(51) / 3 /
      DATA      CPARAS(52) / 'RSIZEC1 ' /, ITYPE(52) / 3 /
      DATA      CPARAS(53) / 'RSIZEC2 ' /, ITYPE(53) / 3 /

      DATA      CPARAS(54) / 'LOFFSET ' /, ITYPE(54) / 2 /
      DATA      CPARAS(55) / 'XOFFSET ' /, ITYPE(55) / 3 /
      DATA      CPARAS(56) / 'YOFFSET ' /, ITYPE(56) / 3 /
      DATA      CPARAS(57) / 'XFACT   ' /, ITYPE(57) / 3 /
      DATA      CPARAS(58) / 'YFACT   ' /, ITYPE(58) / 3 /

      DATA      CPARAS(59) / 'PAD1    ' /, ITYPE(59) / 3 /
      DATA      CPARAS(60) / 'PAD2    ' /, ITYPE(60) / 3 /
      DATA      CPARAS(61) / 'IFLAG   ' /, ITYPE(61) / 1 /
      DATA      CPARAS(62) / 'LBTWN   ' /, ITYPE(62) / 2 /

      DATA      CPARAS(63) / 'RBTWN   ' /, ITYPE(63) / 3 /
      DATA      CPARAS(64) / 'LBOUND  ' /, ITYPE(64) / 2 /
      DATA      CPARAS(65) / 'LBMSG   ' /, ITYPE(65) / 2 /
      DATA      CPARAS(66) / 'INNER   ' /, ITYPE(66) / 1 /

      DATA      CPARAS(67) / 'IUNDEF  ' /, ITYPE(67) / 1 /
      DATA      CPARAS(68) / 'RUNDEF  ' /, ITYPE(68) / 3 /

*     / LONG NAME /

      DATA      CPARAL( 1) / '++++UXUSER  ' /
      DATA      CPARAL( 2) / '++++UYUSER  ' /

      DATA      CPARAL( 3) / '****ROFFXB  ' /
      DATA      CPARAL( 4) / '****ROFFXT  ' /
      DATA      CPARAL( 5) / '****ROFFXU  ' /
      DATA      CPARAL( 6) / '****ROFFYL  ' /
      DATA      CPARAL( 7) / '****ROFFYR  ' /
      DATA      CPARAL( 8) / '****ROFFYU  ' /

      DATA      CPARAL( 9) / '****ROFGXB  ' /
      DATA      CPARAL(10) / '****ROFGXT  ' /
      DATA      CPARAL(11) / '****ROFGXU  ' /
      DATA      CPARAL(12) / '****ROFGYL  ' /
      DATA      CPARAL(13) / '****ROFGYR  ' /
      DATA      CPARAL(14) / '****ROFGYU  ' /

      DATA      CPARAL(15) / 'DRAW_BOTTOM_LABEL' /
      DATA      CPARAL(16) / 'DRAW_TOP_LABEL' /
      DATA      CPARAL(17) / 'DRAW_HORIZONTAL_LABEL' /
      DATA      CPARAL(18) / 'DRAW_LEFT_LABEL' /
      DATA      CPARAL(19) / 'DRAW_RIGHT_LABEL' /
      DATA      CPARAL(20) / 'DRAW_VERTICAL_LABEL' /

      DATA      CPARAL(21) / 'BOTTOM_LABAL_ANGLE' /
      DATA      CPARAL(22) / 'TOP_LABEL_ANGLE' /
      DATA      CPARAL(23) / 'HORIZONTAL_LABEL_ANGLE' /
      DATA      CPARAL(24) / 'LEFT_LABEL_ANGLE' /
      DATA      CPARAL(25) / 'RIGHT_LABEL_ANGLE' /
      DATA      CPARAL(26) / 'VERTICAL_LABEL_ANGLE' /

      DATA      CPARAL(27) / 'BOTTOM_TITLE_ANGLE' /
      DATA      CPARAL(28) / 'TOP_TITLE_ANGLE' /
      DATA      CPARAL(29) / 'HORIZONTAL_TITLE_ANGLE' /
      DATA      CPARAL(30) / 'LEFT_TITLE_ANGLE' /
      DATA      CPARAL(31) / 'RIGHT_TITLE_ANGLE' /
      DATA      CPARAL(32) / 'VERTICAL_TITLE_ANGLE' /

      DATA      CPARAL(33) / 'BOTTOM_LABEL_CENTERING' /
      DATA      CPARAL(34) / 'TOP_LABEL_CENTERING' /
      DATA      CPARAL(35) / 'HORIZONTAL_LABEL_CENTERING' /
      DATA      CPARAL(36) / 'LEFT_LABEL_CENTERING' /
      DATA      CPARAL(37) / 'RIGHT_LABEL_CENTERING' /
      DATA      CPARAL(38) / 'VERTICAL_LABEL_CENTERING' /

      DATA      CPARAL(39) / 'AXIS_LINE_INDEX0' /
      DATA      CPARAL(40) / 'AXIS_LINE_INDEX1' /
      DATA      CPARAL(41) / 'AXIS_LINE_INDEX2' /

      DATA      CPARAL(42) / 'LABEL_INDEX0' /
      DATA      CPARAL(43) / 'LABEL_INDEX1' /
      DATA      CPARAL(44) / 'LABEL_INDEX2' /

      DATA      CPARAL(45) / 'TICK_LENGTH0' /
      DATA      CPARAL(46) / 'TICK_LENGTH1' /
      DATA      CPARAL(47) / 'TICK_LENGTH2' /

      DATA      CPARAL(48) / 'LABEL_HEIGHT0' /
      DATA      CPARAL(49) / 'LABEL_HEIGHT1' /
      DATA      CPARAL(50) / 'LABEL_HEIGHT2' /

      DATA      CPARAL(51) / 'TITLE_HEIGHT0' /
      DATA      CPARAL(52) / 'TITLE_HEIGHT1' /
      DATA      CPARAL(53) / 'TITLE_HEIGHT2' /

      DATA      CPARAL(54) / 'LINEAR_OFFSET' /
      DATA      CPARAL(55) / 'X_AXIS_OFFSET' /
      DATA      CPARAL(56) / 'Y_AXIS_OFFSET' /
      DATA      CPARAL(57) / 'X_AXIS_FACTOR' /
      DATA      CPARAL(58) / 'Y_AXIS_FACTOR' /

      DATA      CPARAL(59) / '****PAD1    ' /
      DATA      CPARAL(60) / '****PAD2    ' /
      DATA      CPARAL(61) / 'LABEL_SIDE_FOR_USER_AXIS' /
      DATA      CPARAL(62) / 'ENABLE_SPAN_LABELING' /
      DATA      CPARAL(63) / 'SPAN_LABELING_CENTERING' /
      DATA      CPARAL(64) / 'TITLE_OVER_VIEWPORT' /
      DATA      CPARAL(65) / 'TITLE_OVER_VIEWPORT_MESSAGE' /
      DATA      CPARAL(66) / 'TICKMARK_SIDE' /

      DATA      CPARAL(67) / '----IUNDEF  ' /
      DATA      CPARAL(68) / '----RUNDEF  ' /


      NCP = NPARA

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZPQID(CP, IDX)

      DO 10 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IDX = N
          RETURN
        END IF
   10 CONTINUE
      CMSG='PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E','UZPQID',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZPQCP(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAS(IDX)
      ELSE
        CALL MSGDMP('E','UZPQCP','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZPQCL(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAL(IDX)
      ELSE
        CALL MSGDMP('E','UZPQCL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZPQIT(IDX, ITP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        ITP = ITYPE(IDX)
      ELSE
        CALL MSGDMP('E','UZPQIT','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZPQVL(IDX, IPARA)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        IF (ITYPE(IDX) .EQ. 1) THEN
          CALL UZIQID(CPARAS(IDX), ID)
          CALL UZIQVL(ID, IPARA)
        ELSE IF (ITYPE(IDX) .EQ. 2) THEN
          CALL UZLQID(CPARAS(IDX), ID)
          CALL UZLQVL(ID, IPARA)
        ELSE IF (ITYPE(IDX) .EQ. 3) THEN
          CALL UZRQID(CPARAS(IDX), ID)
          CALL UZRQVL(ID, IPARA)
        END IF
      ELSE
        CALL MSGDMP('E','UZPQVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZPSVL(IDX, IPARA)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        IF (ITYPE(IDX) .EQ. 1) THEN
          CALL UZIQID(CPARAS(IDX), ID)
          CALL UZISVL(ID, IPARA)
        ELSE IF (ITYPE(IDX) .EQ. 2) THEN
          CALL UZLQID(CPARAS(IDX), ID)
          CALL UZLSVL(ID, IPARA)
        ELSE IF (ITYPE(IDX) .EQ. 3) THEN
          CALL UZRQID(CPARAS(IDX), ID)
          CALL UZRSVL(ID, IPARA)
        END IF
      ELSE
        CALL MSGDMP('E','UZPSVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZPQIN(CP, IN)

      DO 20 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IN = N
          RETURN
        END IF
   20 CONTINUE

      IN = 0

      RETURN
      END
