*-----------------------------------------------------------------------
*     UZRGET / UZRSET / UZRSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UZRGET(CP, RPARA)

      CHARACTER CP*(*)

      CHARACTER CX*8
      CHARACTER CL*40


      CALL UZRQID(CP, IDX)
      CALL UZRQVL(IDX, RPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZRSET(CP, RPARA)

      CALL UZRQID(CP, IDX)
      CALL UZRSVL(IDX, RPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZRSTX(CP, RPARA)

      RP = RPARA
      CALL UZRQID(CP, IDX)

*     / SHORT NAME /

      CALL UZRQCP(IDX, CX)
      CALL RTRGET('UZ', CX, RP, 1)

*     / LONG NAME /

      CALL UZRQCL(IDX, CL)
      CALL RLRGET(CL, RP, 1)

      CALL UZRSVL(IDX,RP)

      RETURN
      END
