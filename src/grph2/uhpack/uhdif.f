*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UHDIF(N,UPX1,UPX2,UPY)

      REAL      UPX1(*),UPX2(*),UPY(*)


      CALL UUQARP(ITPAT1, ITPAT2)

      CALL UHDIFZ(N,UPX1,UPX2,UPY,ITPAT1,ITPAT2)

      END
