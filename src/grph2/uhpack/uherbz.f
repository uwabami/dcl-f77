*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UHERBZ(N,UPX1,UPX2,UPY,ITYPE,INDEX,RSIZE)

      REAL      UPX1(*),UPX2(*),UPY(*)

      LOGICAL   LMISS, LYUNI
      CHARACTER COBJ*80

      COMMON    /SZBLS2/ LCLIP
      COMMON    /SZBTX3/ LCLIPT
      LOGICAL   LCLIP, LCLIPT


      IF (N.LT.1) THEN
        CALL MSGDMP('E','UHERBZ','NUMBER OF POINTS IS LESS THAN 1.')
      END IF
      IF (ITYPE.EQ.0) THEN
        CALL MSGDMP('M','UHERBZ','LINE TYPE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.EQ.0) THEN
        CALL MSGDMP('M','UHERBZ','LINE INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.LT.0) THEN
        CALL MSGDMP('E','UHERBZ','LINE INDEX IS LESS THAN 0.')
      END IF
      IF (RSIZE.EQ.0) THEN
        CALL MSGDMP('M','UHERBZ','MARKER SIZE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (RSIZE.LT.0) THEN
        CALL MSGDMP('E','UHERBZ','MARKER SIZE IS LESS THAN ZERO.')
      END IF

      CALL SGLGET('LCLIP' , LCLIP )
      LCLIPT = LCLIP
      CALL GLRGET('RUNDEF', RUNDEF)
      CALL GLRGET('RMISS' , RMISS)
      CALL GLLGET('LMISS' , LMISS)

      IF (UPX1(1).EQ.RUNDEF .OR. UPX2(1).EQ.RUNDEF) THEN
        CALL MSGDMP('E', 'UHERBZ',
     #    'RUNDEF CAN NOT BE UESED FOR UPY1 OR UPY2')
      END IF

      WRITE(COBJ,'(2I8,F8.5)') ITYPE, INDEX, RSIZE
      CALL CDBLK(COBJ)
      CALL SWOOPN('UHERBZ',COBJ)

      CALL SZSLTI(ITYPE,INDEX)

      LYUNI = UPY(1).EQ.RUNDEF

      IF (LYUNI) THEN
        CALL UUQIDV(UYMIN, UYMAX)
        IF (UYMIN.EQ.RUNDEF) CALL SGRGET('UYMIN', UYMIN)
        IF (UYMAX.EQ.RUNDEF) CALL SGRGET('UYMAX', UYMAX)
        DY = (UYMAX-UYMIN)/(N-1)
      END IF

      DO 20 I=1,N
        IF (LYUNI) THEN
          UYY = UYMIN + DY*(I-1)
        ELSE
          UYY = UPY(I)
        END IF

        IF (.NOT.
     #    ((UYY.EQ.RMISS .OR. UPX1(I).EQ.RMISS .OR. UPX2(I).EQ.RMISS)
     #     .AND. LMISS)) THEN

          CALL STFTRF(UPX1(I), UYY, VX1, VYY)
          CALL STFTRF(UPX2(I), UYY, VX2, VYY)

          CALL SZOPLV
          CALL SZMVLV(VX1, VYY)
          CALL SZPLLV(VX2, VYY)
          CALL SZCLLV

          CALL SZOPSV
          CALL SZMVSV(VX1, VYY-RSIZE/2.)
          CALL SZPLSV(VX1, VYY+RSIZE/2.)
          CALL SZMVSV(VX2, VYY-RSIZE/2.)
          CALL SZPLSV(VX2, VYY+RSIZE/2.)
          CALL SZCLSV
        END IF
   20 CONTINUE

      CALL SWOCLS('UHERBZ')

      END
