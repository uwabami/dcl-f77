*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UHBXFZ(N,UPX1,UPX2,UPY,ITYPE,INDEX)

      REAL      UPX1(*),UPX2(*),UPY(*)

      LOGICAL   LMISS, LYUNI, LXC1, LXC2
      CHARACTER COBJ*80

      COMMON    /SZBLS2/ LCLIP
      LOGICAL   LCLIP


      IF (N.LT.1) THEN
        CALL MSGDMP('E','UHBXFZ','NUMBER OF POINTS IS LESS THAN 1.')
      END IF
      IF (ITYPE.EQ.0) THEN
        CALL MSGDMP('M','UHBXFZ','LINETYPE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.EQ.0) THEN
        CALL MSGDMP('M','UHBXFZ','LINE INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.LT.0) THEN
        CALL MSGDMP('E','UHBXFZ','LINE INDEX IS LESS THAN 0.')
      END IF

      CALL SGLGET('LCLIP' , LCLIP )
      CALL GLRGET('RUNDEF', RUNDEF)
      CALL GLRGET('RMISS' , RMISS)
      CALL GLLGET('LMISS' , LMISS)

      WRITE(COBJ,'(2I8)') ITYPE, INDEX
      CALL CDBLK(COBJ)
      CALL SWOOPN('UHBXFZ',COBJ)

      CALL SZSLTI(ITYPE,INDEX)

      LYUNI = UPY(1).EQ.RUNDEF
      LXC1  = UPX1(1).EQ.RUNDEF
      LXC2  = UPX2(1).EQ.RUNDEF

      IF (LYUNI) THEN
        CALL UUQIDV(UYMIN, UYMAX)
        IF (UYMIN.EQ.RUNDEF) CALL SGRGET('UYMIN', UYMIN)
        IF (UYMAX.EQ.RUNDEF) CALL SGRGET('UYMAX', UYMAX)
        DY = (UYMAX-UYMIN)/N
      END IF

      IF (LXC1 .OR. LXC2) THEN
        CALL UURGET('UREF', UREF)
      END IF

      DO 20 I=1,N
        IF (LYUNI) THEN
          UY1 = UYMIN + DY*(I-1)
          UY2 = UYMIN + DY*I
        ELSE
          UY1 = UPY(I)
          UY2 = UPY(I+1)
        END IF

        IF (LXC1) THEN
          UX1 = UREF
        ELSE
          UX1 = UPX1(I)
        END IF

        IF (LXC2) THEN
          UX2 = UREF
        ELSE
          UX2 = UPX2(I)
        END IF

        IF (.NOT.
     #    ((UX1.EQ.RMISS .OR. UX1.EQ.RMISS .OR.
     #      UY1.EQ.RMISS .OR. UY2.EQ.RMISS) .AND. LMISS)) THEN

          CALL SZOPLU
          CALL SZMVLU(UX1, UY2)
          CALL SZPLLU(UX2, UY2)
          CALL SZPLLU(UX2, UY1)
          CALL SZPLLU(UX1, UY1)
          CALL SZPLLU(UX1, UY2)
          CALL SZCLLU

        END IF
   20 CONTINUE

      CALL SWOCLS('UHBXFZ')

      END
