*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UHBRL(N,UPX,UPY)

      REAL      UPX(*),UPY(*)


      CALL UUQLNT(ITYPE)
      CALL UUQLNI(INDEX)
      CALL UUQBRS(RSIZE)

      CALL UHBRLZ(N,UPX,UPY,ITYPE,INDEX,RSIZE)

      END
