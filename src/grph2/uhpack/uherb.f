*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UHERB(N,UPX1,UPX2,UPY)

      REAL      UPX1(*),UPX2(*),UPY(*)


      CALL UUQEBT(ITYPE)
      CALL UUQEBI(INDEX)
      CALL UUQEBS(RSIZE)

      CALL UHERBZ(N,UPX1,UPX2,UPY,ITYPE,INDEX,RSIZE)

      END
