*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UHBRA(N,UPX1,UPX2,UPY)

      REAL      UPX1(*),UPX2(*),UPY(*)


      CALL UUQARP(ITPAT1, ITPAT2)
      CALL UUQBRS(RSIZE)

      CALL UHBRAZ(N,UPX1,UPX2,UPY,ITPAT1,ITPAT2,RSIZE)

      END
