*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UHBXLZ(N,UPX,UPY,ITYPE,INDEX)

      REAL      UPX(*),UPY(*)

      LOGICAL   LFLAG, LMISS, LYUNI
      CHARACTER COBJ*80

      COMMON    /SZBLS2/ LCLIP
      LOGICAL   LCLIP


      IF (N.LT.2) THEN
        CALL MSGDMP('E','UHBOXL','NUMBER OF POINTS IS LESS THAN 2.')
      END IF
      IF (ITYPE.EQ.0) THEN
        CALL MSGDMP('M','UHBOXL','LINE TYPE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.EQ.0) THEN
        CALL MSGDMP('M','UHBOXL','LINE INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.LT.0) THEN
        CALL MSGDMP('E','UHBOXL','LINE INDEX IS LESS THAN 0.')
      END IF

      CALL SGLGET('LCLIP' , LCLIP )
      CALL GLRGET('RUNDEF', RUNDEF)
      CALL GLRGET('RMISS' , RMISS)
      CALL GLLGET('LMISS' , LMISS)

      IF (UPX(1).EQ.RUNDEF) THEN
        CALL MSGDMP('E', 'UHBXLZ', 'RUNDEF CAN NOT BE UESED FOR UPX.')
      END IF

      WRITE(COBJ,'(2I8)') ITYPE,INDEX
      CALL CDBLK(COBJ)
      CALL SWOOPN('UHBXLZ',COBJ)

      CALL SZSLTI(ITYPE,INDEX)
      CALL SZOPLU

      LYUNI = UPY(1).EQ.RUNDEF

      IF (LYUNI) THEN
        CALL UUQIDV(UYMIN, UYMAX)
        IF (UYMIN.EQ.RUNDEF) CALL SGRGET('UYMIN', UYMIN)
        IF (UYMAX.EQ.RUNDEF) CALL SGRGET('UYMAX', UYMAX)
        DY = (UYMAX-UYMIN)/N
      END IF

      CALL SZOPLU

      LFLAG=.FALSE.
      DO 20 I=1,N
        IF (LYUNI) THEN
          UY1 = UYMIN + DY*(I-1)
          UY2 = UYMIN + DY*I
        ELSE
          UY1 = UPY(I)
          UY2 = UPY(I+1)
        END IF

        IF ((UPX(I).EQ.RMISS .OR. UY1.EQ.RMISS .OR. UY2.EQ.RMISS)
     #    .AND. LMISS) THEN
          LFLAG=.FALSE.
        ELSE
          IF (LFLAG) THEN
            CALL SZPLLU(UPX(I),UY1)
            CALL SZPLLU(UPX(I),UY2)
          ELSE
            CALL SZMVLU(UPX(I),UY1)
            CALL SZPLLU(UPX(I),UY2)
            LFLAG=.TRUE.
          END IF
        END IF
   20 CONTINUE

      CALL SZCLLU
      CALL SWOCLS('UHBXLZ')

      END
