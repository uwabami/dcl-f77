*-----------------------------------------------------------------------
*     RPRD
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      REAL FUNCTION RPRD(RX,RY,N,JX,JY)

      REAL      RX(*),RY(*)


      SUM=0
      KX=1-JX
      KY=1-JY
      DO 10 I=1,N
        KX=KX+JX
        KY=KY+JY
        SUM=SUM+RX(KX)*RY(KY)
   10 CONTINUE
      RPRD=SUM

      END
