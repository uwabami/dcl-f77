*-----------------------------------------------------------------------
*     RCOV
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      REAL FUNCTION RCOV(RX,RY,N,JX,JY)

      REAL      RX(*),RY(*)

      EXTERNAL  RAVE0


      XAVE=RAVE0(RX,N,JX)
      YAVE=RAVE0(RY,N,JY)
      SUM=0
      KX=1-JX
      KY=1-JY
      DO 10 I=1,N
        KX=KX+JX
        KY=KY+JY
        SUM=SUM+(RX(KX)-XAVE)*(RY(KY)-YAVE)
   10 CONTINUE
      RCOV=SUM/N

      END
