*-----------------------------------------------------------------------
*     RCOR
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      REAL FUNCTION RCOR(RX,RY,N,JX,JY)

      REAL      RX(*),RY(*)

      EXTERNAL  RVAR0,RCOV


      RVARX=RVAR0(RX,N,JX)
      RVARY=RVAR0(RY,N,JY)
      IF (RVARX.EQ.0 .OR. RVARY.EQ.0) THEN
        CALL GLRGET('RMISS',RMISS)
        RCOR=RMISS
        CALL MSGDMP('W','RCOR  ',
     +    'VARIANCE OF RX OR RY IS EQUAL TO 0 / '//
     +    'MISSING VALUE IS SUBSTITUTED.')
      ELSE
        RCOR=RCOV(RX,RY,N,JX,JY)/SQRT(RVARX*RVARY)
      END IF

      END
