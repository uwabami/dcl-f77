*-----------------------------------------------------------------------
*     G2FBL2
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
*     G2FBL2  Bilinear interpolation of 2 parameters:
*             Interpolation if 0<=P<=1 and 0<=Q<=1.
*             Extraporation otherwise.
*-----------------------------------------------------------------------
      SUBROUTINE G2FBL2(P,Q,X00,X10,X01,X11,Y00,Y10,Y01,Y11,
     &                  X,Y)

      CALL G2FBLI(P,Q,X00,X10,X01,X11, X)
      CALL G2FBLI(P,Q,Y00,Y10,Y01,Y11, Y)

      END
