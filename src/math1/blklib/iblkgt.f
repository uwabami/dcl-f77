*-----------------------------------------------------------------------
*     IBLKGT
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      INTEGER FUNCTION IBLKGT(RX,N,RR)

      REAL      RX(*)
      LOGICAL   LRGT

      EXTERNAL  LRGT


      DO 10 I=1,N-1
        IF (.NOT.(RX(I).LT.RX(I+1))) THEN
          CALL MSGDMP('E','IBLKGT','ORDER OF RX IS INVALID.')
        END IF
   10 CONTINUE

      DO 15 I=N,1,-1
        IF (LRGT(RR,RX(I))) THEN
          IBLKGT=I
          RETURN
        END IF
   15 CONTINUE
      IBLKGT=0

      END
