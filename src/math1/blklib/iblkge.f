*-----------------------------------------------------------------------
*     IBLKGE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      INTEGER FUNCTION IBLKGE(RX,N,RR)

      REAL      RX(*)
      LOGICAL   LRGE

      EXTERNAL  LRGE


      DO 10 I=1,N-1
        IF (.NOT.(RX(I).LT.RX(I+1))) THEN
          CALL MSGDMP('E','IBLKGE','ORDER OF RX IS INVALID.')
        END IF
   10 CONTINUE

      DO 15 I=N,1,-1
        IF (LRGE(RR,RX(I))) THEN
          IBLKGE=I
          RETURN
        END IF
   15 CONTINUE
      IBLKGE=0

      END
