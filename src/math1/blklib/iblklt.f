*-----------------------------------------------------------------------
*     IBLKLT
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      INTEGER FUNCTION IBLKLT(RX,N,RR)

      REAL      RX(*)
      LOGICAL   LRLT

      EXTERNAL  LRLT


      DO 10 I=1,N-1
        IF (.NOT.(RX(I).LT.RX(I+1))) THEN
          CALL MSGDMP('E','IBLKLT','ORDER OF RX IS INVALID.')
        END IF
   10 CONTINUE

      DO 15 I=1,N,+1
        IF (LRLT(RR,RX(I))) THEN
          IBLKLT=I
          RETURN
        END IF
   15 CONTINUE
      IBLKLT=N+1

      END
