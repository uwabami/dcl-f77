*-----------------------------------------------------------------------
*     IBLKLE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      INTEGER FUNCTION IBLKLE(RX,N,RR)

      REAL      RX(*)
      LOGICAL   LRLE

      EXTERNAL  LRLE


      DO 10 I=1,N-1
        IF (.NOT.(RX(I).LT.RX(I+1))) THEN
          CALL MSGDMP('E','IBLKLE','ORDER OF RX IS INVALID.')
        END IF
   10 CONTINUE

      DO 15 I=1,N,+1
        IF (LRLE(RR,RX(I))) THEN
          IBLKLE=I
          RETURN
        END IF
   15 CONTINUE
      IBLKLE=N+1

      END
