*-----------------------------------------------------------------------
*     RGNGT
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      REAL FUNCTION RGNGT(RX)

      EXTERNAL  REXP


      CALL GNGT(RX,BX,IP)
      RGNGT=REXP(BX,10,IP)

      END
