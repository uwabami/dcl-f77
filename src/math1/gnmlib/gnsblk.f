*-----------------------------------------------------------------------
*     GNSBLK / GNQBLK / GNLT / GNLE / GNGT / GNGE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE GNSBLK(XB,NB)

      REAL      XB(*)

      PARAMETER (NBMAX=20,NBINT=11)

      REAL      XBZ(NBMAX)

      EXTERNAL  IMOD,IGUS,IBLKLT,IBLKLE,IBLKGT,IBLKGE

      SAVE

      DATA      NBZ/NBINT/
      DATA      (XBZ(I),I=1,NBINT)
     +          /1.0,1.2,1.5,2.0,2.5,3.0,4.0,5.0,6.0,8.0,10.0/


      IF (.NOT.(2.LE.NB .AND. NB.LE.NBMAX)) THEN
        CALL MSGDMP('E','GNSBLK','NUMBER OF BLOCKS IS INVALID.')
      END IF
      IF (.NOT.(XB(1).EQ.1.0 .AND. XB(NB).EQ.10.0)) THEN
        CALL MSGDMP('E','GNSBLK','XB(1).NE.1 OR XB(NB).NE.10.')
      END IF

      NBZ=NB
      CALL VRSET(XB,XBZ,NBZ,1,1)

      RETURN
*-----------------------------------------------------------------------
      ENTRY GNQBLK(XB,NB)

      NB=NBZ
      CALL VRSET(XBZ,XB,NB,1,1)

      RETURN
*-----------------------------------------------------------------------
      ENTRY GNLT(RX,BX,IP)

      IF (RX.EQ.0) THEN
        BX=0
        IP=0
      ELSE
        SX=ABS(RX)
        PS=LOG10(SX)
        IP=IGUS(PS)
        TX=SX*10.0**(-IP)
        IF (RX.GT.0) THEN
          IB=IBLKGT(XBZ,NBZ,TX)
        ELSE
          IB=IBLKLT(XBZ,NBZ,TX)
        END IF
        IBD=IMOD(IB-1,NBZ-1)+1
        IBE=(IB-IBD)/(NBZ-1)
        BX=SIGN(XBZ(IBD),RX)
        IP=IP+IBE
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY GNLE(RX,BX,IP)

      IF (RX.EQ.0) THEN
        BX=0
        IP=0
      ELSE
        SX=ABS(RX)
        PS=LOG10(SX)
        IP=IGUS(PS)
        TX=SX*10.0**(-IP)
        IF (RX.GT.0) THEN
          IB=IBLKGE(XBZ,NBZ,TX)
        ELSE
          IB=IBLKLE(XBZ,NBZ,TX)
        END IF
        IBD=IMOD(IB-1,NBZ-1)+1
        IBE=(IB-IBD)/(NBZ-1)
        BX=SIGN(XBZ(IBD),RX)
        IP=IP+IBE
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY GNGT(RX,BX,IP)

      IF (RX.EQ.0) THEN
        BX=0
        IP=0
      ELSE
        SX=ABS(RX)
        PS=LOG10(SX)
        IP=IGUS(PS)
        TX=SX*10.0**(-IP)
        IF (RX.GT.0) THEN
          IB=IBLKLT(XBZ,NBZ,TX)
        ELSE
          IB=IBLKGT(XBZ,NBZ,TX)
        END IF
        IBD=IMOD(IB-1,NBZ-1)+1
        IBE=(IB-IBD)/(NBZ-1)
        BX=SIGN(XBZ(IBD),RX)
        IP=IP+IBE
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY GNGE(RX,BX,IP)

      IF (RX.EQ.0) THEN
        BX=0
        IP=0
      ELSE
        SX=ABS(RX)
        PS=LOG10(SX)
        IP=IGUS(PS)
        TX=SX*10.0**(-IP)
        IF (RX.GT.0) THEN
          IB=IBLKLE(XBZ,NBZ,TX)
        ELSE
          IB=IBLKGE(XBZ,NBZ,TX)
        END IF
        IBD=IMOD(IB-1,NBZ-1)+1
        IBE=(IB-IBD)/(NBZ-1)
        BX=SIGN(XBZ(IBD),RX)
        IP=IP+IBE
      END IF

      RETURN
      END
