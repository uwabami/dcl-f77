*-----------------------------------------------------------------------
*     RGNLE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      REAL FUNCTION RGNLE(RX)

      EXTERNAL  REXP


      CALL GNLE(RX,BX,IP)
      RGNLE=REXP(BX,10,IP)

      END
