*-----------------------------------------------------------------------
*     RGNLT
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      REAL FUNCTION RGNLT(RX)

      EXTERNAL  REXP


      CALL GNLT(RX,BX,IP)
      RGNLT=REXP(BX,10,IP)

      END
