*-----------------------------------------------------------------------
*     DXILOC
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE DXILOC(ND,NS,NP,NCP)

      INTEGER   NS(*),NP(*)


      MCP=NCP-1
      DO 10 N=1,ND-1
        NP(N)=MOD(MCP,NS(N))+1
        MCP=MCP/NS(N)
   10 CONTINUE
      NP(ND)=MCP+1

      END
