*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      INTEGER FUNCTION IFROMC(CX)

      CHARACTER CX*(*)

      CHARACTER CMSG*80

      EXTERNAL  LENY,LENZ,JFROMC


      LC1=LENY(CX)+1
      LC2=LENZ(CX)

      IF (LC2.EQ.0) THEN
        CMSG='THERE IS NO VALID CHARACTER.'
        CALL MSGDMP('E','IFROMC',CMSG)
      END IF

      IF (CX(LC1:LC1).EQ.'-') THEN
        ISGN=-1
        LC1=LC1+1
      ELSE IF (CX(LC1:LC1).EQ.'+') THEN
        ISGN=+1
        LC1=LC1+1
      ELSE
        ISGN=+1
      END IF

      IFROMC=ISGN*JFROMC(CX(LC1:LC2))

      END
