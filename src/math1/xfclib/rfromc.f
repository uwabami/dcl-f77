*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      REAL FUNCTION RFROMC(CX)

      CHARACTER CX*(*)

      CHARACTER CMSG*80

*     EXTERNAL  LENY,LENZ,INDXMF,IFROMC,FFROMC
      EXTERNAL  LENZ,INDXMF,IFROMC,FFROMC


*     LC1=LENY(CX)+1
      LC2=LENZ(CX)

      IF (LC2.EQ.0) THEN
        CMSG='THERE IS NO VALID CHARACTER.'
        CALL MSGDMP('E','FFROMC',CMSG)
      END IF

      IEXP=INDXMF(CX,LC2,1,'E')

      IF (IEXP.EQ.0) THEN
        RFROMC=FFROMC(CX)
      ELSE
        RFROMC=FFROMC(CX(1:IEXP-1))*10.0**IFROMC(CX(IEXP+1:LC2))
      END IF
          
      END
