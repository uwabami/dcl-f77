*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      INTEGER FUNCTION JFROMC(CX)

      CHARACTER CX*(*)

      CHARACTER CNUM*10,CMSG*80

      INTRINSIC INDEX

      EXTERNAL  LENZ

      SAVE

      DATA      CNUM/'0123456789'/


      LCX=LENZ(CX)

      IF (LCX.EQ.0) THEN
        CMSG='THERE IS NO VALID CHARACTER.'
        CALL MSGDMP('E','JFROMC',CMSG)
      END IF

      JFROMC=0
      IDIG=1
      DO 10 N=LCX,1,-1
        IDX=INDEX(CNUM,CX(N:N))
        IF (IDX.EQ.0) THEN
          CMSG='THERE IS A NON-NUMERAL CHARACTER.'
          CALL MSGDMP('E','JFROMC',CMSG)
        END IF
        JFROMC=JFROMC+(IDX-1)*IDIG
        IDIG=IDIG*10
   10 CONTINUE

      END
