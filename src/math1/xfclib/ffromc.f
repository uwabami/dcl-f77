*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      REAL FUNCTION FFROMC(CX)

      CHARACTER CX*(*)

      CHARACTER CMSG*80,CXX*16

      INTRINSIC INDEX

      EXTERNAL  LENY,LENZ,JFROMC


      LC1=LENY(CX)+1
      LC2=LENZ(CX)

      IF (LC2.EQ.0) THEN
        CMSG='THERE IS NO VALID CHARACTER.'
        CALL MSGDMP('E','FFROMC',CMSG)
      END IF

      IF (CX(LC1:LC1).EQ.'-') THEN
        ISGN=-1
        LC1=LC1+1
      ELSE IF (CX(LC1:LC1).EQ.'+') THEN
        ISGN=+1
        LC1=LC1+1
      ELSE
        ISGN=+1
      END IF

      IDOT=INDEX(CX(1:LC2),'.')

      IF (IDOT.EQ.0) THEN
        CXX=CX(LC1:LC2)
        INX=0
      ELSE
        IF (IDOT.EQ.LC1 .AND. IDOT.EQ.LC2) THEN
          CMSG='THERE EXISTS A PERIOD ONLY.'
          CALL MSGDMP('E','FFROMC',CMSG)
        ELSE IF (IDOT.EQ.LC1) THEN
          CXX=CX(IDOT+1:LC2)
        ELSE IF (IDOT.EQ.LC2) THEN
          CXX=CX(LC1:IDOT-1)
        ELSE
          CXX=CX(LC1:IDOT-1)//CX(IDOT+1:LC2)
        END IF
        INX=IDOT-LC2
      END IF

      LCXX=LENZ(CXX)

   10 IF (CXX(1:1).NE.'0' .OR. LCXX.EQ.1) GO TO 20
        CXX(1:1)=' '
        CALL CLADJ(CXX)
        LCXX=LCXX-1
      GO TO 10
   20 CONTINUE

      IF (LCXX.GT.8) THEN
        CMSG='GIVEN NUMBER IS TRUNCATED WITHIN 8 DIGITS.'
        CALL MSGDMP('W','FFROMC',CMSG)
        INX=INX+(LCXX-8)
        LCXX=8
      END IF

      FFROMC=ISGN*REAL(JFROMC(CXX(1:LCXX)))*10.0**INX
          
      END
