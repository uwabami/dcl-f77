*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      LOGICAL FUNCTION LFROMC(CX)

      CHARACTER CX*(*)

      LOGICAL   LCHREQ
      CHARACTER CMSG*80

      EXTERNAL  LENY,LENZ,LCHREQ


      LC1=LENY(CX)+1
      LC2=LENZ(CX)

      IF (LC2.EQ.0) THEN
        CMSG='THERE IS NO VALID CHARACTER.'
        CALL MSGDMP('E','IFROMC',CMSG)
      END IF

      IF (CX(LC1:LC1).EQ.'.') THEN
        LC1=LC1+1
      END IF

      IF (LCHREQ(CX(LC1:LC1),'T')) THEN
        LFROMC=.TRUE.
      ELSE IF (LCHREQ(CX(LC1:LC1),'F')) THEN
        LFROMC=.FALSE.
      ELSE
        CMSG='THIS IS INVALID LOGICAL EXPRESSION.'
        CALL MSGDMP('E','LFROMC',CMSG)
      END IF

      END
