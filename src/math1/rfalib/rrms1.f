*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      REAL FUNCTION RRMS1(RX,N,JX)

      REAL      RX(*)


      CALL GLRGET('RMISS',RMISS)
      SUM=0
      NN=0
      DO 10 I=1,JX*(N-1)+1,JX
        IF (RX(I).NE.RMISS) THEN
          NN=NN+1
          SUM=SUM+RX(I)*RX(I)
        END IF
   10 CONTINUE
      IF (NN.EQ.0) THEN
        RRMS1=RMISS
      ELSE
        RRMS1=SQRT(SUM/NN)
      END IF

      END
