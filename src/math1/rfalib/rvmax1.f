*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      REAL FUNCTION RVMAX1(RX,NS,NP,NQ,ND)

      INTEGER   NS(*),NP(*),NQ(*)
      REAL      RX(*)

      PARAMETER (MD=10)

      INTEGER   NZX(MD),NAD(MD)
      LOGICAL   LMADA


      IF (.NOT.(1.LE.ND .AND. ND.LE.MD)) THEN
        CALL MSGDMP('E','RVMAX1',
     +       'NUMBER OF DIMENSION SHOULD BE 1<= ND <= 10.')
      END IF

      DO 10 N=1,ND
        IF (.NOT.(1.LE.NP(N) .AND. NP(N).LE.NQ(N)
     +       .AND. NQ(N).LE.NS(N))) THEN
          CALL MSGDMP('E','RVMAX1',
     +         'RELATION SHOULD BE 1 <= NP(N) <= NQ(N) <= NS(N).')
        END IF
   10 CONTINUE

      CALL GLRGET('RMISS',RMISS)

      CALL DXFLOC(ND,NS,NP,N1)
      CALL DXFLOC(ND,NS,NQ,N2)

      LMADA=.TRUE.

      NDX=1
      DO 20 NN=1,ND
        NZX(NN)=NP(NN)
        NAD(NN)=(NS(NN)-(NQ(NN)-NP(NN)+1))*NDX
        NDX=NDX*NS(NN)
   20 CONTINUE

      N=N1
   30 CONTINUE

        DO 40 NN=1,ND
          IF (NZX(NN).GT.NQ(NN)) THEN
            NZX(NN)=NP(NN)
            NZX(NN+1)=NZX(NN+1)+1
            N=N+NAD(NN)
          ELSE
            GO TO 50
          END IF
   40   CONTINUE
   50   CONTINUE

        IF (LMADA) THEN
          IF (RX(N).NE.RMISS) THEN
            RVMAX1=RX(N)
            LMADA=.FALSE.
          END IF
        ELSE
          IF (RX(N).NE.RMISS .AND. RX(N).GT.RVMAX1) THEN
            RVMAX1=RX(N)
          END IF
        END IF

        NZX(1)=NZX(1)+1
        N=N+1

      IF (N.LE.N2) GO TO 30

      IF (LMADA) THEN
        RVMAX1=RMISS
      END IF

      END
