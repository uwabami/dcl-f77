*-----------------------------------------------------------------------
*     RVMIN
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      REAL FUNCTION RVMIN(RX,NS,NP,NQ,ND)

      INTEGER   NS(*),NP(*),NQ(*)
      REAL      RX(*)

      LOGICAL   LMISS

      EXTERNAL  RVMIN0,RVMIN1


      CALL GLLGET('LMISS',LMISS)
      IF (LMISS) THEN
        RVMIN=RVMIN1(RX,NS,NP,NQ,ND)
      ELSE
        RVMIN=RVMIN0(RX,NS,NP,NQ,ND)
      END IF

      END
