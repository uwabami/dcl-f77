*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      REAL FUNCTION RAMP0(RX,N,JX)

      REAL      RX(*)


      SUM=0
      DO 10 I=1,JX*(N-1)+1,JX
        SUM=SUM+RX(I)*RX(I)
   10 CONTINUE
      RAMP0=SQRT(SUM)

      END
