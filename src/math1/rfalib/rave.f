*-----------------------------------------------------------------------
*     RAVE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      REAL FUNCTION RAVE(RX,N,JX)

      REAL      RX(*)

      LOGICAL   LMISS

      EXTERNAL  RAVE0,RAVE1


      CALL GLLGET('LMISS',LMISS)
      IF (LMISS) THEN
        RAVE=RAVE1(RX,N,JX)
      ELSE
        RAVE=RAVE0(RX,N,JX)
      END IF

      END
