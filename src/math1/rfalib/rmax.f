*-----------------------------------------------------------------------
*     RMAX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      REAL FUNCTION RMAX(RX,N,JX)

      REAL      RX(*)

      LOGICAL   LMISS

      EXTERNAL  RMAX0,RMAX1


      CALL GLLGET('LMISS',LMISS)
      IF (LMISS) THEN
        RMAX=RMAX1(RX,N,JX)
      ELSE
        RMAX=RMAX0(RX,N,JX)
      END IF

      END
