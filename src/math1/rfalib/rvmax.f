*-----------------------------------------------------------------------
*     RVMAX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      REAL FUNCTION RVMAX(RX,NS,NP,NQ,ND)

      INTEGER   NS(*),NP(*),NQ(*)
      REAL      RX(*)

      LOGICAL   LMISS

      EXTERNAL  RVMAX0,RVMAX1


      CALL GLLGET('LMISS',LMISS)
      IF (LMISS) THEN
        RVMAX=RVMAX1(RX,NS,NP,NQ,ND)
      ELSE
        RVMAX=RVMAX0(RX,NS,NP,NQ,ND)
      END IF

      END
