*-----------------------------------------------------------------------
*     RSTD
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      REAL FUNCTION RSTD(RX,N,JX)

      REAL      RX(*)

      LOGICAL   LMISS

      EXTERNAL  RSTD0,RSTD1


      CALL GLLGET('LMISS',LMISS)
      IF (LMISS) THEN
        RSTD=RSTD1(RX,N,JX)
      ELSE
        RSTD=RSTD0(RX,N,JX)
      END IF

      END
