*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      REAL FUNCTION RSUM0(RX,N,JX)

      REAL      RX(*)


      SUM=0
      DO 10 I=1,JX*(N-1)+1,JX
        SUM=SUM+RX(I)
   10 CONTINUE
      RSUM0=SUM

      END
