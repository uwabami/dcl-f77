*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      REAL FUNCTION RMAX0(RX,N,JX)

      REAL      RX(*)


      RMAX0=RX(1)
      DO 10 I=1,JX*(N-1)+1,JX
        IF (RX(I).GT.RMAX0) THEN
          RMAX0=RX(I)
        END IF
   10 CONTINUE

      END
