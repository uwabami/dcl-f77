*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      REAL FUNCTION RSTD1(RX,N,JX)

      REAL      RX(*)

      EXTERNAL  RAVE1


      CALL GLRGET('RMISS',RMISS)
      AVE=RAVE1(RX,N,JX)
      SUM=0
      NN=0
      DO 10 I=1,JX*(N-1)+1,JX
        IF (RX(I).NE.RMISS) THEN
          NN=NN+1
          SUM=SUM+(RX(I)-AVE)**2
        END IF
   10 CONTINUE
      IF (NN.EQ.0) THEN
        RSTD1=RMISS
      ELSE
        RSTD1=SQRT(SUM/NN)
      END IF

      END
