*-----------------------------------------------------------------------
*     RRMS
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      REAL FUNCTION RRMS(RX,N,JX)

      REAL      RX(*)

      LOGICAL   LMISS

      EXTERNAL  RRMS0,RRMS1


      CALL GLLGET('LMISS',LMISS)
      IF (LMISS) THEN
        RRMS=RRMS1(RX,N,JX)
      ELSE
        RRMS=RRMS0(RX,N,JX)
      END IF

      END
