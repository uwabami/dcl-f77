*-----------------------------------------------------------------------
*     RSUM
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      REAL FUNCTION RSUM(RX,N,JX)

      REAL      RX(*)

      LOGICAL   LMISS

      EXTERNAL  RSUM0,RSUM1


      CALL GLLGET('LMISS',LMISS)
      IF (LMISS) THEN
        RSUM=RSUM1(RX,N,JX)
      ELSE
        RSUM=RSUM0(RX,N,JX)
      END IF

      END
