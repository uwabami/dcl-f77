*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      REAL FUNCTION RAMP1(RX,N,JX)

      REAL      RX(*)


      CALL GLRGET('RMISS',RMISS)
      SUM=0
      NN=0
      DO 10 I=1,JX*(N-1)+1,JX
        IF (RX(I).NE.RMISS) THEN
          NN=NN+1
          SUM=SUM+RX(I)*RX(I)
        END IF
   10 CONTINUE
      IF (NN.EQ.0) THEN
        RAMP1=RMISS
      ELSE
        RAMP1=SQRT(SUM)
      END IF

      END
