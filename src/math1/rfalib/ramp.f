*-----------------------------------------------------------------------
*     RAMP
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      REAL FUNCTION RAMP(RX,N,JX)

      REAL      RX(*)

      LOGICAL   LMISS

      EXTERNAL  RAMP0,RAMP1


      CALL GLLGET('LMISS',LMISS)
      IF (LMISS) THEN
        RAMP=RAMP1(RX,N,JX)
      ELSE
        RAMP=RAMP0(RX,N,JX)
      END IF

      END
