*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      REAL FUNCTION RMIN1(RX,N,JX)

      REAL      RX(*)

      LOGICAL   MADA


      CALL GLRGET('RMISS',RMISS)
      MADA=.TRUE.
      DO 10 I=1,JX*(N-1)+1,JX
        IF (MADA) THEN
          IF (RX(I).NE.RMISS) THEN
            RMIN1=RX(I)
            MADA=.FALSE.
          END IF
        ELSE
          IF (RX(I).NE.RMISS .AND. RX(I).LT.RMIN1) THEN
            RMIN1=RX(I)
          END IF
        END IF
   10 CONTINUE
      IF (MADA) THEN
        RMIN1=RMISS
      END IF

      END
