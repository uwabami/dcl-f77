*-----------------------------------------------------------------------
*     RMIN
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      REAL FUNCTION RMIN(RX,N,JX)

      REAL      RX(*)

      LOGICAL   LMISS

      EXTERNAL  RMIN0,RMIN1


      CALL GLLGET('LMISS',LMISS)
      IF (LMISS) THEN
        RMIN=RMIN1(RX,N,JX)
      ELSE
        RMIN=RMIN0(RX,N,JX)
      END IF

      END
