*-----------------------------------------------------------------------
*     VIMLT
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE VIMLT(IX,IY,IZ,N,JX,JY,JZ)

      INTEGER   IX(*),IY(*),IZ(*)

      LOGICAL   LMISS


      CALL GLLGET('LMISS',LMISS)
      IF (LMISS) THEN
        CALL VIMLT1(IX,IY,IZ,N,JX,JY,JZ)
      ELSE
        CALL VIMLT0(IX,IY,IZ,N,JX,JY,JZ)
      END IF

      END
