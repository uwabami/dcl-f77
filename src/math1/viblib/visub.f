*-----------------------------------------------------------------------
*     VISUB
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE VISUB(IX,IY,IZ,N,JX,JY,JZ)

      INTEGER   IX(*),IY(*),IZ(*)

      LOGICAL   LMISS


      CALL GLLGET('LMISS',LMISS)
      IF (LMISS) THEN
        CALL VISUB1(IX,IY,IZ,N,JX,JY,JZ)
      ELSE
        CALL VISUB0(IX,IY,IZ,N,JX,JY,JZ)
      END IF

      END
