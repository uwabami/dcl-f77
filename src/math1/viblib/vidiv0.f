*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE VIDIV0(IX,IY,IZ,N,JX,JY,JZ)

      INTEGER   IX(*),IY(*),IZ(*)


      KX=1-JX
      KY=1-JY
      KZ=1-JZ
      DO 10 J=1,N
        KX=KX+JX
        KY=KY+JY
        KZ=KZ+JZ
        IZ(KZ)=IX(KX)/IY(KY)
   10 CONTINUE

      END
