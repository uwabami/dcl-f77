*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE VIDIV1(IX,IY,IZ,N,JX,JY,JZ)

      INTEGER   IX(*),IY(*),IZ(*)


      CALL GLIGET('IMISS',IMISS)
      KX=1-JX
      KY=1-JY
      KZ=1-JZ
      DO 10 J=1,N
        KX=KX+JX
        KY=KY+JY
        KZ=KZ+JZ
        IF (IX(KX).NE.IMISS .AND. IY(KY).NE.IMISS) THEN
          IZ(KZ)=IX(KX)/IY(KY)
        ELSE
          IZ(KZ)=IMISS
        END IF
   10 CONTINUE

      END
