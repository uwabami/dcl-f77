*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE VIFNB0(IX,IY,IZ,N,JX,JY,JZ,IFNB)

      INTEGER   IX(*),IY(*),IZ(*)

      EXTERNAL  IFNB


      KX=1-JX
      KY=1-JY
      KZ=1-JZ
      DO 10 J=1,N
        KX=KX+JX
        KY=KY+JY
        KZ=KZ+JZ
        IZ(KZ)=IFNB(IX(KX),IY(KY))
   10 CONTINUE

      END
