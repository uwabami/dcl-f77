*-----------------------------------------------------------------------
*     VIADD
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE VIADD(IX,IY,IZ,N,JX,JY,JZ)

      INTEGER   IX(*),IY(*),IZ(*)

      LOGICAL   LMISS


      CALL GLLGET('LMISS',LMISS)
      IF (LMISS) THEN
        CALL VIADD1(IX,IY,IZ,N,JX,JY,JZ)
      ELSE
        CALL VIADD0(IX,IY,IZ,N,JX,JY,JZ)
      END IF

      END
