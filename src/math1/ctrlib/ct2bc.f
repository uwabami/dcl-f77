*-----------------------------------------------------------------------
*     COORDINATE TRANSFORMATION (2D BIPOLAR -> CARTESIAN )
*                                              93/02/18   S.SAKAI
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE CT2BC(U, V, X, Y)


      UV = COSH(V)+COS(U)
      IF (UV.NE.0) THEN
        X = SINH(V)/UV
        Y = SIN (U)/UV
      ELSE
        CALL GLRGET('RUNDEF',RUNDEF)
        X = RUNDEF
        Y = RUNDEF
      END IF

      END
