*-----------------------------------------------------------------------
*     COORDINATE ROTATION (3D CARTESIAN)
*                                              93/02/19   S.SAKAI
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE CR3C(THETA, PHI, PSI, X0, Y0, Z0, X1, Y1, Z1)


      CALL CR2C( PHI,   X0, Y0, X1, YT )
      CALL CR2C(-THETA, X1, Z0, XT, Z1 )
      CALL CR2C( PSI,   XT, YT, X1, Y1 )

      END
