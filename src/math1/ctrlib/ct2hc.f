*-----------------------------------------------------------------------
*     COORDINATE TRANSFORMATION (2D HYPERBOLIC -> CARTESIAN )
*                                              93/02/18   S.SAKAI
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE CT2HC(U, V, X, Y)


      UV = SQRT(U*U + V*V)
      IF (V.GT.0) THEN
        X = SQRT( U + UV)/2
      ELSE
        X = - SQRT( U + UV)/2
      END IF
      Y = SQRT(-U + UV)/2

      END
