*-----------------------------------------------------------------------
*     COORDINATE TRANSFORMATION (3D CARTESIAN -> SPHERICAL)
*                                              93/02/18   S.SAKAI
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE CT3CS(X, Y, Z, R, THETA, PHI)

*                  0 < THETA < PI
*                -PI < PHI   < PI


      R     = SQRT(X*X + Y*Y + Z*Z)
      PHI   = ATAN2(Y, X)
      THETA = ATAN2(SQRT(X*X + Y*Y), Z)

      END
