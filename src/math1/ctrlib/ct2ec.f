*-----------------------------------------------------------------------
*     COORDINATE TRANSFORMATION (2D ELLIPTIC -> CARTESIAN )
*                                              93/02/18   S.SAKAI
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE CT2EC(U, V, X, Y)


      X = COSH(U)*COS(V)
      Y = SINH(U)*SIN(V)

      END
