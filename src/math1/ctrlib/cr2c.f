*-----------------------------------------------------------------------
*     COORDINATE ROTATION (2D CARTESIAN)
*                                              93/02/19   S.SAKAI
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE CR2C(THETA, X0, Y0, X1, Y1)


      ST = SIN(THETA)
      CT = COS(THETA)

      X1 =  CT*X0 + ST*Y0
      Y1 = -ST*X0 + CT*Y0

      END
