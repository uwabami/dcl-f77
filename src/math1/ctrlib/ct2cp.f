*-----------------------------------------------------------------------
*     COORDINATE TRANSFORMATION (2D CARTESIAN -> POLAR)
*                                              93/02/18   S.SAKAI
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE CT2CP(X, Y, R, THETA)

*                  0 < THETA < PI
*                -PI < PHI   < PI


      R     = SQRT(X*X + Y*Y)
      THETA = ATAN2(Y, X)

      END
