*-----------------------------------------------------------------------
*     COORDINATE TRANSFORMATION (2D POLAR -> CARTESIAN )
*                                              93/02/18   S.SAKAI
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE CT2PC(R, THETA, X, Y)


      X = R*COS(THETA)
      Y = R*SIN(THETA)

      END
