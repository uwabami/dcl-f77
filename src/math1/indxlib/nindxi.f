*-----------------------------------------------------------------------
*     NINDXI
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      INTEGER FUNCTION NINDXI(IX,N,JD,II)

      INTEGER   IX(*)


      NINDXI=0
      DO 10 I=1,N
        J=JD*(I-1)+1
        IF (IX(J).EQ.II) THEN
          NINDXI=NINDXI+1
        END IF
   10 CONTINUE

      END
