*-----------------------------------------------------------------------
*     INDXIL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      INTEGER FUNCTION INDXIL(IX,N,JD,II)

      INTEGER   IX(*)


      INDXIL=0
      DO 10 I=1,N
        J=JD*(I-1)+1
        IF (IX(J).EQ.II) THEN
          INDXIL=I
        END IF
   10 CONTINUE

      END
