*-----------------------------------------------------------------------
*     INDXCL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      INTEGER FUNCTION INDXCL(CX,N,JD,CH)

      CHARACTER CX(*)*1,CH*1


      INDXCL=0
      DO 10 I=1,N
        J=JD*(I-1)+1
        IF (CX(J).EQ.CH) THEN
          INDXCL=I
        END IF
   10 CONTINUE

      END
