*-----------------------------------------------------------------------
*     NINDXR
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      INTEGER FUNCTION NINDXR(RX,N,JD,RR)

      REAL      RX(*)


      NINDXR=0
      DO 10 I=1,N
        J=JD*(I-1)+1
        IF (RX(J).EQ.RR) THEN
          NINDXR=NINDXR+1
        END IF
   10 CONTINUE

      END
