*-----------------------------------------------------------------------
*     INDXCF
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      INTEGER FUNCTION INDXCF(CX,N,JD,CH)

      CHARACTER CX(*)*1,CH*1


      INDXCF=0
      DO 10 I=1,N
        J=JD*(I-1)+1
        IF (CX(J).EQ.CH) THEN
          INDXCF=I
          RETURN
        END IF
   10 CONTINUE

      END
