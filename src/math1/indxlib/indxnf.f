*-----------------------------------------------------------------------
*     INDXNF
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      INTEGER FUNCTION INDXNF(CX,N,JD,CH)

      CHARACTER CX*(*),CH*(*)


      NC=LEN(CH)
      INDXNF=0
      DO 10 I=1,N
        J1=JD*(I-1)+1
        J2=JD*(I-1)+NC
        IF (CX(J1:J2).EQ.CH) THEN
          INDXNF=I
          RETURN
        END IF
   10 CONTINUE

      END
