*-----------------------------------------------------------------------
*     INDXRL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      INTEGER FUNCTION INDXRL(RX,N,JD,RR)

      REAL      RX(*)


      INDXRL=0
      DO 10 I=1,N
        J=JD*(I-1)+1
        IF (RX(J).EQ.RR) THEN
          INDXRL=I
        END IF
   10 CONTINUE

      END
