*-----------------------------------------------------------------------
*     MAP PROJECTION (MOLLWEIDE-LIKE)                  93/02/20 S.SAKAI
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE MPFMWL(XLON, YLAT, X, Y)

      PARAMETER (A = 1.4142136)

      EXTERNAL  RFPI, XMPLON


      PI = RFPI()
      X = 2*A*XMPLON(XLON)*COS(YLAT)/PI
      Y = A*SIN(YLAT)

      RETURN
*-----------------------------------------------------------------------
      ENTRY MPIMWL(X, Y, XLON, YLAT)

      PI = RFPI()
      IF (ABS(Y) .LT. A) THEN
        YLAT  = ASIN(Y/A)
        XLON = X/A/COS(YLAT)*PI/2
        IF (ABS(XLON) .LE. PI) RETURN
      ELSE IF (ABS(Y) .EQ. A .AND. X .EQ. 0) THEN
        XLON = 0.
        YLAT = Y/A*PI/2
      END IF

      CALL GLRGET('RUNDEF',RNA)
      XLON = RNA
      YLAT = RNA

      END
