*-----------------------------------------------------------------------
*     MAP PROJECTION (KITADA)                          93/02/20 S.SAKAI
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE MPFKTD(XLON, YLAT, X, Y)

      PARAMETER (A = 1.535649, A1 = 0.8660254*A, B=2.960421)

      EXTERNAL  RFPI, XMPLON, MPZKTD


      PI = RFPI()
      ALPHA = YLAT*0.9
      CALL MPZNWT(MPZKTD, YLAT, ALPHA)

      X = A*XMPLON(XLON)*COS(ALPHA)*1.8/PI
      Y = A*SIN(ALPHA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY MPIKTD(X, Y, XLON, YLAT)

      PI = RFPI()
      IF (ABS(Y) .LE. A1) THEN
        ALPHA = ASIN(Y/A)
        XLON  = X/A/COS(ALPHA)/1.8*PI
        IF (ABS(XLON) .LE. PI) THEN
          YLAT = ASIN((2*ALPHA+SIN(2*ALPHA))/B)
          RETURN
        END IF
      END IF

      CALL GLRGET('RUNDEF',RNA)
      XLON = RNA
      YLAT = RNA

      END
