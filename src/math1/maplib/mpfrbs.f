*-----------------------------------------------------------------------
*     MAP PROJECTION (ROBINSON)                     2007-10-21 E. TOYODA
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE MPFRBS(XLON, YLAT, X, Y)

      PARAMETER (EPSL = 1.0E-5)
      EXTERNAL  RFPI, XMPLON

      PI = RFPI()
      X = XMPLON(XLON)
      YY = MAX(-PI * 0.5 + EPSL, MIN(PI * 0.5 - EPSL, YLAT))
      X = X * (9.98668E-01 
     &        -1.06580E-01 * YY ** 2
     &        -1.58451E-01 * YY ** 4
     &        +1.27230E-01 * YY ** 6
     &        -4.67883E-02 * YY ** 8
     &        +4.13844E-03 * YY ** 10
     &        +9.10851E-04 * YY ** 12)
      Y = ( 3.58968E-01 * YY
     &     +1.09118E-02 * YY ** 3
     &     -2.04323E-02 * YY ** 5
     &     +9.40259E-03 * YY ** 7
     &     -2.15423E-03 * YY ** 9 ) * PI

      RETURN
*-----------------------------------------------------------------------
      ENTRY MPIRBS(X, Y, XLON, YLAT)

      RPI = 1.0 / RFPI()
      YY = Y * RPI
      YLAT = 2.73250E+00 * YY
     &      +1.75437E+00 * YY ** 3
     &      -1.75922E+01 * YY ** 5
     &      +6.19335E+01 * YY ** 7
     &      +1.94069E-02 * YY ** 9
     &      +1.02340E-02 * YY ** 11
      XLON = X / (9.98668E-01 
     &        -1.06580E-01 * YLAT ** 2
     &        -1.58451E-01 * YLAT ** 4
     &        +1.27230E-01 * YLAT ** 6
     &        -4.67883E-02 * YLAT ** 8
     &        +4.13844E-03 * YLAT ** 10
     &        +9.10851E-04 * YLAT ** 12)
      IF (ABS(XLON) .LE. PI + 0.1 .AND. ABS(YLAT) .LE. PI/2) RETURN

      CALL GLRGET('RUNDEF',RNA)
      XLON = RNA
      YLAT = RNA

      END
