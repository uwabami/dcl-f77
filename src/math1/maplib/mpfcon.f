*-----------------------------------------------------------------------
*     MAP PROJECTION (CONICAL)                         93/02/20 S.SAKAI
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE MPFCON(XLON, YLAT, X, Y)

      EXTERNAL  RFPI, XMPLON

      SAVE

      TH  = PI/2 - S*YLAT
      R   = TH + CC
      DLM = XMPLON(XLON)*DK

      X =    R*SIN(DLM)
      Y = -S*R*COS(DLM)

      RETURN
*-----------------------------------------------------------------------
      ENTRY MPICON(X, Y, XLON, YLAT)

      R  = SQRT(X*X + Y*Y)

      IF (R.EQ.0) THEN
        XLON = 0.
      ELSE
        XLON = ATAN2(X, -S*Y)/DK
      END IF
      YLAT = S*(PI/2 - R + CC)
      IF (ABS(XLON) .GT. PI .OR. ABS(YLAT) .GT. PI/2) THEN
        XLON = RNA
        YLAT = RNA
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY MPSCON(YLAT0)

      PI = RFPI()
      CALL GLRGET('RUNDEF',RNA)

      S  = SIGN(1., YLAT0)
      TH = PI/2 - ABS(YLAT0)
      DK = COS(TH)
      CC = TAN(TH) - TH

      END
