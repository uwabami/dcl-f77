*-----------------------------------------------------------------------
*     MAP PROJECTION (HAMMER)                          93/02/20 S.SAKAI
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE MPFHMR(XLON, YLAT, X, Y)

      PARAMETER (A = 1.4142136)

      EXTERNAL  XMPLON


      XLM2 = XMPLON(XLON)/2
      CPHI = COS(YLAT)
      CF   = SQRT(1.+CPHI*COS(XLM2))

      X = 2*A*CPHI*SIN(XLM2)/CF
      Y = A*SIN(YLAT)/CF

      RETURN
*-----------------------------------------------------------------------
      ENTRY MPIHMR(X, Y, XLON, YLAT)

      RR  = (X/2/A)**2+(Y/A)**2
      IF (RR .LE. 1.) THEN
        SQA = SQRT(2-RR)
        YY  = SQA*Y/A
        IF (ABS(YY) .LE. 1) THEN
          PHI = ASIN(YY)
          XX  = X*SQA/2/A/COS(PHI)
          IF (ABS(XX) .LE. 1) THEN
            XLON = 2*ASIN(XX)
            YLAT = PHI
            RETURN
          END IF
        END IF
      END IF

      CALL GLRGET('RUNDEF',RNA)
      XLON = RNA
      YLAT = RNA

      END
