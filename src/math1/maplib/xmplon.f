*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      REAL FUNCTION XMPLON(XLON)

      PARAMETER (EPSIL = 1.0E-5)

      LOGICAL   LREQA

      EXTERNAL  RFPI, LREQA


      PI = RFPI()
      RX = ABS(XLON)/PI
      NX = NINT(RX)
      IF (LREQA(RX, REAL(NX), EPSIL)) THEN
        II = NX - 1
      ELSE
        II = INT(RX) - 1 + INT(RX-INT(RX)+1)
      END IF
      IX = SIGN((II+1)/2, INT(XLON))
      XMPLON = XLON - IX*PI*2

      END
