*-----------------------------------------------------------------------
*     MAP PROJECTION (LONGITUDE-LATITUDE)              93/02/20 S.SAKAI
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE MPFCYL(XLON, YLAT, X, Y)

      EXTERNAL  RFPI, XMPLON


      PI = RFPI()
      X = XMPLON(XLON)
      Y = YLAT
      IF (Y.GT. PI/2) Y =  PI/2
      IF (Y.LT.-PI/2) Y = -PI/2

      RETURN
*-----------------------------------------------------------------------
      ENTRY MPICYL(X, Y, XLON, YLAT)

      PI = RFPI()
      XLON = X
      YLAT = Y
      IF (ABS(XLON) .LE. PI .AND. ABS(YLAT) .LE. PI/2) RETURN

      CALL GLRGET('RUNDEF',RNA)
      XLON = RNA
      YLAT = RNA

      END
