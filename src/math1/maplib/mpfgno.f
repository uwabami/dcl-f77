*-----------------------------------------------------------------------
*     MAP PROJECTION (GNOMONIC)                     2007-11-12 E.TOYODA
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE MPFGNO(XLON, YLAT, X, Y)
      PARAMETER (HALFPI = ATAN(1.0) * 2.0)
      PARAMETER (ZMARGN = 1.0E-3)

      TH = MIN(MAX(HALFPI - YLAT, 0.0), HALFPI - ZMARGN)
      R =  TAN(TH)
      X =  R*SIN(XLON)
      Y = -R*COS(XLON)

      RETURN
*-----------------------------------------------------------------------
      ENTRY MPIGNO(X, Y, XLON, YLAT)

      R = SQRT(X*X + Y*Y)
      IF (R .EQ. 0.) THEN
        XLON = 0.
        YLAT = HALFPI
        RETURN
      ELSE
        XLON = ATAN2(X, -Y)
        YLAT = HALFPI - ATAN(R)
        RETURN
      END IF

      END
