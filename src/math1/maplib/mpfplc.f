*-----------------------------------------------------------------------
*     MAP PROJECTION (POLYCONIC)                    2007-10-29 E. TOYODA
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE MPFPLC(XLON, YLAT, X, Y)
      PARAMETER (RADIAN = 3.1415926535897926 / 180.0)
      PARAMETER (GRAUTI = 30)
      PARAMETER (NI = 360 / GRAUTI, NJ = 180 / GRAUTI)
      REAL CX(NI, NJ), CY(NI, NJ)
      LOGICAL LFIRST
      SAVE CX, CY, LFIRST
      EXTERNAL  XMPLON,MPXPLC
      DATA LFIRST /.TRUE./

*-----------------------------------------------------------------------
*---- ENTRY MPFPLC(XLON, YLAT, X, Y)
        XX = XMPLON(XLON)
        CALL MPXPLC(XX, YLAT, X, Y)
      RETURN
*-----------------------------------------------------------------------
      ENTRY MPIPLC(X, Y, XLON, YLAT)
        IF (LFIRST) THEN
          DO 1000, I = 1, NI
            CLON = RADIAN * ((I - 0.5) / NI * 360. - 180.)
            DO 1010, J = 1, NJ
              CLAT = RADIAN * ((J - 0.5) / NJ * 180. - 90.)
              CALL MPXPLC(CLON, CLAT, CX(I, J), CY(I, J))
 1010       CONTINUE
 1000     CONTINUE
          LFIRST = .FALSE.
        ENDIF
        DIST = 1.0E10
        DO 1100, I = 1, NI
          DO 1110, J = 1, NJ
            CDIST = MAX(ABS(CX(I, J) - X), ABS(CY(I, J) - Y))
            IF (CDIST .LT. DIST) THEN
              XLON = RADIAN * ((I - 0.5) / NI * 360. - 180.)
              YLAT = RADIAN * ((J - 0.5) / NJ * 180. - 90.)
              DIST = CDIST
            ENDIF
 1110     CONTINUE
 1100   CONTINUE
        CALL MPNWTN(X, Y, XLON, YLAT, MPXPLC, .TRUE.)
      END

      SUBROUTINE MPXPLC(XLON, YLAT, X, Y)
      PARAMETER (EPSL = 1.0e-4)
      IF (ABS(YLAT) .LT. EPSL) THEN
        X = XLON
        Y = YLAT
      ELSE
        TH = XLON * SIN(YLAT)
        TANZ = 1.0 / TAN(YLAT)
        X = SIN(TH) * TANZ
        Y = YLAT + TANZ * (1.0 - COS(TH))
      ENDIF
      END
