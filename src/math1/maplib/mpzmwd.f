*-----------------------------------------------------------------------
*     FUNCTION OF LATITUDE (MOLLWEID)
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE MPZMWD(PHI, ALPHA, F, DF)

      EXTERNAL  RFPI


      PI = RFPI()
      F  = 2*ALPHA +   SIN(2*ALPHA) - PI*SIN(PHI)
      DF = 2       + 2*COS(2*ALPHA)

      END
