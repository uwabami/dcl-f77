*-----------------------------------------------------------------------
*     MAP PROJECTION (ORTHOGRAPIC)                     93/02/20 S.SAKAI
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE MPFOTG(XLON, YLAT, X, Y)

      LOGICAL   LSAT

      EXTERNAL  XMPLON

      SAVE

      DATA      LSAT / .FALSE. /

      XLM = XMPLON(XLON)
      IF (LSAT) THEN
        R = COS(YLAT)/(1-C*SIN(YLAT))
      ELSE
        R = COS(YLAT)
      END IF

      X =  R*SIN(XLM)
      Y = -R*COS(XLM)

      RETURN
*-----------------------------------------------------------------------
      ENTRY MPIOTG(X, Y, XLON, YLAT)

      IF (LSAT) THEN
        RR = X*X + Y*Y
        R1 = SQRT(RR)
        R2 = SQRT(RR / (1+C*C*RR))
        IF (R2.LE.1) THEN
          YLAT = ACOS(R2)+ATAN(C*R1)
          IF (R1.EQ.0) THEN
            XLON = 0.
            RETURN
          ELSE
            XLON = ATAN2(X,-Y)
            RETURN
          END IF
       END IF
      ELSE
        R = SQRT(X*X + Y*Y)
        IF (R.EQ.0) THEN
          XLON = 0.
          YLAT = ACOS(R)
          RETURN
        ELSE IF (R.LE.1) THEN
          XLON = ATAN2(X,-Y)
          YLAT = ACOS(R)
          RETURN
        END IF
      END IF

      CALL GLRGET('RUNDEF',RNA)
      XLON = RNA
      YLAT = RNA

      RETURN
*-----------------------------------------------------------------------
      ENTRY MPSOTG(R0)

      LSAT = R0 .GT. 1
      IF (LSAT) THEN
        C = 1./R0
      END IF
      
      END
