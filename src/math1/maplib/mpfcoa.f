*-----------------------------------------------------------------------
*     MAP PROJECTION (LAMBERT CONICAL 1)               93/02/20 S.SAKAI
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE MPFCOA(XLON, YLAT, X, Y)

      EXTERNAL  RFPI, XMPLON

      SAVE


      TH  = PI/2 - S*YLAT
      R   = CK*SIN(TH/2)
      DLM = XMPLON(XLON)*DK

      X =    R*SIN(DLM)
      Y = -S*R*COS(DLM)

      RETURN
*-----------------------------------------------------------------------
      ENTRY MPICOA(X, Y, XLON, YLAT)

      R = SQRT(X*X + Y*Y)
      IF ( R.EQ. 0) THEN
        XLON = 0.
      ELSE
        XLON = ATAN2(X, -S*Y)/DK
      END IF
      IF (ABS(XLON) .LE. PI) THEN
        RC = R/CK
        IF (RC .LE. 1) THEN
          YLAT = S*(PI/2 - 2*ASIN(RC))
          RETURN
        END IF
      END IF
      XLON = RNA
      YLAT = RNA

      RETURN
*-----------------------------------------------------------------------
      ENTRY MPSCOA(YLAT0)

      PI = RFPI()
      CALL GLRGET('RUNDEF',RNA)

      S  = SIGN(1., YLAT0)
      BK = COS((PI/2 - ABS(YLAT0)) / 2)
      CK = 2/BK
      DK = BK*BK

      END
