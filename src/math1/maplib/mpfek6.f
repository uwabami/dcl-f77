*-----------------------------------------------------------------------
*     MAP PROJECTION (ECKERT NO.6)                     93/02/20 S.SAKAI
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE MPFEK6(XLON, YLAT, X, Y)

      PARAMETER (A = 0.8820255)

      EXTERNAL  RFPI, XMPLON, MPZEK6


      PI = RFPI()
      ALPHA = YLAT
      CALL MPZNWT(MPZEK6, YLAT, ALPHA)

      X = A*XMPLON(XLON)*(1+COS(ALPHA))/2
      Y = A*ALPHA

      RETURN
*-----------------------------------------------------------------------
      ENTRY MPIEK6(X, Y, XLON, YLAT)

      PI = RFPI()
      B  = (PI+2)/2
      ALPHA = Y/A
      IF (ABS(ALPHA) .LE. PI/2) THEN
        PHI  = ASIN((ALPHA+SIN(ALPHA))/B)
        XLON = 2*X/A/(1+COS(ALPHA))
        IF (ABS(XLON) .LE. PI) THEN
          YLAT = PHI
          RETURN
        END IF
      END IF

      CALL GLRGET('RUNDEF',RNA)
      XLON = RNA
      YLAT = RNA

      END
