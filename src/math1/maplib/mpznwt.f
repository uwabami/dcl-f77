*-----------------------------------------------------------------------
*     NEWTON METHOD
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE MPZNWT(FUNC, PHI, ALPHA)

      PARAMETER (IMAX = 20, EPS0 = 1.E-4, EPS1 = 1.2E-5)

      EXTERNAL  FUNC


      DO 10 I=1, IMAX
        CALL FUNC(PHI, ALPHA, F, DF)
        IF (ABS(DF) .LT. EPS1) RETURN
        DX = F/DF
        ALPHA = ALPHA - DX
        IF (ABS(DX) .LT. EPS0) RETURN
   10 CONTINUE
      CALL MSGDMP('E', 'MPZNWT', 'EXCEEDING MAXIMUM ITERATIONS.')

      END
