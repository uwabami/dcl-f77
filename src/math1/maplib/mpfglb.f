*-----------------------------------------------------------------------
*     MAP PROJECTION (GLOBULAR)                     2007-11-24 E. TOYODA
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE MPFGLB(XLON, YLAT, X, Y)

      PARAMETER (EPSL = 1.0E-5)
      PARAMETER (HALFPI = ATAN(1.0) * 2.0)
      EXTERNAL  XMPLON

      XX = XMPLON(XLON)
      Y = SIN(YLAT)
      X = COS(YLAT) * XX / HALFPI
      RETURN
*-----------------------------------------------------------------------
      ENTRY MPIGLB(X, Y, XLON, YLAT)

      IF (X**2 + Y**2 .GT. 1.0) GOTO 999
      YLAT = ASIN(Y)
      COSLAT = COS(YLAT)
      IF (ABS(COSLAT) .GT. EPSL) THEN
        XLON = X * HALFPI / COSLAT
      ELSE
        XLON = 0.0
      ENDIF
      RETURN

  999 CONTINUE
      CALL GLRGET('RUNDEF',RNA)
      XLON = RNA
      YLAT = RNA

      END
