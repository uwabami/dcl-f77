*-----------------------------------------------------------------------
*     MAP PROJECTION (MERCATOR)                        93/02/20 S.SAKAI
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE MPFMER(XLON, YLAT, X, Y)

      PARAMETER (EPSL = 1.E-5)

      EXTERNAL  RFPI, XMPLON


      PI = RFPI()
      THETA = (YLAT+PI/2)/2.
      IF (THETA .LT. EPSL)      THETA = EPSL
      IF (THETA .GT. PI/2-EPSL) THETA = PI/2 - EPSL

      X = XMPLON(XLON)
      Y = LOG(TAN(THETA))

      RETURN
*-----------------------------------------------------------------------
      ENTRY MPIMER(X, Y, XLON, YLAT)

      PI = RFPI()
      XLON = X
      IF (ABS(XLON) .LE. PI) THEN
        YLAT = 2*ATAN(EXP(Y)) - PI/2
        RETURN
      END IF

      CALL GLRGET('RUNDEF',RNA)
      XLON = RNA
      YLAT = RNA

      END
