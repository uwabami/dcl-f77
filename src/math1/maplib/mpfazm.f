*-----------------------------------------------------------------------
*     MAP PROJECTION (AZIMUTHAL EQUIDISTANT)           93/02/20 S.SAKAI
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE MPFAZM(XLON, YLAT, X, Y)

      EXTERNAL  RFPI


      PI = RFPI()
      R = PI/2 - YLAT
      IF (R .GT. PI) R = PI
      IF (R .LT. 0.) R = 0

      X =  R*SIN(XLON)
      Y = -R*COS(XLON)

      RETURN
*-----------------------------------------------------------------------
      ENTRY MPIAZM(X, Y, XLON, YLAT)

      PI = RFPI()
      R = SQRT(X*X + Y*Y)

      IF (R .EQ. 0.) THEN
        XLON = 0.
        YLAT = PI/2.
        RETURN
      ELSE IF (R .LE. PI) THEN
        XLON = ATAN2(X, -Y)
        YLAT = PI/2 - R
        RETURN
      END IF

      CALL GLRGET('RUNDEF',RNA)
      XLON = RNA
      YLAT = RNA

      END
