*-----------------------------------------------------------------------
*     FUNCTION OF LATITUDE (ECKERT NO.6)
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE MPZEK6(PHI, ALPHA, F, DF)

      EXTERNAL  RFPI


      PI = RFPI()
      B = (PI+2)/2

      F  = ALPHA + SIN(ALPHA) - B*SIN(PHI)
      DF = 1     + COS(ALPHA)

      END
