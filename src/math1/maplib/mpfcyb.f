*-----------------------------------------------------------------------
*     MAP PROJECTION (BRAUN PERSPECTIVE CYLINDRICAL) 2007-10-14 E. TOYODA
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE MPFCYB(XLON, YLAT, X, Y)

      PARAMETER (EPSL = 1.0E-5)
      EXTERNAL  RFPI, XMPLON
      SAVE C
      DATA C /1.0/

        PI = RFPI()
        X = XMPLON(XLON)
        Y = 2.0 * C * TAN(YLAT * 0.5)

      RETURN
*-----------------------------------------------------------------------
      ENTRY MPICYB(X, Y, XLON, YLAT)

        PI = RFPI()
        XLON = X
        YLAT = ATAN(Y * 0.5 / C) * 2.0
        IF (ABS(XLON) .LE. PI) RETURN

        CALL GLRGET('RUNDEF',RNA)
        XLON = RNA
        YLAT = RNA
      RETURN
*-----------------------------------------------------------------------
      ENTRY MPSCYB(STDLAT)

        C = COS(STDLAT)

      END
