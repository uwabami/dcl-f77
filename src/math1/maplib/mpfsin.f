*-----------------------------------------------------------------------
*     MAP PROJECTION (SINUSOIDAL)                   2007-10-29 E. TOYODA
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE MPFSIN(XLON, YLAT, X, Y)

      PARAMETER (EPSL = 1.0e-5)
      EXTERNAL  RFPI, XMPLON

      PI = RFPI()
      X = XMPLON(XLON)
      Y = MAX(-PI * 0.5, MIN(PI * 0.5, YLAT))
      X = X * COS(Y)

      RETURN
*-----------------------------------------------------------------------
      ENTRY MPISIN(X, Y, XLON, YLAT)

      YLAT = Y
      COSY = COS(Y)
      IF (COSY .LE. EPSL) THEN
        X = 0.0
      ELSE
        X = X / COSY
      ENDIF
      IF (ABS(XLON) .LE. PI + 0.1 .AND. ABS(YLAT) .LE. PI/2) RETURN

      CALL GLRGET('RUNDEF',RNA)
      XLON = RNA
      YLAT = RNA

      END
