*-----------------------------------------------------------------------
*     IRGE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      INTEGER FUNCTION IRGE(RX)

      LOGICAL   LREQ

      EXTERNAL  LREQ


      NX=NINT(RX)
      IF (LREQ(RX,REAL(NX))) THEN
        IRGE=NX
      ELSE
        IRGE=INT(RX)+INT(RX-INT(RX)+1)
      END IF

      END
