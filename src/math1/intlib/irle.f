*-----------------------------------------------------------------------
*     IRLE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      INTEGER FUNCTION IRLE(RX)

      LOGICAL   LREQ

      EXTERNAL  LREQ


      NX=NINT(RX)
      IF (LREQ(RX,REAL(NX))) THEN
        IRLE=NX
      ELSE
        IRLE=INT(RX)-1+INT(RX-INT(RX)+1)
      END IF

      END
