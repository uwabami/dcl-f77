*-----------------------------------------------------------------------
*     IRLT
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      INTEGER FUNCTION IRLT(RX)

      LOGICAL   LREQ

      EXTERNAL  LREQ


      NX=NINT(RX)
      IF (LREQ(RX,REAL(NX))) THEN
        IRLT=NX-1
      ELSE
        IRLT=INT(RX)-1+INT(RX-INT(RX)+1)
      END IF

      END
