*-----------------------------------------------------------------------
*     LCHREQ FOR ASCII CHARACTER CODE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      LOGICAL FUNCTION LCHREQ(CH1,CH2)

      CHARACTER CH1*(*),CH2*(*)


      LCHREQ=.TRUE.
      LCH1=LEN(CH1)
      LCH2=LEN(CH2)
      LCHMAX=MAX(LCH1,LCH2)
      LCHMIN=MIN(LCH1,LCH2)

      DO 10 I=1,LCHMIN
        IDX1=ICHAR(CH1(I:I))
        IDX2=ICHAR(CH2(I:I))
        IF (IDX1.NE.IDX2) THEN
          IF (65.LE.IDX1 .AND. IDX1.LE.90) THEN
            LCHREQ=(IDX2-IDX1).EQ.32
          ELSE IF (97.LE.IDX1 .AND. IDX1.LE.122) THEN
            LCHREQ=(IDX1-IDX2).EQ.32
          ELSE
            LCHREQ=.FALSE.
          END IF
          IF (.NOT.LCHREQ) RETURN
        END IF
   10 CONTINUE
      IF (LCH1.EQ.LCH2) RETURN

      IBLK=ICHAR(' ')
      IF (LCH1.GT.LCH2) THEN
        DO 20 I=LCHMIN+1,LCHMAX
          LCHREQ=ICHAR(CH1(I:I)).EQ.IBLK
          IF (.NOT.LCHREQ) RETURN
   20   CONTINUE
      ELSE
        DO 30 I=LCHMIN+1,LCHMAX
          LCHREQ=ICHAR(CH2(I:I)).EQ.IBLK
          IF (.NOT.LCHREQ) RETURN
   30   CONTINUE
      END IF

      END
