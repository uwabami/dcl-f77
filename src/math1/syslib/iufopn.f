*-----------------------------------------------------------------------
*     IUFOPN
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      INTEGER FUNCTION IUFOPN()

      PARAMETER (IUMIN=1,IUMAX=99)

      LOGICAL   LEXIST,LOPEN


      DO 10 IU=IUMIN,IUMAX
        INQUIRE(UNIT=IU,EXIST=LEXIST,OPENED=LOPEN)
*       / THE FOLLOWING 1 LINE IS JUST FOR CONVENIENCE /
        LEXIST=.TRUE.
        IF (LEXIST .AND. .NOT.LOPEN) THEN
          IUFOPN=IU
          RETURN
        END IF
   10 CONTINUE

      CALL MSGDMP('E','IUFOPN','THERE IS NO UNIT TO BE OPENED.')

      END
