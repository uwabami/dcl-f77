*-----------------------------------------------------------------------
*     GET EXTERNAL PARAMETERS
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE RPNGET

      INTEGER   IPARA(*)
      LOGICAL   LPARA(*)
      REAL      RPARA(*)
      CHARACTER CPARA(*)*80
      CHARACTER CPFIX*(*), CPS(*)*8, CPL(*)*40

      LOGICAL   LCHREQ, LFIRST, LGLC

      EXTERNAL  LCHREQ

      DATA      LFIRST / .TRUE. /

      RETURN
*-----------------------------------------------------------------------
*     GET INTEGER PARAMETERS WITH SHORT NAME
*-----------------------------------------------------------------------
      ENTRY RTIGET(CPFIX, CPS, IPARA, MAX)

      IF (LFIRST) THEN
        CALL RPNXFL
        CALL RPNENV
        CALL RPNOPT
        LFIRST = .FALSE.
      END IF

      DO 10 N = 1, MAX
        CALL RTIXFL(CPFIX, CPS(N), IPARA(N))
        CALL RTIENV(CPFIX, CPS(N), IPARA(N))
        CALL RTIOPT(CPFIX, CPS(N), IPARA(N))
   10 CONTINUE

      RETURN
*-----------------------------------------------------------------------
*     GET LOGICAL PARAMETERS WITH SHORT NAME
*-----------------------------------------------------------------------
      ENTRY RTLGET(CPFIX, CPS, LPARA, MAX)

      IF (LFIRST) THEN
        CALL RPNXFL
        CALL RPNENV
        CALL RPNOPT
        LFIRST = .FALSE.
      END IF

      DO 20 N = 1, MAX
        CALL RTLXFL(CPFIX, CPS(N), LPARA(N))
        CALL RTLENV(CPFIX, CPS(N), LPARA(N))
        CALL RTLOPT(CPFIX, CPS(N), LPARA(N))
   20 CONTINUE

      RETURN
*-----------------------------------------------------------------------
*     GET REAL PARAMETERS WITH SHORT NAME
*-----------------------------------------------------------------------
      ENTRY RTRGET(CPFIX, CPS, RPARA, MAX)

      IF (LFIRST) THEN
        CALL RPNXFL
        CALL RPNENV
        CALL RPNOPT
        LFIRST = .FALSE.
      END IF

      DO 30 N = 1, MAX
        CALL RTRXFL(CPFIX, CPS(N), RPARA(N))
        CALL RTRENV(CPFIX, CPS(N), RPARA(N))
        CALL RTROPT(CPFIX, CPS(N), RPARA(N))
   30 CONTINUE

      RETURN
*-----------------------------------------------------------------------
*     GET CHARACTER PARAMETERS WITH SHORT NAME
*-----------------------------------------------------------------------
      ENTRY RTCGET(CPFIX, CPS, CPARA, MAX)

      LGLC = CPFIX.EQ.'GL'

      IF (LFIRST) THEN
        IF (.NOT.LGLC) THEN
          CALL RPNXFL
          LFIRST = .FALSE.
        END IF
        CALL RPNENV
        CALL RPNOPT
      END IF

      DO 40 N = 1, MAX
        IF (.NOT.LGLC) CALL RTCXFL(CPFIX, CPS(N), CPARA(N))
        CALL RTCENV(CPFIX, CPS(N), CPARA(N))
        CALL RTCOPT(CPFIX, CPS(N), CPARA(N))
   40 CONTINUE

      RETURN
*-----------------------------------------------------------------------
*     GET INTEGER PARAMETERS WITH LONG NAME
*-----------------------------------------------------------------------
      ENTRY RLIGET(CPL, IPARA, MAX)

      IF (LFIRST) THEN
        CALL RPNXFL
        CALL RPNENV
        CALL RPNOPT
        LFIRST = .FALSE.
      END IF

      DO 50 N = 1, MAX
        CALL RLIXFL(CPL(N), IPARA(N))
        CALL RLIENV(CPL(N), IPARA(N))
        CALL RLIOPT(CPL(N), IPARA(N))
   50 CONTINUE

      RETURN
*-----------------------------------------------------------------------
*     GET LOGICAL PARAMETERS WITH LONG NAME
*-----------------------------------------------------------------------
      ENTRY RLLGET(CPL, LPARA, MAX)

      IF (LFIRST) THEN
        CALL RPNXFL
        CALL RPNENV
        CALL RPNOPT
        LFIRST = .FALSE.
      END IF

      DO 60 N = 1, MAX
        CALL RLLXFL(CPL(N), LPARA(N))
        CALL RLLENV(CPL(N), LPARA(N))
        CALL RLLOPT(CPL(N), LPARA(N))
   60 CONTINUE

      RETURN
*-----------------------------------------------------------------------
*     GET REAL PARAMETERS WITH LONG NAME
*-----------------------------------------------------------------------
      ENTRY RLRGET(CPL, RPARA, MAX)

      IF (LFIRST) THEN
        CALL RPNXFL
        CALL RPNENV
        CALL RPNOPT
        LFIRST = .FALSE.
      END IF

      DO 70 N = 1, MAX
        CALL RLRXFL(CPL(N), RPARA(N))
        CALL RLRENV(CPL(N), RPARA(N))
        CALL RLROPT(CPL(N), RPARA(N))
   70 CONTINUE

      RETURN
*-----------------------------------------------------------------------
*     GET CHARACTER PARAMETERS WITH LONG NAME
*-----------------------------------------------------------------------
      ENTRY RLCGET(CPL, CPARA, MAX)

      LGLC = LCHREQ(CPL(1)(1:LENC(CPL(1))),'CONFIG_FILE')

      IF (LFIRST) THEN
        IF (.NOT.LGLC) THEN
          CALL RPNXFL
          LFIRST = .FALSE.
        END IF
        CALL RPNENV
        CALL RPNOPT
      END IF

      DO 80 N = 1, MAX
        IF (.NOT.LGLC) CALL RLCXFL(CPL(N), CPARA(N))
        CALL RLCENV(CPL(N), CPARA(N))
        CALL RLCOPT(CPL(N), CPARA(N))
   80 CONTINUE

      RETURN
      END
