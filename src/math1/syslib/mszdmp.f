*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE MSZDMP(CMSG,IUNIT,LNSIZE)

      CHARACTER CMSG*(*)

      PARAMETER (MMAX=200)

      CHARACTER CMSGX*(MMAX),CFM*12

      EXTERNAL  LENC


      CMSGX=CMSG
      NMSG=LENC(CMSGX)
      MMSG=(NMSG-1)/LNSIZE+1
      CFM='(TR1,A###)'
      WRITE(CFM(7:9),'(I3)') LNSIZE

      DO 10 M=1,MMSG
        WRITE(IUNIT,CFM) CMSGX(1+LNSIZE*(M-1):MIN(LNSIZE*M,MMAX))
   10 CONTINUE

      END
