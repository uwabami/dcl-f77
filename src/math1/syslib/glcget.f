*-----------------------------------------------------------------------
*     GLCGET / GLCSET / GLCSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE GLCGET(CP,CPARA)

      CHARACTER CP*(*),CPARA*(*)

      CHARACTER CX*40,CPVAL*80


      CALL GLCQID(CP,IDX)
      CALL GLCQVL(IDX,CPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY GLCSET(CP,CPARA)

      CALL GLCQID(CP,IDX)
      CALL GLCSVL(IDX,CPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY GLCSTX(CP,CPARA)

      CPVAL=CPARA
      CALL GLCQID(CX,IDX)

*     / SHORT NAME /

      CALL GLCQCP(IDX, CX)
      CALL RTCGET('GL',CX,CPVAL,1)

*     / LONG NAME /

      CALL GLCQCL(IDX, CX)
      CALL RLCGET(CX,CPVAL,1)

      CALL GLCSVL(IDX,CPVAL)

      RETURN
      END
