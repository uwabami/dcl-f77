*-----------------------------------------------------------------------
*     GLRGET / GLRSET / GLRSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE GLRGET(CP, RPARA)

      CHARACTER CP*(*)

      CHARACTER CX*40


      CALL GLRQID(CP, IDX)
      CALL GLRQVL(IDX, RPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY GLRSET(CP, RPARA)

      CALL GLRQID(CP, IDX)
      CALL GLRSVL(IDX, RPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY GLRSTX(CP, RPARA)

      RP = RPARA
      CALL GLRQID(CP, IDX)

*     / SHORT NAME /

      CALL GLRQCP(IDX, CX)
      CALL RTRGET('GL', CX, RP, 1)

*     / LONG NAME /

      CALL GLRQCL(IDX, CX)
      CALL RLRGET(CX, RP, 1)

      CALL GLRSVL(IDX, RP)

      RETURN
      END
