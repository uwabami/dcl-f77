*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE GLQFNM(CPARA,CFNAME)

      CHARACTER CPARA*(*),CFNAME*(*)

      PARAMETER (MAXP=3,MAXF=1)

      CHARACTER CPLIST(MAXP)*1024,CFLIST(MAXF)*1024


      CPLIST(1)=' '
      CALL GLCGET('DUPATH',CPLIST(2))
      CALL GLCGET('DSPATH',CPLIST(3))
      CALL GLCGET(CPARA,CFLIST(1))

      CALL CFSRCH(CPLIST,MAXP,CFLIST,MAXF,CFNAME)

      END
