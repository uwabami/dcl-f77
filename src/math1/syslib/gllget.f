*-----------------------------------------------------------------------
*     GLLGET / GLLSET / GLLSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE GLLGET(CP, LPARA)

      LOGICAL   LPARA
      CHARACTER CP*(*)

      LOGICAL   LP
      CHARACTER CX*40


      CALL GLLQID(CP, IDX)
      CALL GLLQVL(IDX, LPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY GLLSET(CP, LPARA)

      CALL GLLQID(CP, IDX)
      CALL GLLSVL(IDX, LPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY GLLSTX(CP, LPARA)

      LP = LPARA
      CALL GLLQID(CP, IDX)

*     / SHORT NAME /

      CALL GLLQCP(IDX, CX)
      CALL RTLGET('GL', CX, LP, 1)

*     / LONG NAME /

      CALL GLLQCL(IDX, CX)
      CALL RLLGET(CX, LP, 1)

      CALL GLLSVL(IDX, LP)

      RETURN
      END
