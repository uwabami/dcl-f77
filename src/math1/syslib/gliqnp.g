*-----------------------------------------------------------------------
*     INTEGER PARAMETER CONTROL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE GLIQNP(NCP)

      CHARACTER CP*(*)

      PARAMETER (NPARA = 12)

      INTEGER   IX(NPARA)
      LOGICAL   LW(NPARA), LCHREQ, LFIRST
      CHARACTER CPARAS(NPARA)*8
      CHARACTER CPARAL(NPARA)*40
      CHARACTER CMSG*80

      EXTERNAL  LCHREQ, LENC

      SAVE

*     / SHORT NAME /

      DATA CPARAS( 1) / 'IMISS   ' /, IX( 1) /  999 /
      DATA CPARAS( 2) / 'IUNDEF  ' /, IX( 2) / -999 /
      DATA CPARAS( 3) / 'INTMAX  ' /, IX( 3) / @INTMAX /
      DATA CPARAS( 4) / 'NBITSPW ' /, IX( 4) / 32 /
      DATA CPARAS( 5) / 'NCHRSPW ' /, IX( 5) / 4 /
      DATA CPARAS( 6) / 'IIUNIT  ' /, IX( 6) / 5 /
      DATA CPARAS( 7) / 'IOUNIT  ' /, IX( 7) / 6 /
      DATA CPARAS( 8) / 'MSGUNIT ' /, IX( 8) / 0 /
      DATA CPARAS( 9) / 'MAXMSG  ' /, IX( 9) / 20 /
      DATA CPARAS(10) / 'MSGLEV  ' /, IX(10) / 0 /
      DATA CPARAS(11) / 'NLNSIZE ' /, IX(11) / 78 /
      DATA CPARAS(12) / 'SCKPORT ' /, IX(12) / 57010 /

*     / LONG NAME /

      DATA CPARAL( 1) / 'MISSING_INT        ' /, LW( 1) / .TRUE. /
      DATA CPARAL( 2) / 'UNDEFINED_INT      ' /, LW( 2) / .TRUE. /
      DATA CPARAL( 3) / 'MAX_INT            ' /, LW( 3) / .FALSE. /
      DATA CPARAL( 4) / 'WORD_LENGTH_IN_BIT ' /, LW( 4) / .FALSE. /
      DATA CPARAL( 5) / 'WORD_LENGTH_IN_CHAR' /, LW( 5) / .FALSE. /
      DATA CPARAL( 6) / 'INPUT_UNIT         ' /, LW( 6) / .TRUE. /
      DATA CPARAL( 7) / 'OUTPUT_UNIT        ' /, LW( 7) / .TRUE. /
      DATA CPARAL( 8) / 'MESSAGE_UNIT       ' /, LW( 8) / .TRUE. /
      DATA CPARAL( 9) / 'MAX_MESSAGE_NUMBER ' /, LW( 9) / .TRUE. /
      DATA CPARAL(10) / 'MESSAGE_LEVEL      ' /, LW(10) / .TRUE. /
      DATA CPARAL(11) / 'LINE_SIZE          ' /, LW(11) / .TRUE. /
      DATA CPARAL(12) / 'SOCKET_PORT        ' /, LW(12) / .TRUE. /

      DATA      LFIRST / .TRUE. /


      NCP = NPARA

      RETURN
*-----------------------------------------------------------------------
      ENTRY GLIQID(CP, IDX)

      DO 10 N = 1, NPARA
        IF (LCHREQ(CP,CPARAS(N)) .OR.  LCHREQ(CP,CPARAL(N))) THEN
          IDX = N
          RETURN
        END IF
   10 CONTINUE
      CMSG = 'PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E', 'GLIQID', CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY GLIQCP(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAS(IDX)
      ELSE
        CALL MSGDMP('E', 'GLIQCP', 'IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY GLIQCL(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAL(IDX)
      ELSE
        CALL MSGDMP('E', 'GLIQCL', 'IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY GLIQVL(IDX, IPARA)

      IF (LFIRST) THEN
        CALL RTIGET('GL', CPARAS, IX, NPARA)
        CALL RLIGET(CPARAL, IX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        IPARA = IX(IDX)
      ELSE
        CALL MSGDMP('E','GLIQVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY GLISVL(IDX, IPARA)

      IF (LFIRST) THEN
        CALL RTIGET('GL', CPARAS, IX, NPARA)
        CALL RLIGET(CPARAL, IX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        IF (LW(IDX)) THEN
          IX(IDX) = IPARA
          RETURN
        ELSE
          CMSG = 'PARAMETER'''//CPARAS(IDX)//''' CANNOT BE SET.'
          CALL MSGDMP('E', 'GLIQVL', CMSG)
        END IF
      ELSE
        CALL MSGDMP('E','GLIQVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY GLIQIN(CP, IN)

      DO 20 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR.  LCHREQ(CP, CPARAL(N))) THEN
          IN = N
          RETURN
        END IF
   20 CONTINUE

      IN = 0

      RETURN
      END

