*-----------------------------------------------------------------------
*     LOGICAL PARAMETER CONTROL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE GLLQNP(NCP)

      LOGICAL   LPARA
      CHARACTER CP*(*)

      PARAMETER (NPARA = 3)

      LOGICAL   LX(NPARA), LW(NPARA), LCHREQ, LFIRST
      CHARACTER CPARAS(NPARA)*8
      CHARACTER CPARAL(NPARA)*40
      CHARACTER CMSG*80

      EXTERNAL  LCHREQ, LENC

      SAVE

*     / SHORT NAME /

      DATA CPARAS(1) / 'LMISS   ' /, LX( 1) / .FALSE. /
      DATA CPARAS(2) / 'LEPSL   ' /, LX( 2) / .FALSE. /
      DATA CPARAS(3) / 'LLMSG   ' /, LX( 3) / .FALSE. /

*     / LONG NAME /

      DATA CPARAL(1) / 'INTERPRET_MISSING_VALUE' /, LW( 1) / .TRUE. /
      DATA CPARAL(2) / 'INTERPRET_TRUNCATION   ' /, LW( 2) / .TRUE. /
      DATA CPARAL(3) / 'ENABLE_LONG_MESSAGE    ' /, LW( 3) / .TRUE. /

      DATA      LFIRST / .TRUE. /


      NCP = NPARA

      RETURN
*-----------------------------------------------------------------------
      ENTRY GLLQID(CP, IDX)

      DO 10 N = 1, NPARA
        IF (LCHREQ(CP,CPARAS(N)) .OR. LCHREQ(CP,CPARAL(N))) THEN
          IDX = N
          RETURN
        END IF
   10 CONTINUE
      CMSG = 'PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E', 'GLLQID', CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY GLLQCP(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAS(IDX)
      ELSE
        CALL MSGDMP('E', 'GLLQCP', 'IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY GLLQCL(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAL(IDX)
      ELSE
        CALL MSGDMP('E', 'GLLQCL', 'IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY GLLQVL(IDX, LPARA)

      IF (LFIRST) THEN
        CALL RTLGET('GL', CPARAS, LX, NPARA)
        CALL RLLGET(CPARAL, LX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        LPARA = LX(IDX)
      ELSE
        CALL MSGDMP('E', 'GLLQVL', 'IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY GLLSVL(IDX, LPARA)

      IF (LFIRST) THEN
        CALL RTLGET('GL', CPARAS, LX, NPARA)
        CALL RLLGET(CPARAL, LX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        IF (LW(IDX)) THEN
          LX(IDX) = LPARA
          RETURN
        ELSE
          CMSG = 'PARAMETER'''//CPARAS(IDX)//''' CANNOT BE SET.'
          CALL MSGDMP('E', 'GLLQVL', CMSG)
        END IF
      ELSE
        CALL MSGDMP('E', 'GLLQVL', 'IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY GLLQIN(CP, IN)

      DO 20 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IN = N
          RETURN
        END IF
   20 CONTINUE

      IN = 0

      RETURN
      END
