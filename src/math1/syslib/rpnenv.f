*-----------------------------------------------------------------------
*     GET EXTERNAL PARAMETER FROM ENVIRONMENTAL VARIABLE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE RPNENV

      LOGICAL   LPARA
      CHARACTER CPFIX*(*), CP*(*), CPARA*(*)

      LOGICAL   LFROMC
      CHARACTER CARG*80, CPX*16, CSEP*1, CSEPX*1

      EXTERNAL  LENC,IFROMC,LFROMC,RFROMC

      SAVE

      DATA      CSEPX / '_' /


      CALL OSGENV('DCLENVCHAR',CSEP)
      IF (CSEP.EQ.' ') CSEP=CSEPX

      RETURN
*-----------------------------------------------------------------------
*     GET INTEGER PARAMETER WITH SHORT NAME
*-----------------------------------------------------------------------
      ENTRY RTIENV(CPFIX, CP, IPARA)

      CPX = CPFIX(1:LENC(CPFIX))//CSEP//CP

      CALL OSGENV(CPX, CARG)
      IF (CARG .NE. ' ') THEN
        IPARA = IFROMC(CARG)
      END IF

      RETURN
*-----------------------------------------------------------------------
*     GET LOGICAL PARAMETER WITH SHORT NAME
*-----------------------------------------------------------------------
      ENTRY RTLENV(CPFIX, CP, LPARA)

      CPX = CPFIX(1:LENC(CPFIX))//CSEP//CP

      CALL OSGENV(CPX, CARG)
      IF (CARG .NE. ' ') THEN
        LPARA = LFROMC(CARG)
      END IF

      RETURN
*-----------------------------------------------------------------------
*     GET REAL PARAMETER WITH SHORT NAME
*-----------------------------------------------------------------------
      ENTRY RTRENV(CPFIX, CP, RPARA)

      CPX = CPFIX(1:LENC(CPFIX))//CSEP//CP

      CALL OSGENV(CPX, CARG)
      IF (CARG .NE. ' ') THEN
        RPARA = RFROMC(CARG)
      END IF

      RETURN
*-----------------------------------------------------------------------
*     GET CHARACTER PARAMETER WITH SHORT NAME
*-----------------------------------------------------------------------
      ENTRY RTCENV(CPFIX, CP, CPARA)

      CPX = CPFIX(1:LENC(CPFIX))//CSEP//CP

      CALL OSGENV(CPX, CARG)
      IF (CARG .NE. ' ') THEN
        CPARA = CARG
      END IF

      RETURN
*-----------------------------------------------------------------------
*     GET INTEGER PARAMETER WITH LONG NAME
*-----------------------------------------------------------------------
      ENTRY RLIENV(CP, IPARA)

      CALL OSGENV(CP, CARG)
      IF (CARG .NE. ' ') THEN
        IPARA = IFROMC(CARG)
      END IF

      RETURN
*-----------------------------------------------------------------------
*     GET LOGICAL PARAMETER WITH LONG NAME
*-----------------------------------------------------------------------
      ENTRY RLLENV(CP, LPARA)

      CALL OSGENV(CP, CARG)
      IF (CARG .NE. ' ') THEN
        LPARA = LFROMC(CARG)
      END IF

      RETURN
*-----------------------------------------------------------------------
*     GET REAL PARAMETER WITH LONG NAME
*-----------------------------------------------------------------------
      ENTRY RLRENV(CP, RPARA)

      CALL OSGENV(CP, CARG)
      IF (CARG .NE. ' ') THEN
        RPARA = RFROMC(CARG)
      END IF

      RETURN
*-----------------------------------------------------------------------
*     GET CHARACTER PARAMETER WITH LONG NAME
*-----------------------------------------------------------------------
      ENTRY RLCENV(CP, CPARA)

      CALL OSGENV(CP, CARG)
      IF (CARG .NE. ' ') THEN
        CPARA = CARG
      END IF

      RETURN
      END
