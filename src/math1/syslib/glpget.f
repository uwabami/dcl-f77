*-----------------------------------------------------------------------
*     GLPGET / GLPSET / GLPSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE GLPGET(CP, IPARA)

      CHARACTER CP*(*)

      CHARACTER CX*40, CL*40


      CALL GLPQID(CP, IDX)
      CALL GLPQVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY GLPSET(CP, IPARA)

      CALL GLPQID(CP, IDX)
      CALL GLPSVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY GLPSTX(CP, IPARA)

      IP = IPARA
      CALL GLPQID(CP, IDX)
      CALL GLPQIT(IDX, IT)
      CALL GLPQCP(IDX, CX)
      CALL GLPQCL(IDX, CL)
      IF (IT .EQ. 1) THEN
        CALL RTIGET('GL', CX, IP, 1)
        CALL RLIGET(CL, IP, 1)
        CALL GLIQID(CP, IDX)
        CALL GLISVL(IDX, IP)
      ELSE IF (IT .EQ. 2) THEN
        CALL RTLGET('GL', CX, IP, 1)
        CALL RLLGET(CL, IP, 1)
        CALL GLLQID(CP, IDX)
        CALL GLLSVL(IDX, IP)
      ELSE IF (IT .EQ. 3) THEN
        CALL RTRGET('GL', CX, IP, 1)
        CALL RLRGET(CL, IP, 1)
        CALL GLRQID(CP, IDX)
        CALL GLRSVL(IDX, IP)
      END IF

      RETURN
      END
