*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      INTEGER FUNCTION ISUM1(IX,N,JX)

      INTEGER   IX(*)


      CALL GLIGET('IMISS',IMISS)
      ISUM=0
      NN=0
      DO 10 I=1,JX*(N-1)+1,JX
        IF (IX(I).NE.IMISS) THEN
          NN=NN+1
          ISUM=ISUM+IX(I)
        END IF
   10 CONTINUE
      IF (NN.EQ.0) THEN
        ISUM1=IMISS
      ELSE
        ISUM1=ISUM
      END IF

      END
