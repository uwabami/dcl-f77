*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      INTEGER FUNCTION IMAX0(IX,N,JX)

      INTEGER   IX(*)


      IMAX0=IX(1)
      DO 10 I=1,JX*(N-1)+1,JX
        IF (IX(I).GT.IMAX0) THEN
          IMAX0=IX(I)
        END IF
   10 CONTINUE

      END
