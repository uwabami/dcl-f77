*-----------------------------------------------------------------------
*     IMIN
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      INTEGER FUNCTION IMIN(IX,N,JX)

      INTEGER   IX(*)

      LOGICAL   LMISS

      EXTERNAL  IMIN0,IMIN1


      CALL GLLGET('LMISS',LMISS)
      IF (LMISS) THEN
        IMIN=IMIN1(IX,N,JX)
      ELSE
        IMIN=IMIN0(IX,N,JX)
      END IF

      END
