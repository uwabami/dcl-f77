*-----------------------------------------------------------------------
*     IMAX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      INTEGER FUNCTION IMAX(IX,N,JX)

      INTEGER   IX(*)

      LOGICAL   LMISS

      EXTERNAL  IMAX0,IMAX1


      CALL GLLGET('LMISS',LMISS)
      IF (LMISS) THEN
        IMAX=IMAX1(IX,N,JX)
      ELSE
        IMAX=IMAX0(IX,N,JX)
      END IF

      END
