*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      INTEGER FUNCTION IMIN0(IX,N,JX)

      INTEGER   IX(*)


      IMIN0=IX(1)
      DO 10 I=1,JX*(N-1)+1,JX
        IF (IX(I).LT.IMIN0) THEN
          IMIN0=IX(I)
        END IF
   10 CONTINUE

      END
