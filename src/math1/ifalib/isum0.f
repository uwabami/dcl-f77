*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      INTEGER FUNCTION ISUM0(IX,N,JX)

      INTEGER   IX(*)


      ISUM=0
      DO 10 I=1,JX*(N-1)+1,JX
        ISUM=ISUM+IX(I)
   10 CONTINUE
      ISUM0=ISUM

      END
