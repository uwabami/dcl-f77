*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      INTEGER FUNCTION IMAX1(IX,N,JX)

      INTEGER   IX(*)

      LOGICAL   MADA


      CALL GLIGET('IMISS',IMISS)
      MADA=.TRUE.
      DO 10 I=1,JX*(N-1)+1,JX
        IF (MADA) THEN
          IF (IX(I).NE.IMISS) THEN
            IMAX1=IX(I)
            MADA=.FALSE.
          END IF
        ELSE
          IF (IX(I).NE.IMISS .AND. IX(I).GT.IMAX1) THEN
            IMAX1=IX(I)
          END IF
        END IF
   10 CONTINUE
      IF (MADA) THEN
        IMAX1=IMISS
      END IF

      END
