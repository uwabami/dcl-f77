*-----------------------------------------------------------------------
*     ISUM
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      INTEGER FUNCTION ISUM(IX,N,JX)

      INTEGER   IX(*)

      LOGICAL   LMISS

      EXTERNAL  ISUM0,ISUM1


      CALL GLLGET('LMISS',LMISS)
      IF (LMISS) THEN
        ISUM=ISUM1(IX,N,JX)
      ELSE
        ISUM=ISUM0(IX,N,JX)
      END IF

      END
