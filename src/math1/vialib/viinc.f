*-----------------------------------------------------------------------
*     VIINC
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE VIINC(IX,IY,N,JX,JY,II)

      INTEGER   IX(*),IY(*)

      LOGICAL   LMISS


      CALL GLLGET('LMISS',LMISS)
      IF (LMISS) THEN
        CALL VIINC1(IX,IY,N,JX,JY,II)
      ELSE
        CALL VIINC0(IX,IY,N,JX,JY,II)
      END IF

      END
