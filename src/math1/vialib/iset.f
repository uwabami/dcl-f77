*-----------------------------------------------------------------------
*     ISET
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE ISET(IX,N,JX,II)

      INTEGER   IX(*)

      LOGICAL   LMISS


      CALL GLLGET('LMISS',LMISS)
      IF (LMISS) THEN
        CALL ISET1(IX,N,JX,II)
      ELSE
        CALL ISET0(IX,N,JX,II)
      END IF

      END
