*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE VIFNA0(IX,IY,N,JX,JY,IFNA)

      INTEGER   IX(*),IY(*)

      EXTERNAL  IFNA


      KX=1-JX
      KY=1-JY
      DO 10 J=1,N
        KX=KX+JX
        KY=KY+JY
        IY(KY)=IFNA(IX(KX))
   10 CONTINUE

      END
