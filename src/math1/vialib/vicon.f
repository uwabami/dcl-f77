*-----------------------------------------------------------------------
*     VICON
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE VICON(IX,IY,N,JX,JY,II)

      INTEGER   IX(*),IY(*)

      LOGICAL   LMISS


      CALL GLLGET('LMISS',LMISS)
      IF (LMISS) THEN
        CALL VICON1(IX,IY,N,JX,JY,II)
      ELSE
        CALL VICON0(IX,IY,N,JX,JY,II)
      END IF

      END
