*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE VICON0(IX,IY,N,JX,JY,II)

      INTEGER   IX(*),IY(*)


      KX=1-JX
      KY=1-JY
      DO 10 J=1,N
        KX=KX+JX
        KY=KY+JY
        IY(KY)=II
   10 CONTINUE

      END
