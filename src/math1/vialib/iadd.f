*-----------------------------------------------------------------------
*     IADD
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE IADD(IX,N,JX,II)

      INTEGER   IX(*)

      LOGICAL   LMISS


      CALL GLLGET('LMISS',LMISS)
      IF (LMISS) THEN
        CALL IADD1(IX,N,JX,II)
      ELSE
        CALL IADD0(IX,N,JX,II)
      END IF

      END
