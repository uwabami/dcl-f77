*-----------------------------------------------------------------------
*     VIFNA
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE VIFNA(IX,IY,N,JX,JY,IFNA)

      INTEGER   IX(*),IY(*)

      LOGICAL   LMISS

      EXTERNAL  IFNA


      CALL GLLGET('LMISS',LMISS)
      IF (LMISS) THEN
        CALL VIFNA1(IX,IY,N,JX,JY,IFNA)
      ELSE
        CALL VIFNA0(IX,IY,N,JX,JY,IFNA)
      END IF

      END
