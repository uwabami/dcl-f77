*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE VIFNA1(IX,IY,N,JX,JY,IFNA)

      INTEGER   IX(*),IY(*)

      EXTERNAL  IFNA


      CALL GLIGET('IMISS',IMISS)
      KX=1-JX
      KY=1-JY
      DO 10 J=1,N
        KX=KX+JX
        KY=KY+JY
        IF (IX(KX).NE.IMISS) THEN
          IY(KY)=IFNA(IX(KX))
        ELSE
          IY(KY)=IMISS
        END IF
   10 CONTINUE

      END
