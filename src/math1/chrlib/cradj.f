*-----------------------------------------------------------------------
*     CRADJ
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE CRADJ(CHR)

      CHARACTER CHR*(*)

      EXTERNAL  LENC


      LC1=LEN(CHR)
      LC2=LENC(CHR)
      IF (LC1.NE.LC2) THEN
        DO 10 I=LC1,1,-1
          IF (I.GT.LC1-LC2) THEN
            CHR(I:I)=CHR(I-(LC1-LC2):I-(LC1-LC2))
          ELSE
            CHR(I:I)=' '
          END IF
   10   CONTINUE
      END IF

      END
