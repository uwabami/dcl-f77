*-----------------------------------------------------------------------
*     CRVRS
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE CRVRS(CHR)

      CHARACTER CHR*(*)

      CHARACTER CH1*1


      LC=LEN(CHR)
      DO 10 I=1,LC/2
        CH1=CHR(LC-I+1:LC-I+1)
        CHR(LC-I+1:LC-I+1)=CHR(I:I)
        CHR(I:I)=CH1
   10 CONTINUE

      END
