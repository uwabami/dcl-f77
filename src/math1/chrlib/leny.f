*-----------------------------------------------------------------------
*     LENY
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      INTEGER FUNCTION LENY(C)

      CHARACTER C*(*)

      LOGICAL   LCHAR
      CHARACTER CN*1,CS*1


      CN=CHAR(0)
      CS=' '

      LC=LEN(C)
      N=1

   15 LCHAR=N.LE.LC .AND. (C(N:N).EQ.CN .OR. C(N:N).EQ.CS)
      IF (.NOT.LCHAR) GO TO 10
        N=N+1
        GO TO 15
   10 CONTINUE

      LENY=N-1

      END
