*-----------------------------------------------------------------------
*     CLADJ
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE CLADJ(CHR)

      CHARACTER CHR*(*)

      EXTERNAL  LENB


      LC1=LEN(CHR)
      LC2=LENB(CHR)
      IF (LC2.NE.0) THEN
        DO 10 I=1,LC1,1
          IF (I.LE.LC1-LC2) THEN
            CHR(I:I)=CHR(I+LC2:I+LC2)
          ELSE
            CHR(I:I)=' '
          END IF
   10   CONTINUE
      END IF

      END
