*-----------------------------------------------------------------------
*     LENC
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      INTEGER FUNCTION LENC(C)

      CHARACTER C*(*)

      LOGICAL   LCHAR
      CHARACTER CN*1,CS*1


      CN=CHAR(0)
      CS=' '

      N=LEN(C)

   15 LCHAR=N.GT.1 .AND. (C(N:N).EQ.CN .OR. C(N:N).EQ.CS)
      IF (.NOT.LCHAR) GO TO 10
        N=N-1
        GO TO 15
   10 CONTINUE

      LENC=N

      END
