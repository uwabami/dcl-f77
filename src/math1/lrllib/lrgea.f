*-----------------------------------------------------------------------
*     LRGEA
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      LOGICAL FUNCTION LRGEA(X,Y,EPSL)

      LOGICAL   LREQA

      EXTERNAL  LREQA


      LRGEA=X.GE.Y .OR. LREQA(X,Y,EPSL)

      END
