*-----------------------------------------------------------------------
*     LRLE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      LOGICAL FUNCTION LRLE(X,Y)

      LOGICAL   LRLE0,LRLE1,LEPSL

      EXTERNAL  LRLE0,LRLE1


      CALL GLLGET('LEPSL',LEPSL)
      IF (LEPSL) THEN
        LRLE=LRLE1(X,Y)
      ELSE
        LRLE=LRLE0(X,Y)
      END IF

      END
