*-----------------------------------------------------------------------
*     LRLTA
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      LOGICAL FUNCTION LRLTA(X,Y,EPSL)

      LOGICAL   LRNEA

      EXTERNAL  LRNEA


      LRLTA=X.LT.Y .AND. LRNEA(X,Y,EPSL)

      END
