*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      LOGICAL FUNCTION LRLT1(X,Y)

      LOGICAL   LRNE1

      EXTERNAL  LRNE1


      LRLT1=X.LT.Y .AND. LRNE1(X,Y)

      END
