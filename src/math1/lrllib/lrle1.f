*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      LOGICAL FUNCTION LRLE1(X,Y)

      LOGICAL   LREQ1

      EXTERNAL  LREQ1


      LRLE1=X.LE.Y .OR. LREQ1(X,Y)

      END
