*-----------------------------------------------------------------------
*     LRLT
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      LOGICAL FUNCTION LRLT(X,Y)

      LOGICAL   LRLT0,LRLT1,LEPSL

      EXTERNAL  LRLT0,LRLT1


      CALL GLLGET('LEPSL',LEPSL)
      IF (LEPSL) THEN
        LRLT=LRLT1(X,Y)
      ELSE
        LRLT=LRLT0(X,Y)
      END IF

      END
