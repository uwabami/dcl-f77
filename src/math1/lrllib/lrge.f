*-----------------------------------------------------------------------
*     LRGE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      LOGICAL FUNCTION LRGE(X,Y)

      LOGICAL   LRGE0,LRGE1,LEPSL

      EXTERNAL  LRGE0,LRGE1


      CALL GLLGET('LEPSL',LEPSL)
      IF (LEPSL) THEN
        LRGE=LRGE1(X,Y)
      ELSE
        LRGE=LRGE0(X,Y)
      END IF

      END
