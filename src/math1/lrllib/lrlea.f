*-----------------------------------------------------------------------
*     LRLEA
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      LOGICAL FUNCTION LRLEA(X,Y,EPSL)

      LOGICAL   LREQA

      EXTERNAL  LREQA


      LRLEA=X.LE.Y .OR. LREQA(X,Y,EPSL)

      END
