*-----------------------------------------------------------------------
*     LREQ
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      LOGICAL FUNCTION LREQ(X,Y)

      LOGICAL   LREQ0,LREQ1,LEPSL

      EXTERNAL  LREQ0,LREQ1


      CALL GLLGET('LEPSL',LEPSL)
      IF (LEPSL) THEN
        LREQ=LREQ1(X,Y)
      ELSE
        LREQ=LREQ0(X,Y)
      END IF

      END
