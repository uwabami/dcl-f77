*-----------------------------------------------------------------------
*     LRGT
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      LOGICAL FUNCTION LRGT(X,Y)

      LOGICAL   LRGT0,LRGT1,LEPSL

      EXTERNAL  LRGT0,LRGT1


      CALL GLLGET('LEPSL',LEPSL)
      IF (LEPSL) THEN
        LRGT=LRGT1(X,Y)
      ELSE
        LRGT=LRGT0(X,Y)
      END IF

      END
