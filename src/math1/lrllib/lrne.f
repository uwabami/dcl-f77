*-----------------------------------------------------------------------
*     LRNE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      LOGICAL FUNCTION LRNE(X,Y)

      LOGICAL   LRNE0,LRNE1,LEPSL

      EXTERNAL  LRNE0,LRNE1


      CALL GLLGET('LEPSL',LEPSL)
      IF (LEPSL) THEN
        LRNE=LRNE1(X,Y)
      ELSE
        LRNE=LRNE0(X,Y)
      END IF

      END
