*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      LOGICAL FUNCTION LRGE1(X,Y)

      LOGICAL   LREQ1

      EXTERNAL  LREQ1


      LRGE1=X.GE.Y .OR. LREQ1(X,Y)

      END
