*-----------------------------------------------------------------------
*     LRGTA
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      LOGICAL FUNCTION LRGTA(X,Y,EPSL)

      LOGICAL   LRNEA

      EXTERNAL  LRNEA


      LRGTA=X.GT.Y .AND. LRNEA(X,Y,EPSL)

      END
