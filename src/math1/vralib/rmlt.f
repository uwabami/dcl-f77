*-----------------------------------------------------------------------
*     RMLT
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE RMLT(RX,N,JX,RR)

      REAL      RX(*)

      LOGICAL   LMISS


      CALL GLLGET('LMISS',LMISS)
      IF (LMISS) THEN
        CALL RMLT1(RX,N,JX,RR)
      ELSE
        CALL RMLT0(RX,N,JX,RR)
      END IF

      END
