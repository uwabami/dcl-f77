*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE VRCON1(RX,RY,N,JX,JY,RR)

      REAL      RX(*),RY(*)


      CALL GLRGET('RMISS',RMISS)
      KX=1-JX
      KY=1-JY
      DO 10 J=1,N
        KX=KX+JX
        KY=KY+JY
        IF (RX(KX).NE.RMISS) THEN
          RY(KY)=RR
        ELSE
          RY(KY)=RMISS
        END IF
   10 CONTINUE

      END
