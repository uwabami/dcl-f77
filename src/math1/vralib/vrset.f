*-----------------------------------------------------------------------
*     VRSET
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE VRSET(RX,RY,N,JX,JY)

      REAL      RX(*),RY(*)

*     ---------------------------
      ENTRY VRSET0(RX,RY,N,JX,JY)
      ENTRY VRSET1(RX,RY,N,JX,JY)
*     ---------------------------

      KX=1-JX
      KY=1-JY
      DO 10 J=1,N
        KX=KX+JX
        KY=KY+JY
        RY(KY)=RX(KX)
   10 CONTINUE

      END
