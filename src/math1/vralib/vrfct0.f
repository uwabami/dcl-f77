*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE VRFCT0(RX,RY,N,JX,JY,RR)

      REAL      RX(*),RY(*)


      KX=1-JX
      KY=1-JY
      DO 10 J=1,N
        KX=KX+JX
        KY=KY+JY
        RY(KY)=RX(KX)*RR
   10 CONTINUE

      END
