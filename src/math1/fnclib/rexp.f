*-----------------------------------------------------------------------
*     REXP
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      REAL FUNCTION REXP(RX,IB,IE)


      IF (IB.EQ.0) THEN
        CALL MSGDMP('E','REXP  ','IB .EQ. 0.')
      END IF

      REXP=RX
      DO 10 I=1,ABS(IE)
        IF (IE.GT.0) THEN
          REXP=REXP*IB
        ELSE
          REXP=REXP/IB
        END IF
   10 CONTINUE

      END
