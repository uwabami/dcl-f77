*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      REAL FUNCTION RD2R(X)

      EXTERNAL  RFPI


      RD2R=X*RFPI()/180

      END
