*-------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      REAL FUNCTION RFPI()

      LOGICAL   LFIRST

      SAVE

      DATA      LFIRST/.TRUE./


      IF (LFIRST) THEN
        PI=ATAN(1.)*4.
        LFIRST=.FALSE.
      END IF
      RFPI=PI

      END
