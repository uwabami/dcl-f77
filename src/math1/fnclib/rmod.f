*-----------------------------------------------------------------------
*     RMOD
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      REAL FUNCTION RMOD(RX,RD)


      IF (RD.LE.0) THEN
        CALL MSGDMP('E','RMOD  ','RD .LE. 0.')
      END IF

      RMOD=MOD(MOD(RX,RD)+RD,RD)

      END
