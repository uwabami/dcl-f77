*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE VRFNB1(RX,RY,RZ,N,JX,JY,JZ,RFNB)

      REAL      RX(*),RY(*),RZ(*)

      REAL      RFNB
      EXTERNAL  RFNB


      CALL GLRGET('RMISS',RMISS)
      KX=1-JX
      KY=1-JY
      KZ=1-JZ
      DO 10 J=1,N
        KX=KX+JX
        KY=KY+JY
        KZ=KZ+JZ
        IF (RX(KX).NE.RMISS .AND. RY(KY).NE.RMISS) THEN
          RZ(KZ)=RFNB(RX(KX),RY(KY))
        ELSE
          RZ(KZ)=RMISS
        END IF
   10 CONTINUE

      END
