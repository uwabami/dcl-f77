*-----------------------------------------------------------------------
*     VRDIV
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE VRDIV(RX,RY,RZ,N,JX,JY,JZ)

      REAL      RX(*),RY(*),RZ(*)

      LOGICAL   LMISS


      CALL GLLGET('LMISS',LMISS)
      IF (LMISS) THEN
        CALL VRDIV1(RX,RY,RZ,N,JX,JY,JZ)
      ELSE
        CALL VRDIV0(RX,RY,RZ,N,JX,JY,JZ)
      END IF

      END
