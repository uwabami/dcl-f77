*-----------------------------------------------------------------------
*     VRMLT
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE VRMLT(RX,RY,RZ,N,JX,JY,JZ)

      REAL      RX(*),RY(*),RZ(*)

      LOGICAL   LMISS


      CALL GLLGET('LMISS',LMISS)
      IF (LMISS) THEN
        CALL VRMLT1(RX,RY,RZ,N,JX,JY,JZ)
      ELSE
        CALL VRMLT0(RX,RY,RZ,N,JX,JY,JZ)
      END IF

      END
