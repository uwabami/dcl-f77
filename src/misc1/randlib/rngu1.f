*-----------------------------------------------------------------------
*     RANDOM NUMBER GENERATOR (LINEAR CONGRUENTIAL METHOD)
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      REAL FUNCTION RNGU1(ISEED)

      PARAMETER (IM=259200, IA=7141, IC=54773, RM=1./IM)

      LOGICAL   LFIRST

      SAVE

      DATA      LFIRST / .TRUE. /


      IF (ISEED.NE.0) THEN
        JSEED  = ABS(ISEED)
        ISEED  = 0
        LFIRST = .FALSE.
      END IF

      IF (LFIRST) CALL MSGDMP('E', 'RNGU1',
     #     'ISEED MUST BE > 0 FOR 1ST CALL.')
      JSEED = MOD(JSEED*IA+IC, IM)
      RNGU1 = REAL(JSEED)*RM

      END
