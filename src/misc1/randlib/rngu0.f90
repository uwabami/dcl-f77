!-----------------------------------------------------------------------
!     random number generator (Fortran90 Standard)
!-----------------------------------------------------------------------
real function rngu0(iseed)

logical lfirst
integer, allocatable :: is(:)
data lfirst /.true./
save

  if (lfirst) then
    call random_seed(size=i)
    allocate (is(i))
    is = iseed
    call random_seed(put=is)
    lfirst = .false.
  end if
  call random_number(rngu0)
end
