*-----------------------------------------------------------------------
*     GBYTES
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE GBYTES(NPACK,ISAM,IBIT,NBITS,NSKIP,ITER)

      IMPLICIT  INTEGER (A-Z)

      INTEGER   NPACK(*),ISAM(*)

      PARAMETER (BPERI=32)

      INTEGER   MASK(BPERI)

      SAVE

      EXTERNAL  ISHIFT

      DATA      NCALL/0/


      IF (NCALL.EQ.0) THEN
        CALL GLIGET('NBITSPW',NBITPW)
        IF (NBITPW.NE.BPERI) THEN
          CALL MSGDMP('E','GBYTES',
     +      'NUMBER OF BITS PER ONE WORD IS INVALID / '//
     +      'CHECK BPERI IN THE PARAMETER STATEMENT OF GBYTES '//
     +      'AND CHANGE IT CORRECTLY.')
        END IF
        MASK(1)=1
        DO 10 I=2,BPERI
          MASK(I)=IOR(ISHIFT(MASK(I-1),1),1)
   10   CONTINUE
        NCALL=1
      END IF

      IF (.NOT.(NBITS.LE.BPERI .AND. NBITS.GT.0)) THEN
        CALL MSGDMP('E','GBYTES','NBITS OUT OF RANGE.')
      END IF

      MSK1=MASK(NBITS)
      BITSEP=NBITS+NSKIP
      DO 20 I=1,ITER
        BPO=IBIT+(I-1)*BITSEP
        WPO=BPO/BPERI+1
        RBITS=BPERI*WPO-BPO
        SHFT=NBITS-RBITS
        IF (SHFT.LE.0) THEN
          ISAM(I)=IAND(MSK1,ISHIFT(NPACK(WPO),SHFT))
        ELSE
          BTMPL=ISHIFT(IAND(NPACK(WPO),MASK(RBITS)),SHFT)
          BTMPR=IAND(ISHIFT(NPACK(WPO+1),SHFT-BPERI),MASK(SHFT))
          ISAM(I)=IOR(BTMPL,BTMPR)
        END IF
   20 CONTINUE

      END
