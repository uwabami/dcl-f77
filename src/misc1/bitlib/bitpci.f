*-----------------------------------------------------------------------
*     BITPCI
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE BITPCI(CP,IP)

      CHARACTER CP*(*)

      PARAMETER (NB=32)

      INTEGER   MASK(NB)
      LOGICAL   LFST

      SAVE

      EXTERNAL  ISHIFT

      DATA      LFST/.TRUE./


      IF (LFST) THEN
        CALL GLIGET('NBITSPW',NBITPW)
        IF (NBITPW.NE.NB) THEN
          CALL MSGDMP('E','BITPCI',
     +      'NUMBER OF BITS PER ONE WORD IS INVALID / '//
     +      'CHECK NB IN THE PARAMETER STATEMENT OF BITPCI '//
     +      'AND CHANGE IT CORRECTLY.')
        END IF
        MASK(1)=1
        DO 10 I=2,NB
          MASK(I)=ISHIFT(MASK(I-1),1)
   10   CONTINUE
        LFST=.FALSE.
      END IF

      NBC=LEN(CP)
      IP=0
      DO 15 I=1,MIN(NBC,NB)
        II=NBC-I+1
        IF (CP(II:II).NE.'0') THEN
          IP=IP+MASK(I)
        END IF
   15 CONTINUE

      END
