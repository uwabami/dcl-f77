*-----------------------------------------------------------------------
*     SBYTE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SBYTE(NPACK,IOUT,IBIT,NBITS)

      INTEGER   NPACK(*)


      CALL SBYTES(NPACK,IOUT,IBIT,NBITS,0,1)

      END
