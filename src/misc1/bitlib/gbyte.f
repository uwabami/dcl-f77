*-----------------------------------------------------------------------
*     GBYTE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE GBYTE(NPACK,IOUT,IBIT,NBITS)

      INTEGER   NPACK(*)


      CALL GBYTES(NPACK,IOUT,IBIT,NBITS,0,1)

      END
