*-----------------------------------------------------------------------
*     CLOWER
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE CLOWER(CH)

      CHARACTER CH*(*)


      LCH=LEN(CH)
      DO 10 I=1,LCH
        IDX=ICHAR(CH(I:I))
        IF (65.LE.IDX .AND. IDX.LE.90) THEN
          CH(I:I)=CHAR(IDX+32)
        END IF
   10 CONTINUE

      END
