*-----------------------------------------------------------------------
*     CUPPER
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE CUPPER(CH)

      CHARACTER CH*(*)


      LCH=LEN(CH)
      DO 10 I=1,LCH
        IDX=ICHAR(CH(I:I))
        IF (97.LE.IDX .AND. IDX.LE.122) THEN
          CH(I:I)=CHAR(IDX-32)
        END IF
   10 CONTINUE

      END
