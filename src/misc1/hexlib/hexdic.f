*-----------------------------------------------------------------------
*     HEXDIC
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE HEXDIC(IP,CP)

      CHARACTER CP*(*)

      LOGICAL   LFST
      CHARACTER CHEX(0:15)*1

      SAVE

      DATA      CHEX/ '0', '1', '2', '3', '4', '5', '6', '7',
     +                '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'/
      DATA      LFST/.TRUE./


      IF (LFST) THEN
        CALL GLIGET('NBITSPW',NBITS)
        NHEX=NBITS/4
        LFST=.FALSE.
      END IF

      NH=LEN(CP)
      CP(1:NH)=' '
      DO 10 I=1,MIN(NHEX,NH)
        ISKIP=NBITS-I*4
        IDX=NH-I+1
        CALL GBYTE(IP,IPX,ISKIP,4)
        CP(IDX:IDX)=CHEX(IPX)
   10 CONTINUE

      END
