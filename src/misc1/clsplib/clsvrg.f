*-----------------------------------------------------------------------
      SUBROUTINE CLSVRG(H,S,V,R,G,B,N,M)

      INTEGER R(N,M),G(N,M),B(N,M),HI
      REAL    H(N,M),S(N,M),V(N,M),F,P,Q,T

      DO 10 I=1,N
      DO 20 J=1,M
      IF(S(I,J).EQ.0.)THEN
        R(I,J)=V(I,J)
        G(I,J)=V(I,J)
        B(I,J)=V(I,J)
      ELSE
        HI = H(I,J) / 60.0
        HI = MOD(HI,6)
        F=H(I,J)/60.0 - HI
        P=V(I,J)*(1.0-S(I,J))
        Q=V(I,J)*(1.0-F*S(I,J))
        T=V(I,J)*(1.0-(1.0-F)*S(I,J))
        IF(HI.EQ.0)THEN
          R(I,J)=V(I,J)
          G(I,J)=T
          B(I,J)=P
        ELSE IF(HI.EQ.1)THEN
          R(I,J)=Q
          G(I,J)=V(I,J)
          B(I,J)=P
        ELSE IF(HI.EQ.2)THEN
          R(I,J)=P
          G(I,J)=V(I,J)
          B(I,J)=T
        ELSE IF(HI.EQ.3)THEN
          R(I,J)=P
          G(I,J)=Q
          B(I,J)=V(I,J)
        ELSE IF(HI.EQ.4)THEN
          R(I,J)=T
          G(I,J)=P
          B(I,J)=V(I,J)
        ELSE IF(HI.EQ.5)THEN
          R(I,J)=V(I,J)
          G(I,J)=P
          B(I,J)=Q
        ELSE
          CALL MSGDMP('E','CLSVRG',
     +          'CAN NOT CONVERT HSV TO RGB.')
        ENDIF
      ENDIF
 20   CONTINUE
 10   CONTINUE

      RETURN
      END
