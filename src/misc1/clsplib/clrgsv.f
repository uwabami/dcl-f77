*-----------------------------------------------------------------------
      SUBROUTINE CLRGSV(R,G,B,H,S,V,N,M)

      INTEGER R(N,M),G(N,M),B(N,M)
      REAL    H(N,M),S(N,M),V(N,M)
      INTEGER MAX,MIN

      DO 10 I=1,N
      DO 20 J=1,M
      IF ((R(I,J).GE.G(I,J)) .AND.(R(I,J).GE.B(I,J))) THEN
        MAX=R(I,J)
      ELSE IF (G(I,J).GE.B(I,J)) THEN
        MAX=G(I,J)
      ELSE
        MAX=B(I,J)
      ENDIF
      IF (( R(I,J).LE.G(I,J) ) .AND. ( R(I,J).LE.B(I,J))) THEN
        MIN=R(I,J)
      ELSE IF (G(I,J).LE.B(I,J)) THEN
        MIN=G(I,J)
      ELSE
        MIN=B(I,J)
      ENDIF

      IF (MAX.EQ.R(I,J))THEN
        H(I,J) = 60.0 * (G(I,J)-B(I,J))/(MAX-MIN)
      ELSE IF (MAX.EQ.G(I,J))THEN
        H(I,J) = 60.0 * (B(I,J)-R(I,J))/(MAX-MIN) + 120.0
      ELSE
        H(I,J) = 60.0 * (R(I,J)-G(I,J))/(MAX-MIN) + 240.0
      ENDIF

      H(I,J) = MOD(H(I,J),360.0)
      S(I,J) = (MAX-MIN)/MAX
      V(I,J) = (R(I,J) + G(I,J) +B(I,J)) / 3.0
 20   CONTINUE
 10   CONTINUE

      RETURN
      END
