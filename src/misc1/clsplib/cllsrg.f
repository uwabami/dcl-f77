*-----------------------------------------------------------------------
      SUBROUTINE CLLSRG(H,L,S,R,G,B,N,M)

      INTEGER R(N,M),G(N,M),B(N,M)
      REAL    H(N,M),L(N,M),S(N,M)
      INTEGER MAX,MIN

      DO 10 I=1,N
      DO 20 J=1,M
      IF(L(I,J).LE.50.)THEN
        MAX=2.55*(L(I,J)+L(I,J)*S(I,J)/100.)
        MIN=2.55*(L(I,J)-L(I,J)*S(I,J)/100.)
      ELSE
        MAX=2.55*(L(I,J)+(100.-L(I,J))*(S(I,J)/100.))
        MIN=2.55*(L(I,J)-(100.-L(I,J))*(S(I,J)/100.))
      ENDIF

      IF(300..LE.H(I,J))THEN
        R(I,J)=MAX
        G(I,J)=MIN
        B(I,J)=((360.-H(I,J))/60.)*(MAX-MIN)+MIN
      ELSE IF(240..LE.H(I,J))THEN
        R(I,J)=(H(I,J)-240./60.)*(MAX-MIN)+MIN
        G(I,J)=MIN
        B(I,J)=MAX
      ELSE IF(180..LE.H(I,J))THEN
        R(I,J)=MIN
        G(I,J)=((240.-H(I,J))/60.)*(MAX-MIN)+MIN
        B(I,J)=MAX
      ELSE IF(120..LE.H(I,J))THEN
        R(I,J)=MIN
        G(I,J)=MAX
        B(I,J)=((H(I,J)-120.)/60.)*(MAX-MIN)+MIN
      ELSE IF(60..LE.H(I,J))THEN
        R(I,J)=((120.-H(I,J))/60.)*(MAX-MIN)+MIN
        G(I,J)=MAX
        B(I,J)=MIN
      ELSE
        R(I,J)=MAX
        G(I,J)=(H(I,J)/60.)*(MAX-MIN)+MIN
        B(I,J)=MIN
      ENDIF
 20   CONTINUE
 10   CONTINUE
       
      RETURN
      END

