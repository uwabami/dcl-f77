*-----------------------------------------------------------------------
*     CNS
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      CHARACTER*1 FUNCTION CNS(INS)


      IF (INS.GT.0) THEN
        CNS='N'
      ELSE IF (INS.LT.0) THEN
        CNS='S'
      ELSE
        CNS=' '
      END IF

      END
