/*
 *	dclvnm
 *
 *    Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
 *
 */

#include <stdlib.h>
#include <string.h>

#ifndef DCLVER
#define DCLVER "dcl-ver.7.5.0"
#endif

#ifndef WINDOWS
void dclvnm_(char *cvname, int Lvname)
#else
void DCLVNM(char *cvname, int Lvname)
#endif
{
    int minlen, lbuf;
    char *dclver, *buf;

    memset (cvname, ' ', Lvname);
    dclver = DCLVER;
    if ((buf = strrchr(dclver, '/')) == NULL)
	buf = dclver;
    else
	*++buf;
    lbuf = strlen (buf);
    minlen = (lbuf < Lvname) ? lbuf : Lvname;
    memcpy (cvname, buf, minlen);
}
