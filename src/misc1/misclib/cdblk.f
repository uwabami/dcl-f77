*-----------------------------------------------------------------------
*     CDBLK
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE CDBLK(CHR)

      CHARACTER CHR*(*)

      LOGICAL   LBLK


      LBLK=.TRUE.
      NC=LEN(CHR)
      N=0
      DO 10 I=1,NC
        IF (.NOT.(CHR(I:I).EQ.' ' .AND. LBLK)) THEN
          N=N+1
          IF (I.NE.N) THEN
            CHR(N:N)=CHR(I:I)
          END IF
        END IF
        LBLK=CHR(I:I).EQ.' '
   10 CONTINUE
      DO 20 J=N+1,NC
        CHR(J:J)=' '
   20 CONTINUE

      END
