*-----------------------------------------------------------------------
*     LCHRC
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      LOGICAL FUNCTION LCHRC(CH)

      CHARACTER CH*(*)

      PARAMETER (NC=1)

      CHARACTER CLST(NC)*1

      SAVE

      EXTERNAL  INDXCF

      DATA      CLST/'$'/


      NCH=LEN(CH)
      LCHRC=.TRUE.
      DO 10 I=1,NCH
        NIDX=INDXCF(CLST,NC,1,CH(I:I))
        LCHRC=LCHRC.AND.NIDX.NE.0
        IF (.NOT.LCHRC) RETURN
   10 CONTINUE

      END
