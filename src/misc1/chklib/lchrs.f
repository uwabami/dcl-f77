*-----------------------------------------------------------------------
*     LCHRS
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      LOGICAL FUNCTION LCHRS(CH)

      CHARACTER CH*(*)

      PARAMETER (NC=13)

      CHARACTER CLST(NC)*1

      SAVE

      EXTERNAL  INDXCF

      DATA      CLST/ ' ','''', '(', ')', '*', '+', ',', '-', '.', '/',
     +                ':', '=', '$'/


      NCH=LEN(CH)
      LCHRS=.TRUE.
      DO 10 I=1,NCH
        NIDX=INDXCF(CLST,NC,1,CH(I:I))
        LCHRS=LCHRS.AND.NIDX.NE.0
        IF (.NOT.LCHRS) RETURN
   10 CONTINUE

      END
