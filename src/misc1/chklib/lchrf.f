*-----------------------------------------------------------------------
*     LCHRF
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      LOGICAL FUNCTION LCHRF(CH)

      CHARACTER CH*(*)

      PARAMETER (NC=49)

      CHARACTER CLST(NC)*1

      SAVE

      EXTERNAL  INDXCF

      DATA      CLST/ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
     +                'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
     +                'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
     +                'U', 'V', 'W', 'X', 'Y', 'Z', ' ','''', '(', ')',
     +                '*', '+', ',', '-', '.', '/', ':', '=', '$'/


      NCH=LEN(CH)
      LCHRF=.TRUE.
      DO 10 I=1,NCH
        NIDX=INDXCF(CLST,NC,1,CH(I:I))
        LCHRF=LCHRF.AND.NIDX.NE.0
        IF (.NOT.LCHRF) RETURN
   10 CONTINUE

      END
