*-----------------------------------------------------------------------
*     LCHRD
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      LOGICAL FUNCTION LCHRD(CH)

      CHARACTER CH*(*)

      PARAMETER (NC=10)

      CHARACTER CLST(NC)*1

      SAVE

      EXTERNAL  INDXCF

      DATA      CLST/ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'/


      NCH=LEN(CH)
      LCHRD=.TRUE.
      DO 10 I=1,NCH
        NIDX=INDXCF(CLST,NC,1,CH(I:I))
        LCHRD=LCHRD.AND.NIDX.NE.0
        IF (.NOT.LCHRD) RETURN
   10 CONTINUE

      END
