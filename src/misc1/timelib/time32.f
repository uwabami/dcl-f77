*-----------------------------------------------------------------------
*     TIME32
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE TIME32(IH,IM,IS,ITT)

*     IH   : HOUR                                               (I/ )
*     IM   : MINUTE                                             (I/ )
*     IS   : SECOND                                             (I/ )
*     ITT  : TOTAL TIME                                         ( /O)


      ITT=IH*3600+IM*60+IS

      END
