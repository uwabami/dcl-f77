*-----------------------------------------------------------------------
*     TIMEQ1
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE TIMEQ1(ITIME)

*     ITIME : TIME(IH*10000+IM*100+IS)                           ( /O)


      CALL TIMEQ3(IH, IM, IS)
      CALL TIME31(ITIME, IH, IM, IS)

      END
