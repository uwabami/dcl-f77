/*
 *    timeq3 (written in C)
 *
 *    Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
 *
 */

#include <time.h>
#include "../../../config.h"

#ifndef WINDOWS
void timeq3_(DCL_INT *ih, DCL_INT *im, DCL_INT *is)
#else
void TIMEQ3(DCL_INT *ih, DCL_INT *im, DCL_INT *is)
#endif
{
    long tp;
    struct tm lt;

    time(&tp);
    lt = *localtime(&tp);
    *ih = lt.tm_hour;
    *im = lt.tm_min;
    *is = lt.tm_sec;
} 
