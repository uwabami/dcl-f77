*-----------------------------------------------------------------------
*     TIMEQ2
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE TIMEQ2(ITT)

*     ITT   : TOTAL TIME                                        ( /O)


      CALL TIMEQ3(IH, IM, IS)
      CALL TIME32(IH, IM, IS, ITT)

      END
