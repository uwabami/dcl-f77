*-----------------------------------------------------------------------
*     TIMEC1
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE TIMEC1(CFORM,ITIME)

      CHARACTER CFORM*(*)


      CALL TIME13(ITIME,IH,IM,IS)
      CALL TIMEC3(CFORM,IH,IM,IS)

      END
