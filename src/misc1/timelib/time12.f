*-----------------------------------------------------------------------
*     TIME12
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE TIME12(ITIME,ITT)

*     ITIME : TIME (IH*10000+IM*100+IS)                         (I/ )
*     ITT   : TOTAL TIME                                        ( /O)


      CALL TIME13(ITIME,IH,IM,IS)
      CALL TIME32(IH,IM,IS,ITT)

      END
