*-----------------------------------------------------------------------
*     TIME23
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE TIME23(IH,IM,IS,ITT)

*     IH   : HOUR                                               (I/ )
*     IM   : MINUTE                                             (I/ )
*     IS   : SECOND                                             (I/ )
*     ITT  : TOTAL TIME                                         ( /O)


      IH=ITT/3600
      IM=(ITT-IH*3600)/60
      IS=ITT-IM*60-IH*3600

      END
