*-----------------------------------------------------------------------
*     TIME21
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE TIME21(ITIME,ITT)

*     ITIME : TIME (IH*10000+IM*100+IS)                         ( /O)
*     ITT   : TOTAL TIME                                        (I/ )


      CALL TIME23(IH,IM,IS,ITT)
      CALL TIME31(ITIME,IH,IM,IS)

      END
