*-----------------------------------------------------------------------
*     DATEQ1
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE DATEQ1(IDATE)

*     IDATE : DATE (IY*10000+IM*100+ID)                         ( /O)


      CALL DATEQ3(IY, IM, ID)
      CALL DATE31(IDATE, IY, IM, ID)

      END
