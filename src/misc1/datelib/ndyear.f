*----------------------------------------------------------------------
*     NDYEAR
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      INTEGER FUNCTION NDYEAR(IY)

      LOGICAL   LEAP


      LEAP=(MOD(IY,4).EQ.0 .AND. MOD(IY,100).NE.0)
     +     .OR. MOD(IY,400).EQ.0
      IF (LEAP) THEN
        NDYEAR=366
      ELSE
        NDYEAR=365
      END IF

      END
