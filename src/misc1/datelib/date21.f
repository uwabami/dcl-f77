*-----------------------------------------------------------------------
*     DATE21
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE DATE21(IDATE,IY,ITD)

*     IDATE : DATE (IY*10000+IM*100+ID)                         ( /O)
*     IY    : YEAR                                              (I/ )
*     ITD   : TOTAL DAY                                         (I/ )


      CALL DATE23(IY,IM,ID,ITD)
      CALL DATE31(IDATE,IY,IM,ID)

      END
