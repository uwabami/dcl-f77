*-----------------------------------------------------------------------
*     DATE32
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE DATE32(IY,IM,ID,ITD)

*     IY   : YEAR                                               (I/O)
*     IM   : MONTH                                              (I/ )
*     ID   : DAY                                                (I/ )
*     ITD  : TOTAL DAY                                          ( /O)

      INTEGER   MN(12)
      LOGICAL   LEAP

      SAVE

      DATA      MN/31,28,31,30,31,30,31,31,30,31,30,31/


      LEAP=(MOD(IY,4).EQ.0 .AND. MOD(IY,100).NE.0)
     +     .OR. MOD(IY,400).EQ.0
      IF (LEAP) THEN
        MN(2)=29
      ELSE
        MN(2)=28
      END IF

      ITD=ID
      DO 10 I=1,IM-1
        ITD=ITD+MN(I)
   10 CONTINUE

      END
