*-----------------------------------------------------------------------
*     DATEC2
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE DATEC2(CFORM,IY,ITD)

      CHARACTER CFORM*(*)


      CALL DATE23(IY,IM,ID,ITD)
      CALL DATEC3(CFORM,IY,IM,ID)

      END
