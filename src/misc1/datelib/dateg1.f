*-----------------------------------------------------------------------
*     DATEG1
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE DATEG1(N,IDATE,NDATE)


      CALL DATE12(IDATE,IY,ITD)
      CALL DATE12(NDATE,NY,NTD)
      CALL DATEG2(N,IY,ITD,NY,NTD)

      END
