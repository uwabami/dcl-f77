*-----------------------------------------------------------------------
*     NDATE1
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      INTEGER FUNCTION NDATE1(IDATE,NDATE)


      CALL DATE12(IDATE,IY,ITD)
      CALL DATE12(NDATE,NY,NTD)
      CALL DATEG2(NDATEX,IY,ITD,NY,NTD)
      NDATE1=NDATEX

      END
