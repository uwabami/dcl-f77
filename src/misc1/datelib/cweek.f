*-----------------------------------------------------------------------
*     CWEEK
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      CHARACTER*(*) FUNCTION CWEEK(IW)

      CHARACTER WEEK(7)*9

      SAVE

      DATA      WEEK/'SUNDAY   ','MONDAY   ','TUESDAY  ','WEDNESDAY',
     +               'THURSDAY ','FRIDAY   ','SATURDAY '/


      IF (.NOT.(1.LE.IW .AND. IW.LE.7)) THEN
        CALL MSGDMP('E','CWEEK ','IW IS OUT OF RANGE (1-7).')
      END IF

      CWEEK=WEEK(IW)

      END
