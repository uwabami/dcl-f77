*-----------------------------------------------------------------------
*     IWEEK2
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      INTEGER FUNCTION IWEEK2(IY,ITD)

      PARAMETER (IY1=1989,ITD1=365)

      EXTERNAL  IMOD


      CALL DATEG2(N,IY1,ITD1,IY,ITD)
      IWEEK2=IMOD(N,7)+1

      END
