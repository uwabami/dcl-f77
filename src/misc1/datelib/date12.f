*-----------------------------------------------------------------------
*     DATE12
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE DATE12(IDATE,IY,ITD)

*     IDATE : DATE (IY*10000+IM*100+ID)                         (I/ )
*     IY    : YEAR                                              ( /O)
*     ITD   : TOTAL DAY                                         ( /O)


      CALL DATE13(IDATE,IY,IM,ID)
      CALL DATE32(IY,IM,ID,ITD)

      END
