*-----------------------------------------------------------------------
*     NDATE3
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      INTEGER FUNCTION NDATE3(IY,IM,ID,NY,NM,ND)


      CALL DATE32(IY,IM,ID,ITD)
      CALL DATE32(NY,NM,ND,NTD)
      CALL DATEG2(NDATEX,IY,ITD,NY,NTD)
      NDATE3=NDATEX

      END
