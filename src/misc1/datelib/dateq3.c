/*
 *    dateq3 (written in C)
 *
 *    Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
 *
 */

#include <time.h>
#include "../../../config.h"

#ifndef WINDOWS
void dateq3_(DCL_INT *iy, DCL_INT *im, DCL_INT *id)
#else
void DATEQ3(DCL_INT *iy, DCL_INT *im, DCL_INT *id)
#endif
{
    long tp;
    struct tm lt;

    time(&tp);
    lt = *localtime(&tp);
    *iy = lt.tm_year + 1900;
    *im = lt.tm_mon + 1;
    *id = lt.tm_mday;
} 
