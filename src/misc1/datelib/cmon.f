*-----------------------------------------------------------------------
*     CMON
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      CHARACTER*(*) FUNCTION CMON(IM)

      CHARACTER MON(12)*9

      SAVE

      DATA MON/'JANUARY  ','FEBRUARY ','MARCH    ','APRIL    ',
     +         'MAY      ','JUNE     ','JULY     ','AUGUST   ',
     +         'SEPTEMBER','OCTOBER  ','NOVEMBER ','DECEMBER '/


      IF (.NOT.(1.LE.IM .AND. IM.LE.12)) THEN
        CALL MSGDMP('E','CMON  ','IM IS OUT OF RANGE (1-12).')
      END IF

      CMON=MON(IM)

      END
