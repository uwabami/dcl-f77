*-----------------------------------------------------------------------
*     DATEC1
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE DATEC1(CFORM,IDATE)

      CHARACTER CFORM*(*)


      CALL DATE13(IDATE,IY,IM,ID)
      CALL DATEC3(CFORM,IY,IM,ID)

      END
