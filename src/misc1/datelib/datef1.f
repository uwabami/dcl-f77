*-----------------------------------------------------------------------
*     DATEF1
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE DATEF1(N,IDATE,NDATE)


      CALL DATE12(IDATE,IY,ITD)
      CALL DATEF2(N,IY,ITD,NY,NTD)
      CALL DATE21(NDATE,NY,NTD)

      END
