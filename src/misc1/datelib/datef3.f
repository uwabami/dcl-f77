*-----------------------------------------------------------------------
*     DATEF3
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE DATEF3(N,IY,IM,ID,NY,NM,ND)


      CALL DATE32(IY,IM,ID,ITD)
      CALL DATEF2(N,IY,ITD,NY,NTD)
      CALL DATE23(NY,NM,ND,NTD)

      END
