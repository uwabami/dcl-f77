*----------------------------------------------------------------------
*     NDMON
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      INTEGER FUNCTION NDMON(IY,IM)

      INTEGER   MN(12)
      LOGICAL   LEAP

      SAVE

      DATA      MN/31,28,31,30,31,30,31,31,30,31,30,31/


      IF (.NOT.(1.LE.IM .AND. IM.LE.12)) THEN
        CALL MSGDMP('E','NDMON ','IM IS OUT OF RANGE (1-12).')
      END IF

      LEAP=(MOD(IY,4).EQ.0 .AND. MOD(IY,100).NE.0)
     +     .OR. MOD(IY,400).EQ.0
      IF (LEAP) THEN
        MN(2)=29
      ELSE
        MN(2)=28
      END IF
      NDMON=MN(IM)

      END
