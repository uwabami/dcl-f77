*-----------------------------------------------------------------------
*     DATEG2
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE DATEG2(N,IY,ITD,NY,NTD)

      EXTERNAL  NDYEAR


      N=0
      JY=NY-IY
      IF (JY.GT.0) THEN
        DO 10 J=1,JY
          N=N+NDYEAR(IY+J-1)
   10   CONTINUE
        N=N+NTD-ITD
      ELSE IF (JY.LT.0) THEN
        DO 15 J=1,-JY
          N=N-NDYEAR(IY-J)
   15   CONTINUE
        N=N-ITD+NTD
      ELSE
        N=NTD-ITD
      END IF

      END
