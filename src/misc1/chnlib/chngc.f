*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE CHNGC(CH,CA,CB)

      CHARACTER CH*(*),CA*(*),CB*(*)

      CHARACTER CPAT*1024

      EXTERNAL  INDXNF


      NH=LEN(CH)
      NA=LEN(CA)
      NB=LEN(CB)
      CPAT=CA(1:NA)

      IF (.NOT.(NA.EQ.NB)) THEN
        CALL MSGDMP('E','CHNGC','CHARACTER LENGTH IS INCONSISTENT.')
      END IF
      NN=NA
      IF (.NOT.(NH.GE.NN)) THEN
        CALL MSGDMP('E','CHNGC','TEXT LENGTH IS TOO SHORT.')
      END IF

      ID1=INDXNF(CH,NH-NN+1,1,CA)
      ID2=ID1+NN-1
      IF (ID1.EQ.0) THEN
        CALL MSGDMP('W','CHNGC',
     +       'PATTERN < '//CPAT(1:NA)//' > WAS NOT FOUND.')
      ELSE
        CH(ID1:ID2)=CB
      END IF

      END
