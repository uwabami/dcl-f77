*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE CHNGR(CH,CA,RR,CFMT)

      CHARACTER CH*(*),CA*(*),CFMT*(*)

      CHARACTER CPAT*1024

      EXTERNAL  INDXNF


      NH=LEN(CH)
      NN=LEN(CA)
      CPAT=CA(1:NN)

      IF (.NOT.(NH.GE.NN)) THEN
        CALL MSGDMP('E','CHNGI','TEXT LENGTH IS TOO SHORT.')
      END IF

      ID1=INDXNF(CH,NH-NN+1,1,CA)
      ID2=ID1+NN-1
      IF (ID1.EQ.0) THEN
        CALL MSGDMP('W','CHNGC',
     +       'PATTERN < '//CPAT(1:NN)//' > WAS NOT FOUND.')
      ELSE
        WRITE(CH(ID1:ID2),CFMT,IOSTAT=IOS) RR
        IF (IOS.NE.0) THEN
          CALL MSGDMP('W','CHNGR','SUBSTITUTION ERROR.')
        END IF
      END IF

      END
