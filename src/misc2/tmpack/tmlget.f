*-----------------------------------------------------------------------
*     TMLGET / TMLSET / TMLSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE TMLGET(CP, LPARA)
      IMPLICIT NONE

      CHARACTER CP*(*)

      CHARACTER CX*8
      CHARACTER CL*40
      LOGICAL   LPARA, LP

      INTEGER   IDX

      CALL TMLQID(CP, IDX)
      CALL TMLQVL(IDX, LPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY TMLSET(CP, LPARA)

      CALL TMLQID(CP, IDX)
      CALL TMLSVL(IDX, LPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY TMLSTX(CP, LPARA)

      LP = LPARA
      CALL TMLQID(CP, IDX)

*     / SHORT NAME /

      CALL TMLQCP(IDX, CX)
      CALL RTLGET('TM', CX, LP, 1)

*     / LONG NAME /

      CALL TMLQCL(IDX, CL)
      CALL RLLGET(CL, LP, 1)

      CALL TMLSVL(IDX,LP)

      RETURN
      END
