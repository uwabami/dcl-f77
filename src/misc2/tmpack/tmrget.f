*-----------------------------------------------------------------------
*     TMRGET / TMRSET / TMRSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE TMRGET(CP, RPARA)
      IMPLICIT NONE

      CHARACTER CP*(*)

      CHARACTER CX*8
      CHARACTER CL*40

      INTEGER   IDX
      REAL      RP, RPARA

      CALL TMRQID(CP, IDX)
      CALL TMRQVL(IDX, RPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY TMRSET(CP, RPARA)

      CALL TMRQID(CP, IDX)
      CALL TMRSVL(IDX, RPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY TMRSTX(CP, RPARA)

      RP = RPARA
      CALL TMRQID(CP, IDX)

*     / SHORT NAME /

      CALL TMRQCP(IDX, CX)
      CALL RTRGET('TM', CX, RP, 1)

*     / LONG NAME /

      CALL TMRQCL(IDX, CL)
      CALL RLRGET(CL, RP, 1)

      CALL TMRSVL(IDX,RP)

      RETURN
      END
