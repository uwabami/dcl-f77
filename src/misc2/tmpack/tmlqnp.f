*-----------------------------------------------------------------------
*     TMLQNP / TMLQID / TMLQCP / TMLQVL / TMLSVL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE TMLQNP(NCP)
      IMPLICIT NONE

      INTEGER   NCP
      LOGICAL   LPARA
      CHARACTER CP*(*)

      INTEGER   NPARA
      PARAMETER (NPARA = 5)

      LOGICAL   LX(NPARA)
      LOGICAL   LCHREQ, LFIRST
      CHARACTER CPARAS(NPARA)*8
      CHARACTER CPARAL(NPARA)*40
      CHARACTER CMSG*80

      INTEGER   LENC
      EXTERNAL  LCHREQ,LENC

      SAVE

*     / SHORT NAME /

      DATA      CPARAS(1) / 'PERIODX ' /, LX(1) / .FALSE. /
      DATA      CPARAS(2) / 'PERIODY ' /, LX(2) / .FALSE. /
      DATA      CPARAS(3) / 'NODRSHRT' /, LX(3) / .FALSE. /
      DATA      CPARAS(4) / 'ENDARROW' /, LX(4) / .FALSE. /
      DATA      CPARAS(5) / 'FIXEDDT ' /, LX(5) / .FALSE. /

*     / LONG NAME /

      DATA      CPARAL(1) / 'PERIOD_BOUNDARY_XDIRECTION' /
      DATA      CPARAL(2) / 'PERIOD_BOUNDARY_YDIRECTION' /
      DATA      CPARAL(3) / 'NO_DRAW_SHORTLINE' /
      DATA      CPARAL(4) / 'END_ARROW_FLAG' /
      DATA      CPARAL(5) / 'FIXED_TIME_STEP' /

      DATA      LFIRST / .TRUE. /

      INTEGER   N, IDX, IN

      NCP = NPARA

      RETURN
*-----------------------------------------------------------------------
      ENTRY TMLQID(CP, IDX)

      DO 10 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IDX = N
          RETURN
        END IF
   10 CONTINUE
      CMSG = 'PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E','TMLQID',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY TMLQCP(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAS(IDX)
      ELSE
        CALL MSGDMP('E','TMLQCP','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY TMLQCL(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAL(IDX)
      ELSE
        CALL MSGDMP('E','TMLQCL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY TMLQVL(IDX, LPARA)

      IF (LFIRST) THEN
        CALL RTLGET('TM', CPARAS, LX, NPARA)
        CALL RLLGET(CPARAL, LX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        LPARA = LX(IDX)
      ELSE
        CALL MSGDMP('E','TMLQVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY TMLSVL(IDX, LPARA)

      IF (LFIRST) THEN
        CALL RTLGET('TM', CPARAS, LX, NPARA)
        CALL RLLGET(CPARAL, LX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        LX(IDX) = LPARA
      ELSE
        CALL MSGDMP('E','TMLSVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY TMLQIN(CP, IN)

      DO 20 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IN = N
          RETURN
        END IF
   20 CONTINUE

      IN = 0

      RETURN
      END
