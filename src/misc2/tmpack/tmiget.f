*-----------------------------------------------------------------------
*     TMIGET / TMISET / TMISTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE TMIGET(CP, IPARA)
      IMPLICIT NONE

      CHARACTER CP*(*)

      CHARACTER CX*8
      CHARACTER CL*40

      INTEGER   IDX, IP, IPARA

      CALL TMIQID(CP, IDX)
      CALL TMIQVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY TMISET(CP, IPARA)

      CALL TMIQID(CP, IDX)
      CALL TMISVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY TMISTX(CP, IPARA)

      IP = IPARA
      CALL TMIQID(CP, IDX)

*     / SHORT NAME /

      CALL TMIQCP(IDX, CX)
      CALL RTIGET('TM', CX, IP, 1)

*     / LONG NAME /

      CALL TMIQCL(IDX, CL)
      CALL RLIGET(CL, IP, 1)

      CALL TMISVL(IDX,IP)

      RETURN
      END
