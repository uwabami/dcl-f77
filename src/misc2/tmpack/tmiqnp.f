*-----------------------------------------------------------------------
*     TMIQNP / TMIQID / TMIQCP / TMIQVL / TMISVL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE TMIQNP(NCP)
      IMPLICIT NONE

      CHARACTER CP*(*)

      INTEGER   NPARA
      INTEGER   IUNDEF
      PARAMETER (NPARA  = 4)
      PARAMETER (IUNDEF = -999)

      INTEGER   IX(NPARA)
      LOGICAL   LCHREQ, LFIRST
      CHARACTER CPARAS(NPARA)*8
      CHARACTER CPARAL(NPARA)*40
      CHARACTER CMSG*80

      INTEGER   LENC
      EXTERNAL  LCHREQ,LENC

      SAVE

*     / SHORT NAME /

      DATA      CPARAS(1) / 'GRDTHRES' /, IX(1) / 1 /
      DATA      CPARAS(2) / 'SKIPINTV' /, IX(2) / 1 /
      DATA      CPARAS(3) / 'STLNNUM ' /, IX(3) / 1 /
      DATA      CPARAS(4) / 'STLNGLIM' /, IX(4) / 100 /

*     / LONG NAME /

      DATA      CPARAL(1) / 'GRID_PASSING_THRESHOLD' /
      DATA      CPARAL(2) / 'SKIPPING_POINT_INTERVAL' /
      DATA      CPARAL(3) / 'STREAM_LINE_NUMBERS' /
      DATA      CPARAL(4) / 'STREAM_LIMIT_INTERVAL' /

      DATA      LFIRST / .TRUE. /

      INTEGER   N, NCP
      INTEGER   IP, IDX, IPARA, IN

      NCP = NPARA

      RETURN
*-----------------------------------------------------------------------
      ENTRY TMIQID(CP, IDX)

      DO 10 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IDX = N
          RETURN
        END IF
   10 CONTINUE
      CMSG = 'PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E','TMIQID',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY TMIQCP(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAS(IDX)
      ELSE
        CALL MSGDMP('E','TMIQCP','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY TMIQCL(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAL(IDX)
      ELSE
        CALL MSGDMP('E','TMIQCL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY TMIQVL(IDX, IPARA)

      IF (LFIRST) THEN
        CALL RTIGET('TM', CPARAS, IX, NPARA)
        CALL RLIGET(CPARAL, IX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        IPARA = IX(IDX)
      ELSE
        CALL MSGDMP('E','TMIQVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY TMISVL(IDX, IPARA)

      IF (LFIRST) THEN
        CALL RTIGET('TM', CPARAS, IX, NPARA)
        CALL RLIGET(CPARAL, IX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        IX(IDX) = IPARA
      ELSE
        CALL MSGDMP('E','TMISVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY TMIQIN(CP, IN)

      DO 20 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IN = N
          RETURN
        END IF
   20 CONTINUE

      IN = 0

      RETURN
      END
