*-----------------------------------------------------------------------
*     TMSTLA : THE ROUTINE FOR DRAWING STREAM LINES (COORD. OF ANNULAR)
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD DENNOU CLUB. ALL RIGHTS RESERVED.
*-----------------------------------------------------------------------
      SUBROUTINE TMSTLA( R, T, U, V, NX, NY )

      IMPLICIT NONE

*-- AGREENMENT
      INTEGER  NX        !* GRID NUMBERS FOR X-DIRECTION
      INTEGER  NY        !* GRID NUMBERS FOR Y-DIRECTION
      REAL     U(NX,NY)  !* VECTOR COMPONENT OF X-DIRECTION
      REAL     V(NX,NY)  !* VECTOR COMPONENT OF Y-DIRECTION
      REAL     R(NX)     !* GRID POINTS OF RADIAL DIR. [UNIT:LENGTH]
      REAL     T(NY)     !* GRID POINTS OF TANGENT DIR. [UNIT:DEGREE]

*-- INTERNAL VARIABLES
      REAL     TRX(NX*NY,2)     !* STREAM LINE OF X-COORDINATE
      REAL     TRY(NX*NY,2)     !* STREAM LINE OF Y-COORDINATE
      INTEGER  I, J, K          !* TMP VALUES
      INTEGER  COUNTER          !* NUMBER OF STREAM LINE
      INTEGER  TOTNM(NX*NY,2)   !* ARRAY NUMBER IN TOTAL STREAM LINES AT END
      INTEGER  TOTSNM(NX*NY,2)  !* ARRAY NUMBER IN TOTAL STREAM LINES AT START
      REAL     X(NX), Y(NY)
      REAL     TMPU(NX,NY), TMPV(NX,NY)
      REAL     ARROW_THRES
      REAL     DXA, DYA
      REAL     ARROW_LENG
      REAL     VXMIN, VXMAX, VYMIN, VYMAX
      REAL     UXMIN, UXMAX, UYMIN, UYMAX
      REAL     VRRATIO
      REAL     RCOE
      REAL     CIRC_FLAG
      REAL     UNDEF
      INTEGER  N, M, IBLKGE, ARRCNT
      LOGICAL  NO_SHORT, END_ARR

*-- EXTERNAL FUNCTION
      REAL     RFPI
      EXTERNAL RFPI

      RCOE=RFPI()/180.0

*-- GETTING TMPACK'S PARAMETERS
      CALL TMRGET( 'ARRWINTV', ARROW_THRES )
      CALL TMLGET( 'NODRSHRT', NO_SHORT )
      CALL TMLGET( 'ENDARROW', END_ARR )

      CALL GLRGET( 'RMISS', UNDEF )
      CALL GLLSET( 'LMISS', .TRUE. )

*-- GETTING VIEWPORT PARAMETERS
      CALL SGQVPT( VXMIN, VXMAX, VYMIN, VYMAX)
      CALL SGQWND( UXMIN, UXMAX, UYMIN, UYMAX)
      VRRATIO=(VXMAX-VXMIN)/(UXMAX-UXMIN)

*-- CONVERTING DEGREE TO RADIAN
      DO 11 I=1,NX
         X(I)=R(I)
 11   CONTINUE
      DO 12 J=1,NY
         Y(J)=T(J)*RCOE
 12   CONTINUE

*-- CONVERTING VECTORS TO THEMSELVES INCLUDING SCALE PARAMETER
      DO 20 J=1,NY
         DO 21 I=1,NX
            IF(U(I,J).NE.UNDEF.AND.V(I,J).NE.UNDEF
     &         .AND.R(I).NE.0.0)THEN
               TMPU(I,J)=U(I,J)
               TMPV(I,J)=V(I,J)/R(I)
            ELSE
               TMPU(I,J)=UNDEF
               TMPV(I,J)=UNDEF
            END IF
 21      CONTINUE
 20   CONTINUE

*-- DOING TMSTLN ROUTINE
      CALL TMSTLN( X, Y, TMPU, TMPV, NX, NY, TRX, TRY, TOTNM )

*-- GETTING STREAM LINE NUMBERS
      CALL TMIGET( 'STLNNUM', COUNTER )

*-- SETTING START NUMBERS IN TOTAL STREAM LINES
      TOTSNM(1,1:2)=1
      DO 10 I=2,COUNTER
         TOTSNM(I,1)=TOTNM(I-1,1)+1
         TOTSNM(I,2)=TOTNM(I-1,2)+1
 10   CONTINUE

*-- CONVERTING TRY(RADIAN) TO TRY(DEGREE)
      DO 32 K=1,2
         DO 30 I=1,COUNTER
            DO 31 J=TOTSNM(I,K),TOTNM(I,K)
               IF(TRX(J,K).NE.UNDEF.AND.TRY(J,K).NE.UNDEF)THEN
                  TRY(J,K)=TRY(J,K)/RCOE
               END IF
 31         CONTINUE
 30      CONTINUE
 32   CONTINUE

*-- SETTING PARAMETER FOR ARROW. (NOW, CONSTANT VALUES)
      CALL SGLSET('LPROP',.FALSE.)
      CALL SGRSET('CONST',0.01)

*-- DRAWING STREAM LINES
      DO 82 K=1,2
         DO 81 I=1,COUNTER

*-- CALCULATING THE INTERVAL LENGTH OF DRAWING ARROWS.
            ARROW_LENG=0.0
            ARRCNT=0
            IF(((TOTNM(I,K)-TOTSNM(I,K)+1).GT.1).AND.
     &         ((TOTNM(I,K)-TOTSNM(I,K)+1).LE.NX*NY))THEN
               DO 80 J=TOTSNM(I,K)+1,TOTNM(I,K)
                  IF(TRX(J,K).NE.UNDEF.AND.TRY(J,K).NE.UNDEF.AND.
     &               TRX(J-1,K).NE.UNDEF.AND.TRY(J-1,K).NE.UNDEF
     &               )THEN
                     DXA=(TRX(J,K)-TRX(J-1,K))*VRRATIO
                     DYA=(TRY(J,K)-TRY(J-1,K))*VRRATIO
     &                                        *TRX(J,K)*RCOE
                     ARROW_LENG=ARROW_LENG+SQRT(DXA*DXA+DYA*DYA)
                     IF(ARROW_LENG.GE.ARROW_THRES)THEN
                        CALL SGLAU( TRX(J-1,K), TRY(J-1,K),
     &                              TRX(J,K), TRY(J,K) )
                        ARROW_LENG=0.0
                        ARRCNT=ARRCNT+1
                     END IF
                  END IF
 80            CONTINUE

*-- IF NO_SHORT IS TRUE, NO DRAWING STREAM LINE WHOSE ARROW NUMBER IS ZERO
*-- 流線の始点と終点が近似点にいれば, 閉じた流線とみなして描く.
*-- 矢羽は終点に描く.
               IF(ARRCNT.EQ.0)THEN
                  IF(NO_SHORT.EQV..TRUE.)THEN
                     IF((TOTNM(I,K)-TOTSNM(I,K)+1).LE.2)THEN
                        GO TO 81
                     ELSE
                        N=IBLKGE( R, NX, TRX(TOTSNM(I,K),K) )
                        M=IBLKGE( R, NX, TRX(TOTNM(I,K),K) )
                        IF(ABS(N-M).GT.1)THEN
                           GO TO 81
                        ELSE
*-- 一周しているのか, 短いだけなのかのチェック
*--  始点, 終点, 終点１つ前の順に増減していれば一周.
                           CIRC_FLAG=
     &                        (TRX(TOTNM(I,K),K)-TRX(TOTNM(I,K)-1,K))
     &                        *(TRX(TOTSNM(I,K),K)-TRX(TOTNM(I,K),K))
                           IF(CIRC_FLAG.LT.0.0)THEN  ! 単に短いだけ
                              GO TO 81
                           ELSE
                              N=IBLKGE( T, NY, TRY(TOTSNM(I,K),K) )
                              M=IBLKGE( T, NY, TRY(TOTNM(I,K),K) )
                              IF(ABS(N-M).GT.1)THEN
                                 GO TO 81
                              ELSE
                                 CIRC_FLAG=
     &                         (TRY(TOTNM(I,K),K)-TRY(TOTNM(I,K)-1,K))
     &                         *(TRY(TOTSNM(I,K),K)-TRY(TOTNM(I,K),K))
                                 IF(CIRC_FLAG.LT.0.0)THEN
                                    GO TO 81
!                                 ELSE
!                                 CALL SGLAU( TRX(TOTNM(I,K)-1,K), TRY(TOTNM(I,K)-1,K),
!     &                                       TRX(TOTNM(I,K),K), TRY(TOTNM(I,K),K) )
                                 END IF
                              END IF
                           END IF
                        END IF
                     END IF
                  ELSE
                     IF(END_ARR.EQV..TRUE.)THEN
                        CALL SGLAU( TRX(TOTNM(I,K)-1,K),
     &                              TRY(TOTNM(I,K)-1,K),
     &                              TRX(TOTNM(I,K),K),
     &                              TRY(TOTNM(I,K),K) )
                     END IF
                  END IF
               END IF

               CALL UULIN( (TOTNM(I,K)-TOTSNM(I,K)+1),
     &                     TRX(TOTSNM(I,K):TOTNM(I,K),K),
     &                     TRY(TOTSNM(I,K):TOTNM(I,K),K) )

            END IF

 81      CONTINUE

 82   CONTINUE

      CALL MSGDMP( 'M', 'TMSTLA', 'DRAWING FINISHED.' )

      END SUBROUTINE TMSTLA

