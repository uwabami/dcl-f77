#!/usr/bin/ruby
# coding: utf-8
begin
ARGV.each_with_index do |arg, i|
  FNAME=ARGV[0]
end
VALS=['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
CHAR_VALS=[]
CHAR_LEN=[]

chartrim = <<"CHRTRM"

Subroutine CHAR_TRIM(string,tfstring,len)
    Use Iso_C_Binding
    Character(1,C_char),Intent(In)  :: string(*)
    Character(1,C_char),Intent(Out) :: tfstring(*)
    Integer(c_int) i,len

    Do i=1,len
      tfstring(i:i) = string(i)
    End Do
End Subroutine

Subroutine CHAR_TRIM2(string,tfstring,len,len2,len3)
    Use Iso_C_Binding
    Character(1,C_char),Intent(In) :: string(*)
    Character  :: tfstring(*)*(*)
    Integer(c_int) i,j,k,len,len2,len3

    DO i=1,len2
    DO j=1,len
      tfstring(i)(j:j)  = string((i-1)*len3+j)
    End DO
    END DO
End Subroutine

Subroutine CHAR_TRIMC(string,tfstring,len)
    Use Iso_C_Binding
    Character(1,C_char),Intent(In) :: string(*)
    Character*(*)  :: tfstring
    Integer(c_int) i,len

    Do i=1,len-1
      tfstring(i:i) = string(i)
    End Do
    tfstring(len:len)=CHAR(0)
End Subroutine



CHRTRM

puts chartrim

  File.open(FNAME) do |file|

    file.each_line do |labmen|
      CHAR_VALS.clear
      CHAR_LEN.clear
      words=labmen.split
      if words[0] == "extern" then
        fcname=words[2..-1].join(" ").slice!(0..-2).split("(")
        fcname[0].slice!(-1,1)
        if fcname[1] != nil then
          ars=fcname[1].slice!(0..-2).split(",")
          arss=VALS[0..ars.count-1].join(",")
        else
          arss=""
        end
        if words[1] == "char" then
          words[1]="CHARACTER(c_char) FUNCTION WR_"
          corf="  WR_"+fcname[0].upcase+"="
        end
        if words[1] == "int" then
          words[1]="INTEGER(c_int) FUNCTION WR_"
          corf="  WR_"+fcname[0].upcase+"="
        end
        if words[1] == "logical" then
          words[1]="INTEGER(c_int) FUNCTION WR_"
          corf="  WR_"+fcname[0].upcase+"="
        end
        if words[1] == "real" then
          words[1]="REAL(c_float) FUNCTION WR_"
          corf="  WR_"+fcname[0].upcase+"="
        end
        if words[1] == "double" then
          words[1]="REAL(c_double) FUNCTION WR_"
          corf="  WR_"+fcname[0].upcase+"="
        end
        if words[1] == "void" then
          words[1]="SUBROUTINE WR_"
          corf="  CALL "
        end
        print(words[1])
        print(fcname[0].upcase)
        print("("+arss+")")
        print(" BIND(C,NAME='" + fcname[0] + "_')" +"\n")

        print("  USE iso_c_binding" +"\n")
        if ars != nil then
          i=0
          ars.each do |ar|
            arr=ar.split
            if arr[1] == nil then
              if arr[0] == "void" then
                arr[1]=" "
              else
                arr[1]=" "
              end
            end

            if arr[0] == "char" then
              arr[0] ="  CHARACTER(1,c_char)"
              CHAR_VALS.push(i)
            elsif arr[0] == "integer" then
              arr[0] ="  INTEGER(c_int)"
            elsif arr[0] == "real" then
                arr[0] ="  REAL(c_float)"
            elsif arr[0] == "logical*" then
                arr[0] ="  INTEGER(c_int)"
            elsif arr[0] == "ftnlen" then
                arr[0] ="  INTEGER(c_int)"
              CHAR_LEN.push(i)
            elsif arr[0] == "I_fp" then
                arr[0] ="  Type(c_funptr) "
            elsif arr[0] == "S_fp" then
                arr[0] ="  Type(c_funptr) "
            elsif arr[0] == "U_fp" then
                arr[0] ="  Type(c_funptr) "
            elsif arr[0] == "R_fp" then
                arr[0] ="  Type(c_funptr) "
            end
            if arr[1][0]=="*" || CHAR_VALS[-1]==i then
              arr[0]=arr[0]+" :: "
            elsif arr[0] != "void" then
              arr[0]=arr[0]+",VALUE :: "
            end
            if arr[0]!="void" then
              if arr[1][0]=="*" || CHAR_VALS[-1]==i then
                print(arr[0] + VALS[i] + "(*)\n")
              else
                print(arr[0] + VALS[i] + "\n")
              end
              i=i+1
            end
          end
#          print(" ;;;; " + ars[0..-1].join.slice!(0..-2) + "\n")
        end

        CHAR_LEN.length.times do |i|
          print("! CV" + VALS[CHAR_VALS[i]] + "CL " + CHAR_LEN[i].to_s + "\n")
          print("  CHARACTER(:,C_char),ALLOCATABLE :: " + VALS[CHAR_VALS[i]]+"A\n")

#          print("  CALL CHAR_TRIM("+VALS[CHAR_VALS[i]]+","+VALS[CHAR_LEN[i]]+");\n")

#         "for(i=" + VAL[CHAR_LEN[i] ;i++
#          *A
#          print("VALS" + CHAR_VALS[i].to_s)
#          print("ftnlen" +CHAR_LEN[i].to_s)
        end

#        print("  WRITE(*,*)"+'"' + fcname[0][0..-2] + '"' + "\n")

        arss=""

        CHAR_LEN.length.times do |i|
          print("  Allocate(Character("+VALS[CHAR_LEN[i]].to_s+",C_char) :: " + VALS[CHAR_VALS[i]]+"A )\n" )
        end 

        CHAR_LEN.length.times do |i|
          if fcname[0][-4..-1]=="get_"
            if VALS[CHAR_VALS[i]]=="A"
              print("  CALL CHAR_TRIM(" +VALS[CHAR_VALS[i]]+","+VALS[CHAR_VALS[i]]+"A,"+VALS[CHAR_LEN[i]].to_s+")\n")
            end
          else
            print("  CALL CHAR_TRIM(" +VALS[CHAR_VALS[i]]+","+VALS[CHAR_VALS[i]]+"A,"+VALS[CHAR_LEN[i]].to_s+")\n")
          end

        end 

        for i in 0..ars.count-1 do
###          if CHAR_VALS.include?(i) then
          if CHAR_VALS.include?(i)
            arss=arss+VALS[i]+"A,"
          elsif CHAR_LEN.include?(i)
            arss=arss
###                                   print(VALS[i]+",")
          else
            arss=arss+VALS[i]+","
###                                   print(VALS[i]+",")
          end
###            arss=arss+""+VALS[i]+","
#            arss=arss+"fstring("+VALS[i]+"),"
#            print("write(*,*)fstring("+VALS[i]+")\n")
###          else
###            arss=arss+VALS[i]+","
###          end
        end


        if fcname[0][0..-2] == "szfont" then
          print("!  CALL "+fcname[0][0..-2]+"(A,B,C,D,E,F)\n")
        elsif fcname[0][0..-2] == "csgi" then
          print("  AA="+fcname[0][0..-2]+"(C)\n")
        else
          print(corf+fcname[0][0..-2]+"("+arss[0..-2]+")\n")
        end

        CHAR_LEN.length.times do |i|

          if fcname[0][-4..-1]=="get_" || fcname[0][-4..-1]=="set_" then
            if fcname[0][-5..-1]=="cget_" then
              if VALS[CHAR_VALS[i]]=="B" then
                print("  CALL CHAR_TRIM(" +VALS[CHAR_VALS[i]]+"A,"+VALS[CHAR_VALS[i]]+","+VALS[CHAR_LEN[i]].to_s+")\n")
              end
            else
            end
          else
            print("  CALL CHAR_TRIM(" +VALS[CHAR_VALS[i]]+"A,"+VALS[CHAR_VALS[i]]+","+VALS[CHAR_LEN[i]].to_s+")\n")
          end
        end 


        print("END\n\n")
      end
    end
  end




# 例外は小さい単位で捕捉する
rescue SystemCallError => e
  puts %Q(class=[#{e.class}] message=[#{e.message}])
rescue IOError => e
  puts %Q(class=[#{e.class}] message=[#{e.message}])
end

