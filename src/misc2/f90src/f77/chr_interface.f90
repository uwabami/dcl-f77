!-------------------------------------------------
!interface module of chrlib
!-------------------------------------------------
module chr_interface
  interface

    subroutine cladj(chr)                         !文字列の左詰めをおこなう．
      character(len=*), intent(inout) :: chr      !操作をほどこす文字列
    end subroutine
      
    subroutine cradj(chr)                         !文字列の右詰めをおこなう．
      character(len=*), intent(inout) :: chr      !操作をほどこす文字列
    end subroutine
      
    subroutine crvrs(chr)                         !文字列を反転する．
      character(len=*), intent(inout) :: chr      !操作をほどこす文字列
    end subroutine
      
    function lenb(c)                              !先行する空白の数を求める．
      character(len=*), intent(in) :: c           !調べる文字列
      integer lenb                                !先行する空白の数を返す関数値
    end function
      
    function leny(c)                              !先行する空白の数を求める．
      character(len=*), intent(in) :: c           !調べる文字列
      integer leny                                !先行する空白の数を返す関数値
    end function
      
    function lenc(c)                              !後続する空白を除いた文字数を求める．
      character(len=*), intent(in) :: c           !調べる文字列
      integer lenc                                !後続する空白を除いた文字数を返す関数値
    end function
      
    function lenz(c)                              !後続する空白を除いた文字数を求める．
      character(len=*), intent(in) :: c           !調べる文字列
      integer lenz                                !後続する空白を除いた文字数を返す関数値
    end function

  end interface
end module
!chrlib library end ----
      
