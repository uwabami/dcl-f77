!-------------------------------------------------
!interface module of randlib
!-------------------------------------------------
module ran_interface
  interface

    function rngu0(iseed)                         !一様乱数．システムルーチンを使用する
      integer,   intent(inout) :: iseed              !乱数の種
      real rngu0                                  !一様乱数
    end function
      
    function rngu1(iseed)                         !一様乱数．混合合同法
      integer,   intent(inout) :: iseed              !乱数の種
      real rngu1                                  !一様乱数．
    end function
      
    function rngu2(iseed)                         !一様乱数．混合合同法+シャッフル
      integer,   intent(inout) :: iseed              !乱数の種
      real rngu2                                  !一様乱数．
    end function
      
    function rngu3(iseed)                         !一様乱数．numerical recipes の ran3
      integer,   intent(inout) :: iseed              !乱数の種
      real rngu3                                  !一様乱数．
    end function

  end interface
end module
!randlib library end ----
