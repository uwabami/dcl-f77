!-------------------------------------------------
!     interface module of scpack
!-------------------------------------------------
module sc_interface
    interface
!-----------------------------------------------------------------------
!正規化変換 
        subroutine scsvpt(vxmin3,vxmax3,vymin3,vymax3,vzmin3,vzmax3) !ビューポートの設定
            real,      intent(in) :: vxmin3  !直角直線座標のビューポート
            real,      intent(in) :: vxmax3
            real,      intent(in) :: vymin3
            real,      intent(in) :: vymax3
            real,      intent(in) :: vzmin3        
            real,      intent(in) :: vzmax3
        end subroutine

        subroutine scswnd(uxmin3,uxmax3,uymin3,uymax3,uzmin3,uzmax3) !ウインドウの設定．
            real,      intent(in) :: uxmin3      !直角直線座標のウインドウ
            real,      intent(in) :: uxmax3
            real,      intent(in) :: uymin3
            real,      intent(in) :: uymax3
            real,      intent(in) :: uzmin3    
            real,      intent(in) :: uzmax3
        end subroutine

        subroutine scslog(lxlog3,lylog3,lzlog3) !対数軸の設定．
            logical,   intent(in) :: lxlog3      !直角直線座標の対数軸指定
            logical,   intent(in) :: lylog3
            logical,   intent(in) :: lzlog3
        end subroutine

        subroutine scsorg(simfac,vxorg3,vyorg3,vzorg3) !スケーリングファクターと原点の設定
            real,      intent(in) :: simfac      !円筒座標，球座標のスケーリングファクター
            real,      intent(in) :: vxorg3      !円筒座標，球座標の原点の位置
            real,      intent(in) :: vyorg3
            real,      intent(in) :: vzorg3
        end subroutine

        subroutine scstrn(itr3) !変換関数番号の設定
            integer,   intent(in) :: itr3  !変換関数番号
        end subroutine

        subroutine scstrf() !変換関数の確定． 
        end subroutine
        
        !パラメタ参照ルーチン
        subroutine scqvpt(vxmin3,vxmax3,vymin3,vymax3,vzmin3,vzmax3)
            real,      intent(out) :: vxmin3     !直角直線座標のビューポート
            real,      intent(out) :: vxmax3
            real,      intent(out) :: vymin3
            real,      intent(out) :: vymax3
            real,      intent(out) :: vzmin3
            real,      intent(out) :: vzmax3
        end subroutine

        subroutine scqwnd(uxmin3,uxmax3,uymin3,uymax3,uzmin3,uzmax3) 
            real,      intent(out) :: uxmin3     !直角直線座標のウインドウ
            real,      intent(out) :: uxmax3
            real,      intent(out) :: uymin3
            real,      intent(out) :: uymax3
            real,      intent(out) :: uzmin3
            real,      intent(out) :: uzmax3
        end subroutine

        subroutine scqlog(lxlog3,lylog3,lzlog3) 
            logical,   intent(out) :: lxlog3     !直角直線座標の対数軸指定
            logical,   intent(out) :: lylog3
            logical,   intent(out) :: lzlog3
        end subroutine

        subroutine scqorg(simfac,vxorg3,vyorg3,vzorg3) 
            real,      intent(out) :: simfac     !スケーリングファクター
            real,      intent(out) :: vxorg3     !円筒座標，球座標の原点の位置
            real,      intent(out) :: vyorg3
            real,      intent(out) :: vzorg3
        end subroutine

        subroutine scqtrn(itr3) 
            integer,   intent(out) :: itr3        !変換関数番号
        end subroutine
!-----------------------------------------------------------------------------
!透視変換
        subroutine scseye(xeye3, yeye3, zeye3)  !視点の設定．
            real,      intent(in) :: xeye3
            real,      intent(in) :: yeye3
            real,      intent(in) :: zeye3
        end subroutine

        subroutine scsobj(xobj3, yobj3, zobj3)  !焦点の設定
            real,      intent(in) :: xobj3
            real,      intent(in) :: yobj3
            real,      intent(in) :: zobj3
        end subroutine

        subroutine scspln(ixc3, iyc3, sec3)  !2次元平面の割付
            integer,   intent(in) :: ixc3
            integer,   intent(in) :: iyc3
            real,      intent(in) :: sec3
        end subroutine
 
        subroutine scsprj()  !透視変換の確定． 
        end subroutine

        !パラメタ参照ルーチン
        subroutine scqeye(xeye3, yeye3, zeye3)
            real,      intent(out) :: xeye3
            real,      intent(out) :: yeye3
            real,      intent(out) :: zeye3
        end subroutine

        subroutine scqobj(xobj3, yobj3, zobj3) 
            real,      intent(out) :: xobj3
            real,      intent(out) :: yobj3
            real,      intent(out) :: zobj3
        end subroutine
 
         subroutine scqpln(ixc3, iyc3, sec3) 
            integer,   intent(out) :: ixc3
            integer,   intent(out) :: iyc3
            real,      intent(out) :: sec3
        end subroutine
!-----------------------------------------------------------------------------
!ポリライン
        subroutine scplzu(n,upx,upy,upz,index)  !u 座標系で折れ線を描く．  
            integer,   intent(in) :: n                 !配列UPX, UPY, UPZ の長さ
            real,      intent(in), dimension(*) :: upx !折れ線を結ぶ点のU座標
            real,      intent(in), dimension(*) :: upy 
            real,      intent(in), dimension(*) :: upz
            integer,   intent(in) :: index
        end subroutine

        subroutine scplzv(n,vpx,vpy,vpz,index)   !v 座標系で折れ線を描く．    
            integer,   intent(in) :: n                 !配列VPX, VPY, VPZ の長さ
            real,      intent(in), dimension(*) :: vpx !折れ線を結ぶ点のV座標
            real,      intent(in), dimension(*) :: vpy 
            real,      intent(in), dimension(*) :: vpz
            integer,   intent(in) :: index
        end subroutine

        subroutine scplu(n,upx,upy,upz)  !u 座標系で折れ線を描く．  
            integer,   intent(in) :: n                 !配列UPX, UPY, UPZ の長さ
            real,      intent(in), dimension(*) :: upx !折れ線を結ぶ点のR座標
            real,      intent(in), dimension(*) :: upy 
            real,      intent(in), dimension(*) :: upz
        end subroutine

        subroutine scplv(n,vpx,vpy,vpz) !v 座標系で折れ線を描く．
            integer,   intent(in) :: n                 !配列VPX, VPY, VPZ の長さ
            real,      intent(in), dimension(*) :: vpx !折れ線を結ぶ点のV座標系
            real,      intent(in), dimension(*) :: vpy
            real,      intent(in), dimension(*) :: vpz
        end subroutine

        subroutine scspli(index)  !ラインインデクスの設定．
            integer,   intent(in) :: index  !折れ線のラインインデクス 
        end subroutine
        
                 !パラメタ参照ルーチン
        subroutine scqpli(index)
            integer,   intent(out) :: index     !折れ線のラインインデクス 
        end subroutine
!------------------------------------------------------------------------------
! ポリマーカー
        subroutine scpmzu(n,upx,upy,upz,itype,index,rsize)  !u 座標系でマーカー列を描く
            integer,   intent(in) :: n                  !配列UPX, UPY, UPZの長さ
            real,      intent(in), dimension(*) :: upx  !マーカーを打つ点のU座標
            real,      intent(in), dimension(*) :: upy
            real,      intent(in), dimension(*) :: upz    
            integer,   intent(in) :: itype              !マーカータイプ  
            integer,   intent(in) :: index              !マーカーのラインインデクス
            real,      intent(in) :: rsize              !マーカーのサイズ
        end subroutine

        subroutine scpmzv(n,vpx,vpy,vpz,itype,index,rsize)  !v 座標系でマーカー列を描く．    
            integer,   intent(in) :: n                  !配列UPX, UPY, UPZの長さ
            real,      intent(in), dimension(*) :: vpx  !マーカーを打つ点のv座標 
            real,      intent(in), dimension(*) :: vpy
            real,      intent(in), dimension(*) :: vpz
            integer,   intent(in) :: itype              !マーカータイプ  
            integer,   intent(in) :: index              !マーカーのラインインデクス
            real,      intent(in) :: rsize              !マーカーのサイズ
        end subroutine

        subroutine scpmu(n,upx,upy,upz) !u 座標系でマーカー列を描く． 
            integer,   intent(in) :: n                  !配列UPX, UPY, UPZの長さ
            real,      intent(in), dimension(*) :: upx  !マーカーを打つ点のU座標
            real,      intent(in), dimension(*) :: upy    
            real,      intent(in), dimension(*) :: upz
        end subroutine

        subroutine scpmv(n,vpx,vpy,vpz)  !v 座標系でマーカー列を描く
            integer,   intent(in) :: n                  !配列VPX, VPY, VPZの長さ
            real,      intent(in), dimension(*) :: vpx  !マーカーを打つ点のU座標 
            real,      intent(in), dimension(*) :: vpy  
            real,      intent(in), dimension(*) :: vpz
        end subroutine

        subroutine scspmt(itype)  !マーカータイプの設定．    
            integer,   intent(in) :: itype  !マーカータイプ
        end subroutine

        subroutine scspmi(index)  !マーカーのラインインデクスの設定  
            integer,   intent(in) :: index  !マーカーを描く線のラインインデクス 
        end subroutine

        subroutine scspms(rsize)  !マーカーの大きさ設定．      
            real,      intent(in) :: rsize  !マーカーの大きさ
        end subroutine
                
                ! パラメタ参照ルーチン
        subroutine scqpmt(itype)
            integer,   intent(out) :: itype !マーカータイプ
        end subroutine

        subroutine scqpmi(index)
            integer,   intent(out) :: index    !マーカーを描く線のラインインデクス
        end subroutine

        subroutine scqpms(rsize)  
            real,      intent(out) :: rsize    !マーカーの大きさをR-座標系における単位で指定
        end subroutine
!--------------------------------------------------------------------
!トーン
        subroutine sctnzu(upx,upy,upz,itpat1,itpat2)  ! u 座標系で多角形領域の塗りつぶし
            real,      intent(in), dimension(3) :: upx  !三角形を定義する頂点のU座標          
            real,      intent(in), dimension(3) :: upy    
            real,      intent(in), dimension(3) :: upz
            integer,   intent(in) :: itpat1            !表と裏のトーンパターン番号
            integer,   intent(in) :: itpat2
        end subroutine

        subroutine sctnzv(vpx,vpy,vpz,itpat1,itpat2) !v 座標系で多角形領域の塗りつぶし．
            real,      intent(in), dimension(3) :: vpx  !三角形を定義する頂点のV座標
            real,      intent(in), dimension(3) :: vpy    
            real,      intent(in), dimension(3) :: vpz
            integer,   intent(in) :: itpat1                !表と裏のトーンパターン番号
            integer,   intent(in) :: itpat2
        end subroutine

        subroutine sctnu(upx,upy,upz)  !u 座標系で多角形領域の塗りつぶし．
            real,      intent(in), dimension(3) :: upx  !三角形を定義する頂点のU座標
            real,      intent(in), dimension(3) :: upy
            real,      intent(in), dimension(3) :: upz
        end subroutine

        subroutine sctnv(vpx,vpy,vpz) !v 座標系で多角形領域の塗りつぶし．
            real,      intent(in), dimension(3) :: vpx  !三角形を定義する頂点のV座標
            real,      intent(in), dimension(3) :: vpy    
            real,      intent(in), dimension(3) :: vpz
        end subroutine

        subroutine scstnp(itpat1,itpat2)  !トーンパターン番号設定． 
            integer,   intent(in) :: itpat1  !表と裏のトーンパターン番号（初期値は1)
            integer,   intent(in) :: itpat2
        end subroutine

        subroutine scqtnp(itpat1,itpat2)  !現在設定されているトーンパターン番号 
            integer,   intent(out) :: itpat1  !表と裏のトーンパターン番号
            integer,   intent(out) :: itpat2
        end subroutine
    end interface
end module
