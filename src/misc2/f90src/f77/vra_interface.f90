!-------------------------------------------------
!interface module of vralib
!-------------------------------------------------
module vra_interface
  interface

    subroutine vrfna(rx,ry,n,jx,jy,rfna)          !RXにRFNAを作用させてRYに代入する
      real,      intent(in), dimension(*) :: rx   !処理する実数型配列
      real,      intent(out), dimension(*) :: ry
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !配列rx, ryにおいて，処理する配列要素の間隔
      integer,   intent(in) :: jy
      external rfna                               !引数が1個である実数型関数名
    end subroutine
      
    subroutine vrinc(rx,ry,n,jx,jy,rr)            !RXにRRを加えてRYに代入する
      real,      intent(in), dimension(*) :: rx   !処理する実数型配列
      real,      intent(out), dimension(*) :: ry
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !配列rx, ryにおいて，処理する配列要素の間隔
      integer,   intent(in) :: jy
      real,      intent(in) :: rr                 !加える実数値
    end subroutine
      
    subroutine vrfct(rx,ry,n,jx,jy,rr)            !RXにRRを掛けてRYに代入する．     
      real,      intent(in), dimension(*) :: rx   !処理する実数型配列
      real,      intent(out), dimension(*) :: ry
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !配列rx, ryにおいて，処理する配列要素の間隔
      integer,   intent(in) :: jy
      real,      intent(in) :: rr                 !掛ける実数値
    end subroutine
      
    subroutine vrcon(rx,ry,n,jx,jy,rr)            !RRをRYに代入する．   
      real,      intent(in), dimension(*) :: rx   !処理する実数型配列
      real,      intent(out), dimension(*) :: ry
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !配列rx, ryにおいて，処理する配列要素の間隔
      integer,   intent(in) :: jy
      real,      intent(in) :: rr                 !代入する実数値
    end subroutine
      
    subroutine vrset(rx,ry,n,jx,jy)               !RXをRYに代入する． 
      real,      intent(in), dimension(*) :: rx   !処理する実数型配列
      real,      intent(out), dimension(*) :: ry
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !配列rx, ryにおいて，処理する配列要素の間隔
      integer,   intent(in) :: jy
    end subroutine
!-------------------------------------------
    subroutine radd(rx,n,jx,rr)                   !RXにRRを加える．   
      real,      intent(inout), dimension(*) :: rx !処理する実数型配列
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !配列rx, ryにおいて，処理する配列要素の間隔
      real,      intent(in) :: rr                 !加える実数値
    end subroutine
      
    subroutine rmlt(rx,n,jx,rr)                   !RXにRRを掛ける．
      real,      intent(inout), dimension(*) :: rx !処理する実数型配列
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !配列rx, ryにおいて，処理する配列要素の間隔
      real,      intent(in) :: rr                 !掛ける実数値
    end subroutine
      
    subroutine rset(rx,n,jx,rr)                   !RXにRRを代入する．  
      real,      intent(inout), dimension(*) :: rx !処理する実数型配列
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !配列rx, ryにおいて，処理する配列要素の間隔
      real,      intent(in) :: rr                 !代入する実数値
    end subroutine

  end interface
end module
!vralib library end ----
