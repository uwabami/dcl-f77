!-------------------------------------------------
!interface module of fnclib
!-------------------------------------------------
module fnc_interface
  interface

    function igus(rx)                             !RXを越えない最大の整数を返す
      real,      intent(in) :: rx                 !調べる実数値
      integer igus                                !最大の整数 [ rx ]関数値
    end function
      
    function imod(ix,id)                          !数学的な剰余を求める．整数用
      integer,   intent(in) :: ix                 !調べる整数値
      integer,   intent(in) :: id                 !除数
      integer imod                                !剰余関数値
    end function
      
    function rmod(rx,rd)                          !数学的な剰余を求める.実数用
      real,      intent(in) :: rx                 !調べる実数値
      real,      intent(in) :: rd                 !除数
      real rmod                                   !剰余関数値
    end function
      
    function rexp(rx,ib,ie)                       !rx ib^ie を求める．
      real,      intent(in) :: rx
      integer,   intent(in) :: ib
      integer,   intent(in) :: ie
      real rexp                                   !求める関数値
    end function
      
    function rfpi()                               !円周率 π を返す．
      real rfpi                                   !円周率 πを返す実数関数値
    end function
      
    function rd2r(x)                              !角度の変換をおこなう．
      real,      intent(in) :: x                  !「ラジアン」を単位とする実数値
      real rd2r                                   !「ラジアン」を単位とした値を返す実数関数値
    end function
      
    function rr2d(x)                              !角度の変換をおこなう．
      real,      intent(in) :: x                  !「度」 を単位とする実数値
      real rr2d                                   !度」を単位とした値を返す実数関数値
    end function

  end interface
end module
!fnclib library end ----
