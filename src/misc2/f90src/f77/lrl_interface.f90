!-------------------------------------------------
!interface module of lrllib
!-------------------------------------------------
module lrl_interface
  interface

    function lreq(x,y)                            !xとyが等しいかどうか調べる
      real,      intent(in) :: x                  !調べる2つの実数値
      real,      intent(in) :: y
      logical lreq                                !xとyが等しいとき .true.である論理関数値
    end function
      
    function lrne(x,y)                            !xとyが等しくないかどうか調べる．
      real,      intent(in) :: x                  !調べる2つの実数値
      real,      intent(in) :: y
      logical lrne                                !xとyが等しくないとき .true.である論理関数値
    end function
      
    function lrlt(x,y)                            !xがyより小さいかどうか調べる．
      real,      intent(in) :: x                  !調べる2つの実数値
      real,      intent(in) :: y
      logical lrlt                                !xがyより小さいとき .true.である論理関数値
    end function
      
    function lrle(x,y)                            !xがy以下かどうか調べる
      real,      intent(in) :: x                  !調べる2つの実数値
      real,      intent(in) :: y
      logical lrle                                !xがy以下のとき .true.である論理関数値
    end function
      
    function lrgt(x,y)                            !xがyより大きいかどうか調べる
      real,      intent(in) :: x                  !調べる2つの実数値
      real,      intent(in) :: y
      logical lrgt                                !xがyより大きいとき .true.である論理関数値
    end function
      
    function lrge(x,y)                            !xがy以上かどうか調べる
      real,      intent(in) :: x                  !調べる2つの実数値
      real,      intent(in) :: y
      logical lrge                                !xがy以上のとき .true.である論理関数値
    end function
      
    function lreqa(x,y,epsl)                      !xが誤差epslの範囲でy と等しいかどうか調べる
      real,      intent(in) :: x                  !調べる2つの実数値
      real,      intent(in) :: y
      real,      intent(in) :: epsl               !大小関係を調べるときの誤差
      logical lreqa                               !xとyが等しいとき .true.である論理関数値
    end function
      
    function lrnea(x,y,epsl)                      !xが誤差epslの範囲でyと等しくないかどうか調べる
      real,      intent(in) :: x                  !調べる2つの実数値
      real,      intent(in) :: y
      real,      intent(in) :: epsl               !大小関係を調べるときの誤差
      logical lrnea                               !xとyが等しくないとき  .true.である論理関数値
    end function
      
    function lrlta(x,y,epsl)                      !xが誤差epslの範囲でy より小さいかどうか調べる．
      real,      intent(in) :: x                  !調べる2つの実数値
      real,      intent(in) :: y
      real,      intent(in) :: epsl               !大小関係を調べるときの誤差
      logical lrlta                               !xがyより小さいとき .true.である論理関数値
    end function
      
    function lrlea(x,y,epsl)                      !xが誤差epslの範囲でy 以下かどうか調べる．
      real,      intent(in) :: x                  !調べる2つの実数値
      real,      intent(in) :: y
      real,      intent(in) :: epsl               !大小関係を調べるときの誤差
      logical lrlea                               !xがy以下のとき .true.である論理関数値
    end function
      
    function lrgta(x,y,epsl)                      !xが誤差epslの範囲でy より大きいかどうか調べる
      real,      intent(in) :: x                  !調べる2つの実数値
      real,      intent(in) :: y
      real,      intent(in) :: epsl               !大小関係を調べるときの誤差
      logical lrgta                               !xがyより大きいとき .true.である論理関数値
    end function
      
    function lrgea(x,y,epsl)                      !xが誤差epslの範囲でy 以上かどうか調べる
      real,      intent(in) :: x                  !調べる2つの実数値
      real,      intent(in) :: y
      real,      intent(in) :: epsl               !大小関係を調べるときの誤差
      logical lrgea                               !xがy以上のとき .true.である論理関数値
    end function

  end interface
end module
!lrllib library end ----
