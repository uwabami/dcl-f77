!-------------------------------------------------
!interface module of blklib
!-------------------------------------------------
module blk_interface
  interface

    function iblklt(rx,n,rr)                      !rx(i-1)≦ rr<rx(i) を満たすiを求める. ただし rr<rx(1) のとき1, rx(n)≦ rr のときn+1を返す
      real,      intent(in), dimension(*) :: rx   !昇順に並んだ実数型配列
      integer,   intent(in) :: n                  !配列の寸法
      real,      intent(in) :: rr                 !調べる実数値
      integer iblklt                              !rx(i-1)≦ rr<rx(i) を満たすiを返す．
    end function
      
    function iblkle(rx,n,rr)                      !rx(i-1)<rr≦ rx(i) を満たすiを求める.ただし rr≦ rx(1) のとき1, rx(n)<rr のときn+1を返す
      real,      intent(in), dimension(*) :: rx   !昇順に並んだ実数型配列
      integer,   intent(in) :: n                  !配列の寸法
      real,      intent(in) :: rr                 !調べる実数値
      integer iblkle                              !rx(i-1)<rr≦ rx(i) を満たすiを返す． 
    end function
      
    function iblkgt(rx,n,rr)                      !rx(i)<rr≦ rx(i+1) を満たすiを求める．ただし rr≦ rx(1) のとき0, rx(n)<rr のときnを返す
      real,      intent(in), dimension(*) :: rx   !昇順に並んだ実数型配列
      integer,   intent(in) :: n                  !配列の寸法
      real,      intent(in) :: rr                 !調べる実数値
      integer iblkgt                              !rx(i)<rr≦ rx(i+1) を満たすiを返す．
    end function
      
    function iblkge(rx,n,rr)                      !rx(i)≦ rr< rx(i+1) を満たすiを求める.ただし rr<rx(1) のとき0, rx(n)≦ rr のときnを返す
      real,      intent(in), dimension(*) :: rx   !昇順に並んだ実数型配列
      integer,   intent(in) :: n                  !配列の寸法
      real,      intent(in) :: rr                 !調べる実数値
      integer iblkge                              !rx(i)≦ rr< rx(i+1) を満たすiを返す． 
    end function

  end interface
end module
!blklib library end ----
