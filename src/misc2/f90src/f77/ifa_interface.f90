!-------------------------------------------------
!interface module of ifalib
!-------------------------------------------------
module ifa_interface
  interface

    function imax(ix,n,jx)                        !最大値を求める
      integer,   intent(in), dimension(*) :: ix   !処理する整数型配列
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !処理する配列要素の間隔
      integer imax                                !最大値を与える関数値
    end function
      
    function imin(ix,n,jx)                        !最小値を求める
      integer,   intent(in), dimension(*) :: ix   !処理する整数型配列
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !処理する配列要素の間隔
      integer imin                                !最小値を与える関数値
    end function
      
    function isum(ix,n,jx)                        !総和をもとめる
      integer,   intent(in), dimension(*) :: ix   !処理する整数型配列
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !処理する配列要素の間隔
      integer isum                                !総和を与える関数値
    end function

  end interface
end module
!ifalib library end ----
