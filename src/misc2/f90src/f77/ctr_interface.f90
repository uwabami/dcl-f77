!-------------------------------------------------
!interface module of ctrlib
!-------------------------------------------------
module ctr_interface
  interface

    subroutine ct2pc(r,theta,x,y)                 !2次元極座標と直角座標の変換をする．
      real,      intent(in) :: r                  !極座標
      real,      intent(in) :: theta
      real,      intent(out) :: x                 !直角座標
      real,      intent(out) :: y
    end subroutine
      
    subroutine ct2cp(x,y,r,theta)                 !2次元極座標と直角座標の変換をする．
      real,      intent(in) :: x                  !直角座標
      real,      intent(in) :: y
      real,      intent(out) :: r                 !極座標
      real,      intent(out) :: theta
    end subroutine
      
    subroutine ct2ec(u,v,x,y)                     !楕円座標と直角座標の変換をする．
      real,      intent(in) :: u                  !楕円座標
      real,      intent(in) :: v
      real,      intent(out) :: x                 !直角座標
      real,      intent(out) :: y
    end subroutine
      
    subroutine ct2bc(u,v,x,y)                     !双極座標と直角座標の変換をする．
      real,      intent(in) :: u                  !双極座標
      real,      intent(in) :: v
      real,      intent(out) :: x                 !直角座標
      real,      intent(out) :: y
    end subroutine
      
    subroutine ct2hc(u,v,x,y)                     !直角双曲線座標と直角座標の変換をする．
      real,      intent(in) :: u                  !直角双曲線座標
      real,      intent(in) :: v
      real,      intent(out) :: x                 !直角座標
      real,      intent(out) :: y
    end subroutine
      
    subroutine ct2ch(x,y,u,v)                     !直角双曲線座標と直角座標の変換をする．
      real,      intent(in) :: x                  !直角座標
      real,      intent(in) :: y
      real,      intent(out) :: u                 !直角双曲線座標
      real,      intent(out) :: v
    end subroutine
      
    subroutine ct3sc(r,theta,phi,x,y,z)           !3次元球面座標と直角座標の変換をする．
      real,      intent(in) :: r                  !極座標
      real,      intent(in) :: theta
      real,      intent(in) :: phi
      real,      intent(out) :: x                 !直角座標
      real,      intent(out) :: y
      real,      intent(out) :: z
    end subroutine
      
    subroutine ct3cs(x,y,z,r,theta,phi)           !3次元球面座標と直角座標の変換をする．
      real,      intent(in) :: x                  !直角座標
      real,      intent(in) :: y
      real,      intent(in) :: z
      real,      intent(out) :: r                 !極座標
      real,      intent(out) :: theta
      real,      intent(out) :: phi
    end subroutine
      
    subroutine cr2c(theta,x0,y0,x1,y1)            !2次元直角座標を回転する．
      real,      intent(in) :: theta              !回転角
      real,      intent(in) :: x0                 !回転前の座標値
      real,      intent(in) :: y0
      real,      intent(out) :: x1                !回転後の座標値
      real,      intent(out) :: y1
    end subroutine
      
    subroutine cr3c(theta,phi,psi,x0,y0,z0,x1,y1,z1) !3次元直角座標を回転する．
      real,      intent(in) :: theta              !euler の回転角 (θ, φ, ψ)
      real,      intent(in) :: phi
      real,      intent(in) :: psi
      real,      intent(in) :: x0                 !回転前の座標値
      real,      intent(in) :: y0
      real,      intent(in) :: z0
      real,      intent(out) :: x1                !回転後の座標値
      real,      intent(out) :: y1
      real,      intent(out) :: z1
    end subroutine
      
    subroutine cr3s(theta,phi,psi,theta0,phi0,theta1,phi1) !球面座標を回転する．
      real,      intent(in) :: theta              !euler の回転角 (θ, φ, ψ)
      real,      intent(in) :: phi
      real,      intent(in) :: psi
      real,      intent(in) :: theta0             !回転前の座標値
      real,      intent(in) :: phi0
      real,      intent(out) :: theta1            !回転後の座標値
      real,      intent(out) :: phi1
    end subroutine

  end interface
end module
!ctrlib library end ----
