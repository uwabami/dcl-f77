!-------------------------------------------------
!interface module of fmtlib
!-------------------------------------------------
module fmt_interface
  interface

    subroutine chval(cfmt,val,cval)               !数値を文字列化する
      character(len=*), intent(in) :: cfmt        !文字書式仕様あるい はchval が 解釈するオプション
      real,      intent(in) :: val                !文字化する実数値
      character(len=*), intent(out) :: cval       !文字化された実数値を返す
    end subroutine

  end interface
end module
!fmtlib library end ----
