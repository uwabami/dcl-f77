!-------------------------------------------------
!interface module of intrlib
!-------------------------------------------------
module intr_interface
  interface

    subroutine vrintr(rx,n,jx)                    !実数型配列の補間をする
      real,      intent(inout), dimension(*) :: rx !処理する実数型の配列
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !処理する配列要素の間隔
    end subroutine
      
    subroutine vcintr(cx,n,jx)                    !複素数型配列の補間をする
      complex,   intent(inout), dimension(*) :: cx !処理する複素数型の配列
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !処理する配列要素の間隔
    end subroutine

  end interface
end module
!intrlib library end ----
