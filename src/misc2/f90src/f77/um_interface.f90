!-------------------------------------------------
!interface module of umpack
!-------------------------------------------------
module um_interface
  interface

    subroutine umscnt(uxc, uyc, rot)              !投影面の「接点」を指定する．
      real,      intent(in) :: uxc                !接点の経度，緯度
      real,      intent(in) :: uyc
      real,      intent(in) :: rot                !中央の経線の傾き
    end subroutine
      
    subroutine umscwd(uxc, uyc, r)                !円形のウィンドウを設定する．
      real,      intent(in) :: uxc                !中心の経度，緯度
      real,      intent(in) :: uyc
      real,      intent(in) :: r                  !ウィンドウ半径
    end subroutine
      
    subroutine umspnt(n, uxp, uyp)                !地図に含める点を指定する．
      integer,   intent(in) :: n                  !点の数
      real,      intent(in), dimension(n) :: uxp  !点の経度，緯度
      real,      intent(in), dimension(n) :: uyp
    end subroutine
      
    subroutine umpfit()                           !地図投影の変換関数のパラメタを適切に決める．
    end subroutine
      
    subroutine umpglb()                           !地図の境界線（縁）と緯度線,経度線を描く．
    end subroutine
      
    subroutine umpgrd()                           !緯度線・経度線を描く．
    end subroutine
      
    subroutine umplim()                           !地図の境界線（縁）を描く．
    end subroutine
      
    subroutine umpmap(cdsn)                       !各種地図情報を描く．
      character(len=*), intent(in) :: cdsn        !地図情報ファイル名
    end subroutine
			
    subroutine umfmap(cdsn)                       !陸地塗り潰し
      character(len=*), intent(in) :: cdsn        !地図情報ファイル名
    end subroutine
			
    subroutine umpget(cp,ipara)                   !内部変数を参照する
      character(len=*), intent(in) :: cp          !内部変数の名前
      integer,   intent(out) :: ipara             !内部変数の値
    end subroutine
      
    subroutine umiget(cp,ipara)                   !内部変数を参照する
      character(len=*), intent(in) :: cp          !内部変数の名前
      integer,   intent(out) :: ipara             !内部変数の値
    end subroutine
      
    subroutine umrget(cp,ipara)                   !内部変数を参照する
      character(len=*), intent(in) :: cp          !内部変数の名前
      real,      intent(out) :: ipara             !内部変数の値
    end subroutine
      
    subroutine umlget(cp,ipara)                   !内部変数を参照する
      character(len=*), intent(in) :: cp          !内部変数の名前
      logical,   intent(out) :: ipara             !内部変数の値
    end subroutine
      
    subroutine umpset(cp,ipara)                   !内部変数を変更する
      character(len=*), intent(in) :: cp          !内部変数の名前
      integer,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine umiset(cp,ipara)                   !内部変数を変更する
      character(len=*), intent(in) :: cp          !内部変数の名前
      integer,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine umrset(cp,ipara)                   !内部変数を変更する
      character(len=*), intent(in) :: cp          !内部変数の名前
      real,      intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine umlset(cp,ipara)                   !内部変数を変更する
      character(len=*), intent(in) :: cp          !内部変数の名前
      logical,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine umpstx(cp,ipara)                   !内部変数を変更する
      character(len=*), intent(in) :: cp          !内部変数の名前
      integer,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine umistx(cp,ipara)                   !内部変数を変更する
      character(len=*), intent(in) :: cp          !内部変数の名前
      integer,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine umrstx(cp,ipara)                   !内部変数を変更する
      character(len=*), intent(in) :: cp          !内部変数の名前
      real,      intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine umlstx(cp,ipara)                   !内部変数を変更する
      character(len=*), intent(in) :: cp          !内部変数の名前
      logical,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine umpqnp(ncp)                        !内部変数の総数を求める
      integer,   intent(out) :: ncp
    end subroutine
      
    subroutine umpqid(cp,idx)                     !内部変数cpの位置を求める．
      character(len=*), intent(in) :: cp
      integer,   intent(out) :: idx
    end subroutine
      
    subroutine umpqcp(idx,cp)                     !idxの位置にある内部変数の名前を参照する
      character(len=*), intent(out) :: cp
      integer,   intent(in) :: idx
    end subroutine
      
    subroutine umpqvl(idx,ipara)                  !idxの位置にある内部変数の値を参照する
      integer,   intent(in) :: idx
      integer,   intent(out) :: ipara
    end subroutine
      
    subroutine umpsvl(idx,ipara)                  !idxの位置にある内部変数の値を変更する
      integer,   intent(in) :: idx
      integer,   intent(in) :: ipara
    end subroutine

  end interface
end module
!umpack library end ----
