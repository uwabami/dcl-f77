module dcl_f77

!----- math1 -----
  use blk_interface
  use chr_interface
  use ctr_interface
  use fnc_interface
  use gnm_interface
  use ifa_interface
  use ind_interface
  use int_interface
  use lrl_interface
  use map_interface
  use os_interface
  use rfa_interface
  use rfb_interface
  use sub_interface
  use sys_interface
  use via_interface
  use vib_interface
  use vra_interface
  use vrb_interface
  use xfc_interface

!----- math2 -----
  use fft_interface
  use ode_interface
  use sht_interface
  use vst_interface
  use intr_interface
  use rnm_interface

!----- misc1 -----
  use bit_interface
  use chg_interface
  use chk_interface
  use chn_interface
  use fmt_interface
  use dat_interface
  use tim_interface
  use mis_interface
  use clc_interface
  use fio_interface
  use ran_interface
  use hex_interface
  use rea_interface

!---- graph1 -----
  use sz_interface
  use sg_interface
  use sl_interface
  use st_interface
  use sw_interface
  use sc_interface

!---- graph2 -----
  use gr_interface
  use uc_interface
  use ud_interface
  use ue_interface
  use ug_interface
  use uh_interface
  use ul_interface
  use um_interface
  use us_interface
  use uu_interface
  use uv_interface
  use uw_interface
  use ux_interface
  use uy_interface
  use uz_interface


end module
