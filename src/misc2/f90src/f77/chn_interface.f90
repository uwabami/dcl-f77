!-------------------------------------------------
!interface module of chnlib
!-------------------------------------------------
module chn_interface
  interface

    subroutine chngc(ch,ca,cb)                    !指定された文字列を見つけだし， その部分を別の文字列でおきかえる．
      character(len=*), intent(inout) :: ch          !調べる文字列
      character(len=*), intent(in) :: ca          !見つけ出す文字列
      character(len=*), intent(in) :: cb          !おきかえる文字列
    end subroutine
      
    subroutine chngi(ch,ca,ii,cfmt)               !指定された文字列を見つけだし， その部分を書式にしたがって整数でおきかえる．
      character(len=*), intent(inout) :: ch          !調べる文字列
      character(len=*), intent(in) :: ca          !見つけ出す文字列
      integer,   intent(in) :: ii                 !おきかえる整数値
      character(len=*), intent(in) :: cfmt        !文字書式仕様． たとえば，'(i4)', '(e12.4)'と指定する
    end subroutine
      
    subroutine chngr(ch,ca,rr,cfmt)               !指定された文字列を見つけだし， その部分を書式にしたがって実数でおきかえる．
      character(len=*), intent(inout) :: ch          !調べる文字列
      character(len=*), intent(in) :: ca          !見つけ出す文字列
      real,      intent(in) :: rr                 !おきかえる実数値
      character(len=*), intent(in) :: cfmt        !文字書式仕様． たとえば，'(i4)', '(e12.4)'と指定する
    end subroutine

  end interface
end module
!chnlib library end ----
