!-------------------------------------------------
!interface module of intlib
!-------------------------------------------------
module int_interface
  interface

    function irlt(rx)                             !rxより小さい最大の整数を求める
      real,      intent(in) :: rx                 !調べる実数値
      integer irlt                                !rxより小さい最大の整数関数値
    end function
      
    function irle(rx)                             !rx以下の最大の整数を求める
      real,      intent(in) :: rx                 !調べる実数値
      integer irle                                !rx以下の最大の整数関数値
    end function
      
    function irgt(rx)                             !rxより大きい最小の整数を求める
      real,      intent(in) :: rx                 !調べる実数値
      integer irgt                                !rxより大きい最大の整数関数値
    end function
      
    function irge(rx)                             !rx以上の最小の整数を求める
      real,      intent(in) :: rx                 !調べる実数値
      integer irge                                !rx以上の最大の整数関数値
    end function

  end interface
end module
!intlib library end ----
