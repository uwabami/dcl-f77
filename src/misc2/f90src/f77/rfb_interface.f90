!-------------------------------------------------
!interface module of rfblib
!-------------------------------------------------
module rfb_interface
  interface

    function rprd(rx,ry,n,jx,jy)                  !内積をもとめる．
      real,      intent(in), dimension(*) :: rx   !処理する実数型配列
      real,      intent(in), dimension(*) :: ry
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !配列rx, ryにおいて，処理する配列要素の間隔
      integer,   intent(in) :: jy
      real rprd                                   !内積を与える関数値
    end function
      
    function rcov(rx,ry,n,jx,jy)                  !共分散をもとめる．
      real,      intent(in), dimension(*) :: rx   !処理する実数型配列
      real,      intent(in), dimension(*) :: ry
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !配列rx, ryにおいて，処理する配列要素の間隔
      real rcov                                   !共分散を与える関数値
    end function
      
    function rcor(rx,ry,n,jx,jy)                  !相関係数をもとめる．
      real,      intent(in), dimension(*) :: rx   !処理する実数型配列
      real,      intent(in), dimension(*) :: ry
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !配列rx, ryにおいて，処理する配列要素の間隔
      real rcor                                   !相関係数を与える関数値
    end function

  end interface
end module
!rfblib library end ----
