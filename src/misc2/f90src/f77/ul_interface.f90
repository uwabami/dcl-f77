!-------------------------------------------------d
!interface module of ulpack
!-------------------------------------------------
module ul_interface
  interface
    subroutine ulxlog(cside,nlbl,nticks)          !対数座標軸を描く．
      character(len=1), intent(in) :: cside       !座標軸を描く場所を指定する
      integer,   intent(in) :: nlbl               !1桁の範囲に描くラベルの数
      integer,   intent(in) :: nticks             !1桁の範囲に描く目盛の数
    end subroutine
      
    subroutine ulylog(cside,nlbl,nticks)          !対数座標軸を描く．
      character(len=1), intent(in) :: cside       !座標軸を描く場所を指定する
      integer,   intent(in) :: nlbl               !1桁の範囲に描くラベルの数
      integer,   intent(in) :: nticks             !1桁の範囲に描く目盛の数
    end subroutine
      
    subroutine ulxsfm(cfmt)                       !フォーマットを設定する
      character(len=*), intent(in) :: cfmt        !文字書式仕様あるいはサブルーチンchvalが解釈するオプション
    end subroutine
      
    subroutine ulysfm(cfmt)                       !フォーマットを設定する
      character(len=*), intent(in) :: cfmt        !文字書式仕様あるいはサブルーチンchvalが解釈するオプション
    end subroutine
      
    subroutine ulxqfm(cfmt)                       !フォーマット参照する
      character(len=*), intent(out) :: cfmt
    end subroutine
      
    subroutine ulyqfm(cfmt)                       !フォーマット参照する
      character(len=*), intent(out) :: cfmt
    end subroutine
      
    subroutine ulsxbl(bl,nbl)                     !x軸ラベルを描く位置を指定する．
      real,      intent(in), dimension(nbl) :: bl !1桁の範囲に描くラベルの位置
      integer,   intent(in) :: nbl                !配列blの長さ
    end subroutine
      
    subroutine ulsybl(bl,nbl)                     !y軸ラベルを描く位置を指定する．
      real,      intent(in), dimension(nbl) :: bl !1桁の範囲に描くラベルの位置
      integer,   intent(in) :: nbl                !配列blの長さ
    end subroutine
      
    subroutine ulqxbl(bl,nbl)                     !x軸ラベルを描く位置を参照する．
      real,      intent(out), dimension(nbl) :: bl
      integer,   intent(in) :: nbl
    end subroutine
      
    subroutine ulqybl(bl,nbl)                     !y軸ラベルを描く位置を参照する．
      real,      intent(out), dimension(nbl) :: bl
      integer,   intent(in) :: nbl
    end subroutine
      
    subroutine ulpget(cp,ipara)                   !内部変数を参照
      character(len=*), intent(in) :: cp          !内部変数の名前
      integer,   intent(out) :: ipara             !内部変数の値
    end subroutine
      
    subroutine uliget(cp,ipara)                   !内部変数を参照
      character(len=*), intent(in) :: cp          !内部変数の名前
      integer,   intent(out) :: ipara             !内部変数の値
    end subroutine
      
    subroutine ulrget(cp,ipara)                   !内部変数を参照
      character(len=*), intent(in) :: cp          !内部変数の名前
      real,      intent(out) :: ipara             !内部変数の値
    end subroutine
      
    subroutine ullget(cp,ipara)                   !内部変数を参照
      character(len=*), intent(in) :: cp          !内部変数の名前
      logical,   intent(out) :: ipara             !内部変数の値
    end subroutine
      
    subroutine ulpset(cp,ipara)                   !内部変数を設定
      character(len=*), intent(in) :: cp          !内部変数の名前
      integer,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine uliset(cp,ipara)                   !内部変数を設定
      character(len=*), intent(in) :: cp          !内部変数の名前
      integer,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine ulrset(cp,ipara)                   !内部変数を設定
      character(len=*), intent(in) :: cp          !内部変数の名前
      real,      intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine ullset(cp,ipara)                   !内部変数を設定
      character(len=*), intent(in) :: cp          !内部変数の名前
      logical,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine ulpstx(cp,ipara)                   !内部変数を設定
      character(len=*), intent(in) :: cp          !内部変数の名前
      integer,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine ulistx(cp,ipara)                   !内部変数を設定
      character(len=*), intent(in) :: cp          !内部変数の名前
      integer,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine ulrstx(cp,ipara)                   !内部変数を設定
      character(len=*), intent(in) :: cp          !内部変数の名前
      real,      intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine ullstx(cp,ipara)                   !内部変数を設定
      character(len=*), intent(in) :: cp          !内部変数の名前
      logical,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine ulpqnp(ncp)                        !内部変数の総数を求める
      integer,   intent(out) :: ncp
    end subroutine
      
    subroutine ulpqid(cp,idx)                     !内部変数cpの位置を求める．
      character(len=*), intent(in) :: cp
      integer,   intent(out) :: idx
    end subroutine
      
    subroutine ulpqcp(idx,cp)                     !idxの位置にある内部変数の名前を参照する
      character(len=*), intent(out) :: cp
      integer,   intent(in) :: idx
    end subroutine
      
    subroutine ulpqvl(idx,ipara)                  !idxの位置にある内部変数の値を参照する
      integer,   intent(in) :: idx
      integer,   intent(out) :: ipara
    end subroutine
      
    subroutine ulpsvl(idx,ipara)                  !idxの位置にある内部変数の値を変更する
      integer,   intent(in) :: idx
      integer,   intent(in) :: ipara
    end subroutine

  end interface
end module
!ulpack library end ----
