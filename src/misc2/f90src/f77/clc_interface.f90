!-------------------------------------------------
!interface module of clcklib
!-------------------------------------------------
module clc_interface
  interface

    subroutine clckst()                           !時刻を初期化する．
    end subroutine
      
    subroutine clckgt(time)                       !clckstが呼ばれてからのcpu時間を求める．
      real,      intent(out) :: time              !clckstが呼ばれてからの時間, 単位は秒
    end subroutine
      
    subroutine clckdt(dt)                         !時間の分解能を返す．
      real,      intent(out) :: dt                !時間の分解能．単位は秒
    end subroutine

  end interface
end module
      
