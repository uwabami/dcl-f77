!-------------------------------------------------
!interface module of sublib
!-------------------------------------------------
module sub_interface
  interface

    subroutine vignn(ix,n,jx)                     !自然数列を生成して整数型配列で返す．
      integer,   intent(out), dimension(*) :: ix   !自然数列を返す整数型配列
      integer,   intent(in) :: n                  !数列を構成する配列要素の個数
      integer,   intent(in) :: jx                 !数列を構成する配列要素の間隔
    end subroutine
      
    subroutine vrgnn(rx,n,jx)                     !自然数列を生成して実数型配列で返す．
      real,      intent(out), dimension(*) :: rx   !自然数列を返す実数型配列
      integer,   intent(in) :: n                  !数列を構成する配列要素の個数
      integer,   intent(in) :: jx                 !数列を構成する配列要素の間隔
    end subroutine
      
    subroutine dxfloc(nd,ns,np,ncp)               !添字の値から配列要素の位置を求める．
      integer,   intent(in) :: nd                 !配列の次元数
      integer,   intent(in), dimension(*) :: ns   !配列の寸法を収めた長さndの 整数型配列
      integer,   intent(in), dimension(*) :: np   !配列の添字を収めた長さ ndの整数型配列
      integer,   intent(out) :: ncp               !配列要素の位置． dxflocでは出力パラメータ
    end subroutine
      
    subroutine dxiloc(nd,ns,np,ncp)               !: 配列要素の位置から添字の値を求める
      integer,   intent(in) :: nd                 !配列の次元数
      real,      intent(in), dimension(*) :: ns   !配列の寸法を収めた長さndの 整数型配列
      integer,   intent(out), dimension(*) :: np  !配列の添字を収めた長さ ndの整数型配列
      integer,   intent(in) :: ncp                !配列要素の位置． dxflocでは出力パラメータ
    end subroutine

  end interface
end module
!sublib library end ----
