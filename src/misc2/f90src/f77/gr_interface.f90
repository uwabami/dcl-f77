!-------------------------------------------------
!interface module of grpack
!-------------------------------------------------
module gr_interface
  interface
!-------------------------------------------------
!コントロール
    subroutine gropn(iws)                         !図形出力装置をオープン
      integer,   intent(in) :: iws                !ワークステーション番号
    end subroutine
      
    subroutine grfrm                            !新しい作画領域を設定する
    end subroutine
      
    subroutine grfig                            !初期化
    end subroutine
      
    subroutine grcls                            !図形出力装置をクローズ.
    end subroutine
!-------------------------------------------------
!正規化変換
    subroutine grsvpt(vxmin,vxmax,vymin,vymax)    !正規化変換のためのビューポートを設定．
      real,      intent(in) :: vxmin              !ビューポート
      real,      intent(in) :: vxmax
      real,      intent(in) :: vymin
      real,      intent(in) :: vymax
    end subroutine
      
    subroutine grswnd(uxmin,uxmax,uymin,uymax)    !正規化変換のためのパラメータを設定．
      real,      intent(in) :: uxmin              !ウインドウ
      real,      intent(in) :: uxmax
      real,      intent(in) :: uymin
      real,      intent(in) :: uymax
    end subroutine
      
    subroutine grssim(simfac,vxoff,vyoff)         !正規化変換のためのパラメータを設定．
      real,      intent(in) :: simfac             !相似変換のスケーリングファクター
      real,      intent(in) :: vxoff              !原点のオフセット
      real,      intent(in) :: vyoff
    end subroutine
      
    subroutine grsmpl(plx,ply,plrot)              !正規化変換のためのパラメータを設定．
      real,      intent(in) :: plx                !地図投影の際の回転角
      real,      intent(in) :: ply
      real,      intent(in) :: plrot
    end subroutine
      
    subroutine grstrn(itr)                        !正規化変換の変換関数番号を指定．
      integer,   intent(in) :: itr                !変換関数番号
    end subroutine
      
    subroutine grstrf()                           !正規化変換を確定．
    end subroutine

  end interface
end module
!grpack library end ----
