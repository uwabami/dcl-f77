!-------------------------------------------------
!interface module of uwpack
!-------------------------------------------------
module uw_interface
  interface

    subroutine uwsgxa(xp,nx)                      !格子点配列の格子点座標を各座標値で設定する．
      real,      intent(in), dimension(nx) :: xp  !長さnxの配列． x座標をu座標系の値で指定する
      integer,   intent(in) :: nx                 !配列xpの長さ
    end subroutine
      
    subroutine uwqgxa(xp,nx)                      !現在設定されている格子点の座標値
      real,      intent(out), dimension(nx) :: xp
      integer,   intent(in) :: nx
    end subroutine
      
    subroutine uwsgya(yp,ny)                      !格子点配列の格子点座標を各座標値で設定する．
      real,      intent(in), dimension(ny) :: yp  !長さnyの配列． y座標をu座標系の値で指定する
      integer,   intent(in) :: ny                 !配列xpの長さ
    end subroutine
      
    subroutine uwqgya(yp,ny)                      !現在設定されている格子点の座標値
      real,      intent(out), dimension(ny) :: yp
      integer,   intent(in) :: ny
    end subroutine
      
    subroutine uwsgxb(uxmin,uxmax,nx)             !格子点座標を最小値,最大値,格子点数で設定する．
      real,      intent(in) :: uxmin              !u座標系におけるx座標の最小値
      real,      intent(in) :: uxmax              !u座標系におけるx座標の最大値
      integer,   intent(in) :: nx                 !x方向の格子点数．
    end subroutine
      
    subroutine uwsgyb(uymin,uymax,ny)             !格子点座標を最小値,最大値,格子点数で設定する．
      real,      intent(in) :: uymin              !u座標系におけるy座標の最小値
      real,      intent(in) :: uymax              !u座標系におけるy座標の最大値
      integer,   intent(in) :: ny                 !y方向の格子点数．
    end subroutine
      
    subroutine uwqgxb(uxmin,uxmax,nx)             !格子点座標の最小値,最大値,格子点数の参照
      real,      intent(out) :: uxmin             !u座標系におけるx座標の最小値
      real,      intent(out) :: uxmax             !u座標系におけるx座標の最大値
      integer,   intent(out) :: nx                 !x方向の格子点数．
    end subroutine
      
    subroutine uwqgyb(uymin,uymax,ny)             !格子点座標の最小値,最大値,格子点数の参照
      real,      intent(out) :: uymin             !u座標系におけるy座標の最小値
      real,      intent(out) :: uymax             !u座標系におけるy座標の最大値
      integer,   intent(out) :: ny                 !y方向の格子点数
    end subroutine
      
    subroutine uwqgxz(lsetx)                      !格子点が定義されているかどうかを調べる．
      logical,   intent(out) :: lsetx             !格子点が定義されているかどうかを調べる論理変数
    end subroutine
      
    subroutine uwqgyz(lsety)                      !格子点が定義されているかどうかを調べる．
      logical,   intent(out) :: lsety             !格子点が定義されているかどうかを調べる論理変数
    end subroutine
      
    subroutine uwsgxz(lsetx)
      logical,   intent(in) :: lsetx
    end subroutine
      
    subroutine uwsgyz(lsety)
      logical,   intent(in) :: lsety
    end subroutine
      
    function ruwgx(ix)                            !格子点の座標値を返す．
      integer,   intent(in) :: ix                 !格子点の番号
      real ruwgx                                  !格子点のu座標系の座標値
    end function
      
    function ruwgy(iy)                            !格子点の座標値を返す．
      integer,   intent(in) :: iy                 !格子点の番号
      real ruwgy                                  !格子点のu座標系の座標値
    end function
      
    function iuwgx(ux)                            !座標値に最も近い格子番号を返す．
      real,      intent(in) :: ux                 !座標値
      integer iuwgx                               !格子点番号
    end function
      
    function iuwgy(uy)                            !座標値に最も近い格子番号を返す．
      real,      intent(in) :: uy                 !格子点の番号
      integer iuwgy                               !格子点番号
    end function
      
    subroutine uwqgxi(ux,iux,fracx)               !与えられた座標値よりも小さい最大の格子点番号を返す． 次のグリッドとの間の位置も返す．
      real,      intent(in) :: ux                 !座標値
      integer,   intent(out) :: iux               !格子点番号
      real,      intent(out) :: fracx             !次の格子点との間での位置．
    end subroutine
      
    subroutine uwqgyi(uy,iuy,fracy)               !与えられた座標値よりも小さい最大の格子点番号を返す． 次のグリッドとの間の位置も返す．
      real,      intent(in) :: uy                 !座標値
      integer,   intent(out) :: iuy               !格子点番号
      real,      intent(out) :: fracy             !次の格子点との間での位置
    end subroutine
      
    subroutine uwdflt(nx,ny)                      !等間隔グリッドを設定する．
      integer,   intent(in) :: nx                 !グリッド数
      integer,   intent(in) :: ny                 !グリッド数
    end subroutine

  end interface
end module
!uwpack library end ----
