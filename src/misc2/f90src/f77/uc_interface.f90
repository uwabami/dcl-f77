!-------------------------------------------------
!interface module of ucpack
!-------------------------------------------------
module uc_interface
  interface
    subroutine ucxacl(cside,jd0,nd)               !日・月・年に関する座標軸を描く．
      character(len=1), intent(in) :: cside       !座標軸を書く場所を指定する
      integer,   intent(in) :: jd0                !ucで0に相当する場所の日付を指定する
      integer,   intent(in) :: nd                 !作画する日数を指定する
    end subroutine
      
    subroutine ucyacl(cside,jd0,nd)               !日・月・年に関する座標軸を描く．
      character(len=1), intent(in) :: cside       !座標軸を書く場所を指定する
      integer,   intent(in) :: jd0                !ucで0に相当する場所の日付を指定する
      integer,   intent(in) :: nd                 !作画する日数を指定する
    end subroutine
      
    subroutine ucxady(cside,jd0,nd)               !日に関する座標軸を描く．
      character(len=1), intent(in) :: cside       !座標軸を書く場所を指定する
      integer,   intent(in) :: jd0                !ucで0に相当する場所の日付を指定する
      integer,   intent(in) :: nd                 !作画する日数を指定する
    end subroutine
      
    subroutine ucyady(cside,jd0,nd)               !日に関する座標軸を描く．
      character(len=1), intent(in) :: cside       !座標軸を書く場所を指定する
      integer,   intent(in) :: jd0                !ucで0に相当する場所の日付を指定する
      integer,   intent(in) :: nd                 !作画する日数を指定する
    end subroutine
      
    subroutine ucxamn(cside,jd0,nd)               !月に関する座標軸を描く．
      character(len=1), intent(in) :: cside       !座標軸を書く場所を指定する
      integer,   intent(in) :: jd0                !ucで0に相当する場所の日付を指定する
      integer,   intent(in) :: nd                 !作画する日数を指定する
    end subroutine
      
    subroutine ucyamn(cside,jd0,nd)               !月に関する座標軸を描く．
      character(len=1), intent(in) :: cside       !座標軸を書く場所を指定する
      integer,   intent(in) :: jd0                !ucで0に相当する場所の日付を指定する
      integer,   intent(in) :: nd                 !作画する日数を指定する
    end subroutine
      
    subroutine ucxayr(cside,jd0,nd)               !年に関する座標軸を描く．
      character(len=1), intent(in) :: cside       !座標軸を書く場所を指定する
      integer,   intent(in) :: jd0                !ucで0に相当する場所の日付を指定する
      integer,   intent(in) :: nd                 !作画する日数を指定する
    end subroutine
      
    subroutine ucyayr(cside,jd0,nd)               !年に関する座標軸を描く．
      character(len=1), intent(in) :: cside       !座標軸を書く場所を指定する
      integer,   intent(in) :: jd0                !ucで0に相当する場所の日付を指定する
      integer,   intent(in) :: nd                 !作画する日数を指定する
    end subroutine
      
    subroutine ucpget(cp,ipara)                   !内部変数を参照する
      character(len=8), intent(in) :: cp          !内部変数の名前
      integer,   intent(out) :: ipara             !内部変数の値
    end subroutine
      
    subroutine uciget(cp,ipara)                   !内部変数を参照する
      character(len=8), intent(in) :: cp          !内部変数の名前
      integer,   intent(out) :: ipara             !内部変数の値
    end subroutine
      
    subroutine ucrget(cp,ipara)                   !内部変数を参照する
      character(len=8), intent(in) :: cp          !内部変数の名前
      real,   intent(out) :: ipara             !内部変数の値
    end subroutine
      
    subroutine uclget(cp,ipara)                   !内部変数を参照する
      character(len=8), intent(in) :: cp          !内部変数の名前
      logical,   intent(out) :: ipara             !内部変数の値
    end subroutine
      
    subroutine ucpset(cp,ipara)                   !内部変数を変更する
      character(len=8), intent(in) :: cp          !内部変数の名前
      integer,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine uciset(cp,ipara)                   !内部変数を変更する
      character(len=8), intent(in) :: cp          !内部変数の名前
      integer,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine ucrset(cp,ipara)                   !内部変数を変更する
      character(len=8), intent(in) :: cp          !内部変数の名前
      real,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine uclset(cp,ipara)                   !内部変数を変更する
      character(len=8), intent(in) :: cp          !内部変数の名前
      logical,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine ucpstx(cp,ipara)                   !内部変数を変更する
      character(len=8), intent(in) :: cp          !内部変数の名前
      integer,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine ucistx(cp,ipara)                   !内部変数を変更する
      character(len=8), intent(in) :: cp          !内部変数の名前
      integer,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine ucrstx(cp,ipara)                   !内部変数を変更する
      character(len=8), intent(in) :: cp          !内部変数の名前
      real,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine uclstx(cp,ipara)                   !内部変数を変更する
      character(len=8), intent(in) :: cp          !内部変数の名前
      logical,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine ucpqnp(ncp)                        !内部変数の総数ncpを求める
      integer,   intent(out) :: ncp
    end subroutine
      
    subroutine ucpqid(cp,idx)                     !内部変数cpの位置idxを求める．
      character(len=8), intent(in) :: cp
      integer,   intent(out) :: idx
    end subroutine
      
    subroutine ucpqcp(idx,cp)                     !idxの位置にある内部変数の名前cpを参照する
      character(len=8), intent(out) :: cp
      integer,   intent(in) :: idx
    end subroutine
      
    subroutine ucpqvl(idx,ipara)                  !idxの位置にある内部変数の値iparaを参照する
      integer,   intent(in) :: idx
      integer,   intent(out) :: ipara
    end subroutine
      
    subroutine ucpsvl(idx,ipara)                  !idxの位置にある内部変数の値iparaを変更する
      integer,   intent(in) :: idx
      integer,   intent(in) :: ipara
    end subroutine
      
  end interface
end module
!ucpack library end ----
