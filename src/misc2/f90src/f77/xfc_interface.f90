!-------------------------------------------------
!interface module of xfclib
!-------------------------------------------------
module xfc_interface
  interface

    function ifromc(cx)                           !整数文字列のあらわす整数値を返す．
      character(len=*), intent(in) :: cx          !調べる文字列
      integer ifromc                              !求める整数値
    end function
      
    function lfromc(cx)                           !論理定数を表現する文字列のあらわす論理値を返す．
      character(len=*), intent(in) :: cx          !調べる文字列
      logical lfromc                              !求める論理値
    end function
      
    function rfromc(cx)                           !指数表現を含む実数文字列のあらわす実数値を返す．
      character(len=*), intent(in) :: cx          !調べる文字列
      real rfromc                                 !求める実数値
    end function

  end interface
end module
!xfclib library end ----
