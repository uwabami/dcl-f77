!-------------------------------------------------
!interface module of bitlib
!-------------------------------------------------
module bit_interface
  interface

    subroutine bitpic(ip,cp)                      !ビットパターンを'0', '1'の文字列として返す．
      integer,   intent(in) :: ip                 !ビットパターン を調べる1語長の引数
      character(len=*), intent(out) :: cp         !ビットパターン を返す文字型の引数
    end subroutine
      
    subroutine bitpci(cp,ip)                      !'0', '1'の文字列をビットパターンとして返す．
      character(len=*), intent(in) :: cp          !ビットパターンを与える文字型の引数
      integer,   intent(out) :: ip                !ビットパターンを返す1語長の引数
    end subroutine
      
    subroutine gbyte(np,io,ib,nb)                 !連続した記憶領域からビットパターンを切り出す．
      integer,   intent(in), dimension(*) :: np   !連続した記憶領域（たとえば配列）の先頭となる語
      integer,   intent(out) :: io                !切り出したビットパターンを返す変数
      integer,   intent(in) :: ib                 !先頭の語においてスキップするビット数
      integer,   intent(in) :: nb                 !切り出すビット数
    end subroutine
      
    subroutine gbytes(np,no,ib,nb,ns,it)          !連続した記憶領域からビットパターンを切り出す．
      integer,   intent(in), dimension(*) :: np   !連続した記憶領域（たとえば配列）の先頭となる語
      integer,   intent(out), dimension(*) :: no  !切り出したビットパターンを返す配列
      integer,   intent(in) :: ib                 !先頭の語においてスキップするビット数
      integer,   intent(in) :: nb                 !切り出すビット数
      integer,   intent(in) :: ns                 !切り出すビットパターン間のビット数
      integer,   intent(in) :: it                 !配列noの長さ
    end subroutine
      
    subroutine sbyte(np,io,ib,nb)                 !連続した記憶領域にビットパターンを埋め込む．
      integer,   intent(out), dimension(*) :: np  !連続した記憶領域（たとえば配列）の先頭となる語
      integer,   intent(in) :: io                 !埋め込むビットパターンを与える変数
      integer,   intent(in) :: ib                 !先頭の語においてスキップするビット数
      integer,   intent(in) :: nb                 !埋め込むビット数
    end subroutine
      
    subroutine sbytes(np,no,ib,nb,ns,it)          !連続した記憶領域にビットパターンを埋め込む．
      integer,   intent(out), dimension(*) :: np  !連続した記憶領域（たとえば配列）の先頭となる語
      integer,   intent(in), dimension(*) :: no   !埋め込むビットパターンを与える配列
      integer,   intent(in) :: ib                 !先頭の語においてスキップするビット数
      integer,   intent(in) :: nb                 !埋め込むビット数
      integer,   intent(in) :: ns                 !埋め込むビットパターン間のビット数
      integer,   intent(in) :: it                 !配列noの長さ
    end subroutine
      
    function ishift(iw,n)                         !ビットパターンをシフトする．
      integer,   intent(in) :: iw                 !ットパターンをシフトする1語長の引数
      integer,   intent(in) :: n                  !シフトするビット数
      integer ishift                              !シフトした結果を返す関数値
    end function
      
    function iand(i1,i2)                          !ビットごとのブール積を求める．
      integer,   intent(in) :: i1                 !ビット列を与える1語長の引数
      integer,   intent(in) :: i2
      integer iand                                !ビット列のブール積を返す関数値
    end function
      
    function ior(i1,i2)                           !ビットごとのブール積を求める．
      integer,   intent(in) :: i1                 !ビット列を与える1語長の引数
      integer,   intent(in) :: i2
      integer ior                                 !ビット列のブール和を返す関数値
    end function

  end interface
end module
!bitlib library end ----
