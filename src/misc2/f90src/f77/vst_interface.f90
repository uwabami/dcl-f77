!-------------------------------------------------
!interface module of vstlib
!-------------------------------------------------
module vst_interface
  interface
!------------------------------------------------------------
!1種類のベクトルデータを処理するサブルーチン．
    subroutine vs1int(wz,nw,ix)                   !1初期化をおこなう
      real,      intent(out), dimension(ix,2) :: wz !大きさが(ix,2)の2次元配列
      integer,   intent(out), dimension(ix) :: nw  !長さが(ix) の1 次元配列．何個のベクトルデータを処理したかを保持している
      integer,   intent(in) :: ix                 !wzの第1次元の寸法， およびnw, xの長さ
    end subroutine
      
    subroutine vs1din(wz,nw,ix,x)                 !データを読み込む．
      real,      intent(inout), dimension(ix,2) :: wz !大きさが(ix,2)の2次元配列
      integer,   intent(inout), dimension(ix) :: nw !長さが(ix) の1 次元配列．何個のベクトルデータを処理したかを保持している
      integer,   intent(in) :: ix                 !wzの第1次元の寸法， およびnw, xの長さ
      real,      intent(in), dimension(ix) :: x   !読み込むベクトルデータ．長さixの配列
    end subroutine
      
    subroutine vs1out(wz,nw,ix)                   !結果を求める．
      real,      intent(inout), dimension(ix,2) :: wz !大きさが(ix,2)の2次元配列
      integer,   intent(in), dimension(ix) :: nw  !長さが(ix) の1 次元配列．何個のベクトルデータを処理したかを保持している
      integer,   intent(in) :: ix                 !wzの第1次元の寸法， およびnw, xの長さ
    end subroutine
!------------------------------------------------------------
!2種類のベクトルデータを連続的に読み込んで平均と分散を求める
    subroutine vs2int(wz,nw,ix,iy)                !初期化をおこなう．
      real,      intent(out), dimension(ix,iy,5) :: wz !大きさが(ix,iy,5) の3次元配列
      integer,   intent(out), dimension(ix,iy) :: nw !大きさが(ix,iy,5)の3次元配列, 何個のベクトルデータを処理したかを保持している
      integer,   intent(in) :: ix                 !wzの第1 次元の寸法， nwの第1次元の寸法，およびxの長さ
      integer,   intent(in) :: iy                 !wzの第2 次元の寸法， nwの第2次元の寸法，およびyの長さ
    end subroutine
      
    subroutine vs2din(wz,nw,ix,iy,x,y)            !データを読み込む．
      real,      intent(inout), dimension(ix,iy,5) :: wz !大きさが(ix,iy,5) の3次元配列
      integer,   intent(inout), dimension(ix,iy) :: nw !大きさが(ix,iy,5)の3次元配列, 何個のベクトルデータを処理したかを保持している
      integer,   intent(in) :: ix                 !wzの第1 次元の寸法， nwの第1次元の寸法，およびxの長さ
      integer,   intent(in) :: iy                 !wzの第2 次元の寸法， nwの第2次元の寸法，およびyの長さ
      real,      intent(in), dimension(ix) :: x   !読み込むベクトルデータ．長さixの配列
      real,      intent(in), dimension(ix) :: y   !読み込むベクトルデータ．長さiyの配列
    end subroutine
      
    subroutine vs2out(wz,nw,ix,iy)                !結果を求める．
      real,      intent(inout), dimension(ix,iy,5) :: wz !大きさが(ix,iy,5) の3次元配列
      integer,   intent(in), dimension(ix,iy) :: nw !大きさが(ix,iy,5)の3次元配列, 何個のベクトルデータを処理したかを保持している
      integer,   intent(in) :: ix                 !読み込むベクトルデータ．長さixの配列
      integer,   intent(in) :: iy                 !読み込むベクトルデータ．長さiyの配列
    end subroutine

  end interface
end module
!vstlib library end ----
