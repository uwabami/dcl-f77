!-------------------------------------------------
!interface module of rnmlib
!-------------------------------------------------
module rnm_interface
  interface

    subroutine vrrnm(rx,ry,n,jx,jy,nb)            !移動平均を計算する． ．
      real,      intent(in), dimension(*) :: rx !移動平均を計算する実数型の配列
      real,      intent(out), dimension(*) :: ry !計算結果を納める実数型の配列
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !配列rx, ryにおいて，処理する配列要素の間隔
      integer,   intent(in) :: jy
      integer,   intent(in) :: nb                 !移動平均をとるデータ長
    end subroutine
      
    subroutine vrrnm0(rx,ry,n,jx,jy,nb)           !移動平均を計算する（欠損値処理をしない）.
      real,      intent(in), dimension(*) :: rx !移動平均を計算する実数型の配列
      real,      intent(out), dimension(*) :: ry !計算結果を納める実数型の配列
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !配列rx, ryにおいて，処理する配列要素の間隔
      integer,   intent(in) :: jy
      integer,   intent(in) :: nb                 !移動平均をとるデータ長
    end subroutine
      
    subroutine vrrnm1(rx,ry,n,jx,jy,nb)           !移動平均を計算する （欠損値処理をする）.
      real,      intent(in), dimension(*) :: rx !移動平均を計算する実数型の配列
      real,      intent(out), dimension(*) :: ry !計算結果を納める実数型の配列
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !配列rx, ryにおいて，処理する配列要素の間隔
      integer,   intent(in) :: jy
      integer,   intent(in) :: nb                 !移動平均をとるデータ長
    end subroutine

  end interface
end module
!rnmlib library end ----
