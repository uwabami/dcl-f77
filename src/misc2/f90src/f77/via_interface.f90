!-------------------------------------------------
!interface module of vialib
!-------------------------------------------------
module via_interface
  interface

    subroutine vifna(ix,iy,n,jx,jy,ifna)          !IXにIFNAを作用させてIYに代入する
      integer,   intent(in), dimension(*) :: ix   !処理する整数型配列
      integer,   intent(out), dimension(*) :: iy
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !配列ix, iyにおいて，処理する配列要素の間隔
      integer,   intent(in) :: jy
      external ifna                               !引数が1個である整数型関数名
    end subroutine
      
    subroutine viinc(ix,iy,n,jx,jy,ii)            !IXにIIを加えてIYに代入する． 
      integer,   intent(in), dimension(*) :: ix   !処理する整数型配列
      integer,   intent(out), dimension(*) :: iy
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !配列ix, iyにおいて，処理する配列要素の間隔
      integer,   intent(in) :: jy
      integer,   intent(in) :: ii                 !加える整数値
    end subroutine
      
    subroutine vifct(ix,iy,n,jx,jy,ii)            !IXにIIを掛けてIYに代入する．
      integer,   intent(in), dimension(*) :: ix   !処理する整数型配列
      integer,   intent(out), dimension(*) :: iy
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !配列ix, iyにおいて，処理する配列要素の間隔
      integer,   intent(in) :: jy
      integer,   intent(in) :: ii                 !掛ける整数値
    end subroutine
      
    subroutine vicon(ix,iy,n,jx,jy,ii)            !IIをIYに代入する． 
      integer,   intent(in), dimension(*) :: ix   !処理する整数型配列
      integer,   intent(out), dimension(*) :: iy
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !配列ix, iyにおいて，処理する配列要素の間隔
      integer,   intent(in) :: jy
      integer,   intent(in) :: ii                 !代入する整数値
    end subroutine
      
    subroutine viset(ix,iy,n,jx,jy)               !IXをIYに代入する．
      integer,   intent(in), dimension(*) :: ix   !処理する整数型配列
      integer,   intent(out), dimension(*) :: iy
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !配列ix, iyにおいて，処理する配列要素の間隔
      integer,   intent(in) :: jy
    end subroutine
      
    subroutine iadd(ix,n,jx,ii)                   !IXにIIを加える．  
      integer,   intent(inout), dimension(*) :: ix !処理する整数型配列
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !配列ixにおいて，処理する配列要素の間隔
      integer,   intent(in) :: ii                 !加える整数値
    end subroutine
      
    subroutine imlt(ix,n,jx,ii)                   !IXにIIを掛ける．  
      integer,   intent(inout), dimension(*) :: ix !処理する整数型配列
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !配列ixにおいて，処理する配列要素の間隔
      integer,   intent(in) :: ii                 !掛ける整数値
    end subroutine
      
    subroutine iset(ix,n,jx,ii)                   !IXにIIを代入する． 
      integer,   intent(inout), dimension(*) :: ix !処理する整数型配列
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !配列ixにおいて，処理する配列要素の間隔
      integer,   intent(in) :: ii                 !代入する整数値
    end subroutine

  end interface
end module
!vialib library end ----
