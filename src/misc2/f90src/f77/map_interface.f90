!-------------------------------------------------
!interface module of maplib
!-------------------------------------------------
module map_interface
  interface

    subroutine mpfcyl(xlon,ylat,x,y)              !正距円筒図法の投影を行なう．
      real,      intent(in) :: xlon               !経度，緯度
      real,      intent(in) :: ylat
      real,      intent(out) :: x                 !x, y座標
      real,      intent(out) :: y
    end subroutine
      
    subroutine mpicyl(x,y,xlon,ylat)              !正距円筒図法の逆投影を行なう．
      real,      intent(in) :: x                 !x, y座標
      real,      intent(in) :: y
      real,      intent(out) :: xlon               !経度，緯度
      real,      intent(out) :: ylat
    end subroutine
      
    subroutine mpfmer(xlon,ylat,x,y)              !メルカトール図法の投影を行なう．
      real,      intent(in) :: xlon               !経度，緯度
      real,      intent(in) :: ylat
      real,      intent(out) :: x                 !x, y座標
      real,      intent(out) :: y
    end subroutine
      
    subroutine mpimer(x,y,xlon,ylat)              !メルカトール図法の逆投影を行なう．
      real,      intent(in) :: x                  !x, y座標
      real,      intent(in) :: y
      real,      intent(out) :: xlon              !経度，緯度
      real,      intent(out) :: ylat
    end subroutine
      
    subroutine mpfmwd(xlon,ylat,x,y)              !モルワイデ図法の投影を行なう．
      real,      intent(in) :: xlon               !経度，緯度
      real,      intent(in) :: ylat
      real,      intent(out) :: x                 !x, y座標
      real,      intent(out) :: y
    end subroutine
      
    subroutine mpimwd(x,y,xlon,ylat)              !モルワイデ図法の逆投影を行なう．
      real,      intent(out) :: xlon              !経度，緯度
      real,      intent(out) :: ylat
      real,      intent(in) :: x                  !x, y座標
      real,      intent(in) :: y
    end subroutine
      
    subroutine mpfmwl(xlon,ylat,x,y)              !モルワイデ図法（もどき）の投影を行なう．
      real,      intent(in) :: xlon               !経度，緯度
      real,      intent(in) :: ylat
      real,      intent(out) :: x                 !x, y座標
      real,      intent(out) :: y
    end subroutine
      
    subroutine mpimwl(x,y,xlon,ylat)              !モルワイデ図法（もどき）の逆投影を行なう．
      real,      intent(out) :: xlon              !経度，緯度
      real,      intent(out) :: ylat
      real,      intent(in) :: x                  !x, y座標
      real,      intent(in) :: y
    end subroutine
      
    subroutine mpfhmr(xlon,ylat,x,y)              !ハンメル図法の投影を行なう．
      real,      intent(in) :: xlon               !経度，緯度
      real,      intent(in) :: ylat
      real,      intent(out) :: x                 !x, y座標
      real,      intent(out) :: y
    end subroutine
      
    subroutine mpihmr(x,y,xlon,ylat)              !ハンメル図法の逆投影を行なう．
      real,      intent(out) :: xlon              !経度，緯度
      real,      intent(out) :: ylat
      real,      intent(in) :: x
      real,      intent(in) :: y
    end subroutine
      
    subroutine mpfek6(xlon,ylat,x,y)              !エッケルト第6図法の投影を行なう．
      real,      intent(in) :: xlon               !経度，緯度
      real,      intent(in) :: ylat
      real,      intent(out) :: x                 !x, y座標
      real,      intent(out) :: y
    end subroutine
      
    subroutine mpiek6(x,y,xlon,ylat)              !エッケルト第6図法の逆投影を行なう．
      real,      intent(out) :: xlon              !経度，緯度
      real,      intent(out) :: ylat
      real,      intent(in) :: x                  !x, y座標
      real,      intent(in) :: y
    end subroutine
      
    subroutine mpfktd(xlon,ylat,x,y)              !北田楕円図法の投影を行なう．
      real,      intent(in) :: xlon               !経度，緯度
      real,      intent(in) :: ylat
      real,      intent(out) :: x                 !x, y座標
      real,      intent(out) :: y
    end subroutine
      
    subroutine mpiktd(x,y,xlon,ylat)              !北田楕円図法の逆投影を行なう．
      real,      intent(out) :: xlon              !経度，緯度
      real,      intent(out) :: ylat
      real,      intent(in) :: x                  !x, y座標
      real,      intent(in) :: y
    end subroutine
      
    subroutine mpfcon(xlon,ylat,x,y)              !正距円錐図法の投影を行なう．
      real,      intent(in) :: xlon               !経度，緯度
      real,      intent(in) :: ylat
      real,      intent(out) :: x                 !x, y座標
      real,      intent(out) :: y
    end subroutine
      
    subroutine mpicon(x,y,xlon,ylat)              !正距円錐図法の逆投影を行なう．
      real,      intent(out) :: xlon              !経度，緯度
      real,      intent(out) :: ylat
      real,      intent(in) :: x                  !x, y座標
      real,      intent(in) :: y
    end subroutine
      
    subroutine mpscon(ylat1)                      !標準緯線の指定
      real,      intent(in) :: ylat1              !標準緯線の緯度
    end subroutine
      
    subroutine mpfcoa(xlon,ylat,x,y)              !ランベルト正積円錐図法の投影を行なう．
      real,      intent(in) :: xlon               !経度，緯度
      real,      intent(in) :: ylat
      real,      intent(out) :: x                 !x, y座標
      real,      intent(out) :: y
    end subroutine
      
    subroutine mpicoa(x,y,xlon,ylat)              !ランベルト正積円錐図法の逆投影を行なう．
      real,      intent(out) :: xlon              !経度，緯度
      real,      intent(out) :: ylat
      real,      intent(in) :: x                  !x, y座標
      real,      intent(in) :: y
    end subroutine
      
    subroutine mpscoa(ylat1)                      !標準緯線の指定
      real,      intent(in) :: ylat1
    end subroutine
      
    subroutine mpfcoc(xlon,ylat,x,y)              !ランベルト正角円錐図法の投影を行なう．
      real,      intent(in) :: xlon               !経度，緯度
      real,      intent(in) :: ylat
      real,      intent(out) :: x                 !x, y座標
      real,      intent(out) :: y
    end subroutine
      
    subroutine mpicoc(x,y,xlon,ylat)              !ランベルト正角円錐図法の逆投影を行なう．
      real,      intent(out) :: xlon              !経度，緯度
      real,      intent(out) :: ylat
      real,      intent(in) :: x                  !x, y座標
      real,      intent(in) :: y
    end subroutine
      
    subroutine mpscoc(ylat1,ylat2)                !標準緯線の指定
      real,      intent(in) :: ylat1              !標準緯線の緯度
      real,      intent(in) :: ylat2
    end subroutine
      
    subroutine mpfbon(xlon,ylat,x,y)              !ボンヌ図法の投影を行なう．
      real,      intent(in) :: xlon               !経度，緯度
      real,      intent(in) :: ylat
      real,      intent(out) :: x                 !x, y座標
      real,      intent(out) :: y
    end subroutine
      
    subroutine mpibon(x,y,xlon,ylat)              !ボンヌ図法の逆投影を行なう．
      real,      intent(out) :: xlon              !経度，緯度
      real,      intent(out) :: ylat
      real,      intent(in) :: x                  !x, y座標
      real,      intent(in) :: y
    end subroutine
      
    subroutine mpsbon(ylat1)                      !標準緯線の指定
      real,      intent(in) :: ylat1              !標準緯線の緯度
    end subroutine
      
    subroutine mpfotg(xlon,ylat,x,y)              !正射図法および satellite view の投影を行なう．
      real,      intent(in) :: xlon               !経度，緯度
      real,      intent(in) :: ylat
      real,      intent(out) :: x                 !x, y座標
      real,      intent(out) :: y
    end subroutine
      
    subroutine mpiotg(x,y,xlon,ylat)              !正射図法および satellite view の逆投影を行なう．
      real,      intent(in) :: x                  !x, y座標
      real,      intent(in) :: y
      real,      intent(out) :: xlon              !経度，緯度
      real,      intent(out) :: ylat
    end subroutine
      
    subroutine mpsotg(rsat)                       !軌道半径の設定
      real,      intent(in) :: rsat               !地球の半径を単位とする衛星の軌道半径
    end subroutine
      
    subroutine mpfpst(xlon,ylat,x,y)              !ポーラーステレオ図法の投影を行なう．
      real,      intent(in) :: xlon               !経度，緯度
      real,      intent(in) :: ylat
      real,      intent(out) :: x                 !x, y座標
      real,      intent(out) :: y
    end subroutine
      
    subroutine mpipst(x,y,xlon,ylat)              !ポーラーステレオ図法の逆投影を行なう．
      real,      intent(in) :: x                  !x, y座標
      real,      intent(in) :: y
      real,      intent(out) :: xlon              !経度，緯度
      real,      intent(out) :: ylat
    end subroutine
      
    subroutine mpfazm(xlon,ylat,x,y)              !正距方位図法の投影を行なう．
      real,      intent(in) :: xlon               !経度，緯度
      real,      intent(in) :: ylat
      real,      intent(out) :: x                  !x, y座標
      real,      intent(out) :: y
    end subroutine
      
    subroutine mpiazm(x,y,xlon,ylat)              !正距方位図法の逆投影を行なう．
      real,      intent(in) :: x                 !x, y座標
      real,      intent(in) :: y
      real,      intent(out) :: xlon               !経度，緯度
      real,      intent(out) :: ylat
    end subroutine
      
    subroutine mpfaza(xlon,ylat,x,y)              !ランベルト正積方位図法の投影を行なう．
      real,      intent(in) :: xlon               !経度，緯度
      real,      intent(in) :: ylat
      real,      intent(out) :: x                 !x, y座標
      real,      intent(out) :: y
    end subroutine
      
    subroutine mpiaza(x,y,xlon,ylat)              !ランベルト正積方位図法の逆投影を行なう．
      real,      intent(in) :: x                  !x, y座標
      real,      intent(in) :: y
      real,      intent(out) :: xlon              !経度，緯度
      real,      intent(out) :: ylat
    end subroutine

  end interface
end module
!maplib library end ----
