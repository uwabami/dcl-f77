!-------------------------------------------------
!interface module of uepack
!-------------------------------------------------
module ue_interface
  interface
    subroutine uetone(z,mx,nx,ny)                 !2次元等値線図のぬりわけをおこなう．
      real,      intent(in), dimension(mx,*) :: z !mx, nyの2次元配列
      integer,   intent(in) :: mx                 !配列zの第1次元整合寸法
      integer,   intent(in) :: nx                 !作画に使う配列zの第1次元寸法
      integer,   intent(in) :: ny                 !作画に使う配列zの第2次元寸法
    end subroutine
      
    subroutine uetonf(z,mx,nx,ny)                 !2次元等値線図のぬりわけをおこなう．
      real,      intent(in), dimension(mx,*) :: z !mx, nyの2次元配列．作画にはnx nyの部分を使う
      integer,   intent(in) :: mx                 !配列zの第1次元整合寸法
      integer,   intent(in) :: nx                 !作画に使う配列zの第1次元寸法
      integer,   intent(in) :: ny                 !作画に使う配列zの第2次元寸法
    end subroutine
      
    subroutine uegtla(xmin,xmax,dx)               !トーンレベル値を最小値・最大値,きざみ値,レベル数で設定する．
      real,      intent(in) :: xmin               !トーンレベルの最小値
      real,      intent(in) :: xmax               !トーンレベルの最大値
      real,      intent(in) :: dx                 !dx > 0 のときdxをきざみ幅
    end subroutine
      
    subroutine uegtlb(z,mx,nx,ny,dx)              !コンターレベル値を格子点値を与える配列,きざみ値,レベル数で設定する．
      real,      intent(in), dimension(mx,*) :: z
      integer,   intent(in) :: mx                 !配列zの第1次元整合寸法
      integer,   intent(in) :: nx                 !作画に使う配列zの第1次元寸法
      integer,   intent(in) :: ny                 !作画に使う配列zの第2次元寸法
      real,      intent(in) :: dx                 !dx > 0 のときdxをきざみ幅
    end subroutine
      
    subroutine uestlv(tlev1,tlev2,ipat)           !ぬりわけるレベルとパターンを1レベルごと指定する．
      real,      intent(in) :: tlev1              !ぬりわけるレベルの下限値
      real,      intent(in) :: tlev2              !ぬりわけるレベルの上限値
      integer,   intent(in) :: ipat               !トーンパターン番号
    end subroutine
      
    subroutine ueqtlv(tlev1,tlev2,ipat,iton)      !第iton番目のトーンレベルの属性
      real,      intent(out) :: tlev1
      real,      intent(out) :: tlev2
      integer,   intent(out) :: ipat
      integer,   intent(in) :: iton
    end subroutine
      
    subroutine ueqntl(nton)                       !現在設定されているトーンレベルの数
      integer,   intent(out) :: nton
    end subroutine
      
    subroutine uestln(tlevn,ipatn,nton)           !ぬりわけるレベルとパターンを配列で複数レベル指定する．
      real,      intent(in), dimension(nton+1) :: tlevn !ぬりわけるレベルの値を格納する配列．
      integer,   intent(in), dimension(nton) :: ipatn !トーンパターン番号を格納する配列．
      integer,   intent(in) :: nton               !配列ipatnの大きさ
    end subroutine
      
    subroutine ueitlv()                           !トーンレベルを無効にする．
    end subroutine
      
    subroutine uepget(cp,ipara)                   !内部変数を参照する
      character(len=*), intent(in) :: cp          !内部変数の名前
      integer,   intent(out) :: ipara             !内部変数の値
    end subroutine
      
    subroutine ueiget(cp,ipara)                   !内部変数を参照する
      character(len=*), intent(in) :: cp          !内部変数の名前
      integer,   intent(out) :: ipara             !内部変数の値
    end subroutine
      
    subroutine uerget(cp,ipara)                   !内部変数を参照する
      character(len=*), intent(in) :: cp          !内部変数の名前
      real,      intent(out) :: ipara             !内部変数の値
    end subroutine
      
    subroutine uelget(cp,ipara)                   !内部変数を参照する
      character(len=*), intent(in) :: cp          !内部変数の名前
      logical,   intent(out) :: ipara             !内部変数の値
    end subroutine
      
    subroutine uepset(cp,ipara)                   !内部変数を変更する
      character(len=*), intent(in) :: cp          !内部変数の名前
      integer,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine ueiset(cp,ipara)                   !内部変数を変更する
      character(len=*), intent(in) :: cp          !内部変数の名前
      integer,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine uerset(cp,ipara)                   !内部変数を変更する
      character(len=*), intent(in) :: cp          !内部変数の名前
      real,      intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine uelset(cp,ipara)                   !内部変数を変更する
      character(len=*), intent(in) :: cp          !内部変数の名前
      logical,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine uepstx(cp,ipara)                   !内部変数を変更する
      character(len=*), intent(in) :: cp          !内部変数の名前
      integer,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine ueistx(cp,ipara)                   !内部変数を変更する
      character(len=*), intent(in) :: cp          !内部変数の名前
      integer,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine uerstx(cp,ipara)                   !内部変数を変更する
      character(len=*), intent(in) :: cp          !内部変数の名前
      real,      intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine uelstx(cp,ipara)                   !内部変数を変更する
      character(len=*), intent(in) :: cp          !内部変数の名前
      logical,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine uepqnp(ncp)                        !内部変数の総数を求める
      integer,   intent(out) :: ncp
    end subroutine
      
    subroutine uepqid(cp,idx)                     !内部変数cpの位置を求める．
      character(len=*), intent(in) :: cp
      integer,   intent(out) :: idx
    end subroutine
      
    subroutine uepqcp(idx,cp)                     !idxの位置にある内部変数の名前を参照する
      character(len=*), intent(out) :: cp
      integer,   intent(in) :: idx
    end subroutine
      
    subroutine uepqvl(idx,ipara)                  !idxの位置にある内部変数の値を参照する
      integer,   intent(in) :: idx
      integer,   intent(out) :: ipara
    end subroutine
      
    subroutine uepsvl(idx,ipara)                  !idxの位置にある内部変数の値を変更する
      integer,   intent(in) :: idx
      integer,   intent(in) :: ipara
    end subroutine
      
  end interface
end module
!uepack library end ----
