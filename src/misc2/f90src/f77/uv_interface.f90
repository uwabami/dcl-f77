!-------------------------------------------------
!interface module of uvpack
!-------------------------------------------------
module uv_interface
  interface
!--------------------------------------------------------
    subroutine uverb (n,x,y1,y2)                  !エラーバーを描く．
      integer,   intent(in) :: n                  !配列の長さ
      real,      intent(in), dimension(*) :: x    !独立変数
      real,      intent(in), dimension(*) :: y1   !エラーバーの両端の座標値
      real,      intent(in), dimension(*) :: y2
    end subroutine
      
    subroutine uverbz(n,x,y1,y2,itype,index,rsize) !エラーバーを描く．
      integer,   intent(in) :: n                  !配列の長さ
      real,      intent(in), dimension(*) :: x
      real,      intent(in), dimension(*) :: y1   !エラーバーの両端の座標値
      real,      intent(in), dimension(*) :: y2
      integer,   intent(in) :: itype              !エラーバーのラインタイプ
      integer,   intent(in) :: index              !エラーバーのラインインデクス
      real,      intent(in) :: rsize              !エラーバーの幅
    end subroutine
!--------------------------------------------------------
    subroutine uvdif (n,x,y1,y2)                  !2本の折れ線の間をトーンパターンで塗る．
      integer,   intent(in) :: n                  !配列の長さ
      real,      intent(in), dimension(*) :: x    !独立変数
      real,      intent(in), dimension(*) :: y1   !従属変数
      real,      intent(in), dimension(*) :: y2
    end subroutine
      
    subroutine uvdifz(n,x,y1,y2,itp1,itp2)        !2本の折れ線の間をトーンパターンで塗る．
      integer,   intent(in) :: n                  !配列の長さ
      real,      intent(in), dimension(*) :: x    !独立変数
      real,      intent(in), dimension(*) :: y1   !従属変数
      real,      intent(in), dimension(*) :: y2
      integer,   intent(in) :: itp1               !トーンパターン番号．
      integer,   intent(in) :: itp2
    end subroutine
!--------------------------------------------------------
    subroutine uvbrf (n,x,y1,y2)                  !棒グラフの枠を描く．
      integer,   intent(in) :: n                  !配列の長さ
      real,      intent(in), dimension(*) :: x    !独立変数
      real,      intent(in), dimension(*) :: y1   !棒の両端の座標値
      real,      intent(in), dimension(*) :: y2
    end subroutine
      
    subroutine uvbrfz(n,x,y1,y2,itype,index,rsize) !棒グラフの枠を描く．
      integer,   intent(in) :: n                  !配列の長さ
      real,      intent(in), dimension(*) :: x    !独立変数
      real,      intent(in), dimension(*) :: y1   !棒の両端の座標値
      real,      intent(in), dimension(*) :: y2
      integer,   intent(in) :: itype              !枠のラインタイプ
      integer,   intent(in) :: index              !枠のラインインデクス
      real,      intent(in) :: rsize              !棒の幅
    end subroutine
      
    subroutine uvbra (n,x,y1,y2)                  !棒グラフの内部領域を塗る．
      integer,   intent(in) :: n                  !配列の長さ
      real,      intent(in), dimension(*) :: x    !独立変数
      real,      intent(in), dimension(*) :: y1   !棒の両端の座標値
      real,      intent(in), dimension(*) :: y2
    end subroutine
      
    subroutine uvbraz(n,x,y1,y2,itp1,itp2,rsize)  !棒グラフの内部領域を塗る．
      integer,   intent(in) :: n                  !配列の長さ
      real,      intent(in), dimension(*) :: x    !独立変数
      real,      intent(in), dimension(*) :: y1   !棒の両端の座標値
      real,      intent(in), dimension(*) :: y2
      integer,   intent(in) :: itp1               !トーンパターン番号．
      integer,   intent(in) :: itp2
      real,      intent(in) :: rsize              !棒の幅
    end subroutine
      
    subroutine uvbrl(n,x,yd)                      !棒グラフを線で結ぶ．
      integer,   intent(in) :: n                  !配列の長さ
      real,      intent(in), dimension(*) :: x    !独立変数
      real,      intent(in), dimension(*) :: yd   !棒の端の値
    end subroutine
      
    subroutine uvbrlz(n,x,yd,itype,index,rsize)   !棒グラフを線で結ぶ．
      integer,   intent(in) :: n                  !配列の長さ
      real,      intent(in), dimension(*) :: x    !独立変数
      real,      intent(in), dimension(*) :: yd   !棒の端の値
      integer,   intent(in) :: itype              !ラインタイプ
      integer,   intent(in) :: index              !ラインインデクス
      real,      intent(in) :: rsize              !棒の幅
    end subroutine
!--------------------------------------------------------
    subroutine uvbxf(n,x,y1,y2)                   !箱グラフの枠を描く．
      integer,   intent(in) :: n                  !配列の長さ
      real,      intent(in), dimension(*) :: x  !独立変数
      real,      intent(in), dimension(*) :: y1   !箱の両端の座標値
      real,      intent(in), dimension(*) :: y2
    end subroutine
      
    subroutine uvbxfz(n,x,y1,y2,itype,index)      !箱グラフの枠を描く．
      integer,   intent(in) :: n                  !配列の長さ
      real,      intent(in), dimension(*) :: x  !独立変数
      real,      intent(in), dimension(*) :: y1   !箱の両端の座標値
      real,      intent(in), dimension(*) :: y2
      integer,   intent(in) :: itype              !枠のラインタイプ
      integer,   intent(in) :: index              !枠のラインインデクス
    end subroutine
      
    subroutine uvbxa(n,x,y1,y2)                   !箱グラフの内部領域を塗る．
      integer,   intent(in) :: n                  !配列の長さ
      real,      intent(in), dimension(*) :: x  !独立変数
      real,      intent(in), dimension(*) :: y1   !箱の両端の座標値
      real,      intent(in), dimension(*) :: y2
    end subroutine
      
    subroutine uvbxaz(n,x,y1,y2,itp1,itp2)        !箱グラフの内部領域を塗る．
      integer,   intent(in) :: n                  !配列の長さ
      real,      intent(in), dimension(*) :: x  !独立変数
      real,      intent(in), dimension(*) :: y1   !箱の両端の座標値
      real,      intent(in), dimension(*) :: y2
      integer,   intent(in) :: itp1               !トーンパターン番号．
      integer,   intent(in) :: itp2
    end subroutine
      
    subroutine uvbxl(n,x,yd)                      !階段状のグラフを描く．
      integer,   intent(in) :: n                  !配列の長さ
      real,      intent(in), dimension(*) :: x  !独立変数
      real,      intent(in), dimension(*) :: yd   !従属変数
    end subroutine
      
    subroutine uvbxlz(n,x,yd,itype,index)         !階段状のグラフを描く．
      integer,   intent(in) :: n                  !配列の長さ
      real,      intent(in), dimension(*) :: x  !独立変数
      real,      intent(in), dimension(*) :: yd   !従属変数
      integer,   intent(in) :: itype              !ラインタイプ
      integer,   intent(in) :: index              !ラインインデクス
    end subroutine

  end interface
end module
!uvpack library end ----
