!-------------------------------------------------
!interface module of gnmlib
!-------------------------------------------------
module gnm_interface
  interface
!-----------------------------------------------------------------
    subroutine gnlt(rx,bx,ip)                     !与えた実数値より小さいきりのよい数のうちで最大/最小のものを求める． 結果はbx 10^ipの形で返される．
      real,      intent(in) :: rx                 !調べる実数値
      real,      intent(out) :: bx                !きりのよい数の仮数部
      integer,   intent(out) :: ip                !きりのよい数の指数部
    end subroutine
      
    subroutine gngt(rx,bx,ip)                     !与えた実数値より大きいきりのよい数のうちで最大/最小のものを求める． 結果はbx 10^ipの形で返される．
      real,      intent(in) :: rx                 !調べる実数値
      real,      intent(out) :: bx                !きりのよい数の仮数部
      integer,   intent(out) :: ip                !きりのよい数の指数部
    end subroutine
      
    subroutine gnle(rx,bx,ip)                     !与えた実数値以下のきりのよい数のうちで最大/最小のものを求める． 結果はbx 10^ipの形で返される．
      real,      intent(in) :: rx                 !調べる実数値
      real,      intent(out) :: bx                !きりのよい数の仮数部
      integer,   intent(out) :: ip                !きりのよい数の指数部
    end subroutine
      
    subroutine gnge(rx,bx,ip)                     !与えた実数値以上のきりのよい数のうちで最大/最小のものを求める． 結果はbx 10^ipの形で返される．
      real,      intent(in) :: rx                 !調べる実数値
      real,      intent(out) :: bx                !きりのよい数の仮数部
      integer,   intent(out) :: ip                !きりのよい数の指数部
    end subroutine
      
    subroutine gnsblk(xb,nb)                      !きりのよい数のリストを設定/参照する．
      real,      intent(in),dimension(*) :: xb    !きりのよい数を昇順に納めた実数型配列．
      integer,   intent(in) :: nb                 !配列の寸法
    end subroutine
      
    subroutine gnqblk(xb,nb)                      !きりのよい数のリストを参照する．
      real,      intent(out),dimension(*) :: xb   !きりのよい数を昇順に納めた実数型配列
      integer,   intent(in) :: nb                 !配列の寸法
    end subroutine
      
    subroutine gnsave()                           !現在設定されているきりのよい数のリストを保存する
    end subroutine
      
    subroutine gnrset()                           !現在設定されているきりのよい数のリストを 保存してあったリストで再設定する．
    end subroutine
!-----------------------------------------------------------------      
    function rgnlt(rx)                            !きりのよい数を求める．
      real,      intent(in) :: rx                 !調べる実数値
      real rgnlt                                  !rxより小さいきりのよい数のうちで 最大のものを与える関数値．
    end function
      
    function rgnle(rx)                            !きりのよい数を求める．
      real,      intent(in) :: rx                 !調べる実数値
      real rgnle                                  !rx以下のきりのよい数のうちで 最大のものを与える関数値．
    end function
      
    function rgngt(rx)                            !きりのよい数を求める．
      real,      intent(in) :: rx                 !調べる実数値
      real rgngt                                  !rxより大きいきりのよい数のうちで 最小のものを与える関数値
    end function
      
    function rgnge(rx)                            !きりのよい数を求める．
      real,      intent(in) :: rx                 !調べる実数値
      real rgnge                                  !rx以上のきりのよい数のうちで 最小のものを与える関数値．
    end function

  end interface
end module
!gnmlib library end ----
