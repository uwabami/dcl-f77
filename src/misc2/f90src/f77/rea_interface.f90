!-------------------------------------------------
!interface module of reallib
!-------------------------------------------------
module rea_interface
  interface

    function r4ibm(rr)                            !IBM形式の4バイト実数表現を解釈する
      real,      intent(in) :: rr                 !ibm形式の4バイト実数値
      real r4ibm                                  !いま使用しているシステムにおける実数値を返す実数型関数値
    end function
      
    function r4ieee(rr)                           !IEEE形式の4バイト実数表現を解釈する．  
      real,      intent(in) :: rr                 !ibm形式の4バイト実数値
      real r4ieee                                 !いま使用しているシステムにおける実数値を返す実数型関数値
    end function

  end interface
end module
!reallib library end ----
