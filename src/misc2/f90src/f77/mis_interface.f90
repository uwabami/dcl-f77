!-------------------------------------------------
!interface module of misclib
!-------------------------------------------------
module mis_interface
  interface

    subroutine dclvnm(cv)                         !「電脳ライブラリ」のバージョン名を返す．
      character(len=*), intent(out) :: cv         !バージョン名を返す文字型の引数
    end subroutine
      
    subroutine cdblk(chr)                         !連続した2個以上の空白を1個の空白で置き換える．
      character(len=*), intent(inout) :: chr      !操作をほどこす文字列
    end subroutine
      
    function cns(ins)                             !調べる値が正なら'n', 負なら's'を返す ．
      integer,   intent(in) :: ins                !調べる整数値
      character cns*1                             !insが正なら'n',負なら's'を返す長さ1の文字型関数
    end function

  end interface
end module
!misclib library end ----
