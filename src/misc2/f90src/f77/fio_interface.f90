!-------------------------------------------------
!interface module of fiolib
!-------------------------------------------------
module fio_interface
  interface

    subroutine fcopen(iou,cdsn,nrl,cact,icon)     !ファイルをオープンする．
      integer,   intent(in) :: iou                !入出力装置番号
      character(len=*), intent(in) :: cdsn        !ファイル名
      integer,   intent(in) :: nrl                !レコード長
      character(len=1), intent(in) :: cact        !入出力モードの指定
      integer,   intent(in) :: icon               !コンディションコード
    end subroutine
      
    subroutine fcclos(iou,icon)                   !ファイルをクローズする．
      integer,   intent(in) :: iou                !入出力装置番号
      integer,   intent(in) :: icon               !コンディションコード
    end subroutine
      
    subroutine fcslfc(clx)                        !行末の改行文字を設定する．
      character(len=*), intent(in) :: clx         !指定する改行文字
    end subroutine
      
    subroutine fcleol(iou,leol)                   !行末の改行制御をするかどうかを指定する．
      integer,   intent(in) :: iou                !入出力装置番号
      logical,   intent(in) :: leol               !改行制御の指定をする． .true.なら改行制御をする;.false.ならしない
    end subroutine
      
    subroutine fcnrec(iou,nrec)                   !レコード番号を指定する．
      integer,   intent(in) :: iou                !入出力装置番号
      integer,   intent(in) :: nrec               !レコード番号． 1以上の整数値
    end subroutine
      
    subroutine fcgetr(iou,cbuf,icon)              !1レコードを（文字列で）読み込む．
      integer,   intent(in) :: iou                !入出力装置番号
      character(len=*), intent(out) :: cbuf       !読み込むデータレコード
      integer,   intent(in) :: icon               !コンディションコード
    end subroutine
      
    subroutine fcputr(iou,cbuf,icon)              !1レコードを（文字列で）書き込む．
      integer,   intent(in) :: iou                !入出力装置番号
      character(len=*), intent(in) :: cbuf        !読み込むデータレコード
      integer,   intent(in) :: icon               !コンディションコード
    end subroutine
      
    subroutine fcgets(iou,ibuf,icon)              !1レコードを（配列で）読み込む．
      integer,   intent(in) :: iou                !入出力装置番号
      integer,   intent(out) :: ibuf              !読み込むデータレコード
      integer,   intent(in) :: icon               !コンディションコード
    end subroutine
      
    subroutine fcputs(iou,ibuf,icon)              !1レコードを（配列で）書き込む．
      integer,   intent(in) :: iou                !入出力装置番号
      integer,   intent(in) :: ibuf               !読み込むデータレコード
      integer,   intent(in) :: icon               !コンディションコード
    end subroutine
      
    subroutine fcrwnd(iou,icon)                   !リワインドする．
      integer,   intent(in) :: iou                !入出力装置番号
      integer,   intent(in) :: icon               !コンディションコード
    end subroutine

  end interface
end module
!fiolib library end ----
