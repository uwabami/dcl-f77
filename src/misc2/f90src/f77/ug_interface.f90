!-------------------------------------------------
!interface module of ugpack
!-------------------------------------------------
module ug_interface
  interface

    subroutine ugvect(u,mu,v,mv,nx,ny)            !2次元ベクトル場を描く．
      real,      intent(in), dimension(mu,*) :: u !ベクトルのx成分を与えるmu,nyの2次元配列．
      integer,   intent(in) :: mu                 !配列uの第1次元整合寸法
      real,      intent(in), dimension(mv,*) :: v !ベクトルのy成分を与えるmu,nyの2次元配列．
      integer,   intent(in) :: mv                 !配列vの第1次元整合寸法
      integer,   intent(in) :: nx                 !作画に使う配列u, vの第1次元寸法
      integer,   intent(in) :: ny                 !作画に使う配列u, vの第2次元寸法
    end subroutine
      
    subroutine ugsut(cside,cttl)                  !ユニットベクトルにつけるタイトルを設定する．
      character(len=1), intent(in) :: cside       !タイトル位置を指定する
      character(len=*), intent(in) :: cttl        !タイトル
    end subroutine
      
    subroutine ugpget(cp,ipara)                   !内部変数を参照する
      character(len=*), intent(in) :: cp          !内部変数の名前
      integer,   intent(out) :: ipara             !内部変数の値
    end subroutine
      
    subroutine ugiget(cp,ipara)                   !内部変数を参照する
      character(len=*), intent(in) :: cp          !内部変数の名前
      integer,   intent(out) :: ipara             !内部変数の値
    end subroutine
      
    subroutine ugrget(cp,ipara)                   !内部変数を参照する
      character(len=*), intent(in) :: cp          !内部変数の名前
      real,      intent(out) :: ipara             !内部変数の値
    end subroutine
      
    subroutine uglget(cp,ipara)                   !内部変数を参照する
      character(len=*), intent(in) :: cp          !内部変数の名前
      logical,   intent(out) :: ipara             !内部変数の値
    end subroutine
      
    subroutine ugpset(cp,ipara)                   !内部変数を変更する
      character(len=*), intent(in) :: cp          !内部変数の名前
      integer,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine ugiset(cp,ipara)                   !内部変数を変更する
      character(len=*), intent(in) :: cp          !内部変数の名前
      integer,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine ugrset(cp,ipara)                   !内部変数を変更する
      character(len=*), intent(in) :: cp          !内部変数の名前
      real,      intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine uglset(cp,ipara)                   !内部変数を変更する
      character(len=*), intent(in) :: cp          !内部変数の名前
      logical,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine ugpstx(cp,ipara)                   !内部変数を変更する
      character(len=*), intent(in) :: cp          !内部変数の名前
      integer,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine ugistx(cp,ipara)                   !内部変数を変更する
      character(len=*), intent(in) :: cp          !内部変数の名前
      integer,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine ugrstx(cp,ipara)                   !内部変数を変更する
      character(len=*), intent(in) :: cp          !内部変数の名前
      real,      intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine uglstx(cp,ipara)                   !内部変数を変更する
      character(len=*), intent(in) :: cp          !内部変数の名前
      logical,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine ugpqnp(ncp)                        !内部変数の総数を求める
      integer,   intent(out) :: ncp
    end subroutine
      
    subroutine ugpqid(cp,idx)                     !内部変数cpの位置を求める．
      character(len=*), intent(in) :: cp
      integer,   intent(out) :: idx
    end subroutine
      
    subroutine ugpqcp(idx,cp)                     !idxの位置にある内部変数の名前を参照する
      character(len=*), intent(out) :: cp
      integer,   intent(in) :: idx
    end subroutine
      
    subroutine ugpqvl(idx,ipara)                  !idxの位置にある内部変数の値を参照する
      integer,   intent(in) :: idx
      integer,   intent(out) :: ipara
    end subroutine
      
    subroutine ugpsvl(idx,ipara)                  !idxの位置にある内部変数の値を変更する
      integer,   intent(in) :: idx
      integer,   intent(in) :: ipara
    end subroutine
      
  end interface
end module
!ugpack library end ----
