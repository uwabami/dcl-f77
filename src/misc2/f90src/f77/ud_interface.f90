!-------------------------------------------------
!interface module of udpack
!-------------------------------------------------
module ud_interface
  interface
    subroutine udcntr(z,mx,nx,ny)                 !2次元等高線図を描く
      real,      intent(in), dimension(mx,*) :: z !mx  nyの2次元配列．作画にはnx nyの部分を使う
      integer,   intent(in) :: mx                 !配列zの第1次元整合寸法
      integer,   intent(in) :: nx                 !作画に使う配列zの第1次元寸法
      integer,   intent(in) :: ny                 !作画に使う配列zの第2次元寸法
    end subroutine
      
    subroutine udcntz(z,mx,nx,ny,ibr,nbr3)        !2次元等高線図を描く.
      real,      intent(in), dimension(mx,*) :: z !mx  nyの2次元配列． 作画にはnx nyの部分を使う
      integer,   intent(in) :: mx                 !配列zの第1次元整合寸法
      integer,   intent(in) :: nx                 !作画に使う配列zの第1次元寸法
      integer,   intent(in) :: ny                 !作画に使う配列zの第2次元寸法
      integer,   intent(out), dimension(*) :: ibr !作業領域として用いられる配列
      integer,   intent(in) :: nbr3               !配列ibrの長さ
    end subroutine
      
    subroutine udgcla(xmin,xmax,dx)               !コンターレベル値を最小値・最大値,きざみ値,レベル数で設定する．
      real,      intent(in) :: xmin               !コンターレベルの最小値
      real,      intent(in) :: xmax               !コンターレベルの最大値.
      real,      intent(in) :: dx                 !きざみ幅
    end subroutine
      
    subroutine udgclb(z,mx,nx,ny,dx)              !コンターレベル値を格子点値を与える配列,きざみ値,レベル数で設定する．
      real,      intent(in), dimension(mx,*) :: z !mx  nyの2次元配列．作画にはnx nyの部分を使う
      integer,   intent(in) :: mx                 !配列zの第1次元整合寸法
      integer,   intent(in) :: nx                 !作画に使う配列zの第1次元寸法
      integer,   intent(in) :: ny                 !作画に使う配列zの第2次元寸法
      real,      intent(in) :: dx                 !きざみ幅
    end subroutine
      
    subroutine udsclv(zlev,indx,ityp,clv,hl)      !コンターラインの属性をすべて指定して1本のコンターレベルを設定する．
      real,      intent(in) :: zlev               !コンターレベルの値
      integer,   intent(in) :: indx               !コンターラインのラインインデクス
      integer,   intent(in) :: ityp               !コンターラインのラインタイプ
      character(len=*), intent(in) :: clv         !コンターラインにつけるラベル
      real,      intent(in) :: hl                 !ラベルの高さ(単位はv座標系）
    end subroutine
      
    subroutine udqclv(zlev,indx,ityp,clv,hl,nlev) !第nl番目のコンターレベルの属性
      real,      intent(out) :: zlev
      integer,   intent(out) :: indx
      integer,   intent(out) :: ityp
      character(len=*), intent(out) :: clv
      real,      intent(out) :: hl
      integer,   intent(in) :: nlev
    end subroutine
      
    subroutine udqcln(nlev)                       !現在設定されているコンターレベルの総本数
      integer,   intent(out) :: nlev
    end subroutine
      
    subroutine uddclv(zlev)                       !あるコンターレベルを削除する．
      real,      intent(in) :: zlev               !削除するコンターレベルの値
    end subroutine
      
    subroutine udiclv()                           !コンターレベルを無効にする．
    end subroutine
      
    function rudlev(nlev)                         !コンターレベルの間隔を求める．
      integer,   intent(in) :: nlev               !何番目のコンター間隔を調べるか指定する
      real rudlev                                 !コンター間隔を与える実数型関数値
    end function
      
    subroutine udsfmt(cfmt)                       !コンターラベルのフォーマットを指定する．
      character(len=*), intent(in) :: cfmt        !指定するフォーマット（長さは16文字以下）
    end subroutine
      
    subroutine udqfmt(cfmt)                       !現在設定されているフォーマット
      character(len=*), intent(out) :: cfmt
    end subroutine
      
    subroutine udpget(cp,ipara)                   !内部変数を参照
      character(len=*), intent(in) :: cp          !内部変数の名前
      integer,   intent(out) :: ipara             !内部変数の値
    end subroutine
      
    subroutine udiget(cp,ipara)                   !内部変数を参照
      character(len=*), intent(in) :: cp          !内部変数の名前
      integer,   intent(out) :: ipara             !内部変数の値
    end subroutine
      
    subroutine udrget(cp,ipara)                   !内部変数を参照
      character(len=*), intent(in) :: cp          !内部変数の名前
      real,      intent(out) :: ipara             !内部変数の値
    end subroutine
      
    subroutine udlget(cp,ipara)                   !内部変数を参照
      character(len=*), intent(in) :: cp          !内部変数の名前
      logical,   intent(out) :: ipara             !内部変数の値
    end subroutine
      
    subroutine udpset(cp,ipara)                   !内部変数を変更
      character(len=*), intent(in) :: cp          !内部変数の名前
      integer,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine udiset(cp,ipara)                   !内部変数を変更
      character(len=*), intent(in) :: cp          !内部変数の名前
      integer,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine udrset(cp,ipara)                   !内部変数を変更
      character(len=*), intent(in) :: cp          !内部変数の名前
      real,      intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine udlset(cp,ipara)                   !内部変数を変更
      character(len=*), intent(in) :: cp          !内部変数の名前
      logical,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine udpstx(cp,ipara)                   !内部変数を変更
      character(len=*), intent(in) :: cp          !内部変数の名前
      integer,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine udistx(cp,ipara)                   !内部変数を変更
      character(len=*), intent(in) :: cp          !内部変数の名前
      integer,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine udrstx(cp,ipara)                   !内部変数を変更
      character(len=*), intent(in) :: cp          !内部変数の名前
      real,      intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine udlstx(cp,ipara)                   !内部変数を変更
      character(len=*), intent(in) :: cp          !内部変数の名前
      logical,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine udpqnp(ncp)                        !内部変数の総数を求める
      integer,   intent(out) :: ncp
    end subroutine
      
    subroutine udpqid(cp,idx)                     !内部変数cpの位置を求める．
      character(len=*), intent(in) :: cp
      integer,   intent(out) :: idx
    end subroutine
      
    subroutine udpqcp(idx,cp)                     !idxの位置にある内部変数の名前を参照する
      character(len=*), intent(out) :: cp
      integer,   intent(in) :: idx
    end subroutine
      
    subroutine udpqvl(idx,ipara)                  !idxの位置にある内部変数の値を参照する
      integer,   intent(in) :: idx
      integer,   intent(out) :: ipara
    end subroutine
      
    subroutine udpsvl(idx,ipara)                  !idxの位置にある内部変数の値を変更する
      integer,   intent(in) :: idx
      integer,   intent(in) :: ipara
    end subroutine
      
  end interface
end module
!udpack library end ----
