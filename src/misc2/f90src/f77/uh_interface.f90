!-------------------------------------------------
!interface module of uhpack
!-------------------------------------------------
module uh_interface
  interface
    subroutine uherb (n,x1,x2,y)                  !エラーバーを描く．
      integer,   intent(in) :: n                  !配列の長さ
      real,      intent(in), dimension(*) :: y    !独立変数
      real,      intent(in), dimension(*) :: x1   !エラーバーの両端の座標値
      real,      intent(in), dimension(*) :: x2
    end subroutine
      
    subroutine uherbz(n,x1,x2,y,itype,index,rsize) !エラーバーを描く．
      integer,   intent(in) :: n                  !配列の長さ
      real,      intent(in), dimension(*) :: y    !独立変数
      real,      intent(in), dimension(*) :: x1   !エラーバーの両端の座標値
      real,      intent(in), dimension(*) :: x2
      integer,   intent(in) :: itype              !エラーバーのラインタイプ
      integer,   intent(in) :: index              !エラーバーのラインインデクス
      real,      intent(in) :: rsize              !エラーバーの幅
    end subroutine
!--------------------------------------------------------
    subroutine uhdif (n,x1,x2,y)                  !2本の折れ線の間をトーンパターンで塗る．
      integer,   intent(in) :: n                  !配列の長さ
      real,      intent(in), dimension(*) :: y    !独立変数
      real,      intent(in), dimension(*) :: x1   !従属変数
      real,      intent(in), dimension(*) :: x2
    end subroutine
      
    subroutine uhdifz(n,x1,x2,y,itp1,itp2)        !2本の折れ線の間をトーンパターンで塗る．
      integer,   intent(in) :: n                  !配列の長さ
      real,      intent(in), dimension(*) :: y    !独立変数
      real,      intent(in), dimension(*) :: x1   !従属変数
      real,      intent(in), dimension(*) :: x2
      integer,   intent(in) :: itp1               !トーンパターン番号．
      integer,   intent(in) :: itp2
    end subroutine
 !--------------------------------------------------------
    subroutine uhbrf (n,x1,x2,y)                  !棒グラフの枠を描く．
      integer,   intent(in) :: n                  !配列の長さ
      real,      intent(in), dimension(*) :: y    !独立変数
      real,      intent(in), dimension(*) :: x1   !棒の両端の座標値
      real,      intent(in), dimension(*) :: x2
    end subroutine
      
    subroutine uhbrfz(n,x1,x2,y,itype,index,rsize) !棒グラフの枠を描く．
      integer,   intent(in) :: n                  !配列の長さ
      real,      intent(in), dimension(*) :: y    !独立変数
      real,      intent(in), dimension(*) :: x1   !棒の両端の座標値
      real,      intent(in), dimension(*) :: x2
      integer,   intent(in) :: itype              !枠のラインタイプ
      integer,   intent(in) :: index              !枠のラインインデクス
      real,      intent(in) :: rsize              !棒の幅
    end subroutine
      
    subroutine uhbra (n,x1,x2,y)                  !棒グラフの内部領域を塗る
      integer,   intent(in) :: n                  !配列の長さ
      real,      intent(in), dimension(*) :: y    !独立変数
      real,      intent(in), dimension(*) :: x1   !棒の両端の座標値
      real,      intent(in), dimension(*) :: x2
    end subroutine
      
    subroutine uhbraz(n,x1,x2,y,itp1,itp2,rsize)  !棒グラフの内部領域を塗る
      integer,   intent(in) :: n                  !配列の長さ
      real,      intent(in), dimension(*) :: y    !独立変数
      real,      intent(in), dimension(*) :: x1   !棒の両端の座標値
      real,      intent(in), dimension(*) :: x2
      integer,   intent(in) :: itp1               !トーンパターン番号．
      integer,   intent(in) :: itp2
      real,      intent(in) :: rsize              !棒の幅
    end subroutine
      
    subroutine uhbrl (n,xd,y)                     !棒グラフを線で結ぶ．
      integer,   intent(in) :: n                  !配列の長さ
      real,      intent(in), dimension(*) :: y    !独立変数
      real,      intent(in), dimension(*) :: xd   !棒の端の値
    end subroutine
      
    subroutine uhbrlz(n,xd,y,itype,index,rsize)   !棒グラフを線で結ぶ．
      integer,   intent(in) :: n                  !配列の長さ
      real,      intent(in), dimension(*) :: y    !独立変数
      real,      intent(in), dimension(*) :: xd   !棒の端の値
      integer,   intent(in) :: itype              !ラインタイプ
      integer,   intent(in) :: index              !ラインインデクス
      real,      intent(in) :: rsize              !棒の幅
    end subroutine
!--------------------------------------------------------
    subroutine uhbxf (n,x1,x2,y)                  !箱グラフの枠を描く．
      integer,   intent(in) :: n                  !配列の長さ
      real,      intent(in), dimension(*) :: y  !独立変数
      real,      intent(in), dimension(*) :: x1   !箱の両端の座標値
      real,      intent(in), dimension(*) :: x2
    end subroutine
      
    subroutine uhbxfz(n,x1,x2,y,itype,index)      !箱グラフの枠を描く．
      integer,   intent(in) :: n                  !配列の長さ
      real,      intent(in), dimension(*) :: y  !独立変数
      real,      intent(in), dimension(*) :: x1   !箱の両端の座標値
      real,      intent(in), dimension(*) :: x2
      integer,   intent(in) :: itype              !枠のラインタイプ
      integer,   intent(in) :: index              !枠のラインインデクス
    end subroutine
      
    subroutine uhbxa (n,x1,x2,y)                  !箱グラフの内部領域を塗る．
      integer,   intent(in) :: n                  !配列の長さ
      real,      intent(in), dimension(*) :: y  !独立変数
      real,      intent(in), dimension(*) :: x1   !箱の両端の座標値
      real,      intent(in), dimension(*) :: x2
    end subroutine
      
    subroutine uhbxaz(n,x1,x2,y,itp1,itp2)        !箱グラフの内部領域を塗る．
      integer,   intent(in) :: n                  !配列の長さ
      real,      intent(in), dimension(*) :: y  !独立変数
      real,      intent(in), dimension(*) :: x1   !箱の両端の座標値
      real,      intent(in), dimension(*) :: x2
      integer,   intent(in) :: itp1               !トーンパターン番号．
      integer,   intent(in) :: itp2
    end subroutine
      
    subroutine uhbxl (n,xd,y)                     !階段状のグラフを描く．
      integer,   intent(in) :: n                  !配列の長さ
      real,      intent(in), dimension(*) :: y  !独立変数
      real,      intent(in), dimension(*) :: xd   !従属変数
    end subroutine
      
    subroutine uhbxlz(n,xd,y,itype,index)         !階段状のグラフを描く．
      integer,   intent(in) :: n                  !配列の長さ
      real,      intent(in), dimension(*) :: y  !独立変数
      real,      intent(in), dimension(*) :: xd   !従属変数
      integer,   intent(in) :: itype              !ラインタイプ
      integer,   intent(in) :: index              !ラインインデクス
    end subroutine

  end interface
end module
!uhpack library end ----
