!-------------------------------------------------
!interface module of uupack
!-------------------------------------------------
module uu_interface
  interface
!--------------------------------------------------------
    subroutine uulin (n,x,y)                      !折れ線を描く．
      integer,   intent(in) :: n                  !配列x, y の長さ
      real,      intent(in), dimension(*) :: x    !折れ線の座標値
      real,      intent(in), dimension(*) :: y
    end subroutine
      
    subroutine uulinz(n,x,y,itype,index)          !折れ線を描く．
      integer,   intent(in) :: n                  !配列x, y の長さ
      real,      intent(in), dimension(*) :: x    !折れ線の座標値
      real,      intent(in), dimension(*) :: y
      integer,   intent(in) :: itype              !折れ線のラインタイプ
      integer,   intent(in) :: index              !折れ線のラインインデクス
    end subroutine
!--------------------------------------------------------      
    subroutine uumrk (n,x,y)                      !マーカー列を描く．
      integer,   intent(in) :: n                  !配列x, y の長さ
      real,      intent(in), dimension(*) :: x    !マーカーの座標値
      real,      intent(in), dimension(*) :: y
    end subroutine
      
    subroutine uumrkz(n,x,y,itype,index,rsize)    !マーカー列を描く．
      integer,   intent(in) :: n                  !配列x, y の長さ
      real,      intent(in), dimension(*) :: x    !マーカーの 座標値
      real,      intent(in), dimension(*) :: y
      integer,   intent(in) :: itype              !マーカーのラインタイプ
      integer,   intent(in) :: index              !マーカーのラインインデクス
      real,      intent(in) :: rsize              !マーカーの大きさ
    end subroutine
!--------------------------------------------------------
    subroutine uuslnt(itype)                      !折れ線の属性を指定する．
      integer,   intent(in) :: itype              !折れ線のラインタイプ
    end subroutine
      
    subroutine uuslni(index)                      !折れ線の属性を指定する．
      integer,   intent(in) :: index              !折れ線のラインインデクス
    end subroutine
      
    subroutine uusmkt(itype)                      !マーカーの属性を指定する．
      integer,   intent(in) :: itype              !マーカーのラインタイプ
    end subroutine
      
    subroutine uusmki(index)                      !マーカーの属性を指定する．
      integer,   intent(in) :: index              !マーカーのラインインデクス
    end subroutine
      
    subroutine uusmks(rsize)                      !マーカーの属性を指定する．
      real,      intent(in) :: rsize              !マーカーの大きさ
    end subroutine
      
    subroutine uusebt(itype)                      !エラーバーの属性を指定する．
      integer,   intent(in) :: itype              !エラーバーのラインタイプ
    end subroutine
      
    subroutine uusebi(index)                      !エラーバーの属性を指定する．
      integer,   intent(in) :: index              !エラーバーのラインインデクス
    end subroutine
      
    subroutine uusebs(rsize)                      !エラーバーの属性を指定する．
      real,      intent(in) :: rsize              !エラーバーの横幅
    end subroutine
      
    subroutine uusbrs(rsize)                      !棒グラフの横巾を指定する．
      real,      intent(in) :: rsize              !棒グラフの横幅
    end subroutine
      
    subroutine uusarp(itp1,itp2)                  !内部領域を塗るトーンパターンを指定する．
      integer,   intent(in) :: itp1               !トーンパターン番号
      integer,   intent(in) :: itp2
    end subroutine
      
    subroutine uusfrt(itype)                      !枠の属性を指定する．
      integer,   intent(in) :: itype              !枠のラインタイプ
    end subroutine
      
    subroutine uusfri(index)                      !枠の属性を指定する．
      integer,   intent(in) :: index              !枠のラインインデクス
    end subroutine
      
    subroutine uusidv(umin, umax)                 !独立変数の定義域を指定する．
      real,      intent(in) :: umin               !定義域の最大最小値
      real,      intent(in) :: umax
    end subroutine

  end interface
end module
!uupack library end ----
