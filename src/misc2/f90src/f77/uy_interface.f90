!-------------------------------------------------
!interface module of uypack
!-------------------------------------------------
module uy_interface
  interface
!--------------------------------------------------------------------------
    subroutine uyaxdv(cside,dy1,dy2)              !目盛とラベルを描く間隔を指定して座標軸を描く．
      character(len=1), intent(in) :: cside       !座標軸を描く場所を指定する
      real,      intent(in) :: dy1                !小さめの目盛を描く 間隔を指定する
      real,      intent(in) :: dy2                !大きめの目盛および ラベルを描く間隔を指定する
    end subroutine
      
    subroutine uyaxnm(cside,uy1,n1,uy2,n2)        !目盛とラベルを描く場所を指定して座標軸を描く．
      character(len=1), intent(in) :: cside       !座標軸を描く場所を指定する
      real,      intent(in), dimension(n1) :: uy1 !小さめの目盛を描く 場所を格納した長さn1の配列
      integer,   intent(in) :: n1                 !配列ux1, uy1の長さ
      real,      intent(in), dimension(n2) :: uy2 !大きめの目盛およびラベルを描く場所を格納した長さn2の配列
      integer,   intent(in) :: n2                 !配列ux1, uy1の長さ
    end subroutine
      
    subroutine uyaxlb(cside,uy1,n1,uy2,ch,nc,n2)  !目盛とラベルを描く場所を指定して座標軸を描く
      character(len=1), intent(in) :: cside       !座標軸を描く場所を指定する
      real,      intent(in), dimension(n1) :: uy1 !小さめの目盛を描く 場所を格納した長さn1の配列
      integer,   intent(in) :: n1                 !配列ux1, uy1の長さ
      real,      intent(in), dimension(n2) :: uy2 !大きめの目盛およびラベルを描く場所を格納した長さn2の配列
      character(len=nc), intent(in), dimension(n2) :: ch !描くラベルを格納した文字長nc,長さn2の文字型配列
      integer,   intent(in) :: nc                 !配列chの文字長
      integer,   intent(in) :: n2                 !配列ux2, uy2, chの長さ
    end subroutine
      
    subroutine uymttl(cside,cttl,py)              !大きめのタイトルを描く．
      character(len=1), intent(in) :: cside       !座標軸を描く場所を指定する
      character(len=*), intent(in) :: cttl        !描くタイトル
      real,      intent(in) :: py                 !-1から+1の間の実数値でタイトルを描く位置を指定する
    end subroutine
      
    subroutine uysttl(cside,cttl,py)              !小さめのタイトルを描く．
      character(len=1), intent(in) :: cside       !座標軸を描く場所を指定する
      character(len=*), intent(in) :: cttl        !描くタイトル
      real,      intent(in) :: py                 !-1から+1の間の実数値でタイトルを描く位置を指定する
    end subroutine
!----------------------------------------------------------------------------
    subroutine uypaxs(cside,islct)                !軸を示す線分を描く．
      character(len=1), intent(in) :: cside       !座標軸を描く場所を指定する
      integer,   intent(in) :: islct              !描く軸の属性を指定する
    end subroutine
      
    subroutine uyptmk(cside,islct,uy,n)           !目盛を描く．
      character(len=1), intent(in) :: cside       !目盛を描く座標軸の場所を指定する
      integer,   intent(in) :: islct              !描く目盛の属性を指定する
      real,      intent(in), dimension(n) :: uy   !目盛を描く場所を格納した長さnの 配列
      integer,   intent(in) :: n                  !配列ux, uyの長さ
    end subroutine
      
    subroutine uyplbl(cside,islct,uy,ch,nc,n)     !文字列で指定したラベルを描く．
      character(len=1), intent(in) :: cside       !ラベルを描く座標軸を描く場所を指定する
      integer,   intent(in) :: islct              !描くラベルの属性を指定する
      real,      intent(in), dimension(n) :: uy   !ラベルを描く場所を格納した 長さnの配列
      character(len=nc), intent(in), dimension(n) :: ch !描くラベルを格納した文字長nc長さnの文字型配列
      integer,   intent(in) :: nc                 !配列chの文字長
      integer,   intent(in) :: n                  !配列ux, uy, chの長さ
    end subroutine
      
    subroutine uypnum(cside,islct,uy,n)           !数値で指定したラベルを描く．
      character(len=1), intent(in) :: cside       !ラベルを描く座標軸を描く場所を指定する
      integer,   intent(in) :: islct              !描くラベルの属性を指定する
      real,      intent(in), dimension(n) :: uy   !ラベルを描く場所を格納した 長さnの配列
      integer,   intent(in) :: n                  !配列ux, uyの長さ
    end subroutine
      
    subroutine uypttl(cside,islct,cttl,py)        !タイトルを描く．
      character(len=1), intent(in) :: cside       !タイトルを描く座標軸を描く場所を指定する
      integer,   intent(in) :: islct              !描くタイトルの属性を指定する
      character(len=*), intent(in) :: cttl        !描くタイトル
      real,      intent(in) :: py                 !-1から+1の間の実数値でタイトルを描く位置を指定する
    end subroutine
      
    subroutine uysaxs(cside)                      !軸をずらす。
      character(len=1), intent(in) :: cside 
    end subroutine

  end interface
end module
!uypack library end ----
