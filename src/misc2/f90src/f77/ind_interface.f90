!-------------------------------------------------
!interface module of indxlib
!-------------------------------------------------
module ind_interface
  interface

    function indxcf(cx,n,jd,ch)                   !指定した文字が最初に現れる位置を求める（文字長 は1)．
      character(len=1), intent(in),dimension(*) :: cx          !調べる文字列
      integer,   intent(in) :: n                  !調べる文字の個数
      integer,   intent(in) :: jd                 !調べる文字の間隔
      character(len=1), intent(in) :: ch          !指定した文字
      integer indxcf                              !最初に現れる位置を与える関数値
    end function                                  
      
    function indxcl(cx,n,jd,ch)                   !指定した文字が最後に現れる位置を求める（文字長 は1).
      character(len=1), intent(in),dimension(*) :: cx          !調べる文字列
      integer,   intent(in) :: n                  !調べる文字の個数
      integer,   intent(in) :: jd                 !調べる文字の間隔
      character(len=1), intent(in) :: ch          !指定した文字
      integer indxcl                              !最後に現れる位置を与える関数値
    end function                                  
      
    function indxnf(cx,n,jd,ch)                   !指定した文字が最初に現れる位置を求める（文字長 は1以上).
      character(len=*), intent(in) :: cx          !調べる文字列
      integer,   intent(in) :: n                  !調べる文字の個数
      integer,   intent(in) :: jd                 !調べる文字の間隔
      character(len=*), intent(in) :: ch          !指定した文字
      integer indxnf                              !最初に現れる位置を与える関数値
    end function
      
    function indxnl(cx,n,jd,ch)                   !指定した文字が最後に現れる位置を求める（文字長 は1以上).
      character(len=*), intent(in) :: cx          !調べる文字列
      integer,   intent(in) :: n                  !調べる文字の個数
      integer,   intent(in) :: jd                 !調べる文字の間隔
      character(len=*), intent(in) :: ch          !指定した文字
      integer indxnl                              !最後に現れる位置を与える関数値
    end function
      
    function indxmf(cx,n,jd,ch)                   !指定した文字が最初に現れる位置を求める．大文字・小文字を区別しない
      character(len=*), intent(in) :: cx          !調べる文字列
      integer,   intent(in) :: n                  !調べる文字の個数
      integer,   intent(in) :: jd                 !調べる文字の間隔
      character(len=*), intent(in) :: ch          !指定した文字
      integer indxmf                              !最初に現れる位置を与える関数値
    end function
      
    function indxml(cx,n,jd,ch)                   !指定した文字が最後に現れる位置を求める．大文字・小文字を区別しない
      character(len=*), intent(in) :: cx          !調べる文字列
      integer,   intent(in) :: n                  !調べる文字の個数
      integer,   intent(in) :: jd                 !調べる文字の間隔
      character(len=*), intent(in) :: ch          !指定した文字
      integer indxml                              !最後に現れる位置を与える関数値
    end function
      
    function indxif(ix,n,jd,ii)                   !指定した整数値が最初に現れる位置を求める．
      integer,   intent(in), dimension(*) :: ix   !調べる整数型配列
      integer,   intent(in) :: n                  !調べる配列要素の個数
      integer,   intent(in) :: jd                 !調べる配列要素の間隔
      integer,   intent(in) :: ii                 !指定した整数値
      integer indxif                              !最初に現れる位置を与える関数値
    end function
      
    function indxil(ix,n,jd,ii)                   !指定した整数値が最後に現れる位置を求める．
      integer,   intent(in), dimension(*) :: ix   !調べる整数型配列
      integer,   intent(in) :: n                  !調べる配列要素の個数
      integer,   intent(in) :: jd                 !調べる配列要素の間隔
      integer,   intent(in) :: ii                 !指定した整数値
      integer indxil                              !最後に現れる位置を与える関数値
    end function
      
    function indxrf(rx,n,jd,rr)                   !指定した実数値が最初に現れる位置を求める．
      real,      intent(in), dimension(*) :: rx   !調べる実数型配列
      integer,   intent(in) :: n                  !調べる配列要素の個数
      integer,   intent(in) :: jd                 !調べる配列要素の間隔
      real,      intent(in) :: rr                 !指定した実数値
      integer indxrf                              !最初に現れる位置を与える関数値
    end function
      
    function indxrl(rx,n,jd,rr)                   !指定した実数値が最後に現れる位置を求める．
      real,      intent(in), dimension(*) :: rx   !調べる実数型配列
      integer,   intent(in) :: n                  !調べる配列要素の個数
      integer,   intent(in) :: jd                 !調べる配列要素の間隔
      real,      intent(in) :: rr                 !指定した実数値
      integer indxrl                              !最後に現れる位置を与える関数値
    end function
      
    function nindxc(cx,n,jd,ch)                   !指定した文字（文字長は1)が何個あるかを求める．
      character(len=1), intent(in), dimension(*) :: cx          !調べる文字列
      integer,   intent(in) :: n                  !調べる文字の個数
      integer,   intent(in) :: jd                 !調べる文字の間隔
      character(len=1), intent(in) :: ch          !指定した文字
      integer nindxc                              !文字の個数を与える関数値
    end function
      
    function nindxn(cx,n,jd,ch)                   !指定した文字（文字長は1以上）が何個あるかを求める．
      character(len=*), intent(in) :: cx          !調べる文字列
      integer,   intent(in) :: n                  !調べる文字の個数
      integer,   intent(in) :: jd                 !調べる文字の間隔
      character(len=*), intent(in) :: ch          !指定した文字
      integer nindxn                              !文字の個数を与える関数値
    end function
      
    function nindxm(cx,n,jd,ch)                   !指定した文字（文字長は1以上）が何個あるかを求める． 大文字・小文字を区別しないで調べる．
      character(len=*), intent(in) :: cx          !調べる文字列
      integer,   intent(in) :: n                  !調べる文字の個数
      integer,   intent(in) :: jd                 !調べる文字の間隔
      character(len=*), intent(in) :: ch          !指定した文字
      integer nindxm                              !文字の個数を与える関数値
    end function
      
    function nindxi(ix,n,jd,ii)                   !指定した整数値が何個あるかを求める．
      integer,   intent(in), dimension(*) :: ix   !調べる整数型配列
      integer,   intent(in) :: n                  !調べる配列要素の個数
      integer,   intent(in) :: jd                 !調べる配列要素の間隔
      integer,   intent(in) :: ii                 !指定した整数値
      integer nindxi                              !整数値の個数を与える関数値
    end function
      
    function nindxr(rx,n,jd,rr)                   !指定した実数値が何個あるかを求める．
      real,      intent(in), dimension(*) :: rx   !調べる実数型配列
      integer,   intent(in) :: n                  !調べる配列要素の個数
      integer,   intent(in) :: jd                 !調べる配列要素の間隔
      real,      intent(in) :: rr                 !指定した実数値
      integer nindxr                              !実数の個数を与える関数値
    end function

  end interface
end module
!indxlib library end ----
