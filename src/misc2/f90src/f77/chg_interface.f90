!-------------------------------------------------
!interface module of chglib
!-------------------------------------------------
module chg_interface
  interface

    subroutine cupper(ch)                         !文字列を大文字化化する．
      character(len=*), intent(inout) :: ch       !処理する文字列
    end subroutine
      
    subroutine clower(ch)                         !文字列を小文字化する．
      character(len=*), intent(inout) :: ch       !処理する文字列
    end subroutine

  end interface
end module
!chglib library end ----
