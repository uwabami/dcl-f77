!-------------------------------------------------
!interface module of vrblib
!-------------------------------------------------
module vrb_interface
  interface

    subroutine vrfnb(rx,ry,rz,n,jx,jy,jz,rfnb)    !RXとRYにRFNBを作用させてRZに代入する
      real,      intent(in), dimension(*) :: rx   !処理する実数型配列
      real,      intent(in), dimension(*) :: ry
      real,      intent(out), dimension(*) :: rz
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !配列rx, ry, rzにおいて，処理する配列要素の間隔
      integer,   intent(in) :: jy
      integer,   intent(in) :: jz
      external rfnb                               !引数が2個である実数型関数名
    end subroutine
      
    subroutine vradd(rx,ry,rz,n,jx,jy,jz)         !RXとRYを加えてRZに代入する
      real,      intent(in), dimension(*) :: rx   !処理する実数型配列
      real,      intent(in), dimension(*) :: ry
      real,      intent(out), dimension(*) :: rz
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !配列rx, ry, rzにおいて，処理する配列要素の間隔
      integer,   intent(in) :: jy
      integer,   intent(in) :: jz
    end subroutine
      
    subroutine vrsub(rx,ry,rz,n,jx,jy,jz)         !RXからRYを引いてRZに代入する．
      real,      intent(in), dimension(*) :: rx   !処理する実数型配列
      real,      intent(in), dimension(*) :: ry
      real,      intent(out), dimension(*) :: rz
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !配列rx, ry, rzにおいて，処理する配列要素の間隔
      integer,   intent(in) :: jy
      integer,   intent(in) :: jz
    end subroutine
      
    subroutine vrmlt(rx,ry,rz,n,jx,jy,jz)         !RXとRYを掛けてRZに代入する．
      real,      intent(in), dimension(*) :: rx   !処理する実数型配列
      real,      intent(in), dimension(*) :: ry
      real,      intent(out), dimension(*) :: rz
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !配列rx, ry, rzにおいて，処理する配列要素の間隔
      integer,   intent(in) :: jy
      integer,   intent(in) :: jz
    end subroutine
      
    subroutine vrdiv(rx,ry,rz,n,jx,jy,jz)         !RXをRYで割ってRZに代入する．
      real,      intent(in), dimension(*) :: rx   !処理する実数型配列
      real,      intent(in), dimension(*) :: ry
      real,      intent(out), dimension(*) :: rz
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !配列rx, ry, rzにおいて，処理する配列要素の間隔
      integer,   intent(in) :: jy
      integer,   intent(in) :: jz
    end subroutine

  end interface
end module
!vrblib library end ----
