!-------------------------------------------------
!interface module of timelib
!-------------------------------------------------
module tim_interface
  interface

    subroutine time12(itime,itt)                  !1型の時刻を2型の時刻に変換する．
      integer,   intent(in) :: itime              !1型の時刻
      integer,   intent(out) :: itt               !通しの秒数
    end subroutine
      
    subroutine time13(itime,ih,im,is)             !1型の時刻を3型の時刻に変換する．
      integer,   intent(in) :: itime              !1型の時刻
      integer,   intent(out) :: ih                !時
      integer,   intent(out) :: im                !分
      integer,   intent(out) :: is                !秒
    end subroutine
      
    subroutine time21(itime,itt)                  !2型の時刻を1型の時刻に変換する．
      integer,   intent(out) :: itime              !1型の時刻
      integer,   intent(in) :: itt               !通しの秒数
    end subroutine
      
    subroutine time23(ih,im,is,itt)               !2型の時刻を3型の時刻に変換する．
      integer,   intent(out) :: ih                 !時
      integer,   intent(out) :: im                 !分
      integer,   intent(out) :: is                 !秒
      integer,   intent(in) :: itt               !通しの秒数
    end subroutine
      
    subroutine time31(itime,ih,im,is)             !3型の時刻を1型の時刻に変換する．
      integer,   intent(out) :: itime              !1型の時刻
      integer,   intent(in) :: ih                !時
      integer,   intent(in) :: im                !分
      integer,   intent(in) :: is                !秒
    end subroutine
      
    subroutine time32(ih,im,is,itt)               !3型の時刻を2型の時刻に変換する．
      integer,   intent(in) :: ih                 !時
      integer,   intent(in) :: im                 !分
      integer,   intent(in) :: is                 !秒
      integer,   intent(out) :: itt               !通しの秒数
    end subroutine
      
    subroutine timeq1(itime)                      !現在の1型の時刻を求める．
      integer,   intent(out) :: itime             !1型の時刻
    end subroutine
      
    subroutine timeq2(itt)                        !現在の2型の時刻を求める．
      integer,   intent(out) :: itt               !通しの秒数
    end subroutine
      
    subroutine timeq3(ih,im,is)                   !現在の3型の時刻を求める．
      integer,   intent(out) :: ih                !時
      integer,   intent(out) :: im                !分
      integer,   intent(out) :: is                !秒
    end subroutine
      
    subroutine timec1(cform,itime)                !itimeをcformに従って表現してcformで返す．
      integer,   intent(in) :: itime              !1型の時刻
      character(len=*), intent(inout) :: cform    !時刻のフォーマット
    end subroutine
      
    subroutine timec2(cform,itt)                  !ittをcformに従って表現してcformで返す．
      integer,   intent(in) :: itt                !通しの秒数
      character(len=*), intent(inout) :: cform    !時刻のフォーマット
    end subroutine
      
    subroutine timec3(cform,ih,im,is)             !ih,im,isをcformに従って表現してcformで返す．
      integer,   intent(in) :: ih                 !時
      integer,   intent(in) :: im                 !分
      integer,   intent(in) :: is                 !秒
      character(len=*), intent(inout) :: cform    !時刻のフォーマット
    end subroutine

  end interface
end module
!timelib library end ----
