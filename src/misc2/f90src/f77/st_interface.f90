!-------------------------------------------------
!     interface module of stpack
!-------------------------------------------------
      module st_interface
        interface
!--------------------------------------------------
!座標変換
        subroutine stftrf(ux,uy,vx,vy)  !正規化変換 
            real,      intent(in) :: ux  !UC の 座標
            real,      intent(in) :: uy
            real,      intent(out) :: vx !VC の 座標 
            real,      intent(out) :: vy
        end subroutine

        subroutine stitrf(vx,vy,ux,uy)   !逆変換            
            real,      intent(in) :: vx   !VC の 座標 
            real,      intent(in) :: vy
            real,      intent(out) :: ux  !UC の 座標
            real,      intent(out) :: uy
        end subroutine

        subroutine ststrf(lmap)
            logical,   intent(in) :: lmap     !地図投影関数を示すフラグ
        end subroutine

        subroutine stqtrf(lmap) !パラメタ問い合わせルーチン             
            logical,   intent(out) :: lmap     !地図投影関数を示すフラグ
        end subroutine
        !----------------------------
        subroutine stfpr2(vx,vy,rx,ry) !二次元透視変換  
            real,      intent(in) :: vx  !VC の座標
            real,      intent(in) :: vy
            real,      intent(out) :: rx !RC の座標
            real,      intent(out) :: ry
        end subroutine

        subroutine stipr2(rx,ry,vx,vy) !逆変換    
            real,      intent(in) :: rx        !RC の座標
            real,      intent(in) :: ry
            real,      intent(out) :: vx    !VC の座標
            real,      intent(out) :: vy
        end subroutine

        subroutine stspr2(ix,iy,sect) 
            integer,   intent(in) :: ix    !2次元のX軸とY軸を，3次元のどの軸に                      
            integer,   intent(in) :: iy    !対応させるか，番号で指定する
            real,      intent(in) :: sect     !2次元平面と交わる座標軸の交点座標
        end subroutine
        !----------------------------               
        subroutine stftr3(ux,uy,uz,vx,vy,vz) !3次元正規化変換 
            real,      intent(in) :: ux   !UC の座標
            real,      intent(in) :: uy
            real,      intent(in) :: uz
            real,      intent(out) :: vx  !VC の座標
            real,      intent(out) :: vy
            real,      intent(out) :: vz
        end subroutine
                !----------------------------
        subroutine stfpr3(vx,vy,vz,rx,ry) !三次元透視変換
            real,      intent(in) :: vx  !VC の座標
            real,      intent(in) :: vy
            real,      intent(in) :: vz
            real,      intent(out) :: rx !RC の座標
            real,      intent(out) :: ry
        end subroutine

        subroutine stspr3(xfc,yfc,zfc,theta,phi,psi,fac,zview,rxoff,ryoff)
            real,      intent(in) :: xfc   !焦点中心の座標                                                    
            real,      intent(in) :: yfc
            real,      intent(in) :: zfc
            real,      intent(in) :: theta !回転角                
            real,      intent(in) :: phi
            real,      intent(in) :: psi
            real,      intent(in) :: fac   !拡大縮小ファクター
            real,      intent(in) :: zview !視点のZ座標
            real,      intent(in) :: rxoff !オフセット
            real,      intent(in) :: ryoff
        end subroutine
        !----------------------------
        subroutine stfwtr(rx,ry,wx,wy) !ワークステーション変換 
            real,      intent(in) :: rx   !RC の座標                         
            real,      intent(in) :: ry
            real,      intent(out) :: wx  !WC の座標
            real,      intent(out) :: wy
        end subroutine

        subroutine stiwtr(wx,wy,rx,ry) 
            real,      intent(in) :: wx   !WC の座標
            real,      intent(in) :: wy
            real,      intent(out) :: rx  !RC の座標
            real,      intent(out) :: ry
        end subroutine
                              
        subroutine stswtr(rxmin,rxmax,rymin,rymax,wxmin,wxmax,wymin,wymax,iwtrf)  
            real,      intent(in) :: rxmin  !ワークステーションウインドウ
            real,      intent(in) :: rxmax
            real,      intent(in) :: rymin
            real,      intent(in) :: rymax
            real,      intent(in) :: wxmin !ワークステーションビューポート
            real,      intent(in) :: wxmax
            real,      intent(in) :: wymin
            real,      intent(in) :: wymax
            integer,   intent(in) :: iwtrf  !変換番号
        end subroutine
    
        subroutine stqwtr(rxmin,rxmax,rymin,rymax,wxmin,wxmax,wymin,wymax,iwtrf)
            real,      intent(out) :: rxmin  !ワークステーションウインドウ
            real,      intent(out) :: rxmax
            real,      intent(out) :: rymin
            real,      intent(out) :: rymax
            real,      intent(out) :: wxmin     !ワークステーションビューポート
            real,      intent(out) :: wxmax
            real,      intent(out) :: wymin
            real,      intent(out) :: wymax
            integer,   intent(out) :: iwtrf  !変換番号
        end subroutine
!-----------------------------------------------------------------------
!下請けルーチン
        subroutine stftrn(ux,uy,vx,vy)  !正規化変換
            real,      intent(in) :: ux  !UC の座標
            real,      intent(in) :: uy
            real,      intent(out) :: vx !VC の座標
            real,      intent(out) :: vy
        end subroutine

        subroutine stitrn(vx,vy,ux,uy)
            real,      intent(in) :: vx  !VC の座標
            real,      intent(in) :: vy
            real,      intent(out) :: ux !UC の座標
            real,      intent(out) :: uy
        end subroutine

        subroutine ststrn(itr,cxa,cya,vx0,vy0)
            integer,   intent(in) :: itr     !変換関数番号
            real,      intent(in) :: cxa     !座標値にかけるファクター
            real,      intent(in) :: cya
            real,      intent(in) :: vx0     !VC における原点の位置       
            real,      intent(in) :: vy0
        end subroutine
        !----------------------------------------
        subroutine stfrot(ux,uy,tx,ty)!        地図座標の回転
            real,      intent(in) :: ux  !VC の座標
            real,      intent(in) :: uy
            real,      intent(out) :: tx !TC の座標
            real,      intent(out) :: ty
        end subroutine

        subroutine stirot(tx,ty,ux,uy)
            real,      intent(in) :: tx  !TC の座標
            real,      intent(in) :: ty
            real,      intent(out) :: ux !VC の座標
            real,      intent(out) :: uy
        end subroutine

        subroutine stsrot(theta,phi,psi) !地図座標の回転パラメタ設定
            real,      intent(in) :: theta     !回転角       
            real,      intent(in) :: phi
            real,      intent(in) :: psi
        end subroutine
        !-------------------------------------------
        subroutine stfrad(ux1,uy1,ux2,uy2)  !角度の単位変換
            real,      intent(in) :: ux1    !UC の座標                                         
            real,      intent(in) :: uy1
            real,      intent(out) :: ux2    !UC の座標（常にラジアン）                         
            real,      intent(out) :: uy2
        end subroutine

        subroutine stirad(ux2,uy2,ux1,uy1)                    
            real,      intent(in) :: ux2    !UC の座標（常にラジアン）
            real,      intent(in) :: uy2
            real,      intent(out) :: ux1    !UC の座標
            real,      intent(out) :: uy1
        end subroutine

        subroutine stsrad(lxdeg,lydeg)  !角度の単位変換パラメタ設定 
            logical,   intent(in) :: lxdeg  !単位が「度」であることを示すフラグ
            logical,   intent(in) :: lydeg    
        end subroutine
        end interface
      end module
!  stpack library end ----
