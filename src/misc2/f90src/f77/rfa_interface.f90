!-------------------------------------------------
!interface module of rfalib
!-------------------------------------------------
module rfa_interface
  interface

    function rmax(rx,n,jx)                        !最大値を求める
      real,      intent(in), dimension(*) :: rx   !処理する実数型配列
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !処理する配列要素の間隔
      real rmax                                   !最大値を与える関数値
    end function
      
    function rmin(rx,n,jx)                        !最小値を求める
      real,      intent(in), dimension(*) :: rx   !処理する実数型配列
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !処理する配列要素の間隔
      real rmin                                   !最小値を与える関数値
    end function
      
    function rsum(rx,n,jx)                        !総和を求める
      real,      intent(in), dimension(*) :: rx   !処理する実数型配列
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !処理する配列要素の間隔
      real rsum                                   !総和を与える関数値
    end function
      
    function rave(rx,n,jx)                        !平均を求める
      real,      intent(in), dimension(*) :: rx   !処理する実数型配列
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !処理する配列要素の間隔
      real rave                                   !平均を与える関数値
    end function
      
    function rvar(rx,n,jx)                        !分散を求める
      real,      intent(in), dimension(*) :: rx   !処理する実数型配列
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !処理する配列要素の間隔
      real rvar                                   !分散を与える関数値
    end function
      
    function rstd(rx,n,jx)                        !標準偏差を求める
      real,      intent(in), dimension(*) :: rx   !処理する実数型配列
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !処理する配列要素の間隔
      real rstd                                   !標準偏差を与える関数値
    end function
      
    function rrms(rx,n,jx)                        !root mean squareを求める
      real,      intent(in), dimension(*) :: rx   !処理する実数型配列
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !処理する配列要素の間隔
      real rrms                                   !root mean squareを与える関数値．
    end function
      
    function ramp(rx,n,jx)                        !大きさ (（Σ rx^2)^1/2 ) を求める
      real,      intent(in), dimension(*) :: rx   !処理する実数型配列
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !処理する配列要素の間隔
      real ramp                                   !大きさを与える関数値
    end function
      
    function rvmax(rx,ns,np,nq,nd)                !多次元配列の（一部分の）最大値を求める
      real,      intent(in), dimension(*) :: rx   !処理する実数型配列
      integer,   intent(in), dimension(*) :: ns   !配列の各次元の長さを与える 整数型配列
      integer,   intent(in), dimension(*) :: np   !各次元の処理をおこなう最初の位置を与える整数型配列
      integer,   intent(in), dimension(*) :: nq   !各次元の処理をおこなう最後の位置を与える整数型配列
      integer,   intent(in) :: nd                 !配列の次元数
      real                  :: rvmax              !最大値を与える関数値
    end function
      
    function rvmin(rx,ns,np,nq,nd)                !多次元配列の（一部分の）最小値を求める
      real,      intent(in), dimension(*) :: rx   !処理する実数型配列
      integer,   intent(in), dimension(*) :: ns   !配列の各次元の長さを与える 整数型配列
      integer,   intent(in), dimension(*) :: np   !各次元の処理をおこなう最初の位置を与える整数型配列
      integer,   intent(in), dimension(*) :: nq   !各次元の処理をおこなう最後の位置を与える整数型配列
      integer,   intent(in) :: nd                 !配列の次元数
      real                  :: rvmin              !最小値を与える関数値
    end function

  end interface
end module
!rfalib library end ----
