!-------------------------------------------------
!  CHKlib Module
!-------------------------------------------------
module chklib
  use dcl_common
  contains

    function DclCheckBlank(c)             !空白かどうかを判別する．
      character(len=1), intent(in) :: c
      logical DclCheckBlank, lchrb

      call prcopn('DclCheckBlank')
      DclCheckBlank = lchrb(c) 
      call prccls('DclCheckBlank')
    end function
      
    function DclCheckCurrency(c)          !通貨記号かどうかを判別する
      character(len=1), intent(in) :: c
      logical DclCheckCurrency, lchrc

      call prcopn('DclCheckCurrency')
      DclCheckCurrency = lchrc(c)
      call prccls('DclCheckCurrency')
    end function
      
    function DclCheckSpecial(c)           !特殊文字かどうかを判別する
      character(len=1), intent(in) :: c
      logical DclCheckSpecial, lchrs

      call prcopn('DclCheckSpecial')
      DclCheckSpecial = lchrs(c)
      call prccls('DclCheckSpecial')
    end function
      
    function DclCheckAlphabet(c)          !英字かどうかを判別する
      character(len=1), intent(in) :: c 
      logical DclCheckAlphabet, lchrl

      call prcopn('DclCheckAlphabet')
      DclCheckAlphabet = lchrl(c)
      call prccls('DclCheckAlphabet')
    end function
      
    function DclCheckNumber(c)            !数字かどうかを判別する
      character(len=1), intent(in) :: c 
      logical DclCheckNumber, lchrd

      call prcopn('DclCheckNumber')
      DclCheckNumber = lchrd(c) 
      call prccls('DclCheckNumber')
    end function
      
    function DclCheckAlphaNum(c)          !英数字かどうかを判別する
      character(len=1), intent(in) :: c
      logical DclCheckAlphaNum, lchra

      call prcopn('DclCheckAlphaNum')
      DclCheckAlphaNum = lchra(c)
      call prccls('DclCheckAlphaNum')
    end function
      
    function DclCheckFortran(c)           !fortran文字かどうかを判別する
      character(len=1), intent(in) :: c 
      logical DclCheckFortran, lchrf

      call prcopn('DclCheckFortran')
      DclCheckFortran = lchrf(c) 
      call prccls('DclCheckFortran')
    end function
      
    function DclCheckCharPattern(char,cref) !文字列の種類を判別する．
      character(len=*), intent(in) :: char  !文字種類を調べる文字列
      character(len=*), intent(in) :: cref  !文字列の種類を与えるテンプレート文字列
      logical DclCheckCharPattern, lchr

      call prcopn('DclCheckCharPattern')
      DclCheckCharPattern = lchr(char,cref)
      call prccls('DclCheckCharPattern')
    end function

end module
