module dcl_common
!  use dcl_f77

!-------------------- basic constants ------------------

  integer, parameter :: DCL_DISPLAY =1, DCL_PRINTER =2, DCL_PS_FILE =3

  integer, parameter :: DCL_UNI_UNI =1, DCL_UNI_LOG =2, DCL_LOG_UNI =3, &
                      & DCL_LOG_LOG =4, DCL_POLAR   =5, DCL_BIPOLAR =6, &
                      & DCL_ELLIPTIC=7
                      
  integer, parameter :: DCL_CYLINDRICAL=10, DCL_MERCATOR   =11, &
                      & DCL_MOLLWEIDE  =12, DCL_HAMMER     =13, &
                      & DCL_ECKERT6    =14, DCL_KITADA     =15, &
                      & DCL_CONICAL    =20, DCL_CON_AREA   =21, &
                      & DCL_CON_CONF   =22, DCL_BONNE      =23, &
                      & DCL_OTHOGAPHIC =30, DCL_POLARSTEREO=31, &
                      & DCL_AZIMUTHAL  =32, DCL_AZIM_AREA  =33
                      
  real,    parameter :: DCL_PI=3.1415927

!---------------- time & date -------------------

  character(len=9), dimension(12) :: DCL_MONTH =  &
         &   (/ 'JANUARY  ','FEBRUARY ','MARCH    ','APRIL    ', &
         &      'MAY      ','JUNE     ','JULY     ','AUGUST   ', &
         &      'SEPTEMBER','OCTOBER  ','NOVEMBER ','DECEMBER '  /)

  character(len=9), dimension(7) :: DCL_WEEK = &
         &   (/ 'SUNDAY   ','MONDAY   ','TUESDAY  ','WEDNESDAY', &
         &      'THURSDAY ','FRIDAY   ','SATURDAY '              /)
         

  type dcl_date
      integer :: year, month, day
  end type dcl_date

  type dcl_time
     integer :: hour, minute, second
  end type dcl_time
  
!------------------- coordinate -----------------
  type map
    real:: lon, lat
  end type map

  type cartesian
    real::x,y
  end type cartesian

  type polar
    real:: r, theta
  end type polar

  type elliptic
    real::u,v
  end type elliptic

  type hyperbolic
    real::u,v
  end type hyperbolic

  type bipolar
    real::u,v
  end type bipolar

  type cartesian3D
    real::x,y,z
  end type cartesian3D

  type spherical
    real::r,theta,phi
  end type spherical
!---------------------------------------------
  contains
    function DclSwapIndex(x)
      real, dimension(:,:), intent(in) :: x
      real, dimension(size(x,1), size(x,2)) :: DclSwapIndex
      integer, dimension(2)          :: shp, ord

      shp = (/size(x,2), size(x,1) /)
      ord = (/ 2, 1 /)

      DclSwapIndex = reshape(x, shape=shp, order=ord)
    end function
end module
