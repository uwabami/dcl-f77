!-------------------------------------------------
!  SCpack Module
!-------------------------------------------------
module scpack
  use dcl_common
  contains
!-----------------------------------------------------------------------
!正規化変換 
    subroutine DclSet3DViewPort(xmin,xmax, ymin,ymax, zmin,zmax)
      real, intent(in) :: xmin, xmax, ymin, ymax, zmin, zmax

      call prcopn('DclSet3DViewPort')
      call scsvpt(xmin,xmax,ymin,ymax,zmin,zmax)
      call prccls('DclSet3DViewPort')
    end subroutine

    subroutine DclSet3DWindow(xmin,xmax,ymin,ymax,zmin,zmax) !ウインドウの設定．
      real, intent(in) :: xmin, xmax, ymin, ymax, zmin, zmax

      call prcopn('DclSet3DWindow')
      call scswnd(xmin,xmax,ymin,ymax,zmin,zmax)
      call prccls('DclSet3DWindow')
    end subroutine

    subroutine DclSet3DLogAxis(log_x,log_y,log_z) !対数軸の設定．
      logical,  intent(in) :: log_x, log_y, log_z      !直角直線座標の対数軸指定

      call prcopn('DclSet3DLogAxis')
      call scslog(log_x, log_y, log_z )
      call prccls('DclSet3DLogAxis')
    end subroutine

    subroutine DclSet3DOrigin(factor,x,y,z) !スケーリングファクターと原点の設定
      real, intent(in) :: factor   !円筒座標，球座標のスケーリングファクター
      real, intent(in) :: x, y, z  !円筒座標，球座標の原点の位置

      call prcopn('DclSet3DOrigin')
      call scsorg(factor, x, y, z)
      call prccls('DclSet3DOrigin')
    end subroutine

    subroutine DclSet3DTransNumber(number) !変換関数番号の設定
      integer,   intent(in) :: number  !変換関数番号

      call prcopn('DclSet3DTransNumber')
      call scstrn(number)
      call prccls('DclSet3DTransNumber')
    end subroutine

    subroutine DclSet3DTransFunction() !変換関数の確定． 
      call prcopn('DclSet3DTransFunction')
      call scstrf()
      call prccls('DclSet3DTransFunction')
    end subroutine
        
!パラメタ参照ルーチン
    subroutine DclGet3DViewPort(xmin,xmax,ymin,ymax,zmin,zmax)
      real, intent(out) :: xmin, xmax, ymin, ymax, zmin, zmax

      call prcopn('DclGet3DViewPort')
      call scqvpt(xmin,xmax,ymin,ymax,zmin,zmax)
      call prccls('DclGet3DViewPort')
    end subroutine

    subroutine DclGet3DWindow(xmin,xmax,ymin,ymax,zmin,zmax) 
      real, intent(out) :: xmin, xmax, ymin, ymax, zmin, zmax

      call prcopn('DclGet3DWindow')
      call scqwnd(xmin,xmax,ymin,ymax,zmin,zmax) 
      call prccls('DclGet3DWindow')
    end subroutine

    subroutine DclGet3DLogAxis(log_x,log_y,log_z) 
      logical, intent(out) :: log_x,log_y,log_z

      call prcopn('DclGet3DLogAxis')
      call scqlog(log_x,log_y,log_z) 
      call prccls('DclGet3DLogAxis')
    end subroutine

    subroutine DclGet3DOrigin(factor, x, y, z) 
      real, intent(out) :: factor   !円筒座標，球座標のスケーリングファクター
      real, intent(out) :: x, y, z  !円筒座標，球座標の原点の位置

      call prcopn('DclGet3DOrigin')
      call scqorg(factor, x, y, z)
      call prccls('DclGet3DOrigin')
    end subroutine

    function DclGet3DTransNumber()
      integer :: DclGet3DTransNumber        !変換関数番号

      call prcopn('DclGet3DTransNumber')
      call scqtrn(DclGet3DTransNumber) 
      call prccls('DclGet3DTransNumber')
    end function
!-----------------------------------------------------------------------------
!透視変換
    subroutine DclSet3DEyePoint(x, y, z)  !視点の設定．
      real, intent(in) :: x, y, z

      call prcopn('DclSet3DEyePoint')
      call scseye(x, y, z)
      call prccls('DclSet3DEyePoint')
    end subroutine

    subroutine DclSet3DObjectPoint(x, y, z)  !焦点の設定
      real, intent(in) :: x, y, z

      call prcopn('DclSet3DObjectPoint')
      call scsobj(x, y, z)
      call prccls('DclSet3DObjectPoint')
    end subroutine

    subroutine DclSet2DPlane(x_dir, y_dir, section)  !2次元平面の割付
      integer, intent(in) :: x_dir, y_dir
      real,    intent(in) :: section

      call prcopn('DclSet2DPlane')
      call scspln(x_dir, y_dir, section)
      call prccls('DclSet2DPlane')
    end subroutine
 
    subroutine DclSet3DProjection()  !透視変換の確定． 
      call prcopn('DclSet3DProjection')
      call scsprj()
      call prccls('DclSet3DProjection')
    end subroutine

!パラメタ参照ルーチン
    subroutine DclGet3DEyePoint(x, y, z)
      real, intent(out) :: x, y, z

      call prcopn('DclGet3DEyePoint')
      call scqeye(x, y, z)
      call prccls('DclGet3DEyePoint')
    end subroutine

    subroutine DclGet3DObjectPoint(x, y, z)
      real, intent(out) :: x, y, z

      call prcopn('DclGet3DObjectPoint')
      call scqobj(x, y, z)
      call prccls('DclGet3DObjectPoint')
    end subroutine

    subroutine DclGet2DPlane(x_dir, y_dir, section)
      integer, intent(out) :: x_dir, y_dir
      real,    intent(out) :: section

      call prcopn('DclGet2DPlane')
      call scqpln(x_dir, y_dir, section)
      call prccls('DclGet2DPlane')
    end subroutine

!-----------------------------------------------------------------------------
!ポリライン
    subroutine DclDraw3DLine(x,y,z,index)  !u 座標系で折れ線を描く．  
      real,    intent(in), dimension(:) :: x, y, z !折れ線を結ぶ点のU座標
      integer, intent(in), optional     :: index
        

        call sgoopn('DclDraw3DLine', ' ')

        if(present(index)) then; index0 = index
                           else; call scqpli(index0)
        end if

        nx = size(x)
        ny = size(y)
        nz = size(z)
        if(nx.ne.ny .or. ny.ne.nz) call msgdmp('M', 'DclDraw3DLine',  &
                                    & 'Length of x, y, z don''t match.')
        n = min(nx, ny, nz)

        call scplzu(n,x,y,z,index0) 
        call sgocls('DclDraw3DLine')
    end subroutine

    subroutine DclDraw3DLineNormalized(x,y,z,index)   !v 座標系で折れ線を描く
      real,    intent(in), dimension(:) :: x, y, z !折れ線を結ぶ点のU座標
      integer, intent(in), optional     :: index
        
        call sgoopn('DclDraw3DLineNormalized', ' ')
        if(present(index)) then; index0 = index
                           else; call scqpli(index0)
        end if

        nx = size(x)
        ny = size(y)
        nz = size(z)
        if(nx.ne.ny .or. ny.ne.nz) call msgdmp('M', 'DclDraw3DLineNormalized',  &
                                    & 'Length of x, y, z don''t match.')
        n = min(nx, ny, nz)

        call scplzv(n,x,y,z,index0) 
        call sgocls('DclDraw3DLineNormalized')
    end subroutine

    subroutine DclSet3DLineIndex(index)  !ラインインデクスの設定．
      integer,   intent(in) :: index  !折れ線のラインインデクス 

      call prcopn('DclSet3DLineIndex')
      call scspli(index)
      call prccls('DclSet3DLineIndex')
    end subroutine
        
    function DclGet3DLineIndex()
      integer  :: DclGet3DLineIndex     !折れ線のラインインデクス 

      call prcopn('DclGet3DLineIndex')
      call scqpli(DclGet3DLineIndex)
      call prccls('DclGet3DLineIndex')
    end function
    
!------------------------------------------------------------------------------
! ポリマーカー
    subroutine DclDraw3DMarker(x,y,z,type,index,height)  !u 座標系でマーカー列を描く
      real,    intent(in), dimension(:) :: x, y, z
      integer, intent(in), optional     :: type, index
      real,    intent(in), optional     :: height

      integer :: type0, index0

        call sgoopn('DclDraw3DMarker', ' ')
        if(present(type))   then; type0 = type
                            else; call scqpmt(type0)
        end if

        if(present(index))  then; index0 = index
                            else; call scqpmi(index0)
        end if

        if(present(height)) then; height0 = height
                            else; call scqpms(height0)
        end if

        nx = size(x)
        ny = size(y)
        nz = size(z)
        if(nx.ne.ny .or. ny.ne.nz) call msgdmp('M', 'DclDraw3DMarker',  &
                                    & 'Length of x, y, z don''t match.')
        n = min(nx, ny, nz)

        call scpmzu(n, x, y, z, type0, index0, height0)
        call sgocls('DclDraw3DMarker')
    end subroutine

    subroutine DclDraw3DMarkerNormalized(x,y,z,type,index,height)
      real,    intent(in), dimension(:) :: x, y, z
      integer, intent(in), optional     :: type, index
      real,    intent(in), optional     :: height

      integer :: type0, index0

        call sgoopn('DclDraw3DMarkerNormalized', ' ')
        if(present(type))   then; type0 = type
                            else; call scqpmt(type0)
        end if

        if(present(index))  then; index0 = index
                            else; call scqpmi(index0)
        end if

        if(present(height)) then; height0 = height
                            else; call scqpms(height0)
        end if

        nx = size(x)
        ny = size(y)
        nz = size(z)
        if(nx.ne.ny .or. ny.ne.nz) call msgdmp('M', 'DclDraw3DMarkerNormalized',  &
                                    & 'Length of x, y, z don''t match.')
        n = min(nx, ny, nz)

        call scpmzv(n, x, y, z, type0, index0, height0)
        call sgocls('DclDraw3DMarkerNormalized')
    end subroutine

    subroutine DclSet3DMarkerType(type)  !マーカータイプの設定．    
      integer,   intent(in) :: type  !マーカータイプ

      call prcopn('DclSet3DMarkerType')
      call scspmt(type)
      call prccls('DclSet3DMarkerType')
    end subroutine

    subroutine DclSet3DMarkerIndex(index)  !マーカーのラインインデクスの設定  
      integer,   intent(in) :: index  !マーカーを描く線のラインインデクス 

      call prcopn('DclSet3DMarkerIndex')
      call scspmi(index)
      call prccls('DclSet3DMarkerIndex')
    end subroutine

    subroutine DclSet3DMarkerSize(height)  !マーカーの大きさ設定．      
      real,      intent(in) :: height  !マーカーの大きさ

      call prcopn('DclSet3DMarkerSize')
      call scspms(height)
      call prccls('DclSet3DMarkerSize')
    end subroutine
                
! パラメタ参照ルーチン
    function DclGet3DMarkerType()
      integer :: DclGet3DMarkerType !マーカータイプ

      call prcopn('DclGet3DMarkerType')
      call scqpmt(DclGet3DMarkerType)
      call prccls('DclGet3DMarkerType')
    end function

    function DclGet3DMarkerIndex()
      integer :: DclGet3DMarkerIndex    !マーカーを描く線のラインインデクス

      call prcopn('DclGet3DMarkerIndex')
      call scqpmi(DclGet3DMarkerIndex)
      call prccls('DclGet3DMarkerIndex')
    end function

    function DclGet3DMarkerSize()  
      real :: DclGet3DMarkerSize    !マーカーの大きさをR-座標系における単位で指定

      call prcopn('DclGet3DMarkerSize')
      call scqpms(DclGet3DMarkerSize)  
      call prccls('DclGet3DMarkerSize')
    end function

!--------------------------------------------------------------------
!トーン
    subroutine DclDraw3DHatch(x,y,z,pattern1,pattern2)
      real,    intent(in), dimension(3) :: x, y, z
      integer, intent(in), optional     :: pattern1, pattern2

        call sgoopn('DclDraw3DHatch', ' ')
        if(present(pattern1)) then; itpat1 = pattern1
                              else; call scqtnp(itpat1,idummy) 
        end if

        if(present(pattern2)) then; itpat2 = pattern2
                              else; call scqtnp(idummy, itpat2) 
        end if

        call sctnzu(x,y,z,itpat1,itpat2) 
        call sgocls('DclDraw3DHatch')
    end subroutine

    subroutine DclDraw3DHatchNormalized(x,y,z,pattern1,pattern2)
      real,    intent(in), dimension(3) :: x, y, z
      integer, intent(in), optional     :: pattern1, pattern2

        call sgoopn('DclDraw3DHatchNormalized', ' ')
        if(present(pattern1)) then; itpat1 = pattern1
                              else; call scqtnp(itpat1,idummy) 
        end if

        if(present(pattern2)) then; itpat2 = pattern2
                              else; call scqtnp(idummy, itpat2) 
        end if

        call sctnzv(x,y,z,itpat1,itpat2) 
        call sgocls('DclDraw3DHatchNormalized')
    end subroutine

    subroutine DclSet3DHatchPattern(pattern1,pattern2)  !トーンパターン番号設定． 
      integer,   intent(in) :: pattern1,pattern2

      call prcopn('DclSet3DHatchPattern')
      call scstnp(pattern1,pattern2) 
      call prccls('DclSet3DHatchPattern')
    end subroutine

    subroutine DclGet3DHatchPattern(pattern1,pattern2)
      integer,   intent(out) :: pattern1,pattern2

      call prcopn('DclGet3DHatchPattern')
      call scqtnp(pattern1,pattern2) 
      call prccls('DclGet3DHatchPattern')
    end subroutine

end module

