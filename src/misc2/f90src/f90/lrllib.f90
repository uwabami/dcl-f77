!-------------------------------------------------
!  LRLlib Module
!-------------------------------------------------
module lrllib
  use dcl_common
  contains
!-------------------------------------------
    function DclEQ(x,y,epsilon)
      real, intent(in)           :: x, y
      real, intent(in), optional :: epsilon
      logical :: DclEQ, lreqa, lreq1

      call prcopn('DclEQ')
      if(present(epsilon)) then
        DclEQ = lreqa(x,y,epsilon)
      else
        DclEQ = lreq1(x,y)
      end if
      call prccls('DclEQ')
      
    end function
!-------------------------------------------
    function DclNE(x,y,epsilon) 
      real, intent(in)           :: x, y
      real, intent(in), optional :: epsilon
      logical :: DclNE, lrnea, lrne1
      
      call prcopn('DclNE')
      if(present(epsilon)) then
        DclNE = lrnea(x,y,epsilon)
      else
        DclNE = lrne1(x,y)
      end if
      call prccls('DclNE')
      
    end function
!-------------------------------------------
    function DclLT(x,y,epsilon)
      real, intent(in)           :: x, y
      real, intent(in), optional :: epsilon
      logical :: DclLT, lrlta, lrlt1
      
      call prcopn('DclLT')
      if(present(epsilon)) then
        DclLT = lrlta(x,y,epsilon)
      else
        DclLT = lrlt1(x,y)
      end if
      call prccls('DclLT')
      
    end function
!-------------------------------------------
    function DclLE(x,y,epsilon) 
      real, intent(in)           :: x, y
      real, intent(in), optional :: epsilon
      logical :: DclLE, lrlea, lrle1
      
      call prcopn('DclLE')
      if(present(epsilon)) then
        DclLE = lrlea(x,y,epsilon)
      else
        DclLE = lrle1(x,y)
      end if
      call prccls('DclLE')
      
    end function
!-------------------------------------------
    function DclGT(x,y,epsilon) 
      real, intent(in)           :: x, y
      real, intent(in), optional :: epsilon
      logical :: DclGT, lrgta, lrgt1
      
      call prcopn('DclGT')
      if(present(epsilon)) then
        DclGT = lrgta(x,y,epsilon)
      else
        DclGT = lrgt1(x,y)
      end if
      call prccls('DclGT')
      
    end function
!-------------------------------------------
    function DclGE(x,y,epsilon)
      real, intent(in)           :: x, y
      real, intent(in), optional :: epsilon
      logical :: DclGE, lrgea, lrge1
      
      call prcopn('DclGE')
      if(present(epsilon)) then
        DclGE = lrgea(x,y,epsilon)
      else
        DclGE = lrge1(x,y)
      end if
      call prccls('DclGE')
      
    end function
end module
