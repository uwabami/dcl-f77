!-------------------------------------------------
!  RFAlib Module
!-------------------------------------------------
module rfalib
  use dcl_common
  contains

    function DclGetAVE(x)                        !平均を求める
      real, intent(in), dimension(:) :: x
      real                           :: DclGetAVE

      call prcopn('DclGetAVE')
      DclGetAVE = rave(x,size(x),1)
      call prccls('DclGetAVE')
    end function
      
    function DclGetVAR(x)                        !分散を求める
      real, intent(in), dimension(:) :: x
      real                           :: DclGetVAR

      call prcopn('DclGetVAR')
      DclGetVAR = rvar(x,size(x),1)
      call prccls('DclGetVAR')
    end function
      
    function DclGetSTD(x)                        !標準偏差を求める
      real, intent(in), dimension(:) :: x
      real                           :: DclGetSTD

      call prcopn('DclGetSTD')
      DclGetSTD = rstd(x,size(x),1)
      call prccls('DclGetSTD')
    end function
      
    function DclGetRMS(x)                        !root mean squareを求める
      real, intent(in), dimension(:) :: x
      real                           :: DclGetRMS

      call prcopn('DclGetRMS')
      DclGetRMS = rrms(x,size(x),1)
      call prccls('DclGetRMS')
    end function
      
    function DclGetAMP(x)                        !大きさ (（Σ x^2)^1/2 ) を求める
      real, intent(in), dimension(:) :: x
      real                           :: DclGetAMP

      call prcopn('DclGetAMP')
      DclGetAMP = ramp(x,size(x),1)
      call prccls('DclGetAMP')
    end function
end module
