!-------------------------------------------------
!  SGpack Module
!-------------------------------------------------
module sgpack
  use dcl_common

  interface DclDrawLine
    module procedure DclDrawLine1, DclDrawLine2
  end interface  

  interface DclDrawLineNormalized
    module procedure DclDrawLineNormalized1, DclDrawLineNormalized2
  end interface  

  interface DclDrawLineProjected
    module procedure DclDrawLineProjected1, DclDrawLineProjected2
  end interface  

  private :: DclDrawLine1,           DclDrawLine2
  private :: DclDrawLineNormalized1, DclDrawLineNormalized2
  private :: DclDrawLineProjected1,  DclDrawLineProjected2

  contains
!---------------------------------------------------------
    subroutine DclPrintDeviceList() !ワークステーション名のリスト
      call prcopn('DclPrintDeviceList')
      call sgpwsn()
      call prccls('DclPrintDeviceList')
    end subroutine
!------------------------------------------------------------------------
!正規化変換
!---------------------------------
    subroutine DclTransShortToLong(short,long) !略称から名称を求める．
      character(len=*), intent(in)  :: short 
      character(len=*), intent(out) :: long

      call prcopn('DclTransShortToLong')
      call sgtrsl(short,long) 
      call prccls('DclTransShortToLong')
    end subroutine
!---------------------------------
    subroutine DclTransShortToNum(short,num) !略称から変換関数番号を求める．
      character(len=*), intent(in)  :: short 
      integer,          intent(out) :: num

      call prcopn('DclTransShortToNum')
      call sgtrsn(short,num)
      call prccls('DclTransShortToNum')
    end subroutine
!---------------------------------
    subroutine DclTransLongToShort(long,short) !名称から略称を求める．
      character(len=*), intent(in)  :: long
      character(len=*), intent(out) :: short 

      call prcopn('DclTransLongToShort')
      call sgtrls(long,short)
      call prccls('DclTransLongToShort')
    end subroutine
!---------------------------------
    subroutine DclTransLongToNum(long,num) !名称から変換関数番号を求める．
      character(len=*), intent(in)  :: long
      integer,          intent(out) :: num

      call prcopn('DclTransLongToNum')
      call sgtrln(long,num)
      call prccls('DclTransLongToNum')
    end subroutine
!---------------------------------
    subroutine DclTransNumToShort(num,short) !変換関数番号から略称を求める．
      integer,          intent(in)  :: num
      character(len=*), intent(out) :: short

      call prcopn('DclTransNumToShort')
      call sgtrns(num,short)
      call prccls('DclTransNumToShort')
    end subroutine
!---------------------------------
    subroutine DclTransNumToLong(num,long) !変換関数番号から名称を求める．
      integer,          intent(in) :: num
      character(len=*), intent(out) :: long

      call prcopn('DclTransNumToLong')
      call sgtrnl(num,long)
      call prccls('DclTransNumToLong')
    end subroutine
!---------------------------------
    subroutine DclGetViewPort(xmin, xmax, ymin, ymax) 
      real, intent(out), optional :: xmin, xmax, ymin, ymax !ビューポート

      call prcopn('DclGetViewPort')
      call sgqvpt(xmin0, xmax0, ymin0, ymax0) 
      if(present(xmin)) xmin = xmin0
      if(present(xmax)) xmax = xmax0
      if(present(ymin)) ymin = ymin0
      if(present(ymax)) ymax = ymax0
      call prccls('DclGetViewPort')
    end subroutine
!---------------------------------
    subroutine DclGetWindow(xmin, xmax, ymin, ymax) 
      real, intent(out), optional :: xmin, xmax, ymin, ymax !ウインドウ

      call prcopn('DclGetWindow')
      call sgqwnd(xmin0, xmax0, ymin0, ymax0) 
      if(present(xmin)) xmin = xmin0
      if(present(xmax)) xmax = xmax0
      if(present(ymin)) ymin = ymin0
      if(present(ymax)) ymax = ymax0
      call prccls('DclGetWindow')
    end subroutine
!---------------------------------
    subroutine DclGetSimilarity(factor, xoffset, yoffset)  
      real, intent(out), optional :: factor, xoffset, yoffset

      call prcopn('DclGetSimilarity')
      call sgqsim(factor0, xoff0, yoff0) 
      if(present(factor))  factor  = factor0 
      if(present(xoffset)) xoffset = xoff0 
      if(present(yoffset)) yoffset = yoff0 
      call prccls('DclGetSimilarity')
    end subroutine
!---------------------------------
    subroutine DclGetMapProjectionAngle(longitude,latitude,rotation)  
      real, intent(out), optional :: longitude,latitude,rotation 
      real                        :: lon, lat, rot

      call prcopn('DclGetMapProjectionAngle')
      call sgqmpl(lon, lat, rot)
      if(present(longitude)) longitude = lon
      if(present(latitude )) latitude  = lat
      if(present(rotation )) rotation  = rot
      call prccls('DclGetMapProjectionAngle')
    end subroutine
!---------------------------------
    function DclGetTransNumber() 
      integer :: DclGetTransNumber     !変換関数番号

      call prcopn('DclGetTransNumber')
      call sgqtrn(DclGetTransNumber) 
      call prccls('DclGetTransNumber')
    end function
!------------------------------------------------------------------------
!ポリラインプリミティブ
!---------------------------------
    subroutine DclDrawLine1 (x,y,type,index)  !折れ線を描く．
      real,      intent(in), dimension(:) :: x, y
      integer,   intent(in), optional     :: type, index
      integer                             :: type0, index0
      
        call sgoopn('DclDrawLine', ' ')
        if(present(type))  then ; type0 = type
                           else ; call sgqplt(type0)
        end if
        
        if(present(index)) then ; index0 = index
                           else ; call sgqpli(index0)
        end if

        nx = size(x)
        ny = size(y)
        if(nx.ne.ny) call msgdmp('M', 'DclDrawLine', 'Length of x and y don''t match.')
        n = min(nx, ny)
        call sgplzu(n,x,y,type0,index0)
        call sgocls('DclDrawLine')
        
    end subroutine

!---------------------------------
    subroutine DclDrawLine2 (x1,y1,x2,y2,type,index)  !折れ線を描く．
      real,      intent(in)               :: x1, y1, x2, y2
      integer,   intent(in), optional     :: type, index
      integer                             :: type0, index0
      real,      dimension(2)             :: x, y
      
        call sgoopn('DclDrawLine', ' ')
        if(present(type))  then ; type0 = type
                           else ; call sgqplt(type0)
        end if
        
        if(present(index)) then ; index0 = index
                           else ; call sgqpli(index0)
        end if

        x(1) = x1 ; y(1) = y1
        x(2) = x2 ; y(2) = y2

        call sgplzu(2,x,y,type0,index0)
        call sgocls('DclDrawLine')
        
    end subroutine

!---------------------------------
    subroutine DclDrawLineNormalized1(x, y, type, index)
      real,      intent(in), dimension(:) :: x, y
      integer,   intent(in), optional     :: type, index
      integer                             :: type0, index0
      
        call sgoopn('DclDrawLineNormalized', ' ')
        if(present(type))  then ; type0 = type
                           else ; call sgqplt(type0)
        end if
        
        if(present(index)) then ; index0 = index
                           else ; call sgqpli(index0)
        end if

        nx = size(x)
        ny = size(y)
        if(nx.ne.ny) call msgdmp('M', 'DclDrawLineNormalized', &
                           & 'Length of x and y don''t match.')
        n = min(nx, ny)

        call sgplzv(n, x, y, type0, index0)
        call sgocls('DclDrawLineNormalized')
    end subroutine
!---------------------------------
    subroutine DclDrawLineNormalized2(x1, y1, x2, y2, type, index)
      real,      intent(in)            :: x1, y1, x2, y2
      integer,   intent(in), optional  :: type, index
      integer                          :: type0, index0
      real,      dimension(2)          :: x, y
      
        call sgoopn('DclDrawLineNormalized', ' ')
        if(present(type))  then ; type0 = type
                           else ; call sgqplt(type0)
        end if
        
        if(present(index)) then ; index0 = index
                           else ; call sgqpli(index0)
        end if

        x(1) = x1 ; y(1) = y1
        x(2) = x2 ; y(2) = y2

        call sgplzv(2, x, y, type0, index0)
        call sgocls('DclDrawLineNormalized')
    end subroutine
!---------------------------------
    subroutine DclDrawLineProjected1(x,y,type,index)
      real,      intent(in), dimension(:) :: x, y
      integer,   intent(in), optional     :: type, index
      integer                             :: type0, index0
      
        call sgoopn('DclDrawLineProjected', ' ')
        if(present(type))  then ; type0 = type
                           else ; call sgqplt(type0)
        end if
        
        if(present(index)) then ; index0 = index
                           else ; call sgqpli(index0)
        end if

        nx = size(x)
        ny = size(y)
        if(nx.ne.ny) call msgdmp('M', 'DclDrawLineProjected', &
                           & 'Length of x and y don''t match.')
        n = min(nx, ny)

        call sgplzr(n, x, y, type0, index0)
        call sgocls('DclDrawLineProjected')
    end subroutine
!---------------------------------
    subroutine DclDrawLineProjected2(x1,y1,x2,y2,type,index)
      real,    intent(in)           :: x1, y1, x2, y2
      integer, intent(in), optional :: type, index
      integer                       :: type0, index0
      real,    dimension(2)         :: x, y
      
        call sgoopn('DclDrawLineProjected', ' ')
        if(present(type))  then ; type0 = type
                           else ; call sgqplt(type0)
        end if
        
        if(present(index)) then ; index0 = index
                           else ; call sgqpli(index0)
        end if

        x(1) = x1 ; y(1) = y1
        x(2) = x2 ; y(2) = y2

        call sgplzr(2, x, y, type0, index0)
        call sgocls('DclDrawLineProjected')
    end subroutine
!---------------------------------
    subroutine DclSetLineType(type)  !ラインタイプを設定する．   
      integer, intent(in) :: type    !線種

      call prcopn('DclSetLineType')
      call sgsplt(type)
      call prccls('DclSetLineType')
    end subroutine
!---------------------------------
    subroutine DclSetLineIndex(index)  !ラインインデクスの設定．   
      integer,   intent(in) :: index    !ラインインデクス 

      call prcopn('DclSetLineIndex')
      call sgspli(index)
      call prccls('DclSetLineIndex')
    end subroutine
!---------------------------------
    subroutine DclSetLineText(text)  !ラベルの文字列設定．  
      character(len=*), intent(in) :: text  !描く文字列（初期値は'A')

      call prcopn('DclSetLineText')
      call sgsplc(text)
      call prccls('DclSetLineText')
    end subroutine
!---------------------------------
    subroutine DclSetLineTextSize(height)  !ラベルの文字高設定  
      real, intent(in) :: height  !文字列の高さ

      call prcopn('DclSetLineTextSize')
      call sgspls(height)
      call prccls('DclSetLineTextSize')
    end subroutine
!---------------------------------
    subroutine DclNextLineText()  !ラベルの最後の文字番号を増やす．

      call prcopn('DclNextLineText')
      call sgnplc()
      call prccls('DclNextLineText')
    end subroutine
!---------------------------------
    function DclGetLineType()  
      integer :: DclGetLineType 

      call prcopn('DclGetLineType')
      call sgqplt(DclGetLineType)
      call prccls('DclGetLineType')
    end function
!---------------------------------
    function DclGetLineIndex() 
      integer :: DclGetLineIndex

      call prcopn('DclGetLineIndex')
      call sgqpli(DclGetLineIndex) 
      call prccls('DclGetLineIndex')
    end function
!---------------------------------
    subroutine DclGetLineText(text)   
      character(len=*), intent(out) :: text !描く文字列（初期値は'A')

      call prcopn('DclGetLineText')
      call sgqplc(text) 
      call prccls('DclGetLineText')
    end subroutine
!---------------------------------
    function DclGetLineTextSize()   
      real :: DclGetLineTextSize

      call prcopn('DclGetLineTextSize')
      call sgqpls(DclGetLineTextSize)   
      call prccls('DclGetLineTextSize')
    end function
!------------------------------------------------------------------------------
! ポリマーカープリミティブ
!---------------------------------
    subroutine DclDrawMarker(x,y,type,index,height)
      real,      intent(in), dimension(:) :: x, y
      integer,   intent(in), optional     :: type, index
      real,      intent(in), optional     :: height

      integer                             :: type0, index0
      real                                :: height0
        
        call sgoopn('DclDrawMarker', ' ')
        nx = size(x)
        ny = size(y)
        if(nx.ne.ny) call msgdmp('M', 'DclDrawMarker', 'Length of x and y don''t match.')
        n = min(nx, ny)

        if(present(type)  ) then ; type0 = type
                            else ; call sgqpmt(type0)
        end if
        
        if(present(index) ) then ; index0 = index
                            else ; call sgqpmi(index0)
        end if
        
        if(present(height)) then ; height0 = height
                            else ; call sgqpms(height0)
        end if
        
        call sgpmzu(n,x,y,type0,index0, height0)
        call sgocls('DclDrawMarker')
    end subroutine
!---------------------------------
    subroutine DclDrawMarkerNormalized(x, y, type, index, height) 

      real,      intent(in), dimension(:) :: x, y
      integer,   intent(in), optional     :: type, index
      real,      intent(in), optional     :: height

      integer                             :: type0, index0
      real                                :: height0
        
        call sgoopn('DclDrawMarkerNormalized', ' ')
        nx = size(x)
        ny = size(y)
        if(nx.ne.ny) call msgdmp('M', 'DclDrawMarkerNormalized', &
                               & 'Length of x and y don''t match.')
        n = min(nx, ny)

        if(present(type)  ) then ; type0 = type
                            else ; call sgqpmt(type0)
        end if
        
        if(present(index) ) then ; index0 = index
                            else ; call sgqpmi(index0)
        end if
        
        if(present(height)) then ; height0 = height
                            else ; call sgqpms(height0)
        end if
        
        call sgpmzv(n,x,y,type0,index0, height0)
        call sgocls('DclDrawMarkerNormalized')

    end subroutine
!---------------------------------
    subroutine DclDrawMarkerProjected(x, y, type, index, height)

      real,      intent(in), dimension(:) :: x, y
      integer,   intent(in), optional     :: type, index
      real,      intent(in), optional     :: height

      integer                             :: type0, index0
      real                                :: height0
        
        call sgoopn('DclDrawMarkerProjected', ' ')
        nx = size(x)
        ny = size(y)
        if(nx.ne.ny) call msgdmp('M', 'DclDrawMarkerProjected', &
                               & 'Length of x and y don''t match.')
        n = min(nx, ny)

        if(present(type)  ) then ; type0 = type
                            else ; call sgqpmt(type0)
        end if
        
        if(present(index) ) then ; index0 = index
                            else ; call sgqpmi(index0)
        end if
        
        if(present(height)) then ; height0 = height
                            else ; call sgqpms(height0)
        end if
        
        call sgpmzr(n,x,y,type0,index0, height0)
        call sgocls('DclDrawMarkerProjected')

    end subroutine
!---------------------------------
    subroutine DclSetMarkerType(type)  !マーカータイプの設定．    
      integer, intent(in) :: type    !マーカータイプ

      call prcopn('DclSetMarkerType')
      call sgspmt(type)
      call prccls('DclSetMarkerType')
    end subroutine
!---------------------------------
    subroutine DclSetMarkerIndex(index)  !マーカーのラインインデクスの設定  
      integer,   intent(in) :: index     !ラインインデクス 

      call prcopn('DclSetMarkerIndex')
      call sgspmi(index)
      call prccls('DclSetMarkerIndex')
    end subroutine
!---------------------------------
    subroutine DclSetMarkerSize(height)   !マーカーの大きさ設定．     
      real,    intent(in) :: height    !マーカーの大きさ

      call prcopn('DclSetMarkerSize')
      call sgspms(height) 
      call prccls('DclSetMarkerSize')
    end subroutine
!---------------------------------
    function DclGetMarkerType()  
      integer :: DclGetMarkerType     !マーカータイプ

      call prcopn('DclGetMarkerType')
      call sgqpmt(DclGetMarkerType) 
      call prccls('DclGetMarkerType')
    end function
!---------------------------------
    function DclGetMarkerIndex()
      integer :: DclGetMarkerIndex     !ラインインデクス 

      call prcopn('DclGetMarkerIndex')
      call sgqpmi(DclGetMarkerIndex)
      call prccls('DclGetMarkerIndex')
    end function
!---------------------------------
    function DclGetMarkerSize()
      real :: DclGetMarkerSize     !マーカーの大きさ

      call prcopn('DclGetMarkerSize')
      call sgqpms(DclGetMarkerSize)
      call prccls('DclGetMarkerSize')
    end function
!------------------------------------------------------------------
!     テキスト
!---------------------------------
    subroutine DclDrawText(x, y, text, height, angle, centering, index) 
      real,             intent(in) :: x, y          !文字の位置
      character(len=*), intent(in) :: text          !文字列
      real,             intent(in) :: height, angle !文字の高さ
      integer,          intent(in) :: centering,index

      optional :: height, angle, centering,index

      real    :: height0
      integer :: angle0, cent0, index0

        call sgoopn('DclDrawText', ' ')
        if(present(height))    then ; height0 = height
                               else ; call sgqtxs(height0)
        end if

        if(present(angle) )    then ; angle0 = nint(angle)
                               else ; call sgqtxr(angle0)
        end if

        if(present(centering)) then ; cent0 = centering
                               else ; call sgqtxc(cent0)
        end if
        
        if(present(index) )    then ; index0 = index
                               else ; call sgqtxi(index0)
        end if

        call sgtxzu(x,y,text,height0,angle0,cent0,index0) 
        call sgocls('DclDrawText')
    end subroutine
!---------------------------------
    subroutine DclDrawTextNormalized(x, y, text, height, angle, centering, index)

      real,             intent(in) :: x, y          !文字の位置
      character(len=*), intent(in) :: text          !文字列
      real,             intent(in) :: height, angle !文字の高さ
      integer,          intent(in) :: centering,index

      optional :: height, angle, centering,index

      real    :: height0
      integer :: angle0, cent0, index0

        call sgoopn('DclDrawTextNormalized', ' ')
        if(present(height))    then ; height0 = height
                               else ; call sgqtxs(height0)
        end if

        if(present(angle) )    then ; angle0 = nint(angle)
                               else ; call sgqtxr(angle0)
        end if

        if(present(centering)) then ; cent0 = centering
                               else ; call sgqtxc(cent0)
        end if
        
        if(present(index) )    then ; index0 = index
                               else ; call sgqtxi(index0)
        end if

        call sgtxzv(x,y,text,height0,angle0,cent0,index0) 
        call sgocls('DclDrawTextNormalized')
    end subroutine
!---------------------------------
    subroutine DclDrawTextProjected(x, y, text, height, angle, centering, index)

      real,             intent(in) :: x, y          !文字の位置
      character(len=*), intent(in) :: text          !文字列
      real,             intent(in) :: height, angle !文字の高さ
      integer,          intent(in) :: centering,index

      optional :: height, angle, centering,index

      real    :: height0 
      integer :: angle0, cent0, index0

        call sgoopn('DclDrawTextProjected', ' ')
        if(present(height))    then ; height0 = height
                               else ; call sgqtxs(height0)
        end if

        if(present(angle) )    then ; angle0 = nint(angle)
                               else ; call sgqtxr(angle0)
        end if

        if(present(centering)) then ; cent0 = centering
                               else ; call sgqtxc(cent0)
        end if
        
        if(present(index) )    then ; index0 = index
                               else ; call sgqtxi(index0)
        end if

        call sgtxzr(x,y,text,height0,angle0,cent0,index0) 
        call sgocls('DclDrawTextProjected')
    end subroutine
!---------------------------------
    subroutine DclSetTextHeight(height)  !文字の高さ設定．  
      real,  intent(in) :: height  !文字の高さ

      call prcopn('DclSetTextHeight')
      call sgstxs(height)
      call prccls('DclSetTextHeight')
    end subroutine
!---------------------------------
    subroutine DclSetTextAngle(angle)  !文字列の角度の設定．  
      real,  intent(in) :: angle   !文字列の傾きを度の単位で与える 

      call prcopn('DclSetTextAngle')
      call sgstxr(nint(angle))
      call prccls('DclSetTextAngle')
    end subroutine
!---------------------------------
    subroutine DclSetTextIndex(index)  !文字列のラインインデクスの設定．                 
      integer,   intent(in) :: index     !ラインインデクス 

      call prcopn('DclSetTextIndex')
      call sgstxi(index) 
      call prccls('DclSetTextIndex')
    end subroutine
!---------------------------------
    subroutine DclSetTextPosition(centering)  !文字列のセンタリングオプション設定
      integer,   intent(in) :: centering   !センタリングオプション

      call prcopn('DclSetTextPosition')
      call sgstxc(centering)
      call prccls('DclSetTextPosition')
    end subroutine
!---------------------------------
    function DclGetTextHeight() 
      real :: DclGetTextHeight  !文字の高さ

      call prcopn('DclGetTextHeight')
      call sgqtxs(DclGetTextHeight) 
      call prccls('DclGetTextHeight')
    end function
!---------------------------------
    function DclGetTextAngle()
      real :: DclGetTextAngle      !文字列の傾きを度の単位で与える

      call prcopn('DclGetTextAngle')
      call sgqtxr(irot)
      DclGetTextAngle = irot
      call prccls('DclGetTextAngle')
    end function
!---------------------------------
    function DclGetTextIndex() 
      integer :: DclGetTextIndex      !ラインインデクス

      call prcopn('DclGetTextIndex')
      call sgqtxi(DclGetTextIndex) 
      call prccls('DclGetTextIndex')
    end function
!---------------------------------
    function DclGetTextPosition()  
      integer :: DclGetTextPosition   !センタリングオプション

      call prcopn('DclGetTextPosition')
      call sgqtxc(DclGetTextPosition)  
      call prccls('DclGetTextPosition')
    end function
!-------------------------------------------------------------
! トーンプリミティブ
!---------------------------------
    subroutine DclShadeRegion(x, y, pattern)  !u 座標系で多角形領域の塗りつぶし．
      real,    intent(in), dimension(:) :: x,y 
      integer, intent(in), optional     :: pattern

        call sgoopn('DclShadeRegion', ' ')
        nx = size(x)
        ny = size(y)
        if(nx.ne.ny) call msgdmp('M', 'DclShadeRegion', &
                               & 'Length of x and y don''t match.')
        n = min(nx, ny)

        if(present(pattern)) then ; ipat0 = pattern
                             else ; call sgqtnp(ipat0)
        end if

        call sgtnzu(n,x,y,ipat0) 
        call sgocls('DclShadeRegion')
    end subroutine
!---------------------------------
    subroutine DclShadeRegionNormalized(x, y, pattern)
      real,    intent(in), dimension(:) :: x,y 
      integer, intent(in), optional     :: pattern

        call sgoopn('DclShadeRegionNormalized', ' ')
        nx = size(x)
        ny = size(y)
        if(nx.ne.ny) call msgdmp('M', 'DclShadeRegionNormalized', &
                               & 'Length of x and y don''t match.')
        n = min(nx, ny)

        if(present(pattern)) then ; ipat0 = pattern
                             else ; call sgqtnp(ipat0)
        end if

        call sgtnzv(n,x,y,ipat0) 
        call sgocls('DclShadeRegionNormalized')
    end subroutine
!---------------------------------
    subroutine DclShadeRegionProjected(x, y, pattern)
      real,    intent(in), dimension(:) :: x,y 
      integer, intent(in), optional     :: pattern

        call sgoopn('DclShadeRegionProjected', ' ')
        nx = size(x)
        ny = size(y)
        if(nx.ne.ny) call msgdmp('M', 'DclShadeRegionProjected', &
                               & 'Length of x and y don''t match.')
        n = min(nx, ny)

        if(present(pattern)) then ; ipat0 = pattern
                             else ; call sgqtnp(ipat0)
        end if

        call sgtnzr(n,x,y,ipat0) 
        call sgocls('DclShadeRegionProjected')
    end subroutine
!---------------------------------
    subroutine DclSetShadePattern(pattern) ! トーンパターン番号設定
      integer,   intent(in) :: pattern  !トーンパターン番号

      call prcopn('DclSetShadePattern')
      call sgstnp(pattern) 
      call prccls('DclSetShadePattern')
    end subroutine
!---------------------------------
    function DclGetShadePattern() !現在設定されているトーンパターン番号参照
      integer :: DclGetShadePattern     !現在設定されているトーンパターン番号

      call prcopn('DclGetShadePattern')
      call sgqtnp(DclGetShadePattern)
      call prccls('DclGetShadePattern')
    end function
!-------------------------------------------------------------------------
!     アローサブプリミティブ
!-------------------------------------------------------------------------
    subroutine DclDrawArrow(x1,y1,x2,y2,type,index)
      real,    intent(in)           :: x1, y1, x2, y2
      integer, intent(in), optional :: type, index

      integer :: type0

        call sgoopn('DclDrawArrow', ' ')
        if(present(type)  ) then ; type0 = type
                            else ; call sgqlat(type0)
        end if
        
        if(present(index) ) then ; index0 = index
                            else ; call sgqlai(index0)
        end if
        
        call sglazu(x1,y1,x2,y2,type0,index0)
        call sgocls('DclDrawArrow')
    end subroutine
!---------------------------------
    subroutine DclDrawArrowNormalized(x1,y1,x2,y2,type,index)
      real,    intent(in)           :: x1, y1, x2, y2
      integer, intent(in), optional :: type, index

      integer :: type0

        call sgoopn('DclDrawArrowNormalized', ' ')
        if(present(type)  ) then ; type0 = type
                            else ; call sgqlat(type0)
        end if
        
        if(present(index) ) then ; index0 = index
                            else ; call sgqlai(index0)
        end if
        
        call sglazv(x1,y1,x2,y2,type0,index0)
        call sgocls('DclDrawArrowNormalized')
    end subroutine
!---------------------------------
    subroutine DclDrawArrowProjected(x1,y1,x2,y2,type,index)
      real,    intent(in)           :: x1, y1, x2, y2
      integer, intent(in), optional :: type, index

      integer :: type0

        call sgoopn('DclDrawArrowProjected', ' ')
        if(present(type)  ) then ; type0 = type
                            else ; call sgqlat(type0)
        end if
        
        if(present(index) ) then ; index0 = index
                            else ; call sgqlai(index0)
        end if
        
        call sglazr(x1,y1,x2,y2,type0,index0)
        call sgocls('DclDrawArrowProjected')
    end subroutine
!---------------------------------
    subroutine DclSetArrowLineType(type)  !描く線分のラインタイプを設定する． 
      integer,   intent(in) :: type  !線分のラインタイプ 

      call prcopn('DclSetArrowLineType')
      call sgslat(type)
      call prccls('DclSetArrowLineType')
    end subroutine
!---------------------------------
    subroutine DclSetArrowLineIndex(index) !描く線分の ラインインデクスを設定する
      integer,   intent(in) :: index  !線分のラインインデクス 

      call prcopn('DclSetArrowLineIndex')
      call sgslai(index)
      call prccls('DclSetArrowLineIndex')
    end subroutine
!---------------------------------
    function DclGetArrowLineType() !現在設定されているラインタイプ
      integer :: DclGetArrowLineType   !線分のラインタイプ

      call prcopn('DclGetArrowLineType')
      call sgqlat(DclGetArrowLineType)
      call prccls('DclGetArrowLineType')
    end function
!---------------------------------
    function DclGetArrowLineIndex() !現在設定されているラインインデクス
      integer :: DclGetArrowLineIndex      !線分のラインインデクス 

      call prcopn('DclGetArrowLineIndex')
      call sgqlai(DclGetArrowLineIndex) 
      call prccls('DclGetArrowLineIndex')
    end function
!---------------------------------
end module

