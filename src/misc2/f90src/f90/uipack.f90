!----------------------------------------------
!  UIpack Module
!----------------------------------------------

module uipack
!  use dcl_common
  contains
!-------------------------------------------------------------
    subroutine DclPaintData(z)
      real, intent(in), dimension(:,:) :: z
      integer, allocatable, dimension(:)   :: image

      call prcopn('DclPaintGrid')

      nx = size(z,1)
      ny = size(z,2)
      call sgqvpt(vxmin, vxmax, vymin, vymax)

      call stfpr2(vxmin, vymin, rx, ry)
      call stfwtr(rx, ry, wx, wy)
      call swfint(wx, wy, ix1, iy1)

      call stfpr2(vxmax, vymax, rx, ry)
      call stfwtr(rx, ry, wx, wy)
      call swfint(wx, wy, ix2, iy2)

      iwidth = abs(ix1-ix2)+1
      allocate(image(iwidth))

      call uipdaz(z, nx, nx, ny,image,iwidth)				
      deallocate(image)  

      call prccls('DclPaintGrid')
    end subroutine

!-------------------------------------------------------------
    subroutine DclSetColorRange(zmin, zmax)
      real, intent(in) :: zmin, zmax

      call prcopn('DclSetColorRange')
      call uiscrg(zmin, zmax)
      call prccls('DclSetColorRange')
    end subroutine

!-------------------------------------------------------------
    subroutine DclSetColorSequence(color_level, irgb)
      real,    intent(in), dimension(:) :: color_level
      integer, intent(in), dimension(:) :: irgb

      n = size(color_level,1)
      call prcopn('DclSetColorSequence')
      call uiscsq(color_level, irgb, n)
      call prccls('DclSetColorSequence')
    end subroutine

!-------------------------------------------------------------
    subroutine DclSetColorFile(file_name)
      character (len=*), intent(in) :: file_name

      call prcopn('DclSetColorFile')
      call uiscfl(file_name)
      call prccls('DclSetColorFile')
    end subroutine

!-------------------------------------------------------------
    subroutine DclSetMaskRange(zmin, zmax)
      real, intent(in) :: zmin, zmax

      call prcopn('DclSetMaskRange')
      call uismrg(zmin, zmax)
      call prccls('DclSetMaskRange')
    end subroutine

!-------------------------------------------------------------
    subroutine DclSetMaskSequence(mask_level, mask)
      real,    intent(in), dimension(:) :: mask_level, mask

      n = size(mask_level,1)
      call prcopn('DclSetMaskSequence')
      call uismsq(mask_level, mask, n)
      call prccls('DclSetMaskSequence')
    end subroutine

!-------------------------------------------------------------
    subroutine DclSetMaskFile(file_name)
      character (len=*), intent(in) :: file_name

      call prcopn('DclSetMaskFile')
      call uismfl(file_name)
      call prccls('DclSetMaskFile')
    end subroutine

!-------------------------------------------------------------
    subroutine DclPaintColorBarX(xmin,xmax,ymin,ymax,zmin,zmax,cpos)
      real, intent(in) :: xmin, xmax, ymin, ymax, zmin, zmax
      character(len=*), intent(in), optional :: cpos
      character(len=2)                       :: cpos0

      if(present(cpos))  then
        cpos0 = cpos
      else
        cpos0 = 'TB'
      end if

      call prcopn('DclPaintColorBarX')
      call uixbar(xmin,xmax,ymin,ymax,zmin,zmax,cpos0)
      call prccls('DclPaintColorBarX')
    end subroutine

!-------------------------------------------------------------
    subroutine DclPaintColorBarY(xmin,xmax,ymin,ymax,zmin,zmax,cpos)
      real, intent(in) :: xmin, xmax, ymin, ymax, zmin, zmax
      character(len=*), intent(in), optional :: cpos
      character(len=2) :: cpos0

      if(present(cpos))  then
        cpos0 = cpos
      else
        cpos0 = 'LR'
      end if

      call prcopn('DclPaintColorBarY')
      call uiybar(xmin,xmax,ymin,ymax,zmin,zmax,cpos0)
      call prccls('DclPaintColorBarY')
    end subroutine

!=============================================================
    subroutine DclPaintData2(u, v)
      real, intent(in), dimension(:, :) :: u, v
      integer, allocatable, dimension(:)   :: image

      call prcopn('DclPaintGrid2')

      nu1 = size(u,1)
      nu2 = size(u,2)
      nv1 = size(v,1)
      nv2 = size(v,2)

      if(nu1 /= nv1 .or. nu2 /= nv2) then
        call msgdmp('E', 'DclPaintGrid2', 'Size of arrays are not consistent.')
      end if

      call sgqvpt(vxmin, vxmax, vymin, vymax)

      call stfpr2(vxmin, vymin, rx, ry)
      call stfwtr(rx, ry, wx, wy)
      call swfint(wx, wy, ix1, iy1)

      call stfpr2(vxmax, vymax, rx, ry)
      call stfwtr(rx, ry, wx, wy)
      call swfint(wx, wy, ix2, iy2)

      iwidth = abs(ix1-ix2)+1
      allocate(image(iwidth))

      call uipd2z(u, v, nu1, nu1, nu2, image, iwidth)
      deallocate(image)  

      call prccls('DclPaintGrid2')
    end subroutine

!-------------------------------------------------------------
    subroutine DclSetColorRange2(umin, umax, vmin, vmax)
      real, intent(in) :: umin, umax, vmin, vmax

      call prcopn('DclSetColorRange2')
      call uiscr2(umin, umax, vmin, vmax)
      call prccls('DclSetColorRange2')
    end subroutine

!-------------------------------------------------------------
    subroutine DclSetColorMap(irgb1, irgb2, irgb3, irgb4)
      integer, intent(in) :: irgb1, irgb2, irgb3, irgb4

      call prcopn('DclSetColorMap')
      call uiscmp(irgb1, irgb2, irgb3, irgb4)
      call prccls('DclSetColorMap')
    end subroutine

!-------------------------------------------------------------
    subroutine DclPaintColorMap(xmin,xmax,ymin,ymax,cpos)
      real, intent(in) :: xmin, xmax, ymin, ymax
      character(len=*), intent(in), optional :: cpos
      character(len=4) :: cpos0

      if(present(cpos))  then
        cpos0 = cpos
      else
        cpos0 = 'TBLR'
      end if

      call prcopn('DclPaintColorMap')
      call uipcmp(xmin,xmax,ymin,ymax,cpos0)
      call prccls('DclPaintColorMap')
    end subroutine

!=============================================================
    subroutine DclPackColor(ir, ig, ib, irgb)
      integer, intent(in)  :: ir, ig, ib
      integer, intent(out) :: irgb

      call prcopn('DclPackColor')
      call uifpac(ir, ig, ib, irgb)
      call prccls('DclPackColor')
    end subroutine

!-------------------------------------------------------------
    subroutine DclUnPackColor(irgb, ir, ig, ib)
      integer, intent(in)  :: irgb
      integer, intent(out) :: ir, ig, ib

      call prcopn('DclUnPackColor')
      call uiipac(irgb, ir, ig, ib)
      call prccls('DclUnPackColor')
    end subroutine

end module
