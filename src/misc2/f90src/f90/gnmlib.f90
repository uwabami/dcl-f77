!-------------------------------------------------
!  GNMlib Module
!-------------------------------------------------
module gnmlib
  use dcl_common
  contains
!-----------------------------------------------------------------
    subroutine DclGoodNumExLT(value,mantissa,exponent)  
      real,      intent(in)  :: value                   !調べる実数値
      real,      intent(out) :: mantissa                !きりのよい数の仮数部
      integer,   intent(out) :: exponent                !きりのよい数の指数部

      call prcopn('DclGoodNumExLT')
      call gnlt(value,mantissa,exponent)
      call prccls('DclGoodNumExLT')
    end subroutine
      
    subroutine DclGoodNumExGT(value,mantissa,exponent) 
      real,      intent(in)  :: value                   !調べる実数値
      real,      intent(out) :: mantissa                !きりのよい数の仮数部
      integer,   intent(out) :: exponent                !きりのよい数の指数部

      call prcopn('DclGoodNumExGT')
      call gngt(value,mantissa,exponent)
      call prccls('DclGoodNumExGT')
    end subroutine
      
    subroutine DclGoodNumExLE(value,mantissa,exponent) 
      real,      intent(in)  :: value                   !調べる実数値
      real,      intent(out) :: mantissa                !きりのよい数の仮数部
      integer,   intent(out) :: exponent                !きりのよい数の指数部

      call prcopn('DclGoodNumExLE')
      call gnle(value,mantissa,exponent)
      call prccls('DclGoodNumExLE')
    end subroutine
      
    subroutine DclGoodNumExGE(value,mantissa,exponent) 
      real,      intent(in)  :: value                   !調べる実数値
      real,      intent(out) :: mantissa                !きりのよい数の仮数部
      integer,   intent(out) :: exponent                !きりのよい数の指数部

      call prcopn('DclGoodNumExGE')
      call gnge(value,mantissa,exponent)
      call prccls('DclGoodNumExGE')
    end subroutine
      
    subroutine DclSetGoodNumList(list)
      real, intent(in), dimension(:) :: list !きりのよい数を昇順に納めた実数型配列．

      call prcopn('DclSetGoodNumList')
      call gnsblk(list, size(list))
      call prccls('DclSetGoodNumList')
    end subroutine
      
    subroutine DclGetGoodNumList(list)
      real, intent(out), dimension(:) :: list !きりのよい数を昇順に納めた実数型配列

      call prcopn('DclGetGoodNumList')
      call gnqblk(list, size(list))
      call prccls('DclGetGoodNumList')
    end subroutine
      
    subroutine DclSaveGoodNumList()
      call prcopn('DclSaveGoodNumList')
      call gnsave()
      call prccls('DclSaveGoodNumList')
    end subroutine
      
    subroutine DclRestoreGoodNumList() 
      call prcopn('DclRestoreGoodNumList')
      call gnrset()
      call prccls('DclRestoreGoodNumList')
    end subroutine
!-----------------------------------------------------------------      
    function DclGoodNumLT(value)
      real, intent(in) :: value 
      real             :: DclGoodNumLT

      call prcopn('DclGoodNumLT')
      DclGoodNumLT = rgnlt(value)
      call prccls('DclGoodNumLT')
    end function
      
    function DclGoodNumLE(value) 
      real, intent(in) :: value  
      real             :: DclGoodNumLE

      call prcopn('DclGoodNumLE')
      DclGoodNumLE = rgnle(value) 
      call prccls('DclGoodNumLE')
    end function
      
    function DclGoodNumGT(value) 
      real, intent(in) :: value
      real             :: DclGoodNumGT

      call prcopn('DclGoodNumGT')
      DclGoodNumGT = rgngt(value) 
      call prccls('DclGoodNumGT')
    end function
      
    function DclGoodNumGE(value)
      real, intent(in) :: value 
      real             :: DclGoodNumGE

      call prcopn('DclGoodNumGE')
      DclGoodNumGE = rgnge(value)
      call prccls('DclGoodNumGE')
    end function

end module
