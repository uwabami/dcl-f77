!-------------------------------------------------
!interface module of intlib
!-------------------------------------------------
module intlib
  use dcl_common
  contains

    function DclIntLT(value)           !valueより小さい最大の整数を求める
      real, intent(in) :: value        !調べる実数値
      integer          :: DclIntLT     !valueより小さい最大の整数関数値

      call prcopn('DclIntLT')
      DclIntLT = irlt(value) 
      call prccls('DclIntLT')
    end function
      
    function DclIntLE(value)           !value以下の最大の整数を求める
      real, intent(in) :: value        !調べる実数値
      integer          :: DclIntLE     !value以下の最大の整数関数値

      call prcopn('DclIntLE')
      DclIntLE = irle(value)  
      call prccls('DclIntLE')
    end function
      
    function DclIntGT(value)           !valueより大きい最小の整数を求める
      real, intent(in) :: value        !調べる実数値
      integer          :: DclIntGT     !valueより大きい最大の整数関数値

      call prcopn('DclIntGT')
      DclIntGT = irgt(value) 
      call prccls('DclIntGT')
    end function
      
    function DclIntGE(value)           !value以上の最小の整数を求める
      real, intent(in) :: value        !調べる実数値
      integer          :: DclIntGE     !value以上の最大の整数関数値

      call prcopn('DclIntGE')
      DclIntGE = irge(value) 
      call prccls('DclIntGE')
    end function

end module
