!-------------------------------------------------
!  BLKlib Module
!-------------------------------------------------
module blklib
  use dcl_common
  contains

    function DclIntervalLT(bounds,value)
      real, intent(in), dimension(:) :: bounds 
      real, intent(in)               :: value 
      integer                        :: DclIntervalLT

      call prcopn('DclIntervalLT')
      DclIntervalLT = iblklt(bounds,n,value)
      call prccls('DclIntervalLT')
    end function
      
    function DclIntervalLE(bounds,value)
      real, intent(in), dimension(:) :: bounds
      real, intent(in)               :: value
      integer                        :: DclIntervalLE

      call prcopn('DclIntervalLE')
      DclIntervalLE = iblkle(bounds,n,value) 
      call prccls('DclIntervalLE')
    end function
      
    function DclIntervalGT(bounds,value)
      real, intent(in), dimension(:) :: bounds
      real, intent(in)               :: value 
      integer                        :: DclIntervalGT

      call prcopn('DclIntervalGT')
      DclIntervalGT = iblkgt(bounds,n,value)
      call prccls('DclIntervalGT')
    end function
      
    function DclIntervalGE(bounds,value)
      real, intent(in), dimension(:) :: bounds
      real, intent(in)               :: value
      integer                        :: DclIntervalGE

      call prcopn('DclIntervalGE')
      DclIntervalGE = iblkge(bounds,n,value)
      call prccls('DclIntervalGE')
    end function

end module
