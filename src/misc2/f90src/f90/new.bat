df -c %1.f90 /include:%DLIB%\module

lib %DLIB%\%DMODE%\dcl.lib *.obj
copy *.mod %DLIB%\module /y

del *.mod
del *.obj
