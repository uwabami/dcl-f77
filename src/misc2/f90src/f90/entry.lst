dcl_common
    function DclSwapIndex(x)                  ! 配列添字の順序逆転

dcl_parm
    interface DclGetParm(name,value)
    interface DclSetParm(name,value)
    interface DclSetParmEx(name,value)
    function  DclGetInteger(name)
    function  DclGetReal(name)
    function  DclGetLogical(name)
    function  DclGetChar(name)

------------------- math1 --------------------

syslib
    subroutine DclMessageDump(level,name,message)                    !メッセージを出力する．
    function   DclCompChar(ch1,ch2)
    function   DclGetUnitNum()                                       !利用可能なもっとも小さい入出力装置番号を返す．

oslib
    subroutine DclExecCommand(command)          !os コマンドを実行する．
    subroutine DclGetEnv(name,value)            !環境変数の値を取得する．
    function   DclGetArgumentNum()              !コマンドライン引数の数 N を得る．
    subroutine DclGetArgument(n,arg)            !N 番目のコマンドライン引数を得る． 
    subroutine DclAbort()                       !エラー処理をおこなって強制終了する．

lrllib
    function   DclEQ(x,y,[epsilon])
    function   DclNE(x,y,[epsilon]) 
    function   DclLT(x,y,[epsilon])
    function   DclLE(x,y,[epsilon]) 
    function   DclGT(x,y,[epsilon]) 
    function   DclGE(x,y,[epsilon])

blklib
    function   DclIntervalLT(bounds,value)
    function   DclIntervalLE(bounds,value)
    function   DclIntervalGT(bounds,value)
    function   DclIntervalGE(bounds,value)

gnmlib
    subroutine DclGoodNumExLT(value,mantissa,exponent)  
    subroutine DclGoodNumExGT(value,mantissa,exponent) 
    subroutine DclGoodNumExLE(value,mantissa,exponent) 
    subroutine DclGoodNumExGE(value,mantissa,exponent) 
    subroutine DclSetGoodNumList(list)
    subroutine DclGetGoodNumList(list)
    subroutine DclSaveGoodNumList()
    subroutine DclRestoreGoodNumList() 
    function   DclGoodNumLT(value)
    function   DclGoodNumLE(value) 
    function   DclGoodNumGT(value) 
    function   DclGoodNumGE(value)

intlib
    function   DclIntLT(value)           !valueより小さい最大の整数を求める
    function   DclIntLE(value)           !value以下の最大の整数を求める
    function   DclIntGT(value)           !valueより大きい最小の整数を求める
    function   DclIntGE(value)           !value以上の最小の整数を求める

rfalib
    function   DclGetAVE(rx)                        !平均を求める
    function   DclGetVAR(rx)                        !分散を求める
    function   DclGetSTD(rx)                        !標準偏差を求める
    function   DclGetRMS(rx)                        !root mean squareを求める
    function   DclGetAMP(rx)                        !大きさ (（Σ rx^2)^1/2 ) を求める

indxlib
    interface  DclLocFirst(array, value)         ! integer, real, char の generic
    interface  DclLocLast(array, value)
    function   DclLocFirstCharEx(array,value)    ! 大文字小文字を区別しない
    function   DclLocLastCharEx(array,value)

ctrlib
    interface  DclConv2D           !極座標、楕円座標、双極座標、直角双曲線座標->直角座標
    function   DclConvPolar(point)         !直角座標->極座標
    function   DclConvHyperbolic(point)    !直角双曲線座標->直角座標
    function   DclConv3D(point)            !3次元球面座標->直角座標
    function   DclConvSpherical(point)     !3次元球面座標と直角座標の変換をする．
    function   DclRotate2D(theta,point)    !2次元直角座標を回転する．
    function   DclRotate3D(theta,phi,psi, point) !3次元直角座標を回転する．
    function   DclRotateSpherical(theta,phi,psi,point) !球面座標を回転する．

maplib
    function   DclCylindrical_F(point)  
    function   DclMercator_F(point)
    function   DclMollweide_F(point)
    function   DclMollweideLike_F(point)
    function   DclHammer_F(point)
    function   DclEckert6_F(point)
    function   DclKitada_F(point)
    function   DclConicalA_F(point)
    function   DclConical_F(point)
    function   DclConicalC_F(point)
    function   DclBonnes_F(point)
    function   DclOrthographic_F(point)
    function   DclPolarStereo_F(point)
    function   DclAzimuthal_F(point)
    function   DclAzimuthalA_F(point)
    function   DclCylindrical_B(point) 
    function   DclMercator_B(point)
    function   DclMollweide_B(point)
    function   DclMollweideLike_B(point)
    function   DclHammer_B(point)
    function   DclEckert6_B(point)
    function   DclKitada_B(point)
    function   DclConical_B(point)
    function   DclConicalA_B(point)
    function   DclConicalC_B(point)
    function   DclBonnes_B(point)
    function   DclOrthographic_B(point) 
    function   DclPolarStereo_B(point)
    function   DclAzimuthal_B(point)
    function   DclAzimuthalA_B(point)
    subroutine DclSetConical(ylat)         !標準緯線の指定
    subroutine DclSetConicalA(ylat)        !標準緯線の指定
    subroutine DclSetConicalC(ylat1,ylat2) !標準緯線の指定
    subroutine DclSetBonnes(ylat)          !標準緯線の指定
    subroutine DclSetOrthographic(rsat)    !軌道半径の設定


------------------- math2 --------------------

fftlib
    subroutine DclInitRealFFT(n, [index])       ! 実 FFT
    subroutine DclDeallocRealFFT([index])
    function   DclRealFFT_F(r, [index])
    function   DclRealFFT_B(r, [index])

    subroutine DclInitEasyFFT(n, [index])       !簡易版
    subroutine DclDeallocEasyFFT([index])
    subroutine DclEasyFFT_F(r,a0,a,b,[index])
    subroutine DclEasyFFT_B(r,a0,a,b,[index])

    subroutine DclInitSinFFT(n,[index])         ! sin FFT
    subroutine DclDeallocSinFFT([index])
    function   DclSinFFT(r,[index])

    subroutine DclInitCosFFT(n,[index])         ! cos FFT
    subroutine DclDeallocCosFFT([index])
    function   DclCosFFT(r, [index])

    subroutine DclInitSinQFT(n,[index])         ! 1/4周期 sin FFT
    subroutine DclDeallocSinQFT([index])
    function   DclSinQFT_F(r, [index])
    function   DclSinQFT_B(r, [index])

    subroutine DclInitCosQFT(n,[index])         ! 1/4周期 cos FFT
    subroutine DclDeallocCosQFT([index])
    function   DclCosQFT_F(r, [index])
    function   DclCosQFT_B(r, [index])

    subroutine DclInitComplexFFT(n,[index])      ! 複素 FFT
    subroutine DclDeallocComplexFFT([index])
    function   DclComplexFFT_F(c,[index])
    function   DclComplexFFT_B(c,[index])

shtlib
    subroutine DclInitSHT(mm,jm,im,[index])              !初期化ルーチン． 
    subroutine DclDeallocSHT([index])                    !作業領域解放
    subroutine DclGetSpectrumNumber(mm,n,m,lr,li)        !スペクトルデータの格納位置を求める．
    subroutine DclOperateLaplacian(mm,ind,a,b)           !スペクトルデータに対してラプラシアンを演算する．
    subroutine DclGridToWave(g,w,[index])                !グリッドデータからウエーブデータへの変換
    subroutine DclWaveToSpectrum(isw,w,s,[index])        !ウエーブデータからスペクトルデータへの変換  
    subroutine DclGridToSpectrum(isw,g,w,s,[index])      !グリッドデータからスペクトルデータへの変換
    subroutine DclSpectrumToWave(isw,s,w,min,max,[index])                  !SHTS2Wの下位ルーチン
    subroutine DclWaveToGrid(w,g,min,max,[index])                          !SHTW2Gの下位ルーチン   
    subroutine DclSpectrumToGrid(isw,s,w,g,min,max,[index])                !SHTS2Gの下位ルーチン．
    subroutine DclSpectrumToWaveForWave(m,isw,s,wr,wi,[index])             !SHTS2Wの下位ルーチン 
    subroutine DclWaveToGridForWave(m,wr,wi,g,[index])                     !SHTW2Gの下位ルーチン 
    subroutine DclSpectrumToGridForWave(m,isw,s,wr,wi,g,[index])           !SHTS2Gの下位ルーチ
    subroutine DclSpectrumToWaveForZonal(isw,s,wz,[index])                 !SHTS2Wの下位ルーチン
    subroutine DclWaveToGridForZonal(jm,im,wz,g)                           !SHTW2Gの下位ルーチン
    subroutine DclSpectrumToGridForZonal(isw,s,wz,g,[index])               !SHTS2Gの下位ルーチン s
    subroutine DclSpectrumToWaveForLatitude(isw,j,s,wj,min,max,[index])    !SHTS2Wの下位ルーチン
    subroutine DclWaveToGridForLatitude(wj,gj,min,max,[index])             !SHTW2Gの下位ルーチン．
    subroutine DclSpectrumToGridForLatitude(isw,j,s,wj,gj,min,max,[index]) !SHTS2Gの下位  

intrlib
    function   DclInterpolateReal(rx)               !実数型配列の補間をする
    function   DclInterpolateComplex(cx)            !複素数型配列の補間をする

rnmlib
    function   DclRunningMean(rx,nb)  !移動平均を計算する． ．

------------------- misc1 --------------------

datelib
    function   DclGetDate()
    function   DclAddDate(date, n) 
    function   DclDiffDate(date1, date2)
    subroutine DclFormatDate(cform,date)
    function   DclDayOfWeek(date)
    function   DclLengthOfMonth(iy,im)            !iy年im月は何日あるかを返す．
    function   DclLengthOfYear(iy)                !iy年は何日あるかを返す．

timelib
    function   DclGetTime()
    subroutine DclFormatTime(cform,time)

chklib
    subroutine DclToUpper(ch)               !文字列を大文字化化する．
    subroutine DclToLower(ch)               !文字列を小文字化する．
    function   DclCheckBlank(c)             !空白かどうかを判別する．
    function   DclCheckCurrency(c)          !通貨記号かどうかを判別する
    function   DclCheckSpecial(c)           !特殊文字かどうかを判別する
    function   DclCheckAlphabet(c)          !英字かどうかを判別する
    function   DclCheckNumber(c)            !数字かどうかを判別する
    function   DclCheckAlphaNum(c)          !英数字かどうかを判別する
    function   DclCheckFortran(c)           !fortran文字かどうかを判別する
    function   DclCheckCharPattern(char,cref) !文字列の種類を判別する．

chglib
    subroutine DclToUpper(ch)                         !文字列を大文字化化する．
    subroutine DclToLower(ch)                         !文字列を小文字化する．

------------------- grph1 --------------------

sgpack
    subroutine DclPrintDeviceList()                     !ワークステーション名のリスト
    subroutine DclTransShortToLong(cts,ctl)             !略称から名称を求める．
    subroutine DclTransShortToNum(cts,ntx)              !略称から変換関数番号を求める．
    subroutine DclTransLongToShort(ctl,cts)             !名称から略称を求める．
    subroutine DclTransLongToNum(ctl,ntx)               !名称から変換関数番号を求める．
    subroutine DclTransNumToShort(ntx,cts)              !変換関数番号から略称を求める．
    subroutine DclTransNumToLong(ntx,ctl)               !変換関数番号から名称を求める．

    subroutine DclGetViewPort(xmin, xmax, ymin, ymax) 
    subroutine DclGetWindow(xmin, xmax, ymin, ymax) 
    subroutine DclGetSimilarity(factor, xoffset, yoffset)  
    subroutine DclGetMapProjectionAngle(longitude,latitude,rotation)  
    function   DclGetTransNumber() 

    subroutine DclDrawLine (x,y,[type],[index])             !折れ線を描く．
    subroutine DclDrawLineNormalized(x, y, [type], [index]) !V座標
    subroutine DclDrawLineProjected(x,y,[type],[index])     !R座標
    subroutine DclSetLineType(type)                         !ラインタイプを設定する．   
    subroutine DclSetLineIndex(index)                       !ラインインデクスの設定．   
    subroutine DclSetLineText(text)                         !ラベルの文字列設定．  
    subroutine DclSetLineTextSize(height)                   !ラベルの文字高設定  
    subroutine DclNextLineText()                            !ラベルの最後の文字番号を増やす．
    function   DclGetLineType()  
    function   DclGetLineIndex() 
    subroutine DclGetLineText(text)   
    function   DclGetLineTextSize()   

    subroutine DclDrawMarker(x,y,[type],[index],[height])         ! マーカ
    subroutine DclDrawMarkerNormalized(x, y, [type], [index], [height]) 
    subroutine DclDrawMarkerProjected(x, y, [type], [index], [height])
    subroutine DclSetMarkerType(type)                  !マーカータイプの設定．    
    subroutine DclSetMarkerIndex(index)                !マーカーのラインインデクスの設定  
    subroutine DclSetMarkerSize(height)                !マーカーの大きさ設定．     
    function   DclGetMarkerType()  
    function   DclGetMarkerIndex()
    function   DclGetMarkerSize()

    subroutine DclDrawText(x, y, text, [height], [angle], [centering], [index]) 
    subroutine DclDrawTextNormalized(x, y, text, [height], [angle], [centering], [index])
    subroutine DclDrawTextProjected(x, y, text, [height], [angle], [centering], [index])
    subroutine DclSetTextHeight(height)                !文字の高さ設定．  
    subroutine DclSetTextAngle(angle)                  !文字列の角度の設定．  
    subroutine DclSetTextIndex(index)                  !文字列のラインインデクスの設定．                 
    subroutine DclSetTextPosition(centering)           !文字列のセンタリングオプション設定
    function   DclGetTextHeight() 
    function   DclGetTextAngle()
    function   DclGetTextIndex() 
    function   DclGetTextPosition()  

    subroutine DclDrawHatch(x, y, [pattern])             !u 座標系で多角形領域の塗りつぶし．
    subroutine DclDrawHatchNormalized(x, y, [pattern])
    subroutine DclDrawHatchProjected(x, y, [pattern])
    subroutine DclSetHatchPattern(pattern)             ! トーンパターン番号設定
    function   DclGetHatchPattern()                    !現在設定されているトーンパターン番号参照

               DclShadeRegion
               DclShadeRegionNormalized
               DclShadeRegionProjected
               DclSetShadePattern
               DclGetShadePattern

    subroutine DclDrawArrow(x1,y1,x2,y2,[type],[index])
    subroutine DclDrawArrowNormalized(x1,y1,x2,y2,[type],[index])
    subroutine DclDrawArrowProjected(x1,y1,x2,y2,[type],[index])
    subroutine DclSetArrowLineType(type)               !描く線分のラインタイプを設定する． 
    subroutine DclSetArrowLineIndex(index)             !描く線分の ラインインデクスを設定する
    function   DclGetArrowLineType()                   !現在設定されているラインタイプ
    function   DclGetArrowLineIndex()                  !現在設定されているラインインデクス

scpack
    subroutine DclSet3DViewPort(xmin,xmax, ymin,ymax, zmin,zmax)
    subroutine DclSet3DWindow(xmin,xmax,ymin,ymax,zmin,zmax)     !ウインドウの設定．
    subroutine DclSet3DLogAxis(log_x,log_y,log_z)                !対数軸の設定．
    subroutine DclSet3DOrigin(factor,x,y,z)                      !スケーリングファクターと原点の設定
    subroutine DclSet3DTransNumber(number)                       !変換関数番号の設定
    subroutine DclSet3DTransfunction  ()                         !変換関数の確定． 
    subroutine DclGet3DViewPort(xmin,xmax,ymin,ymax,zmin,zmax)
    subroutine DclGet3DWindow(xmin,xmax,ymin,ymax,zmin,zmax) 
    subroutine DclGet3DLogAxis(log_x,log_y,log_z) 
    subroutine DclGet3DOrigin(factor, x, y, z) 
    function   DclGet3DTransNumber(number) 
    subroutine DclSet3DEyePoint(x, y, z)                         !視点の設定．
    subroutine DclSet3DObjectPoint(x, y, z)                      !焦点の設定
    subroutine DclSet2DPlane(x_dir, y_dir, section)              !2次元平面の割付
    subroutine DclSet3DProjection()                              !透視変換の確定． 
    subroutine DclGet3DEyePoint(x, y, z)
    subroutine DclGet3DObjectPoint(x, y, z)
    subroutine DclGet2DPlane(x_dir, y_dir, section)
    subroutine DclDraw3DLine(x,y,z,index)                        !u 座標系で折れ線を描く．  
    subroutine DclDraw3DLineNormalized(x,y,z,index)              !v 座標系で折れ線を描く
    subroutine DclSet3DLineIndex(index)                          !ラインインデクスの設定．
    function   DclGet3DLineIndex()
    subroutine DclDraw3DMarker(x,y,z,type,index,height)          !u 座標系でマーカー列を描く
    subroutine DclDraw3DMarkerNormalized(x,y,z,type,index,height)
    subroutine DclSet3DMarkerType(type)                          !マーカータイプの設定．    
    subroutine DclSet3DMarkerIndex(index)                        !マーカーのラインインデクスの設定  
    subroutine DclSet3DMarkerSize(height)                        !マーカーの大きさ設定．      
    function   DclGet3DMarkerType()
    function   DclGet3DMarkerIndex()
    function   DclGet3DMarkerSize()  
    subroutine DclDraw3DHatch(x,y,z,pattern1,pattern2)
    subroutine DclDraw3DHatchNormalized(x,y,z,pattern1,pattern2)
    subroutine DclSet3DHatchPattern(pattern1,pattern2)           !トーンパターン番号設定． 
    subroutine DclGet3DHatchPattern(pattern1,pattern2)

slpack
    subroutine DclInitLayout(xmax,ymax,factor)                       !初期化
    interface  DclSetFrameSize(dx,dy)|(csize)                        !第1フレームの再設定
    subroutine DclDivideFrame(cform,ix,iy)                           !フレームの分割
    subroutine DclSetFrameMargin(left, right, bottom, top)           !マージンの設定． 
    subroutine DclSetAspectRatio(rx,ry)                              !縦横比の設定．   
    subroutine DclSetFrameTitle(title,side,x_position,y_position,height,nt)
    subroutine DclDrawViewPortFrame(index)                           !ビューポートの枠を描く．
    subroutine DclDrawDeviceWindowFrame(index)                       !ウインドウの枠を描く
    subroutine DclDrawDeviceViewPortFrame(index)                     !最大作画領域の枠を描
    subroutine DclDrawViewPortCorner(index,size)                     !ビューポートのコーナーマークを描く
    subroutine DclDrawDeviceWindowCorner(index,size)                 !ウインドウのコーナーマークを描く
    subroutine DclDrawDeviceViewPortCorner(index,size) 

------------------- grph2 --------------------

grpack
    subroutine DclOpenGraphics([ws_id])
    subroutine DclNewFrame
    subroutine DclNewFig
    subroutine DclCloseGraphics
    subroutine DclSetTransfunction  
    subroutine DclSetTransNumber (itr)
    subroutine DclSetMapProjectionAngle ([longitude], [latitude], [rotation])
    subroutine DclSetSimilaity ([factor],[xoffset],[yoffset])
    subroutine DclSetMapProjectionWindow([xmin], [xmax], [ymin], [ymax])
    subroutine DclSetViewPort([xmin],[xmax],[ymin],[ymax])
    subroutine DclSetWindow([xmin],[xmax],[ymin],[ymax])

ucpack
    subroutine DclDrawXAxisDay(cside,jd0,nd)      !日に関する座標軸を描く．
    subroutine DclDrawYAxisDay(cside,jd0,nd)      !日に関する座標軸を描く．
    subroutine DclDrawXAxisMonth(cside,jd0,nd)    !月に関する座標軸を描く．
    subroutine DclDrawYAxisMonth(cside,jd0,nd)    !月に関する座標軸を描く．
    subroutine DclDrawXAxisYear(cside,jd0,nd)     !年に関する座標軸を描く．
    subroutine DclDrawYAxisYear (cside,jd0,nd)    !年に関する座標軸を描く．

udpack
    subroutine DclDrawContour(z)                  !2次元等高線図を描く
    subroutine DclSetContourLine(level,[index] ,[type] ,[label] ,[height])
    subroutine DclGetContourLine(number,[level],[index],[type],[label],[height]) 
    interface  DclSetCoutourLevel(xmin, xmax, dx)!コンターレベル値を設定する
                                 (z,dx)
    function   DclGetContourLevelNumber()         !現在設定されているコンターレベルの総本数
    subroutine DclDelContourLevel(zlev)           !あるコンターレベルを削除する．
    subroutine DclClearCounourLevel()             !コンターレベルを無効にする．
    function   DclGetContourInterval(nlev)        !コンターレベルの間隔を求める．
    subroutine DclSetContourLabelFormat(cfmt)     !コンターラベルのフォーマットを指定する．
    subroutine DclGetContourLabelFormat(cfmt)     !現在設定されているフォーマット

uepack
    subroutine DclDrawHatch(z)
    subroutine DclDrawHatchEx(z)
    interface  DclSetHatchLevel(xmin,xmax,dx)
                               (z,mx,nx,ny,dx)
                               (level1,level2,pattern)
                               (level,pattern)
    subroutine DclGetHatchLevel(number, level1, level2, pattern) 
    function   DclGetHatchLevelNumber()
    subroutine DclClearHatchLevel()

               DclShadeContour
               DclShadeContourEX
               DclSetShadeLevel
               DclGetShadeLevel
               DclGetShadeLevelNumber
               DclClearShadeLevel

ugpack
    subroutine DclDrawVectors(u,v)
    subroutine DclSetUnitVectorTitle(cside,title) 

ulpack
    subroutine DclDrawXLogAxis(cside,nlbl,nticks)
    subroutine DclDrawYLogAxis(cside,nlbl,nticks)
    subroutine DclSetXLogFormat(cfmt)
    subroutine DclSetYLogFormat(cfmt)
    subroutine DclGetXLogFormat(cfmt) 
    subroutine DclGetYLogFormat(cfmt)
    subroutine DclSetXLogLabel(position)
    subroutine DclSetYLogLabel(position)
    subroutine DclGetXLogLabel(position) 
    subroutine DclGetYLogLabel(position)            !y軸ラベルを描く位置を参照する．

umpack
    subroutine DclSetMapContactPoint(x, y, rot)  !投影面の「接点」を指定する．
    subroutine DclSetCircleWindow(x, y, r)       !円形のウィンドウを設定する．
    subroutine DclSetMapPoint(x, y)              !地図に含める点を指定する．
    subroutine DclFitMapParm()                   !地図投影の変換関数のパラメタを適切に決める．
    subroutine DclDrawGlobe()                    !地図の境界線（縁）と緯度線,経度線を描く．
    subroutine DclDrawGrid()                     !緯度線・経度線を描く．
    subroutine DclDrawLimb()                     !地図の境界線（縁）を描く．
    subroutine DclDrawMap(cdsn)                  !各種地図情報を描く．

uspack
    subroutine DclDrawAxis
    subroutine DclFitScalingParm
    subroutine DclDrawXAxis(side)
    subroutine DclDrawYAxis(side)
    subroutine DclDrawGraph(x, y, [type], [index])
    subroutine DclScalingPoint(x, y)
    subroutine DclSetTitle(xtitle, ytitle, [xunit], [yunit])

uupack
    subroutine DclSetErrorBarLineType(itype)          !エラーバーのラインタイプを指定する．  
    function   DclGetErrorBarLineType()   
    subroutine DclSetErrorBarLineIndex(index)         !エラーバーのラインインデクスを指定する．
    function   DclGetErrorBarLineIndex()
    subroutine DclSetErrorBarWidth(width)             !エラーバーの横幅を指定する．
    function   DclGetErrorBarWidth()
    subroutine DclSetBarWidth(width)                  !棒グラフの横巾を指定する．
    function   DclGetBarWidth()
    subroutine DclSetAreaPattern(itp1,itp2)           ! 内部領域を塗るトーンパターンを指定する．
    subroutine DclGetAreaPattern(itp1,itp2)
    subroutine DclSetFrameType(type)                  !枠の属性を指定する．
    function   DclGetFrameType()
    subroutine DclSetFrameIndex(index)                !枠のラインインデクスを指定する．
    function   DclGetFrameIndex()

uhpack
    subroutine DclDrawXErrorBar(x1,x2,y,[type],[index],[width])
    subroutine DclHatchXGap(x1,x2,y,[pattern1],[pattern2])
    subroutine DclDrawXBarFrame(x1,x2,y,[type],[index],[width])
    subroutine DclHatchXBarArea(x1,x2,y,[pattern1],[pattern2],[width])
    subroutine DclDrawXBarLine(x,y,[type],[index],[width])
    subroutine DclDrawXBoxFrame(x1,x2,y,[type],[index])
    subroutine DclHatchXBoxArea(x1,x2,y,[pattern1],[pattern2])
    subroutine DclDrawXBoxLine(x,y,[type],[index])

uvpack
    subroutine DclDrawYErrorBar(x,y1,y2,[type],[index],[width])
    subroutine DclHatchYGap(x,y1,y2,[pattern1],[pattern2])
    subroutine DclDrawYBarFrame(x,y1,y2,[type],[index],[width])
    subroutine DclHatchYBarArea(x,y1,y2,[pattern1],[pattern2],[width])
    subroutine DclDrawYBarLine(x,y,[type],[index],[width])
    subroutine DclDrawYBoxFrame(x,y1,y2,[type],[index])
    subroutine DclHatchYBoxArea(x,y1,y2,[pattern1],[pattern2])
    subroutine DclDrawYBoxLine(x,y,[type],[index])

uwpack
    subroutine DclSetXGrid(x)                  !格子点配列の格子点座標を各座標値で設定する．
    subroutine DclGetXGrid(x)                  !現在設定されている格子点の座標値
    subroutine DclSetYGrid(y)                  !格子点配列の格子点座標を各座標値で設定する．
    subroutine DclGetYGrid(y)                  !現在設定されている格子点の座標値
    subroutine DclSetXEvenGrid(xmin,xmax,n)    !格子点座標を等間隔に設定する．
    subroutine DclSetYEvenGrid(ymin,ymax,n)    !格子点座標を等間隔に設定する．
    subroutine DclGetXEvenGrid(xmin,xmax,n)    !格子点座標の最小値,最大値,格子点数の参照
    subroutine DclGetYEvenGrid(ymin,ymax,n)    !格子点座標の最小値,最大値,格子点数の参照
    function   DclGetXGridValue(ix)            !格子点の座標値を返す．
    function   DclGetYGridValue(iy)            !格子点の座標値を返す．
    function   DclGetXGridNumber(x)            !座標値に最も近い格子番号を返す．
    function   DclGetYGridNumber(y)            !座標値に最も近い格子番号を返す．

uxpack
    subroutine DclDrawXAxisInterval(side,dx1,dx2) 
    subroutine DclDrawXAxisValue(side,x1,x2)
    subroutine DclDrawXAxisLabel(side,x1,x2,label)
    subroutine DclDrawXMainTitle(side,title,position)
    subroutine DclDrawXSubTitle(side,title,position)
    subroutine DclDrawXAxisLine(side,switch)
    subroutine DclDrawXTickmark(side,switch,x)
    subroutine DclDrawXLabel(side,switch,x,label)
    subroutine DclDrawXLabelValue(side,switch,x)
    subroutine DclDrawXTitle(side,switch,title,position)
    subroutine DclSetXLabelFormat(format)
    subroutine DclGetXLabelFormat(format)

uypack
    subroutine DclDrawYAxisInterval(side,dy1,dy2) 
    subroutine DclDrawYAxisValue(side,y1,y2)
    subroutine DclDrawYAxisLabel(side,y1,y2,label)
    subroutine DclDrawYMainTitle(side,title,position)
    subroutine DclDrawYSubTitle(side,title,position)
    subroutine DclDrawYAxisLine(side,switch)
    subroutine DclDrawYTickmark(side,switch,y)
    subroutine DclDrawYLabel(side,switch,y,label)
    subroutine DclDrawYLabelValue(side,switch,y)
    subroutine DclDrawYTitle(side,switch,title,position)
    subroutine DclSetYLabelFormat(format)
    subroutine DclGetYLabelFormat(format)

uzpack
    subroutine DclSetAxisFactor(fact)
