!-------------------------------------------------
!  OSlib Module
!-------------------------------------------------
module oslib
  use dcl_common
  contains

    subroutine DclExecCommand(command)          !os コマンドを実行する．
      character(len=*), intent(in) :: command   !コマンド名

      call prcopn('DclExecCommand')
      call osexec(command)
      call prccls('DclExecCommand')
    end subroutine
      
    subroutine DclGetEnv(name,value)            !環境変数の値を取得する．
      character(len=*), intent(in)  :: name     !環境変数名
      character(len=*), intent(out) :: value    !環境変数の値

      call prcopn('DclGetEnv')
      call osgenv(name,value)  
      call prccls('DclGetEnv')
    end subroutine
      
    function DclGetArgumentNum()                !コマンドライン引数の数 N を得る．
      integer :: DclGetArgumentNum              !コマンドライン引数の数

      call prcopn('DclGetArgumentNum')
      call osqarn(DclGetArgumentNum)
      call prccls('DclGetArgumentNum')
    end function
      
    subroutine DclGetArgument(n,arg)            !N 番目のコマンドライン引数を得る． 
      integer,           intent(in) :: n        !コマンドライン引数の位置
      character(len=*), intent(out) :: arg      !n 番目のコマンドライン引数

      call prcopn('DclGetArgument')
      call osgarg(n,arg)
      call prccls('DclGetArgument')
    end subroutine
      
    subroutine DclAbort()                       !エラー処理をおこなって強制終了する．
      call prcopn('DclAbort')
      call osabrt() 
      call prccls('DclAbort')
    end subroutine

end module
