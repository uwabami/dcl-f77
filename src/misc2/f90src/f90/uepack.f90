!-------------------------------------------------
!  UEpack Module
!-------------------------------------------------
module uepack
  use dcl_common

  interface DclSetShadeLevel
     module procedure DclSetShadeA, DclSetShadeB, DclSetShadeV, DclSetShadeN
  end interface

  private :: DclSetShadeA, DclSetShadeB, DclSetShadeV, DclSetShadeN

  contains
!------------------------------2次元等値線図のぬりわけをおこなう．
    subroutine DclShadeContour(z)
      real,      intent(in), dimension(:,:) :: z

      call sgoopn('DclShadeContour', ' ')
      nx = size(z,1)
      ny = size(z,2)
      call uetone(z,nx,nx,ny)   
      call sgocls('DclShadeContour')
    end subroutine
!------------------------------2次元等値線図のぬりわけをおこなう．
    subroutine DclShadeContourEx(z)
      real,      intent(in), dimension(:,:) :: z

      call sgoopn('DclShadeContourEX', ' ')
      nx = size(z,1)
      ny = size(z,2)
      call uetonf(z,nx,nx,ny)  
      call sgocls('DclShadeContourEX')
    end subroutine
!------------------------------トーンレベル値を最小値・最大値,きざみ値で設定する．
    subroutine   DclSetShadeA(xmin,xmax,dx)
      real,      intent(in) :: xmin,xmax    !トーンレベルの最小・最大値
      real,      intent(in) :: dx           !dx > 0 のときdxをきざみ幅

      call prcopn('DclSetShadeLevel')
      call uegtla(xmin,xmax,dx) 
      call prccls('DclSetShadeLevel')
    end subroutine
!------------------------------トーンレベル値を配列,きざみ値で設定する．
    subroutine DclSetShadeB(z,dx)
      real,      intent(in), dimension(:,:) :: z
      real,      intent(in) :: dx            !dx > 0 のときdxをきざみ幅

      call prcopn('DclSetShadeLevel')
      nx = size(z,1)
      ny = size(z,2)
      call uegtlb(z,nx,nx,ny,dx) 
      call prccls('DclSetShadeLevel')
     end subroutine
!------------------------------ ぬりわけるレベルとパターンを1レベルごと指定する．
    subroutine DclSetShadeV(level1,level2,pattern)
      real,      intent(in) :: level1,level2  !ぬりわけるレベルの下限上限値
      integer,   intent(in) :: pattern        !トーンパターン番号

      call prcopn('DclSetShadeLevel')
      call uestlv(level1,level2,pattern)
      call prccls('DclSetShadeLevel')
    end subroutine
!----------------------------- ぬりわけるレベルとパターンを配列で複数レベル指定する．
    subroutine DclSetShadeN(level,pattern)
      integer, intent(in), dimension(:) :: pattern 
      real,    intent(in), dimension(size(pattern)) :: level 

      call prcopn('DclSetShadeLevel')
      call uestln(level,pattern,size(pattern)) 
      call prccls('DclSetShadeLevel')
    end subroutine
!-------------------------------第iton番目のトーンレベルの属性
    subroutine DclGetShadeLevel(number, level1, level2, pattern) 
      integer,   intent(in) :: number
      real,      intent(out) :: level1, level2
      integer,   intent(out) :: pattern

      call prcopn('DclGetShadeLevel')
      call ueqtlv(level1,level2,pattern,number) 
      call prccls('DclGetShadeLevel')
    end subroutine
!------------------------------現在設定されているトーンレベルの数
    function DclGetShadeLevelNumber()
      integer :: DclGetShadeLevelNumber

      call prcopn('DclGetShadeLevelNumber')
      call ueqntl(DclGetShadeLevelNumber)  
      call prccls('DclGetShadeLevelNumber')
    end function
!-----------------------------トーンレベルを無効にする．
    subroutine DclClearShadeLevel()

      call prcopn('DclClearShadeLevel')
      call ueitlv() 
      call prccls('DclClearShadeLevel')
    end subroutine
      
end module
