!-------------------------------------------------
!interface module of ugpack
!-------------------------------------------------
module ugpack
  use dcl_common
  contains
!-----------------------------2次元ベクトル場を描く．
    subroutine DclDrawVectors(u,v)
      real, intent(in), dimension(:,:)                 :: u 
      real, intent(in), dimension(size(u,1),size(u,2)) :: v 

      call sgoopn('DclDrawVectors', ' ')
      nx = size(u,1)
      ny = size(u,2)
      call ugvect(u,nx,v,nx,nx,ny)
      call sgocls('DclDrawVectors')
    end subroutine
!-----------------------------ユニットベクトルにつけるタイトルを設定する．
    subroutine DclSetUnitVectorTitle(side,title) 
      character(len=1), intent(in) :: side  !タイトル位置を指定する
      character(len=*), intent(in) :: title  !タイトル

      call prcopn('DclSetUnitVectorTitle')
      call ugsut(side,title) 
      call prccls('DclSetUnitVectorTitle')
    end subroutine
      
end module

