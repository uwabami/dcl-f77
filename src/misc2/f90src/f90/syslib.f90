!-------------------------------------------------
!  SYSlib Module
!-------------------------------------------------
module syslib
  use dcl_common
  contains
!------------------------------------------------------------
    subroutine DclMessageDump(level,name,message)  !メッセージを出力する．
      character(len=1), intent(in) :: level
      character(len=6), intent(in) :: name 
      character(len=*), intent(in) :: message
      
      call prcopn('DclMessageDump')
      call msgdmp(level,name,message)
      call prccls('DclMessageDump')
    end subroutine
!------------------------------------------------------------
 
    function DclCompChar(ch1,ch2)
      logical DclCompChar, lchreq
      character(len=*), intent(in) :: ch1, ch2

      call prcopn('DclCompChar')
      DclCompChar = lchreq(ch1,ch2) 
      call prccls('DclCompChar')
    end function
      
    function DclGetUnitNum()  !利用可能なもっとも小さい入出力装置番号を返す．
      integer DclGetUnitNum

      call prcopn('DclGetUnitNum')
      DclGetUnitNum = iufopn()
      call prccls('DclGetUnitNum')
    end function
end module
