!-------------------------------------------------
!  UWpack Module
!-------------------------------------------------
module uwpack
  use dcl_common
  contains
!-------------------------------------------------
    subroutine DclSetXGrid(x)  !格子点配列の格子点座標を各座標値で設定する．
      real, intent(in), dimension(:) :: x  !x座標をu座標系の値で指定する

      call prcopn('DclSetXGrid')
      call uwsgxa(x,size(x))
      call prccls('DclSetXGrid')
    end subroutine
!-------------------------------------------------
    subroutine DclGetXGrid(x)    !現在設定されている格子点の座標値
      real, intent(out), dimension(:) :: x

      call prcopn('DclGetXGrid')
      call uwqgxa(x,size(x)) 
      call prccls('DclGetXGrid')
    end subroutine
!-------------------------------------------------
    subroutine DclSetYGrid(y)    !格子点配列の格子点座標を各座標値で設定する．
      real, intent(in), dimension(:) :: y  ! y座標をu座標系の値で指定する

      call prcopn('DclSetYGrid')
      call uwsgya(y, size(y))
      call prccls('DclSetYGrid')
    end subroutine
!-------------------------------------------------
    subroutine DclGetYGrid(y)    !現在設定されている格子点の座標値
      real, intent(out), dimension(:) :: y

      call prcopn('DclGetYGrid')
      call uwqgya(y,size(y))
      call prccls('DclGetYGrid')
    end subroutine
!-------------------------------------------------
    subroutine DclSetXEvenGrid(xmin,xmax,n)  !格子点座標を等間隔に設定する．
      real,   intent(in) :: xmin, xmax       !u座標系におけるx座標の最小最大値
      integer,intent(in) :: n                !x方向の格子点数．

      call prcopn('DclSetXEvenGrid')
      call uwsgxb(xmin,xmax,n) 
      call prccls('DclSetXEvenGrid')
    end subroutine
!-------------------------------------------------
    subroutine DclSetYEvenGrid(ymin,ymax,n)  !格子点座標を等間隔に設定する．
      real,    intent(in) :: ymin,ymax       !u座標系におけるy座標の最小最大値
      integer, intent(in) :: n               !y方向の格子点数．

      call prcopn('DclSetYEvenGrid')
      call uwsgyb(ymin,ymax,n)   
      call prccls('DclSetYEvenGrid')
    end subroutine
!-------------------------------------------------
    subroutine DclGetXEvenGrid(xmin,xmax,n)  !格子点座標の最小値,最大値,格子点数の参照
      real,   intent(out) :: xmin,xmax       !u座標系におけるx座標の最小値
      integer,intent(out) :: n               !x方向の格子点数．

      call prcopn('DclGetXEvenGrid')
      call uwqgxb(xmin,xmax,n)  
      call prccls('DclGetXEvenGrid')
    end subroutine
!-------------------------------------------------
    subroutine DclGetYEvenGrid(ymin,ymax,n)  !格子点座標の最小値,最大値,格子点数の参照
      real,      intent(out) :: ymin,ymax    !u座標系におけるy座標の最小値
      integer,   intent(out) :: n            !y方向の格子点数

      call prcopn('DclGetYEvenGrid')
      call uwqgyb(ymin,ymax,n)       
      call prccls('DclGetYEvenGrid')
    end subroutine
!-------------------------------------------------
    function DclGetXGridValue(ix)            !格子点の座標値を返す．
      real                :: DclGetXGridValue
      integer, intent(in) :: ix              !格子点の番号

      call prcopn('DclGetXGridValue')
      DclGetXGridValue = ruwgx(ix)   
      call prccls('DclGetXGridValue')
    end function
!-------------------------------------------------
    function DclGetYGridValue(iy)            !格子点の座標値を返す．
      real                :: DclGetYGridValue
      integer, intent(in) :: iy              !格子点の番号

      call prcopn('DclGetYGridValue')
      DclGetYGridValue = ruwgy(iy)  
      call prccls('DclGetYGridValue')
    end function
!-------------------------------------------------
    function DclGetXGridNumber(x)            !座標値に最も近い格子番号を返す．
      integer             :: DclGetXGridNumber
      real,    intent(in) :: x               !座標値

      call prcopn('DclGetXGridNumber')
      DclGetXGridNumber = iuwgx(x) 
      call prccls('DclGetXGridNumber')
    end function
!-------------------------------------------------
    function DclGetYGridNumber(y)            !座標値に最も近い格子番号を返す．
      integer             :: DclGetYGridNumber
      real,    intent(in) :: y               !格子点の番号

      call prcopn('DclGetYGridNumber')
      DclGetYGridNumber = iuwgy(y) 
      call prccls('DclGetYGridNumber')
    end function
end module
