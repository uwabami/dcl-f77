!-------------------------------------------------
!  INDXlib Module
!-------------------------------------------------
module indxlib
  use dcl_common
  interface DclLocFirst
    module procedure DclLocFirstChar, DclLocFirstInt, DclLocFirstReal
  end interface

  interface DclLocLast
    module procedure DclLocLastChar, DclLocLastInt, DclLocLastReal
  end interface

  private :: DclLocFirstChar, DclLocFirstInt, DclLocFirstReal
  private :: DclLocLastChar,  DclLocLastInt,  DclLocLastReal
  
  contains
    
    function DclLocFirstChar(array,value)
      character(len=*), intent(in), dimension(:) :: array 
      character(len=*), intent(in)               :: value
      integer                                    :: DclLocFirstChar

      call prcopn('DclLocFirst')
      DclLocFirstChar = indxnf(array,size(array),1,value)
      call prccls('DclLocFirst')
    end function
      
    function DclLocLastChar(array,value)
      character(len=*), intent(in), dimension(:) :: array
      character(len=*), intent(in)               :: value
      integer                                    :: DclLocLastChar

      call prcopn('DclLocLast')
      DclLocLastChar = indxnl(array,size(array),1,value) 
      call prccls('DclLocLast')
    end function
      
    function DclLocFirstCharEx(array,value)
      character(len=*), intent(in), dimension(:) :: array
      character(len=*), intent(in)               :: value 
      integer                                    :: DclLocFirstCharEx

      call prcopn('DclLocFirst')
      DclLocFirstCharEx = indxmf(array,size(array),1,value)
      call prccls('DclLocFirst')
    end function
      
    function DclLocLastCharEx(array,value)
      character(len=*), intent(in), dimension(:) :: array
      character(len=*), intent(in)               :: value
      integer                                    :: DclLocLastCharEx

      call prcopn('DclLocLast')
      DclLocLastCharEx = indxml(array,size(array),1,value)
      call prccls('DclLocLast')
    end function
      
    function DclLocFirstInt(array,value)
      integer, intent(in), dimension(:) :: array
      integer, intent(in)               :: value
      integer                           :: DclLocFirstInt

      call prcopn('DclLocFirst')
      DclLocFirstInt = indxif(array,size(array),1,value)
      call prccls('DclLocFirst')
    end function
      
    function DclLocLastInt(array,value)
      integer, intent(in), dimension(:) :: array
      integer, intent(in)               :: value
      integer                           :: DclLocLastInt

      call prcopn('DclLocLast')
      DclLocLastInt = indxil(array,size(array),1,value)
      call prccls('DclLocLast')
    end function
      
    function DclLocFirstReal(array,value) 
      real, intent(in), dimension(:) :: array
      real, intent(in)               :: value
      integer                        :: DclLocFirstReal

      call prcopn('DclLocFirst')
      DclLocFirstReal = indxrf(array,size(array),1,value)
      call prccls('DclLocFirst')
    end function
      
    function DclLocLastReal(array,value)
      real, intent(in), dimension(:) :: array
      real, intent(in)               :: value
      integer                        :: DclLocLastReal

      call prcopn('DclLocLast')
      DclLocLastReal = indxrl(array,size(array),1,value)
      call prccls('DclLocLast')
    end function

end module

