!-------------------------------------------------
!  UMpack Module
!-------------------------------------------------
module umpack
  use dcl_common
  contains
!以下の２つは見当たらない
!UMQCNT(XCNT, YCNT, ROT)				DclGetMapContactPoint
!UMQCWD(XCNTR, YCNTR, R)				DclGetCircleWindow

!-------------------------------------------------
    subroutine DclSetMapContactPoint(lon, lat, rot)  !投影面の「接点」を指定する．
      real, intent(in), optional :: lon, lat, rot 

      call prcopn('DclSetMapContactPoint')
      call glrget('rundef', rundef)

      x0   = rundef
      y0   = rundef
      rot0 = rundef

      if(present(lon))  x0   = lon
      if(present(lat))  y0   = lat
      if(present(rot))  rot0 = rot
      
      call umscnt(x0, y0, rot0) 
      call prccls('DclSetMapContactPoint')
    end subroutine
!-------------------------------------------------
    subroutine DclSetCircleWindow(lon, lat, r)  !円形のウィンドウを設定する．
      real,intent(in), optional :: lon, lat, r  

      call prcopn('DclSetCircleWindow')
      call glrget('rundef', rundef)

      x0 = rundef
      y0 = rundef
      r0 = rundef

      if(present(lon)) x0 = lon
      if(present(lat)) y0 = lat
      if(present(r))   r0 = r
      
      call umscwd(x0, y0, r0) 
      call prccls('DclSetCircleWindow')
    end subroutine
!-------------------------------------------------
    subroutine DclSetMapPoint(lon, lat)        !地図に含める点を指定する．
      real, intent(in), dimension(:) :: lon, lat  !点の経度，緯度

      call prcopn('DclSetMapPoint')
      nx = size(lon)
      ny = size(lat)

      if(nx.ne.ny) call msgdmp('M', 'DclSetMapPoint', 'Length of x and y don''t match.')

      n = min(nx, ny)
      call umspnt(n, lon, lat)
      call prccls('DclSetMapPoint')
    end subroutine
!-------------------------------------------------
    subroutine DclFitMapParm()         !地図投影の変換関数のパラメタを適切に決める．
      call prcopn('DclFitMapParm')
      call umpfit() 
      call prccls('DclFitMapParm')
    end subroutine
!-------------------------------------------------
    subroutine DclDrawGlobe()          !地図の境界線（縁）と緯度線,経度線を描く．
      call sgoopn('DclDrawGlobe', ' ')
      call umpglb() 
      call sgocls('DclDrawGlobe')
    end subroutine
!-------------------------------------------------
    subroutine DclDrawGrid()           !緯度線・経度線を描く．
      call sgoopn('DclDrawGrid', ' ')
      call umpgrd()  
      call sgocls('DclDrawGrid')
    end subroutine
!-------------------------------------------------
    subroutine DclDrawLimb()           !地図の境界線（縁）を描く．
      call sgoopn('DclDrawLimb', ' ')
      call umplim()
      call sgocls('DclDrawLimb')
    end subroutine
!-------------------------------------------------
    subroutine DclDrawMap(file_name)        !各種地図情報を描く．
      character(len=*), intent(in) :: file_name        !地図情報ファイル名
      call sgoopn('DclDrawMap', ' ')
      call umpmap(file_name)
      call sgocls('DclDrawMap')
    end subroutine
!-------------------------------------------------
    subroutine DclFillMap(file_name)        !陸地塗り潰し
      character(len=*), intent(in) :: file_name        !地図情報ファイル名
      call sgoopn('DclFillMap', ' ')
      call umfmap(file_name)
      call sgocls('DclFillMap')
    end subroutine
!-------------------------------------------------
end module
