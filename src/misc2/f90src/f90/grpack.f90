!----------------------------------------------
!  GRpack Module
!----------------------------------------------

module grpack
  use dcl_common
  contains
!-------------------------------------------------------------
    function DclSelectDevice()
      integer :: DclSelectDevice

      call prcopn('DclSelectDevice')
      write(*,*) ' workstation id (i)  ? ;'
      call sgpwsn
      read(*,*) DclSelectDevice
      call prccls('DclSelectDevice')

    end function
!-------------------------------------------------------------
    subroutine DclOpenGraphics(ws_id)
      integer, intent(in), optional :: ws_id

      call prcopn('DclOpenGraphics')
      if(present(ws_id)) then
        iws = ws_id
      else
        write(*,*) ' workstation id (i)  ? ;'
        call sgpwsn
        read(*,*) iws
      end if

      call gropn(iws)
      call prccls('DclOpenGraphics')

    end subroutine
!-------------------------------------------------------------
    subroutine DclNewFrame
      call prcopn('DclNewFrame')
      call grfrm
      call prccls('DclNewFrame')
    end subroutine
!-------------------------------------------------------------
    subroutine DclNewFig
      call prcopn('DclNewFig')
      call grfig
      call prccls('DclNewFig')
    end subroutine
!-------------------------------------------------------------
    subroutine DclCloseGraphics
      call prcopn('DcCloseGraphics')
      call grcls
      call prccls('DcCloseGraphics')
    end subroutine
!-------------------------------------------------------------
    subroutine DclSetTransFunction
      call prcopn('DclSetTransFunction')
      call grstrf
      call prccls('DclSetTransFunction')
    end subroutine
!-------------------------------------------------------------
    subroutine DclSetTransNumber (num)
      integer, intent(in) :: num

      call prcopn('DclSetTransNumber')
      call grstrn(num)
      call prccls('DclSetTransNumber')
    end subroutine
!-------------------------------------------------------------
    subroutine DclSetMapProjectionAngle (longitude, latitude, rotation)
      real, intent(in), optional :: latitude, longitude, rotation
      real                       :: lat0,     lon0,      rot0

      call prcopn('DclSetMapProjectionAngle')
      call glrget('RUNDEF', rundef)

      if(present(longitude)) then; lon0 = longitude
                             else; lon0 = rundef
      end if

      if(present(latitude))  then; lat0 = latitude
                             else; lat0 = rundef
      end if
      
      if(present(rotation))  then; rot0 = rotation
                             else; rot0 = rundef
      end if

      call grsmpl(lon0, lat0, rot0)
      call prccls('DclSetMapProjectionAngle')

    end subroutine
!-------------------------------------------------------------
    subroutine DclSetSimilarity (factor,xoffset,yoffset)
      real, intent(in), optional :: factor, xoffset, yoffset
      real                       :: fac0,   xoff0,   yoff0

      call prcopn('DclSetSimilarity')
      call glrget('RUNDEF', rundef)

      if(present(factor))  then; fac0 = factor
                           else; fac0 = rundef
      end if

      if(present(xoffset)) then; xoff0 = xoffset
                           else; xoff0 = rundef
      end if
      
      if(present(yoffset)) then; yoff0 = yoffset
                           else; yoff0 = rundef
      end if

      call grssim(fac0, xoff0, yoff0)
      call prccls('DclSetSimilarity')
    end subroutine
!-------------------------------------------------------------
    subroutine DclSetMapProjectionWindow(xmin, xmax, ymin, ymax)
      real, intent(in), optional :: xmin,  xmax,  ymin,  ymax
      real                       :: xmin0, xmax0, ymin0, ymax0

      call prcopn('DclSetMapProjectionWindow')
      call glrget('RUNDEF', rundef)

      if(present(xmin)) then; xmin0 = xmin
                        else; xmin0 = rundef
      end if
      
      if(present(xmax)) then; xmax0 = xmax
                        else; xmax0 = rundef
      end if

      if(present(ymin)) then; ymin0 = ymin
                        else; ymin0 = rundef
      end if

      if(present(ymax)) then; ymax0 = ymax
                        else; ymax0 = rundef
      end if

      call grstxy(xmin0, xmax0, ymin0, ymax0)
      call prccls('DclSetMapProjectionWindow')
    end subroutine
!-------------------------------------------------------------
    subroutine DclSetViewPort(xmin,xmax,ymin,ymax)
      real, intent(in), optional :: xmin,  xmax,  ymin,  ymax
      real                       :: xmin0, xmax0, ymin0, ymax0

      call prcopn('DclSetViewPort')
      call glrget('RUNDEF', rundef)

      if(present(xmin)) then; xmin0 = xmin
                        else; xmin0 = rundef
      end if
      
      if(present(xmax)) then; xmax0 = xmax
                        else; xmax0 = rundef
      end if

      if(present(ymin)) then; ymin0 = ymin
                        else; ymin0 = rundef
      end if

      if(present(ymax)) then; ymax0 = ymax
                        else; ymax0 = rundef
      end if

      call grsvpt(xmin0, xmax0, ymin0, ymax0)
      call prccls('DclSetViewPort')
    end subroutine
!-------------------------------------------------------------
    subroutine DclSetWindow(xmin,xmax,ymin,ymax)
      real, intent(in), optional :: xmin,  xmax,  ymin,  ymax
      real                       :: xmin0, xmax0, ymin0, ymax0

      call prcopn('DclSetWindow')
      call glrget('RUNDEF', rundef)

      if(present(xmin)) then; xmin0 = xmin
                        else; xmin0 = rundef
      end if
      
      if(present(xmax)) then; xmax0 = xmax
                        else; xmax0 = rundef
      end if

      if(present(ymin)) then; ymin0 = ymin
                        else; ymin0 = rundef
      end if

      if(present(ymax)) then; ymax0 = ymax
                        else; ymax0 = rundef
      end if
      
      call grswnd(xmin0, xmax0, ymin0, ymax0)
      call prccls('DclSetWindow')
    end subroutine

end module
