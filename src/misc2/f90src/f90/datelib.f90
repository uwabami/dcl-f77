!-------------------------------------------------
!  DATElib Module
!-------------------------------------------------
module datelib
  use dcl_common
  
contains
!----------------------------------------------------------------
    function DclGetDate()
      type(dcl_date) :: DclGetDate

      call prcopn('DclGetDate')
      call dateq3(DclGetDate%year,DclGetDate%month,DclGetDate%day) 
      call prccls('DclGetDate')
    end function
!----------------------------------------------------------------
    function DclAddDate(date, n) 
      type(dcl_date) :: DclAddDate
      type(dcl_date), intent(in) :: date
      integer,        intent(in) :: n 

      call prcopn('DclAddDate')
      call datef3(n, date%year,      date%month,      date%day, &
            &        DclAddDate%year,DclAddDate%month,DclAddDate%day)
      call prccls('DclAddDate')
    end function
!----------------------------------------------------------------
    function DclDiffDate(date1, date2)
      integer                    :: DclDiffDate
      type(dcl_date), intent(in) :: date1, date2
      
      call prcopn('DclDiffDate')
      DclDiffDate = ndate3(date1%year, date1%month, date1%day, &
                  &        date2%year, date2%month, date2%day)
      call prccls('DclDiffDate')
    end function
!----------------------------------------------------------------
    subroutine DclFormatDate(cform,date)
      type(dcl_date),   intent(in)    :: date
      character(len=*), intent(inout) :: cform    !日付のフォーマット

      call prcopn('DclFormatDate')
      call datec3(cform,date%year,date%month,date%day)
      call prccls('DclFormatDate')
    end subroutine
!----------------------------------------------------------------
    function DclDayOfWeek(date)
      integer ::  DclDayOfWeek
      type(dcl_date),   intent(in)    :: date

      call prcopn('DclDayOfWeek')
      DclDayOfWeek = iweek3(date%year,date%month,date%day)  
      call prccls('DclDayOfWeek')
    end function
!----------------------------------------------------------------
    function DclLengthOfMonth(year,month)  !year年month月は何日あるかを返す．
      integer,   intent(in) :: year                 !年
      integer,   intent(in) :: month                 !月
      integer               :: DclLengthOfMonth 

      call prcopn('DclLengthOfMonth')
      DclLengthOfMonth = ndmon(year,month) 
      call prccls('DclLengthOfMonth')
    end function
!----------------------------------------------------------------
    function DclLengthOfYear(year)                !year年は何日あるかを返す．
      integer, intent(in) :: year                 !年
      integer             :: DclLengthOfYear    !指定した年の日数を与える関数値

      call prcopn('DclLengthOfYear')
      DclLengthOfYear = ndyear(year)  
      call prccls('DclLengthOfYear')
    end function
!----------------------------------------------------------------
end module
