!-------------------------------------------------
!  MAPlib Module
!-------------------------------------------------
module maplib
  use dcl_common

  contains
!------------------------------------------------- 正距円筒図法
  function DclCylindrical_F(point)  
    type(map), intent(in) :: point
    type(cartesian)       :: DclCylindrical_F

    call prcopn('DclCylindrical_F')
    call mpfcyl(point%lon, point%lat, DclCylindrical_F%x, DclCylindrical_F%y)
    call prccls('DclCylindrical_F')
  end function
!------------------------------------------------- メルカトール図法
  function DclMercator_F(point)
    type(map), intent(in) :: point
    type(cartesian)       :: DclMercator_F

    call prcopn('DclMercator_F')
    call mpfmer(point%lon,point%lat,DclMercator_F%x,DclMercator_F%y)
    call prccls('DclMercator_F')
  end function
!------------------------------------------------- モルワイデ図法
  function DclMollweide_F(point)
    type(map), intent(in) :: point
    type(cartesian)       :: DclMollweide_F

    call prcopn('DclMollweide_F')
    call mpfmwd(point%lon,point%lat,DclMollweide_F%x,DclMollweide_F%y)
    call prccls('DclMollweide_F')
  end function
!------------------------------------------------- モルワイデ図法(もどき)
  function DclMollweideLike_F(point)
    type(map), intent(in) :: point
    type(cartesian)       :: DclMollweideLike_F

    call prcopn('DclMollweideLike_F')
    call mpfmwl(point%lon,point%lat,DclMollweideLike_F%x,DclMollweideLike_F%y)
    call prccls('DclMollweideLike_F')
  end function
!------------------------------------------------- ハンメル図法
  function DclHammer_F(point)
    type(map), intent(in) :: point
    type(cartesian)       :: DclHammer_F

    call prcopn('DclHammer_F')
    call mpfhmr(point%lon,point%lat,DclHammer_F%x,DclHammer_F%y)
    call prccls('DclHammer_F')
  end function
!------------------------------------------------- エッケルト第6図法
  function DclEckert6_F(point)
    type(map), intent(in) :: point
    type(cartesian)       :: DclEckert6_F

    call prcopn('DclEckert6_F')
    call mpfek6(point%lon,point%lat,DclEckert6_F%x,DclEckert6_F%y) 
    call prccls('DclEckert6_F')
  end function
!------------------------------------------------- 北田楕円図法
  function DclKitada_F(point)
    type(map), intent(in) :: point
    type(cartesian)       :: DclKitada_F

    call prcopn('DclKitada_F')
    call mpfktd(point%lon,point%lat,DclKitada_F%x,DclKitada_F%y)
    call prccls('DclKitada_F')
  end function
!------------------------------------------------- ランベルト正積円錐図法
  function DclConicalA_F(point)
    type(map), intent(in) :: point
    type(cartesian)       :: DclConicalA_F

    call prcopn('DclConicalA_F')
    call mpfcoa(point%lon,point%lat,DclConicalA_F%x,DclConicalA_F%y)
    call prccls('DclConicalA_F')
  end function
!------------------------------------------------- 正距円錐図法
  function DclConical_F(point)
    type(map), intent(in) :: point
    type(cartesian)       :: DclConical_F

    call prcopn('DclConical_F')
    call mpfcon(point%lon,point%lat,DclConical_F%x,DclConical_F%y)
    call prccls('DclConical_F')
  end function
!------------------------------------------------- ランベルト正角円錐図法
  function DclConicalC_F(point)
    type(map), intent(in) :: point
    type(cartesian)       :: DclConicalC_F

    call prcopn('DclConicalC_F')
    call mpfcoc(point%lon,point%lat,DclConicalC_F%x,DclConicalC_F%y)
    call prccls('DclConicalC_F')
  end function
!------------------------------------------------- ボンヌ図法
  function DclBonnes_F(point)
    type(map), intent(in) :: point
    type(cartesian)       :: DclBonnes_F

    call prcopn('DclBonnes_F')
    call mpfbon(point%lon,point%lat,DclBonnes_F%x,DclBonnes_F%y) 
    call prccls('DclBonnes_F')
  end function
!------------------------------------------------- 正射図法/satellite view
  function DclOrthographic_F(point)
    type(map), intent(in) :: point
    type(cartesian)       :: DclOrthographic_F

    call prcopn('DclOrthographic_F')
    call mpfotg(point%lon,point%lat,DclOrthographic_F%x,DclOrthographic_F%y) 
    call prccls('DclOrthographic_F')
  end function
!------------------------------------------------- ポーラーステレオ図法
  function DclPolarStereo_F(point)
    type(map), intent(in) :: point
    type(cartesian)       :: DclPolarStereo_F

    call prcopn('DclPolarStereo_F')
    call mpfpst(point%lon,point%lat,DclPolarStereo_F%x,DclPolarStereo_F%y) 
    call prccls('DclPolarStereo_F')
  end function
!------------------------------------------------- 正距方位図法
  function DclAzimuthal_F(point)
    type(map), intent(in) :: point
    type(cartesian)       :: DclAzimuthal_F

    call prcopn('DclAzimuthal_F')
    call mpfazm(point%lon,point%lat,DclAzimuthal_F%x,DclAzimuthal_F%y)
    call prccls('DclAzimuthal_F')
  end function
!------------------------------------------------- ランベルト正積方位図法
  function DclAzimuthalA_F(point)
    type(map), intent(in) :: point
    type(cartesian)       :: DclAzimuthalA_F

    call prcopn('DclAzimuthalA_F')
    call mpfaza(point%lon,point%lat,DclAzimuthalA_F%x,DclAzimuthalA_F%y)
    call prccls('DclAzimuthalA_F')
  end function
    
!===========================================================
!------------------------------------------------- 正距円筒図法
  function DclCylindrical_B(point) 
    type(cartesian), intent(in) :: point
    type(map)                   :: DclCylindrical_B

    call prcopn('DclCylindrical_B')
    call mpicyl(point%x,point%y,DclCylindrical_B%lon,DclCylindrical_Blat)
    call prccls('DclCylindrical_B')
  end function
!------------------------------------------------- メルカトール図法
  function DclMercator_B(point)
    type(cartesian), intent(in) :: point
    type(map)                   :: DclMercator_B

    call prcopn('DclMercator_B')
    call mpimer(point%x,point%y,DclMercator_B%lon,DclMercator_B%lat)
    call prccls('DclMercator_B')
  end function
!------------------------------------------------- モルワイデ図法
  function DclMollweide_B(point)
    type(cartesian), intent(in) :: point
    type(map)                   :: DclMollweide_B

    call prcopn('DclMollweide_B')
    call mpimwd(point%x,point%y,DclMollweide_B%lon,DclMollweide_B%lat)
    call prccls('DclMollweide_B')
  end function
!------------------------------------------------- モルワイデ図法（もどき）
  function DclMollweideLike_B(point)
    type(cartesian), intent(in) :: point
    type(map)                   :: DclMollweideLike_B

    call prcopn('DclMollweideLike_B')
    call mpimwl(point%x,point%y,DclMollweideLike_B%lon,DclMollweideLike_B%lat)
    call prccls('DclMollweideLike_B')
  end function
!------------------------------------------------- ハンメル図法
  function DclHammer_B(point)
    type(cartesian), intent(in) :: point
    type(map)                   :: DclHammer_B

    call prcopn('DclHammer_B')
    call mpihmr(point%x,point%y,DclHammer_B%lon,DclHammer_B%lat)
    call prccls('DclHammer_B')
  end function
 !------------------------------------------------- エッケルト第6図法
  function DclEckert6_B(point)
    type(cartesian), intent(in) :: point
    type(map)                   :: DclEckert6_B

    call prcopn('DclEckert6_B')
    call mpiek6(point%x,point%y,DclEckert6_B%lon,DclEckert6_B%lat)
    call prccls('DclEckert6_B')
  end function
!------------------------------------------------- 北田楕円図法
  function DclKitada_B(point)
    type(cartesian), intent(in) :: point
    type(map)                   :: DclKitada_B

    call prcopn('DclKitada_B')
    call mpiktd(point%x,point%y,DclKitada_B%lon,DclKitada_B%lat)
    call prccls('DclKitada_B')
  end function
!------------------------------------------------- 正距円錐図法
  function DclConical_B(point)
    type(cartesian), intent(in) :: point
    type(map)                   :: DclConical_B

    call prcopn('DclConical_B')
    call mpicon(point%x,point%y,DclConical_B%lon,DclConical_B%lat)
    call prccls('DclConical_B')
  end function
!------------------------------------------------- ランベルト正積円錐図法
  function DclConicalA_B(point)
    type(cartesian), intent(in) :: point
    type(map)                   :: DclConicalA_B

    call prcopn('DclConicalA_B')
    call mpicoa(point%x,point%y,DclConicalA_B%lon,DclConicalA_B%lat)
    call prccls('DclConicalA_B')
  end function
!------------------------------------------------- ランベルト正角円錐図法
  function DclConicalC_B(point)
    type(cartesian), intent(in) :: point
    type(map)                   :: DclConicalC_B

    call prcopn('DclConicalC_B')
    call mpicoc(point%x,point%y,DclConicalC_B%lon,DclConicalC_B%lat)
    call prccls('DclConicalC_B')
  end function
!------------------------------------------------- ボンヌ図法
  function DclBonnes_B(point)
    type(cartesian), intent(in) :: point
    type(map)                   :: DclBonnes_B

    call prcopn('DclBonnes_B')
    call mpibon(point%x,point%y,DclBonnes_B%lon,DclBonnes_B%lat) 
    call prccls('DclBonnes_B')
  end function
!------------------------------------------------- 正射図法および satellite view
  function DclOrthographic_B(point) 
    type(cartesian), intent(in) :: point
    type(map)                   :: DclOrthographic_B

    call prcopn('DclOrthographic_B')
    call mpiotg(point%x,point%y,DclOrthographic_B%lon,DclOrthographic_B%lat)  
    call prccls('DclOrthographic_B')
  end function
!------------------------------------------------- ポーラーステレオ図法
  function DclPolarStereo_B(point)
    type(cartesian), intent(in) :: point
    type(map)                   :: DclPolarStereo_B

    call prcopn('DclPolarStereo_B')
    call mpipst(point%x,point%y,DclPolarStereo_B%lon,DclPolarStereo_B%lat) 
    call prccls('DclPolarStereo_B')
  end function
!------------------------------------------------- 正距方位図法
  function DclAzimuthal_B(point)
    type(cartesian), intent(in) :: point
    type(map)                   :: DclAzimuthal_B

    call prcopn('DclAzimuthal_B')
    call mpiazm(point%x,point%y,DclAzimuthal_B%lon,DclAzimuthal_B%lat)
    call prccls('DclAzimuthal_B')
  end function
!------------------------------------------------- ランベルト正積方位図法
  function DclAzimuthalA_B(point)
    type(cartesian), intent(in) :: point
    type(map)                   :: DclAzimuthalA_B

    call prcopn('DclAzimuthalA_B')
    call mpiaza(point%x,point%y,DclAzimuthalA_B%lon,DclAzimuthalA_B%lat) 
    call prccls('DclAzimuthalA_B')
  end function

!========================================================

  subroutine DclSetConical(lat)         !標準緯線の指定
    real,      intent(in) :: lat

    call prcopn('DclSetConical')
    call mpscon(lat)
    call prccls('DclSetConical')
  end subroutine
    
  subroutine DclSetConicalA(lat)        !標準緯線の指定
    real,      intent(in) :: lat

    call prcopn('DclSetConicalA')
    call mpscoa(lat) 
    call prccls('DclSetConicalA')
  end subroutine
    
  subroutine DclSetConicalC(lat1,lat2) !標準緯線の指定
    real,      intent(in) :: lat1,lat2 
 
    call prcopn('DclSetConicalC')
    call mpscoc(lat1,lat2)
    call prccls('DclSetConicalC')
  end subroutine
    
  subroutine DclSetBonnes(lat)          !標準緯線の指定
    real,      intent(in) :: lat

    call prcopn('DclSetBonnes')
    call mpsbon(lat)
    call prccls('DclSetBonnes')
  end subroutine
    
  subroutine DclSetOrthographic(rsat)    !軌道半径の設定
    real,      intent(in) :: rsat

    call prcopn('DclSetOrthographic')
    call mpsotg(rsat)
    call prccls('DclSetOrthographic')
  end subroutine
    
end module

