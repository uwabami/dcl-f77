!-------------------------------------------------
!  CHGlib Module
!-------------------------------------------------
module chglib
  use dcl_common
  contains

    subroutine DclToUpper(ch)                         !文字列を大文字化化する．
      character(len=*), intent(inout) :: ch       !処理する文字列

      call prcopn('DclToUpper')
      call cupper(ch) 
      call prccls('DclToUpper')
    end subroutine
      
    subroutine DclToLower(ch)                         !文字列を小文字化する．
      character(len=*), intent(inout) :: ch       !処理する文字列

      call prcopn('DclToLower')
      call clower(ch) 
      call prccls('DclToLower')
    end subroutine

end module
