!-------------------------------------------------
!  RFAlib Module
!-------------------------------------------------
module rfblib
  use dcl_common
  contains

    function DclGetPRD(x,y)                        !内積を求める
      real, intent(in), dimension(:) :: x
      real, intent(in), dimension(:) :: y
      real                           :: DclGetPRD

      call prcopn('DclGetPRD')
      DclGetPRD = rprd(x,y,size(x),1,1)
      call prccls('DclGetPRD')
    end function
      
    function DclGetCOV(x,y)                        !共分散を求める
      real, intent(in), dimension(:) :: x
      real, intent(in), dimension(:) :: y
      real                           :: DclGetCOV

      call prcopn('DclGetCOV')
      DclGetCOV = rcov(x,y,size(x),1,1)
      call prccls('DclGetCOV')
    end function
      
    function DclGetCOR(x,y)                        !相関係数を求める
      real, intent(in), dimension(:) :: x
      real, intent(in), dimension(:) :: y
      real                           :: DclGetCOR

      call prcopn('DclGetCOR')
      DclGetCOR = rcor(x,y,size(x),1,1)
      call prccls('DclGetCOR')
    end function
      
end module
