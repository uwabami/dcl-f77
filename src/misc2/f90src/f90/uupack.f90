!-------------------------------------------------
!     UUpack module
!-------------------------------------------------
module uupack
  use dcl_common
  contains
!-----------------------------------------------

    subroutine DclSetErrorBarLineType(type)  !エラーバーのラインタイプを指定する．  
      integer,   intent(in) :: type

      call prcopn('DclSetErrorBarLineType')
      call uusebt(type)
      call prccls('DclSetErrorBarLineType')
    end subroutine

    function DclGetErrorBarLineType()   
      integer  :: DclGetErrorBarLineType

      call prcopn('DclGetErrorBarLineType')
      call uuqebt(DclGetErrorBarLineType)
      call prccls('DclGetErrorBarLineType')
    end function

    subroutine DclSetErrorBarLineIndex(index)  !エラーバーのラインインデクスを指定する．
      integer,   intent(in) :: index

      call prcopn('DclSetErrorBarLineIndex')
      call uusebi(index) 
      call prccls('DclSetErrorBarLineIndex')
    end subroutine

    function DclGetErrorBarLineIndex()
      integer  :: DclGetErrorBarLineIndex

      call prcopn('DclGetErrorBarLineIndex')
      call uuqebi(DclGetErrorBarLineIndex) 
      call prccls('DclGetErrorBarLineIndex')
    end function

    subroutine DclSetErrorBarWidth(width)  !エラーバーの横幅を指定する．
      real,      intent(in) :: width

      call prcopn('DclSetErrorBarWidth')
      call uusebs(width)
      call prccls('DclSetErrorBarWidth')
    end subroutine
    
    function DclGetErrorBarWidth()
      real     :: DclGetErrorBarWidth

      call prcopn('DclGetErrorBarWidth')
      call uuqebs(DclGetErrorBarWidth)
      call prccls('DclGetErrorBarWidth')
    end function
    
    subroutine DclSetBarWidth(width)  !棒グラフの横巾を指定する．
      real,      intent(in) :: width

      call prcopn('DclSetBarWidth')
      call uusbrs(width)
      call prccls('DclSetBarWidth')
    end subroutine

    function DclGetBarWidth()
      real     :: DclGetBarWidth

      call prcopn('DclGetBarWidth')
      call uuqbrs(DclGetBarWidth)
      call prccls('DclGetBarWidth')
    end function

    subroutine DclSetAreaPattern(pattern1,pattern2)  ! 内部領域を塗るトーンパターンを指定する．
      integer,   intent(in) :: pattern1, pattern2

      call prcopn('DclSetAreaPattern')
      call uusarp(pattern1,pattern2)
      call prccls('DclSetAreaPattern')
    end subroutine

    subroutine DclGetAreaPattern(pattern1,pattern2)
      integer,   intent(out) :: pattern1, pattern2

      call prcopn('DclGetAreaPattern')
      call uuqarp(pattern1,pattern2)
      call prccls('DclGetAreaPattern')
    end subroutine

    subroutine DclSetFrameType(type)  !枠の属性を指定する．
      integer,   intent(in) :: type

      call prcopn('DclSetFrameType')
      call uusfrt(type)
      call prccls('DclSetFrameType')
    end subroutine

    function DclGetFrameType()
      integer  :: DclGetFrameType

      call prcopn('DclGetFrameType')
      call uuqfrt(DclGetFrameType)
      call prccls('DclGetFrameType')
    end function

    subroutine DclSetFrameIndex(index)  !枠のラインインデクスを指定する．
      integer,   intent(in) :: index

      call prcopn('DclSetFrameIndex')
      call uusfri(index)
      call prccls('DclSetFrameIndex')
    end subroutine

    function DclGetFrameIndex()
      integer :: DclGetFrameIndex

      call prcopn('DclGetFrameIndex')
      call uuqfri(DclGetFrameIndex)
      call prccls('DclGetFrameIndex')
    end function
    
end module
