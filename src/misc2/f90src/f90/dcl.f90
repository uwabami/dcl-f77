module dcl
  use dcl_parm

!--- math1 ---
  use syslib
  use oslib
  use lrllib
  use blklib
  use gnmlib
  use intlib
  use indxlib
  use rfalib
  use rfblib
  use ctrlib
  use maplib

!--- math2 ---
  use fftlib
  use shtrlib
  use intrlib
  use rnmlib

!--- misc1 ---
  use chklib
  use chglib
  use datelib
  use timelib

!--- grph1 ---
  use sgpack
  use scpack
  use slpack

!--- grph2 ---
  use grpack
!  use uxpack
!  use uypack
  use uzpack
!  use ulpack
  use uupack
  use uhpack
  use uvpack
  use uspack
  use udpack
  use uepack
  use ugpack
  use uwpack
  use umpack
!  use ucpack

end module dcl
