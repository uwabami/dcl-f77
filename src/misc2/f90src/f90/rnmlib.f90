!-------------------------------------------------
!  RNMlib Module
!-------------------------------------------------
module rnmlib
  use dcl_common
  contains

    function DclRunningMean(x,length)  !移動平均を計算する． ．
      real, intent(in), dimension(:) :: x !移動平均を計算する実数型の配列
      integer, intent(in)            :: length
      real,      dimension(size(x)) :: DclRunningMean !計算結果を納める実数型の配列

      call prcopn('DclRunningMean')
      call vrrnm(x,DclRunningMean,size(x),1,1,length)
      call prccls('DclRunningMean')
    end function

end module
