!-------------------------------------------------
!  UZpack module
!-------------------------------------------------
module uzpack
  use dcl_common
  contains

!------------------------------ !文字の大きさ等をすべて何倍かする．
    subroutine DclSetAxisFactor(fact)
      real,      intent(in) :: fact       !倍率

      call prcopn('DclSetAxisFactor')
      call uzfact(fact) 
      call prccls('DclSetAxisFactor')
    end subroutine
end module

