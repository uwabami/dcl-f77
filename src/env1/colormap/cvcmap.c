/*
 * program for converting from namedcolor to colormap   (92/11/03)
 *
 *    Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
 *
 */

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <stdio.h>

int main(void)
{
    int n, ncolor;
    Display *d;
    Colormap cm;
    XColor c1, c2;
    char color[16];

    d = XOpenDisplay (NULL);
    cm = DefaultColormap (d, 0);

    scanf ("%d", &ncolor);
    printf (" %d : no._of_colors\n", ncolor);
    for (n = 0; n < ncolor; n++){
	scanf ("%s", color);
	XAllocNamedColor (d, cm, color, &c1, &c2);
	printf ("%6d%6d%6d : %-s\n",
		c1.red, c1.green, c1.blue, color);
    }
    return(0);
}
