*-----------------------------------------------------------------------
*     PROGRAM FOR GENERATING COLORMAP
*
*       WRITTEN BY K. ISHIOKA, MODIFIED BY M. SHIOTANI (94/05/07)
*
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM CMAP02

      CHARACTER CCOLOR*6
      PARAMETER(NC=100,NCSTEP=4369)

      CCOLOR=' : C**'
      
  100 FORMAT(I3,A15)
  200 FORMAT(3I6,A6)

      WRITE(*,100) NC,': NO._OF_COLORS'
      WRITE(*,200) 65535,65535,65535,' : C00'
      WRITE(*,200)     0,    0,    0,' : C01'
      WRITE(*,200) 65535,    0,    0,' : C02'
      WRITE(*,200)     0,65535,    0,' : C03'
      WRITE(*,200)     0,    0,65535,' : C04'
      WRITE(*,200) 65535,65535,    0,' : C05'
      WRITE(*,200) 56797,41120,56797,' : C06'
      WRITE(*,200)     0,    0,32896,' : C07'
      WRITE(*,200) 65535,49344,52171,' : C08'
      WRITE(*,200)     0,65535,65535,' : C09'      

      DO 10 I=10,99

      WRITE(CCOLOR(5:6),'(I2)') I

      IF(I.LE.20) THEN
      IR=(20-I)*65535/10
      IG=0
      IB=(20-10)*NCSTEP
      WRITE(*,200) IR,IG,IB,CCOLOR

      ELSE IF(I.LE.25) THEN
      IR=0
      IG=0
      IB=(I-10)*NCSTEP
      WRITE(*,200) IR,IG,IB,CCOLOR

      ELSE IF(I.LE.40) THEN
      IR=0
      IG=(I-25)*NCSTEP
      IB=15*NCSTEP
      WRITE(*,200) IR,IG,IB,CCOLOR

      ELSE IF(I.LE.55) THEN
      IR=0
      IG=15*NCSTEP
      IB=(55-I)*NCSTEP
      WRITE(*,200) IR,IG,IB,CCOLOR

      ELSE IF(I.LE.70) THEN
      IR=(I-55)*NCSTEP
      IG=15*NCSTEP
      IB=0
      WRITE(*,200) IR,IG,IB,CCOLOR

      ELSE IF(I.LE.85) THEN
      IR=15*NCSTEP
      IG=(85-I)*NCSTEP
      IB=0
      WRITE(*,200) IR,IG,IB,CCOLOR

      ELSE IF(I.LE.99) THEN
      IR=15*NCSTEP
      IG=(I-85)*NCSTEP
      IB=(I-85)*NCSTEP
      WRITE(*,200) IR,IG,IB,CCOLOR
      END IF

   10 CONTINUE 

      STOP
      END
