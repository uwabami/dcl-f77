*-----------------------------------------------------------------------
*     PROGRAM FOR MAPDATA SUBTRUCTION                        (94/05/07)
*-----------------------------------------------------------------------
*     Copyright (C) 2000 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM RMAPD1

      PARAMETER (IU1=11,IU2=21)

      REAL      FLIM(4),PTX1(20000),PTY1(20000),PTX2(20000),PTY2(20000)
      CHARACTER CDSNI*64,CDSNO*64


      READ(*,*) CDSNI,CDSNO

      OPEN(IU1,FILE=CDSNI,FORM='FORMATTED',STATUS='OLD')
      REWIND(IU1)
      OPEN(IU2,FILE=CDSNO,FORM='FORMATTED',STATUS='NEW')
      REWIND(IU2)

   10 CONTINUE
        READ(IU1,'(2I8,4F8.3)',IOSTAT=IOS) NPTS,IGID,(FLIM(I),I=1,4)
        NPT1=NPTS/2
        IF (IOS.EQ.0) THEN
          IF (NPT1.GE.1) THEN
            READ(IU1,'(10F8.3)') (PTX1(I),PTY1(I),I=1,NPT1)
          END IF
          CALL RMPSUB(PTX1,PTY1,NPT1,PTX2,PTY2,NPT2)
          CALL RMPSUB(PTX2,PTY2,NPT2,PTX1,PTY1,NPT1)
          WRITE(IU2,'(2I8,4F8.3)') NPT1*2,IGID,(FLIM(I),I=1,4)
          WRITE(IU2,'(10F8.3)') (PTX1(I),PTY1(I),I=1,NPT1)
        END IF
      IF (IOS.EQ.0) GO TO 10

      CLOSE(IU1)
      CLOSE(IU2)

      END
*-----------------------------------------------------------------------
      SUBROUTINE RMPSUB(PTX1,PTY1,NPT1,PTX2,PTY2,NPT2)

      PARAMETER (CXLIM=0.5)

      REAL      PTX1(*),PTY1(*),PTX2(*),PTY2(*)


      IF (NPT1.LT.2) THEN
        WRITE(*,*) 'PMAPD1/RMPSUB - ERROR IN POINTS.'
        STOP
      END IF

      PTX2(1)=PTX1(1)
      PTY2(1)=PTY1(1)

      IF (NPT1.LE.2) THEN
        NPT2=2
        PTX2(NPT2)=PTX1(NPT1)
        PTY2(NPT2)=PTY1(NPT1)
        RETURN
      END IF

      NX=1
      NPT2=1

   10 CONTINUE

        N0=NX
        N1=NX+1
        N2=NX+2
        DX1=PTX1(N1)-PTX1(N0)
        DX2=PTX1(N2)-PTX1(N1)
        DY1=PTY1(N1)-PTY1(N0)
        DY2=PTY1(N2)-PTY1(N1)
        CX=(DX1*DX2+DY1*DY2)/(SQRT(DX1**2+DY1**2)*SQRT(DX2**2+DY2**2))
        IF (CX.GT.CXLIM) THEN
          NX=N2
        ELSE
          NX=N1
        END IF
        NPT2=NPT2+1
        PTX2(NPT2)=PTX1(NX)
        PTY2(NPT2)=PTY1(NX)

      IF (NX+2.LE.NPT1) GO TO 10

      IF (NX.NE.NPT1) THEN
        NPT2=NPT2+1
        PTX2(NPT2)=PTX1(NPT1)
        PTY2(NPT2)=PTY1(NPT1)
      END IF

      END
