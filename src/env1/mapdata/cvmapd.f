*-----------------------------------------------------------------------
*     PROGRAM FOR MAPDATA CONVERSION                         (94/05/07)
*-----------------------------------------------------------------------
*     Copyright (C) 2000 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM CVMAPD

      PARAMETER (IU1=11,IU2=21)

      REAL      FLIM(4),PNTS(20000)
      CHARACTER CDSNI*64,CDSNO*64

      READ(*,*) CDSNI,CDSNO

      OPEN(IU1,FILE=CDSNI,FORM='FORMATTED',STATUS='OLD')
      REWIND(IU1)
      OPEN(IU2,FILE=CDSNO,FORM='UNFORMATTED',STATUS='NEW')
      REWIND(IU2)

   10 CONTINUE
        READ(IU1,'(2I8,4F8.3)',IOSTAT=IOS) NPTS,IGID,(FLIM(I),I=1,4)
        IF (IOS.EQ.0) THEN
          IF (NPTS.GE.2) THEN
            READ(IU1,'(10F8.3)') (PNTS(I),I=1,NPTS)
          END IF
          WRITE(IU2) NPTS,IGID,(FLIM(I),I=1,4),(PNTS(I),I=1,NPTS)
        END IF
      IF (IOS.EQ.0) GO TO 10

      CLOSE(IU1)
      CLOSE(IU2)

      END
