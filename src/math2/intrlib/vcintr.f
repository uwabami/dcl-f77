*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE VCINTR(CX,N,JX)

      COMPLEX CX(*),CMISS,CD,C1
      LOGICAL LFLAG


      CALL GLRGET('RMISS',RMISS)
      CMISS=CMPLX(RMISS,RMISS)

      LFLAG=.FALSE.

      KX=1-JX
      DO 15 J=1,N
        KX=KX+JX
        IF (LFLAG) THEN
          IF (CX(KX).EQ.CMISS) THEN
            IF (.NOT.(CX(KX-JX).EQ.CMISS)) THEN
              NMISS=1
            ELSE
              NMISS=NMISS+1
            END IF
          ELSE
            IF (CX(KX-JX).EQ.CMISS) THEN
              C1=CX(KX-(NMISS+1)*JX)
              CD=(CX(KX)/C1)**(1.0/(NMISS+1))
              DO 10 NN=1,NMISS
                CX(KX-(NMISS+1-NN)*JX)=C1*(CD**NN)
   10         CONTINUE
            END IF
          END IF
        ELSE
          IF (.NOT.(CX(KX).EQ.CMISS)) THEN
            LFLAG=.TRUE.
          END IF
        END IF
   15 CONTINUE

      END
