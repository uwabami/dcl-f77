*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE VRINTR(RX,N,JX)

      REAL    RX(*)
      LOGICAL LFLAG


      CALL GLRGET('RMISS',RMISS)

      LFLAG=.FALSE.

      KX=1-JX
      DO 15 J=1,N
        KX=KX+JX
        IF (LFLAG) THEN
          IF (RX(KX).EQ.RMISS) THEN
            IF (RX(KX-JX).NE.RMISS) THEN
              NMISS=1
            ELSE
              NMISS=NMISS+1
            END IF
          ELSE
            IF (RX(KX-JX).EQ.RMISS) THEN
              X1=RX(KX-(NMISS+1)*JX)
              XD=(RX(KX)-X1)/(NMISS+1)
              DO 10 NN=1,NMISS
                RX(KX-(NMISS+1-NN)*JX)=X1+XD*NN
   10         CONTINUE
            END IF
          END IF
        ELSE
          IF (RX(KX).NE.RMISS) THEN
            LFLAG=.TRUE.
          END IF
        END IF
   15 CONTINUE

      END
