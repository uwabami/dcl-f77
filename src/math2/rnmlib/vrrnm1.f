*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE VRRNM1(RX,RY,N,JX,JY,NB)

      REAL      RX(*),RY(*)


      IF (.NOT.(1.LE.NB .AND. NB.LE.N)) THEN
        CALL MSGDMP('E','VRRNM1','AVERAGING LENGTH IS INVALID.')
      END IF
      IF (MOD(NB,2).EQ.0) THEN
        CALL MSGDMP('E','VRRNM1','AVERAGING LENGTH IS EVEN NUMBER.')
      END IF

      CALL GLRGET('RMISS',RMISS)

      NB2=NB/2
      KX=1-JX
      KY=1-JY
      DO 10 J=1,N
        KX=KX+JX
        KY=KY+JY
        IF (1+NB2.LE.J .AND. J.LE.N-NB2) THEN
          RY(KY)=RAVE1(RX(KX-JX*NB2),NB,JX)
        ELSE
          RY(KY)=RMISS
        END IF
   10 CONTINUE

      END
