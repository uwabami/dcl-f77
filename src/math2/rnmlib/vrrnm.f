*-----------------------------------------------------------------------
*     VRRNM
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE VRRNM(RX,RY,N,JX,JY,NB)

      REAL      RX(*),RY(*)

      LOGICAL   LMISS


      IF (.NOT.(1.LE.NB .AND. NB.LE.N)) THEN
        CALL MSGDMP('E','VRRNM ','AVERAGING LENGTH IS INVALID.')
      END IF
      IF (MOD(NB,2).EQ.0) THEN
        CALL MSGDMP('E','VRRNM ','AVERAGING LENGTH IS EVEN NUMBER.')
      END IF

      CALL GLLGET('LMISS',LMISS)
      IF (LMISS) THEN
        CALL VRRNM1(RX,RY,N,JX,JY,NB)
      ELSE
        CALL VRRNM0(RX,RY,N,JX,JY,NB)
      END IF

      END
