*-----------------------------------------------------------------------
*     VS1INT / VS1DIN / VS1OUT
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE VS1INT(WZ,NW,IX)

      INTEGER   NW(IX)
      REAL      WZ(IX,2),X(IX)

      LOGICAL   LMISS

      SAVE


      CALL GLLGET('LMISS',LMISS)
      CALL GLRGET('RMISS',RMISS)

      CALL RSET0(WZ,IX*2,1,0)
      CALL ISET0(NW,IX,1,0)

      RETURN
*-----------------------------------------------------------------------
      ENTRY VS1DIN(WZ,NW,IX,X)

      DO 10 I=1,IX
        IF (.NOT.(LMISS .AND. X(I).EQ.RMISS)) THEN
          NW(I)=NW(I)+1
          WZ(I,1)=WZ(I,1)+X(I)
          WZ(I,2)=WZ(I,2)+X(I)*X(I)
        END IF
   10 CONTINUE

      RETURN
*-----------------------------------------------------------------------
      ENTRY VS1OUT(WZ,NW,IX)

      DO 15 I=1,IX
        IF (NW(I).NE.0) THEN
          XAVE=WZ(I,1)/NW(I)
          XVAR=WZ(I,2)/NW(I)-XAVE*XAVE
          WZ(I,1)=XAVE
          WZ(I,2)=XVAR
        ELSE
          WZ(I,1)=RMISS
          WZ(I,2)=RMISS
        END IF
   15 CONTINUE

      RETURN
      END
