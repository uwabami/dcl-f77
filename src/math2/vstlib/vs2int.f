*-----------------------------------------------------------------------
*     VS2INT / VS2DIN / VS2OUT
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE VS2INT(WZ,NW,IX,IY)

      INTEGER   NW(IX,IY)
      REAL      WZ(IX,IY,5),X(IX),Y(IY)

      LOGICAL   LMISS

      SAVE


      CALL GLLGET('LMISS',LMISS)
      CALL GLRGET('RMISS',RMISS)

      CALL RSET0(WZ,IX*IY*5,1,0)
      CALL ISET0(NW,IX*IY,1,0)

      RETURN
*-----------------------------------------------------------------------
      ENTRY VS2DIN(WZ,NW,IX,IY,X,Y)

      DO 15 J=1,IY
        DO 10 I=1,IX
          IF (.NOT.(LMISS
     +        .AND. (X(I).EQ.RMISS .OR. Y(J).EQ.RMISS))) THEN
            NW(I,J)=NW(I,J)+1
            WZ(I,J,1)=WZ(I,J,1)+X(I)
            WZ(I,J,2)=WZ(I,J,2)+Y(J)
            WZ(I,J,3)=WZ(I,J,3)+X(I)*X(I)
            WZ(I,J,4)=WZ(I,J,4)+Y(J)*Y(J)
            WZ(I,J,5)=WZ(I,J,5)+X(I)*Y(J)
          END IF
   10   CONTINUE
   15 CONTINUE

      RETURN
*-----------------------------------------------------------------------
      ENTRY VS2OUT(WZ,NW,IX,IY)

      DO 25 J=1,IY
        DO 20 I=1,IX
          IF (NW(I,J).NE.0) THEN
            XAVE=WZ(I,J,1)/NW(I,J)
            YAVE=WZ(I,J,2)/NW(I,J)
            XVAR=WZ(I,J,3)/NW(I,J)-XAVE*XAVE
            YVAR=WZ(I,J,4)/NW(I,J)-YAVE*YAVE
            XYCV=WZ(I,J,5)/NW(I,J)-XAVE*YAVE
            WZ(I,J,1)=XAVE
            WZ(I,J,2)=YAVE
            WZ(I,J,3)=XVAR
            WZ(I,J,4)=YVAR
            WZ(I,J,5)=XYCV
          ELSE
            WZ(I,J,1)=RMISS
            WZ(I,J,2)=RMISS
            WZ(I,J,3)=RMISS
            WZ(I,J,4)=RMISS
            WZ(I,J,5)=RMISS
          END IF
   20   CONTINUE
   25 CONTINUE

      RETURN
      END
