*-----------------------------------------------------------------------
*     1st order Runge-Kutta (Euler) algorithm routine.
*                                                 Oct. 5, 1990  S.Sakai
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE ODRK1(N, FCN, T, DT, X, DX, XOUT, WORK)
      DIMENSION  X(N), XOUT(N), DX(N)
      EXTERNAL   FCN

      DO 10 I=1,N
        XOUT(I) = X(I) + DT*DX(I)
   10 CONTINUE

      RETURN
      END
