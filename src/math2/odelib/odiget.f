*-----------------------------------------------------------------------
*     ODIGET / ODISET / ODISTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE ODIGET(CP, IPARA)

      CHARACTER CP*(*)

      CHARACTER CX*8
      CHARACTER CL*40


      CALL ODIQID(CP, IDX)
      CALL ODIQVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY ODISET(CP, IPARA)

      CALL ODIQID(CP, IDX)
      CALL ODISVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY ODISTX(CP, IPARA)

      IP = IPARA
      CALL ODIQID(CP, IDX)

*     / SHORT NAME /

      CALL ODIQCP(IDX, CX)
      CALL RTIGET('OD', CX, IP, 1)

*     / LONG NAME /

      CALL ODIQCL(IDX, CL)
      CALL RLIGET(CL, IP, 1)

      CALL ODISVL(IDX,IP)

      RETURN
      END
