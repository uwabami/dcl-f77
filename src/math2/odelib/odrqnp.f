*-----------------------------------------------------------------------
*     REAL PARAMETER CONTROL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE ODRQNP(NCP)

      CHARACTER CP*(*)

      PARAMETER (NPARA=1)

      REAL      RX(NPARA)
      LOGICAL   LCHREQ,LFIRST
      CHARACTER CPARAS(NPARA)*8
      CHARACTER CPARAL(NPARA)*40
      CHARACTER CMSG*80

      EXTERNAL  LCHREQ,LENC

      SAVE

      DATA      CPARAS( 1)/'EPSILON '/, RX( 1) / 1.E-5 /

      DATA      CPARAL( 1)/'EPSILON '/

      DATA      LFIRST /.TRUE./

      NCP=NPARA

      RETURN
*-----------------------------------------------------------------------
      ENTRY ODRQID(CP,IDX)

      DO 10 N=1,NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IDX = N
          RETURN
        END IF
   10 CONTINUE
      CMSG = 'PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E','ODRQID',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY ODRQCP(IDX,CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAS(IDX)
      ELSE
        CALL MSGDMP('E','ODRQCP','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY ODRQCL(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAL(IDX)
      ELSE
        CALL MSGDMP('E','ODRQCL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY ODRQVL(IDX, RPARA)

      IF (LFIRST) THEN
        CALL RTLGET('OD', CPARAS, IX, NPARA)
        CALL RLLGET(CPARAL, IX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        RPARA = RX(IDX)
      ELSE
        CALL MSGDMP('E','ODRQVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY ODRSVL(IDX,RPARA)

      IF (LFIRST) THEN
        CALL RTRGET('OD', CPARAS, IX, NPARA)
        CALL RLRGET(CPARAL, IX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        RX(IDX) = RPARA
      ELSE
        CALL MSGDMP('E','ODRSVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY ODRQIN(CP, IN)

      DO 20 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IN = N
          RETURN
        END IF
   20 CONTINUE

      IN = 0

      RETURN
      END

