*-----------------------------------------------------------------------
*     ODPQNP / ODPQID / ODPQCP / ODPQVL / ODPSVL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE ODPQNP(NCP)

      CHARACTER CP*(*)

      PARAMETER (NPARA = 16)

      INTEGER   ITYPE(NPARA)
      CHARACTER CPARAS(NPARA)*8
      CHARACTER CPARAL(NPARA)*40
      CHARACTER CMSG*80

      LOGICAL   LCHREQ

      EXTERNAL  LCHREQ,LENC

      SAVE

*     / SHORT NAME /

      DATA      CPARAS(1) / 'LBAR    ' /, ITYPE(1) / 2 /
      DATA      CPARAS(2) / 'EPSILON ' /, ITYPE(2) / 3 /
      DATA      CPARAS(3) / 'MAXSTEP ' /, ITYPE(3) / 1 /
      DATA      CPARAS(4) / 'NSTEP   ' /, ITYPE(4) / 1 /

*     / LONG NAME /

      DATA      CPARAL(1) / 'LBAR    ' /
      DATA      CPARAL(2) / 'EPSILON ' /
      DATA      CPARAL(3) / 'MAXSTEP ' /
      DATA      CPARAL(4) / 'NSTEP   ' /

      NCP = NPARA

      RETURN
*-----------------------------------------------------------------------
      ENTRY ODPQID(CP, IDX)

      DO 10 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IDX = N
          RETURN
        END IF
   10 CONTINUE
      CMSG = 'PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E','ODPQID',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY ODPQCP(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAS(IDX)
      ELSE
        CALL MSGDMP('E','ODPQCP','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY ODPQCL(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAL(IDX)
      ELSE
        CALL MSGDMP('E','ODPQCL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY ODPQIT(IDX, ITP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        ITP = ITYPE(IDX)
      ELSE
        CALL MSGDMP('E','ODPQIT','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY ODPQVL(IDX, IPARA)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        IF (ITYPE(IDX) .EQ. 1) THEN
          CALL ODIQID(CPARAS(IDX), ID)
          CALL ODIQVL(ID, IPARA)
        ELSE IF (ITYPE(IDX) .EQ. 2) THEN
          CALL ODLQID(CPARAS(IDX), ID)
          CALL ODLQVL(ID, IPARA)
        ELSE IF (ITYPE(IDX) .EQ. 3) THEN
          CALL ODRQID(CPARAS(IDX), ID)
          CALL ODRQVL(ID, IPARA)
        END IF
      ELSE
         CALL MSGDMP('E','ODPQVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY ODPSVL(IDX, IPARA)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        IF (ITYPE(IDX) .EQ. 1) THEN
          CALL ODIQID(CPARAS(IDX), ID)
          CALL ODISVL(ID, IPARA)
        ELSE IF (ITYPE(IDX) .EQ. 2) THEN
          CALL ODLQID(CPARAS(IDX), ID)
          CALL ODLSVL(ID, IPARA)
        ELSE IF (ITYPE(IDX) .EQ. 3) THEN
          CALL ODRQID(CPARAS(IDX), ID)
          CALL ODRSVL(ID, IPARA)
        END IF
      ELSE
        CALL MSGDMP('E','ODPSVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY ODPQIN(CP, IN)

      DO 20 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IN = N
          RETURN
        END IF
   20 CONTINUE

      IN = 0

      RETURN
      END
