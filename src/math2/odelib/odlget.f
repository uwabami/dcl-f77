*-----------------------------------------------------------------------
*     ODLGET / ODLSET / ODLSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE ODLGET(CP, LPARA)

      LOGICAL   LPARA
      CHARACTER CP*(*)

      LOGICAL   LP
      CHARACTER CX*8
      CHARACTER CL*40

      CALL ODLQID(CP, IDX)
      CALL ODLQVL(IDX, LPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY ODLSET(CP, LPARA)

      CALL ODLQID(CP, IDX)
      CALL ODLSVL(IDX, LPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY ODLSTX(CP, LPARA)

      LP = LPARA
      CALL ODLQID(CP, IDX)

*     / SHORT NAME /

      CALL ODLQCP(IDX, CX)
      CALL RTLGET('OD', CX, LP, 1)

*     / LONG NAME /

      CALL ODLQCL(IDX, CL)
      CALL RLLGET(CL, LP, 1)

      CALL ODLSVL(IDX,LP)

      RETURN
      END
