*-----------------------------------------------------------------------
*     ODPGET / ODPSET / ODPSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE ODPGET(CP, IPARA)

      CHARACTER CP*(*)

      CHARACTER CX*8, CL*40


      CALL ODPQID(CP, IDX)
      CALL ODPQVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY ODPSET(CP, IPARA)

      CALL ODPQID(CP, IDX)
      CALL ODPSVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY ODPSTX(CP, IPARA)

      IP = IPARA
      CALL ODPQID(CP, IDX)
      CALL ODPQIT(IDX, IT)
      CALL ODPQCP(IDX, CX)
      CALL ODPQCL(IDX, CL)
      IF (IT .EQ. 1) THEN
        CALL RTIGET('OD', CX, IP, 1)
        CALL RLIGET(CL, IP, 1)
        CALL ODIQID(CP, IDX)
        CALL ODISVL(IDX, IP)
      ELSE IF (IT .EQ. 2) THEN
        CALL RTLGET('OD', CX, IP, 1)
        CALL RLLGET(CL, IP, 1)
        CALL ODLQID(CP, IDX)
        CALL ODLSVL(IDX, IP)
      ELSE IF (IT .EQ. 3) THEN
        CALL RTRGET('OD', CX, IP, 1)
        CALL RLRGET(CL, IP, 1)
        CALL ODRQID(CP, IDX)
        CALL ODRSVL(IDX, IP)
      ENDIF

      RETURN
      END
