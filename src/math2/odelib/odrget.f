*-----------------------------------------------------------------------
*     ODRGET / ODRSET / ODRSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE ODRGET(CP, RPARA)

      CHARACTER CP*(*)

      CHARACTER CX*8
      CHARACTER CL*40


      CALL ODRQID(CP, IDX)
      CALL ODRQVL(IDX, RPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY ODRSET(CP, RPARA)

      CALL ODRQID(CP, IDX)
      CALL ODRSVL(IDX, RPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY ODRSTX(CP, RPARA)

      RP = RPARA
      CALL ODRQID(CP, IDX)

*     / SHORT NAME /

      CALL ODRQCP(IDX, CX)
      CALL RTRGET('OD', CX, RP, 1)

*     / LONG NAME /

      CALL ODRQCL(IDX, CL)
      CALL RLRGET(CL, RP, 1)

      CALL ODRSVL(IDX,RP)

      RETURN
      END
