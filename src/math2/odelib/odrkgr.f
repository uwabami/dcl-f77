*-----------------------------------------------------------------------
*    Runge-Kutta Gill Stepper.
*    Re-try when error condition is not satisfied.
*                                                 Oct. 5, 1990  S.Sakai
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE ODRKGR(N, FCN, T, DT, EPSL, X, WORK)

      PARAMETER (SAFETY=0.9,ERRCON=6.E-4)
      DIMENSION X(N), WORK(N,5)
      EXTERNAL  FCN

      PGROW  = -0.20
      PSHRNK = -0.25
      TINY   = 1.E-30
      T0     = T
      CALL FCN(N, T0, X, WORK(1,2))

*-------------------- Integration with DT & DT*2 -----------------------

  100 CONTINUE
      DO 10 I=1, N
        WORK(I,1) = WORK(I,2)
  10  CONTINUE
      DT2 = DT*2
      CALL ODRKG(N, FCN, T0, DT2, X, WORK(1,1), WORK(1,3), WORK(1,5))

      DO 20 I=1, N
        WORK(I,1) = WORK(I,2)
  20  CONTINUE
      CALL ODRKG(N, FCN, T0, DT,  X, WORK(1,1), WORK(1,4), WORK(1,5))
      T = T0 + DT
      CALL FCN(N, T, WORK(1,4), WORK(1,1))
      CALL ODRKG(N, FCN, T, DT,
     +           WORK(1,4), WORK(1,1), WORK(1,4),WORK(1,5))
      T = T0 + DT2

*------------------------- ERROR EVALUATION ----------------------------

      ERRMAX=0.
      DO 200 I=1, N
        WORK(I,1) = WORK(I,4)-WORK(I,3)
        XSCAL   = ABS(X(I)) + ABS(DT2*WORK(I,2)) + TINY
        ERRMAX  = MAX(ERRMAX, ABS(WORK(I,1)/XSCAL))
  200 CONTINUE
      ERRMAX = ERRMAX/EPSL

      IF(ERRMAX.GT.1.) THEN
        DT = SAFETY*DT*(ERRMAX**PSHRNK)
        CALL MSGDMP('M', 'ODRKGR', 'RECALCULATING.')
        GOTO 100
      ELSE
        IF(ERRMAX.GT.ERRCON)THEN
          DT = SAFETY*DT*(ERRMAX**PGROW)
        ELSE
          DT = 4.*DT
        ENDIF
      ENDIF

      DO 300 I=1, N
        X(I) = WORK(I,4)
  300 CONTINUE

      RETURN
      END
