************************************************************************
*     CALCULATE THE POSITION OF A SPECTRUM COEFFICIENT OF P_N^M
************************************************************************
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SHNM2L(MM,N,M,LR,LI)

*     LR: REAL PART, LI: IMAGINARY PART

      IF(M.EQ.0) THEN
        LR=N+1
        LI=LR
      ELSE
        LR=M*(2*MM-M+2)-MM+N
        LI=LR+MM-M+1
      END IF

      END
