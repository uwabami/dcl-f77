***********************************************************************
*     BACKWARD LEGENDRE TRANSFORMATION (LOWER LEVEL)
***********************************************************************
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SHLBWL(MM,M,SD,WS,WA,PMJ)

      REAL SD(0:MM+1)
      REAL WS,WA
      REAL PMJ(0:MM+1)
      REAL*8 SUMS,SUMA

      N=M
      SUMS=SD(N)*PMJ(N)
      SUMA=SD(N+1)*PMJ(N+1)

      DO 10 N=M+2,MM,2
        SUMS=SUMS+SD(N)*PMJ(N)
        SUMA=SUMA+SD(N+1)*PMJ(N+1)
   10 CONTINUE

      IF(MOD(MM-M,2).EQ.1) THEN
        SUMS=SUMS+SD(MM+1)*PMJ(MM+1)
      END IF

      WS=SUMS
      WA=SUMA

      END
