***********************************************************************
*     BACKWARD LEGENDRE TRANSFORMATION (UPPER LEVEL)
***********************************************************************
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SHLBWU(MM,JM,M,ISW,SM,WM,SD,PM,YS,YC,PY,R)

      REAL SM(M:MM)
      REAL WM(-JM:JM)
      REAL SD(0:MM+1),PM(0:MM+1,0:JM)
      REAL PY(2,0:JM,0:MM)
      REAL YS(0:JM),YC(0:JM)
      REAL R((MM+1)*(MM+1))

      CALL SHPPMA(MM,JM,M,PM,YS,PY,R)
      CALL SHLBWM(MM,JM,M,ISW,SM,WM,SD,PM,YC,R)

      END
