***********************************************************************
*     FORWARD LEGENDRE TRANSFORMATION (MIDDLE LEVEL)
***********************************************************************
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SHLFWM(MM,JM,M,ISW,WM,SM,SD,XC,PM,WY,WX,R,Z)

      REAL SM(M:MM),WM(-JM:JM)
      REAL SD(0:MM+1)
      REAL XC(0:JM)
      REAL PM(0:MM+1,0:JM)
      REAL WY(0:JM,2),WX(0:JM,2)
      REAL R((MM+1)*(MM+1)),Z(JM,0:JM,4)

      IF((ISW.EQ.-1).AND.(M.EQ.0)) THEN
        DO 10 N=M,MM
          SM(N)=0
   10   CONTINUE
      ELSE
        CALL SHLY2X(JM,M,ISW,WM,WX,WY,Z)
        IF((ISW.EQ.0).AND.(M.NE.0)) THEN
          DO 20 J=0,JM
            WX(J,1)=WX(J,1)*XC(J)
            WX(J,2)=WX(J,2)*XC(J)
   20     CONTINUE
        ELSE IF((ISW.EQ.1).AND.(M.EQ.0)) THEN
          DO 30 J=0,JM
            WX(J,1)=WX(J,1)/XC(J)
            WX(J,2)=WX(J,2)/XC(J)
   30     CONTINUE
        END IF
        DO 40 N=M,MM+1
          CALL SHLFWL(MM,JM,N,M,WX,SD(N),PM)
   40   CONTINUE
        CALL SHLSDS(MM,M,ISW,SD,SM,R)
      END IF

      END
