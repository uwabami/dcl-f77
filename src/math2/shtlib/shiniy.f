************************************************************************
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SHINIY(JM,Y,YS,YC)

      PARAMETER(PI=3.1415926535897932385D0)
      REAL Y(0:JM),YS(0:JM),YC(0:JM)

      DO 10 J=0,JM
        YD=PI*J/(JM*2)
        Y(J)=YD
        YS(J)=SIN(YD)
        YC(J)=COS(YD)
   10 CONTINUE

      END
