************************************************************************
*     OPERATE LAPLACIAN
************************************************************************
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SHOLAP(MM,IND,A,B)

      REAL A((MM+1)*(MM+1)),B((MM+1)*(MM+1))

*     IND=1: \nabla^2, IND=-1: \nabla^{-2}

      IF(IND.EQ.1) THEN
        M=0
        L=0
        DO 10 N=0,MM
          L=L+1
          B(L)=-N*(N+1)*A(L)
   10   CONTINUE
        DO 40 M=1,MM
          DO 30 IR=1,2
            DO 20 N=M,MM
              L=L+1
              B(L)=-N*(N+1)*A(L)
   20       CONTINUE
   30     CONTINUE
   40   CONTINUE
      ELSE IF(IND.EQ.-1) THEN
        M=0
        L=1
        B(L)=0
        DO 50 N=1,MM
          L=L+1
          B(L)=-A(L)/(N*(N+1))
   50   CONTINUE
        DO 80 M=1,MM
          DO 70 IR=1,2
            DO 60 N=M,MM
              L=L+1
              B(L)=-A(L)/(N*(N+1))
   60       CONTINUE
   70     CONTINUE
   80   CONTINUE
      END IF

      END
