***********************************************************************
*     INITIALIZE MATRICES FOR CALCULATION OF LEGENDRE FUNCTIONS
************************************************************************
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SHINIR(MM,R)
 
      REAL R((MM+1)*(MM+1))

      L=0
      DO 30 M=0,MM
        DO 10 N=M+1,MM+1
          L=L+1
          R(L)=SQRT(1.0*(N*N-M*M)/(4*N*N-1))
   10   CONTINUE
        DO 20 N=M+2,MM+1
          L=L+1
          R(L)=SQRT(1.0*(4*N*N-1)/(N*N-M*M))
   20   CONTINUE
   30 CONTINUE

      END
