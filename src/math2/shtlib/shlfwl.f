***********************************************************************
*     FORWARD LEGENDRE TRANSFORMATION (LOWER LEVEL)
***********************************************************************
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SHLFWL(MM,JM,N,M,WX,SD,PM)

      REAL WX(0:JM,2)
      REAL SD,PM(0:MM+1,0:JM)
      REAL*8 SUM

      IF(MOD(N-M,2).EQ.0) THEN
        SUM=0.5*WX(0,1)*PM(N,0)
        DO 10 J=1,JM
          SUM=SUM+WX(J,1)*PM(N,J)
   10   CONTINUE
      ELSE
        SUM=0
        DO 20 J=1,JM
          SUM=SUM+WX(J,2)*PM(N,J)
   20   CONTINUE
      END IF
      SD=SUM

      END
