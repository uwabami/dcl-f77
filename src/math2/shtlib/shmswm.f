***********************************************************************
*     TRANSFORM SPECTRA INTO WAVE (FOR M>0)
***********************************************************************
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SHMSWM(MM,JM,M,ISW,S,WR,WI,SD,PM,YS,YC,PY,R)

      REAL S((MM+1)*(MM+1)),WR(-JM:JM),WI(-JM:JM)
      REAL SD(0:MM+1),PM(0:MM+1,0:JM)
      REAL YS(0:JM),YC(0:JM)
      REAL PY(2,0:JM,0:MM)
      REAL R((MM+1)*(MM+1))

      CALL SHNM2L(MM,M,M,LR,LI)
      CALL SHPPMA(MM,JM,M,PM,YS,PY,R)
      CALL SHLBWM(MM,JM,M,ISW,S(LR),WR,SD,PM,YC,R)
      CALL SHLBWM(MM,JM,M,ISW,S(LI),WI,SD,PM,YC,R)
      IF(ISW.EQ.-1) THEN
        CALL SHMDXM(JM,WR(-JM),WI(-JM))
      END IF

      END
