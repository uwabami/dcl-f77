***********************************************************************
*     TRANSFORM SPECTRA INTO WAVE (FOR M=0)
***********************************************************************
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SHMSWZ(MM,JM,ISW,S,WZ,SD,PM,YS,YC,PY,R)

      REAL S((MM+1)*(MM+1)),WZ(-JM:JM)
      REAL SD(0:MM+1),PM(0:MM+1,0:JM)
      REAL YS(0:JM),YC(0:JM)
      REAL PY(2,0:JM,0:MM)
      REAL R((MM+1)*(MM+1))

      M=0
      IF(ISW.EQ.-1) THEN
        DO 10 J=-JM,JM
          WZ(J)=0
   10   CONTINUE
      ELSE 
        CALL SHPPMA(MM,JM,M,PM,YS,PY,R)
        CALL SHLBWM(MM,JM,M,ISW,S,WZ,SD,PM,YC,R)
      END IF

      END
