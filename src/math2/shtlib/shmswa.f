***********************************************************************
*     TRANSFORM SPECTRA INTO WAVE (FROM M=M1 TO M=M2)
***********************************************************************
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SHMSWA(MM,JM,ISW,M1,M2,S,W,SD,PM,YS,YC,PY,R)

      REAL S((MM+1)*(MM+1)),W(-JM:JM,-MM:MM)
      REAL SD(0:MM+1),PM(0:MM+1,0:JM)
      REAL YS(0:JM),YC(0:JM)
      REAL PY(2,0:JM,0:MM)
      REAL R((MM+1)*(MM+1))

      IF(M1.GT.0) THEN
        DO 10 J=-JM,JM
          W(J,0)=0
   10   CONTINUE
      ELSE
        CALL SHMSWZ(MM,JM,ISW,S,W(-JM,0),SD,PM,YS,YC,PY,R)
      END IF

      DO 20 M=M1,M2
        CALL SHMSWM(MM,JM,M,ISW,S,W(-JM,M),W(-JM,-M),SD,PM,YS,YC,PY,R)
   20 CONTINUE

      DO 40 M=M2+1,MM
        DO 30 J=-JM,JM
          W(J, M)=0
          W(J,-M)=0
   30   CONTINUE
   40 CONTINUE

      END
