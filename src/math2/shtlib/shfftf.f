***********************************************************************
*     FAST FOURIER TRANSFORMATION (FORWARD)
***********************************************************************
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SHFFTF(NFFT,XFFT,WFFT)

      REAL XFFT(NFFT)
      REAL WFFT(*)

      CALL RFFTF(NFFT,XFFT,WFFT)

      END
