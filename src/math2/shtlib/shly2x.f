***********************************************************************
*     INTERPOLATION FROM GRID POINTS TO GAUSSIAN LATITUDES
***********************************************************************
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SHLY2X(JM,M,ISW,WM,WX,WY,Z)

      REAL WM(-JM:JM)
      REAL WY(0:JM,2),WX(0:JM,2)
      REAL Z(JM,0:JM,4)
      REAL*8 SUMS,SUMA

*     ISW=-1: X-DIFFERENTIAL, ISW=0: NORMAL, ISW=1: Y-DIFFERENTIAL

      ISWA=ABS(ISW)

      WY(0,1)=WM(0)
      WY(0,2)=0
      DO 10 J=1,JM
        WY(J,1)=0.5*(WM(J)+WM(-J))
        WY(J,2)=0.5*(WM(J)-WM(-J))
   10 CONTINUE

      IF(MOD(M+ISWA,2).EQ.0) THEN
        WX(0,1)=WY(0,1)
        WX(0,2)=WY(0,2)
        DO 30 J1=1,JM
          SUMS=0
          SUMA=0
          DO 20 J2=0,JM
            SUMS=SUMS+Z(J1,J2,1)*WY(J2,1)
            SUMA=SUMA+Z(J1,J2,2)*WY(J2,2)
   20     CONTINUE
          WX(J1,1)=SUMS
          WX(J1,2)=SUMA
   30   CONTINUE
      ELSE
        WX(0,1)=WY(0,1)
        WX(0,2)=WY(0,2)
        DO 50 J1=1,JM
          SUMS=0
          SUMA=0
          DO 40 J2=0,JM-1
            SUMS=SUMS+Z(J1,J2,3)*WY(J2,1)
            SUMA=SUMA+Z(J1,J2,4)*WY(J2,2)
   40     CONTINUE
          WX(J1,1)=SUMS
          WX(J1,2)=SUMA
   50   CONTINUE
      END IF

      END
