***********************************************************************
*     TRANSFORM WAVE TO GRID
***********************************************************************
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SHFW2G(MM,JM,IM,W,G,H,WFFT)

      REAL W(-JM:JM,-MM:MM),G(-IM:IM,-JM:JM)
      REAL H(0:2*IM-1)
      REAL WFFT(*)

      CALL SHFWGA(MM,JM,IM,0,MM,W,G,H,WFFT)

      END
