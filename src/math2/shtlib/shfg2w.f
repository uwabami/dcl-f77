***********************************************************************
*     TRANSFORM GRID TO WAVE
***********************************************************************
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SHFG2W(MM,JM,IM,G,W,H,WFFT)

      REAL G(-IM:IM,-JM:JM),W(-JM:JM,-MM:MM)
      REAL H(0:2*IM-1)
      REAL WFFT(*)

*     IM.GE.MM+1

      DO 30 J=-JM,JM
        H(0)=G(0,J)
        DO 10 I=1,IM
          H(I)=G(I,J)
          H(2*IM-I)=G(-I,J)
   10   CONTINUE
        CALL SHFFTF(2*IM,H,WFFT)
        W(J,0)=H(0)/(2*IM)
        DO 20 K=1,MM
          W(J, K)=H(2*K-1)/(2*IM)
          W(J,-K)=H(2*K  )/(2*IM)
   20   CONTINUE
   30 CONTINUE

      END
