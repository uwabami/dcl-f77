***********************************************************************
*     X-DIFFERENTIAL TRANSFORMATION FOR SHLBWM
***********************************************************************
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SHMDXM(JM,WR,WI)

      REAL WR(-JM:JM),WI(-JM:JM)
      REAL WRD,WID

      DO 10 J=-JM,JM
        WRD=WR(J)
        WID=WI(J)
        WR(J)=-WID
        WI(J)= WRD
   10 CONTINUE

      END
