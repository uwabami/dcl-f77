***********************************************************************
*     X-DIFFERENTIAL TRANSFORMATION FOR SHMW2S
***********************************************************************
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SHMSRI(MM,M,SR,SI)

      REAL SR(M:MM),SI(M:MM)
      REAL SRD,SID

      DO 10 N=M,MM
        SRD=SR(N)
        SID=SI(N)
        SR(N)= SID
        SI(N)=-SRD
   10 CONTINUE

      END
