***********************************************************************
*     TRANSFORM WAVE TO GRID FOR M=0
***********************************************************************
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SHFWGZ(JM,IM,WZ,G)

      REAL WZ(-JM:JM),G(-IM:IM,-JM:JM)

      DO 20 J=-JM,JM
        DO 10 I=-IM,IM
          G(I,J)=WZ(J)
   10   CONTINUE
   20 CONTINUE

      END
