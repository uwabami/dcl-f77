***********************************************************************
*     INITIALIZE BASE LEGENDRE FUNCTIONS
************************************************************************
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SHINIP(MM,JM,YS,YC,PY)

      REAL YS(0:JM),YC(0:JM)
      REAL PY(2,0:JM,0:MM)

*     M=0: P^0_0 and P^0_1, 
*     M>0: P^M_M/\cos\phi and P^M_{M+1}/\cos\phi

      DO 10 J=0,JM
        PY(1,J,0)=1
        PY(2,J,0)=SQRT(3.0)*YS(J)
   10 CONTINUE

      M=1
      A=SQRT(1.0*(2*M+1)/(2*M))
      B=SQRT(2*M+3.0)
      DO 20 J=0,JM
        PY(1,J,M)=A*PY(1,J,M-1)
        PY(2,J,M)=B*YS(J)*PY(1,J,M)
   20 CONTINUE

      DO 40 M=2,MM
        A=SQRT(1.0*(2*M+1)/(2*M))
        B=SQRT(2*M+3.0)
        DO 30 J=0,JM
          PY(1,J,M)=A*YC(J)*PY(1,J,M-1)
          PY(2,J,M)=B*YS(J)*PY(1,J,M)
   30   CONTINUE
   40 CONTINUE

      END
