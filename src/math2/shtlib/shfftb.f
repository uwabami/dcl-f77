***********************************************************************
*     FAST FOURIER TRANSFORMATION (BACKWARD)
***********************************************************************
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SHFFTB(NFFT,XFFT,WFFT)

      REAL XFFT(NFFT)
      REAL WFFT(*)

      CALL RFFTB(NFFT,XFFT,WFFT)

      END
