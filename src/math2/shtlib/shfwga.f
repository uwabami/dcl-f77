***********************************************************************
*     TRANSFORM WAVE TO GRID FROM M=M1 TO M=M2
***********************************************************************
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SHFWGA(MM,JM,IM,M1,M2,W,G,H,WFFT)

      REAL W(-JM:JM,-MM:MM),G(-IM:IM,-JM:JM)
      REAL H(0:2*IM-1)
      REAL WFFT(*)

*     IM.GE.MM+1

      DO 50 J=-JM,JM
        IF(M1.GT.0) THEN
          H(0)=0
        ELSE
          H(0)=W(J,0)
        END IF
        DO 10 M=1,M1-1
          H(2*M-1)=0
          H(2*M  )=0
   10   CONTINUE
        DO 20 M=M1,M2
          H(2*M-1)=W(J, M)
          H(2*M  )=W(J,-M)
   20   CONTINUE
        DO 30 K=2*M2+1,2*IM-1
          H(K)=0
   30   CONTINUE
        CALL SHFFTB(2*IM,H,WFFT)
        G(0,J)=H(0)
        DO 40 I=1,IM
          G( I,J)=H(I)
          G(-I,J)=H(2*IM-I)
   40   CONTINUE
   50 CONTINUE

      END
