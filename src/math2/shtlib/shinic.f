************************************************************************
*     OPERATE GAUSSIAN WEIGHT TO LEGENDRE FUNCTIONS 
************************************************************************
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SHINIC(MM,JM,XW,PX)

      REAL XW(0:JM),PX(2,0:JM,0:MM)

      DO 20 M=0,MM
        DO 10 J=0,JM
          PX(1,J,M)=XW(J)*PX(1,J,M)
          PX(2,J,M)=XW(J)*PX(2,J,M)
   10   CONTINUE
   20 CONTINUE 

      END
