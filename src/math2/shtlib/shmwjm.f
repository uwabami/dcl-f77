***********************************************************************
*     TRANSFORM SPECTRA INTO WAVE FOR M>0 AT A LATITUDE
***********************************************************************
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SHMWJM(MM,JM,ISW,J,M,S,WJR,WJI,SD,PMJ,YS,YC,PY,R)

      REAL S((MM+1)*(MM+1)),WJR,WJI
      REAL SD(0:MM+1),PMJ(0:MM+1)
      REAL YS(0:JM),YC(0:JM)
      REAL PY(2,0:JM,0:MM)
      REAL R((MM+1)*(MM+1))

      JD=ABS(J)

      CALL SHNM2L(MM,M,M,LR,LI)
      CALL SHPPMJ(MM,JM,M,JD,PMJ,YS,PY,R)
      CALL SHLBWJ(MM,JM,M,J,ISW,S(LR),WJR,SD,PMJ,YC,R)
      CALL SHLBWJ(MM,JM,M,J,ISW,S(LI),WJI,SD,PMJ,YC,R)
      IF(ISW.EQ.-1) THEN
        CALL SHMDXJ(WJR,WJI)
      END IF

      END
