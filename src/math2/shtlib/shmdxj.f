***********************************************************************
*     X-DIFFERENTIAL TRANSFORMATION FOR SHMWJM
***********************************************************************
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SHMDXJ(WRD,WID)

      REAL WRD,WID
      REAL WRDD,WIDD

      WRDD=WRD
      WIDD=WID
      WRD=-WIDD
      WID= WRDD

      END
