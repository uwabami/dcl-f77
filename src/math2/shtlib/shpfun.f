***********************************************************************
*     CALCULATE LEGENDRE FUNCTIONS
***********************************************************************
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SHPFUN(MM,JM,M,FUN,PM,YS,YC,PY,R)

      REAL FUN(-JM:JM,M:MM)
      REAL PM(0:MM+1,0:JM)
      REAL YS(0:JM),YC(0:JM),PY(2,0:JM,0:MM),R((MM+1)*(MM+1))

      CALL SHPPMA(MM,JM,M,PM,YS,PY,R)

      J=0
      DO 10 N=M,MM
        FUN(J,N)=PM(N,J)
   10 CONTINUE
      IP=-1
      DO 30 N=M,MM
        IP=-IP
        DO 20 J=1,JM
          FUN( J,N)=PM(N,J)
          FUN(-J,N)=PM(N,J)*IP
   20   CONTINUE
   30 CONTINUE

      IF(M.NE.0) THEN
        DO 50 N=M,MM
          DO 40 J=1,JM
            FUN( J,N)=FUN( J,N)*YC(J)
            FUN(-J,N)=FUN(-J,N)*YC(J)
   40     CONTINUE
   50   CONTINUE
      END IF

      END
