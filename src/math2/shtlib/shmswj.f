***********************************************************************
*     TRANSFORM SPECTRA INTO WAVE FROM M=M1 TO M=M2 AT A LATITUDE
***********************************************************************
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SHMSWJ(MM,JM,ISW,J,M1,M2,S,WJ,SD,PMJ,YS,YC,PY,R)

      REAL S((MM+1)*(MM+1)),WJ(-MM:MM)
      REAL SD(0:MM+1),PMJ(0:MM+1)
      REAL YS(0:JM),YC(0:JM)
      REAL PY(2,0:JM,0:MM)
      REAL R((MM+1)*(MM+1))

      IF(M1.GT.0) THEN
        WJ(0)=0
      ELSE
        CALL SHMWJZ(MM,JM,ISW,J,S,WJ(0),SD,PMJ,YS,YC,PY,R)
      END IF

      DO 20 M=M1,M2
        CALL SHMWJM(MM,JM,ISW,J,M,S,WJ(M),WJ(-M),SD,PMJ,YS,YC,PY,R)
   20 CONTINUE

      DO 30 M=M2+1,MM
        WJ( M)=0
        WJ(-M)=0
   30 CONTINUE

      END
