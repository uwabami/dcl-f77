************************************************************************
*     INITIALIZE MATRICES FOR INTERPOLATION
************************************************************************
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SHINIZ(JM,X,Y,Z)

      REAL X(0:JM),Y(0:JM)
      REAL Z(JM,0:JM,4)

      DO 11 J1=1,JM
        DO 10 J2=0,JM
          T1=X(J1)-Y(J2)
          T2=X(J1)+Y(J2)
          Z(J1,J2,1)=(SIN(2*JM*T1)/TAN(T1)+SIN(2*JM*T2)/TAN(T2))/(2*JM)
   10   CONTINUE
        Z(J1, 0,1)=Z(J1, 0,1)/2
        Z(J1,JM,1)=Z(J1,JM,1)/2
   11 CONTINUE
 
      DO 21 J1=1,JM
        Z(J1,0,2)=0
        DO 20 J2=1,JM
          T1=X(J1)-Y(J2)
          T2=X(J1)+Y(J2)
          Z(J1,J2,2)=(SIN(2*JM*T1)/SIN(T1)-SIN(2*JM*T2)/SIN(T2))/(2*JM)
   20   CONTINUE
        Z(J1,JM,2)=Z(J1,JM,2)/2
   21 CONTINUE
 
      DO 31 J1=1,JM
        DO 30 J2=0,JM-1
          T1=X(J1)-Y(J2)
          T2=X(J1)+Y(J2)
          Z(J1,J2,3)=(SIN(2*JM*T1)/SIN(T1)+SIN(2*JM*T2)/SIN(T2))/(2*JM)
   30   CONTINUE
        Z(J1,0,3)=Z(J1,0,3)/2
   31 CONTINUE

      DO 41 J1=1,JM
        Z(J1,0,4)=0
        DO 40 J2=1,JM-1
          T1=X(J1)-Y(J2)
          T2=X(J1)+Y(J2)
          Z(J1,J2,4)=(SIN((2*JM+1)*T1)/SIN(T1)
     &               -SIN((2*JM+1)*T2)/SIN(T2))/(2*JM)
   40   CONTINUE
   41 CONTINUE

      END
