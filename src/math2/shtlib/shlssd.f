***********************************************************************
*     TRANSFORM SM TO SD
***********************************************************************
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SHLSSD(MM,M,ISW,SM,SD,R)

      REAL SM(M:MM)
      REAL SD(0:MM+1)
      REAL R((MM+1)*(MM+1))

*     ISW=-1: X-DIFFERENTIAL, ISW=0: NORMAL, ISW=1: Y-DIFFERENTIAL     

      IF(ISW.EQ.0) THEN
        DO 10 N=M,MM
          SD(N)=SM(N)
   10   CONTINUE
        SD(MM+1)=0
      ELSE IF(ISW.EQ.-1) THEN
        DO 20 N=M,MM
          SD(N)=M*SM(N)
   20   CONTINUE
        SD(MM+1)=0
      ELSE
        LB=M*(2*MM-M)+M
        IF(M.NE.MM) THEN
          N=M
          SD(N)=(N+2)*R(LB+N+1)*SM(N+1)
          DO 30 N=M+1,MM-1
            SD(N)=-(N-1)*R(LB+N)*SM(N-1)+(N+2)*R(LB+N+1)*SM(N+1)
   30     CONTINUE
          N=MM
          SD(N)=-(N-1)*R(LB+N)*SM(N-1)
          N=MM+1
          SD(N)=-(N-1)*R(LB+N)*SM(N-1)
        ELSE IF(M.EQ.MM) THEN
          N=MM
          SD(N)=0
          N=MM+1
          SD(N)=-(N-1)*R(LB+N)*SM(N-1)
        END IF
      END IF

      END
