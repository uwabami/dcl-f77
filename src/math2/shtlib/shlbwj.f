***********************************************************************
*     BACKWARD LEGENDRE TRANSFORMATION (AT A LATITUDE)
***********************************************************************
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SHLBWJ(MM,JM,M,J,ISW,SM,WMJ,SD,PMJ,YC,R)

      REAL SM(M:MM),WMJ
      REAL SD(0:MM+1),PMJ(0:MM+1)
      REAL YC(0:JM)
      REAL R((MM+1)*(MM+1))
      REAL WS,WA

*     ISW=-1: X-DIFFERENTIAL, ISW=0: NORMAL, ISW=1: Y-DIFFERENTIAL

      CALL SHLSSD(MM,M,ISW,SM,SD,R)

      CALL SHLBWL(MM,M,SD,WS,WA,PMJ)

      JD=ABS(J)

      IF((ISW.EQ.0).AND.(M.NE.0)) THEN
        WS=WS*YC(JD)
        WA=WA*YC(JD)
      ELSE IF((ISW.NE.0).AND.(M.EQ.0)) THEN
        IF(JD.EQ.JM) THEN
          WS=0
          WA=0
        ELSE
          WS=WS/YC(JD)
          WA=WA/YC(JD)
        END IF
      END IF

      IF(J.GE.0) THEN
        WMJ=WS+WA
      ELSE
        WMJ=WS-WA
      END IF

      END
