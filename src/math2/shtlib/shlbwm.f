***********************************************************************
*     BACKWARD LEGENDRE TRANSFORMATION (MIDDLE LEVEL)
***********************************************************************
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SHLBWM(MM,JM,M,ISW,SM,WM,SD,PM,YC,R)

      REAL SM(M:MM)
      REAL WM(-JM:JM)
      REAL SD(0:MM+1),PM(0:MM+1,0:JM)
      REAL YC(0:JM)
      REAL R((MM+1)*(MM+1))
      REAL WS,WA

*     ISW=-1: X-DIFFERENTIAL, ISW=0: NORMAL, ISW=1: Y-DIFFERENTIAL

      IF((ISW.EQ.-1).AND.(M.EQ.0)) THEN
        DO 10 J=-JM,JM
          WM(J)=0
   10   CONTINUE
      ELSE
        CALL SHLSSD(MM,M,ISW,SM,SD,R)
        DO 20 J=0,JM
          CALL SHLBWL(MM,M,SD,WS,WA,PM(0,J))
          WM( J)=WS+WA
          WM(-J)=WS-WA
   20   CONTINUE
        IF((ISW.EQ.0).AND.(M.NE.0)) THEN
          DO 30 J=1,JM
            WM( J)=WM( J)*YC(J)
            WM(-J)=WM(-J)*YC(J)
   30     CONTINUE
        ELSE IF((ISW.EQ.1).AND.(M.EQ.0)) THEN
          DO 40 J=1,JM-1
            WM( J)=WM( J)/YC(J)
            WM(-J)=WM(-J)/YC(J)
   40     CONTINUE
          WM( JM)=0
          WM(-JM)=0
        END IF
      END IF

      END
