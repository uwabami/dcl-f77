*-----------------------------------------------------------------------
      PROGRAM FFTL01

      PARAMETER (N=12,NW=N/2,NWS=3*N+15)

      REAL      RX(N),AK(NW),BK(NW),WSAVE(NWS)

      DATA      RX/ 0, 1, 2, 3, 4, 3, 2, 1, 0, -1, -2, -1/
*     DATA      RX/ 0, 1, 2, 3, 2, 1, 0, -1, -2, -3, -2, -1/


      CALL EZFFTI(N,WSAVE)
      WRITE(*,'(A,12F5.1)') ' LIST OF RX : ',RX
      CALL EZFFTF(N,RX,A0,AK,BK,WSAVE)
      WRITE(*,'(A,7F8.3)') ' LIST OF CX : ',A0,(AK(I),I=1,NW)
      WRITE(*,'(A,TR8,6F8.3)') ' LIST OF SX : ',(BK(I),I=1,NW)
      CALL EZFFTB(N,RX,A0,AK,BK,WSAVE)
      WRITE(*,'(A,12F5.1)') ' LIST OF RX : ',RX

      END
