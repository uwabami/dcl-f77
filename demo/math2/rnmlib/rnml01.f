*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM RNML01

      PARAMETER (N=12)

      REAL      RX(N),RY(N)

      DATA      RX/ 0, 10, 20, 30, 40, 30, 20, 10, 0, -10, -20, -10/


      WRITE(*,'(A,12F5.0)') ' LIST OF RX : ',RX
      CALL VRRNM(RX,RY,N,1,1,3)
      WRITE(*,'(A)') ' AFTER CALLING VRRNM(RX,RY,12,1,1,3)'
      WRITE(*,'(A,12F5.0)') ' LIST OF RY : ',RY

      END
