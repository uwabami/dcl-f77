*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM VSTL01

      PARAMETER (IX=3,NN=4)

      INTEGER   NW1(IX)
      REAL      WZ1(IX,2),X(IX,NN)

      DATA      X/ 1, 3,-1, 2,-4, 0, 2, 1, 3, 2, 1, 0/


      WRITE(*,'(A)') ' *** TEST FOR VS1INT / VS1DIN / VS1OUT'
      CALL VS1INT(WZ1,NW1,IX)
      DO 10 J=1,NN
        WRITE(*,100) ' INPUT = ',(X(I,J),I=1,IX)
        CALL VS1DIN(WZ1,NW1,IX,X(1,J))
   10 CONTINUE
      CALL VS1OUT(WZ1,NW1,IX)
      WRITE(*,100) ' AVE.  = ',(WZ1(I,1),I=1,IX)
      WRITE(*,100) ' VAR.  = ',(WZ1(I,2),I=1,IX)

  100 FORMAT(A,3F6.1)

      END
