*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM VSTL02

      PARAMETER (IX=3,IY=3,NN=4)

      INTEGER   NW2(IX,IY)
      REAL      WZ2(IX,IY,5),X(IX,NN),Y(IY,NN)

      DATA      X/ 1, 3,-1, 2,-4, 0, 2, 1, 3, 2, 1, 0/
      DATA      Y/ 0, 1, 0, 1,-2, 1, 3, 0, 2, 1,-1, 0/


      WRITE(*,'(A)') ' *** TEST FOR VS2INT / VS2DIN / VS2OUT'
      CALL VS2INT(WZ2,NW2,IX,IY)
      DO 20 J=1,NN
        WRITE(*,100) ' INPUT(X) = ',(X(I,J),I=1,IX)
        WRITE(*,100) ' INPUT(Y) = ',(Y(I,J),I=1,IY)
        CALL VS2DIN(WZ2,NW2,IX,IY,X(1,J),Y(1,J))
   20 CONTINUE
      CALL VS2OUT(WZ2,NW2,IX,IY)
      WRITE(*,110) ' AVE. (X) = ',((WZ2(I,J,1),I=1,IX),J=1,IY)
      WRITE(*,110) ' AVE. (Y) = ',((WZ2(I,J,2),I=1,IX),J=1,IY)
      WRITE(*,110) ' VAR. (X) = ',((WZ2(I,J,3),I=1,IX),J=1,IY)
      WRITE(*,110) ' VAR. (Y) = ',((WZ2(I,J,4),I=1,IX),J=1,IY)
      WRITE(*,110) ' COV(X,Y) = ',((WZ2(I,J,5),I=1,IX),J=1,IY)

  100 FORMAT(A,3F6.1)
  110 FORMAT(A12,3F6.1,2(/TR12,3F6.1))

      END
