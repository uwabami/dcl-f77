*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM INTR01

      PARAMETER (N=12)

      REAL      RX(N)

      DATA      RX/ 0, 1, 2, 999, 4, 3, 999, 999, 0, -1, -2, 999/


      WRITE(*,'(A,12F5.0)') ' LIST OF RX : ',RX
      CALL VRINTR(RX,N,1)
      WRITE(*,'(A)') ' AFTER CALLING VRINTR(RX,12,1)'
      WRITE(*,'(A,12F5.0)') ' LIST OF RX : ',RX

      END
