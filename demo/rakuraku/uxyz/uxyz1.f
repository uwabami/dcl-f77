*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM UXYZ1

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN( IWS )
      CALL GRFRM

      CALL GRSWND( -180., 180., -90., 90. )
      CALL GRSVPT(   0.2,  0.8,  0.3, 0.7 )
      CALL GRSTRN( 1 )
      CALL GRSTRF

      CALL UXAXDV( 'B', 10., 60. )
      CALL UXAXDV( 'T', 10., 60. )
      CALL UXSTTL( 'B', 'LONGITUDE', 0. )
      CALL UXSTTL( 'B', '<- WEST      EAST ->', 0. )

      CALL UYAXDV( 'L', 10., 30. )
      CALL UYAXDV( 'R', 10., 30. )
      CALL UYSTTL( 'L', 'LATITUDE', 0. )
      CALL UYSTTL( 'L', '<- SH    NH ->', 0. )

      CALL UXMTTL( 'T', 'UXAXDV/UYAXDV', 0. )

      CALL GRCLS

      END
