*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM UXYZ6

      PARAMETER( ID0=19911201, ND=720, RND=ND )

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN( IWS )
      CALL UZFACT( 0.8 )
      CALL GRFRM

      CALL GRSWND( -180., 180., RND, 0.0 )
      CALL GRSVPT(   0.2,  0.8, 0.2, 0.8 )
      CALL GRSTRN( 1 )
      CALL GRSTRF

      CALL UXAXDV( 'B', 10., 60. )
      CALL UXAXDV( 'T', 10., 60. )
      CALL UXSTTL( 'B', 'LONGITUDE', 0. )

      CALL UCYACL( 'L', ID0, ND )

      CALL UZLSET( 'LABELYR', .TRUE. )
      CALL UYAXDV( 'R', 20., 100. )
      CALL UZISET( 'IROTCYR', -1 )
      CALL UYSTTL( 'R', 'DAY NUMBER', 0. )

      CALL UXMTTL( 'T', 'UXAXDV/UCYACL', 0. )

      CALL GRCLS

      END
