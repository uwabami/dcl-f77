*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM UXYZ2

      WRITE(*,*) ' WORKSTATION ID (I) ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN( IWS )
      CALL GRFRM

      CALL GRSWND( 1.E0, 1.E5, 1.E3, 1.E0 )
      CALL GRSVPT(  0.2,  0.8,  0.2,  0.8 )
      CALL GRSTRN( 4 )
      CALL GRSTRF

      CALL ULXLOG( 'B', 1, 9 )
      CALL ULXLOG( 'T', 1, 9 )
      CALL UXSTTL( 'B', '[X]', 1. )

      CALL ULYLOG( 'L', 3, 9 )
      CALL ULYLOG( 'R', 3, 9 )
      CALL UYSTTL( 'L', '[Y]', 1. )

      CALL UXMTTL( 'T', 'ULXLOG/ULYLOG', 0. )

      CALL GRCLS

      END
