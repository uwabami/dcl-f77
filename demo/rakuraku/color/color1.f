*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM COLOR1

      PARAMETER( NP=4, NT=10 )
      PARAMETER( DD=0.07, DS=0.003 )
      PARAMETER( DX=DD+DS*2, DY=1.0/NT )

      REAL      XBOX(NP), YBOX(NP)
      CHARACTER CHR*5

      DATA      XBOX/ 0., 1., 1., 0. /
      DATA      YBOX/ 0., 0., 1., 1. /

      WRITE(*,*) ' WORKSTATION ID (I) ? ; '
      CALL SGPWSN
      READ (*,*) IWS

      CALL GLISET( 'MAXMSG', 300 )

      CALL SGOPN( -ABS(IWS) )

      CALL SGLSET( 'LFULL' , .TRUE. )
      CALL SLRAT( 0.85, 1. )
      CALL SLMGN( 0., 0., 0.05, 0.1 )
      CALL SLSTTL( 'TEST OF COLORMAP', 'T', 0., -0.5, 0.025, 1 )

      DO 20 K=1,80

      CALL SWISET('ICLRMAP',K)
*      IF (MOD(K,2).EQ.1) THEN
*        CALL SWLSET('LFGBG',.FALSE.)
*      ELSE
*        CALL SWLSET('LFGBG',.TRUE.)
*      END IF
      CALL SWCLCH
      CALL SGFRM
      CALL SGSWND( 0., 1., 0., 1. )
      CALL SGSTRN( 1 )

      CALL SGSTXS( 0.01 )
      CALL SGSTXC( 1 )
      DO 10 J=0,9
      DO 10 I=0,9
        X1 = DX*I     + DS + (1-DX*NT)/2
        Y1 = DY*(9-J) + DS
        X2 = X1 + DD
        Y2 = Y1 + DD
        ITPAT = (I+J*10)*1000 + 999

        CALL SGSVPT( X1, X2, Y1, Y2 )
        CALL SGSTRF

        WRITE(CHR,'(I5)') ITPAT
        CALL SGIGET('IBGCLI',IBC)
        IF(ITPAT.EQ.999)THEN
*          ITPAT=IBC*1000 + 999
        ENDIF

        CALL SLPVPR( 1 )
        CALL SGSTNP( ITPAT )
        CALL SGTNU( NP, XBOX, YBOX )
        CALL SGTXV( X2, Y2+(DY-DD)/2, CHR )
   10 CONTINUE
   20 CONTINUE
        CALL SGCLS

        END
