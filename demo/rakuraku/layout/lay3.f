*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM LAY3

      WRITE(*,*) 'WORKSTATION ID ? '
      CALL SGPWSN
      READ (*,*) IWS

      CALL SGOPN( IWS )

      CALL SGLSET( 'LFULL', .TRUE. )
      CALL SLMGN( 0., 0., 0.08, 0.08 )
      CALL SLRAT( 1., 0.6 )
      CALL SLSTTL( 'FIGURE TITLE', 'T',  0.,  0., 0.03, 1 )
      CALL SLSTTL( 'PROGRAM.NAME', 'B', -1., -1., 0.02, 2 )
      CALL SLSTTL( 'page:#PAGE',   'B',  1., -1., 0.02, 3 )

      CALL SGFRM
      CALL SLPVPR( 1 )
      CALL SGTXV( 0.5, 0.5, 'FIGURE' )

      CALL SGCLS

      END
