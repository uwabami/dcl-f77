*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM LAY1

      CHARACTER*7  CTXT
      DATA CTXT/'FRAME??'/

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL SGOPN( IWS )

      CALL SLMGN( 0.1, 0.1, 0.05, 0.05 )
      CALL SLDIV( 'Y', 3, 2 )
      CALL SLMGN( 0.05, 0.05, 0.05, 0.05 )

      DO 10 I=1,12
        CALL SGFRM
        CALL SLPVPR( 1 )
        WRITE(CTXT(6:7),'(I2.2)') I
        CALL SGTXV( 0.5, 0.5, CTXT )
   10 CONTINUE

      CALL SGCLS

      END
