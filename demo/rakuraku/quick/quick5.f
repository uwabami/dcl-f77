*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM QUICK5

      PARAMETER ( NX=21, NY=21 )
      PARAMETER ( XMIN=-1, XMAX=1, YMIN=-1, YMAX=1 )
      REAL U(NX,NY), V(NX,NY)

*-- 2次元ベクトルデータ: 変形場 ----
      DO 20 J=1,NY
        DO 10 I=1,NX
          X = XMIN + (XMAX-XMIN)*(I-1)/(NX-1)
          Y = YMIN + (YMAX-YMIN)*(J-1)/(NY-1)
          U(I,J) =  X
          V(I,J) = -Y
   10   CONTINUE
   20 CONTINUE

*-- グラフ ----
      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN( IWS )
      CALL GRFRM

      CALL GRSWND( XMIN, XMAX, YMIN, YMAX )
      CALL GRSVPT(  0.2,  0.8,  0.2,  0.8 )
      CALL GRSTRN( 1 )
      CALL GRSTRF

      CALL USDAXS
      CALL UGVECT( U, NX, V, NX, NX, NY )

      CALL GRCLS

      END
