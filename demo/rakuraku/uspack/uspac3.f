*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM USPAC3

      PARAMETER( NMAX=50, XMIN=1950, XMAX=2000 )
      REAL Y(0:NMAX)

      R    = 3.7
      Y(0) = 0.5
      DO 10 N=0,NMAX-1
        Y(N+1) = R*Y(N)*(1.-Y(N))
   10 CONTINUE

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN( IWS )
      CALL GRFRM

      CALL GLRGET( 'RUNDEF', RUNDEF )
      CALL GRSWND( XMIN, XMAX, RUNDEF, RUNDEF )
      CALL USSTTL( 'TIME', 'YEAR', 'HEAT FLUX', 'W/m\^2' )
      CALL USGRPH( NMAX+1, RUNDEF, Y )

      CALL GRCLS

      END
