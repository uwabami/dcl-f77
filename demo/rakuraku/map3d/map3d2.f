*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM MAP3D2

      PARAMETER( NX=37, NY=37 )
      PARAMETER( XMIN=0, XMAX=360, YMIN=-90, YMAX=90 )
      PARAMETER( PI=3.14159, DRAD=PI/180 )
      REAL P(NX,NY)

      DO 10 J=1,NY
      DO 10 I=1,NX
        ALON = ( XMIN + (XMAX-XMIN)*(I-1)/(NX-1) ) * DRAD
        ALAT = ( YMIN + (YMAX-YMIN)*(J-1)/(NY-1) ) * DRAD
        SLAT = SIN(ALAT)
        P(I,J) = 3*SQRT(1-SLAT**2)*SLAT*COS(ALON) - 0.5*(3*SLAT**2-1)
   10 CONTINUE

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN( IWS )
      CALL SGLSET( 'LSOFTF', .FALSE. )
      CALL GRFRM

      CALL SGSWND( XMIN, XMAX, YMIN, YMAX )
      CALL SGSVPT( 0.1, 0.9, 0.1, 0.9 )
      CALL SGSSIM( 0.4, 0., 0. )
      CALL SGSMPL( 135., 35., 0. )
      CALL SGSTXY( -180., 180., 0., 90. )
      CALL SGSTRN( 30 )
      CALL SGSTRF

      DO 20 K=-5,3
        TLEV1 = 0.4*K
        TLEV2 = TLEV1 + 0.4
        IF(K.LE.-1) THEN
          IPAT = 600 + ABS(K+1)
        ELSE
          IPAT =  30 + K
        END IF
        CALL UESTLV( TLEV1, TLEV2, IPAT )
   20 CONTINUE

      CALL UETONE( P, NX, NX, NY )

      CALL UDGCLB( P, NX, NX, NY, 0.4 )
      CALL UDCNTR( P, NX, NX, NY )

      CALL UMPMAP( 'coast_world' )
      CALL UMPGLB

      CALL GRCLS

      END
