*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM MAP3D6

      PARAMETER( NX=37, NY=37 )
      PARAMETER(  XMIN=   0,  XMAX=360,  YMIN= -90,  YMAX= 90 )
      PARAMETER( VXMIN=-0.4, VXMAX=0.4, VYMIN=-0.3, VYMAX=0.3 )
      PARAMETER(  ZMIN=-1.9,  ZMAX=1.9 )
      PARAMETER( VZMIN=-0.2, VZMAX=0.2 )
      PARAMETER( PI=3.14159, DRAD=PI/180 )

      REAL ALON(NX), ALAT(NY), P(NX,NY)
      REAL XP(3), YP(3), ZP(3)

      DO 10 I=1,NX
        ALON(I) = XMIN + (XMAX-XMIN)*(I-1)/(NX-1)
   10 CONTINUE
      DO 20 J=1,NY
        ALAT(J) = YMIN + (YMAX-YMIN)*(J-1)/(NY-1)
   20 CONTINUE

      DO 30 J=1,NY
        SLAT = SIN(ALAT(J)*DRAD)
        DO 30 I=1,NX
          P(I,J) = 3*SQRT(1-SLAT**2)*SLAT*COS(ALON(I)*DRAD)
     +           - 0.5*(3*SLAT**2-1)
   30 CONTINUE

      WRITE(*,*) ' WORKSTATION ID (I) ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL SGOPN( IWS )
      CALL SGFRM

*-- X-Y PLANE ----

      CALL SGSWND(  XMIN,  XMAX,  YMIN,  YMAX )
      CALL SGSVPT( VXMIN, VXMAX, VYMIN, VYMAX )
      CALL SGSTRN( 1 )
      CALL SGSTRF

      CALL SCSPLN( 1, 2, VZMIN)
      CALL SCSEYE( -1.1, -1.1, 2.5 )
      CALL SCSOBJ(  0.0,  0.0, 0.0 )
      CALL SCSPRJ

      CALL UXAXDV( 'B', 10., 60. )
      CALL UXAXDV( 'T', 10., 60. )
      CALL UXSTTL( 'B', 'LONGITUDE', 0. )
      CALL UYAXDV( 'L', 10., 30. )
      CALL UYAXDV( 'R', 10., 30. )
      CALL UYSTTL( 'L', 'LATITUDE', 0. )

*-- X-Z PLANE ----

      CALL SGSWND(  XMIN,  XMAX,  ZMIN,  ZMAX )
      CALL SGSVPT( VXMIN, VXMAX, VZMIN, VZMAX )
      CALL SGSTRN( 1 )
      CALL SGSTRF

      CALL SCSPLN( 1, 3, VYMAX)
      CALL SCSPRJ

      CALL UZINIT
      CALL UXAXDV( 'T', 10., 60. )
      CALL UZLSET( 'LABELXB', .FALSE. )
      CALL UXAXDV( 'B', 10., 60. )
      CALL UYAXDV( 'L', 0.2, 1.0 )
      CALL UYAXDV( 'R', 0.2, 1.0 )
      CALL UYSTTL( 'L', 'AMPLITUDE', 0. )

*-- Y-Z PLANE ----

      CALL SGSWND(  YMIN,  YMAX,  ZMIN,  ZMAX )
      CALL SGSVPT( VYMIN, VYMAX, VZMIN, VZMAX )
      CALL SGSTRN( 1 )
      CALL SGSTRF

      CALL SCSPLN( 2, 3, VXMAX)
      CALL SCSPRJ

      CALL UZINIT
      CALL UXAXDV( 'T', 10., 30. )
      CALL UXAXDV( 'B', 10., 30. )
      CALL UZLSET( 'LABELYL', .FALSE. )
      CALL UYAXDV( 'L', 0.2, 1.0 )
      CALL UYAXDV( 'R', 0.2, 1.0 )

*---------------- 3-D ------------------

      CALL SCSVPT(VXMIN, VXMAX, VYMIN, VYMAX, VZMIN, VZMAX)
      CALL SCSWND( XMIN,  XMAX,  YMIN,  YMAX,  ZMIN,  ZMAX)
      CALL SCSLOG(.FALSE., .FALSE., .FALSE.)
      CALL SCSTRN(1)
      CALL SCSTRF

      CALL SZL3OP(1)
C      CALL SZT3OP(2999,4999)

      DO 40 J=NY-1, 1, -1
      DO 40 I=NX-1, 1, -1
        XP(1) = ALON(I)
        YP(1) = ALAT(J)
        ZP(1) = P(I,J)
        XP(2) = ALON(I)
        YP(2) = ALAT(J+1)
        ZP(2) = P(I,J+1)
        XP(3) = ALON(I+1)
        YP(3) = ALAT(J+1)
        ZP(3) = P(I+1,J+1)

C        CALL SZT3ZU(XP, YP, ZP)
        CALL SZL3ZU(3, XP, YP, ZP)

        XP(1) = ALON(I+1)
        YP(1) = ALAT(J+1)
        ZP(1) = P(I+1,J+1)
        XP(2) = ALON(I+1)
        YP(2) = ALAT(J)
        ZP(2) = P(I+1,J)
        XP(3) = ALON(I)
        YP(3) = ALAT(J)
        ZP(3) = P(I,J)

C        CALL SZT3ZU(XP, YP, ZP)
        CALL SZL3ZU(3, XP, YP, ZP)

   40 CONTINUE

      CALL SZL3CL
C      CALL SZT3CL

      CALL SGCLS

      END
