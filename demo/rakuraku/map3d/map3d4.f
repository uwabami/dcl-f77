*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM MAP3D4

      PARAMETER(  XMIN= -50,  XMAX= 50,  YMIN= -50,  YMAX= 50 )
      PARAMETER( VXMIN=0., VXMAX=0.8, VYMIN=0., VYMAX=0.8 )
      PARAMETER(  ZMIN= -50,  ZMAX= 50 )
      PARAMETER( VZMIN=0., VZMAX=0.8 )
      PARAMETER( XVP3=2.5, YVP3=-1., ZVP3=1.5 )
      PARAMETER( XFC3=(VXMAX-VXMIN)/2, YFC3=(VYMAX-VYMIN)/2 )
      PARAMETER( ZFC3=(VZMAX-VZMIN)/2 )

      WRITE(*,*) ' WORKSTATION ID (I) ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL SGOPN( IWS )
      CALL SGISET( 'IFONT'   , 2 )

      CALL SGFRM

*-- X-Y PLANE ----
      CALL SGSWND(  XMIN,  XMAX,  YMIN,  YMAX )
      CALL SGSVPT( VXMIN, VXMAX, VYMIN, VYMAX )
      CALL SGSTRN( 1 )
      CALL SGSTRF

      CALL SCSPLN( 1, 2, VZMAX)
      CALL SCSEYE( XVP3, YVP3, ZVP3 )
      CALL SCSOBJ( XFC3, YFC3, ZFC3 )
      CALL SCSPRJ

      CALL APLOT( 1 )

*-- X-Z PLANE ----
      CALL SGSWND(  XMIN,  XMAX,  ZMIN,  ZMAX )
      CALL SGSVPT( VXMIN, VXMAX, VZMIN, VZMAX )
      CALL SGSTRN( 1 )
      CALL SGSTRF

      CALL SCSPLN( 1, 3, VYMIN)
      CALL SCSPRJ

      CALL APLOT( 2 )

*-- Y-Z PLANE ----
      CALL SGSWND(  YMIN,  YMAX,  ZMIN,  ZMAX )
      CALL SGSVPT( VYMIN, VYMAX, VZMIN, VZMAX )
      CALL SGSTRN( 1 )
      CALL SGSTRF

      CALL SCSPLN( 2, 3, VXMAX)
      CALL SCSPRJ

      CALL APLOT( 3 )

      CALL SGCLS

      END
*-----------------------------------------------------------------------
      SUBROUTINE APLOT( IJK )

      PARAMETER( NMAX=40 )
      REAL X(0:NMAX), Y(0:NMAX)
      CHARACTER CTTL*2

      CALL SLPVPR( 1 )

      DT = 2.*3.14159 / NMAX
      DO 10 N=0,NMAX
        X(N) = 40.*SIN(N*DT)
        Y(N) = 40.*COS(N*DT)
   10 CONTINUE

      CALL SGPLU( NMAX+1, X, Y )

      DT = 2.*3.14159 / 3
      DO 20 N=0,3
        X(N) = 40.*SIN(N*DT)
        Y(N) = 40.*COS(N*DT)
   20 CONTINUE

      CALL SGPLU( 4, X, Y )

      CALL SGSTXS( 0.07 )
      CALL SGTXU( 0., 0., 'DENNOU' )
      WRITE(CTTL(1:2), '(I2.2)') IJK
      CALL SGTXU( 0., -30., CTTL )

      RETURN
      END
