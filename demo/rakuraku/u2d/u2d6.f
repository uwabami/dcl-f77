*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM U2D6

      PARAMETER( NX=11, NY=11 )
      PARAMETER( XMIN=-1, XMAX=1, YMIN=-1, YMAX=1 )
      REAL U(NX,NY), V(NX,NY)

      DO 10 J=1,NY
      DO 10 I=1,NX
        X = XMIN + (XMAX-XMIN)*(I-1)/(NX-1)
        Y = YMIN + (YMAX-YMIN)*(J-1)/(NY-1)
        U(I,J) =  X * 0.1
        V(I,J) = -Y
   10 CONTINUE

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN( IWS )
      CALL GRFRM

      CALL GRSWND( XMIN, XMAX, YMIN, YMAX )
      CALL GRSVPT(  0.2,  0.8,  0.2,  0.8 )
      CALL GRSTRN( 1 )
      CALL GRSTRF

      CALL USDAXS

      CALL UGLSET( 'LNRMAL', .FALSE. )
      CALL UGRSET( 'XFACT1', 0.5 )
      CALL UGRSET( 'YFACT1', 0.05 )
      CALL UGLSET( 'LUNIT' , .TRUE. )
      CALL UGRSET( 'VXUNIT', 0.1 )
      CALL UGRSET( 'VYUNIT', 0.1 )
      CALL UGRSET( 'VXUOFF', 0.06 )
      CALL UGSUT( 'X', 'U' )
      CALL UGSUT( 'Y', 'V' )
      CALL UGVECT( U, NX, V, NX, NX, NY )

      CALL GRCLS

      END
