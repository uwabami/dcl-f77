*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM SGPK07

      PARAMETER (N=9)
      DIMENSION Y(N)
      CHARACTER GREEK1*30, GREEK2*30, SYMBOL*30, USGI*3


      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ(*,*) IWS

      CALL SGLSET('LCNTL',.TRUE.)

      CALL SGOPN(IWS)
      CALL SGFRM

      X1 = 0.1
      X2 = 0.9
      XC = 0.5

      CALL SGSLNI(1)
      DO 10 I=1, N
        Y(I) = 0.1 * (10-I)
        CALL SGLNV(X1, Y(I), X2, Y(I))
  10  CONTINUE
      CALL SGLNV(XC, 0.05, XC, 0.95)

*----------------------- SUPER/SUB SCRIPT ------------------------------

      CALL SGTXV(XC, Y(1), 'SGTXV\^{SUP}RST\_{SUB}')

      CALL SGSTXI(2)
      CALL SGLSET('LCNTL', .TRUE.)               ! <-- 添字コントロールON
      CALL SGTXV(XC, Y(2), 'SGTXV\^{SUP}RST\_{SUB}')

*------------------------ OVER UNDER -----------------------------------
      CALL SGRSET('SHIFT', 0.5)                  ! <-- シフト量設定
      CALL SGRSET('SMALL', 0.5)                  ! <-- 添字の大きさ設定
      CALL SWLSET('LSYSFNT', .TRUE.)
      CALL SGTXV(XC, Y(3), 'S\o-G\o~T\u-X\o~\u-V\^{SUP}RST\_{SUB}')
      CALL SWLSET('LSYSFNT', .FALSE.)

*------------------------ FONT SELECTION -------------------------------

      CALL SGSTXI(1)
      CALL SGSTXS(0.05)
      CALL SGTXV(XC, Y(4), 'ABCDEFG abcdefg')

      CALL SGISET('IFONT', 2)                    ! <-- きれいなフォント
      CALL SGTXV(XC, Y(5), 'ABCDEFG abcdefg')

      CALL SGSTXI(3)
      CALL SGTXV(XC, Y(6), 'ABCDEFG abcdefg')

*------------------------- GREEK LETTERS -------------------------------

      GREEK1 = USGI(152)//USGI(153)//USGI(154)//USGI(155)//USGI(156)//
     #         USGI(157)//USGI(158)//USGI(159)//USGI(160)//USGI(161)
      GREEK2 = USGI(130)//USGI(131)//USGI(135)//USGI(138)//USGI(141)//
     #         USGI(143)//USGI(145)//USGI(148)//USGI(150)//USGI(151)

      CALL SGTXV(XC, Y(7), GREEK1)
      CALL SGTXV(XC, Y(8), GREEK2)

*----------------------------- SYMBOLS ---------------------------------

      SYMBOL = USGI(189)//USGI(190)//USGI(191)//USGI(192)//USGI(193)//
     #         USGI(210)//USGI(211)//USGI(212)//USGI(217)//USGI(218)
      CALL SGTXV(XC, Y(9), SYMBOL)

      CALL SGCLS

      END
