*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM KIHON8

      PARAMETER( XMIN=1., XMAX=100., YMIN=1., YMAX=100. )

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL SGOPN( IWS )
      CALL SGLSET( 'LCLIP', .TRUE. )

      CALL SGFRM
*-- 直角一様座標(左上) ----
      CALL SGSWND( XMIN, XMAX, YMIN, YMAX )
      CALL SGSVPT(  0.1,  0.4,  0.6,  0.9 )
      CALL SGSTRN( 1 )
      CALL SGSTRF

      CALL BPLOT

*-- 片対数(y)座標(右上) ----
      CALL SGSWND( XMIN, XMAX, YMIN, YMAX )
      CALL SGSVPT(  0.6,  0.9,  0.6,  0.9 )
      CALL SGSTRN( 2 )
      CALL SGSTRF

      CALL BPLOT

*-- 片対数(x)座標(左下) ----
      CALL SGSWND( XMIN, XMAX, YMIN, YMAX )
      CALL SGSVPT(  0.1,  0.4,  0.1,  0.4 )
      CALL SGSTRN( 3 )
      CALL SGSTRF

      CALL BPLOT
      
*-- 対数座標(右下) ----
      CALL SGSWND( XMIN, XMAX, YMIN, YMAX )
      CALL SGSVPT(  0.6,  0.9,  0.1,  0.4 )
      CALL SGSTRN( 4 )
      CALL SGSTRF

      CALL BPLOT

      CALL SGCLS

      END
*-----------------------------------------------------------------------
      SUBROUTINE BPLOT

      PARAMETER( NMAX=50 )
      REAL X(NMAX), Y(NMAX)

      CALL SLPVPR( 1 )

*-- 一次関数 ----
      DO 10 N=1,NMAX
        X(N) = 2.*N
        Y(N) = X(N)
   10 CONTINUE

      CALL SGSPLT( 1 )
      CALL SGPLU( NMAX, X, Y )

*-- 指数関数 ----
      DO 20 N=1,NMAX
        Y(N) = EXP(0.05*X(N))
   20 CONTINUE

      CALL SGSPLT( 2 )
      CALL SGPLU( NMAX, X, Y )

*-- 対数関数 ----
      DO 30 N=1,NMAX
        Y(N) = 20.*LOG(X(N))
   30 CONTINUE

      CALL SGSPLT( 3 )
      CALL SGPLU( NMAX, X, Y )

*-- 文字列 ----
      CALL SGSTXS( 0.02 )
      CALL SGTXU( 30., 40., '(30,40)' )

      RETURN
      END
