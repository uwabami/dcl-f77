*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM KIHON3

      PARAMETER( NMAX=40, IMAX=7 )
      REAL X(0:NMAX), Y(0:NMAX,IMAX)

      DT = 4.* 3.14159 / NMAX
      DO 20 N=0,NMAX
        X(N) = REAL(N)/REAL(NMAX)
        DO 10 I=1,IMAX
          Y(N,I) = 0.2*SIN(N*DT) + 0.9 - 0.1*I
   10   CONTINUE
   20 CONTINUE

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL SGOPN( IWS )

*-- マーカータイプ: frame 1 --
      CALL SGFRM
      CALL SLPVPR( 1 )

      CALL SGPMV( NMAX+1, X, Y(0,1) )

      DO 30 I=2,IMAX
        ITYPE = I
        CALL SGSPMT( ITYPE )
        CALL SGPMV( NMAX+1, X, Y(0,I) )
   30 CONTINUE

*-- マーカーサイズ: frame 2 --
      CALL SGFRM
      CALL SLPVPR( 1 )

      CALL SGSPMT( 10 )
      CALL SGPMV( NMAX+1, X, Y(0,1) )

      DO 40 I=2,IMAX
        RSIZE = 0.005*(I-1)
        CALL SGSPMS( RSIZE )
        CALL SGPMV( NMAX+1, X, Y(0,I) )
   40 CONTINUE

      CALL SGCLS

      END
