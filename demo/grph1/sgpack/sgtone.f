*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM SGTONE

      PARAMETER (NP=5,NT=6)
      PARAMETER (DD=0.13,DS=0.005)
      PARAMETER (DX=DD+DS*2,DY=1.0/NT)

      REAL      XBOX(NP),YBOX(NP)
      CHARACTER CHR*5,CTTL*32

      DATA      XBOX/ 0.0, 1.0, 1.0, 0.0, 0.0/
      DATA      YBOX/ 0.0, 0.0, 1.0, 1.0, 0.0/


      WRITE(*,*) ' WORKSTATION ID (I) ? ; '
      CALL SGPWSN
      READ(*,*) IW
      WRITE(*,*) ' SOFT FILL=1/HARD FILL=2 (I) ? ;'
      READ(*,*) IFN
      WRITE(*,*) ' TONE PATTERN (0-6) (I) ? ;'
      READ(*,*) N

      CALL GLISET('MAXMSG',300)

      CALL SGOPN(IW)

      IF (IFN.EQ.1) THEN
        CALL SGLSET('LSOFTF',.TRUE.)
      ELSE
        CALL SGLSET('LSOFTF',.FALSE.)
      END IF
      CALL SGLSET('LFULL',.TRUE.)
      CALL SGISET('INDEX',3)
      CALL SLRAT(0.85,1.0)
      CALL SLMGN(0.0,0.0,0.05,0.1)
      CTTL='TONE PATTERN = #XX'
      WRITE(CTTL(16:16),'(I1)') N
      CALL SLSTTL(CTTL,'T',0.0,-0.5,0.03,1)

      CALL SGFRM
      CALL SGSWND(0.0,1.0,0.0,1.0)
      CALL SGSTRN(1)
      DO 20 J=0,5
        DO 10 I=0,5
          X1=DX*I+DS+(1-DX*NT)/2
          X2=X1+DD
          Y1=DY*(5-J)+DS
          Y2=Y1+DD
          LEVEL=I+J*10+N*100
          CALL SGSVPT(X1,X2,Y1,Y2)
          CALL SGSTRF
          WRITE(CHR,'(I5)') LEVEL
          CALL SGTNZU(NP,XBOX,YBOX,LEVEL)
          CALL SGPLZU(NP,XBOX,YBOX,1,1)
          CALL SGTXZV(X2,Y2+(DY-DD)/2,CHR,0.015,0,1,3)
   10   CONTINUE
   20 CONTINUE

      CALL SGCLS

      END
