*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM SGPK10

      PARAMETER ( ND=12, DDEG=360.0/ND, DD=0.25 )
      PARAMETER ( PI=3.141592 )

      REAL      RX(ND), RY(ND)


      DO 10 N = 1, ND
        RX(N) = DD*N*COS(PI/180*DDEG*(N-1))
        RY(N) = DD*N*SIN(PI/180*DDEG*(N-1))
   10 CONTINUE

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ(*,*) IWS

      CALL SGOPN( IWS )

      CALL SGFRM

      CALL SGSWND( 0.0, 10.0, 0.0, 10.0 )
      CALL SGSVPT( 0.0, 1.0, 0.0, 1.0 )
      CALL SGSTRN( 1 )
      CALL SGSTRF

      X1 = 3.0
      Y1 = 7.0
      DO 15 N = 1, ND
        CALL SGLAZU( X1, Y1, X1+RX(N), Y1+RY(N), 1, 2 )
   15 CONTINUE

      CALL SGLSET( 'LPROP', .FALSE. )
      CALL SGRSET( 'CONST', 0.05 )
      X1 = 7.0
      Y1 = 7.0
      DO 20 N = 1, ND
        CALL SGLAZU( X1, Y1, X1+RX(N), Y1+RY(N), 1, 2 )
   20 CONTINUE

      DO 25 I = 1, 8
        CALL SGRSET( 'ANGLE', 10.0*I )
        CALL SGLAZU( REAL(I), 2.0, REAL(I)+1, 3.0, 1, 2 )
   25 CONTINUE

      CALL SGCLS

      END
