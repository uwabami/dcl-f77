*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM SGPK08

      REAL UPX3(3), UPY3(3), UPX6(6), UPY6(6), UPXS(61), UPYS(61)


      A = 0.8
      TH = 3.14159 * 2 / 3
      DO 100 I=1, 3
        UPX3(I) = A*SIN(TH*I)
        UPY3(I) = A*COS(TH*I)
  100 CONTINUE

      TH = 3.14159 * 2 / 6
      DO 200 I=1, 6
        UPX6(I) = A*SIN(TH*I)
        UPY6(I) = A*COS(TH*I)
  200 CONTINUE

      TH = 3.14159 * 4 / 60
      DO 300 I=1, 61
        UPXS(I) = A*(I-31) / 30.
        UPYS(I) = A*SIN(TH*(I-1))
  300 CONTINUE

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ(*,*) IWS

      CALL SGOPN(IWS)

      CALL SGLSET('LSOFTF',.TRUE.)           ! <-- ソフトフィルの指定
      CALL SGFRM

      CALL SGSWND(-1., 1., -1., 1.)
      CALL SGSVPT(0., 0.5, 0., 0.5)
      CALL SGSTRN( 1)
      CALL SGSTRF

      CALL SGPLU(3, UPX3, UPY3)
      CALL SGTNU(3, UPX3, UPY3)              ! <-- 網かけ (左下)

      CALL SGSVPT(0., 0.5, 0.5, 1.)
      CALL SGSTRF
      CALL SGSTNP(101)
      CALL SGTNU(6, UPX6, UPY6)              ! <-- 横線 (左上)

      CALL SGSVPT(0.5, 1., 0., 0.5)
      CALL SGSTRF
      CALL SGTNZU(6, UPX6, UPY6, 201)        ! <-- 斜線 (右下)

      CALL SGSVPT(0.5, 1., 0.5, 1.)
      CALL SGSTRF
      CALL SGTNZU(61, UPXS, UPYS, 601)       ! <-- 横線 (右上)

      CALL SGCLS

      END
