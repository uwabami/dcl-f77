*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM SGKSX1


      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ(*,*) IWS

      CALL SGOPN( IWS )

*     / FIRST PAGE /

      CALL SGFRM

      CALL SGQWND( UX1, UX2, UY1, UY2 )

      UXC=(UX1+UX2)/2
      UYC=(UY1+UY2)/2

      CALL SGLNZU( UX1, UY1, UX2, UY1, 1 )
      CALL SGLNZU( UX2, UY1, UX2, UY2, 1 )
      CALL SGLNZU( UX2, UY2, UX1, UY2, 1 )
      CALL SGLNZU( UX1, UY2, UX1, UY1, 1 )

      CALL SGTXZU( UXC, UYC, 'SGKS TEST : PAGE 1', 0.03, 0, 0, 3 )

*     / SECOND PAGE /

      CALL SGLSET( 'LFULL', .TRUE. )

      CALL SGFRM

      CALL SGQWND( UX1, UX2, UY1, UY2 )

      UXC=(UX1+UX2)/2
      UYC=(UY1+UY2)/2

      CALL SGLNZU( UX1, UY1, UX2, UY1, 1 )
      CALL SGLNZU( UX2, UY1, UX2, UY2, 1 )
      CALL SGLNZU( UX2, UY2, UX1, UY2, 1 )
      CALL SGLNZU( UX1, UY2, UX1, UY1, 1 )

      CALL SGTXZV( UXC, UYC, 'SGKS TEST : PAGE 2', 0.03, 0, 0, 3 )

      CALL SGCLS

      END
