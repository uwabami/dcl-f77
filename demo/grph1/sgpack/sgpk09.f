*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM SGPK09

      PARAMETER ( KMAX=4, NN=73 )
      PARAMETER ( DD=2.0, PI=3.141592 )

      REAL      XORG(KMAX), YORG(KMAX), UX(NN), UY(NN)

      DATA      XORG / 2.5, 7.5, 2.5, 7.5 /
      DATA      YORG / 7.5, 7.5, 2.5, 2.5 /


      WRITE(*,*) ' WORKSTATION ID (I) ? ;'
      CALL SGPWSN
      READ(*,*) IWS

      CALL SGOPN( IWS )

      CALL SGFRM

      CALL SGSWND( 0.0, 10.0, 0.0, 10.0 )
      CALL SGSVPT( 0.0, 1.0, 0.0, 1.0 )
      CALL SGSTRN( 1 )
      CALL SGSTRF

      CALL SGSTXS( 0.03 )
      CALL SGSTXI( 3 )
      CALL SGSTXC( 0 )

      CALL SGSPLC( 'K=1' )

      DO 20 K = 1, KMAX

        CALL SGSLNI( 1 )
        CALL SGLNU( XORG(K)-DD, YORG(K), XORG(K)+DD, YORG(K) )
        CALL SGLNU( XORG(K), YORG(K)-DD, XORG(K), YORG(K)+DD )

        DO 10 I = 1, NN
          TH = 2*PI*(I-1)/(NN-1)
          UX(I) = XORG(K) + DD*COS(TH+(K-1)*PI/7)
          UY(I) = YORG(K) + DD*SIN(K*TH)
   10   CONTINUE

        CALL SGSPLI( 2 )
        CALL SGSPLT( K )
        CALL SGLSET( 'LCHAR', .TRUE. )
        CALL SGPLU( NN, UX, UY )
        CALL SGLSET( 'LCHAR', .FALSE. )
        CALL SGNPLC

   20 CONTINUE

      CALL SGCLS

      END
