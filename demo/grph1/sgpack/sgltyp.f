*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM SGLTYP

      PARAMETER (N=2)

      REAL      X(N),Y(N)
      CHARACTER CPAT*16,CTTL*16


      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ(*,*) IWS

      CALL SGOPN(IWS)

      CALL SGFRM

      CALL SGTXZV(0.5,0.9,'LINE TYPE',0.04,0,0,3)

      X(1)=0.4
      X(2)=0.8

      DO 10 I=1,5

        IF (1.LE.I .AND. I.LE.4) THEN
          ITYPE=I
        ELSE
          CPAT='0011111111001001'
          CALL BITPCI(CPAT,ITYPE)
          CALL SGSPLT(ITYPE)
        END IF

        CTTL='ITYPE = #####'
        WRITE(CTTL(9:13),'(I5)') ITYPE

        Y(1)=0.7-(I-1)*0.12
        Y(2)=Y(1)

        CALL SGTXZV(0.1,Y(1),CTTL,0.02,0,-1,3)
        IF (I.EQ.5) THEN
          CALL SGTXZV(0.1,Y(1)-0.05,'('//CPAT//')',0.015,0,-1,3)
        END IF
        CALL SGPLZV(N,X,Y,ITYPE,3)

   10 CONTINUE

      CALL SGCLS

      END
