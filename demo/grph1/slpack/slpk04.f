*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM SLPK04

      REAL X(5), Y(5)
      CHARACTER USGI*3

      DATA X / -7.5, 7.5, 7.5, -7.5, -7.5/
      DATA Y / -7.5,-7.5, 7.5,  7.5, -7.5/

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ(*,*) IWS

      CALL SGOPN(IWS)

      XMAXF = 26.
      YMAXF = 18.5
      CALL SLFORM(XMAXF, YMAXF)        ! <-- 絶対的な用紙の大きさを指定
      CALL SLRAT(XMAXF, YMAXF)

      CALL SGLSET('LFULL', .TRUE.)
      CALL SGFRM

      CALL SGSWND(-XMAXF/2., XMAXF/2., -YMAXF/2., YMAXF/2.)
      CALL SGSTRF
      CALL SGPLU(5, X, Y)
      CALL SGTXZU(0., 0., '15cm '//USGI(194)//' 15cm', 0.05, 0, 0, 3)

      CALL SGCLS

      END
