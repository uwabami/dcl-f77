*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM SCPKT7

      PARAMETER( NX=37 )
      PARAMETER(  XMIN=   0,  XMAX=360,  YMIN=-1,  YMAX= 1 )
      PARAMETER( PI=3.14159, DRAD=PI/180 )

      REAL ALON(NX), ALAT(NX), R(NX), A(NX)


      DO 10 I=1,NX
        ALON(I) = XMIN + (XMAX-XMIN)*(I-1)/(NX-1)
        ALAT(I) = YMIN + (YMAX-YMIN)*(I-1)/(NX-1)
        R   (I) = 1.
   10 CONTINUE

      WRITE(*,*) ' WORKSTATION ID (I) ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL SGOPN( IWS )
      CALL SGFRM
      CALL SGLSET('LDEG', .TRUE.)

*---------------- 3-D ------------------

      CALL SCSORG(1., 0., 0., 0.)
      CALL SCSTRN(2)
      CALL SCSTRF

      CALL SCSEYE(  5., 1.,  2. )
      CALL SCSOBJ(  0.0,  0.0,  0.0 )
      CALL SCSPRJ

      DO 100 I=1, NX
        DO 110 J=1, NX
          A(J) = ALAT(I)
  110   CONTINUE
        CALL SCPLU(NX, R, ALON, A)
  100 CONTINUE

      DO 200 I=1, NX
        DO 210 J=1, NX
          A(J) = ALON(I)
  210   CONTINUE
        CALL SCPLU(NX, R, A, ALAT)
  200 CONTINUE

      CALL SGCLS

      END
