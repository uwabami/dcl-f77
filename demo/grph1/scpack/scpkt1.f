*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM SCPKT1


      WRITE(*,*) ' WORKSTATION IS (I) ? ;'
      CALL SGPWSN
      READ(*,*) IWS

      CALL SGOPN(IWS)
      CALL SGLSET('LCLIP',.TRUE.)

      DT = RFPI()/10.
      DO 10 I=1, 21
        CALL SGFRM
        CALL SLPWWR(1)
        TH = (I-1)*DT
        XEYE=10.0*COS(TH)
        YEYE=10.0*SIN(TH)
        ZEYE=2.0
        CALL SCSOBJ(0.0,0.0,0.0)
        CALL SCSEYE(XEYE,YEYE,ZEYE)
        CALL SCSPRJ
        CALL CUBE
   10 CONTINUE

      CALL SGCLS

      END
*-----------------------------------------------------------------------
      SUBROUTINE CUBE

      PARAMETER (NMAX = 5)

      DIMENSION X(NMAX), Y(NMAX)

      DATA X / -1.,  1., 1., -1., -1. /
      DATA Y / -1., -1., 1.,  1., -1. /


      DO 10 I=2, NMAX
        I1 = I-1
        I2 = I
        CALL SZOPL3
        CALL SZMVL3(X(I1), Y(I1),  1.)
        CALL SZPLL3(X(I2), Y(I2),  1.)   
        CALL SZPLL3(X(I2), Y(I2), -1.)   
        CALL SZPLL3(X(I1), Y(I1), -1.)   
        CALL SZCLL3
   10 CONTINUE

      END

