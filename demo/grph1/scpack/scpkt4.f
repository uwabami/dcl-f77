*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM SCPKT4

      PARAMETER (N=8, PI=3.14159)


      WRITE(*,*) ' WORKSTATION ID (I) ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL SGOPN(IWS)
      CALL SGFRM
      CALL SGLSET('L2TO3', .TRUE.)
*     CALL SCSEYE( -0.5, -0.5, 2.0)
*     CALL SCSOBJ(  0.5,  0.5, 0.5)
      CALL SCSPRJ
      CALL SGSTRF

      DT = 2.*PI/N
      A  = 0.5

      CALL SZL3OP(1)
      CALL SZT3OP(2999,3999)
      DO 10 I=1, N
        T1 = DT*(I-1)
        T2 = DT*I
        X1 = A*SIN(T1) + 0.5
        X2 = A*SIN(T2) + 0.5
        Y1 = A*COS(T1) + 0.5
        Y2 = A*COS(T2) + 0.5
        CALL BOX (X1, Y1, X2, Y2)
   10 CONTINUE

      CALL SZL3CL
      CALL SZT3CL

      CALL SGCLS

      END
*-----------------------------------------------------------------------
      SUBROUTINE BOX(X1, Y1, X2, Y2)

      PARAMETER (N = 3, Z1=0.1, Z2=-0.1)

      DIMENSION X(N), Y(N), Z(N)


      X(1) = X1
      X(2) = X1
      X(3) = X2
      Y(1) = Y1
      Y(2) = Y1
      Y(3) = Y2
      Z(1) = Z1
      Z(2) = Z2
      Z(3) = Z2

      CALL SZT3ZV(X, Y, Z)
      CALL SZL3ZV(3, X, Y, Z)

      X(1) = X2
      X(2) = X2
      X(3) = X1
      Y(1) = Y2
      Y(2) = Y2
      Y(3) = Y1
      Z(1) = Z2
      Z(2) = Z1
      Z(3) = Z1

      CALL SZT3ZV(X, Y, Z)
      CALL SZL3ZV(3, X, Y, Z)

      END
