*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM SCPKT3

      PARAMETER ( NX=73, NY=37 )
      PARAMETER ( XMIN=0, XMAX=360, YMIN=-90, YMAX=90 )

      REAL      P1(NX,NY), P2(NX,NY)

      EXTERNAL  ISGTRC


      WRITE(*,*) ' WORKSTATION ID (I) ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      OPEN(11,FILE='t810630.dat',FORM='FORMATTED')
      OPEN(12,FILE='t811231.dat',FORM='FORMATTED')
      DO 10 J=1,NY
        READ(11,'(10F8.3)') (P1(I,J),I=1,NX)
        READ(12,'(10F8.3)') (P2(I,J),I=1,NX)
   10 CONTINUE
      CLOSE(11)
      CLOSE(12)

      DO 20 IR=190,245,5
        R=IR
        AMIN=R
        AMAX=R+5
*       IDX=(R-180)*1.4*1000+999
        IDX=INT((R-170)*1.25)*1000+999
        CALL UESTLV(AMIN,AMAX,IDX)
   20 CONTINUE 

      CALL SGOPN( IWS )

      CALL SGLSET( 'LSOFTF', .FALSE. )
      CALL UMLSET( 'LGRIDMJ', .FALSE. )
      CALL UMRSET( 'DGRIDMN', 30.0 )
      CALL UMISET( 'ITYPEMN', 1 )
      CALL UMISET( 'ITYPEOUT', 1 )
      CALL SGFRM

      CALL SGLSET( 'L2TO3', .TRUE. )
*     CALL SCSEYE(-1.0, -1.5,  2.0 )
      CALL SCSOBJ( 0.5,  0.5,  0.0 )
      CALL SCSPLN( 1, 2, -0.3)
      CALL SCSPRJ

      CALL SGSSIM( 0.12, 0.0, 0.0 )
      CALL SGSMPL( 180.0, 90.0, 0.0 )
      CALL SGSVPT( 0.1, 0.9, 0.1, 0.9 )
      CALL SGSWND( XMIN, XMAX, YMIN, YMAX )
      CALL SGSTXY( -180.0, 180.0, -90.0, 90.0 )
      CALL SGSTRN( ISGTRC('MER') )
      CALL SGSTRF

      CALL SGLSET( 'LCLIP', .TRUE. )
      CALL SLPWWR( 1 )

      CALL UETONE( P1, NX, NX, NY )
      CALL UMPMAP( 'coast_world' )
      CALL UMPGLB
      CALL SLPVPR( 1 )

      CALL SGLSET( 'LCLIP', .FALSE. )
      CALL SGTXZV( 0.5, 0.95, 'Temp. : 10 hPa : Jun 30, 1981',
     +             0.025, 0, 0, 3 )

      CALL SCSPLN( 1, 2, 0.4)
*     CALL SGRSET( 'YOFF3', -0.25 )
      CALL SCSPRJ

      CALL SGLSET( 'LCLIP', .TRUE. )
      CALL SLPWWR( 1 )

      CALL UETONE( P2, NX, NX, NY )
      CALL UMPMAP( 'coast_world' )
      CALL UMPGLB
      CALL SLPVPR( 1 )

      CALL SGLSET( 'LCLIP', .FALSE. )
      CALL SGTXZV( 0.5, 0.95, 'Temp. : 10 hPa : Dec 31, 1981',
     +             0.025, 0, 0, 3 )

      CALL SGCLS

      END
