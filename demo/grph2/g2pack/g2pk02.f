*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM G2PK02

      PARAMETER(NX=15,NY=15)

      REAL UX(NX), UY(NY)
      REAL UYW(NX), UXW(NY)
      REAL CX(NX,NY), CY(NX,NY)
      REAL Z(NX,NY)
      REAL TERRAIN(NX)


*     / SET PARAMETERS /

      CALL GLRGET('RUNDEF',RUNDEF)

      DO 10 I=1,NX
        UX(I)=(I-1.0)/(NX-1.0) - 0.5
        TERRAIN(I) = 0.1 * EXP(-24*UX(I)**2)
   10 CONTINUE
      DO 15 J=1,NY
        UY(J)=(J-1.0)/(NY-1.0)
   15 CONTINUE

      CX(1,1) = RUNDEF
      DO 25 J=1,NY
        DO 20 I=1,NX
          CY(I,J) = UY(J)*(1.0-TERRAIN(I)) + TERRAIN(I)
   20   CONTINUE
   25 CONTINUE

*     / GRAPHIC /

      WRITE(*,*) ' WORKSTATION ID (I) ? ;'
      CALL SGPWSN
      READ(*,*) IWS

      CALL GROPN(IWS)
      CALL GRFRM
      CALL GRSVPT(0.15,0.85,0.15,0.85)
      CALL GRSWND(UX(1),UX(NX),UY(1),UY(NY))
      CALL GRSTRN(51)
      CALL G2SCTR(NX, NY, UX,UY, CX,CY)
      CALL GRSTRF

*     / TONE /

      DO 35 J=1,NY
        DO 30 I=1,NX
          Z(I,J) = UX(I) * (1-UY(J))
   30   CONTINUE
   35 CONTINUE

      CALL UELSET('LTONE',.TRUE.)
      CALL UWSGXA(UX,NX)
      CALL UWSGYA(UY,NY)
      CALL UETONE(Z, NX, NX, NY)

*     / GRID LINES /

      DO 45 J=1,NY
        DO 40 I=1,NX
          UYW(I) = UY(J)
   40   CONTINUE
        CALL SGPLU(NX,UX,UYW)
   45 CONTINUE

      DO 55 I=1,NX
        DO 50 J=1,NY
          UXW(J) = UX(I)
   50   CONTINUE
        CALL SGPLU(NY,UXW,UY)
   55 CONTINUE

*     / AXES  (Switch to ITR==1) /

      CALL G2QCTM(CXMIN, CXMAX, CYMIN, CYMAX)
      CALL GRSWND(CXMIN,CXMAX,CYMIN,CYMAX)
      CALL GRSTRN(1)
      CALL GRSTRF
      CALL USDAXS
      CALL UXSTTL('T','TERRAIN FOLLOWING',0.0)

      CALL GRCLS

      END
