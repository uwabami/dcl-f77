*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM GRPK01


*     WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
*     CALL SGPWSN
*     READ (*,*) IWS
      IWS=1

      CALL GROPN(IWS)
      CALL GRFRM

      CALL GRSWND(0.0, 1.0, -0.7, 0.7)
      CALL GRSVPT(0.2, 0.8,  0.2, 0.6)
      CALL GRSTRN(1)
      CALL GRSTRF
      CALL USSTTL('X-AXIS', ' ', 'Y1-AXIS', ' ')
      CALL USDAXS
      CALL SGTXZU(0.5, 0.0, 'PAGE:1, FRAME:1', 0.02, 0, 0, 3)

      CALL GRFIG

      CALL GRSWND(0.0, 1.0, -0.5, 0.5)
      CALL GRSVPT(0.2, 0.8, 0.62, 0.82)
      CALL GRSTRN(1)
      CALL GRSTRF
      CALL UZLSET('LABELXB', .FALSE.)
      CALL USSTTL('X-AXIS', ' ', 'Y2-AXIS', ' ')
      CALL USDAXS
      CALL SGTXZU(0.5, 0.0, 'PAGE:1, FRAME:2', 0.02, 0, 0, 3)
      CALL UZLSET('LABELXB', .TRUE.)

      CALL GRFRM

      CALL UZFACT(0.5)

      CALL GRSWND(0.0, 1.0, -0.7, 0.7)
      CALL GRSVPT(0.2, 0.8,  0.2, 0.6)
      CALL GRSTRN(1)
      CALL GRSTRF
      CALL USSTTL('X-AXIS', ' ', 'Y1-AXIS', ' ')
      CALL USDAXS
      CALL SGTXZU(0.5, 0.0, 'PAGE:2, FRAME:1', 0.02, 0, 0, 3)

      CALL GRFIG

      CALL GRSWND(0.0, 1.0, -0.5, 0.5)
      CALL GRSVPT(0.2, 0.8, 0.62, 0.82)
      CALL GRSTRN(1)
      CALL GRSTRF
      CALL UZLSET('LABELXB', .FALSE.)
      CALL USSTTL('X-AXIS', ' ', 'Y2-AXIS', ' ')
      CALL USDAXS
      CALL SGTXZU(0.5, 0.0, 'PAGE:2, FRAME:2', 0.02, 0, 0, 3)
      CALL UZLSET('LABELXB', .TRUE.)

      CALL GRCLS

      END
