*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM UUPK03

      PARAMETER (N=30)
      DOUBLE PRECISION A, R
      REAL Y(N)

*---------------------------- DATA DEFINITION --------------------------

      R = 0.2D0
      A = 3.7D0
      DO 100 I=1, N
        R = A*R*(1.D0-R)
        Y(I) = R
  100 CONTINUE
      Y(N/2) = 999.
      CALL GLLSET('LMISS', .TRUE.)
      CALL GLRGET('RUNDEF', RUNDEF)

*----------------------------- GRAPH -----------------------------------

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN(IWS)
      CALL GRFRM

      CALL GRSWND(0., 1., RUNDEF, RUNDEF)
      CALL USSPNT(N, RUNDEF, Y)
      CALL USPFIT
      CALL GRSTRF

      CALL USDAXS

      CALL UVBXL(N, RUNDEF, Y)

      CALL GRFRM

      CALL GRSWND(RUNDEF, RUNDEF, 0., 1.)
      CALL USSPNT(N, Y, RUNDEF)
      CALL USPFIT
      CALL GRSTRF

      CALL USDAXS

      CALL UHBXL(N, Y, RUNDEF)

      CALL GRCLS

      END
