*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM UUPK02

      PARAMETER(N=20, M=5)
      REAL X0(N), X1(N), X2(N), Y0(N), Y1(N), Y2(N), A(M)

*-----------------------------------------------------------------------

      DT = 1./(N-1)
      PI = 3.14159
      DO 50 J=1, M
        JJ = J*2-1
        A(J) = (-1)**J *2./(JJ*PI)
  50  CONTINUE

      DO 100 I=1, N
        T     = DT*(I-1)*2*PI
        X0(I) = DT*(I-1)
        Y1(I) = A(1)*COS(T)
        Y2(I) = 0.
        DO 150 J=1, M
          JJ = J*2-1
          YY = A(J)*COS(JJ*T)
          Y2(I)  = Y2(I) + YY
  150   CONTINUE

        Y0(I) = (Y1(I)+Y2(I))/2.
        DY = ABS(Y1(I) - Y2(I))
        X1(I) = X0(I) - DY/5
        X2(I) = X0(I) + DY/5

  100 CONTINUE

*-----------------------------------------------------------------------

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN(IWS)
      CALL GRFRM
      CALL SGLSET('LCLIP', .TRUE.)

      CALL USSPNT(N, X0, Y0)
      CALL USSPNT(N, X1, Y1)
      CALL USSPNT(N, X2, Y2)
      CALL USPFIT
      CALL GRSTRF

      CALL USDAXS

      CALL UVERB(N, X0, Y1, Y2)

      CALL UUSEBT(3)
      CALL UUSEBI(33)
      CALL UHERB(N, X1, X2, Y0)

      CALL UUSMKT(4)
      CALL UUSMKI(21)
      CALL UUMRK(N, X0, Y0)

      CALL GRCLS

      END
