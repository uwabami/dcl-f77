*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM UUPK06

      PARAMETER(N=100, M=5)
      REAL Y1(N), Y2(N), A(M)

*-----------------------------------------------------------------------

      DT = 1./(N-1)
      PI = 3.14159
      DO 50 J=1, M
        JJ = J*2-1
        A(J) = (-1)**J *2./(JJ*PI)
  50  CONTINUE

      DO 100 I=1, N
        T     = DT*(I-1)*2*PI
        Y1(I) = A(1)*COS(T)
        Y2(I) = 0.
        DO 150 J=1, M
          JJ = J*2-1
          YY = A(J)*COS(JJ*T)
          Y2(I)  = Y2(I) + YY
  150   CONTINUE


  100 CONTINUE

*-----------------------------------------------------------------------

      CALL GLRGET('RUNDEF', RUNDEF)
      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN(IWS)
      CALL GRFRM
      CALL SGLSET('LCLIP', .TRUE.)

      CALL USSPNT(N, RUNDEF, Y1)
      CALL USSPNT(N, RUNDEF, Y2)
      CALL GRSWND(0., 1., RUNDEF, RUNDEF)
      CALL USPFIT
      CALL GRSTRF

      CALL USDAXS

      CALL UUSARP(5999, 4999)
      CALL UVDIF(N, RUNDEF, Y1, Y2)
      CALL UULIN(N, RUNDEF, Y1)
      CALL UULIN(N, RUNDEF, Y2)

      CALL GRFRM
      CALL SGLSET('LCLIP', .TRUE.)

      CALL USSPNT(N, Y1, RUNDEF)
      CALL USSPNT(N, Y2, RUNDEF)
      CALL GRSWND(RUNDEF, RUNDEF, 0., 1.)
      CALL USPFIT
      CALL GRSTRF

      CALL USDAXS

      CALL UHDIF(N, Y1, Y2, RUNDEF)
      CALL UULIN(N, Y1, RUNDEF)
      CALL UULIN(N, Y2, RUNDEF)

      CALL GRCLS

      END
