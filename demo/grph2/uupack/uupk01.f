*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM UUPK01

      PARAMETER (N=30)
      DOUBLE PRECISION A, R
      REAL Y(N)

*---------------------------- DATA DEFINITION --------------------------

      R = 0.2D0
      A = 3.7D0
      DO 100 I=1, N
        R = A*R*(1.D0-R)
        Y(I) = R
  100 CONTINUE

      Y(N/2) = 999.
      CALL GLLSET('LMISS', .TRUE.)
      CALL GLRGET('RUNDEF', RUNDEF)

*----------------------------- GRAPH -----------------------------------

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN(IWS)
      CALL GRFRM
      CALL SGLSET('LCLIP', .TRUE.)

      CALL GRSWND(0., 1., RUNDEF, RUNDEF)
      CALL USSPNT(N, RUNDEF, Y)
      CALL USPFIT
      CALL GRSTRF

      CALL USDAXS

      CALL UUSLNI(2)
      CALL UULIN(N, RUNDEF, Y)

      CALL UUSMKT(4)
      CALL UUMRK(N, RUNDEF, Y)

      CALL GRCLS

      END
