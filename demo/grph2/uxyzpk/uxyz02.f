*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM UXYZ02


      WRITE(*,*) ' WORKSTATION ID (I) ? ;'
      CALL SGPWSN
      READ(*,*) IWS

      CALL GROPN( IWS )

      CALL GRFRM

      CALL SGSWND( 1.0E0, 1.0E5, 1.0E3, 1.0E0 )
      CALL SGSVPT( 0.2, 0.8, 0.2, 0.8 )
      CALL SGSTRN( 4 )
      CALL SGSTRF

      CALL ULXLOG( 'B', 1, 9 )
      CALL ULXLOG( 'T', 1, 9 )
      CALL UXSTTL( 'B', '[X]', 1.0 )

      CALL ULYLOG( 'L', 3, 9 )
      CALL ULYLOG( 'R', 3, 9 )
      CALL UYSTTL( 'L', '[Y]', 1.0 )

      CALL UXMTTL( 'T', 'ULXLOG/ULYLOG', 0.0 )

      CALL GRCLS

      END
