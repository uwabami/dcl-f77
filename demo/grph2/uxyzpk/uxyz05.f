*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM UXYZ05

      PARAMETER ( ID0=19910401, ND=90 )


      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ(*,*) IWS

      CALL GROPN( IWS )

      CALL GRFRM

      RND=ND
      CALL SGSWND( 0.0, RND, -1.0, +1.0 )
      CALL SGSVPT( 0.2, 0.8, 0.2, 0.8 )
      CALL SGSTRN( 1 )
      CALL SGSTRF

      CALL UCXACL( 'B', ID0, ND )
      CALL UCXACL( 'T', ID0, ND )

      CALL UYAXDV( 'L', 0.05, 0.25 )
      CALL UZLSET( 'LABELYR', .TRUE. )
      CALL UYSFMT( '(F6.2)' )
      CALL UYAXDV( 'R', 0.05, 0.25 )
      CALL UYSTTL( 'L', 'CORRILATION', 0.0 )

      CALL UXMTTL( 'T', 'UCXACL/UYAXDV', 0.0 )

      CALL GRCLS

      END
