*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM UXYZ10

      PARAMETER ( ID0=19811201, ND=180, RND=ND )


      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ(*,*) IWS

      CALL GROPN( IWS )

      CALL UZFACT( 0.7 )
      CALL UZLSET( 'LOFFSET',.TRUE.)

      CALL GRFRM

      CALL SGSWND( 0.0, RND, 0.0, 100.0 )
      CALL SGSVPT( 0.4, 0.9, 0.3, 0.8 )
      CALL SGSTRN( 1 )
      CALL SGSTRF

      CALL SLPVPR( 1 )

      CALL UCXACL( 'B', ID0, ND )
      CALL UXSAXS( 'B' )
      CALL UXAXDV( 'B', 10.0, 20.0 )
      CALL UXSTTL( 'B', 'DAY NUMBER', 0.0 )
      CALL UCXACL( 'T', ID0, ND )

      CALL UYAXDV( 'L', 5.0, 10.0 )
      CALL UYAXDV( 'R', 5.0, 10.0 )
      CALL UYSTTL( 'L', 'CELSIUS SCALE', 0.0 )
      CALL UYSAXS( 'L' )
      CALL UZRSET( 'YOFFSET', 273.15 )
      CALL UZRSET( 'YFACT  ', 1.0 )
      CALL UYAXDV( 'L', 5.0, 10.0 )
      CALL UYSTTL( 'L', 'KELVIN SCALE', 0.0 )
      CALL UYSAXS( 'L' )
      CALL UZRSET( 'YOFFSET', 32.0 )
      CALL UZRSET( 'YFACT  ', 1.8 )
      CALL UYAXDV( 'L', 10.0, 20.0 )
      CALL UYSTTL( 'L', 'FAHRENHEIT SCALE', 0.0 )

      CALL UXSTTL( 'T', '( LOFSET=.TRUE. )', 0.0 )
      CALL UXSTTL( 'T', 'UXSAXS/UYSAXS', 0.0 )

      CALL GRCLS

      END
