*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM SGFNDL

      CHARACTER FONTNAME*80,CTEXT*80

      EXTERNAL  LENC,ISGC


      CALL SGOPN(1)
      CALL SGFRM

      CTEXT='Please choose a font set...'
      CALL SGTXZV(0.5, 0.75, CTEXT, 0.04, 0, 0, 3)
      CTEXT='(This is drawn by DCL stroke font)'
      CALL SGTXZV(0.5, 0.70, CTEXT, 0.03, 0, 0, 3)

      CALL SWLSET('LSYSFNT', .TRUE.)

      CALL SWSLFT(FONTNAME)
      CTEXT='Selected font name: '//FONTNAME
      CALL SGTXZV(0.5, 0.50, CTEXT, 0.04, 0, 0, 3)
      CTEXT='(This is drawn by DCL defalt system font)'
      CALL SGTXZV(0.5, 0.45, CTEXT, 0.03, 0, 0, 3)

      CALL SWCSET('FONTNAME', FONTNAME)

      CTEXT='GFD Dennou Common Library 電脳ライブラリ αβΣΩ'
      CALL SGTXZV(0.5, 0.25, CTEXT, 0.03, 0, 0, 3)
      CTEXT='(This is drawn by the selected font)'
      CALL SGTXZV(0.5, 0.20, CTEXT, 0.03, 0, 0, 3)

      CALL SGCLS

      END
