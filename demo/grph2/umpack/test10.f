*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM TEST10

      PARAMETER (NP=20)

      REAL      FCT(NP)
      CHARACTER CTTL*32, CTR(NP)*3

      EXTERNAL  ISGTRC

      DATA CTR /'CYL','MER','MWD','HMR','EK6','KTD',
     +          'MIL','RBS','SIN','VDG',
     +          'CON','COA','COC','BON','PLC',
     +          'OTG','PST','AZM','AZA','GNO'/
      DATA FCT / 0.12, 0.12, 0.14, 0.14, 0.14, 0.14,
     +           0.12, 0.12, 0.12, 0.12,
     +           0.11, 0.16, 0.12, 0.12, 0.12,
     +           0.40, 0.12, 0.12, 0.17, 0.20/


      WRITE(*,*) ' WORKSTATION IS (I) ? ;'
      CALL SGPWSN
      READ(*,*) IWS

      CALL SGOPN(IWS)

      CALL SGRSET( 'STLAT1', 45.0 )
      CALL SGRSET( 'STLAT2', 30.0 )

      DO 10 I=1,NP

        CALL SGFRM

        CALL SGSSIM( FCT(I), 0.0, 0.0 )
        CALL SGSMPL( 0.0, 90.0, 0.0 )
        CALL SGSVPT( 0.1, 0.9, 0.1, 0.9 )
        IF ( CTR(I).EQ.'OTG' .OR. CTR(I).EQ.'GNO' ) THEN
          CALL SGSTXY( -180.0, 180.0,   0.0, 90.0 )
        ELSE
          CALL SGSTXY( -180.0, 180.0, -90.0, 90.0 )
        END IF
        CALL SGSTRN( ISGTRC(CTR(I)) )
        CALL SGSTRF

        CALL SGLSET( 'LCLIP', .TRUE. )
        CALL SLPWWR( 1 )
        CALL SLPVPR( 1 )
        CALL SGTRNL( ISGTRC(CTR(I)), CTTL )
        CALL SGTXZR( 0.5, 0.95, CTTL, 0.03, 0, 0, 3 )

        CALL UMPMAP( 'coast_world' )
        CALL UMPGLB

   10 CONTINUE

      CALL SGCLS

      END
