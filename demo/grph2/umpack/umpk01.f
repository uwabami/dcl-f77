*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM UMPK01

      PARAMETER (NP=14)

      INTEGER   NTR(NP)
      REAL      FCT(NP)
      CHARACTER CTTL*32

      DATA NTR /   10,   11,   12,   13,   14,   15,
     +             20,   21,   22,   23,   30,   31,   32,   33/
      DATA FCT / 0.12, 0.12, 0.14, 0.14, 0.14, 0.14,
     +           0.11, 0.16, 0.12, 0.12, 0.40, 0.12, 0.12, 0.17/


      WRITE(*,*) ' WORKSTATION IS (I) ? ;'
      CALL SGPWSN
      READ(*,*) IWS

      CALL SGOPN( -ABS(IWS) )

      CALL SLRAT( 2.0, 3.0 )
      CALL SLDIV( 'Y', 2, 3 )

      CALL SGRSET( 'STLAT1', 45.0 )
      CALL SGRSET( 'STLAT2', 30.0 )

      CALL UMLSET( 'LGRIDMN', .FALSE. )
      CALL UMISET( 'INDEXMJ', 1 )

      DO 10 I=1,NP

        CALL SGFRM

        CALL SGSSIM( FCT(I), 0.0, 0.0 )
        CALL SGSMPL( 0.0, 90.0, 0.0 )
        CALL SGSVPT( 0.1, 0.9, 0.1, 0.9 )
        IF ( NTR(I).EQ.30 ) THEN
          CALL SGSTXY( -180.0, 180.0,   0.0, 90.0 )
        ELSE
          CALL SGSTXY( -180.0, 180.0, -90.0, 90.0 )
        END IF
        CALL SGSTRN( NTR(I) )
        CALL SGSTRF

        CALL SGLSET( 'LCLIP', .TRUE. )
        CALL SLPWWR( 1 )
        CALL SLPVPR( 1 )
        CALL SGTRNL( NTR(I), CTTL )
        CALL SGTXZR( 0.5, 0.95, CTTL, 0.03, 0, 0, 3 )

        CALL UMPMAP( 'coast_world' )
        CALL UMPGLB

        IF ( NTR(I).EQ.23 ) THEN
          CALL SGFRM
          CALL SGFRM
        END IF

   10 CONTINUE

      CALL SGCLS

      END
