*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM TEST08

      EXTERNAL  ISGTRC


      WRITE(*,*) ' WORKSTATION IS (I) ? ;'
      CALL SGPWSN
      READ(*,*) IWS

      CALL SGLSET('LFULL', .TRUE.)
      CALL GROPN(IWS)
      CALL SLRAT(1., 0.6)
      CALL GRFRM

      CALL GRSMPL(140., 90., 0.)
      CALL GRSTRN(ISGTRC('HMR'))
      CALL UMPFIT
      CALL GRSTRF
      CALL UMPGLB
 
      CALL UMISET('INDEXOUT',3)
      CALL UMPMAP('coast_world')
      CALL UMISET('INDEXOUT',30)
      CALL UMPMAP('border_world')
      CALL UMISET('INDEXOUT',20)
      CALL UMPMAP('plate_world')

      CALL GRCLS

      END
