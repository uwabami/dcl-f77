*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM UCPK01

      PARAMETER (JD0=19920401)

      CHARACTER CTL*32


      WRITE(*,*) ' WORKSTATION ID (I) ? ;'
      CALL SGPWSN
      READ(*,*) IWS

      CALL SGOPN(-ABS(IWS))

      CALL SGLSET('LFULL',.TRUE.)
      CALL UZFACT(0.7)
      CALL SLRAT(0.75,1.0)
      CALL SLDIV('T',1,7)

      CALL SGFRM
      CALL UZINIT
      ND=30
      CALL SGSWND(0.0,REAL(ND),0.0,1.0)
      CALL SGSVPT(0.1,0.9,0.1,0.11)
      CALL SGSTRN(1)
      CALL SGSTRF
      CALL UCXACL('B',JD0,ND)

      CALL SGFRM
      CALL UZINIT
      ND=90
      CALL SGSWND(0.0,REAL(ND),0.0,1.0)
      CALL SGSVPT(0.1,0.9,0.1,0.11)
      CALL SGSTRN(1)
      CALL SGSTRF
      CALL UCXACL('B',JD0,ND)

      CALL SGFRM
      CALL UZINIT
      ND=180
      CALL SGSWND(0.0,REAL(ND),0.0,1.0)
      CALL SGSVPT(0.1,0.9,0.1,0.11)
      CALL SGSTRN(1)
      CALL SGSTRF
      CALL UCXACL('B',JD0,ND)

      CALL SGFRM
      CALL UZINIT
      ND=400
      CALL SGSWND(0.0,REAL(ND),0.0,1.0)
      CALL SGSVPT(0.1,0.9,0.1,0.11)
      CALL SGSTRN(1)
      CALL SGSTRF
      CALL UCXACL('B',JD0,ND)

      CALL SGFRM
      CALL UZINIT
      ND=60
      CALL SGSWND(0.0,REAL(ND),0.0,1.0)
      CALL SGSVPT(0.1,0.9,0.1,0.11)
      CALL SGSTRN(1)
      CALL SGSTRF
      CALL UCXADY('B',JD0,ND)
      CTL='UCXADY (ND=60)'
      CALL UXSTTL('B',CTL,0.0)

      CALL SGFRM
      CALL UZINIT
      ND=120
      CALL SGSWND(0.0,REAL(ND),0.0,1.0)
      CALL SGSVPT(0.1,0.9,0.1,0.11)
      CALL SGSTRN(1)
      CALL SGSTRF
      CALL UCXAMN('B',JD0,ND)
      CTL='UCXAMN (ND=120)'
      CALL UXSTTL('B',CTL,0.0)

      CALL SGFRM
      CALL UZINIT
      ND=2000
      CALL SGSWND(0.0,REAL(ND),0.0,1.0)
      CALL SGSVPT(0.1,0.9,0.1,0.11)
      CALL SGSTRN(1)
      CALL SGSTRF
      CALL UCXAYR('B',JD0,ND)
      CTL='UCXAYR (ND=2000)'
      CALL UXSTTL('B',CTL,0.0)

      CALL SGCLS

      END
