*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM UCPK02

      PARAMETER (JD0=19920401)

      CHARACTER CTL*32


      WRITE(*,*) ' WORKSTATION ID (I) ? ;'
      CALL SGPWSN
      READ(*,*) IWS

      CALL SGOPN(+ABS(IWS))

      CALL SGLSET('LFULL',.TRUE.)
      CALL UZFACT(0.7)
      CALL SLRAT(1.0,0.75)
      CALL SLDIV('Y',7,1)

      CALL SGFRM
      CALL UZINIT
      ND=30
      CALL SGSWND(0.0,1.0,0.0,REAL(ND))
      CALL SGSVPT(0.13,0.14,0.1,0.9)
      CALL SGSTRN(1)
      CALL SGSTRF
      CALL UCYACL('L',JD0,ND)

      CALL SGFRM
      CALL UZINIT
      ND=90
      CALL SGSWND(0.0,1.0,0.0,REAL(ND))
      CALL SGSVPT(0.13,0.14,0.1,0.9)
      CALL SGSTRN(1)
      CALL SGSTRF
      CALL UCYACL('L',JD0,ND)

      CALL SGFRM
      CALL UZINIT
      ND=180
      CALL SGSWND(0.0,1.0,0.0,REAL(ND))
      CALL SGSVPT(0.13,0.14,0.1,0.9)
      CALL SGSTRN(1)
      CALL SGSTRF
      CALL UCYACL('L',JD0,ND)

      CALL SGFRM
      CALL UZINIT
      ND=400
      CALL SGSWND(0.0,1.0,0.0,REAL(ND))
      CALL SGSVPT(0.13,0.14,0.1,0.9)
      CALL SGSTRN(1)
      CALL SGSTRF
      CALL UCYACL('L',JD0,ND)

      CALL SGFRM
      CALL UZINIT
      ND=60
      CALL SGSWND(0.0,1.0,0.0,REAL(ND))
      CALL SGSVPT(0.13,0.14,0.1,0.9)
      CALL SGSTRN(1)
      CALL SGSTRF
      CALL UCYADY('L',JD0,ND)
      CTL='UCYADY (ND=60)'
      CALL UYSTTL('L',CTL,0.0)

      CALL SGFRM
      CALL UZINIT
      ND=120
      CALL SGSWND(0.0,1.0,0.0,REAL(ND))
      CALL SGSVPT(0.13,0.14,0.1,0.9)
      CALL SGSTRN(1)
      CALL SGSTRF
      CALL UCYAMN('L',JD0,ND)
      CTL='UCYAMN (ND=120)'
      CALL UYSTTL('L',CTL,0.0)

      CALL SGFRM
      CALL UZINIT
      ND=2000
      CALL SGSWND(0.0,1.0,0.0,REAL(ND))
      CALL SGSVPT(0.13,0.14,0.1,0.9)
      CALL SGSTRN(1)
      CALL SGSTRF
      CALL UCYAYR('L',JD0,ND)
      CTL='UCYAYR (ND=2000)'
      CALL UYSTTL('L',CTL,0.0)

      CALL SGCLS

      END
