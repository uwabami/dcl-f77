*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM USPK03

      PARAMETER (N=30)
      DOUBLE PRECISION A, R
      REAL Y(N)

*---------------------------- DATA DEFINITION --------------------------

      R = 0.2D0
      A = 3.7D0
      DO 100 I=1, N
        R = A*R*(1.D0-R)
        Y(I) = R
  100 CONTINUE

*----------------------------- GRAPH -----------------------------------

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GLRGET('RUNDEF',RUNDEF)

      CALL GROPN(IWS)
      CALL GRFRM
      CALL GRSWND(0.0, 1.0, RUNDEF, RUNDEF)

      CALL USGRPH(N, RUNDEF, Y)

      CALL GRCLS

*-----------------------------------------------------------------------

      END
