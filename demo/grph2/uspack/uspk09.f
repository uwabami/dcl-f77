*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM USPK09

      PARAMETER(N=200, M=5)
      REAL X(N), Y(N), YC(N,M), A(M)

      DT = 1./(N-1)
      PI = 3.14159
      DO 50 J=1, M
        JJ = J*2-1
        A(J) = (-1)**J *2./(JJ*PI)
  50  CONTINUE

      DO 100 I=1, N
        T    = DT*(I-1)*2*PI
        X(I) = DT*(I-1)
        Y(I) = 0.
        DO 150 J=1, M
          JJ = J*2-1
          YC(I,J) = A(J)*COS(JJ*T)
          Y(I)  = Y(I) + YC(I,J)
  150   CONTINUE
  100 CONTINUE

*--------------------------- 1ST PAGE ----------------------------------

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GLRGET('RUNDEF',RUNDEF)

      CALL GROPN(IWS)
      CALL GRFRM
      CALL GRSVPT(0.2, 0.8, 0.2, 0.6)
      CALL USSPNT(N*M, RUNDEF, YC)

      CALL USSTTL('X-AXIS', ' ', 'COMPONENTS', ' ')
      CALL USGRPH(N, X, YC)

      DO 200 J=2,M
        IP = MOD(J-1,4) + 1
        CALL UULINZ(N, X, YC(1,J), 1, IP)
  200 CONTINUE

*     --- NEW FIG ---
      CALL GRFIG
      CALL GRSVPT(0.2, 0.8, 0.62, 0.82)
      CALL UZLSET('LABELXB', .FALSE.)

      CALL USSTTL('X-AXIS', ' ', 'TOTAL', ' ')
      CALL USGRPH(N, X, Y)

*--------------------------- 2ND PAGE ----------------------------------

      CALL GRFRM
      CALL GRSVPT(0.2, 0.8, 0.2, 0.6)

      CALL UZFACT(0.5)
      CALL UZLSET('LABELXB', .TRUE.)

      CALL USSPNT(N*M, RUNDEF, YC)
      CALL USSTTL('X-AXIS', ' ', 'COMPONENTS', ' ')
      CALL USGRPH(N, X, YC)

      DO 300 J=2,M
        IP = MOD(J-1,4) + 1
        CALL UULINZ(N, X, YC(1,J), 1, IP)
  300 CONTINUE

*     --- NEW FIG ---
      CALL GRFIG
      CALL GRSVPT(0.2, 0.8, 0.62, 0.82)
      CALL UZLSET('LABELXB', .FALSE.)

      CALL USSTTL('X-AXIS', ' ', 'TOTAL', ' ')
      CALL USGRPH(N, X, Y)

      CALL GRCLS

      END
