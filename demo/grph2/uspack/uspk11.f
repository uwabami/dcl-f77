*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM USPK11

      PARAMETER(N=181, N1=26, N2=7, NC=4)
      REAL T(N), Y(N), UX1(N1), UX2(N2)
      DOUBLE PRECISION R, A
      CHARACTER*(NC) CH(N2)

      DATA CH  /'JAN', 'FEB', 'MAR', 'APL', 'JUN', 'JULY', '   '/
      DATA UX2 /   0.,   31.,   59.,   90.,  120.,   151.,  181./

      R = 0.2D0
      A = 4.D0
      Y(1) = 120.
      T(1) = 0.5
      DO 10 I=2, N
        R = A*R*(1.D0-R)
        Y(I) = Y(I-1) + (R-0.46)*2
        T(I) = I - 0.5
   10 CONTINUE

      DO 20 I=1, N1
        UX1(I) = (I-1)*7 + 1
   20 CONTINUE

*-----------------------------------------------------------------------

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GLRGET('RUNDEF',RUNDEF)

      CALL USLSET('LYINV',.TRUE.)

      CALL GROPN(IWS)
      CALL GRFRM

      CALL GRSWND(0., 181., RUNDEF, RUNDEF)
      CALL GRSVPT(0.2, 0.8, 0.2, 0.8)
      CALL GRSTRN(1)

      CALL USSPNT(N, RUNDEF, Y)
      CALL USPFIT

      CALL GRSTRF

*------------------------------- Y-AXIS --------------------------------

      CALL USYAXS('L')
      CALL USYAXS('R')
      CALL UYSTTL('L', 'YEN/DOLLAR', 0.)

*------------------------------- X-AXIS --------------------------------

      CALL UZLSET('LBTWN', .TRUE.)
      CALL UXAXLB('B', UX1, N1, UX2, CH, NC, N2)
      CALL UXAXLB('T', UX1, N1, UX2, CH, NC, N2)
      CALL UXSTTL('T', 'EXCHANGE RATE', 0.)

*-------------------------------- LINE ---------------------------------

      CALL SGPLU(N, T, Y)

      CALL GRCLS

      END
