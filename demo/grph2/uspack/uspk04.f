*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM USPK04

      PARAMETER(N=200, M=5)
      REAL X(N), Y0(N), Y1(N), Y2(N), A(M)

*-----------------------------------------------------------------------

      DT = 1./(N-1)
      PI = 3.14159
      DO 50 J=1, M
        JJ = J*2-1
        A(J) = (-1)**J *2./(JJ*PI)
  50  CONTINUE

      DO 100 I=1, N
        T    = DT*(I-1)*2*PI
        X(I) = DT*(I-1)
        Y2(I) = 0.
        DO 150 J=1, M
          JJ = J*2-1
          YY = A(J)*COS(JJ*T)
          Y2(I)  = Y2(I) + YY
  150   CONTINUE
        Y1(I) = A(1)*COS(T)

        IF(T.LT.PI/2. .OR. T.GE.PI*3./2.) THEN
          Y0(I) = -0.5
        ELSE
          Y0(I) = 0.5
        ENDIF
  100 CONTINUE

*-----------------------------------------------------------------------

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN(IWS)
      CALL GRFRM

      CALL USSPNT(N, X, Y1)
      CALL USSPNT(N, X, Y2)

      CALL UUSLNI(5)
      CALL USGRPH(N, X, Y0)

      CALL UULINZ(N, X, Y1, 3, 1)
      CALL UULINZ(N, X, Y2, 2, 2)

      CALL GRCLS

      END
