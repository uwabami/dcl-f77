*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM USPK07

      PARAMETER(N=200, M=10)
      REAL X(N), Y(N), A(M)

*-----------------------------------------------------------------------

      DT = 1./(N-1)
      PI = 3.14159
      DO 50 J=1, M
        JJ = J*2-1
        A(J) = (-1)**J *2./(JJ*PI)
  50  CONTINUE

      DO 100 I=1, N
        T    = DT*(I-1)*2*PI
        X(I) = DT*(I-1)
        YY = 0.
        DO 150 J=1, M
          JJ = J*2-1
          YD = A(J)*COS(JJ*T)
          YY  = YY + YD
  150   CONTINUE
        Y(I) = YY*2.
  100 CONTINUE

*-----------------------------------------------------------------------

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN(IWS)
      CALL GRFRM

*          --- X AXIS ---
      CALL UZRSET('UYUSER'  , 0.)
      CALL USCSET('CXSIDE'  , 'U')

*          --- Y AXIS ---
      CALL UZISET('IROTLYL' , 1)
      CALL UZISET('ICENTYL' , 0)
      CALL USCSET('CYSIDE'  , 'L')

*          --- etc. ---
      CALL UZISET('INNER' , -1)
      CALL USCSET('CBLKT' , '[]')

      CALL USSTTL('TIME', 'SEC', 'VOLTAGE', 'mV')
      CALL USGRPH(N, X, Y)

      CALL GRCLS

      END
