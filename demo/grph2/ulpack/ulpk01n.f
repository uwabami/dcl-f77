*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM ULPK01

      PARAMETER (NBL=2)

      REAL      BL(NBL)
      CHARACTER CTL*64

      DATA      BL/1.0,3.0/


      WRITE(*,*) ' WORKSTATION ID (I) ? ;'
      CALL SGPWSN
      READ(*,*) IWS

      CALL SGOPN(-ABS(IWS))

      CALL SGLSET('LFULL',.TRUE.)
      CALL UZFACT(0.6)
      CALL SLRAT(0.75,1.0)
      CALL SLDIV('T',1,7)

      CALL SGFRM
      CALL UZINIT
      CALL SGSWND(-0.15,-200.,0.1,1E5)
      CALL SGSVPT(0.1,0.9,0.1,0.11)
      CALL SGSTRN(4)
      CALL SGSTRF
      CALL ULISET('IXTYPE',1)
      CALL ULXLOG('B',1,9)
      CTL='ULXLOG (IXTYPE=1,NLBL=1,NTCK=9)'
      CALL UXSTTL('B',CTL,0.0)

      CALL SGLSET('LCNTL',.TRUE.)

      CALL SGFRM
      CALL UZINIT
      CALL SGSWND(-0.15,-200.,0.1,1E5)
      CALL SGSVPT(0.1,0.9,0.1,0.11)
      CALL SGSTRN(4)
      CALL SGSTRF
      CALL ULISET('IXTYPE',1)
      CALL ULXLOG('B',3,9)
      CTL='ULXLOG (IXTYPE=1,NLBL=3,NTCK=9)'
      CALL UXSTTL('B',CTL,0.0)

      CALL SGFRM
      CALL UZINIT
      CALL SGSWND(-0.15,-200.,0.1,1E5)
      CALL SGSVPT(0.1,0.9,0.1,0.11)
      CALL SGSTRN(4)
      CALL SGSTRF
      CALL ULISET('IXTYPE',2)
      CALL ULXLOG('B',3,9)
      CTL='ULXLOG (IXTYPE=2,NLBL=3,NTCK=9)'
      CALL UXSTTL('B',CTL,0.0)

      CALL SGFRM
      CALL UZINIT
      CALL SGSWND(-0.15,-200.,0.1,1E5)
      CALL SGSVPT(0.1,0.9,0.1,0.11)
      CALL SGSTRN(4)
      CALL SGSTRF
      CALL ULISET('IXTYPE',3)
      CALL ULXLOG('B',3,9)
      CTL='ULXLOG (IXTYPE=3,NLBL=3,NTCK=9)'
      CALL UXSTTL('B',CTL,0.0)

      CALL SGFRM
      CALL UZINIT
      CALL SGSWND(-0.15,-200.,0.1,1E5)
      CALL SGSVPT(0.1,0.9,0.1,0.11)
      CALL SGSTRN(4)
      CALL SGSTRF
      CALL ULISET('IXTYPE',4)
      CALL ULXLOG('B',3,9)
      CTL='ULXLOG (IXTYPE=4,NLBL=3,NTCK=9)'
      CALL UXSTTL('B',CTL,0.0)

      CALL SGFRM
      CALL UZINIT
      CALL SGSWND(-0.15,-200.,0.1,1E5)
      CALL SGSVPT(0.1,0.9,0.1,0.11)
      CALL SGSTRN(4)
      CALL SGSTRF
      CALL ULXSFM('(F5.1)')
      CALL ULISET('IXTYPE',3)
      CALL ULXLOG('B',3,9)
      CTL='ULXLOG (IXTYPE=3,NLBL=3,NTCK=9)'
      CALL UXSTTL('B',CTL,0.0)

      CALL SGFRM
      CALL UZINIT
      CALL SGSWND(-0.15,-200.,0.1,1E5)
      CALL SGSVPT(0.1,0.9,0.1,0.11)
      CALL SGSTRN(4)
      CALL SGSTRF
      CALL ULISET('IXTYPE',1)
      CALL ULISET('IXCHR',194)
      CALL ULSXBL(BL,NBL)
      CALL ULXLOG('B',4,5)
      CTL='ULXLOG (IXTYPE=1,NLBL=4,NTCK=5)'
      CALL UXSTTL('B',CTL,0.0)

      CALL SGCLS

      END
