*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM ULPK02

      PARAMETER (NBL=2)

      REAL      BL(NBL)
      CHARACTER CTL*64

      DATA      BL/1.0,3.0/


      WRITE(*,*) ' WORKSTATION ID (I) ? ;'
      CALL SGPWSN
      READ(*,*) IWS

      CALL SGOPN(+ABS(IWS))

      CALL SGLSET('LFULL',.TRUE.)
      CALL UZFACT(0.6)
      CALL SLRAT(1.0,0.75)
      CALL SLDIV('Y',7,1)

      CALL SGFRM
      CALL UZINIT
      CALL SGSWND(0.2,200.,-0.15,-200.)
      CALL SGSVPT(0.13,0.14,0.1,0.9)
      CALL SGSTRN(4)
      CALL SGSTRF
      CALL ULISET('IYTYPE',1)
      CALL ULYLOG('L',1,9)
      CTL='ULYLOG (IYTYPE=1,NLBL=1,NTCK=9)'
      CALL UYSTTL('L',CTL,0.0)

      CALL SGLSET('LCNTL',.TRUE.)

      CALL SGFRM
      CALL UZINIT
      CALL SGSWND(0.2,200.,-0.15,-200.)
      CALL SGSVPT(0.13,0.14,0.1,0.9)
      CALL SGSTRN(4)
      CALL SGSTRF
      CALL ULISET('IYTYPE',1)
      CALL ULYLOG('L',3,9)
      CTL='ULYLOG (IYTYPE=1,NLBL=3,NTCK=9)'
      CALL UYSTTL('L',CTL,0.0)

      CALL SGFRM
      CALL UZINIT
      CALL SGSWND(0.2,200.,-0.15,-200.)
      CALL SGSVPT(0.13,0.14,0.1,0.9)
      CALL SGSTRN(4)
      CALL SGSTRF
      CALL ULISET('IYTYPE',2)
      CALL ULYLOG('L',3,9)
      CTL='ULYLOG (IYTYPE=2,NLBL=3,NTCK=9)'
      CALL UYSTTL('L',CTL,0.0)

      CALL SGFRM
      CALL UZINIT
      CALL SGSWND(0.2,200.,-0.15,-200.)
      CALL SGSVPT(0.13,0.14,0.1,0.9)
      CALL SGSTRN(4)
      CALL SGSTRF
      CALL ULISET('IYTYPE',3)
      CALL ULYLOG('L',3,9)
      CTL='ULYLOG (IYTYPE=3,NLBL=3,NTCK=9)'
      CALL UYSTTL('L',CTL,0.0)

      CALL SGFRM
      CALL UZINIT
      CALL SGSWND(0.2,200.,-0.15,-200.)
      CALL SGSVPT(0.13,0.14,0.1,0.9)
      CALL SGSTRN(4)
      CALL SGSTRF
      CALL ULISET('IYTYPE',4)
      CALL ULYLOG('L',3,9)
      CTL='ULYLOG (IYTYPE=4,NLBL=3,NTCK=9)'
      CALL UYSTTL('L',CTL,0.0)

      CALL SGFRM
      CALL UZINIT
      CALL SGSWND(0.2,200.,-0.15,-200.)
      CALL SGSVPT(0.13,0.14,0.1,0.9)
      CALL SGSTRN(4)
      CALL SGSTRF
      CALL ULYSFM('(F5.1)')
      CALL ULISET('IYTYPE',3)
      CALL ULYLOG('L',3,9)
      CTL='ULYLOG (IYTYPE=3,NLBL=3,NTCK=9)'
      CALL UYSTTL('L',CTL,0.0)

      CALL SGFRM
      CALL UZINIT
      CALL SGSWND(0.2,200.,-0.15,-200.)
      CALL SGSVPT(0.13,0.14,0.1,0.9)
      CALL SGSTRN(4)
      CALL SGSTRF
      CALL ULISET('IYTYPE',1)
      CALL ULISET('IYCHR',194)
      CALL ULSYBL(BL,NBL)
      CALL ULYLOG('L',4,5)
      CTL='ULYLOG (IYTYPE=1,NLBL=4,NTCK=5)'
      CALL UYSTTL('L',CTL,0.0)

      CALL SGCLS

      END
