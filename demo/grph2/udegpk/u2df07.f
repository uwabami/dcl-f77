*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM U2DF07

      PARAMETER ( NX=19, NY=19 )
      PARAMETER ( XMIN=0, XMAX=360, YMIN=-90, YMAX=90 )
      PARAMETER ( PI=3.141592, DRAD=PI/180, DZ=0.05 )

      REAL      P(NX,NY), U(NX,NY), V(NX,NY)

      EXTERNAL  IMOD


      CALL GLRGET( 'RMISS', RMISS )
      CALL GLLSET( 'LMISS', .TRUE. )

      DO 20 J = 1, NY
        DO 10 I = 1, NX
          ALON = ( XMIN + (XMAX-XMIN) * (I-1) / (NX-1) ) * DRAD
          ALAT = ( YMIN + (YMAX-YMIN) * (J-1) / (NY-1) ) * DRAD
          SLAT = SIN(ALAT)
          P(I,J) = COS(ALON) * (1-SLAT**2) * SIN(2*PI*SLAT) + DZ
   10   CONTINUE
   20 CONTINUE

      DO 40 J = 1, NY
        DO 30 I = 1, NX
          IF (J.EQ.1 .OR. J.EQ.NY) THEN
            U(I,J)=RMISS
            V(I,J)=RMISS
          ELSE
            U(I,J) = P(I,J-1) - P(I,J+1)
            V(I,J) = P(IMOD(I,NX-1)+1,J) - P(IMOD(I-2,NX-1)+1,J)
          END IF
   30   CONTINUE
   40 CONTINUE

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN( IWS )
      CALL GRFRM

      CALL GRSWND( XMIN, XMAX, YMIN, YMAX )
      CALL GRSVPT( 0.2, 0.8, 0.2, 0.8 )
      CALL GRSTRN( 1 )
      CALL GRSTRF

      CALL USDAXS

      CALL UGVECT( U, NX, V, NX, NX, NY )

      CALL GRCLS

      END
