*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM U2DF05

      PARAMETER ( NX=19, NY=19 )
      PARAMETER ( XMIN=  0, XMAX=360, DX1=20, DX2=60 )
      PARAMETER ( YMIN=-90, YMAX=+90, DY1=10, DY2=30 )
      PARAMETER ( PI=3.141592, DRAD=PI/180, DZ=0.05, DP=0.2 )

      REAL      P(NX,NY)


      DO 20 J = 1, NY
        DO 10 I = 1, NX
          ALON = ( XMIN + (XMAX-XMIN) * (I-1) / (NX-1) ) * DRAD
          ALAT = ( YMIN + (YMAX-YMIN) * (J-1) / (NY-1) ) * DRAD
          SLAT = SIN(ALAT)
          P(I,J) = COS(ALON) * (1-SLAT**2) * SIN(2*PI*SLAT) + DZ
   10   CONTINUE
   20 CONTINUE

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN( IWS )

      CALL GLRGET( 'RMISS', RMISS )
      CALL SGLSET( 'LSOFTF', .FALSE. )

      CALL GRFRM

      CALL GRSWND( XMIN, XMAX, YMIN, YMAX )
      CALL GRSVPT( 0.2, 0.8, 0.2, 0.8 )
      CALL GRSTRN( 1 )
      CALL GRSTRF

      CALL UESTLV( RMISS,  -DP, 201 )
      CALL UESTLV(    DP, DP*2, 401 )
      CALL UESTLV( DP*2, RMISS, 402 )
      CALL UETONE( P, NX, NX, NY )

      CALL UXAXDV( 'B', DX1, DX2 )
      CALL UXAXDV( 'T', DX1, DX2 )
      CALL UXSTTL( 'B', 'LONGITUDE', 0.0 )

      CALL UYAXDV( 'L', DY1, DY2 )
      CALL UYAXDV( 'R', DY1, DY2 )
      CALL UYSTTL( 'L', 'LATITUDE', 0.0 )

      CALL UDGCLB( P, NX, NX, NY, DP )
      CALL UDCNTR( P, NX, NX, NY )

      CALL GRCLS

      END
