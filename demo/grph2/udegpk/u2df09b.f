*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM U2DF09B

      PARAMETER( NX=21, NY=21 )
      PARAMETER( XMIN=-10, XMAX=10, YMIN=-10, YMAX=10 )
      PARAMETER( DX1=1, DX2=5, DY1=1, DY2=5 )
      PARAMETER( KMAX=5, PMIN=0, PMAX=1 )

      REAL P(NX,NY), PI(2,KMAX+1)


      DO 10 J=1,NY
        DO 15 I=1,NX
          X = XMIN + (XMAX-XMIN)*(I-1)/(NX-1)
          Y = YMIN + (YMAX-YMIN)*(J-1)/(NY-1)
          P(I,J) = EXP( -X**2/64 -Y**2/25 )
   15   CONTINUE
   10 CONTINUE

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN( IWS )
      CALL SGLSET( 'LSOFTF', .TRUE. )
      CALL GRFRM

      CALL GRSWND( XMIN, XMAX, YMIN, YMAX )
      CALL GRSVPT(  0.2,  0.8,  0.2,  0.8 )
      CALL GRSTRN( 1 )
      CALL GRSTRF

      DP = (PMAX-PMIN)/KMAX
      DO 20 K=1,KMAX
        TLEV1 = (K-1)*DP
        TLEV2 = TLEV1 + DP
        IPAT  = 600 + K - 1
        CALL UESTLV( TLEV1, TLEV2, IPAT )
   20 CONTINUE

      CALL UETONB( P, NX, NX, NY )

      CALL UXAXDV( 'B', DX1, DX2 )
      CALL UXAXDV( 'T', DX1, DX2 )
      CALL UXSTTL( 'B', 'X-axis', 0. )
      CALL UXMTTL( 'T', 'FIGURE TITLE', 0. )

      CALL UYAXDV( 'L', DY1, DY2 )
      CALL UYAXDV( 'R', DY1, DY2 )
      CALL UYSTTL( 'L', 'Y-axis', 0. )

*-- トーンバー ----

      CALL GRSWND(  0.0, 1.0, PMIN, PMAX )
      CALL GRSVPT( 0.85, 0.9, 0.45, 0.75 )
      CALL GRSTRN( 1 )
      CALL GRSTRF

      DO 30 K=1,KMAX+1
        PI(1,K) = PMIN + (K-1)*DP
        PI(2,K) = PMIN + (K-1)*DP
   30 CONTINUE

      CALL UDLSET( 'LMSG' , .FALSE. )
      CALL UETONE( PI, 2, 2, KMAX+1 )
*     CALL UDCNTR( PI, 2, 2, KMAX+1 )

      CALL SLPVPR( 3 )
      CALL UZLSET( 'LABELYR', .TRUE. )
      CALL UZFACT( 0.8 )
      CALL UYSFMT( '(F4.1)' )
      CALL UYAXDV( 'R', DP, DP )

      CALL GRCLS

      END
