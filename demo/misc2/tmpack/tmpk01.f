      PROGRAM TMPK1
      IMPLICIT NONE
      INTEGER NX, NY, THRES
      PARAMETER (NX=101, NY=101, THRES=1)
      REAL DX, DY
      PARAMETER (DX=6.2832/REAL(NX-1), DY=6.2832/REAL(NY-1))
      REAL ARROW_THRES
      PARAMETER (ARROW_THRES=1.0)
      REAL U(NX,NY), V(NX,NY)
      REAL X(NX), Y(NY)
      INTEGER I, J, SKIP, IWS
 
      WRITE(*,*) "SKIP NUM INPUT"
      READ(*,*) SKIP

      X=(/(DX*REAL(I-1),I=1,NX)/)
      Y=(/(DY*REAL(I-1),I=1,NY)/)
    
      DO 21 J=1,NY
         DO 20 I=1,NX
            U(I,J)=-COS(Y(J))
            V(I,J)=COS(X(I))
 20      CONTINUE
 21   CONTINUE
    
      WRITE(*,*) ' WORKSTATION IS (I) ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN( IWS )
      CALL GRFRM
      CALL GRSWND( X(1), X(NX), Y(1), Y(NY) )
      CALL USPFIT
      CALL GRSTRF
      CALL USDAXS

C      CALL UETONF( U, NX, NX, NY )
C      CALL UDCNTR( V, NX, NX, NY )
C-- 流線描画
      CALL TMISET( 'SKIPINTV', SKIP )
      CALL TMSTLC( X, Y, U, V, NX, NY )

      CALL GRCLS

      END PROGRAM
