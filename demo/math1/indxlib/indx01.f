*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM INDX01

      PARAMETER (N=10)

      INTEGER   IX(N)
      REAL      RX(N)
      CHARACTER CX(10)*1

      EXTERNAL  INDXIF,INDXIL,NINDXI,INDXRF,INDXRL,NINDXR,
     +          INDXCF,INDXCL,NINDXC

      DATA      IX/  0,  1,  2,  3,  2,  1,  0, -1, -2,  0/
      DATA      RX/  0,  1,  2,  3,  2,  1,  0, -1, -2,  0/
      DATA      CX/'A','B','C','B','A','Z','W','Z','A','Y'/


      WRITE(*,'(A,10I4)') ' LIST OF IX (N=10) : ',IX
      WRITE(*,100) ' *** INDXIF(IX,N,1,2)  = ',INDXIF(IX,N,1,2)
      WRITE(*,100) ' *** INDXIL(IX,N,1,2)  = ',INDXIL(IX,N,1,2)
      WRITE(*,100) ' *** NINDXI(IX,N,1,0)  = ',NINDXI(IX,N,1,0)
      WRITE(*,'(A,10F4.0)') ' LIST OF RX (N=10) : ',RX
      WRITE(*,100) ' *** INDXRF(RX,N,1,1.0)  = ',INDXRF(RX,N,1,1.0)
      WRITE(*,100) ' *** INDXRL(RX,N,1,1.0)  = ',INDXRL(RX,N,1,1.0)
      WRITE(*,100) ' *** NINDXR(RX,N,1,2.0)  = ',NINDXR(RX,N,1,2.0)
      WRITE(*,'(A,10A4)') ' LIST OF CX (N=10) : ',CX
      WRITE(*,100) ' *** INDXCF(CX,N,1,''B'')  = ',INDXCF(CX,N,1,'B')
      WRITE(*,100) ' *** INDXCL(CX,N,1,''B'')  = ',INDXCL(CX,N,1,'B')
      WRITE(*,100) ' *** NINDXC(CX,N,1,''A'')  = ',NINDXC(CX,N,1,'A')

  100 FORMAT(A,I6)

      END
