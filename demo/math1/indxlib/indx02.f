*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM INDX02

      CHARACTER CX*50,CC*50,CH*2

      DATA      CC/'12345678901234567890123456789012345678901234567890'/
      DATA      CX/'NANA AND MAMA GAVE TOMATOES AND BANANAS TO MASATO.'/


      WRITE(*,'(A,A)') ' -------------',CC
      WRITE(*,'(A,A)') ' LIST OF CX : ',CX

      N=25
      J=2
      CH='NA'
      WRITE(*,100) ' N = ',N,' : J = ',J,' : CH = ',CH
      WRITE(*,200) ' *** INDXMF(CX,N,J,CH)  = ',INDXMF(CX,N,J,CH)
      WRITE(*,200) ' *** INDXML(CX,N,J,CH)  = ',INDXML(CX,N,J,CH)
      WRITE(*,200) ' *** NINDXM(CX,N,J,CH)  = ',NINDXM(CX,N,J,CH)

      N=49
      J=1
      CH='MA'
      WRITE(*,100) ' N = ',N,' : J = ',J,' : CH = ',CH
      WRITE(*,200) ' *** INDXMF(CX,N,J,CH)  = ',INDXMF(CX,N,J,CH)
      WRITE(*,200) ' *** INDXML(CX,N,J,CH)  = ',INDXML(CX,N,J,CH)
      WRITE(*,200) ' *** NINDXM(CX,N,J,CH)  = ',NINDXM(CX,N,J,CH)

      N=49
      J=1
      CH='ma'
      WRITE(*,100) ' N = ',N,' : J = ',J,' : CH = ',CH
      WRITE(*,200) ' *** INDXMF(CX,N,J,CH)  = ',INDXMF(CX,N,J,CH)
      WRITE(*,200) ' *** INDXML(CX,N,J,CH)  = ',INDXML(CX,N,J,CH)
      WRITE(*,200) ' *** NINDXM(CX,N,J,CH)  = ',NINDXM(CX,N,J,CH)

  100 FORMAT(A,I4,A,I4,A,A)
  200 FORMAT(A,I4)

      END
