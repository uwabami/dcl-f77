*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM GNML01

      PARAMETER (NR=2)

      REAL      RR(NR)

      EXTERNAL  RGNLT,RGNLE,RGNGT,RGNGE

      DATA      RR/ 1.23, 30.00003 /


      CALL GLLSET('LEPSL',.TRUE.)
      DO 10 I=1,NR
        WRITE(*,'(A,G16.7)') ' *** RR = ',RR(I)
        CALL GNLT(RR(I),BR,IP)
        WRITE(*,'(A,F8.4,A,I4)')
     +       ' *** CALL GNLT(RR,BR,IP) : BR = ',BR,' : IP = ',IP
        CALL GNLE(RR(I),BR,IP)
        WRITE(*,'(A,F8.4,A,I4)')
     +       ' *** CALL GNLE(RR,BR,IP) : BR = ',BR,' : IP = ',IP
        CALL GNGT(RR(I),BR,IP)
        WRITE(*,'(A,F8.4,A,I4)')
     +       ' *** CALL GNGT(RR,BR,IP) : BR = ',BR,' : IP = ',IP
        CALL GNGE(RR(I),BR,IP)
        WRITE(*,'(A,F8.4,A,I4)')
     +       ' *** CALL GNGE(RR,BR,IP) : BR = ',BR,' : IP = ',IP
        WRITE(*,'(A,G16.7)') ' *** RGNLT(RR) = ',RGNLT(RR(I))
        WRITE(*,'(A,G16.7)') ' *** RGNLE(RR) = ',RGNLE(RR(I))
        WRITE(*,'(A,G16.7)') ' *** RGNGT(RR) = ',RGNGT(RR(I))
        WRITE(*,'(A,G16.7)') ' *** RGNGE(RR) = ',RGNGE(RR(I))
   10 CONTINUE

      END
