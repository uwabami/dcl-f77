*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM VIAL02

      PARAMETER (N=10)

      INTEGER   IX(N),IY(N)

      DATA      IX/  0,  1,  4,  6,  3, -4, 12,999, 23, -5/


      WRITE(*,'(A,10I6)') ' LIST OF IX : ',IX
      WRITE(*,'(A)') ' *** OPTION LMISS = .FALSE.'
      CALL VISET(IX,IY,10,1,1)
      WRITE(*,'(A)') ' AFTER CALLING VISET(IX,IY,10,1,1)'
      WRITE(*,'(A,10I6)') ' LIST OF IY : ',IY
      CALL IADD(IY,10,1,+1)
      WRITE(*,'(A)') ' AFTER CALLING IADD(IY,10,1,+1)'
      WRITE(*,'(A,10I6)') ' LIST OF IY : ',IY
      CALL IMLT(IY,10,1,-1)
      WRITE(*,'(A)') ' AFTER CALLING IMLT(IY,10,1,-1)'
      WRITE(*,'(A,10I6)') ' LIST OF IY : ',IY
      CALL ISET(IY,10,1,0)
      WRITE(*,'(A)') ' AFTER CALLING ISET(IY,10,1,0)'
      WRITE(*,'(A,10I6)') ' LIST OF IY : ',IY

      CALL GLLSET('LMISS',.TRUE.)
      WRITE(*,'(A)') ' *** OPTION LMISS = .TRUE.'
      CALL VISET(IX,IY,10,1,1)
      WRITE(*,'(A)') ' AFTER CALLING VISET(IX,IY,10,1,1)'
      WRITE(*,'(A,10I6)') ' LIST OF IY : ',IY
      CALL IADD(IY,10,1,+1)
      WRITE(*,'(A)') ' AFTER CALLING IADD(IY,10,1,+1)'
      WRITE(*,'(A,10I6)') ' LIST OF IY : ',IY
      CALL IMLT(IY,10,1,-1)
      WRITE(*,'(A)') ' AFTER CALLING IMLT(IY,10,1,-1)'
      WRITE(*,'(A,10I6)') ' LIST OF IY : ',IY
      CALL ISET(IY,10,1,0)
      WRITE(*,'(A)') ' AFTER CALLING ISET(IY,10,1,0)'
      WRITE(*,'(A,10I6)') ' LIST OF IY : ',IY

      END
