*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM RFBL01

      PARAMETER (N=8)

      REAL      RX(N),RY(N)

      EXTERNAL  RPRD,RCOV,RCOR

      DATA      RX/ -1,  1,  2, -2, -3, -4, 12, -5/
      DATA      RY/  1,  2, -3,  2,  0, -7,  8, -3/


      WRITE(*,'(A,8F8.1)') ' LIST OF RX : ',RX
      WRITE(*,'(A,8F8.1)') ' LIST OF RY : ',RY
      WRITE(*,'(A,G14.6)')
     +     ' *** RPRD(RX,RY,8,1,1) = ',RPRD(RX,RY,8,1,1)
      WRITE(*,'(A,G14.6)')
     +     ' *** RCOV(RX,RY,8,1,1) = ',RCOV(RX,RY,8,1,1)
      WRITE(*,'(A,G14.6)')
     +     ' *** RCOR(RX,RY,8,1,1) = ',RCOR(RX,RY,8,1,1)

      END
