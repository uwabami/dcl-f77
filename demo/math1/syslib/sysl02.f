*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM SYSL02

      LOGICAL LMISS
      CHARACTER CMSG*120


      CALL GLLSTX('LMISS   ',.TRUE.)
      CALL GLLGET('LMISS   ',LMISS )
      WRITE(*,*) '--- LMISS    WAS CHANGED TO ',LMISS
      CALL GLISTX('IMISS   ',  -999)
      CALL GLIGET('IMISS   ',IMISS )
      WRITE(*,*) '--- IMISS    WAS CHANGED TO ',IMISS
      CALL GLRSTX('RMISS   ',-999.0)
      CALL GLRGET('RMISS   ',RMISS )
      WRITE(*,*) '--- RMISS    WAS CHANGED TO ',RMISS

      CALL GLISTX('NLNSIZE ',    60)
      CALL GLIGET('NLNSIZE ',NLINES)
      WRITE(*,*) '--- NLNSIZE  WAS CHANGED TO ',NLINES
      CALL GLISTX('MAXMSG  ',     5)
      CALL GLIGET('MAXMSG  ',MAXMSG)
      WRITE(*,*) '--- MAXMSG   WAS CHANGED TO ',MAXMSG
      CMSG='1234567890123456789012345678901234567890'
     +   //'1234567890123456789012345678901234567890'
     +   //'1234567890123456789012345678901234567890'
      DO 10 N=1,5
        CALL MSGDMP('M','TMSGDM','TEST PROGRAM FOR MSGDMP.')
        CALL MSGDMP('W','TMSGDM',CMSG)
   10 CONTINUE
*     CALL MSGDMP('E','TMSGDM','TEST PROGRAM FOR MSGDMP.')

      END
