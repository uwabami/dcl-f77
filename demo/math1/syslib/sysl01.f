*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM SYSL01

      LOGICAL LMISS,LEPSL


      CALL GLIGET('NBITSPW ',NBITS )
      WRITE(*,*) '*** NBITSPW  = ',NBITS
      CALL GLIGET('NCHRSPW ',NCHRS )
      WRITE(*,*) '*** NCHRSPW  = ',NCHRS
      CALL GLIGET('INTMAX  ',INTMAX)
      WRITE(*,*) '*** INTMAX   = ',INTMAX
      CALL GLRGET('REALMAX ',RLMAX )
      WRITE(*,*) '*** REALMAX  = ',RLMAX
      CALL GLRGET('REALMIN ',RLMIN )
      WRITE(*,*) '*** REALMIN  = ',RLMIN
      CALL GLIGET('IIUNIT  ',IIUNIT)
      WRITE(*,*) '*** IIUNIT   = ',IIUNIT
      CALL GLIGET('IOUNIT  ',IOUNIT)
      WRITE(*,*) '*** IOUNIT   = ',IOUNIT
      CALL GLIGET('MSGUNIT ',MSGU  )
      WRITE(*,*) '*** MSGUNIT  = ',MSGU
      CALL GLIGET('MAXMSG  ',MAXMSG)
      WRITE(*,*) '*** MAXMSG   = ',MAXMSG
      CALL GLIGET('MSGLEV  ',MSGLEV)
      WRITE(*,*) '*** MSGLEV   = ',MSGLEV
      CALL GLIGET('NLNSIZE ',NLINES)
      WRITE(*,*) '*** NLNSIZE  = ',NLINES
      CALL GLLGET('LMISS   ',LMISS )
      WRITE(*,*) '*** LMISS    = ',LMISS
      CALL GLIGET('IMISS   ',IMISS )
      WRITE(*,*) '*** IMISS    = ',IMISS
      CALL GLRGET('RMISS   ',RMISS )
      WRITE(*,*) '*** RMISS    = ',RMISS
      CALL GLIGET('IUNDEF  ',IUNDEF)
      WRITE(*,*) '*** IUNDEF   = ',IUNDEF
      CALL GLRGET('RUNDEF  ',RUNDEF)
      WRITE(*,*) '*** RUNDEF   = ',RUNDEF
      CALL GLLGET('LEPSL   ',LEPSL )
      WRITE(*,*) '*** LEPSL    = ',LEPSL
      CALL GLRGET('REPSL   ',REPSL )
      WRITE(*,*) '*** REPSL    = ',REPSL
      CALL GLRGET('RFACT   ',RFACT )
      WRITE(*,*) '*** RFACT    = ',RFACT

      END
