*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM FNCL01

      PARAMETER (PI=3.1415926535,DX=180)

      EXTERNAL  IGUS,IMOD,RMOD,REXP,RFPI,RD2R,RR2D


      WRITE(*,'(A)') ' *** TEST FOR IGUS'
      WRITE(*,'(A,I2)') ' IGUS(11.2) = ',IGUS(11.2)
      WRITE(*,'(A,I2)') ' IGUS( 9.0) = ',IGUS( 9.0)
      WRITE(*,'(A,I2)') ' IGUS(-1.2) = ',IGUS(-1.2)
      WRITE(*,'(A)') ' *** TEST FOR IMOD'
      WRITE(*,'(A,I2)') ' IMOD(11,4) = ',IMOD(11,4)
      WRITE(*,'(A,I2)') ' IMOD(-7,3) = ',IMOD(-7,3)
      WRITE(*,'(A)') ' *** TEST FOR RMOD'
      WRITE(*,'(A,F4.1)') ' RMOD(11.0,4.0) = ',RMOD(11.0,4.0)
      WRITE(*,'(A,F4.1)') ' RMOD(-1.0,2.5) = ',RMOD(-1.0,2.5)
      WRITE(*,'(A)') ' *** TEST FOR REXP'
      WRITE(*,'(A,G16.8)') ' 0.8*10.0**(-1)  = ',0.8*10.0**(-1)
      WRITE(*,'(A,G16.8)') ' REXP(0.8,10,-1) = ',REXP(0.8,10,-1)
      WRITE(*,'(A)') ' *** TEST FOR RFPI'
      WRITE(*,'(A,G16.8)') ' RFPI  = ',RFPI()
      WRITE(*,'(A)') ' *** TEST FOR RD2R & RR2D'
      WRITE(*,'(A,G16.8)') ' RD2R(180.0)  = ',RD2R(DX)
      WRITE(*,'(A,G16.8)') ' RR2D(PI) = ',RR2D(PI)

      END
