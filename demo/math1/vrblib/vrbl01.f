*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM VRBL01

      PARAMETER (N=10)

      REAL      RX(N),RY(N),RZ(N)

      REAL      RFNB
      EXTERNAL  RFNB

      DATA      RX/  2,  1,  4,  6,  7, -4, 12,999, 23, -5/
      DATA      RY/  4, -1,  2,999,  3,  7,  1,  4,  3, -1/


      WRITE(*,'(A,10F6.0)') ' LIST OF RX : ',RX
      WRITE(*,'(A,10F6.0)') ' LIST OF RY : ',RY
      WRITE(*,'(A)') ' *** OPTION LMISS = .FALSE.'
      CALL VRADD(RX,RY,RZ,10,1,1,1)
      WRITE(*,'(A)') ' AFTER CALLING VRADD(RX,RY,RZ,10,1,1,1)'
      WRITE(*,'(A,10F6.0)') ' LIST OF RZ : ',RZ
      CALL VRSUB(RX,RY,RZ,10,1,1,1)
      WRITE(*,'(A)') ' AFTER CALLING VRSUB(RX,RY,RZ,10,1,1,1)'
      WRITE(*,'(A,10F6.0)') ' LIST OF RZ : ',RZ
      CALL VRMLT(RX,RY,RZ,10,1,1,1)
      WRITE(*,'(A)') ' AFTER CALLING VRMLT(RX,RY,RZ,10,1,1,1)'
      WRITE(*,'(A,10F6.0)') ' LIST OF RZ : ',RZ
      CALL VRDIV(RX,RY,RZ,10,1,1,1)
      WRITE(*,'(A)') ' AFTER CALLING VRDIV(RX,RY,RZ,10,1,1,1)'
      WRITE(*,'(A,10F6.0)') ' LIST OF RZ : ',RZ
      CALL VRFNB(RX,RY,RZ,10,1,1,1,RFNB)
      WRITE(*,'(A)') ' AFTER CALLING VRFNB(RX,RY,RZ,10,1,1,1,RFNB)'
      WRITE(*,'(A,10F6.0)') ' LIST OF RZ : ',RZ

      CALL GLLSET('LMISS',.TRUE.)
      WRITE(*,'(A)') ' *** OPTION LMISS = .TRUE.'
      CALL VRADD(RX,RY,RZ,10,1,1,1)
      WRITE(*,'(A)') ' AFTER CALLING VRADD(RX,RY,RZ,10,1,1,1)'
      WRITE(*,'(A,10F6.0)') ' LIST OF RZ : ',RZ
      CALL VRSUB(RX,RY,RZ,10,1,1,1)
      WRITE(*,'(A)') ' AFTER CALLING VRSUB(RX,RY,RZ,10,1,1,1)'
      WRITE(*,'(A,10F6.0)') ' LIST OF RZ : ',RZ
      CALL VRMLT(RX,RY,RZ,10,1,1,1)
      WRITE(*,'(A)') ' AFTER CALLING VRMLT(RX,RY,RZ,10,1,1,1)'
      WRITE(*,'(A,10F6.0)') ' LIST OF RZ : ',RZ
      CALL VRDIV(RX,RY,RZ,10,1,1,1)
      WRITE(*,'(A)') ' AFTER CALLING VRDIV(RX,RY,RZ,10,1,1,1)'
      WRITE(*,'(A,10F6.0)') ' LIST OF RZ : ',RZ
      CALL VRFNB(RX,RY,RZ,10,1,1,1,RFNB)
      WRITE(*,'(A)') ' AFTER CALLING VRFNB(RX,RY,RZ,10,1,1,1,RFNB)'
      WRITE(*,'(A,10F6.0)') ' LIST OF RZ : ',RZ

      END
*-----------------------------------------------------------------------
      REAL FUNCTION RFNB(R,S)

      RFNB=MIN(R,S)

      END
