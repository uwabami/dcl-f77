*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM OSLB01

      CHARACTER CENAME*20,CEVAL*40


      CENAME='HOME'
      NL=LENC(CENAME)

      WRITE(*,'(A)') ' CALL OSGENV('''//CENAME(1:NL)//''',CEVAL)'
      CALL OSGENV(CENAME,CEVAL)
      WRITE(*,'(TR1,3A)')
     +     CENAME(1:NL),' = ',CEVAL(1:LENC(CEVAL))

      END
