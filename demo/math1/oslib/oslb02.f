*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM OSLB02

      CHARACTER CMD*80


      CMD='ls'
      NL=LENC(CMD)

      WRITE(*,'(A)') ' CALL OSEXEC('''//CMD(1:NL)//''')'
      CALL OSEXEC(CMD)

      END
