*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM VIBL01

      PARAMETER (N=10)

      INTEGER   IX(N),IY(N),IZ(N)

      INTEGER   IFNB
      EXTERNAL  IFNB

      DATA      IX/  2,  1,  4,  6,  7, -4, 12,999, 23, -5/
      DATA      IY/  4, -1,  2,999,  3,  7,  1,  4,  3, -1/


      WRITE(*,'(A,10I6)') ' LIST OF IX : ',IX
      WRITE(*,'(A,10I6)') ' LIST OF IY : ',IY
      WRITE(*,'(A)') ' *** OPTION LMISS = .FALSE.'
      CALL VIADD(IX,IY,IZ,10,1,1,1)
      WRITE(*,'(A)') ' AFTER CALLING VIADD(IX,IY,IZ,10,1,1,1)'
      WRITE(*,'(A,10I6)') ' LIST OF IZ : ',IZ
      CALL VISUB(IX,IY,IZ,10,1,1,1)
      WRITE(*,'(A)') ' AFTER CALLING VISUB(IX,IY,IZ,10,1,1,1)'
      WRITE(*,'(A,10I6)') ' LIST OF IZ : ',IZ
      CALL VIMLT(IX,IY,IZ,10,1,1,1)
      WRITE(*,'(A)') ' AFTER CALLING VIMLT(IX,IY,IZ,10,1,1,1)'
      WRITE(*,'(A,10I6)') ' LIST OF IZ : ',IZ
      CALL VIDIV(IX,IY,IZ,10,1,1,1)
      WRITE(*,'(A)') ' AFTER CALLING VIDIV(IX,IY,IZ,10,1,1,1)'
      WRITE(*,'(A,10I6)') ' LIST OF IZ : ',IZ
      CALL VIFNB(IX,IY,IZ,10,1,1,1,IFNB)
      WRITE(*,'(A)') ' AFTER CALLING VIFNB(IX,IY,IZ,10,1,1,1,IFNB)'
      WRITE(*,'(A)') ' IFNB(I,J)=MIN(I,J)'
      WRITE(*,'(A,10I6)') ' LIST OF IZ : ',IZ

      CALL GLLSET('LMISS',.TRUE.)
      WRITE(*,'(A)') ' *** OPTION LMISS = .TRUE.'
      CALL VIADD(IX,IY,IZ,10,1,1,1)
      WRITE(*,'(A)') ' AFTER CALLING VIADD(IX,IY,IZ,10,1,1,1)'
      WRITE(*,'(A,10I6)') ' LIST OF IZ : ',IZ
      CALL VISUB(IX,IY,IZ,10,1,1,1)
      WRITE(*,'(A)') ' AFTER CALLING VISUB(IX,IY,IZ,10,1,1,1)'
      WRITE(*,'(A,10I6)') ' LIST OF IZ : ',IZ
      CALL VIMLT(IX,IY,IZ,10,1,1,1)
      WRITE(*,'(A)') ' AFTER CALLING VIMLT(IX,IY,IZ,10,1,1,1)'
      WRITE(*,'(A,10I6)') ' LIST OF IZ : ',IZ
      CALL VIDIV(IX,IY,IZ,10,1,1,1)
      WRITE(*,'(A)') ' AFTER CALLING VIDIV(IX,IY,IZ,10,1,1,1)'
      WRITE(*,'(A,10I6)') ' LIST OF IZ : ',IZ
      CALL VIFNB(IX,IY,IZ,10,1,1,1,IFNB)
      WRITE(*,'(A)') ' AFTER CALLING VIFNB(IX,IY,IZ,10,1,1,1,IFNB)'
      WRITE(*,'(A)') ' IFNB(I,J)=MIN(I,J)'
      WRITE(*,'(A,10I6)') ' LIST OF IZ : ',IZ

      END
*-----------------------------------------------------------------------
      INTEGER FUNCTION IFNB(I,J)

      IFNB=MIN(I,J)

      END
