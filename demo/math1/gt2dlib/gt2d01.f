*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM GT2D01

      PARAMETER(NX=15,NY=15)

      REAL UX(NX), UY(NY)
      REAL CX(NX,NY), CY(NX,NY)
      REAL TERRAIN(NX)


      CALL GLRGET('RUNDEF',RUNDEF)

      DO 10 I=1,NX
        UX(I)=(I-1.0)/(NX-1.0) - 0.5
        TERRAIN(I) = 0.1 * EXP(-24*UX(I)**2)
   10 CONTINUE
      DO 15 J=1,NY
        UY(J)=(J-1.0)/(NY-1.0)
   15 CONTINUE

      CX(1,1) = RUNDEF
      DO 25 J=1,NY
        DO 20 I=1,NX
          CY(I,J) = UY(J)*(1.0-TERRAIN(I)) + TERRAIN(I)
   20   CONTINUE
   25 CONTINUE

      CALL G2SCTR(NX, NY, UX,UY, CX,CY)

      PRINT *, "TERRAIN FOLLOWING COORDINATE"

      UXP = 0.2
      UYP = 0.3
      PRINT *, "ORIGINAL UX, UY:", UXP, UYP

      CALL G2FCTR(UXP, UYP, CXP, CYP)
      PRINT *, "TRANSFORMED CX, CY:", CXP, CYP
      CALL G2ICTR(CXP, CYP, UXP, UYP)
      PRINT *, "INVERTED UX, UY:", UXP, UYP

      END
