*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM INTL01

      PARAMETER (NR=3)

      REAL      RR(NR)

      EXTERNAL  IRLT,IRLE,IRGT,IRGE

      DATA      RR/ 2.5, 3.0, 3.000003 /


      CALL GLLSET('LEPSL',.TRUE.)
      DO 10 I=1,NR
        WRITE(*,'(A,G16.7)') ' *** RR = ',RR(I)
        WRITE(*,'(A,I4)') ' *** IRLT(RR) = ',IRLT(RR(I))
        WRITE(*,'(A,I4)') ' *** IRLE(RR) = ',IRLE(RR(I))
        WRITE(*,'(A,I4)') ' *** IRGT(RR) = ',IRGT(RR(I))
        WRITE(*,'(A,I4)') ' *** IRGE(RR) = ',IRGE(RR(I))
   10 CONTINUE

      END
