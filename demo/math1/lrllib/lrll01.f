*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM LRLL01

      PARAMETER (N=3)

      REAL      X(N),Y(N)

      LOGICAL   LREQ,LRNE,LRLT,LRLE,LRGT,LRGE

      EXTERNAL  LREQ,LRNE,LRLT,LRLE,LRGT,LRGE

      DATA      X/ 1.2, 3.0, 3.0 /
      DATA      Y/ 1.5, 3.0, 3.000003 /


      CALL GLLSET('LEPSL',.TRUE.)
      DO 10 I=1,N
        WRITE(*,'(2(A,G16.7),A)') 'X = ',X(I),'; Y = ',Y(I),';'
        WRITE(*,'(A,L4)') ' LREQ(X,Y)=',LREQ(X(I),Y(I))
        WRITE(*,'(A,L4)') ' LRNE(X,Y)=',LRNE(X(I),Y(I))
        WRITE(*,'(A,L4)') ' LRLT(X,Y)=',LRLT(X(I),Y(I))
        WRITE(*,'(A,L4)') ' LRLE(X,Y)=',LRLE(X(I),Y(I))
        WRITE(*,'(A,L4)') ' LRGT(X,Y)=',LRGT(X(I),Y(I))
        WRITE(*,'(A,L4)') ' LRGE(X,Y)=',LRGE(X(I),Y(I))
   10 CONTINUE

      END
