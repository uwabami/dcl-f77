*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM LRLL02

      PARAMETER (N=3)

      REAL      X(N),Y(N)

      LOGICAL   LREQA,LRNEA,LRLTA,LRLEA,LRGTA,LRGEA

      EXTERNAL  LREQA,LRNEA,LRLTA,LRLEA,LRGTA,LRGEA

      DATA      X/ 1.2, 3.0, 3.0 /
      DATA      Y/ 1.5, 3.0, 3.000003 /


      EPSL=1.0E-5
      DO 10 I=1,N
        WRITE(*,'(3(A,G16.7),A)')
     +       'X = ',X(I),'; Y = ',Y(I),'; EPSL = ',EPSL,';'
        WRITE(*,'(A,L4)') ' LREQA(X,Y,EPSL)=',LREQA(X(I),Y(I),EPSL)
        WRITE(*,'(A,L4)') ' LRNEA(X,Y,EPSL)=',LRNEA(X(I),Y(I),EPSL)
        WRITE(*,'(A,L4)') ' LRLTA(X,Y,EPSL)=',LRLTA(X(I),Y(I),EPSL)
        WRITE(*,'(A,L4)') ' LRLEA(X,Y,EPSL)=',LRLEA(X(I),Y(I),EPSL)
        WRITE(*,'(A,L4)') ' LRGTA(X,Y,EPSL)=',LRGTA(X(I),Y(I),EPSL)
        WRITE(*,'(A,L4)') ' LRGEA(X,Y,EPSL)=',LRGEA(X(I),Y(I),EPSL)
   10 CONTINUE

      END
