*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM SUBL01

      PARAMETER (N=8)

      INTEGER   IX(N)
      REAL      RX(N)


      WRITE(*,'(A)') ' *** TEST FOR VIGNN'
      CALL VIGNN(IX,N,1)
      WRITE(*,'(A,8I6)') ' IX = ',IX
      WRITE(*,'(A)') ' *** TEST FOR VRGNN'
      CALL VRGNN(RX,N,1)
      WRITE(*,'(A,8F6.1)') ' RX = ',RX

      END
