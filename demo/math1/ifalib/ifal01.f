*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM IFAL01

      PARAMETER (N=10)

      INTEGER   IX(N)

      EXTERNAL  IMAX,IMIN,ISUM

      DATA      IX/  0,  1,  4,  6,  3, -4, 12,999, 23, -5/


      WRITE(*,'(A,10I6)') ' LIST OF IX : ',IX
      WRITE(*,'(A)') ' *** OPTION LMISS = .FALSE.'
      JIMAX=IMAX(IX,N,1)
      WRITE(*,'(A,I6)') ' *** IMAX = ',JIMAX
      JIMIN=IMIN(IX,N,1)
      WRITE(*,'(A,I6)') ' *** IMIN = ',JIMIN
      JISUM=ISUM(IX,N,1)
      WRITE(*,'(A,I6)') ' *** ISUM = ',JISUM
      CALL GLLSET('LMISS',.TRUE.)
      WRITE(*,'(A)') ' *** OPTION LMISS = .TRUE.'
      JIMAX=IMAX(IX,N,1)
      WRITE(*,'(A,I6)') ' *** IMAX = ',JIMAX
      JIMIN=IMIN(IX,N,1)
      WRITE(*,'(A,I6)') ' *** IMIN = ',JIMIN
      JISUM=ISUM(IX,N,1)
      WRITE(*,'(A,I6)') ' *** ISUM = ',JISUM

      END
