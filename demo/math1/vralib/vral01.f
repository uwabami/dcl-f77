*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM VRAL01

      PARAMETER (N=10)

      REAL      RX(N),RY(N)

      REAL      RFNA
      EXTERNAL  RFNA

      DATA      RX/  0,  1,  4,  6,  3, -4, 12,999, 23, -5/


      WRITE(*,'(A,10F6.0)') ' LIST OF RX : ',RX
      WRITE(*,'(A)') ' *** OPTION LMISS = .FALSE.'
      CALL VRSET(RX,RY,10,1,1)
      WRITE(*,'(A)') ' AFTER CALLING VRSET(RX,RY,10,1,1)'
      WRITE(*,'(A,10F6.0)') ' LIST OF RY : ',RY
      CALL VRINC(RY,RY,10,1,1,+1.0)
      WRITE(*,'(A)') ' AFTER CALLING VRINC(RY,RY,10,1,1,+1.0)'
      WRITE(*,'(A,10F6.0)') ' LIST OF RY : ',RY
      CALL VRFCT(RY,RY,10,1,1,-1.0)
      WRITE(*,'(A)') ' AFTER CALLING VRFCT(RY,RY,10,1,1,-1.0)'
      WRITE(*,'(A,10F6.0)') ' LIST OF RY : ',RY
      CALL VRFNA(RY,RY,10,1,1,RFNA)
      WRITE(*,'(A)') ' AFTER CALLING VRFNA(RY,RY,10,1,1,RFNA)'
      WRITE(*,'(A)') ' RFNA(R)=R*2+10'
      WRITE(*,'(A,10F6.0)') ' LIST OF RY : ',RY
      CALL VRCON(RY,RY,10,1,1,0.0)
      WRITE(*,'(A)') ' AFTER CALLING VRCON(RY,RY,10,1,1,0.0)'
      WRITE(*,'(A,10F6.0)') ' LIST OF RY : ',RY

      CALL GLLSET('LMISS',.TRUE.)
      WRITE(*,'(A)') ' *** OPTION LMISS = .TRUE.'
      CALL VRSET(RX,RY,10,1,1)
      WRITE(*,'(A)') ' AFTER CALLING VRSET(RX,RY,10,1,1)'
      WRITE(*,'(A,10F6.0)') ' LIST OF RY : ',RY
      CALL VRINC(RY,RY,10,1,1,+1.0)
      WRITE(*,'(A)') ' AFTER CALLING VRINC(RY,RY,10,1,1,+1.0)'
      WRITE(*,'(A,10F6.0)') ' LIST OF RY : ',RY
      CALL VRFCT(RY,RY,10,1,1,-1.0)
      WRITE(*,'(A)') ' AFTER CALLING VRFCT(RY,RY,10,1,1,-1.0)'
      WRITE(*,'(A,10F6.0)') ' LIST OF RY : ',RY
      CALL VRFNA(RY,RY,10,1,1,RFNA)
      WRITE(*,'(A)') ' AFTER CALLING VRFNA(RY,RY,10,1,1,RFNA)'
      WRITE(*,'(A)') ' RFNA(R)=R*2+10'
      WRITE(*,'(A,10F6.0)') ' LIST OF RY : ',RY
      CALL VRCON(RY,RY,10,1,1,0.0)
      WRITE(*,'(A)') ' AFTER CALLING VRCON(RY,RY,10,1,1,0.0)'
      WRITE(*,'(A,10F6.0)') ' LIST OF RY : ',RY

      END
*-----------------------------------------------------------------------
      REAL FUNCTION RFNA(R)

      RFNA=R*2+10

      END
