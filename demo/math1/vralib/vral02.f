*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM VRAL02

      PARAMETER (N=10)

      REAL      RX(N),RY(N)

      DATA      RX/  0,  1,  4,  6,  3, -4, 12,999, 23, -5/


      WRITE(*,'(A,10F6.0)') ' LIST OF RX : ',RX
      WRITE(*,'(A)') ' *** OPTION LMISS = .FALSE.'
      CALL VRSET(RX,RY,10,1,1)
      WRITE(*,'(A)') ' AFTER CALLING VRSET(RX,RY,10,1,1)'
      WRITE(*,'(A,10F6.0)') ' LIST OF RY : ',RY
      CALL RADD(RY,10,1,+1.0)
      WRITE(*,'(A)') ' AFTER CALLING RADD(RY,10,1,+1.0)'
      WRITE(*,'(A,10F6.0)') ' LIST OF RY : ',RY
      CALL RMLT(RY,10,1,-1.0)
      WRITE(*,'(A)') ' AFTER CALLING RMLT(RY,10,1,-1.0)'
      WRITE(*,'(A,10F6.0)') ' LIST OF RY : ',RY
      CALL RSET(RY,10,1,0.0)
      WRITE(*,'(A)') ' AFTER CALLING RMLT(RY,10,1,0.0)'
      WRITE(*,'(A,10F6.0)') ' LIST OF RY : ',RY

      CALL GLLSET('LMISS',.TRUE.)
      WRITE(*,'(A)') ' *** OPTION LMISS = .TRUE.'
      CALL VRSET(RX,RY,10,1,1)
      WRITE(*,'(A)') ' AFTER CALLING VRSET(RX,RY,10,1,1)'
      WRITE(*,'(A,10F6.0)') ' LIST OF RY : ',RY
      CALL RADD(RY,10,1,+1.0)
      WRITE(*,'(A)') ' AFTER CALLING RADD(RY,10,1,+1.0)'
      WRITE(*,'(A,10F6.0)') ' LIST OF RY : ',RY
      CALL RMLT(RY,10,1,-1.0)
      WRITE(*,'(A)') ' AFTER CALLING RMLT(RY,10,1,-1.0)'
      WRITE(*,'(A,10F6.0)') ' LIST OF RY : ',RY
      CALL RSET(RY,10,1,0.0)
      WRITE(*,'(A)') ' AFTER CALLING RMLT(RY,10,1,0.0)'
      WRITE(*,'(A,10F6.0)') ' LIST OF RY : ',RY

      END
