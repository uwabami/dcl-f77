*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM RFAL01

      PARAMETER (N=10)

      REAL      RX(N)

      EXTERNAL  RMAX,RMIN,RSUM,RAVE,RVAR,RSTD,RRMS,RAMP

      DATA      RX/   0,  -1,  -2,-999,  -1,   0,   1,   2, 999,   1/


      WRITE(*,'(A,10F6.0)') ' LIST OF RX : ',RX
      WRITE(*,'(A)') ' *** OPTION LMISS = .FALSE.'
      SRMAX=RMAX(RX,N,1)
      WRITE(*,'(A,G14.6)') ' *** RMAX = ',SRMAX
      SRMIN=RMIN(RX,N,1)
      WRITE(*,'(A,G14.6)') ' *** RMIN = ',SRMIN
      SRSUM=RSUM(RX,N,1)
      WRITE(*,'(A,G14.6)') ' *** RSUM = ',SRSUM
      SRAVE=RAVE(RX,N,1)
      WRITE(*,'(A,G14.6)') ' *** RAVE = ',SRAVE
      SRVAR=RVAR(RX,N,1)
      WRITE(*,'(A,G14.6)') ' *** RVAR = ',SRVAR
      SRSTD=RSTD(RX,N,1)
      WRITE(*,'(A,G14.6)') ' *** RSTD = ',SRSTD
      SRRMS=RRMS(RX,N,1)
      WRITE(*,'(A,G14.6)') ' *** RRMS = ',SRRMS
      SRAMP=RAMP(RX,N,1)
      WRITE(*,'(A,G14.6)') ' *** RAMP = ',SRAMP
      CALL GLLSET('LMISS',.TRUE.)
      WRITE(*,'(A)') ' *** OPTION LMISS = .TRUE.'
      SRMAX=RMAX(RX,N,1)
      WRITE(*,'(A,G14.6)') ' *** RMAX = ',SRMAX
      SRMIN=RMIN(RX,N,1)
      WRITE(*,'(A,G14.6)') ' *** RMIN = ',SRMIN
      SRSUM=RSUM(RX,N,1)
      WRITE(*,'(A,G14.6)') ' *** RSUM = ',SRSUM
      SRAVE=RAVE(RX,N,1)
      WRITE(*,'(A,G14.6)') ' *** RAVE = ',SRAVE
      SRVAR=RVAR(RX,N,1)
      WRITE(*,'(A,G14.6)') ' *** RVAR = ',SRVAR
      SRSTD=RSTD(RX,N,1)
      WRITE(*,'(A,G14.6)') ' *** RSTD = ',SRSTD
      SRRMS=RRMS(RX,N,1)
      WRITE(*,'(A,G14.6)') ' *** RRMS = ',SRRMS
      SRAMP=RAMP(RX,N,1)
      WRITE(*,'(A,G14.6)') ' *** RAMP = ',SRAMP

      END
