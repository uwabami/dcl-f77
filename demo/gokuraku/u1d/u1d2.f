*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM U1D2

      PARAMETER( NMAX=50, YMIN=0., YMAX=50. )
      REAL X1(0:NMAX), X2(0:NMAX)

*-- データ ----
      ISEED = 1
      DO 10 N=0,NMAX
        Y = YMIN + (YMAX-YMIN)*N/NMAX
        X1(N) = 10.*(EXP(-Y/20))**2 * EXP((RNGU0(ISEED)-0.5)*2)**2
        X2(N) = 10.*(EXP(-Y/20))**2
   10 CONTINUE

      CALL GLRGET( 'RUNDEF', RUNDEF )

*-- グラフ ----
      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN( IWS )
      CALL GRFRM

      CALL GRSWND( RUNDEF, RUNDEF, YMIN, YMAX )
      CALL USSPNT( NMAX+1, X1, RUNDEF )
      CALL USSPNT( NMAX+1, X2, RUNDEF )
      CALL GRSTRN( 3 )
      CALL USPFIT
      CALL GRSTRF

      CALL USSTTL( 'MIXING RATIO', 'ppmv', 'HEIGHT', 'km' )
      CALL USDAXS

      CALL UUSMKT( 2 )
      CALL UUMRK( NMAX+1, X1, RUNDEF )

      CALL UULIN( NMAX+1, X2, RUNDEF )

      CALL GRCLS

      END
