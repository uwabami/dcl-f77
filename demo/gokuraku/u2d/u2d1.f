*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM U2D1

      PARAMETER( NT=51, NZ=21 )
      PARAMETER( TMIN=0, TMAX=5, ZMIN=20, ZMAX=50 )
      PARAMETER( DT=(TMAX-TMIN)/(NT-1), DZ=(ZMAX-ZMIN)/(NZ-1) )
      REAL U(NT,NZ)

*-- データ ----
      DO 10 J=1,NZ
        Z  = (J-1)*DZ
        UZ = EXP(-0.2*Z)*(Z**0.5)
        DO 20 I=1,NT
          T = (I-1)*DT - 2.*EXP(-0.1*Z)
          U(I,J) = UZ*SIN(3.*T)
   20   CONTINUE
   10 CONTINUE

*-- グラフ ----
      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN( IWS )
      CALL GRFRM

      CALL GRSWND( TMIN, TMAX, ZMIN, ZMAX )
      CALL USPFIT
      CALL GRSTRF

      CALL USSTTL( 'TIME', 'YEAR', 'HEIGHT', 'km' )
      CALL USDAXS

      CALL UDCNTR( U, NT, NT, NZ )

      CALL GRCLS

      END
