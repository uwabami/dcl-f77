*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM LAY1

      PARAMETER( NMAX=401, JMAX=51 )
      REAL Y(NMAX), YY(JMAX)

*-- データ ----
      Y0 = 0.5
      DO 10 N=1,NMAX
        Y(N) = 5.*Y0 + 10.
        Y0   = 3.7*Y0*(1.-Y0)
   10 CONTINUE

      CALL GLRGET( 'RUNDEF', RUNDEF )

*-- グラフ ----
      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN( IWS )
      CALL SLDIV( 'Y', 3, 2 )

      DO 20 I=1,8
        CALL GRFRM

        IJMIN = (I-1)*(JMAX-1) + 1
        IJ = IJMIN
        DO 30 J=1,JMAX
          YY(J) = Y(IJ)
          IJ = IJ + 1
   30   CONTINUE
        XMIN = IJMIN + 1599
        XMAX =  XMIN + JMAX - 1

        CALL GRSWND( XMIN, XMAX, RUNDEF, RUNDEF )
        CALL USSTTL( 'TIME', 'YEAR', 'TEMPERATURE', 'DEG' )
        CALL USGRPH( JMAX, RUNDEF, YY )
   20 CONTINUE

      CALL GRCLS

      END
