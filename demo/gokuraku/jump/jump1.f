*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM JUMP1

      PARAMETER( NMAX=400 )
      REAL X(NMAX), Y(NMAX)

*-- データ 1 ----
      DT = 2.*3.14159 / (NMAX-1)

      DO 10 N=1,NMAX
        T = DT*(N-1)
        X(N) = 5.*SIN(4.*T)
        Y(N) = 5.*COS(5.*T)
   10 CONTINUE

*-- グラフ 1 ----
      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN( IWS )
      CALL GRFRM

      CALL GRSWND(  -6.,   6.,  -6.,   6. )
      CALL GRSVPT( 0.15, 0.45, 0.65, 0.95 )
      CALL GRSTRN( 1 )
      CALL GRSTRF

      CALL USSTTL( 'X1', '', 'Y1', '' )
      CALL USDAXS

      CALL UULIN( NMAX, X, Y )

*-- データ 2 ----
      ISEED = 1
      X(1) = 2.*(RNGU0(ISEED)-0.5)
      DO 20 N=2,NMAX
        X(N)   = 2.*(RNGU0(ISEED)-0.5)
        Y(N-1) = X(N)
   20 CONTINUE
      Y(NMAX) = X(1)

*-- グラフ 2 ----
      CALL GRFIG

      CALL GRSWND(  -1.1,  1.1, -1.1, 1.1 )
      CALL GRSVPT(  0.15, 0.95,  0.1, 0.5 )
      CALL GRSTRN( 1 )
      CALL GRSTRF

      CALL USSTTL( 'X2-TITLE', '', 'Y2-TITLE', '' )
      CALL USDAXS

      CALL UUMRK( NMAX, X, Y )

      CALL GRCLS

      END

