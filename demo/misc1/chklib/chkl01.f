*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM CHKL01

      LOGICAL   LCHRB,LCHRC,LCHRS,LCHRL,LCHRD,LCHRA,LCHRF,LCHR
      CHARACTER CH*4,CHAR*8,CREF*8

      EXTERNAL  LCHRB,LCHRC,LCHRS,LCHRL,LCHRD,LCHRA,LCHRF,LCHR


      WRITE(*,100) ' *** TEST FOR LCHRB'
      CH='    '
      WRITE(*,200) ' CH = ''',CH,'''',' : LCHRB = ',LCHRB(CH)
      CH='1  A'
      WRITE(*,200) ' CH = ''',CH,'''',' : LCHRB = ',LCHRB(CH)
      WRITE(*,100) ' *** TEST FOR LCHRC'
      CH='$$$$'
      WRITE(*,200) ' CH = ''',CH,'''',' : LCHRC = ',LCHRC(CH)
      CH='$$$A'
      WRITE(*,200) ' CH = ''',CH,'''',' : LCHRC = ',LCHRC(CH)
      WRITE(*,100) ' *** TEST FOR LCHRS'
      CH='*.(/'
      WRITE(*,200) ' CH = ''',CH,'''',' : LCHRS = ',LCHRS(CH)
      CH='2.(/'
      WRITE(*,200) ' CH = ''',CH,'''',' : LCHRS = ',LCHRS(CH)
      WRITE(*,100) ' *** TEST FOR LCHRL'
      CH='ABCD'
      WRITE(*,200) ' CH = ''',CH,'''',' : LCHRL = ',LCHRL(CH)
      CH='AB+D'
      WRITE(*,200) ' CH = ''',CH,'''',' : LCHRL = ',LCHRL(CH)
      WRITE(*,100) ' *** TEST FOR LCHRD'
      CH='9876'
      WRITE(*,200) ' CH = ''',CH,'''',' : LCHRD = ',LCHRD(CH)
      CH='987A'
      WRITE(*,200) ' CH = ''',CH,'''',' : LCHRD = ',LCHRD(CH)
      WRITE(*,100) ' *** TEST FOR LCHRA'
      CH='AS23'
      WRITE(*,200) ' CH = ''',CH,'''',' : LCHRA = ',LCHRA(CH)
      CH='A 23'
      WRITE(*,200) ' CH = ''',CH,'''',' : LCHRA = ',LCHRA(CH)
      WRITE(*,100) ' *** TEST FOR LCHRF'
      CH='A*2='
      WRITE(*,200) ' CH = ''',CH,'''',' : LCHRF = ',LCHRF(CH)
      CH='A#2='
      WRITE(*,200) ' CH = ''',CH,'''',' : LCHRF = ',LCHRF(CH)
      WRITE(*,100) ' *** TEST FOR LCHR'
      CHAR='A=B*2+12'
      CREF='LSLSDSAF'
      WRITE(*,300) ' CHAR = ''',CHAR,''''
      WRITE(*,300) ' CREF = ''',CREF,''''
      WRITE(*,400) ' LCHR = ',LCHR(CHAR,CREF)
      CHAR='A=B*2+12'
      CREF='LSDSDSAF'
      WRITE(*,300) ' CHAR = ''',CHAR,''''
      WRITE(*,300) ' CREF = ''',CREF,''''
      WRITE(*,400) ' LCHR = ',LCHR(CHAR,CREF)

  100 FORMAT(A)
  200 FORMAT(4A,L4)
  300 FORMAT(3A)
  400 FORMAT(A,L4)

      END
