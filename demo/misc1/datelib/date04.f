*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM DATE04

      CHARACTER CFORM*24


      WRITE(*,*) '*** TEST FOR CHARACTER DATE & DATE INQUIRY'
      WRITE(*,*) '*** TYPE-1'
      CFORM='YYYY,MM,DD'
      WRITE(*,*) 'CFORM = ',CFORM
      CALL DATEQ1(IDATE)
      CALL DATEC1(CFORM,IDATE)
      WRITE(*,*) 'AFTER CALLING DATEQ1(IDATE)'
      WRITE(*,*) '    AND       DATEC1(CFORM,IDATE)'
      WRITE(*,*) 'CFORM = ',CFORM
      WRITE(*,*) '*** TYPE-2'
      CFORM='CCCCCCCCC DD,YYYY'
      WRITE(*,*) 'CFORM = ',CFORM
      CALL DATEQ2(IY,ITD)
      CALL DATEC2(CFORM,IY,ITD)
      WRITE(*,*) 'AFTER CALLING DATEQ2(IY,ITD)'
      WRITE(*,*) '    AND       DATEC2(CFORM,IY,ITD)'
      WRITE(*,*) 'CFORM = ',CFORM
      WRITE(*,*) '*** TYPE-3'
      CFORM='YY/MM/DD (WWW)'
      WRITE(*,*) 'CFORM = ',CFORM
      CALL DATEQ3(IY,IM,ID)
      CALL DATEC3(CFORM,IY,IM,ID)
      WRITE(*,*) 'AFTER CALLING DATEQ3(IY,IM,ID)'
      WRITE(*,*) '    AND       DATEC3(CFORM,IY,IM,ID)'
      WRITE(*,*) 'CFORM = ',CFORM

      END
