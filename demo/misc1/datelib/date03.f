*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM DATE03

      CHARACTER CWEEK*9


      CALL DATEQ3(IY,IM,ID)
      CALL DATE31(IDATE,IY,IM,ID)
      CALL DATE32(IY,IM,ID,ITD)

      WRITE(*,*) '*** TYPE-1'
      WRITE(*,*) 'DATE = ',IDATE,' ;'
      WRITE(*,*) 'WEEK = ',CWEEK(IWEEK1(IDATE))

      WRITE(*,*) '*** TYPE-2'
      WRITE(*,*) 'YEAR = ',IY,' ; TOTAL DAYS = ',ITD,' ;'
      WRITE(*,*) 'WEEK = ',CWEEK(IWEEK2(IY,ITD))

      WRITE(*,*) '*** TYPE-3'
      WRITE(*,*) 'YEAR = ',IY,' ; MONTH = ',IM,' ; DAY = ',ID,' ;'
      WRITE(*,*) 'WEEK = ',CWEEK(IWEEK3(IY,IM,ID))

      END
