*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM DATE01


      WRITE(*,*) '*** TEST FOR DATE CONVERSION'
      IDATE=19890215
      WRITE(*,*) 'IDATE = ',IDATE
      CALL DATE12(IDATE,IY,ITD)
      WRITE(*,*) 'AFTER CALLING DATE12(IDATE,IY,ITD)'
      WRITE(*,*) 'IY = ',IY,' : ITD = ',ITD
      CALL DATE21(JDATE,IY,ITD)
      WRITE(*,*) 'AFTER CALLING DATE21(JDATE,IY,ITD)'
      WRITE(*,*) 'JDATE = ',JDATE
      CALL DATE13(JDATE,IY,IM,ID)
      WRITE(*,*) 'AFTER CALLING DATE13(JDATE,IY,IM,ID)'
      WRITE(*,*) 'IY = ',IY,' : IM = ',IM,' : ID = ',ID
      CALL DATE31(KDATE,IY,IM,ID)
      WRITE(*,*) 'AFTER CALLING DATE31(KDATE,IY,IM,ID)'
      WRITE(*,*) 'KDATE = ',KDATE

      END
