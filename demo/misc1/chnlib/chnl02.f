*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM CHNL02

      CHARACTER CTEXT*80,CA*8,CFMT*16


      CTEXT='ERROR NUMBER = ###.'
      CA='###'
      II=12
      CFMT='(I3.3)'
      WRITE(*,*) 'CTEXT = ',CTEXT(1:LENC(CTEXT))
      WRITE(*,*) 'CA = ',CA
      WRITE(*,*) 'II = ',II
      WRITE(*,*) 'CFMT = ',CFMT
      WRITE(*,*) 'AFTER CALLING CHNGI(CTEXT,CA,II,CFMT)'
      CALL CHNGI(CTEXT(1:LENC(CTEXT)),CA(1:LENC(CA)),II,CFMT)
      WRITE(*,*) 'CTEXT = ',CTEXT(1:LENC(CTEXT))

      END
