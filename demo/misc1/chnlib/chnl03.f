*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM CHNL03

      CHARACTER CTEXT*80,CA*8,CFMT*16


      CTEXT='CONTOUR INTERVAL = ########.'
      CA='########'
      RR=1.2
      CFMT='(F8.4)'
      WRITE(*,*) 'CTEXT = ',CTEXT(1:LENC(CTEXT))
      WRITE(*,*) 'CA = ',CA
      WRITE(*,*) 'RR = ',RR
      WRITE(*,*) 'CFMT = ',CFMT
      WRITE(*,*) 'AFTER CALLING CHNGI(CTEXT,CA,RR,CFMT)'
      CALL CHNGR(CTEXT(1:LENC(CTEXT)),CA(1:LENC(CA)),RR,CFMT)
      WRITE(*,*) 'CTEXT = ',CTEXT(1:LENC(CTEXT))

      END
