*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM CHNL01

      CHARACTER CTEXT*80,CA*8,CB*8

      CTEXT='THIS MONTH IS ###.'
      CA='###'
      CB='APR'
      WRITE(*,*) 'CTEXT = ',CTEXT(1:LENC(CTEXT))
      WRITE(*,*) 'CA = ',CA
      WRITE(*,*) 'CB = ',CB
      CALL CHNGC(CTEXT(1:LENC(CTEXT)),CA(1:LENC(CA)),CB(1:LENC(CB)))
      WRITE(*,*) 'AFTER CALLING CHNGC(CTEXT,CA,CB)'
      WRITE(*,*) 'CTEXT = ',CTEXT(1:LENC(CTEXT))

      END
