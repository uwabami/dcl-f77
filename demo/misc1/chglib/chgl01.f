*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM CHGL01

      CHARACTER CH*16


      CH='ABCDefgh'
      WRITE(*,'(A,A)') ' CH = ',CH
      CALL CUPPER(CH)
      WRITE(*,'(A)') ' AFTER CALLING CUPPER'
      WRITE(*,'(A,A)') ' CH = ',CH
      CH='ABCDefgh'
      CALL CLOWER(CH)
      WRITE(*,'(A)') ' AFTER CALLING CLOWER'
      WRITE(*,'(A,A)') ' CH = ',CH

      END
