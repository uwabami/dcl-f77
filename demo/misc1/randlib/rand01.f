*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM RAND01

      PARAMETER (NMAX=10000)


      ISEED=1
      X=RNGU0(ISEED)
      NN=0
      DO 10 I=1,NMAX
        Y=X
        X=RNGU0(ISEED)
        IF (X*X+Y*Y.LE.1) NN=NN+1
   10 CONTINUE
      WRITE(*,*) 'PI = ',4.0*NN/NMAX

      END
