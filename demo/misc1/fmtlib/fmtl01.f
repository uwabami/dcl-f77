*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM FMTL01

      PARAMETER (N=3)

      REAL      VAL(N)

      CHARACTER CVAL*8

      DATA      VAL / 0.0, -0.012, 1230000 /


      DO 10 I=1,N
        WRITE(*,*) 'VAL = ',VAL(I)
        CALL CHVAL('A',VAL(I),CVAL)
        WRITE(*,*) 'OPTION=A : VAL = ',CVAL
        CALL CHVAL('B',VAL(I),CVAL)
        WRITE(*,*) 'OPTION=B : VAL = ',CVAL
        CALL CHVAL('C',VAL(I),CVAL)
        WRITE(*,*) 'OPTION=C : VAL = ',CVAL
        CALL CHVAL('D',VAL(I),CVAL)
        WRITE(*,*) 'OPTION=D : VAL = ',CVAL
   10 CONTINUE

      END
