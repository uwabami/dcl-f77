*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM TIME02

      CHARACTER CFORM*24


      WRITE(*,*) '*** TEST FOR CHARACTER TIME & TIME INQUIRY'
      WRITE(*,*) '*** TYPE-1'
      CFORM='HH,MM,SS'
      WRITE(*,*) 'CFORM = ',CFORM
      CALL TIMEQ1(ITIME)
      CALL TIMEC1(CFORM,ITIME)
      WRITE(*,*) 'AFTER CALLING TIMEQ1(ITIME)'
      WRITE(*,*) '    AND       TIMEC1(CFORM,ITIME)'
      WRITE(*,*) 'CFORM = ',CFORM
      WRITE(*,*) '*** TYPE-2'
      CFORM='HHhMMmSSs'
      WRITE(*,*) 'CFORM = ',CFORM
      CALL TIMEQ2(ITT)
      CALL TIMEC2(CFORM,ITT)
      WRITE(*,*) 'AFTER CALLING TIMEQ2(ITT)'
      WRITE(*,*) '    AND       TIMEC2(CFORM,ITT)'
      WRITE(*,*) 'CFORM = ',CFORM
      WRITE(*,*) '*** TYPE-3'
      CFORM='HH:MM:SS'
      WRITE(*,*) 'CFORM = ',CFORM
      CALL TIMEQ3(IH,IM,IS)
      CALL TIMEC3(CFORM,IH,IM,IS)
      WRITE(*,*) 'AFTER CALLING TIMEQ3(IH,IM,IS)'
      WRITE(*,*) '    AND       TIMEC3(CFORM,IH,IM,IS)'
      WRITE(*,*) 'CFORM = ',CFORM

      END
