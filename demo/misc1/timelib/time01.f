*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM TIME01


      WRITE(*,*) '*** TEST FOR TIME CONVERSION'
      ITIME=100930
      WRITE(*,*) 'ITIME = ',ITIME
      CALL TIME12(ITIME,ITT)
      WRITE(*,*) 'AFTER CALLING TIME12(ITIME,ITT)'
      WRITE(*,*) 'ITT = ',ITT
      CALL TIME21(JTIME,ITT)
      WRITE(*,*) 'AFTER CALLING TIME21(JTIME,ITT)'
      WRITE(*,*) 'JTIME = ',JTIME
      CALL TIME13(JTIME,IH,IM,IS)
      WRITE(*,*) 'AFTER CALLING TIME13(JTIME,IH,IM,IS)'
      WRITE(*,*) 'IH = ',IH,' : IM = ',IM,' : IS = ',IS
      CALL TIME31(KTIME,IH,IM,IS)
      WRITE(*,*) 'AFTER CALLING TIME31(KTIME,IH,IM,IS)'
      WRITE(*,*) 'KTIME = ',KTIME

      END
