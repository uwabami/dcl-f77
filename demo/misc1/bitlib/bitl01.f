*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM BITL01

      CHARACTER CP*32

      DATA      IP/255/


      CALL BITPIC(IP,CP)
      CALL BITPCI(CP,JP)
      WRITE(*,'(A,I8)') ' INTEGER CONSTANT IP = ',IP
      WRITE(*,'(A)') ' AFTER CALLING BITPIC(IP,CP)'
      WRITE(*,'(A,A)') ' BIT PATTERN CP = ',CP
      WRITE(*,'(A)') ' AFTER CALLING BITPCI(CP,JP)'
      WRITE(*,'(A,I8)') ' INTEGER CONSTANT JP = ',JP

      END
