*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM CLCK01


      WRITE(*,*) '*** TEST FOR MEASURING RUNTIME'
      CALL CLCKST
      ISEED=1
      X=RNGU1(ISEED)
      DO 10 I=1,10000000
        X=RNGU1(ISEED)
        SX=SIN(X)
   10 CONTINUE
      CALL CLCKGT(TIME)
      CALL CLCKDT(DT0)
      WRITE(*,*) '*** EXECUTION TIME = ',TIME
      WRITE(*,*) '*** PRECISION = ',DT0

      END
