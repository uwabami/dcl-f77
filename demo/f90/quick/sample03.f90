
program sample03

  use dcl
  integer,parameter :: nx=36, ny=36
  real,parameter :: xmin=0., xmax=360., ymin=-90., ymax=90.
  real,dimension(0:nx,0:ny) :: p

!-- 球面調和関数 ----

    do j=0,ny
      do i=0,nx
        alon = ( xmin + (xmax-xmin)*i/nx ) * DCL_PI/180.
        alat = ( ymin + (ymax-ymin)*j/ny ) * DCL_PI/180.
        p(i,j) = sqrt(1-sin(alat)**2) * sin(alat) * cos(alon)
      end do
    end do

!-- グラフ ----

    call DclOpenGraphics()
    call DclNewFrame

!    call DclSetParm('ENABLE_SOFTFILL',.true.)

    call DclSetWindow( xmin, xmax, ymin, ymax )
    call DclSetViewPort( 0.1, 0.9, 0.2, 0.8 )
    call DclSetTransFunction

    call DclShadeContour( p )

    call DclDrawScaledAxis

    call DclDrawContour( p )

    call DclCloseGraphics

end program
