program sample07

  use dcl
  integer,parameter :: nx=20, ny=20
  real,parameter :: xmin=-1., xmax=1., ymin=-1., ymax=1.
  real,dimension(0:nx,0:ny) :: u,v

!-- 変形場 ----

    do j=0,ny
      do i=0,nx
        x = xmin + (xmax-xmin)*i/nx
        y = ymin + (ymax-ymin)*j/ny
        u(i,j) =  0.8 * x           ! u =  a*x
        v(i,j) = -1.  * y           ! y = -b*y
      end do
    end do

!-- グラフ ----

    call DclOpenGraphics()
    call DclNewFrame

    call DclSetWindow( xmin, xmax, ymin, ymax )
    call DclSetViewPort( 0.2, 0.8, 0.2, 0.8 )
    call DclSetTransFunction

    call DclDrawScaledAxis

    call DclDrawVectors( u, v )

    call DclCloseGraphics

end program
