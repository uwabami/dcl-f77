
program miss02

  use dcl

  integer,parameter :: nx=36, ny=36
  real,parameter :: xmin=0, xmax=360, ymin=-90, ymax=90
  real,parameter :: pi=3.14159, drad=pi/180
  real :: p(0:nx,0:ny)

    call DclGetParm( 'MISSING_REAL', rmiss )
    call DclSetParm( 'INTERPRET_MISSING_VALUE', .true. )

    do j=0,ny
      alat = ( ymin + (ymax-ymin) * j/ny ) * drad
      slat = sin(alat)
      do i=0,nx
        alon = ( xmin + (xmax-xmin) * i/nx ) * drad
        p(i,j) = 3*sqrt(1-slat**2)*slat*cos(alon) - 0.5*(3*slat**2-1)

        if( i==6 .and. j==6 ) then
          p(i,j) = rmiss
        end if
        if( ( 8 <= i .and. i <= 24) .and. j==30 ) then
          p(i,j) = rmiss
        end if
        if( (22 <= i .and. i <= 30) .and. (12 <= j .and. j <= 20) ) then
          p(i,j) = rmiss
        end if
      end do
    end do

    call DclOpenGraphics()
    call DclNewFrame

    call DclSetWindow( xmin, xmax, ymin, ymax )
    call DclSetViewPort(  0.2,  0.8,  0.2,  0.8 )
    call DclSetTransFunction

    call DclShadeContour( p )
    call DclDrawScaledAxis
    call DclDrawContour( p )

    call DclCloseGraphics

end program
