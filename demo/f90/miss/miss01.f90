
program miss01

  use dcl

  integer,parameter :: nmax=40
  real,parameter :: xmin=0., xmax=4*DCL_PI, ymin=-1., ymax=1.
  real :: x(0:nmax), y(0:nmax)

    x = (/( xmax*real(n)/nmax, n=0,nmax )/)
    y = sin(x)

    n1 = nmax/4
    y(n1-1:n1+1) = 999.

    n2 = n1*3
    y(n2-1) = 999.
    y(n2+1) = 999.

    call DclOpenGraphics()
    call DclNewFrame

!-- 欠損値処理なし ----
    call DclSetWindow( xmin, xmax, ymin, ymax )
    call DclSetViewPort(  0.1,  0.9,  0.7,  0.9 )
    call DclSetTransNumber( 1 )
    call DclSetTransFunction

    call DclDrawLine( x, y )

    call DclSetWindow( xmin, xmax, ymin, ymax )
    call DclSetViewPort(  0.1,  0.9,  0.5,  0.7 )
    call DclSetTransFunction

    call DclDrawMarker( x, y, type=3 )

!-- 欠損値処理 ----
    call DclSetParm('INTERPRET_MISSING_VALUE', .true. )

    call DclSetWindow( xmin, xmax, ymin, ymax )
    call DclSetViewPort(  0.1,  0.9,  0.3,  0.5 )
    call DclSetTransFunction

    call DclDrawLine( x, y )

    call DclSetWindow( xmin, xmax, ymin, ymax )
    call DclSetViewPort(  0.1,  0.9,  0.1,  0.3 )
    call DclSetTransFunction

    call DclDrawMarker( x, y )

    call DclCloseGraphics

end program
