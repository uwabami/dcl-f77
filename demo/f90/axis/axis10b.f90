!-----------------------------------------------------------------------
program uxyz10
  use dcl

  integer,parameter :: id0=19811201, nd=180

    call DclOpenGraphics()

    call DclSetAxisFactor( 0.7 )
    call DclSetParm( 'AXIS:loffset',.true.)

    call DclNewFrame

    call DclSetWindow( 0.0, real(nd), 0.0, 100.0 )
    call DclSetViewPort( 0.4, 0.9, 0.3, 0.8 )
    call DclSetTransNumber( DCL_UNI_UNI )
    call DclSetTransFunction

    call DclDrawViewPortFrame( 1 )

    call DclDrawXAxisCalendar( 'b', id0, nd )
    call DclShiftXAxis( 'b' )
    call DclDrawXAxis( 'b', 20.0, 10.0 )
    call DclDrawXSubTitle( 'b', 'Day Number', 0.0 )
    call DclDrawXAxisCalendar( 't', id0, nd )

    call DclDrawYAxis( 'l', 10.0, 5.0 )
    call DclDrawYAxis( 'r', 10.0, 5.0 )
    call DclDrawYSubTitle( 'l', 'Celsius Scale', 0.0 )
    call DclShiftYAxis( 'l' )
    call DclSetParm( 'AXIS:yoffset', 273.15 )
    call DclSetParm( 'AXIS:yfact  ', 1.0 )
    call DclDrawYAxis( 'l', 10.0, 5.0 )
    call DclDrawYSubTitle( 'l', 'Kelvin Scale', 0.0 )
    call DclShiftYAxis( 'l' )
    call DclSetParm( 'AXIS:yoffset', 32.0 )
    call DclSetParm( 'AXIS:yfact  ', 1.8 )
    call DclDrawYAxis( 'l', 20.0, 10.0 )
    call DclDrawYSubTitle( 'l', 'Fahrenheit Scale', 0.0 )

    call DclDrawXSubTitle( 't', '( AXIS:lofset=.true. )', 0.0 )
    call DclDrawXSubTitle( 't', 'DclShiftXAxis', 0.0 )

    call DclCloseGraphics

end program
