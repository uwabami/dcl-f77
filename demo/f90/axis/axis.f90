!-----------------------------------------------------------------------
!     Copyright (c) 2002 GFD Dennou Club. All Rights Reserved.
!-----------------------------------------------------------------------
program axis

  use dcl

    call DclOpenGraphics
    call DclNewFrame

    call DclSetWindow( 1985.0, 1990.0, -2.0, +2.0 )
    call DclSetViewPort( 0.2, 0.8, 0.3, 0.7 )
    call DclSetTransFunction

    call DclSetParm( 'ENABLE_SPAN_LABELING', .true. )
    call uxsfmt( '(i4)' )
    call DclDrawAxis( 'h', 0.25, 1.0, section=0.0 )
    call DclSetParm( 'ENABLE_SPAN_LABELING', .false. )
    call DclDrawTitle( 'h', 'year', +1.0 )


    call DclSetParm( 'TICKMARK_SIDE', -1 )
!    call DclSetYLabelFormat( '(F4.1)' )
    call DclDrawAxis( 'l', 0.25, 1. )
    call DclDrawTitle( 'l', 'S.O.Index', 0. )

    call DclDrawTitle( 't', 'Axis', 0.0, sw=2 )

    call DclCloseGraphics

end program
