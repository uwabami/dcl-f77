
program axis05

  use dcl
  real,parameter :: rlat1=20., rlat2=80., dlat1=5., dlat2=10.
  integer,parameter :: days=180
  type(dcl_date) :: date

    date%year = 1981
    date%month = 12
    date%day = 1

    call DclOpenGraphics()

    call DclSetAxisFactor( 0.7 )
    call DclNewFrame

    call DclSetWindow( 0.0, real(days), rlat1, rlat2 )
    call DclSetViewPort( 0.2, 0.8, 0.4, 0.8 )
    call DclSetTransFunction

    call DclDrawViewPortFrame( 1 )

    call DclShiftAxis( 'b' )
    call DclDrawAxisCalendar( 'b', date, nd=days )
    call DclShiftAxis( 'b' )
    call DclDrawAxis( 'b', 20.0, 10.0 )
    call DclDrawTitle( 'b', 'Day Number', 0.0 )
    call DclShiftAxis( 't' )
    call DclDrawAxisCalendar( 't', date, nd=days )

    call DclShiftAxis( 'l' )
    call DclDrawAxis( 'l', dlat2, dlat1 )
    call DclShiftAxis( 'r' )
    call DclDrawAxis( 'r', dlat2, dlat1 )
    call DclDrawTitle( 'l', 'Latitude', 0.0 )

    call DclDrawTitle( 't', 'DclShiftAxis', 0.0, 2 )

    call DclCloseGraphics

end program
