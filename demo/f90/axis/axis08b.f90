!-----------------------------------------------------------------------
program ucpk02
  use dcl

    integer,parameter :: jd0=19920401
    character(len=32) :: ctl

    call DclOpenGraphics( +abs(DclSelectDevice()) )

    call DclSetParm( 'GRAPH:lfull',.true.)
    call DclSetAxisFactor(0.7)
    call DclSetAspectRatio(1.0,0.75)
    call DclDivideFrame('y',7,1)

    call DclNewFrame
    nd=30
    call DclSetWindow(0.0,1.0,0.0,real(nd))
    call DclSetViewPort(0.13,0.14,0.1,0.9)
    call DclSetTransNumber(1)
    call DclSetTransFunction
    call DclDrawYAxisCalendar('l',jd0,nd)
    ctl='DclDrawYAxisCalendar (30days)'
    call DclDrawYSubTitle('l',ctl,0.0)

    call DclNewFrame
    nd=90
    call DclSetWindow(0.0,1.0,0.0,real(nd))
    call DclSetViewPort(0.13,0.14,0.1,0.9)
    call DclSetTransNumber(1)
    call DclSetTransFunction
    call DclDrawYAxisCalendar('l',jd0,nd)
    ctl='DclDrawYAxisCalendar (90days)'
    call DclDrawYSubTitle('l',ctl,0.0)

    call DclNewFrame
    nd=180
    call DclSetWindow(0.0,1.0,0.0,real(nd))
    call DclSetViewPort(0.13,0.14,0.1,0.9)
    call DclSetTransNumber(1)
    call DclSetTransFunction
    call DclDrawYAxisCalendar('l',jd0,nd)
    ctl='DclDrawYAxisCalendar (180days)'
    call DclDrawYSubTitle('l',ctl,0.0)

    call DclNewFrame
    nd=400
    call DclSetWindow(0.0,1.0,0.0,real(nd))
    call DclSetViewPort(0.13,0.14,0.1,0.9)
    call DclSetTransNumber(1)
    call DclSetTransFunction
    call DclDrawYAxisCalendar('l',jd0,nd)
    ctl='DclDrawYAxisCalendar (400days)'
    call DclDrawYSubTitle('l',ctl,0.0)

    call DclNewFrame
    nd=60
    call DclSetWindow(0.0,1.0,0.0,real(nd))
    call DclSetViewPort(0.13,0.14,0.1,0.9)
    call DclSetTransNumber(1)
    call DclSetTransFunction
    call DclDrawYAxisDay('l',jd0,nd)
    ctl='DclDrawYAxisDay (60days)'
    call DclDrawYSubTitle('l',ctl,0.0)

    call DclNewFrame
    nd=120
    call DclSetWindow(0.0,1.0,0.0,real(nd))
    call DclSetViewPort(0.13,0.14,0.1,0.9)
    call DclSetTransNumber(1)
    call DclSetTransFunction
    call DclDrawYAxisMonth('l',jd0,nd)
    ctl='DclDrawYAxisMonth (120days)'
    call DclDrawYSubTitle('l',ctl,0.0)

    call DclNewFrame
    nd=2000
    call DclSetWindow(0.0,1.0,0.0,real(nd))
    call DclSetViewPort(0.13,0.14,0.1,0.9)
    call DclSetTransNumber(1)
    call DclSetTransFunction
    call DclDrawYAxisYear('l',jd0,nd)
    ctl='DclDrawYAxisYear (2000days)'
    call DclDrawYSubTitle('l',ctl,0.0)

    call DclCloseGraphics

end program
