
program axis06

  use dcl

  integer,parameter :: nx1=21, nx2= 5, ny2=18
  real,dimension(nx1) :: rx1 = (/ &
    -50,-45,-40,-35,-30,-25,-20,-15,-10, -5,  0, &
      5, 10, 15, 20, 25, 30, 35, 40, 45, 50 /)
  real,dimension(nx2) :: rx2 = (/ &
     -40 ,  -20 ,    0 ,   20 ,    40 /)
  real,dimension(ny2) :: ry2 = (/ &
    1000. , 850.  , 700.  , 500.  , 400.  , 300.  , &
     250. , 200.  , 150.  , 100.  , 70.   , 50.   , &
      30. , 10.   , 5.    , 2.    , 1.    , 0.4   /)
  character(len=4),dimension(nx2) :: cx2 = (/ &
    '40S ','20S ','EQ  ','20N ','40N ' /)
  character(len=4),dimension(ny2) :: cy2 = (/ &
    '1000','    ','    ','500 ','    ','    ', &
    '    ','200 ','    ','100 ','    ','50  ', &
    '30  ','10  ','5   ','2   ','1   ','.4  ' /)

    call DclOpenGraphics()

    call DclNewFrame

    call DclSetWindow( -50.0, +50.0, 1.0e3, 0.4 )
    call DclSetViewPort( 0.2, 0.8, 0.2, 0.8 )
    call DclSetTransNumber( DCL_UNI_LOG )
    call DclSetTransFunction

    call DclDrawAxisSpecify( 'b', rx2, rx1, label=cx2 )
    call DclDrawAxisSpecify( 't', rx2, rx1 )
    call DclDrawTitle( 'b', 'Latitude', 0.0 )

    call DclDrawAxisSpecify( 'l', ry2, label=cy2 )
    call DclDrawAxisSpecify( 'r', ry2 )
    call DclDrawTitle( 'l', 'Pressure (hPa)', 0.0 )

    call DclDrawTitle( 't', 'DclDrawAxisSpecify', 0.0, 2 )

    call DclCloseGraphics

end program
