!-----------------------------------------------------------------------
!     Copyright (c) 2002 GFD Dennou Club. All Rights Reserved.
!-----------------------------------------------------------------------
program many

  use dcl
  integer,parameter :: days=180
  type(dcl_date) :: date

    date%year = 1981
    date%month = 12
    date%day = 1

    call DclOpenGraphics

    call DclSetAxisFactor( 0.7 )
    call DclSetParm( 'ENABLE_LINEAR_OFFSET',.true.)

    call DclNewFrame

    call DclSetWindow( 0.0, real(days), 0.0, 100.0 )
    call DclSetViewPort( 0.4, 0.9, 0.3, 0.8 )
    call DclSetTransFunction

    call DclDrawViewPortFrame( 1 )

    call DclDrawAxisCalendar( 'b', date, nd=days )
    call DclShiftAxis( 'b' )
    call DclDrawAxis( 'b', 20.0, 10.0 )
    call DclDrawTitle( 'b', 'Day Number', 0.0 )
    call DclDrawAxisCalendar( 't', date, nd=days )

    call DclDrawAxis( 'lr', 10.0, 5.0 )
    call DclDrawTitle( 'l', 'Celsius Scale', 0.0 )
    call DclShiftAxis( 'l' )
    call DclSetParm( 'Y_AXIS_OFFSET', 273.15 )
    call DclSetParm( 'Y_AXIS_FACTOR', 1.0 )
    call DclDrawAxis( 'l', 10.0, 5.0 )
    call DclDrawTitle( 'l', 'Kelvin Scale', 0.0 )
    call DclShiftAxis( 'l' )
    call DclSetParm( 'Y_AXIS_OFFSET', 32.0 )
    call DclSetParm( 'Y_AXIS_FACTOR', 1.8 )
    call DclDrawAxis( 'l', 20.0, 10.0 )
    call DclDrawTitle( 'l', 'Fahrenheit Scale', 0.0 )

!    call DclDrawTitle( 't', '( AXIS:lofset=.true. )', 0.0 )
    call DclDrawTitle( 't', 'DclShiftXAxis', sw=2 )

    call DclCloseGraphics

end program
