
program axis03

  use dcl

    call DclOpenGraphics()
    call DclNewFrame

    call DclSetWindow( 1.0e0, 1.0e5, 1.0e3, 1.0e0 )
    call DclSetViewPort( 0.2, 0.8, 0.2, 0.8 )
    call DclSetTransNumber( DCL_LOG_LOG )
    call DclSetTransFunction

    call DclDrawAxisLog( 'bt', 1, 9 )
    call DclDrawTitle( 'b', '[x]', 1.0 )

    call DclDrawAxisLog( 'lr', 3, 9 )
    call DclDrawTitle( 'l', '[y]', 1.0 )

    call DclDrawTitle( 't', 'Log Axis', 0.0, 2 )

    call DclCloseGraphics

end program
