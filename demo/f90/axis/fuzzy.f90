!-----------------------------------------------------------------------
!     Copyright (c) 2002 GFD Dennou Club. All Rights Reserved.
!-----------------------------------------------------------------------
program fuzzy

  use dcl

    call DclOpenGraphics()
    call DclNewFrame

    call DclSetWindow( -50.0, +50.0, 1.0e3, 0.4 )
    call DclSetViewPort( 0.2, 0.8, 0.2, 0.8 )
    call DclSetTransNumber( 2 )
    call DclSetTransFunction

    call DclDrawAxis( 'bt', 5.0, 20.0 )
    call DclDrawTitle( 'b', 'Latitude', 0.0 )

    call DclSetParm( 'iytype', 3 )
    call DclDrawAxisLog( 'lr', 3, 9 )
    call DclDrawTitle( 'l', 'Pressure (mb)', 0.0 )
!    call DclDrawTitle( 't', 'uxaxdv/ulylog', 0.0, sw=2 )

    call DclCloseGraphics

end program
