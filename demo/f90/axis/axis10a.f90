!-----------------------------------------------------------------------
program uxyz08
  use dcl

  real,parameter :: x1=-180., x2=+180., dx1=10., dx2=60.
  real,parameter :: y1= -90., y2= +90., dy1=10., dy2=30.

    call DclOpenGraphics()

    call DclNewFrame

    call DclSetWindow( x1, x2, y1, y2 )
    call DclSetViewPort( 0.2, 0.8, 0.3, 0.7 )
    call DclSetTransNumber( DCL_UNI_UNI )
    call DclSetTransFunction

    call DclSetParm( 'AXIS:inner', -1 )
    call DclSetParm( 'AXIS:uxuser', 0.0 )
    call DclSetParm( 'AXIS:uyuser', 0.0 )
    call DclSetParm( 'AXIS:labelxu', .false. )
    call DclSetParm( 'AXIS:labelyu', .false. )

    call DclDrawXAxis( 'b', dx2, dx1 )
    call DclDrawXAxis( 't', dx2, dx1 )
    call DclDrawXSubTitle( 'b', 'Longitude', 0.0 )

    call DclDrawYAxis( 'l', dy2, dy1 )
    call DclDrawYAxis( 'r', dy2, dy1 )
    call DclDrawYSubTitle( 'l', 'Latitude', 0.0 )

    call DclDrawXMainTitle( 't', 'DclDrawXAxis', 0.0 )

    call DclSetAxisFactor( 0.5 )
    call DclDrawXAxis( 'u', dx2, dx1 )
    call DclDrawXSubTitle( 'u', 'EQ', +0.9 )
    call DclDrawYAxis( 'u', dy2, dy1 )
    call DclDrawYSubTitle( 'u', 'GM', -0.9 )
    call DclSetParm( 'AXIS:inner', +1 )
    call DclDrawXAxis( 'u', dx2, dx1 )
    call DclDrawYAxis( 'u', dy2, dy1 )

    call DclCloseGraphics

end program
