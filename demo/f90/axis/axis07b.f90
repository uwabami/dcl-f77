!-----------------------------------------------------------------------
program ulpk02
  use dcl

  integer,parameter :: nbl=2
  real,dimension(nbl) :: bl
  character*64 :: ctl

    bl = (/ 1.0,3.0 /)

    call DclOpenGraphics( DclSelectDevice() )

    call DclSetParm( 'GRAPH:lfull',.true. )
    call DclSetAxisFactor(0.7)
    call DclSetAspectRatio(1.0,0.75)
    call DclDivideFrame('y',7,1)

    do i=1,7

      call DclNewFrame
      call DclSetWindow(0.2,200.,0.15,200.)
      call DclSetViewPort(0.13,0.14,0.1,0.9)
      call DclSetTransNumber( DCL_LOG_LOG )
      call DclSetTransFunction

      select case(i)
      case(1)
        call DclSetParm( 'LOG:IYTYPE',1)
        call DclDrawYLogAxis('l',1,9)
        ctl='iytype=1,nlbl=1,ntck=9'
      case(2)
        call DclSetParm( 'LOG:IYTYPE',1)
        call DclDrawYLogAxis('l',3,9)
        ctl='iytype=1,nlbl=3,ntck=9'
      case(3)
        call DclSetParm( 'LOG:IYTYPE',2)
        call DclDrawYLogAxis('l',3,9)
        ctl='iytype=2,nlbl=3,ntck=9'
      case(4)
        call DclSetParm( 'LOG:IYTYPE',3)
        call DclDrawYLogAxis('l',3,9)
        ctl='iytype=3,nlbl=3,ntck=9'
      case(5)
        call DclSetParm( 'LOG:IYTYPE',4)
        call DclDrawYLogAxis('l',3,9)
        ctl='iytype=4,nlbl=3,ntck=9'
      case(6)
        call DclSetYLogFormat('(f5.1)')
        call DclSetParm( 'LOG:iytype',3)
        call DclDrawYLogAxis('l',3,9)
        ctl='iytype=3,nlbl=3,ntck=9'
      case(7)
        call DclSetParm( 'LOG:IYTYPE',1)
        call DclSetParm( 'LOG:IYCHR',194)
        call DclSetYLogLabel( bl )
        call DclDrawYLogAxis('l',4,5)
        ctl='iytype=1,nlbl=4,ntck=5'
      end select
      call DclDrawYSubTitle('l',ctl,0.0)
    end do

    call DclCloseGraphics

end program

