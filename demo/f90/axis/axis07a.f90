!-----------------------------------------------------------------------
program ulpk01
  use dcl

  integer,parameter :: nbl=2
  real,dimension(nbl) :: bl
  character(len=64) :: ctl

    bl = (/ 1.0,3.0 /)

    call DclOpenGraphics( -DclSelectDevice() )

    call DclSetParm( 'GRAPH:lfull',.true.)
    call DclSetAxisFactor(0.7)
    call DclSetAspectRatio(0.75,1.0)
    call DclDivideFrame('t',1,7)

    do i=1,7

      call DclNewFrame
      call DclSetWindow(0.15,200.,0.1,1e5)
      call DclSetViewPort(0.1,0.9,0.1,0.11)
      call DclSetTransNumber( DCL_LOG_LOG )
      call DclSetTransFunction

      select case(i)
      case(1)
        call DclSetParm( 'LOG:ixtype',1)
        call DclDrawAxisLog('b',1,9)
        ctl='ixtype=1,nlbl=1,ntck=9'
      case(2)
        call DclSetParm( 'LOG:ixtype',1)
        call DclDrawAxisLog('b',3,9)
        ctl='ixtype=1,nlbl=3,ntck=9'
      case(3)
        call DclSetParm( 'LOG:ixtype',2)
        call DclDrawAxisLog('b',3,9)
        ctl='ixtype=2,nlbl=3,ntck=9'
      case(4)
        call DclSetParm( 'LOG:ixtype',3)
        call DclDrawAxisLog('b',3,9)
        ctl='ixtype=3,nlbl=3,ntck=9'
      case(5)
        call DclSetParm( 'LOG:ixtype',4)
        call DclDrawAxisLog('b',3,9)
        ctl='ixtype=4,nlbl=3,ntck=9'
      case(6)
        call DclSetParm( 'LOG:ixtype',3)
        call DclDrawAxisLog('b',3,9,cformat='(f5.1)')
        ctl='ixtype=3,nlbl=3,ntck=9'
      case(7)
        call DclSetParm( 'LOG:ixtype',1)
        call DclSetParm( 'LOG:ixchr',194)
        CALL ULSXBL(BL,NBL)
!        call DclSetXLogLabel( bl )
        call DclDrawAxisLog('b',4,5)
        ctl='ixtype=1,nlbl=4,ntck=5'
      end select

      call DclDrawTitle('b',ctl,0.0,1)
    end do

    call DclCloseGraphics

end program

