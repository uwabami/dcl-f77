
program sample_2d02

  use dcl
  integer,parameter :: nx=18, ny=18
  real,parameter :: xmin=0, xmax=360, dx1=10, dx2=60
  real,parameter :: ymin=-90, ymax=90
  real,parameter :: pi=3.141592, drad=pi/180, dz=0.05
  integer,parameter :: my=6, nc=3

  real ::      p(0:nx, 0:ny), uy1(0:ny), uy2(0:my)
  character(len=nc),dimension(0:my) :: ch

    ch = (/ 'SP ', '60S', '30S', 'EQ ', '30N', '60N', 'NP ' /)

    do j = 0, ny
      alat = ( ymin + (ymax-ymin) * j/ny ) * drad
      slat = sin(alat)
      uy1(j) = slat
      do i = 0, nx
        alon = ( xmin + (xmax-xmin) * i/nx ) * drad
        p(i,j) = cos(alon) * (1-slat**2) * sin(2*pi*slat) + dz
      end do
    end do

    do j = 0, my
      alat = ( ymin + (ymax-ymin) * j/my ) * drad
      slat = sin(alat)
      uy2(j) = slat
    end do

    call DclOpenGraphics()
    call DclNewFrame

    call DclSetWindow( xmin, xmax, uy1(1), uy1(ny) )
    call DclSetViewPort( 0.2, 0.8, 0.2, 0.8 )
    call DclSetTransFunction

    call DclDrawAxis( 'bt', dx2, dx1 )
    call DclDrawTitle( 'b', 'LONGITUDE', 0.0 )

    call DclDrawAxisSpecify( 'lr', uy2, uy1, ch )
    call DclDrawTitle( 'l', 'LATITUDE', 0.0 )

    call DclSetXEvenGrid( xmin, xmax, nx+1 )
    call DclSetYGrid( uy1 )

    call DclSetContourLabelFormat( '(f6.1)' )
    call DclSetContourLevel( p, 0.2 )
    call DclSetContourLine( 0.1, index=1, type=4, height=0.01 )

    call DclDrawContour( p )

    call DclCloseGraphics

end program
