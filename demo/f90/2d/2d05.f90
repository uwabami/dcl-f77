
program sample_2d05

  use dcl
  integer,parameter :: nx=19, ny=19
  real,parameter :: xmin=0, xmax=360,  dx1=20, dx2=60
  real,parameter :: ymin=-90, ymax=90, dy1=10, dy2=30
  real,parameter :: pi=3.141592, drad=pi/180, dz=0.05
  real,dimension(nx,ny) :: p,u,v

    call DclGetParm( 'GLOBAL:rmiss', rmiss )
    call DclSetParm( 'GLOBAL:lmiss', .true. )

    do j = 1, ny
      do i = 1, nx
        alon = ( xmin + (xmax-xmin) * (i-1) / (nx-1) ) * drad
        alat = ( ymin + (ymax-ymin) * (j-1) / (ny-1) ) * drad
        slat = sin(alat)
        p(i,j) = cos(alon) * (1-slat**2) * sin(2*pi*slat) + dz
      end do
    end do

    do j = 1, ny
      do i = 1, nx
        if (j==1 .or. j==ny) then
          u(i,j)=rmiss
          v(i,j)=rmiss
        else
          u(i,j) = p(i,j-1) - p(i,j+1)
          v(i,j) = p(modulo(i,nx-1)+1,j) - p(modulo(i-2,nx-1)+1,j)
        end if
      end do
    end do

    call DclOpenGraphics()
    call DclNewFrame

    call DclSetWindow( xmin, xmax, ymin, ymax )
    call DclSetViewPort( 0.2, 0.8, 0.2, 0.8 )
    call DclSetTransNumber( DCL_UNI_UNI )
    call DclSetTransFunction

    call DclDrawAxis( 'bt', dx2, dx1 )
    call DclDrawTitle( 'b', 'LONGITUDE', 0.0 )

    call DclDrawAxis( 'lr', dy2, dy1 )
    call DclDrawTitle( 'l', 'LATITUDE', 0.0 )

    call DclSetParm( 'CONTOUR:rsizel', 0.015 )
    call DclSetParm( 'CONTOUR:rsizet', 0.015 )
    call DclDrawContour( p )

    call DclSetParm( 'VECTOR:rsizet', 0.015 )
    call DclSetParm( 'VECTOR:lnrmal', .false. )
    call DclSetParm( 'VECTOR:xfact1', 0.025 )
    call DclSetParm( 'VECTOR:yfact1', 0.050 )
    call DclSetParm( 'VECTOR:lunit', .true. )
    call DclSetUnitVectorTitle( 'x', 'u' )
    call DclSetUnitVectorTitle( 'y', 'v' )
    call DclSetParm( 'VECTOR:vxunit', 0.05 )
    call DclSetParm( 'VECTOR:vyunit', 0.10 )
    call DclDrawVectors( u, v )

    call DclCloseGraphics

end program
