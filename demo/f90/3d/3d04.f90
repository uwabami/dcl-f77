!-----------------------------------------------------------------------
!     Copyright (c) 2002 GFD Dennou Club. All Rights Reserved.
!-----------------------------------------------------------------------
program sample_3d04

  use dcl
  real,parameter :: xmin= -50,  xmax= 50,  ymin= -50,  ymax= 50
  real,parameter :: vxmin=0., vxmax=0.8, vymin=0., vymax=0.8
  real,parameter :: zmin= -50,  zmax= 50
  real,parameter :: vzmin=0., vzmax=0.8
  real,parameter :: xvp3=2.5, yvp3=-1., zvp3=1.5 
  real,parameter :: xfc3=(vxmax-vxmin)/2, yfc3=(vymax-vymin)/2 
  real,parameter :: zfc3=(vzmax-vzmin)/2

    call DclOpenGraphics()
    call DclSetParm( 'GRAPH:ifont'   , 2 )

    call DclNewFrame

!-- x-y plane ----
    call DclSetWindow(  xmin,  xmax,  ymin,  ymax )
    call DclSetViewPort( vxmin, vxmax, vymin, vymax )
    call DclSetTransFunction

    call DclSet2DPlane( 1, 2, vzmax)
    call DclSet3DEyePoint( xvp3, yvp3, zvp3 )
    call DclSet3DObjectPoint( xfc3, yfc3, zfc3 )
    call DclSet3DProjection

    call aplot( 1 )

!-- x-z plane ----
    call DclSetWindow(  xmin,  xmax,  zmin,  zmax )
    call DclSetViewPort( vxmin, vxmax, vzmin, vzmax )
    call DclSetTransNumber( 1 )
    call DclSetTransFunction

    call DclSet2DPlane( 1, 3, vymin)
    call DclSet3DProjection

    call aplot( 2 )

!-- y-z plane ----
    call DclSetWindow(  ymin,  ymax,  zmin,  zmax )
    call DclSetViewPort( vymin, vymax, vzmin, vzmax )
    call DclSetTransNumber( 1 )
    call DclSetTransFunction

    call DclSet2DPlane( 2, 3, vxmax)
    call DclSet3DProjection

    call aplot( 3 )

    call DclCloseGraphics

end program
!-----------------------------------------------------------------------
subroutine aplot( ijk )

  use dcl
  integer,parameter :: nmax=40
  real :: x(0:nmax), y(0:nmax)
  character cttl*2

    call DclDrawViewPortFrame( 1 )

    dt = 2.*3.14159 / nmax
    do n=0,nmax
      x(n) = 40.*sin(n*dt)
      y(n) = 40.*cos(n*dt)
    end do

    call DclDrawLine( x, y )

    dt = 2.*3.14159 / 3
    do n=0,3
      x(n) = 40.*sin(n*dt)
      y(n) = 40.*cos(n*dt)
    end do

    call DclDrawLine( x(1:4), y(1:4) )

    call DclSetTextHeight( 0.07 )
    call DclDrawText( 0., 0., 'dennou' )
    write(cttl(1:2), '(i2.2)') ijk
    call DclDrawText( 0., -30., cttl )

    return
end subroutine
