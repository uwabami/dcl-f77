!-----------------------------------------------------------------------
!     Copyright (c) 2002 GFD Dennou Club. All Rights Reserved.
!-----------------------------------------------------------------------
program sample_3d05

  use dcl
  integer,parameter :: nx=21, ny=21
  real,parameter ::  xmin=-10,  xmax= 10,  ymin=-10,  ymax= 10
  real,parameter :: vxmin=0.2, vxmax=0.8, vymin=0.1, vymax=0.5
  real,parameter ::  zmin=0.0,  zmax= 20
  real,parameter :: vzmin=0.0, vzmax=0.6
  real,parameter :: xvp3=-0.7, yvp3=-0.7, zvp3=1.2
  real,parameter :: xfc3=(vxmax-vxmin)/2, yfc3=(vymax-vymin)/2
  real,parameter :: zfc3=(vzmax-vzmin)/2
  real,parameter :: dx1=1, dx2=5, dy1=1, dy2=4, dz1=1, dz2=5, pmin=0, pmax=1
  integer,parameter :: kmax=5
  real :: u(nx,ny), v(nx,ny), p(nx,ny)

    do j=1,ny
      do i=1,nx
        x = xmin + (xmax-xmin)*(i-1)/(nx-1)
        y = ymin + (ymax-ymin)*(j-1)/(ny-1)
        u(i,j) =  x
        v(i,j) = -y
        p(i,j) = exp( -x**2/64 -y**2/25 )
      end do
    end do

    call DclOpenGraphics()
    call DclNewFrame

!-- x-y 平面: 下レベル ----
    call DclSetWindow(  xmin,  xmax,  ymin,  ymax )
    call DclSetViewPort( vxmin, vxmax, vymin, vymax )
    call DclSetTransNumber( 1 )
    call DclSetTransFunction

    call DclSet2DPlane( 1, 2, vzmin )
    call DclSet3DEyePoint( xvp3, yvp3, zvp3 )
    call DclSet3DObjectPoint( xfc3, yfc3, zfc3 )
    call DclSet3DProjection

    call DclDrawAxis( 'bt', dx1, dx2 )
    call DclDrawTitle( 'b', 'x-axis', 0. )

    call DclDrawAxis( 'lr', dy1, dy2 )
    call DclDrawTitle( 'l', 'y-axis', 0. )

    call DclSetParm( 'VECTOR:rsizet', 0.014 )
    call DclDrawVectors( u, v )

!-- x-y 平面: 上レベル ----
    vzlev = vzmin + (vzmax-vzmin)*.6
    call DclSet2DPlane( 1, 2, vzlev )
    call DclSet3DProjection

    dp = (pmax-pmin)/kmax
    do k=1,kmax
      tlev1 = (k-1)*dp
      tlev2 = tlev1 + dp
      ipat  = 600 + k - 1
      call DclSetShadeLevel( tlev1, tlev2, ipat )
    end do
    call DclShadeContour( p )

    call DclSetParm( 'CONTOUR:lmsg' , .false. )
    call DclSetContourLevel( p, 0.1 )
    call DclDrawContour( p )
    call DclDrawViewPortFrame( 1 )

!-- x-z 平面 ----
    call DclSetWindow(  xmin,  xmax,  zmin,  zmax )
    call DclSetViewPort( vxmin, vxmax, vzmin, vzmax )
    call DclSetTransNumber( 1 )
    call DclSetTransFunction

    call DclSet2DPlane( 1, 3, vymax )
    call DclSet3DProjection

    call uzinit
    call DclDrawAxis( 'l', dz1, dz2 )
    call DclDrawTitle( 'l', 'z-axis', 0. )

    call DclCloseGraphics

end program
