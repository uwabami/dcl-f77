
program map03

  use dcl
  integer,parameter :: nx=18, ny=18
  real,parameter :: xmin=  0, xmax=360, ymin=-90, ymax=+90
  real,parameter :: pi=3.141592, drad=pi/180, dz=0.05, dp=0.2
  real,dimension(0:nx,0:ny) :: p

    do j = 0, ny
      do i = 0, nx
        alon = ( xmin + (xmax-xmin) * i/nx ) * drad
        alat = ( ymin + (ymax-ymin) * j/ny ) * drad
        slat = sin(alat)
        p(i,j) = cos(alon) * (1-slat**2) * sin(2*pi*slat) + dz
      end do
    end do

    call DclOpenGraphics()

    rmiss = DclGetReal( 'GLOBAL:RMISS' )
    call DclSetParm( 'GRAPH:lsoftf', .false. )

    call DclNewFrame

    call DclSetWindow( xmin, xmax, ymin, ymax )
    call DclSetViewPort( 0.1, 0.9, 0.1, 0.9 )
    call DclSetSimilarity( 0.4, 0.0, 0.0 )
    call DclSetMapProjectionAngle( 165.0, 60.0, 0.0 )
    call DclSetMapProjectionWindow( -180.0, 180.0, 0.0, 90.0 )
    call DclSetTransNumber( DCL_OTHOGAPHIC )
    call DclSetTransFunction
    call DclSetParm( 'GRAPH:lclip', .true. )

    call DclSetShadeLevel( rmiss,  -dp, 201 )
    call DclSetShadeLevel(    dp, dp*2, 401 )
    call DclSetShadeLevel( dp*2, rmiss, 402 )
    call DclShadeContour( p )

    call DclSetContourLevel( p, dp )
    call DclDrawContour( p )

    call DclDrawMap( 'coast_world' )
    call DclDrawGlobe()

    call DclCloseGraphics

end program
