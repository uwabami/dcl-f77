!-----------------------------------------------------------------------
!     Copyright (c) 2002 GFD Dennou Club. All Rights Reserved.
!-----------------------------------------------------------------------
program map02

  use dcl
  integer,parameter :: np=14
  integer,dimension(np) :: ntr = (/ &
    10,   11,   12,   13,   14,   15,   20, &
    21,   22,   23,   30,   31,   32,   33 /)

  character(len=32) :: cttl

    call DclOpenGraphics( -abs(DclSelectDevice()) )

    call DclSetAspectRatio( 2.0, 3.0 )
    call DclDivideFrame( 'y', 2, 3 )

    call DclSetParm( 'MAP_MAJOR_LINE_INDEX', 1 )
    call DclSetParm( 'MAP_MINOR_LINE_TYPE', 1 )

    do i=1,np
      call DclNewFrame
!       call DclSetMapProjectionAngle( 0.0, 90.0, 0.0 )
      call DclSetViewPort( 0.1, 0.9, 0.1, 0.9 )
      call DclSetTransNumber( ntr(i) )
      call DclFitMapParm
      call DclSetTransFunction

      call DclSetParm( 'ENABLE_CLIPPING', .true. )
      call DclDrawDeviceWindowFrame( 1 )
      call DclDrawViewPortFrame( 1 )
      call DclTransNumToLong( ntr(i), cttl )
      call DclDrawTextProjected( 0.5, 0.95, cttl, height=0.03, index=3 )

      call DclDrawMap( 'coast_world' )
      call DclDrawGlobe

      if ( ntr(i).eq.23 ) then
        call DclNewFrame
        call DclNewFrame
      end if

    end do

    call DclCloseGraphics

end program
