
program map01

  use dcl
  integer,parameter :: np=14
  integer,dimension(np) :: ntr = (/ &
    10,   11,   12,   13,   14,   15,   20, &
    21,   22,   23,   30,   31,   32,   33 /)
  real,dimension(np) :: fct = (/ &
    0.12, 0.12, 0.14, 0.14, 0.14, 0.14, 0.11, &
    0.16, 0.12, 0.12, 0.40, 0.12, 0.12, 0.17 /)
  character(len=32) :: cttl

    call DclOpenGraphics( -abs(DclSelectDevice()) )

    call DclSetAspectRatio( 2.0, 3.0 )
    call DclDivideFrame( 'y', 2, 3 )

    call DclSetParm( 'MAP:LGRIDMN', .false. )
    call DclSetParm( 'MAP:INDEXMJ', 1 )

    do i=1,np

      call DclNewFrame

      call DclSetParm( 'GRAPH:stlat1', 45.0 )
      call DclSetParm( 'GRAPH:stlat2', 30.0 )
      call DclSetParm( 'MAP:dgridmj', 30. )

      call DclSetSimilarity( fct(i), 0.0, 0.0 )
      call DclSetMapProjectionAngle( 0.0, 90.0, 0.0 )
      call DclSetViewPort( 0.1, 0.9, 0.1, 0.9 )
      if ( ntr(i)==30 ) then
        call DclSetMapProjectionWindow( -180.0, 180.0,   0.0, 90.0 )
      else
        call DclSetMapProjectionWindow( -180.0, 180.0, -90.0, 90.0 )
      end if
      call DclSetTransNumber( ntr(i) )
      call DclSetTransFunction

      call DclSetParm( 'GRAPH:LCLIP', .true. )
      call DclDrawDeviceWindowFrame( 1 )
      call DclDrawViewPortFrame( 1 )
      call DclTransNumToLong( ntr(i), cttl )
      call DclDrawTextProjected( 0.5, 0.95, cttl, height=0.03, index=3 )

      call DclDrawMap( 'coast_world' )
      call DclDrawGlobe()

      if ( ntr(i)==23 ) then
        call DclNewFrame
        call DclNewFrame
      end if

    end do

    call DclCloseGraphics

end program
