program etc

  use dcl

  call DclSetParm('WINDOW_WIDTH',300)
  call DclSetParm('WINDOW_HEIGHT',200)
  call DclOpenGraphics(-1)

  call DclDrawDeviceViewPortFrame(1)
  call DclSetParm( 'IFONT', 2 )
  call DclSetParm( 'ENABLE_PROPORTIONAL_FONT', .true. )

  call DclDrawTextProjected(0.3,0.5,'Font2')

  call DclCloseGraphics
end program
