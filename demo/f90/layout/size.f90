
program size

  use dcl

    call DclOpenGraphics()

    call DclSetParm( 'USE_FULL_WINDOW', .true. )
    call DclSetFrameMargin( 0., 0., 0.08, 0.08 )
    call DclSetAspectRatio( 1., 0.6 )

    call DclNewFrame
    call DclSetViewPort( 0.1, 0.9, 0.1, 0.5 )
    call DclDrawDeviceWindowFrame( 1 )
    call DclDrawViewportFrame( 2 )
    call DclDrawTextNormalized( 0.5, 0.3, 'Figure' )

    call DclCloseGraphics

end program
