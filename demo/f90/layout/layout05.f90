
program layout05

  use dcl

    call DclOpenGraphics()

    call DclSetFrameMargin( 0.1, 0.1, 0.1, 0.1 )
    call DclSetFrameTitle( 'figure title', 't',  0.,  0., 0.03, 1 )
    call DclSetFrameTitle( 'program.name', 'b', -1.,  1., 0.02, 2 )
    call DclSetFrameTitle( '#date #time',  'b',  0.,  0., 0.02, 3 )
    call DclSetFrameTitle( 'page:#page',   'b',  1., -1., 0.02, 4 )

    call DclNewFrame
    call DclSetViewPort( 0.1,0.9,0.1,0.9 )
    call DclDrawViewPortFrame( 1 )
    call DclDrawTextNormalized( 0.5, 0.5, 'figure' )

    call DclCloseGraphics

end program
