
program multi

  use dcl

  character(len=7) :: ctxt = 'frame??'

    call DclOpenGraphics()

    call DclSetFrameMargin( 0.1, 0.1, 0.05, 0.05 )
    call DclDivideFrame( 'y', 3, 2 )
    call DclSetFrameMargin( 0.05, 0.05, 0.05, 0.05 )

    do i=1,12
      call DclNewFrame
      call DclSetViewPort( 0.1,0.9,0.1,0.9 )
      call DclDrawViewPortFrame( 1 )
      write(ctxt(6:7),'(i2.2)') i
      call DclDrawTextNormalized( 0.5, 0.5, ctxt )
    end do

    call DclCloseGraphics

end program
