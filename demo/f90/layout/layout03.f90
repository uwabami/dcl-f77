
program layout03

  use dcl

  integer,parameter :: nmax=400
  real :: x(nmax), y(nmax)

!-- �ǡ��� 1 ----
    dt = 2.*3.14159 / (nmax-1)

    do n=1,nmax
      t = dt*(n-1)
      x(n) = 5.*sin(4.*t)
      y(n) = 5.*cos(5.*t)
    end do

!-- ����� 1 ----
    call DclOpenGraphics()
    call DclNewFrame

    call DclSetWindow(  -6.,   6.,  -6.,   6. )
    call DclSetViewPort( 0.15, 0.45, 0.65, 0.95 )
    call DclSetTransNumber( 1 )
    call DclSetTransFunction

    call DclSetTitle( 'x1', 'y1', '', '' )
    call DclDrawScaledAxis

    call DclDrawLine( x, y )

!-- �ǡ��� 2 ----
    iseed = 1
    x(1) = 2.*(rngu2(iseed)-0.5)
    do n=2,nmax
      x(n)   = 2.*(rngu2(iseed)-0.5)
      y(n-1) = x(n)
    end do
    y(nmax) = x(1)

!-- ����� 2 ----
    call DclNewFig

    call DclSetWindow(  -1.1,  1.1, -1.1, 1.1 )
    call DclSetViewPort(  0.15, 0.95,  0.1, 0.5 )
    call DclSetTransNumber( 1 )
    call DclSetTransFunction

    call DclSetTitle( 'x2-title', 'y2-title', '', '' )
    call DclDrawScaledAxis

    call DclDrawMarker( x, y )

    call DclCloseGraphics

end program

