
program marker

  use dcl
  integer,parameter :: nmax=100
  real :: x(nmax), y(nmax)

!-- �ǡ��� ----
    iseed = 1
    x(1) = 2.*(rngu2(iseed)-0.5)
    do n=2,nmax
      x(n)   = 2.*(rngu2(iseed)-0.5)
      y(n-1) = x(n)
    end do
    y(nmax) = x(1)

!-- ����� ----

    call DclOpenGraphics()
    call DclNewFrame

    call DclScalingPoint( x, y )
    call DclFitScalingParm
    call DclSetTransFunction

    call DclSetTitle( 'x-title', 'y-title', 'x-unit', 'y-unit' )
    call DclDrawScaledAxis

    call DclSetMarkerIndex( 5 )
    call DclSetMarkerSize( 0.015 )
    call DclDrawMarker( x(1:25), y(1:25) )
    call DclDrawMarker( x(26:50), y(26:50)  ,type=2 )
    call DclDrawMarker( x(51:75), y(51:75)  ,type=3 )
    call DclDrawMarker( x(75:100), y(76:100),type=4 )

    call DclCloseGraphics

end program
