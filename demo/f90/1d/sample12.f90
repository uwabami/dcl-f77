
program sample12

  use dcl
  integer,parameter :: n=100
  real(selected_real_kind(12)) :: a, r
  real, dimension(n)           :: x, y

!-- データ ----

    r = 0.2d0
    a = 3.6d0
    r0 = 0.
    do i=1, n
      r = a*r*(1.d0-r)
      r0 = r0 + r*4 - 2.58
      x2 = (i-50)**2
      rexp0 = 4.*i/n
      x(i) = 10**rexp0
      y(i) = 1.e5*exp(-x2) + 10.**r0
    end do
    y(20) = 1.e4
    y(40) = 2.e3
    y(65) = 3.e4
    y(70) = 5.e2

!-- グラフ ----

    call DclOpenGraphics()
    call DclNewFrame

    call DclSetTransNumber( DCL_LOG_LOG )
    call DclDrawScaledGraph( x, y )

    call DclCloseGraphics

end program
