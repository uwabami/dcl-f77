
program sample_1d05

  use dcl
  integer, parameter :: n=200, m=10
  real, dimension(n) :: x, y
  real, dimension(m) :: a

    a = (/( (-1)**j *2./((j*2-1)*DCL_PI), j=1, m) /)

    dt = 1./(n-1)
    x  = (/( dt*(i-1), i=1, n )/)

    do  i=1, n
      y(i) = 2*sum(a*cos((/(  2*DCL_PI*dt*(i-1)*(j*2-1), j=1, m )/)) )
    end do

!-----------------------------------------------------------------------

    call DclOpenGraphics
    call DclNewFrame

!         --- x axis ---
    call DclSetParm('AXIS:uyuser'  , 0.)
    call DclSetParm('SCALE:cxside'  , 'u')

!         --- y axis ---
    call DclSetParm('AXIS:irotlyl' , 1)
    call DclSetParm('AXIS:icentyl' , 0)
    call DclSetParm('SCALE:cyside'  , 'l')

!         --- etc. ---
    call DclSetParm('AXIS:inner' , -1)
    call DclSetParm('SCALE:cblkt' , '[]')

    call DclSetTitle('Time', 'Voltage', 'Sec', 'mV')
    call DclDrawScaledGraph(x, y)

    call DclCloseGraphics

end program
