
program label

  use dcl
  integer,parameter :: nmax = 40
  real,dimension(0:nmax) :: x,y1,y2,y3,y4,y5,y6

    x  = (/( n,n=0,nmax )/) / real(nmax) ! 0≦x≦1
    y1 = sin( 2.*DCL_PI * x ) + 1.5      ! y = sin(2πx)+C
    y2 = sin( 2.*DCL_PI * x ) + 1.0
    y3 = sin( 2.*DCL_PI * x ) + 0.5
    y4 = sin( 2.*DCL_PI * x ) - 0.5
    y5 = sin( 2.*DCL_PI * x ) - 1.0
    y6 = sin( 2.*DCL_PI * x ) - 1.5

!-- グラフ ----

    call DclOpenGraphics()
    call DclNewFrame

    call DclSetParm( 'ENABLE_LINE_LABELING', .true. ) ! ラベルつき折れ線

    call DclSetWindow( 0., 1., -3., 3. )        ! ウィンドウの設定
    call DclSetViewPort( 0.2, 0.8, 0.2, 0.8 )   ! ビューポートの設定
    call DclSetTransFunction                    ! 正規変換の確定

    call DclDrawScaledAxis                      ! 座標軸を描く

    call DclSetLineText( 'k=1' )                ! ラベルの文字列
    call DclDrawLine( x, y1 )

    call DclNextLineText  ! ラベルの最後の文字番号を増やす
    call DclDrawLine( x, y2 )

    call DclNextLineText
    call DclDrawLine( x, y3, type=2 )

    call DclSetLineTextSize( 0.01 )             ! ラベルの文字列の高さ
    call DclSetLineText( 'Small Label' )
    call DclDrawLine( x, y4, index=2 )
    call DclSetLineTextSize( 0.02 )             ! 高さ元に戻す

    call DclSetLineText( 'a' )
    call DclSetParm( 'LINE_CYCLE_LENGTH', 5. )    ! ラベルの間隔
    call DclDrawLine( x, y5, type=2 )

    call DclSetLineText( 'zzz' )
    call DclSetParm( 'LINE_START_POSITION', 0.9 )! ラベルの書き始め
    call DclDrawLine( x, y6, type=4 )

    call DclCloseGraphics


end program
