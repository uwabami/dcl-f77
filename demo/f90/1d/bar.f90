
program bar

  use dcl
  integer,parameter :: n=11, m=5
  real,dimension(n) :: x0 , x1, x2, y0, y1, y2, y3
  real,dimension(m) :: a

!-----------------------------------------------------------------------

    dt = 1./(n-1)
    pi = 3.14159

    a = (/( (-1)**j *2./((j*2-1)*pi), j=1,m ) /)

    do i=1, n
      t     = dt*(i-1)*2*pi
      x0(i) = dt*(i-1)
      y1(i) = a(1)*cos(t)
      y2(i) = 0.
      do j=1, m
        jj = j*2-1
        yy = a(j)*cos(jj*t)
        y2(i)  = y2(i) + yy
      end do
    end do
    y0 = ( y1 + y2 )/2
    y3 = -2.
!    x1 = x0 - abs( y1 - y2 )/5
!    x2 = x0 + abs( y1 - y2 )/5
    x1 = x0 - dt
    x2 = x0 + dt

!-----------------------------------------------------------------------

    call DclOpenGraphics()
    call DclNewFrame
    call DclSetParm( 'GRAPH:LCLIP', .true.)

    call DclScalingPoint( x0, y0 )
    call DclScalingPoint( x1, y1 )
    call DclScalingPoint( x2, y2 )
    call DclFitScalingParm
    call DclSetTransFunction

    call DclDrawScaledAxis

    call DclDrawYBarFrame( x0, y3, y0, width=0.04 )
    call DclDrawYErrorBar( x0, y1, y2 )

!    call DclDrawXErrorBar( x1, x2, y0, type=3, index=33)
!    call DclDrawMarker( x0, y0, type=4, index=21 )

    call DclCloseGraphics

end program
