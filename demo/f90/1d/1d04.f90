
program sample_1d04

  use dcl
  integer, parameter :: n=200
  real(kind=selected_real_kind(12)) :: x=0.d0, y=1.d0, z=1.d0
  real(kind=selected_real_kind(12)) :: dx, dy, dz, dt=0.01d0
  real(kind=selected_real_kind(12)) :: s=10.d0, r=26.d0, b=2.6d0
  real, dimension(n)                :: t, a

!-----------------------------------------------------------------------

    do i=1, n
      do  j=1, 8
        dx = -s*x + s*y
        dy = -x*z + r*x - y
        dz =  x*y - b*z
        x = x + dx*dt
        y = y + dy*dt
        z = z + dz*dt
      end do
      t(i) = (i-1)*1000
      a(i) = y + 20.
    end do

!-----------------------------------------------------------------------

    call DclOpenGraphics()
    call DclNewFrame

    call DclSetTitle ('Time', 'Heat Flux', 'Sec', 'W/m\^{2}')
    call DclDrawScaledGraph(t, a)

    call DclCloseGraphics

end program

