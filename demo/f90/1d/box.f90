
program box

  use dcl
  integer,parameter :: n=30
    double precision a, r
  real,dimension(n+1) :: x
  real,dimension(n)   :: y

!---------------------------- data definition --------------------------

    r = 0.2d0
    a = 3.7d0
    x = (/ (1./n*(i-1), i=1,n+1) /)
    do i=1, n
      r = a*r*(1.d0-r)
      y(i) = r
    end do
    y(n/2) = 999.
    call DclSetParm( 'GLOBAL:LMISS', .true.)

!----------------------------- graph -----------------------------------

    call DclOpenGraphics()
    call DclNewFrame

    call DclSetWindow( xmin=0., xmax=1. )
    call DclScalingPoint( y=y )
    call DclFitScalingParm
    call DclSetTransFunction

    call DclDrawScaledAxis

    call DclDrawYBoxLine( x, y )

    call DclNewFrame

    call DclSetWindow( ymin=0., ymax=1. )
    call DclScalingPoint( x=y )
    call DclFitScalingParm
    call DclSetTransFunction

    call DclDrawScaledAxis

    call DclDrawXBoxLine( y, x )

    call DclCloseGraphics

end program
