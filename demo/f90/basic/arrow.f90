!-----------------------------------------------------------------------
!     Copyright (c) 2002 GFD Dennou Club. All Rights Reserved.
!-----------------------------------------------------------------------
program arrow

  use dcl
  integer,parameter :: nd=12
  real,parameter :: ddeg=360.0/nd, dd=0.25
  real :: rx(nd), ry(nd)

    do n = 1, nd
      rx(n) = dd*n*cos(DCL_PI/180*ddeg*(n-1))
      ry(n) = dd*n*sin(DCL_PI/180*ddeg*(n-1))
    end do

    call DclOpenGraphics()
    call DclNewFrame

    call DclSetWindow( 0.0, 10.0, 0.0, 10.0 )
    call DclSetViewPort( 0.0, 1.0, 0.0, 1.0 )
    call DclSetTransFunction

    x1 = 3.0
    y1 = 7.0
    do n = 1, nd
      call DclDrawArrow( x1, y1, x1+rx(n), y1+ry(n) )
    end do

    call DclSetParm( 'FIX_ARROWHEAD_PROPORTION', .false. )
    call DclSetParm( 'ARROWHEAD_SIZE', 0.05 )
    x1 = 7.0
    y1 = 7.0
    do n = 1, nd
      call DclDrawArrow( x1, y1, x1+rx(n), y1+ry(n), index=2 )
    end do

    do i = 1, 8
      call DclSetParm( 'ARROWHEAD_ANGLE', 10.0*i )
      call DclDrawArrow( real(i), 2.0, real(i)+1, 3.0 )
    end do

    call DclCloseGraphics

end program
