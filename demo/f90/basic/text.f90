!-----------------------------------------------------------------------
!     Copyright (c) 2002 GFD Dennou Club. All Rights Reserved.
!-----------------------------------------------------------------------
program text

  use dcl

    call DclOpenGraphics
    call DclNewFrame

    do i=1,9
      call DclDrawLineNormalized( (/0.1,0.9/), (/0.1*i,0.1*i/) )
    end do
    call DclDrawLineNormalized( (/0.5,0.5/), (/0.05,0.95/), type=3 )

! 1段目
    call DclDrawTextNormalized(0.5,0.9,'Test')
! 2段目
    call DclDrawTextNormalized(0.5,0.8,'index3',index=3) ! index 設定
! 3段目
    call DclDrawTextNormalized(0.5,0.7,'SMALL',height=0.03) ! size 設定 (小)
! 4段目
    call DclDrawTextNormalized(0.5,0.6,'Left',centering=-1) ! 左揃え
! 5段目
    call DclDrawTextNormalized(0.5,0.5,'Rotate',angle=20.) ! 回転
! 6段目
    call DclDrawTextNormalized(0.5,0.4,'ABC|sup"XYZ_sub"') ! 上付・下付
! 7段目
    call DclSetParm('ENABLE_PROPORTIONAL_FONT',.true.)!プロポーショナルフォント
    call DclDrawTextNormalized(0.5,0.3,'Proportional',index=2)
! 8段目
    call DclSetParm('IFONT',2)
    call DclDrawTextNormalized(0.5,0.2,'Font2',index=2) ! 高品位フォント
! 9段目
    call DclDrawTextNormalized(0.5,0.1, &
&     achar(152)//achar(153)//achar(154)// & ! ギリシャ小文字
&     achar(145)//achar(151)//achar(148)// & ! ギリシャ大文字
&     achar(189)//achar(217)//achar(218) ) ! 記号

    call DclCloseGraphics

end program
