'-----------------------------------
	Option Explicit

	Dim strKeyroot
	Dim strCLKeyroot
	Dim strKeyname
	Dim KeySet
	Dim CLKeySet
	Dim MaxVersion
	Dim MaxCLVersion
	Dim strGFDCroot
	Dim strGFDCname

	Dim ENV
	Dim package
	Dim VFPATH
	Dim VFENV
	Dim FC 
	Dim CLPATH
	Dim CLENV 
	Dim CC 

	Dim WSHShell
	Dim objFSO      ' FileSystemObject
	Dim objFile     ' ファイル書き込み用

	Dim strDCLValue
	Dim strVFValue
	Dim strCLValue

	Dim Keyname
	Dim Ret


	FC = "ifort.exe /nologo /MD "
	VFENV = "ifortvars.bat"

	CC = "cl.exe /nologo /MD /DHAVE_GTK_H  /DHAVE_GDK_PIXBUF_H /DWINDOWS"
	CLENV = "vcvars32.bat"

'Set Registry Key Root

	Set KeySet = CreateObject("Scripting.Dictionary")
	If Is64() then
		strKeyroot = "SoftWare\Wow6432Node\Intel\Compilers\Fortran"
		strCLKeyroot = "Software\Wow6432Node\Microsoft\VisualStudio"
		strGFDCroot = "Software\Wow6432Node\GFD_Dennou_Club\Fortran"
	Else
		strKeyroot = "SoftWare\Intel\Compilers\Fortran"	
		strCLKeyroot = "Software\Microsoft\VisualStudio"
		strGFDCroot = "Software\GFD_Dennou_Club\Fortran"
	End If
	KeySet = GetRegValueArray(strKeyroot)
	CLKeySet = GetRegValueArray(strCLKeyroot)
	MaxVersion = GetMaxVersion(KeySet)
	MaxCLVersion = GetMaxVersion(CLKeySet)
	strKeyroot = strKeyroot & "\" & MaxVersion
	strCLKeyroot = strCLKeyroot & "\" & MaxCLVersion & "\Setup\VC"

' 以下のものを読み出してSETENV.BAT 	に書き出していくと同時に
' 環境変数に設定する？

	strDCLValue = Array("DLIB", "DWBIN", "GL:DSPATH")
	strVFValue = Array("FC", "VFENV", "VFPATH")
	strCLValue = Array("CC", "CLENV", "CLPATH")

	Set objFSO = WScript.CreateObject("Scripting.FileSystemObject")
    Set objFile = objFSO.OpenTextFile("config.bat", 2, True)

	For Each Keyname In strDCLValue
		Ret = GetRegValue(strGFDCroot,Keyname)
		If Ret = ""  Then
			Select Case KeyName
			Case "DLIB"
				Ret="C:\dcl-fortran\lib"
			Case "GL:DSPATH"
				Ret="C:\dcl-fortran\lib"
			Case "DWBIN"
				Ret="C:\dcl-fortran\bin"
			End Select
		End If
	    objFile.Write("set " & Keyname & "=" & Ret & vbCrLf)
	Next
'今のバージョンではそうなっていないが、ifort が実行できないときだけ
'設定するようにしたい。

	VFPATH = GetRegValue(strKeyroot,"ProductDir")
	CLPATH = GetRegValue(strCLKeyroot,"ProductDir")
	
	objFile.Write("set " & "FC" & "=" & FC & vbCrLf)
	objFile.Write("set " & "CC" & "=" & CC & vbCrLf)
	objFile.Write("set " & "PATH" & "=" & "%PATH%;%DWBIN%" & vbCrLf)
	objFile.Write("@IF ""%1""==""MACRO"" goto macro" & vbCrLf)
	objFile.Write("@goto comline"& vbCrLf)
	objFile.Write(":macro"& vbCrLf)
	objFile.Write("call """ & VFPATH & "\BIN\" & VFENV & _
                  """" & " intel64" & vbCrLf)
'                  """" & " ia32" & vbCrLf)
	objFile.Write("call """ & CLPATH & "\BIN\" & CLENV & _
                  """" & vbCrLf)
	objFile.Write("goto end"& vbCrLf)

	objFile.Write(":comline"& vbCrLf)
	objFile.Write("%comspec% /k """ & VFPATH & "\BIN\" & _
                  VFENV & """" & " intel64" & vbCrLf)
'                  VFENV & """" & " ia32" & vbCrLf)
	objFile.Write("call """ & CLPATH & "\BIN\" & CLENV & _
                  """" & vbCrLf)
	objFile.Write(":end" & vbCrLf)


'Get EnvironmentValue

''	strKeyname = "ProductDir"
''	Ret = GetRegValue(strKeyroot,strKeyname)

''	strGFDCname = "DLIB"
''	Ret = GetRegValue(strGFDCroot,strGFDCname)

'ファイルに出力 "set " & strGFDCname & "=" & Ret


    objFile.Close


' ファイルハンドルをクローズする。
	Set objFile = Nothing
	Set objFSO = Nothing



	Set objFSO = WScript.CreateObject("Scripting.FileSystemObject")
    Set objFile = objFSO.OpenTextFile("Mkinclude.win", 2, True)

	objFile.Write("#" & vbCrLf)
	objFile.Write("#       MkInclude for nmake" & vbCrLf)
	objFile.Write("#" & vbCrLf)
	objFile.Write("#       Copyright (C) 2004 GFD Dennou Club. All rights reserved." & vbCrLf)
	objFile.Write("#" & vbCrLf)
	objFile.Write("" & vbCrLf)
	objFile.Write("PREFIX = ""C:\dcl-fortran""" & vbCrLf)
	objFile.Write("" & vbCrLf)
	objFile.Write("FC    = " & FC & vbCrLf)
	objFile.Write("FCVER = " & MaxVersion & vbCrLf)
	objFile.Write("CC    = " & CC & vbCrLf)
	objFile.Write("CCVER = " & MaxCLVersion & vbCrLf)
	objFile.Write("FTOL  = " & "FTOL" & vbCrLf)
	objFile.Write("CP    = @copy /y" & vbCrLf)
	objFile.Write("MV    = @move /y" & vbCrLf)
	objFile.Write("RM    = @del" & vbCrLf)
	objFile.Write("" & vbCrLf)
	objFile.Write("INTMAX          = Z'7FFFFFFF'" & vbCrLf)
	objFile.Write("REALMAX         = Z'7F7FFFFF'" & vbCrLf)
	objFile.Write("REALMIN         = Z'00800000'" & vbCrLf)
	objFile.Write("REPSL           = 1.19221E-06" & vbCrLf)
	objFile.Write("" & vbCrLf)
	objFile.Write("IWIDTH         = 900" & vbCrLf)
	objFile.Write("IHEIGHT         = 650" & vbCrLf)
	objFile.Write("DCLNWS         = 2" & vbCrLf)
	objFile.Write("" & vbCrLf)
	objFile.Write("#DSPATH = $(PREFIX)""\dbase\""" & vbCrLf)
	objFile.Write("" & vbCrLf)
	objFile.Write("DSPATH = c:\dcl-fortran\dbase" & vbCrLf)
	objFile.Write("" & vbCrLf)
	objFile.Write("MAXNGRID = 4000" & vbCrLf)
	objFile.Write("" & vbCrLf)
	objFile.Write("SUFFIX = .obj .mod .f90 .f .c" & vbCrLf)
	objFile.Write("" & vbCrLf)
	objFile.Write("all: build" & vbCrLf)
	objFile.Write("" & vbCrLf)
	objFile.Write("subdirs:$(SUBDIRS_PRETARGET) $(SUBDIRS) $(SUBDIRS_POSTTARGET)" & vbCrLf)
	objFile.Write("" & vbCrLf)
	objFile.Write("!if ""$(SUBDIRS)"" != """"" & vbCrLf)
	objFile.Write("$(SUBDIRS):subdirs.mak.force-build" & vbCrLf)
	objFile.Write("	cd $@" & vbCrLf)
	objFile.Write("	@echo Entering $@" & vbCrLf)
	objFile.Write("	nmake /NOLOGO /S /$(MAKEFLAGS) /f makefile.win subdirs" & vbCrLf)
	objFile.Write("	cd .." & vbCrLf)
	objFile.Write("	@echo Leaving $@" & vbCrLf)
	objFile.Write("" & vbCrLf)
	objFile.Write("subdirs.mak.force-build:" & vbCrLf)
	objFile.Write("" & vbCrLf)
	objFile.Write("!endif" & vbCrLf)
	objFile.Write("" & vbCrLf)
	objFile.Write("install-exec: install-exec-unique" & vbCrLf)
	objFile.Write("	IF EXIST *.obj $(CP) *.obj $(ROOTDIR)" & vbCrLf)
	objFile.Write("	IF EXIST *.mod $(CP) *.mod $(ROOTDIR)" & vbCrLf)
	objFile.Write("" & vbCrLf)
	objFile.Write("clean-exec: clean-exec-unique" & vbCrLf)
	objFile.Write("	IF EXIST *.mod $(RM) *.mod" & vbCrLf)
	objFile.Write("	IF EXIST *.obj $(RM) *.obj" & vbCrLf)
	objFile.Write("	IF EXIST *.lib $(RM) *.lib" & vbCrLf)
	objFile.Write("	IF EXIST *.bak $(RM) *.bak" & vbCrLf)
	objFile.Write("" & vbCrLf)
	objFile.Write("" & vbCrLf)
	objFile.Write(".f90.obj:" & vbCrLf)
	objFile.Write("	@echo $*.f90" & vbCrLf)
	objFile.Write("	$(FC) /c $*.f90" & vbCrLf)
	objFile.Write("" & vbCrLf)
	objFile.Write(".f90.mod:" & vbCrLf)
	objFile.Write("	@echo $*.f90" & vbCrLf)
	objFile.Write("	$(FC) /c $*.f90" & vbCrLf)
	objFile.Write("" & vbCrLf)
	objFile.Write(".f.obj:" & vbCrLf)
	objFile.Write("	@echo $*.f" & vbCrLf)
	objFile.Write("	$(FC) /c $*.f" & vbCrLf)
	objFile.Write("" & vbCrLf)
	objFile.Write(".c.obj:" & vbCrLf)
	objFile.Write("	@echo $*.c" & vbCrLf)
	objFile.Write("	$(CC) /c $*.c $(CFLAGS)"  & vbCrLf)







    objFile.Close

	Set objFile = Nothing
	Set objFSO = Nothing




'この先に関数を並べていく

Function GetRegValue(strKeyroot,strKeyname)
	Dim WshShell

	Set WshShell = WScript.CreateObject("WScript.Shell")
	On Error Resume Next
		GetRegValue = WshShell.RegRead("HKLM" & "\" & strKeyroot & "\" & strKeyname)
	If Err.Number <>0 Then
	Err.Clear
		GetRegValue=""
	End if
	On Error Goto 0
	Set WshShell = Nothing
End Function

Function GetRegValueArray(strKeyname)
	Dim Wcls
	Dim Locator
	Dim Service
	Dim Ret
	Dim KeySet
	Dim Key

	Dim MaxVersion
	Dim Version

	Const HKEY_CURRENT_USER = &H80000001
	Const HKEY_LOCAL_MACHINE = &H80000002

	Set Locator = WScript.CreateObject("WbemScripting.SWbemLocator")
	Set Service = Locator.ConnectServer(vbNullString, "root\default")
	Set Wcls = Service.Get("StdRegProv")
	Wcls.EnumKey HKEY_LOCAL_MACHINE, strKeyname ,KeySet

	GetRegValueArray=KeySet

	Set Wcls = Nothing
	Set Service = Nothing
	Set Locator = Nothing
End Function

Function Is64()
	Dim softwares
	Dim Key
	Dim SystemBits

	SystemBits = False
	Set softwares = CreateObject("Scripting.Dictionary")
	softwares = GetRegValueArray("Software")
	For Each Key In softwares
		If Key = "Wow6432Node"	Then 
			SystemBits = True
		End If
	Next
	Is64 = SystemBits

End Function

Function GetMaxVersion(KeySet)
	Dim MaxVersion
	Dim AsciiMaxVersion
	Dim Key
	Dim Ret
	Dim Version

	MaxVersion = 0
	For Each Key In KeySet
	
   		Ret = Ret & Key & vbCrLf
		If IsNumeric(Key) Then
			Version = CSng(Key)
		Else
			Version = 0
		End If

		If Version > MaxVersion Then
		    MaxVersion = Version
			AsciiMaxVersion = Key
		End If
	Next

	GetMaxVersion = AsciiMaxVersion

end Function
