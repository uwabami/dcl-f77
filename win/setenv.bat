@SET DCLFRTDIR=%~dp0

@SET VSVER=vs2013
@SET VCVER="Microsoft Visual Studio 12.0"

:CL(Visual C)
@IF EXIST "c:\Program Files (x86)\"%VCVER%"\VC\bin\vcvars32.bat" call   "c:\Program Files (x86)\"%VCVER%"\VC\bin\vcvars32.bat"

:IFRT(Intel Fortran)
call "C:\Program Files (x86)\Intel\Composer XE\bin\ifortvars.bat" ia32 %VSVER%

:DCLFRT
@SET DFLIB=%DCLFRTDIR%lib
@SET DFINCLUDE=%DCLFRTDIR%include
@SET DFWBIN=%DCLFRTDIR%bin
@SET DFSRC=%DCLFRTDIR%src
@PATH=%DFWBIN%;%PATH%
@SET LIB=%DFLIB%;%LIB%
@SET INCLUDE=%DFINCLUDE%;%INCLUDE%
@SET GL_DSPATH=%DCLFRTDIR%lib\dcldbase\

:GTK
@SET PKG_CONFIG_PATH=%DCLFRTDIR%GTK\lib\pkgconfig
@SET DllPath=%DCLFRTDIR%GTK\bin
@PATH=%DllPath%;%PATH%
@SET GTKDIR=C:\dcl-fortran\GTK\lib
@SET GTKLIBS=pango-1.0.lib pangoft2-1.0.lib pangowin32-1.0.lib atk-1.0.lib gdk_pixbuf-2.0.lib gdk-win32-2.0.lib gtk-win32-2.0.lib glib-2.0.lib gmodule-2.0.lib gobject-2.0.lib gthread-2.0.lib cairo.lib

@FOR /f "DELIMS=" %%A IN ('pkg-config --cflags gtk+-2.0') DO @SET CFLTMP=%%A
@FOR /f "DELIMS=" %%A IN ('pkg-config --libs gtk+-2.0') DO @SET GTKTMP=%%A
@SET CFLAGS=%CFLTMP:-mms-bitfields=%
@SET CFLAGS=%CFLAGS% -Dinline= /link /libpath:%GTKDIR% %GTKLIBS% /stack:10000000 /NODEFAULTLIB:libcd.lib /NODEFAULTLIB:libcmt.lib /NODEFAULTLIB:oldnames.lib  
@SET FFLAGS=/module:%DFLIB% %CFLAGS% %DFLIB%\dcl.lib %DFLIB%\dcl-f90.lib
SET INCLUDE=%INCLUDE%;%DCLFRTDIR%GTK\include

:HOME
@IF NOT "%HOME%" == "" GOTO :EOF
@SET HOME=%HOMEDRIVE%%HOMEPATH%
@%HOMEDRIVE%
@CD %HOME%

:EOF
