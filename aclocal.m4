dnl $Id$
dnl DCL macros for dcl-5.3.2 configure
dnl modified 2001/02/27 shiotani
dnl modified 2005/09/21 Kagimoto

dnl Check for C compiler.
dnl
AC_DEFUN(DCL_PROG_CC,
[
    case "${CC-}" in
        '')
            case `uname` in
                ULTRIX)
                    ccs='gcc cc'
                    ;;
                *)
                    ccs='xlc acc cc gcc'
                    ;;
            esac
            for cc in $ccs; do
                AC_CHECK_PROG(CC, $cc, $cc)
                case "$CC" in
                    '') ;;
                    *)  break;;
                esac
            done
            case "${CC-}" in
                '')
                    AC_MSG_ERROR("Could not find C compiler")
                    ;;
            esac
            ;;
        *)
            AC_CHECKING(user-defined C compiler \"$CC\")
            ;;
    esac
    AC_MSG_CHECKING(C compiler)
    cat << EOF > conftest.c
int main () {
  ;
  return 0;
}
EOF
    doit='$CC -c ${CFLAGS-} conftest.c'
    if AC_TRY_EVAL(doit); then
        AC_MSG_RESULT(works)
    else
        AC_MSG_ERROR($CC failed to compile test program)
        CC=
    fi
    rm -f conftest.*
    AC_SUBST(CC)
    case "$CC" in
        *gcc*)
            GCC=yes             # Expected by autoconf(1) macros
            ;;
    esac
    case `uname -s` in
        HP-UX*)
            CFLAGS="${CFLAGS-} -D_INCLUDE_POSIX_SOURCE"
            ;;
        SunOS)
            case `uname -r` in
                4*)
                    CFLAGS="${CFLAGS-} -Dsun4"
                    ;;
                5*)
                    ;;
            esac
            ;;
    esac
])

dnl Check for FORTRAN compiler.
dnl
AC_DEFUN(DCL_PROG_FC,
[
    SUNF=no
    case `uname -s` in
        AIX)
            forts="xlf90 f90 xlf f77"
            ;;
        BSD/OS)
            forts="f77 g77"
            fflags=-w
            ;;
        HI-OSF*)
            forts="f90 f77"
            fflags="-i,E,L,EU"
            ;;
        HP-UX)
            forts=fort77
            fflags=-w
            flibs=-lU77
            ;;
        IRIX)
            forts="f90 f77"
            ;;
        IRIX64)
            forts="f90 f77 g77"
            fflags=-w
            ;;
        Linux)
            forts="f95 f90 ifort ifc g95 pgf77 f77 gfortran g77"
            fflags=-w
            ;;
        OSF1)
            forts="f90 f77"
            ;;
        SunOS)
            case `uname -r` in
                4*)
                    forts="f77 frt g77"
                    fflags=-w
                    ;;
                5*)
                    forts="f90 f77 frt g77"
                    SUNF=yes
                    ;;
            esac
            ;;
        sn*|UNICOS)
            forts="cf90 f90 cf77 f77 g77"
            ;;
        *)
            forts="xlf90 f95 f90 xlf g95 ghf77 f77 cf77 gfortran g77 fort77"
            ;;
    esac
    FFLAGS="${FFLAGS-} ${fflags-}"
    FLIBS="${FLIBS-} ${flibs-}"
    case "${FC+set}" in
        set)
            case "$FC" in
                '')
                    AC_MSG_ERROR(no FORTRAN compiler)
                    ;;
                *)
                    AC_MSG_CHECKING(user-defined FORTRAN compiler \"$FC\")
                    cat << EOF > conftest.f
      CALL FOO
      END
EOF
                    doit='$FC -c ${FFLAGS-} conftest.f'
                    if AC_TRY_EVAL(doit); then
                        AC_MSG_RESULT(works)
                    else
                        AC_MSG_ERROR($FC failed to compile test program)
                        FC=
                    fi
                    rm -f conftest.*
                    ;;
            esac
            ;;
        *)
            for fc in $forts; do
                AC_CHECK_PROG(FC, $fc, $fc)
                case "${FC-}" in
                    '')
                        ;;
                    *)
                        cat << EOF > conftest.f
      CALL FOO
      END
EOF
                    doit='$FC -c ${FFLAGS-} conftest.f'
                    if AC_TRY_EVAL(doit); then
                        break
                    else
                        AC_MSG_ERROR($FC failed to compile test program)
                        unset FC
                        unset ac_cv_prog_FC
                    fi
                    ;;
                esac
            done
            rm -f conftest.*
            case "${FC-}" in
                '')
                    AC_MSG_ERROR(Could not find working FORTRAN compiler)
                    FC=
                    ;;
            esac
            ;;
    esac

dnl If Sun Fortran 90 is selected, -R<f90_dir> should be specified
dnl in the compilation process
dnl
    if test ${SUNF} = yes && test "${FC}" = "f90"; then
        AC_PATH_PROG(SUNFC,"f90")
        flibdir=`dirname ${SUNFC}`
        flibdir=`echo $flibdir | sed -e s/bin/lib/`
        FLIBS="${FLIBS-} -R"$flibdir
    fi

dnl If Intel Fortran Compiler is selected on Linux PC, -Vaxlib linker option
dnl is preferably specified in FLIBS parameter
dnl
    if test "x${FC}" = "xifc"; then
      FCLIB="${FLIBS-} -Vaxlib"
    fi

dnl If gFortran Compiler is selected on Linux PC, -lgfortran linker option
dnl is preferably specified in FLIBS parameter
dnl
    if test "x${FC}" = "xf95"; then
      FCLIBOPT=" -lgfortran"
    fi
    if test "x${FC}" = "xgfortran"; then
      FCLIBOPT=" -lgfortran"
    fi



    AC_SUBST(FC)
    AC_SUBST(FFLAGS)
    AC_SUBST(FLIBS)
    AC_SUBST(FCLIBOPT)
])

dnl dummy function to check FORTRAN compiler (for C-version)
dnl
AC_DEFUN(DCL_PROG_FC_for_C,
[
    FC=
    FFLAGS=
    FLIBS=
    AC_SUBST(FC)
    AC_SUBST(FFLAGS)
    AC_SUBST(FLIBS)
])

dnl Check for awk
dnl
AC_DEFUN(DCL_PROG_AWK,
[
    awk="mawk gawk jgawk nawk awk"
    for prog in $awk
    do
        AC_PATH_PROG(AWK, $prog)
        case "${AWK-}" in
            '')
                ;;
            *)
                break;;
        esac
    done
    case "${AWK-}" in
        '')
            AC_MSG_WARN(Could not find awk program)
            AWK=
            ;;
    esac
    AC_SUBST(AWK)
])

dnl Check for sed
dnl
AC_DEFUN(DCL_PROG_SED,
[
    sed="gsed sed"
    for prog in $sed
    do
        AC_CHECK_PROG(SED, $prog, $prog)
        case "${SED-}" in
            '')
                ;;
            *)
                break;;
        esac
    done
    case "${SED-}" in
        '')
            AC_MSG_WARN(Could not find sed program)
            SED=
            ;;
    esac
    AC_SUBST(SED)
])

dnl Check for jLaTeX program
dnl
AC_DEFUN(DCL_PROG_JLATEX,
[
    jlatex="platex jlatex"
    for jl in $jlatex
    do
        AC_CHECK_PROG(JLATEX, $jl, $jl)
        case "${JLATEX-}" in
            '')
                ;;
            *)
                break
                ;;
        esac
    done
    case "${JLATEX-}" in
        '')
            AC_MSG_WARN(Could not find LaTeX program)
            JLATEX=
            ;;
    esac
    AC_SUBST(JLATEX)
])

dnl Check for DVI->PDF filter
dnl
AC_DEFUN(DCL_PROG_DVIPDF,
[
    dvipdf="dvipdfmx"
    for dvi in $dvipdf
    do
        AC_CHECK_PROG(DVI2PDF, $dvi, $dvi)
        case "${DVI2PDF-}" in
            '')
                ;;
            dvipdfmx)
                DVI2PDF="dvipdfmx "
                break
                ;;
            *)
                break
                ;;
        esac
    done
    case "${DVI2PDF-}" in
        '')
            AC_MSG_WARN(Could not find DVI->PDF program)
            DVI2PDF=
            ;;
    esac
    AC_SUBST(DVI2PDF)
])

dnl Check options for math1/oslib/{osgarg.f,osqarn.f}
dnl
AC_DEFUN(DCL_CHECK_OSLIB_OPTIONS,
[
    cat << EOF > conftest.f
      PROGRAM MAIN
      CHARACTER ARGV*10
*      EXTERNAL GETARG
      CALL GETARG(2, ARGV)
      STOP
      END
EOF
	cat << EOF > conftest1.f90
      CHARACTER CHAR*10
      call get_command_argument(1,char)
      END
EOF
    doit='${FC-} ${FFLAGS-} conftest.f ${FLIBS} > /dev/null 2>&1'
    doit2='${FC-} ${FFLAGS-} conftest1.f90 ${FLIBS} > /dev/null 2>&1'
    AC_MSG_CHECKING(Fortran service routine get_command_argument())
    if AC_TRY_EVAL(doit2); then
        AC_MSG_RESULT(works)
        OSGARG_OPT=f2003
        OSGARG_EXT=f90
    else
        if AC_TRY_EVAL(doit); then
            AC_MSG_WARN(get_command_argument() is not available)
            AC_MSG_CHECKING(Fortran service routine getarg())
            AC_MSG_RESULT(works)
            OSGARG_OPT=getarg
            OSGARG_EXT=f
        else
            AC_MSG_WARN(get_command_argument() is not available)
            AC_MSG_CHECKING(Fortran service routine getarg())
            AC_MSG_WARN(getarg() is not available)
            OSGARG_OPT=dummy
            OSGARG_EXT=f90
        fi
    fi
    rm -f conftest.* a.out

    cat << EOF > conftest.f
      PROGRAM MAIN
*      EXTERNAL IARGC
      N = IARGC()
      STOP
      END
EOF
	cat << EOF > conftest1.f90
      N = command_argument_count()
	  END
EOF
    doit='${FC-} ${FFLAGS-} conftest.f ${FLIBS} > /dev/null 2>&1'
    doit2='${FC-} ${FFLAGS-} conftest1.f90 ${FLIBS} > /dev/null 2>&1'
    AC_MSG_CHECKING(Fortran service routine command_argument_count())
    if AC_TRY_EVAL(doit2); then
       AC_MSG_RESULT(works)
        OSQARN_OPT=f2003
        OSQARN_EXT=f90
    else if AC_TRY_EVAL(doit); then
        AC_MSG_WARN(command_argument_count() is not available)
        AC_MSG_CHECKING(Fortran service routine iargc())
        AC_MSG_RESULT(works)
        OSQARN_OPT=iargc
        OSQARN_EXT=f
    else
        AC_MSG_WARN(command_argument_count() is not available)
        AC_MSG_CHECKING(Fortran service routine iargc())
        AC_MSG_WARN(Service routine iargc() is not available)
        OSQARN_OPT=dummy
        OSQARN_EXT=f
    fi
    fi
    rm -f conftest.* a.out

    AC_SUBST(OSGARG_OPT)
    AC_SUBST(OSGARG_EXT)
    AC_SUBST(OSQARN_OPT)
    AC_SUBST(OSQARN_EXT)
])

dnl Check underscore for C functions' name
dnl
AC_DEFUN(DCL_CHECK_UNDERSCORE,
[
    cat << EOF > conftest.f
      PROGRAM MAIN
      CALL CFUNC
      STOP
      END
EOF
    cat << EOF > conftest1.c
int cfunc()
{
    return 0;
}
EOF
    cat << EOF > conftest2.c
int cfunc_()
{
    return 0;
}
EOF
    ${CC-} -c conftest1.c -o conftest1.o
    ${CC-} -c conftest2.c -o conftest2.o
    doit1='${FC-} ${FFLAGS-} conftest.f conftest1.o'
    doit2='${FC-} ${FFLAGS-} conftest.f conftest2.o'
    if AC_TRY_EVAL(doit1); then
        AC_MSG_RESULT(underscore is unnecessary)
        USCORE="-DUSCORE"
    elif AC_TRY_EVAL(doit2); then
        AC_MSG_RESULT(underscore is necessary)
        USCORE="-DUSCORE=_"
    fi
    rm -f conftest?.? conftest.* a.out
    AC_SUBST(USCORE)
])

dnl Check CSGI package
dnl
AC_DEFUN(DCL_CHECK_CSGI,
[
    cat << EOF > conftest.f
      print*,ichar('a')
      end
EOF
    AC_MSG_CHECKING(Character code set csgi()/isgc())
    ${FC-} -o conftest conftest.f > /dev/null 2>&1
    ia=`./conftest`
    if test $ia -gt 0; then
        CSGI_OPT=general
        AC_MSG_RESULT(general)
    else
        CSGI_OPT=other
        AC_MSG_RESULT(other)
    fi
    rm -f conftest conftest.?
    AC_SUBST(CSGI_OPT)
])

dnl Check CSGI package for C
dnl
AC_DEFUN(DCL_CHECK_CSGI_C,
[
    cat << EOF > conftest.c
#include <stdio.h>
#include <stdlib.h>

int main () {
    int rval;

    rval = *(unsigned char *)"a";
    printf("%d\n", rval);
    return(0);
}
EOF
    AC_MSG_CHECKING(Character code set csgi()/isgc())
    ${CC-} -o conftest conftest.c > /dev/null 2>&1
    ia=`./conftest`
    if test $ia -gt 0; then
        CSGI_OPT=general
        AC_MSG_RESULT(general)
    else
        CSGI_OPT=other
        AC_MSG_RESULT(other)
    fi
    rm -f conftest conftest.?
    AC_SUBST(CSGI_OPT)
])

dnl Set DCLDIR (current working directory)
dnl
AC_DEFUN(DCL_SET_DCLDIR,
[
    AC_PATH_PROG(PWD, pwd)
    case "${PWD-}" in
        '')
            AC_MSG_WARN(*** WARN *** Please set environment variable DCLDIR by yourself)
            DCLDIR=
            ;;
        *)
            DCLDIR=${PWD-}
    esac
    AC_SUBST(DCLDIR)
])

dnl Check for the name format of a Fortran-callable C routine.
dnl
dnl DCL_CHECK_FCALLSCSUB
AC_DEFUN([DCL_CHECK_FCALLSCSUB],
[
    AC_REQUIRE([DCL_PROG_FC])
    case "$FC" in
        '') ;;
        *)  AC_BEFORE([DCL_CHECK_CTYPE_FORTRAN])
            AC_MSG_CHECKING(for C-equivalent to Fortran routine \"SUB\")
            cat >conftest.f <<\EOF
              call sub()
              end
EOF
            doit='$FC -c ${FFLAGS} conftest.f'
            if AC_TRY_EVAL(doit); then
                FCALLSCSUB=`nm conftest.o | awk '
                    /SUB_/{print "SUB_";exit}
                    /SUB/ {print "SUB"; exit}
                    /sub_/{print "sub_";exit}
                    /sub/ {print "sub"; exit}'`
                case "$FCALLSCSUB" in
                    '') AC_MSG_ERROR(not found)
                        ;;
                    *)  AC_MSG_RESULT($FCALLSCSUB)
                        ;;
                esac
            else
                AC_MSG_ERROR(Could not compile conftest.f)
            fi
            rm -f conftest*
            ;;
    esac
])

dnl Check for a C type equivalent to a Fortran type.
dnl
dnl DCL_CHECK_CTYPE_FORTRAN(ftype, ctypes, fmacro_root)
dnl
AC_DEFUN(DCL_CHECK_CTYPE_FORTRAN,
[
    cat >conftestf.f <<EOF
           $1 values(4)
           data values /-1, -2, -3, -4/
           call sub(values)
           end
EOF
    for ctype in $2; do
        AC_MSG_CHECKING(if Fortran \"$1\" is C \"$ctype\")
        cat >conftest.c <<EOF
            #include <stdlib.h>
            void $FCALLSCSUB(values)
                $ctype values[[4]];
            {
                exit(values[[1]] != -2 || values[[2]] != -3);
            }
EOF
        doit='$CC -c ${CPPFLAGS} ${CFLAGS} conftest.c'
        if AC_TRY_EVAL(doit); then
            doit='$FC ${FFLAGS} -c conftestf.f'
            if AC_TRY_EVAL(doit); then
                doit='$FC -o conftest ${FFLAGS} ${FLDFLAGS} conftestf.o conftest.o ${LIBS}'
                if AC_TRY_EVAL(doit); then
                    doit=./conftest
                    if AC_TRY_EVAL(doit); then
                        AC_MSG_RESULT(yes)
                        AC_DEFINE_UNQUOTED(DCL_$3[], $ctype)
                        break
                    else
                        AC_MSG_RESULT(no)
                    fi
                else
			AC_MSG_ERROR(Could not link conftestf.o and conftest.o)
                fi
            else
                AC_MSG_ERROR(Could not compile conftestf.f)
            fi
        else
            AC_MSG_ERROR(Could not compile conftest.c)
        fi
    done
    rm -f conftest*
])

dnl Get information about Fortran data types.
dnl
AC_DEFUN([DCL_FORTRAN_TYPES],
[
    AC_REQUIRE([DCL_PROG_FC])
    case "$FC" in
    '')
        ;;
    *)
        AC_REQUIRE([DCL_CHECK_FCALLSCSUB])

        DCL_CHECK_CTYPE_FORTRAN(integer, int long, INT)
        DCL_CHECK_CTYPE_FORTRAN(real, float double, REAL)
        ;;
    esac
])

dnl
dnl check the form of hexadecimal number in FORTRAN
dnl
AC_DEFUN(DCL_CHECK_FORTRAN_HEX_DATA,
[
    cat << EOF > conftest1.f
      INTEGER I
      DATA I /Z'7FFFFFFF'/
      END
EOF
    cat << EOF > conftest2.f
      INTEGER I
      DATA I /Z7FFFFFFF/
      END
EOF
    doit1='$FC -o conftest1 ${FFLAGS-} conftest1.f'
    doit2='$FC -o conftest2 ${FFLAGS-} conftest2.f'

    if AC_TRY_EVAL(doit1); then
        AC_MSG_RESULT(hexadecimal number requires quotation)
        QFLAG=-DQUOTE
    elif AC_TRY_EVAL(doit2); then
        AC_MSG_RESULT(hexadecimal number requires no quotation)
        QFLAG=
    fi
    rm -f conftest? conftest?.?
])

dnl
dnl Get information about maximum and minimum value
dnl
AC_DEFUN(DCL_CHECK_VALUES,
[
    AC_REQUIRE([DCL_CHECK_FORTRAN_HEX_DATA])
    AC_CHECK_HEADER(float.h, EXTRA_CFLAGS=-DHAVE_FLOAT_H)

cat << EOF > conftest.c
#include <stdio.h>
#include <stdlib.h>
#ifdef HAVE_FLOAT_H
#  include <float.h>
#endif
#include <limits.h>

#ifdef FLT_MIN
#  define float_min FLT_MIN
#else
#  define float_min 1.175494351E-38
#endif
#ifdef FLT_MAX
#  define float_max FLT_MAX
#else
#  define float_max 3.402823466E+38
#endif
#ifdef FLT_EPSILON
#  define float_epsilon FLT_EPSILON
#else
#  define float_epsilon 1.192092896E-07
#endif
#ifdef INT_MAX
#  define int_max INT_MAX
#else
#  define int_max 2147483647
#endif

typedef union {
    int          si;
    unsigned int ui;
} INT;
typedef union {
    float        fl;
    unsigned int ui;
} FLT;

int main ()
{
    FILE *fd1, *fd2, *fd3, *fd4;
    FLT fmin, fmax;
    float feps;
    INT imax;

    fmin.fl = float_min, fmax.fl = float_max;
    feps = float_epsilon * 10.001;
    imax.si = int_max;

    if ((fd1 = fopen("fminval", "w")) == NULL) {
        fprintf(stderr, "conftest: cannot open file fminval\n");
        exit(1);
    }
    if ((fd2 = fopen("fmaxval", "w")) == NULL) {
        fprintf(stderr, "conftest: cannot open file fmaxval\n");
        exit(1);
    }
    if ((fd3 = fopen("fepsval", "w")) == NULL) {
        fprintf(stderr, "conftest: cannot open file fepsval\n");
        exit(1);
    }
    if ((fd4 = fopen("imaxval", "w")) == NULL) {
        fprintf(stderr, "conftest: cannot open file imaxval\n");
        exit(1);
    }
#ifdef QUOTE
    fprintf(fd1, "Z'%08X'\n", fmin.ui);
    fprintf(fd2, "Z'%08X'\n", fmax.ui);
    fprintf(fd4, "Z'%08X'\n", imax.ui);
#else
    fprintf(fd1, "Z%08X\n", fmin.ui);
    fprintf(fd2, "Z%08X\n", fmax.ui);
    fprintf(fd4, "Z%08X\n", imax.ui);
#endif
    fprintf(fd3, "%12.5E\n", feps);
    fclose(fd1);
    fclose(fd2);
    fclose(fd3);
    fclose(fd4);
    exit(0);
}
EOF

    REALMIN=
    REALMAX=
    REPSL=
    INTMAX=
    doit='$CC -o conftest ${CFLAGS-} ${EXTRA_CFLAGS-} $QFLAG conftest.c'
    if AC_TRY_EVAL(doit); then
        doit=`./conftest`
        REALMIN=`cat fminval`
        REALMAX=`cat fmaxval`
        REPSL=`cat fepsval`
        INTMAX=`cat imaxval`
        AC_MSG_RESULT(REALMAX is ${REALMAX})
        AC_MSG_RESULT(REALMIN is ${REALMIN})
        AC_MSG_RESULT(EPSILON is ${REPSL})
        AC_MSG_RESULT(INTMAX is ${INTMAX})
    fi
    AC_SUBST(REALMIN)
    AC_SUBST(REALMAX)
    AC_SUBST(REPSL)
    AC_SUBST(INTMAX)

    rm -f conftest conftest.? fminval fmaxval fepsval imaxval
])

dnl
dnl Check the value CLOCKS_PER_SEC and CLK_TCK
dnl
AC_DEFUN(DCL_CHECK_CLOCK_VALUES,
[
    cat << EOF > conftest.c
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#ifndef CLOCKS_PER_SEC
#  define CLK_PER_SEC 1000000
#else
#  define CLK_PER_SEC 0
#endif
#ifndef CLK_TCK
#  define CLK_RSL_TCK 60
#else
#  define CLK_RSL_TCK 0
#endif

int main ()
{
    FILE *fd1, *fd2;

    if ((fd1 = fopen("confval1", "w")) == NULL) {
        fprintf(stderr, "conftest: cannot open file confval1\n");
        exit(1);
    }
    if ((fd2 = fopen("confval2", "w")) == NULL) {
        fprintf(stderr, "conftest: cannot open file confval2\n");
        exit(1);
    }

    fprintf(fd1, "%d", CLK_PER_SEC);
    fprintf(fd2, "%d", CLK_RSL_TCK);

    fclose(fd1);
    fclose(fd2);
}
EOF

    doit='$CC -o conftest ${CFLAGS-} conftest.c'
    CLK_PER_SEC=
    CLK_RSL_TCK=
    if AC_TRY_EVAL(doit); then
        doit=`./conftest`
        cpsval=`cat confval1`
        crtval=`cat confval2`
        if test ${cpsval} != 0; then
            CLK_PER_SEC=${cpsval}
            AC_MSG_RESULT(CLOCKS_PER_SEC is ${CLK_PER_SEC})
        else
            AC_MSG_RESULT(CLOCKS_PER_SEC is provided by the system)
        fi
        if test ${crtval} != 0; then
            CLK_RSL_TCK=${crtval}
            AC_MSG_RESULT(CLK_TCK is ${CLK_RSL_TCK})
        else
            AC_MSG_RESULT(CLK_TCK is provided by the system)
        fi
    fi
    AC_SUBST(CLK_PER_SEC)
    AC_SUBST(CLK_RSL_TCK)

    rm -f conftest conftest.? confval?
])



# Configure paths for GTK+
# Owen Taylor     1997-2001

dnl AM_PATH_GTK_2_0([MINIMUM-VERSION, [ACTION-IF-FOUND [, ACTION-IF-NOT-FOUND [, MODULES]]]])
dnl Test for GTK+, and define GTK_CFLAGS and GTK_LIBS, if gthread is specified in MODULES,
dnl pass to pkg-config
dnl
AC_DEFUN(AM_PATH_GTK_2_0,
[dnl
dnl Get the cflags and libraries from pkg-config
dnl
AC_ARG_ENABLE(gtktest, [  --disable-gtktest       do not try to compile and run a test GTK+ program],
		    , enable_gtktest=yes)

  pkg_config_args=gtk+-2.0
  for module in . $4
  do
      case "$module" in
         gthread)
             pkg_config_args="$pkg_config_args gthread-2.0"
         ;;
      esac
  done

  no_gtk=""

  AC_PATH_PROG(PKG_CONFIG, pkg-config, no)

  if test x$PKG_CONFIG != xno ; then
    if pkg-config --atleast-pkgconfig-version 0.7 ; then
      :
    else
      echo *** pkg-config too old; version 0.7 or better required.
      no_gtk=yes
      PKG_CONFIG=no
    fi
  else
    no_gtk=yes
  fi

  min_gtk_version=ifelse([$1], ,2.0.0,$1)
  AC_MSG_CHECKING(for GTK+ - version >= $min_gtk_version)

  if test x$PKG_CONFIG != xno ; then
    ## don't try to run the test against uninstalled libtool libs
    if $PKG_CONFIG --uninstalled $pkg_config_args; then
	  echo "Will use uninstalled version of GTK+ found in PKG_CONFIG_PATH"
	  enable_gtktest=no
    fi

    if $PKG_CONFIG --atleast-version $min_gtk_version $pkg_config_args; then
	  :
    else
	  no_gtk=yes
    fi
  fi

  if test x"$no_gtk" = x ; then
    GTK_CFLAGS=`$PKG_CONFIG $pkg_config_args --cflags`
    GTK_LIBS=`$PKG_CONFIG $pkg_config_args --libs`
    gtk_config_major_version=`$PKG_CONFIG --modversion gtk+-2.0 | \
           sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\1/'`
    gtk_config_minor_version=`$PKG_CONFIG --modversion gtk+-2.0 | \
           sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\2/'`
    gtk_config_micro_version=`$PKG_CONFIG --modversion gtk+-2.0 | \
           sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\3/'`
    if test "x$enable_gtktest" = "xyes" ; then
      ac_save_CFLAGS="$CFLAGS"
      ac_save_LIBS="$LIBS"
      CFLAGS="$CFLAGS $GTK_CFLAGS"
      LIBS="$GTK_LIBS $LIBS"
dnl
dnl Now check if the installed GTK+ is sufficiently new. (Also sanity
dnl checks the results of pkg-config to some extent)
dnl
      rm -f conf.gtktest
      AC_TRY_RUN([
#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>

int main ()
{
  int major, minor, micro;
  char *tmp_version;

  system ("touch conf.gtktest");

  /* HP/UX 9 (%@#!) writes to sscanf strings */
  tmp_version = g_strdup("$min_gtk_version");
  if (sscanf(tmp_version, "%d.%d.%d", &major, &minor, &micro) != 3) {
     printf("%s, bad version string\n", "$min_gtk_version");
     exit(1);
   }

  if ((gtk_major_version != $gtk_config_major_version) ||
      (gtk_minor_version != $gtk_config_minor_version) ||
      (gtk_micro_version != $gtk_config_micro_version))
    {
      printf("\n*** 'pkg-config --modversion gtk+-2.0' returned %d.%d.%d, but GTK+ (%d.%d.%d)\n",
             $gtk_config_major_version, $gtk_config_minor_version, $gtk_config_micro_version,
             gtk_major_version, gtk_minor_version, gtk_micro_version);
      printf ("*** was found! If pkg-config was correct, then it is best\n");
      printf ("*** to remove the old version of GTK+. You may also be able to fix the error\n");
      printf("*** by modifying your LD_LIBRARY_PATH enviroment variable, or by editing\n");
      printf("*** /etc/ld.so.conf. Make sure you have run ldconfig if that is\n");
      printf("*** required on your system.\n");
      printf("*** If pkg-config was wrong, set the environment variable PKG_CONFIG_PATH\n");
      printf("*** to point to the correct configuration files\n");
    }
  else if ((gtk_major_version != GTK_MAJOR_VERSION) ||
	   (gtk_minor_version != GTK_MINOR_VERSION) ||
           (gtk_micro_version != GTK_MICRO_VERSION))
    {
      printf("*** GTK+ header files (version %d.%d.%d) do not match\n",
	     GTK_MAJOR_VERSION, GTK_MINOR_VERSION, GTK_MICRO_VERSION);
      printf("*** library (version %d.%d.%d)\n",
	     gtk_major_version, gtk_minor_version, gtk_micro_version);
    }
  else
    {
      if ((gtk_major_version > major) ||
        ((gtk_major_version == major) && (gtk_minor_version > minor)) ||
        ((gtk_major_version == major) && (gtk_minor_version == minor) && (gtk_micro_version >= micro)))
      {
        return 0;
       }
     else
      {
        printf("\n*** An old version of GTK+ (%d.%d.%d) was found.\n",
               gtk_major_version, gtk_minor_version, gtk_micro_version);
        printf("*** You need a version of GTK+ newer than %d.%d.%d. The latest version of\n",
	       major, minor, micro);
        printf("*** GTK+ is always available from ftp://ftp.gtk.org.\n");
        printf("***\n");
        printf("*** If you have already installed a sufficiently new version, this error\n");
        printf("*** probably means that the wrong copy of the pkg-config shell script is\n");
        printf("*** being found. The easiest way to fix this is to remove the old version\n");
        printf("*** of GTK+, but you can also set the PKG_CONFIG environment to point to the\n");
        printf("*** correct copy of pkg-config. (In this case, you will have to\n");
        printf("*** modify your LD_LIBRARY_PATH enviroment variable, or edit /etc/ld.so.conf\n");
        printf("*** so that the correct libraries are found at run-time))\n");
      }
    }
  return 1;
}
],, no_gtk=yes,[echo $ac_n "cross compiling; assumed OK... $ac_c"])
       CFLAGS="$ac_save_CFLAGS"
       LIBS="$ac_save_LIBS"
     fi
  fi
  if test "x$no_gtk" = x ; then
     AC_MSG_RESULT(yes (version $gtk_config_major_version.$gtk_config_minor_version.$gtk_config_micro_version))
     ifelse([$2], , :, [$2])
  else
     AC_MSG_RESULT(no)
     if test "$PKG_CONFIG" = "no" ; then
       echo "*** A new enough version of pkg-config was not found."
       echo "*** See http://pkgconfig.sourceforge.net"
     else
       if test -f conf.gtktest ; then
        :
       else
          echo "*** Could not run GTK+ test program, checking why..."
	  ac_save_CFLAGS="$CFLAGS"
	  ac_save_LIBS="$LIBS"
          CFLAGS="$CFLAGS $GTK_CFLAGS"
          LIBS="$LIBS $GTK_LIBS"
          AC_TRY_LINK([
#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
],      [ return ((gtk_major_version) || (gtk_minor_version) || (gtk_micro_version)); ],
        [ echo "*** The test program compiled, but did not run. This usually means"
          echo "*** that the run-time linker is not finding GTK+ or finding the wrong"
          echo "*** version of GTK+. If it is not finding GTK+, you'll need to set your"
          echo "*** LD_LIBRARY_PATH environment variable, or edit /etc/ld.so.conf to point"
          echo "*** to the installed location  Also, make sure you have run ldconfig if that"
          echo "*** is required on your system"
	  echo "***"
          echo "*** If you have an old version installed, it is best to remove it, although"
          echo "*** you may also be able to get things to work by modifying LD_LIBRARY_PATH" ],
        [ echo "*** The test program failed to compile or link. See the file config.log for the"
          echo "*** exact error that occured. This usually means GTK+ is incorrectly installed."])
          CFLAGS="$ac_save_CFLAGS"
          LIBS="$ac_save_LIBS"
       fi
     fi
     GTK_CFLAGS=""
     GTK_LIBS=""
     ifelse([$3], , :, [$3])
  fi
  AC_SUBST(GTK_CFLAGS)
  AC_SUBST(GTK_LIBS)
  rm -f conf.gtktest
])



dnl gdk-pixbuf-2.0
AC_DEFUN(AM_PATH_GDK_PIXBUF_2_0,
[dnl
  pkg_config_args=gdk-pixbuf-2.0

  no_gdk_pixbuf=""

  AC_PATH_PROG(PKG_CONFIG, pkg-config, no)

  if test x$PKG_CONFIG != xno ; then
    if pkg-config --atleast-pkgconfig-version 0.7 ; then
      :
    else
      echo *** pkg-config too old; version 0.7 or better required.
      no_gdk_pixbuf=yes
      PKG_CONFIG=no
    fi
  else
    no_gdk_pixbuf=yes
  fi

  min_gdk_pixbuf_version=ifelse([$1], ,2.0.0,$1)
  AC_MSG_CHECKING(for GDK_PIXBUF)

  if test x$PKG_CONFIG != xno ; then
    ## don't try to run the test against uninstalled libtool libs
    if $PKG_CONFIG --uninstalled $pkg_config_args; then
	  echo "Will use uninstalled version of GDK_PIXBUF found in PKG_CONFIG_PATH"
    fi

    if $PKG_CONFIG --atleast-version $min_gdk_pixbuf_version $pkg_config_args; then
	  :
    else
	  no_gdk_pixbuf=yes
    fi
  fi

  if test x"$no_gdk_pixbuf" = x ; then
    gdk_pixbuf_config_major_version=`$PKG_CONFIG --modversion gdk-pixbuf-2.0 | \
           sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\1/'`
    gdk_pixbuf_config_minor_version=`$PKG_CONFIG --modversion gdk-pixbuf-2.0 | \
           sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\2/'`
    gdk_pixbuf_config_micro_version=`$PKG_CONFIG --modversion gdk-pixbuf-2.0 | \
           sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\3/'`
  fi

  if test x"$no_gdk_pixbuf" = x ; then
    GDK_PIXBUF_CFLAGS=`$PKG_CONFIG $pkg_config_args --cflags`
    GDK_PIXBUF_LIBS=`$PKG_CONFIG $pkg_config_args --libs`
    AC_MSG_RESULT(yes (version $gdk_pixbuf_config_major_version.$gdk_pixbuf_config_minor_version.$gdk_pixbuf_config_micro_version))
  else
    AC_MSG_RESULT(no)
  fi
  AC_SUBST(GDK_PIXBUF_CFLAGS)
  AC_SUBST(GDK_PIXBUF_LIBS)
])



# Configure paths for GTK+
# Owen Taylor     97-11-3

dnl AM_PATH_GTK([MINIMUM-VERSION, [ACTION-IF-FOUND [, ACTION-IF-NOT-FOUND [, MODULES]]]])
dnl Test for GTK, and define GTK_CFLAGS and GTK_LIBS
dnl
AC_DEFUN(AM_PATH_GTK,
[dnl
dnl Get the cflags and libraries from the gtk-config script
dnl
AC_ARG_WITH(gtk-prefix,[  --with-gtk-prefix=PFX   Prefix where GTK is installed (optional)],
            gtk_config_prefix="$withval", gtk_config_prefix="")
AC_ARG_WITH(gtk-exec-prefix,[  --with-gtk-exec-prefix=PFX Exec prefix where GTK is installed (optional)],
            gtk_config_exec_prefix="$withval", gtk_config_exec_prefix="")
AC_ARG_ENABLE(gtktest, [  --disable-gtktest       Do not try to compile and run a test GTK program],
		    , enable_gtktest=yes)

  for module in . $4
  do
      case "$module" in
         gthread)
             gtk_config_args="$gtk_config_args gthread"
         ;;
      esac
  done

  if test x$gtk_config_exec_prefix != x ; then
     gtk_config_args="$gtk_config_args --exec-prefix=$gtk_config_exec_prefix"
     if test x${GTK_CONFIG+set} != xset ; then
        GTK_CONFIG=$gtk_config_exec_prefix/bin/gtk-config
     fi
  fi
  if test x$gtk_config_prefix != x ; then
     gtk_config_args="$gtk_config_args --prefix=$gtk_config_prefix"
     if test x${GTK_CONFIG+set} != xset ; then
        GTK_CONFIG=$gtk_config_prefix/bin/gtk-config
     fi
  fi

  AC_PATH_PROG(GTK_CONFIG, gtk-config, no)
  min_gtk_version=ifelse([$1], ,0.99.7,$1)
  AC_MSG_CHECKING(for GTK - version >= $min_gtk_version)
  no_gtk=""
  if test "$GTK_CONFIG" = "no" ; then
    no_gtk=yes
  else
    GTK_CFLAGS=`$GTK_CONFIG $gtk_config_args --cflags`
    GTK_LIBS=`$GTK_CONFIG $gtk_config_args --libs`
    gtk_config_major_version=`$GTK_CONFIG $gtk_config_args --version | \
           sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\1/'`
    gtk_config_minor_version=`$GTK_CONFIG $gtk_config_args --version | \
           sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\2/'`
    gtk_config_micro_version=`$GTK_CONFIG $gtk_config_args --version | \
           sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\3/'`
    if test "x$enable_gtktest" = "xyes" ; then
      ac_save_CFLAGS="$CFLAGS"
      ac_save_LIBS="$LIBS"
      CFLAGS="$CFLAGS $GTK_CFLAGS"
      LIBS="$GTK_LIBS $LIBS"
dnl
dnl Now check if the installed GTK is sufficiently new. (Also sanity
dnl checks the results of gtk-config to some extent
dnl
      rm -f conf.gtktest
      AC_TRY_RUN([
#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>

int main ()
{
  int major, minor, micro;
  char *tmp_version;

  system ("touch conf.gtktest");

  /* HP/UX 9 (%@#!) writes to sscanf strings */
  tmp_version = g_strdup("$min_gtk_version");
  if (sscanf(tmp_version, "%d.%d.%d", &major, &minor, &micro) != 3) {
     printf("%s, bad version string\n", "$min_gtk_version");
     exit(1);
   }

  if ((gtk_major_version != $gtk_config_major_version) ||
      (gtk_minor_version != $gtk_config_minor_version) ||
      (gtk_micro_version != $gtk_config_micro_version))
    {
      printf("\n*** 'gtk-config --version' returned %d.%d.%d, but GTK+ (%d.%d.%d)\n",
             $gtk_config_major_version, $gtk_config_minor_version, $gtk_config_micro_version,
             gtk_major_version, gtk_minor_version, gtk_micro_version);
      printf ("*** was found! If gtk-config was correct, then it is best\n");
      printf ("*** to remove the old version of GTK+. You may also be able to fix the error\n");
      printf("*** by modifying your LD_LIBRARY_PATH enviroment variable, or by editing\n");
      printf("*** /etc/ld.so.conf. Make sure you have run ldconfig if that is\n");
      printf("*** required on your system.\n");
      printf("*** If gtk-config was wrong, set the environment variable GTK_CONFIG\n");
      printf("*** to point to the correct copy of gtk-config, and remove the file config.cache\n");
      printf("*** before re-running configure\n");
    }
#if defined (GTK_MAJOR_VERSION) && defined (GTK_MINOR_VERSION) && defined (GTK_MICRO_VERSION)
  else if ((gtk_major_version != GTK_MAJOR_VERSION) ||
	   (gtk_minor_version != GTK_MINOR_VERSION) ||
           (gtk_micro_version != GTK_MICRO_VERSION))
    {
      printf("*** GTK+ header files (version %d.%d.%d) do not match\n",
	     GTK_MAJOR_VERSION, GTK_MINOR_VERSION, GTK_MICRO_VERSION);
      printf("*** library (version %d.%d.%d)\n",
	     gtk_major_version, gtk_minor_version, gtk_micro_version);
    }
#endif /* defined (GTK_MAJOR_VERSION) ... */
  else
    {
      if ((gtk_major_version > major) ||
        ((gtk_major_version == major) && (gtk_minor_version > minor)) ||
        ((gtk_major_version == major) && (gtk_minor_version == minor) && (gtk_micro_version >= micro)))
      {
        return 0;
       }
     else
      {
        printf("\n*** An old version of GTK+ (%d.%d.%d) was found.\n",
               gtk_major_version, gtk_minor_version, gtk_micro_version);
        printf("*** You need a version of GTK+ newer than %d.%d.%d. The latest version of\n",
	       major, minor, micro);
        printf("*** GTK+ is always available from ftp://ftp.gtk.org.\n");
        printf("***\n");
        printf("*** If you have already installed a sufficiently new version, this error\n");
        printf("*** probably means that the wrong copy of the gtk-config shell script is\n");
        printf("*** being found. The easiest way to fix this is to remove the old version\n");
        printf("*** of GTK+, but you can also set the GTK_CONFIG environment to point to the\n");
        printf("*** correct copy of gtk-config. (In this case, you will have to\n");
        printf("*** modify your LD_LIBRARY_PATH enviroment variable, or edit /etc/ld.so.conf\n");
        printf("*** so that the correct libraries are found at run-time))\n");
      }
    }
  return 1;
}
],, no_gtk=yes,[echo $ac_n "cross compiling; assumed OK... $ac_c"])
       CFLAGS="$ac_save_CFLAGS"
       LIBS="$ac_save_LIBS"
     fi
  fi
  if test "x$no_gtk" = x ; then
     AC_MSG_RESULT(yes)
     ifelse([$2], , :, [$2])
  else
     AC_MSG_RESULT(no)
     if test "$GTK_CONFIG" = "no" ; then
       echo "*** The gtk-config script installed by GTK could not be found"
       echo "*** If GTK was installed in PREFIX, make sure PREFIX/bin is in"
       echo "*** your path, or set the GTK_CONFIG environment variable to the"
       echo "*** full path to gtk-config."
     else
       if test -f conf.gtktest ; then
        :
       else
          echo "*** Could not run GTK test program, checking why..."
          CFLAGS="$CFLAGS $GTK_CFLAGS"
          LIBS="$LIBS $GTK_LIBS"
          AC_TRY_LINK([
#include <gtk/gtk.h>
#include <stdio.h>
#include <stdlib.h>
],      [ return ((gtk_major_version) || (gtk_minor_version) || (gtk_micro_version)); ],
        [ echo "*** The test program compiled, but did not run. This usually means"
          echo "*** that the run-time linker is not finding GTK or finding the wrong"
          echo "*** version of GTK. If it is not finding GTK, you'll need to set your"
          echo "*** LD_LIBRARY_PATH environment variable, or edit /etc/ld.so.conf to point"
          echo "*** to the installed location  Also, make sure you have run ldconfig if that"
          echo "*** is required on your system"
	  echo "***"
          echo "*** If you have an old version installed, it is best to remove it, although"
          echo "*** you may also be able to get things to work by modifying LD_LIBRARY_PATH"
          echo "***"
          echo "*** If you have a RedHat 5.0 system, you should remove the GTK package that"
          echo "*** came with the system with the command"
          echo "***"
          echo "***    rpm --erase --nodeps gtk gtk-devel" ],
        [ echo "*** The test program failed to compile or link. See the file config.log for the"
          echo "*** exact error that occured. This usually means GTK was incorrectly installed"
          echo "*** or that you have moved GTK since it was installed. In the latter case, you"
          echo "*** may want to edit the gtk-config script: $GTK_CONFIG" ])
          CFLAGS="$ac_save_CFLAGS"
          LIBS="$ac_save_LIBS"
       fi
     fi
     GTK_CFLAGS=""
     GTK_LIBS=""
     ifelse([$3], , :, [$3])
  fi
  AC_SUBST(GTK_CFLAGS)
  AC_SUBST(GTK_LIBS)
  rm -f conf.gtktest
])


# Configure paths for IMLIB
# Frank Belew     98-8-31
# stolen from Manish Singh
# Shamelessly stolen from Owen Taylor

# Check for gdk-imlib
AC_DEFUN(AM_PATH_GDK_IMLIB,
[dnl
dnl Get the cflags and libraries from the imlib-config script
dnl
AC_ARG_WITH(imlib-prefix,[  --with-imlib-prefix=PFX   Prefix where IMLIB is installed (optional)],
            imlib_prefix="$withval", imlib_prefix="")
AC_ARG_WITH(imlib-exec-prefix,[  --with-imlib-exec-prefix=PFX Exec prefix where IMLIB is installed (optional)],
            imlib_exec_prefix="$withval", imlib_exec_prefix="")
AC_ARG_ENABLE(imlibtest, [  --disable-imlibtest       Do not try to compile and run a test IMLIB program],
		    , enable_imlibtest=yes)

  if test x$imlib_exec_prefix != x ; then
     imlib_args="$imlib_args --exec-prefix=$imlib_exec_prefix"
     if test x${IMLIB_CONFIG+set} != xset ; then
        IMLIB_CONFIG=$imlib_exec_prefix/bin/imlib-config
     fi
  fi
  if test x$imlib_prefix != x ; then
     imlib_args="$imlib_args --prefix=$imlib_prefix"
     if test x${IMLIB_CONFIG+set} != xset ; then
        IMLIB_CONFIG=$imlib_prefix/bin/imlib-config
     fi
  fi

  AC_PATH_PROG(IMLIB_CONFIG, imlib-config, no)
  min_imlib_version=ifelse([$1], ,1.8.2,$1)
  AC_MSG_CHECKING(for IMLIB - version >= $min_imlib_version)
  no_imlib=""
  if test "$IMLIB_CONFIG" = "no" ; then
    no_imlib=yes
  else
    GDK_IMLIB_CFLAGS=`$IMLIB_CONFIG $imlibconf_args --cflags-gdk`
    GDK_IMLIB_LIBS=`$IMLIB_CONFIG $imlibconf_args --libs-gdk`

    imlib_major_version=`$IMLIB_CONFIG $imlib_args --version | \
           sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\1/'`
    imlib_minor_version=`$IMLIB_CONFIG $imlib_args --version | \
           sed 's/\([[0-9]]*\).\([[0-9]]*\).\([[0-9]]*\)/\2/'`
    if test "x$enable_imlibtest" = "xyes" ; then
      ac_save_CFLAGS="$CFLAGS"
      ac_save_LIBS="$LIBS"
      CFLAGS="$CFLAGS $GDK_IMLIB_CFLAGS"
      LIBS="$LIBS $GDK_IMLIB_LIBS"
dnl
dnl Now check if the installed IMLIB is sufficiently new. (Also sanity
dnl checks the results of imlib-config to some extent
dnl
      rm -f conf.imlibtest
      AC_TRY_RUN([
#include <stdio.h>
#include <stdlib.h>
#include <gdk_imlib.h>

int main ()
{
  int major, minor;
  char *tmp_version;

  system ("touch conf.gdkimlibtest");

  /* HP/UX 9 (%@#!) writes to sscanf strings */
  tmp_version = g_strdup("$min_imlib_version");
  if (sscanf(tmp_version, "%d.%d", &major, &minor) != 2) {
     printf("%s, bad version string\n", "$min_imlib_version");
     exit(1);
   }

    if (($imlib_major_version > major) ||
        (($imlib_major_version == major) && ($imlib_minor_version >= minor)))
    {
      return 0;
    }
  else
    {
      printf("\n*** 'imlib-config --version' returned %d.%d, but the minimum version\n", $imlib_major_version, $imlib_minor_version);
      printf("*** of IMLIB required is %d.%d. If imlib-config is correct, then it is\n", major, minor);
      printf("*** best to upgrade to the required version.\n");
      printf("*** If imlib-config was wrong, set the environment variable IMLIB_CONFIG\n");
      printf("*** to point to the correct copy of imlib-config, and remove the file\n");
      printf("*** config.cache before re-running configure\n");
      return 1;
    }
}

],, no_imlib=yes,[echo $ac_n "cross compiling; assumed OK... $ac_c"])
       CFLAGS="$ac_save_CFLAGS"
       LIBS="$ac_save_LIBS"
     fi
  fi
  if test "x$no_imlib" = x ; then
     AC_MSG_RESULT(yes)
     ifelse([$2], , :, [$2])
  else
     AC_MSG_RESULT(no)
     if test "$IMLIB_CONFIG" = "no" ; then
       echo "*** The imlib-config script installed by IMLIB could not be found"
       echo "*** If IMLIB was installed in PREFIX, make sure PREFIX/bin is in"
       echo "*** your path, or set the IMLIB_CONFIG environment variable to the"
       echo "*** full path to imlib-config."
     else
       if test -f conf.gdkimlibtest ; then
        :
       else
          echo "*** Could not run IMLIB test program, checking why..."
          CFLAGS="$CFLAGS $GDK_IMLIB_CFLAGS"
          LIBS="$LIBS $GDK_IMLIB_LIBS"
          AC_TRY_LINK([
#include <stdio.h>
#include <stdlib.h>
#include <gdk_imlib.h>
],      [ return 0; ],
        [ echo "*** The test program compiled, but did not run. This usually means"
          echo "*** that the run-time linker is not finding IMLIB or finding the wrong"
          echo "*** version of IMLIB. If it is not finding IMLIB, you'll need to set your"
          echo "*** LD_LIBRARY_PATH environment variable, or edit /etc/ld.so.conf to point"
          echo "*** to the installed location  Also, make sure you have run ldconfig if that"
          echo "*** is required on your system"
	  echo "***"
          echo "*** If you have an old version installed, it is best to remove it, although"
          echo "*** you may also be able to get things to work by modifying LD_LIBRARY_PATH"],
        [ echo "*** The test program failed to compile or link. See the file config.log for the"
          echo "*** exact error that occured. This usually means IMLIB was incorrectly installed"
          echo "*** or that you have moved IMLIB since it was installed. In the latter case, you"
          echo "*** may want to edit the imlib-config script: $IMLIB_CONFIG" ])
          CFLAGS="$ac_save_CFLAGS"
          LIBS="$ac_save_LIBS"
       fi
     fi
     IMLIB_CFLAGS=""
     IMLIB_LIBS=""
     ifelse([$3], , :, [$3])
  fi
  AC_SUBST(GDK_IMLIB_CFLAGS)
  AC_SUBST(GDK_IMLIB_LIBS)
  rm -f conf.gdkimlibtest
])


dnl
dnl check whether shared library is created or not
dnl

AC_DEFUN(DCL_SHARED,
[
dnl
dnl    Get how to create shared library from archive library depending upon a platform,
dnl    and write it into Mkinclude
dnl
    AC_ARG_ENABLE(shared,[  --enable-shared   create shared library (optional)],
                  [enable_shared="yes"])
    TODO_SHARED_LIBRARY=
    LINKOPT=
    EXTSHLIB=so
    if test "x$enable_shared" = "xyes" ; then
        case `uname -s` in
            Linux)
                TODO_SHARED_LIBRARY="\$(LD) -o \$(LDCLSHLIBFILE) -shared --whole-archive \$(LDCLLIBFILE)"
                ;;
            HP-UX*)
                EXTSHLIB=sl
                TODO_SHARED_LIBRARY="nm \$(LDCLLIBFILE) | grep extern | grep entry | awk '-F|' '{print \@S|@\@S|@1}' | sed 's/^/-u /' >symbols.log; \$(LD) -o \$(LDCLSHLIBFILE) -b -c symbols.log \$(LDCLLIBFILE); rm symbols.log"
                ;;
            IRIX*)
                TODO_SHARED_LIBRARY="\$(LD) -o \$(LDCLSHLIBFILE) -shared -no_archive -all \$(LDCLLIBFILE) -none -lc -lC \$(LDLIBS)"
                ;;
            OSF1)
                TODO_SHARED_LIBRARY="\$(LD) -o \$(LDCLSHLIBFILE) -shared \$(LDCLLIBFILE)"
                ;;
            SunOS)
                case `uname -r` in
                    4*)
                        TODO_SHARED_LIBRARY="undefopts=\`/bin/nm \$(LDCLLIBFILE) | awk '\@S|@\@S|@2~/^T\@S|@\@S|@/{print \@S|@\@S|@3}' | sed 's/^/-u /'\` && \$(LD) -o \$(LDCLSHLIBFILE) \$\$undefopts \$(LDCLLIBFILE)"
                        ;;
                    5*)
                        TODO_SHARED_LIBRARY="undefopts=\`/usr/ccs/bin/nm \$(LDCLLIBFILE) | grep GLOB | grep FUNC | awk '-F|' '{print \@S|@\@S|@8}' | sed 's/^/-u /'\` && \$(LD) -o \$(LDCLSHLIBFILE) -G \$\$undefopts \$(LDCLLIBFILE)"
                        ;;
                esac
                ;;
            *)
                echo "Don't know how to make a shared library on this system."
                exit 1
                ;;
        esac
        case $CC in
            *gcc*)
		CFLAGS="${CFLAGS-} -fPIC"
                LINKOPT="-Xlinker -rpath -Xlinker \$(LIBDIR)"
                ;;
            fcc)
                LINKOPT="-rpath \$(LIBDIR)"
                ;;
            cc)
                case `uname -s` in
                    Linux)
			CFLAGS="${CFLAGS-} -fPIC"
                        LINKOPT="-Xlinker -rpath -Xlinker \$(LIBDIR)"
                        ;;
                    SunOS*)
                        LINKOPT="-R\$(LIBDIR)"
		        case `uname -r` in
				4*)
					CFLAGS="${CFLAGS-} -PIC"
					;;
				5*)
					CFLAGS="${CFLAGS-} -KPIC"
					;;
			esac
                        ;;
                    HP-UX*)
			CFLAGS="${CFLAGS-} +Z"
                        LINKOPT="-rpath \$(LIBDIR)"
                        ;;
                    IRIX*|OSF1)
			CFLAGS="${CFLAGS-} -KPIC"
                        LINKOPT="-Wl,-rpath,\$(LIBDIR)"
                        ;;
                esac
                ;;
            *)
                echo "Don't know link options for linking shared library..."
                echo "*** executable file might not be executed...***"
                ;;
        esac
    fi
    AC_SUBST(TODO_SHARED_LIBRARY)
    AC_SUBST(LINKOPT)
    AC_SUBST(EXTSHLIB)
])
# pkg.m4 - Macros to locate and utilise pkg-config.            -*- Autoconf -*-
# serial 1 (pkg-config-0.24)
#
# Copyright 息 2004 Scott James Remnant <scott@netsplit.com>.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
#
# As a special exception to the GNU General Public License, if you
# distribute this file as part of a program that contains a
# configuration script generated by Autoconf, you may include it under
# the same distribution terms that you use for the rest of that program.

# PKG_PROG_PKG_CONFIG([MIN-VERSION])
# ----------------------------------
AC_DEFUN([PKG_PROG_PKG_CONFIG],
[m4_pattern_forbid([^_?PKG_[A-Z_]+$])
m4_pattern_allow([^PKG_CONFIG(_(PATH|LIBDIR|SYSROOT_DIR|ALLOW_SYSTEM_(CFLAGS|LIBS)))?$])
m4_pattern_allow([^PKG_CONFIG_(DISABLE_UNINSTALLED|TOP_BUILD_DIR|DEBUG_SPEW)$])
AC_ARG_VAR([PKG_CONFIG], [path to pkg-config utility])
AC_ARG_VAR([PKG_CONFIG_PATH], [directories to add to pkg-config's search path])
AC_ARG_VAR([PKG_CONFIG_LIBDIR], [path overriding pkg-config's built-in search path])

if test "x$ac_cv_env_PKG_CONFIG_set" != "xset"; then
	AC_PATH_TOOL([PKG_CONFIG], [pkg-config])
fi
if test -n "$PKG_CONFIG"; then
	_pkg_min_version=m4_default([$1], [0.9.0])
	AC_MSG_CHECKING([pkg-config is at least version $_pkg_min_version])
	if $PKG_CONFIG --atleast-pkgconfig-version $_pkg_min_version; then
		AC_MSG_RESULT([yes])
	else
		AC_MSG_RESULT([no])
		PKG_CONFIG=""
	fi
fi[]dnl
])# PKG_PROG_PKG_CONFIG

# PKG_CHECK_EXISTS(MODULES, [ACTION-IF-FOUND], [ACTION-IF-NOT-FOUND])
#
# Check to see whether a particular set of modules exists.  Similar
# to PKG_CHECK_MODULES(), but does not set variables or print errors.
#
# Please remember that m4 expands AC_REQUIRE([PKG_PROG_PKG_CONFIG])
# only at the first occurence in configure.ac, so if the first place
# it's called might be skipped (such as if it is within an "if", you
# have to call PKG_CHECK_EXISTS manually
# --------------------------------------------------------------
AC_DEFUN([PKG_CHECK_EXISTS],
[AC_REQUIRE([PKG_PROG_PKG_CONFIG])dnl
if test -n "$PKG_CONFIG" && \
    AC_RUN_LOG([$PKG_CONFIG --exists --print-errors "$1"]); then
  m4_default([$2], [:])
m4_ifvaln([$3], [else
  $3])dnl
fi])

# _PKG_CONFIG([VARIABLE], [COMMAND], [MODULES])
# ---------------------------------------------
m4_define([_PKG_CONFIG],
[if test -n "$$1"; then
    pkg_cv_[]$1="$$1"
 elif test -n "$PKG_CONFIG"; then
    PKG_CHECK_EXISTS([$3],
                     [pkg_cv_[]$1=`$PKG_CONFIG --[]$2 "$3" 2>/dev/null`
		      test "x$?" != "x0" && pkg_failed=yes ],
		     [pkg_failed=yes])
 else
    pkg_failed=untried
fi[]dnl
])# _PKG_CONFIG

# _PKG_SHORT_ERRORS_SUPPORTED
# -----------------------------
AC_DEFUN([_PKG_SHORT_ERRORS_SUPPORTED],
[AC_REQUIRE([PKG_PROG_PKG_CONFIG])
if $PKG_CONFIG --atleast-pkgconfig-version 0.20; then
        _pkg_short_errors_supported=yes
else
        _pkg_short_errors_supported=no
fi[]dnl
])# _PKG_SHORT_ERRORS_SUPPORTED


# PKG_CHECK_MODULES(VARIABLE-PREFIX, MODULES, [ACTION-IF-FOUND],
# [ACTION-IF-NOT-FOUND])
#
#
# Note that if there is a possibility the first call to
# PKG_CHECK_MODULES might not happen, you should be sure to include an
# explicit call to PKG_PROG_PKG_CONFIG in your configure.ac
#
#
# --------------------------------------------------------------
AC_DEFUN([PKG_CHECK_MODULES],
[AC_REQUIRE([PKG_PROG_PKG_CONFIG])dnl
AC_ARG_VAR([$1][_CFLAGS], [C compiler flags for $1, overriding pkg-config])dnl
AC_ARG_VAR([$1][_LIBS], [linker flags for $1, overriding pkg-config])dnl

pkg_failed=no
AC_MSG_CHECKING([for $1])

_PKG_CONFIG([$1][_CFLAGS], [cflags], [$2])
_PKG_CONFIG([$1][_LIBS], [libs], [$2])

m4_define([_PKG_TEXT], [Alternatively, you may set the environment variables $1[]_CFLAGS
and $1[]_LIBS to avoid the need to call pkg-config.
See the pkg-config man page for more details.])

if test $pkg_failed = yes; then
   	AC_MSG_RESULT([no])
        _PKG_SHORT_ERRORS_SUPPORTED
        if test $_pkg_short_errors_supported = yes; then
	        $1[]_PKG_ERRORS=`$PKG_CONFIG --short-errors --print-errors --cflags --libs "$2" 2>&1`
        else
	        $1[]_PKG_ERRORS=`$PKG_CONFIG --print-errors --cflags --libs "$2" 2>&1`
        fi
	# Put the nasty error message in config.log where it belongs
	echo "$$1[]_PKG_ERRORS" >&AS_MESSAGE_LOG_FD

	m4_default([$4], [AC_MSG_ERROR(
[Package requirements ($2) were not met:

$$1_PKG_ERRORS

Consider adjusting the PKG_CONFIG_PATH environment variable if you
installed software in a non-standard prefix.

_PKG_TEXT])[]dnl
        ])
elif test $pkg_failed = untried; then
     	AC_MSG_RESULT([no])
	m4_default([$4], [AC_MSG_FAILURE(
[The pkg-config script could not be found or is too old.  Make sure it
is in your PATH or set the PKG_CONFIG environment variable to the full
path to pkg-config.

_PKG_TEXT

To get pkg-config, see <http://pkg-config.freedesktop.org/>.])[]dnl
        ])
else
	$1[]_CFLAGS=$pkg_cv_[]$1[]_CFLAGS
	$1[]_LIBS=$pkg_cv_[]$1[]_LIBS
        AC_MSG_RESULT([yes])
	$3
fi[]dnl
])# PKG_CHECK_MODULES
