*-----------------------------------------------------------------------
      PROGRAM SGPK02

      CALL SWCSTX('FNAME','SGPK02')
      CALL SWLSTX('LSEP',.TRUE.)

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ(*,*) IWS

      CALL SGOPN(IWS)

*----------------------------- page 1 ---------------------------------
      CALL SGFRM
      CALL SGSLNI(3)

*                  XMIN, XMAX, YMIN, YMAX
      CALL SGSWND(  0.0,  1.0,  0.0,  1.0)   !  <--+
      CALL SGSVPT(  0.0,  1.0,  0.0,  1.0)   !     | 変換関数の設定
      CALL SGSTRN(1)                         !     |
      CALL SGSTRF                            !  <--+

      CALL SLPVPR(1)                         !  <--- 枠を描く

      CALL SGLNZU( 0.2,  0.8,  0.8,  0.2, 2) !  <--- X印を描く
      CALL SGLNU ( 0.2,  0.2,  0.8,  0.8)    !  <-|

*----------------------------- page 2 ---------------------------------
      CALL SGFRM

*                  XMIN, XMAX, YMIN, YMAX
      CALL SGSWND(  0.0,  1.0,  0.0,  1.0)   !  <-- 正立
      CALL SGSVPT(  0.1,  0.4,  0.6,  0.9)   !  <-- 左上
      CALL SGSTRF

      CALL SLPVPR(1)

      CALL SGLNZU( 0.2,  0.8,  0.8,  0.2, 2)
      CALL SGLNU ( 0.2,  0.2,  0.8,  0.8)
*   ------------------------------------------
*                  XMIN, XMAX, YMIN, YMAX
      CALL SGSWND(  0.0,  1.0,  1.0,  0.0)   !  <-- 上下逆
      CALL SGSVPT(  0.6,  0.9,  0.6,  0.9)   !  <-- 右上
      CALL SGSTRF

      CALL SLPVPR(1)

      CALL SGLNZU( 0.2,  0.8,  0.8,  0.2, 2)
      CALL SGLNU ( 0.2,  0.2,  0.8,  0.8)
*   ------------------------------------------
*                  XMIN, XMAX, YMIN, YMAX
      CALL SGSWND(  0.0,  1.0,  0.0,  0.6)   !  <-- 小さなウインド
      CALL SGSVPT(  0.1,  0.4,  0.1,  0.4)   !  <-- 左下
      CALL SGSTRF
      CALL SLPVPR(1)

      CALL SGLNZU( 0.2,  0.8,  0.8,  0.2, 2)
      CALL SGLNU ( 0.2,  0.2,  0.8,  0.8)
*   ------------------------------------------
*                  XMIN, XMAX, YMIN, YMAX
      CALL SGSWND(  0.0,  1.0,  0.0,  0.6)   !  <-- 小さなウインド
      CALL SGSVPT(  0.6,  0.9,  0.1,  0.4)   !  <-- 右下
      CALL SGSTRF
      CALL SLPVPR(1)

      CALL SGLSET('LCLIP', .TRUE.)           !  <-- クリッピングの指定
      CALL SGLNZU( 0.2,  0.8,  0.8,  0.2, 2)
      CALL SGLNU ( 0.2,  0.2,  0.8,  0.8)

      CALL SGCLS

      END
