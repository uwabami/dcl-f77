*-----------------------------------------------------------------------
      PROGRAM SGPK05

      PARAMETER (N=9)
      DIMENSION Y(N)


      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ(*,*) IWS

      CALL SGOPN(IWS)
      CALL SGFRM

      X1 = 0.1
      X2 = 0.9
      XC = 0.5

      CALL SGSLNI(1)
      DO 10 I=1, N
        Y(I) = 0.1 * (10-I)
        CALL SGLNV(X1, Y(I), X2, Y(I))
  10  CONTINUE
      CALL SGLNV(XC, 0.05, XC, 0.95)

      CALL SGTXV(XC, Y(1), 'SGTXV TEST') ! <-- テキスト 1 段目

      CALL SGSTXI(2)                     ! <-- テキスト INDEX 設定
      CALL SGTXV(XC, Y(2), 'INDEX2')     ! <-- テキスト 2 段目

      CALL SGSTXI(3)                     ! <-- テキスト INDEX 設定
      CALL SGTXV(XC, Y(3), 'INDEX3')     ! <-- テキスト 3 段目

      CALL SGSTXI(4)

      CALL SGSTXS(0.03)                  ! <-- テキスト SIZE 設定 (小)
      CALL SGTXV(XC, Y(4), 'SMALL')      ! <-- テキスト 4 段目

      CALL SGSTXS(0.07)                  ! <-- テキスト SIZE 設定 (大)
      CALL SGTXV(XC, Y(5), 'LARGE')      ! <-- テキスト 5 段目

      CALL SGSTXS(0.05)

      CALL SGSTXI(5)

      CALL SGSTXC(-1)                    ! <-- 左揃え
      CALL SGTXV(XC, Y(6), 'LEFT')       ! <-- テキスト 6 段目

      CALL SGSTXC(0)                     ! <-- 中央揃え
      CALL SGTXV(XC, Y(7), 'CENTER')     ! <-- テキスト 7 段目

      CALL SGSTXC(1)                     ! <-- 右揃え
      CALL SGTXV(XC, Y(8), 'RIGHT')      ! <-- テキスト 8 段目

      CALL SGSTXC(0)

      CALL SGSTXI(4)
      CALL SGSTXR(20)                    ! <-- テキスト回転
      CALL SGTXV(XC, Y(9), 'ROTATION')   ! <-- テキスト 9 段目

      CALL SGCLS

      END
