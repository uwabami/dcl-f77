*-----------------------------------------------------------------------
      PROGRAM SGPK07

      PARAMETER (N=9)
      DIMENSION Y(N)
      CHARACTER GREEK1*10, GREEK2*10, SYMBOL*10, USGI*3


      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ(*,*) IWS

      CALL SGOPN(IWS)
      CALL SGFRM

      X1 = 0.1
      X2 = 0.9
      XC = 0.5

      CALL SGSLNI(1)
      DO 10 I=1, N
        Y(I) = 0.1 * (10-I)
        CALL SGLNV(X1, Y(I), X2, Y(I))
  10  CONTINUE
      CALL SGLNV(XC, 0.05, XC, 0.95)

*----------------------- SUPER/SUB SCRIPT ------------------------------

      CALL SGTXV(XC, Y(1), 'SGTXV|SUP"RST_SUB"')

      CALL SGSTXI(2)
      CALL SGLSET('LCNTL', .TRUE.)               ! <-- 添鐃緒申鐃緒申鐃緒申鐃夙ワ申鐃緒申鐃緒申ON
      CALL SGTXV(XC, Y(2), 'SGTXV|SUP"RST_SUB"')

      CALL SGRSET('SHIFT', 0.5)                  ! <-- 鐃緒申鐃春ワ申鐃緒申鐃緒申鐃緒申
      CALL SGRSET('SMALL', 0.5)                  ! <-- 添鐃緒申鐃緒申鐃順き鐃緒申鐃緒申鐃緒申
      CALL SGTXV(XC, Y(3), 'SGTXV|SUP"RST_SUB"')

*------------------------ FONT SELECTION -------------------------------

      CALL SGSTXI(1)
      CALL SGSTXS(0.05)
      CALL SGTXV(XC, Y(4), 'ABCDEFG abcdefg')

      CALL SGISET('IFONT', 2)                    ! <-- 鐃緒申鐃曙い鐃淑フワ申鐃緒申鐃緒申
      CALL SGTXV(XC, Y(5), 'ABCDEFG abcdefg')

      CALL SGSTXI(3)
      CALL SGTXV(XC, Y(6), 'ABCDEFG abcdefg')

*------------------------- GREEK LETTERS -------------------------------

      GREEK1 = USGI(152)//USGI(153)//USGI(154)//USGI(155)//USGI(156)//
     #         USGI(157)//USGI(158)//USGI(159)//USGI(160)//USGI(161)
      GREEK2 = USGI(130)//USGI(131)//USGI(135)//USGI(138)//USGI(141)//
     #         USGI(143)//USGI(145)//USGI(148)//USGI(150)//USGI(151)

      CALL SGTXV(XC, Y(7), GREEK1)
      CALL SGTXV(XC, Y(8), GREEK2)

*----------------------------- SYMBOLS ---------------------------------

      SYMBOL = USGI(189)//USGI(190)//USGI(191)//USGI(192)//USGI(193)//
     #         USGI(210)//USGI(211)//USGI(212)//USGI(217)//USGI(218)
      CALL SGTXV(XC, Y(9), SYMBOL)

      CALL SGCLS

      END
