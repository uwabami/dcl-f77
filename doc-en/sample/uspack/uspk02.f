*-----------------------------------------------------------------------
      PROGRAM USPK02

      PARAMETER (N=100)
      DOUBLE PRECISION A, R
      REAL X(N), Y(N)

*---------------------------- DATA DEFINITION --------------------------

      R = 0.2D0
      A = 3.6D0
      R0 = 0.
      DO 100 I=1, N
        R = A*R*(1.D0-R)
        R0 = R0 + R*4 - 2.58
        X2 = (I-50)**2
        REXP = 4.*I/N
        X(I) = 10**REXP
        Y(I) = 1.E5*EXP(-X2) + 10.**R0
  100 CONTINUE
      Y(20) = 1.E4
      Y(40) = 2.E3
      Y(65) = 3.E4
      Y(70) = 5.E2

*----------------------------- GRAPH -----------------------------------

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN(IWS)
      CALL GRFRM
      CALL GRSTRN(4)

      CALL USGRPH(N, X, Y)

      CALL GRCLS

*-----------------------------------------------------------------------

      END
