*-----------------------------------------------------------------------
      PROGRAM USPK01

      PARAMETER (N=400)
      REAL X(N), Y(N)

*---------------------------- DATA DEFINITION --------------------------

      DT = 3.14159 / (N-1)
      A = 1.E5
      B = 1.E-4
      C = 1.

      DO 100 I=1, N
        T = DT*(I-1)
        X(I) = A*SIN(6.*T)
        Y(I) = B*COS(14.*T) + C
  100 CONTINUE

*----------------------------- GRAPH -----------------------------------

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN(IWS)
      CALL GRFRM
      CALL USGRPH(N, X, Y)
      CALL GRCLS

*-----------------------------------------------------------------------

      END
