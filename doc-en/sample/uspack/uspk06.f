*-----------------------------------------------------------------------
      PROGRAM USPK06

      PARAMETER(N=200)
      DOUBLE PRECISION X, Y, Z, DX, DY, DZ, DT, S, R, B
      REAL T(N), A(N)

      DATA    X,    Y,    Z,     S,     R,     B,     DT /
     #     0.D0, 1.D0, 1.D0, 10.D0, 26.D0, 2.6D0, 0.01D0 /

*-----------------------------------------------------------------------

      DO 10 I=1, N
        DO 20 J=1, 8
          DX = -S*X + S*Y
          DY = -X*Z + R*X - Y
          DZ =  X*Y - B*Z
          X = X + DX*DT
          Y = Y + DY*DT
          Z = Z + DZ*DT
   20   CONTINUE
        T(I) = (I-1)*1000
        A(I) = Y + 20.
   10 CONTINUE

*-----------------------------------------------------------------------

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS
      CALL GROPN(IWS)
      CALL GRFRM

      CALL USSTTL('TIME', 'SEC', 'HEAT FLUX', 'W/m|2"')
      CALL USGRPH(N, T, A)

      CALL GRCLS

      END
