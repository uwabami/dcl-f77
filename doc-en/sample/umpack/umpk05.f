*-----------------------------------------------------------------------
      PROGRAM UMPK05

      PARAMETER ( NX=37, NY=37 )
      PARAMETER ( XMIN=  0, XMAX=360, YMIN=-90, YMAX=+90 )
      PARAMETER ( PI=3.141592, DRAD=PI/180, DZ=0.05 )
      PARAMETER ( FACT=10 )

      REAL      P(NX,NY), U(NX,NY), V(NX,NY), ALON(NX), ALAT(NY)

      EXTERNAL  IMOD


      CALL GLRGET( 'RMISS', RMISS )
      CALL GLLSET( 'LMISS', .TRUE. )
     
      DO 10 I = 1, NX
        ALON(I) = XMIN + (XMAX-XMIN) * (I-1) / (NX-1)
   10 CONTINUE

      DO 20 J = 1, NY
        ALAT(J) = YMIN + (YMAX-YMIN) * (J-1) / (NY-1)
   20 CONTINUE

      DO 40 J = 1, NY
        DO 30 I = 1, NX
          SLAT = SIN(ALAT(J)*DRAD)
          P(I,J) = COS(ALON(I)*DRAD) * (1-SLAT**2) * SIN(2*PI*SLAT) 
     +             + DZ
   30   CONTINUE
   40 CONTINUE

      DO 60 J = 1, NY
        DO 50 I = 1, NX
          IF (J.EQ.1 .OR. J.EQ.NY) THEN
            U(I,J) = RMISS
            V(I,J) = RMISS
          ELSE
            U(I,J) = ( P(I,J-1) - P(I,J+1) ) * FACT
            V(I,J) = ( P(IMOD(I,NX-1)+1,J) - P(IMOD(I-2,NX-1)+1,J) )
     +             * FACT
          END IF
   50   CONTINUE
   60 CONTINUE

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN( IWS )
      CALL GRFRM

      CALL GRSWND( XMIN, XMAX, YMIN, YMAX )
      CALL GRSVPT( 0.1, 0.9, 0.1, 0.9 )
      CALL GRSSIM( 0.4, 0.0, 0.0 )
      CALL GRSMPL( 165.0, 60.0, 0.0 )
      CALL GRSTXY( -180.0, 180.0, 0.0, 90.0 )
      CALL GRSTRN( 30 )
      CALL GRSTRF
      CALL SGLSET( 'LCLIP', .TRUE. )

      CALL UMPMAP( 'coast_world' )
      CALL UMPGLB

      CALL UDCNTR( P, NX, NX, NY )

      DO 80 J=1,NY
        DO 70 I=1,NX
          IF (.NOT.(U(I,J).EQ.RMISS .OR. V(I,J).EQ.RMISS)) THEN
            CALL SGLAZU(ALON(I),ALAT(J),ALON(I)+U(I,J),ALAT(J)+V(I,J),
     +           1, 3 )
          END IF
   70   CONTINUE
   80 CONTINUE

      CALL GRCLS

      END
