*-----------------------------------------------------------------------
      PROGRAM UXYZ03


      WRITE(*,*) ' WORKSTATION ID (I) ? ;'
      CALL SGPWSN
      READ(*,*) IWS

      CALL GROPN( IWS )

      CALL GRFRM

      CALL SGSWND( -50.0, +50.0, 1.0E3, 0.4 )
      CALL SGSVPT( 0.2, 0.8, 0.2, 0.8 )
      CALL SGSTRN( 2 )
      CALL SGSTRF

      CALL UXAXDV( 'B', 5.0, 20.0 )
      CALL UXAXDV( 'T', 5.0, 20.0 )
      CALL UXSTTL( 'B', 'LATITUDE', 0.0 )

      CALL ULISET( 'IYTYPE', 3 )
      CALL ULYLOG( 'L', 3, 9 )
      CALL ULYLOG( 'R', 3, 9 )
      CALL UYSTTL( 'L', 'PRESSURE (MB)', 0.0 )

      CALL UXMTTL( 'T', 'UXAXDV/ULYLOG', 0.0 )

      CALL GRCLS

      END
