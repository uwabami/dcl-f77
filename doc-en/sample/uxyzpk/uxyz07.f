*-----------------------------------------------------------------------
      PROGRAM UXYZ07


      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ(*,*) IWS

      CALL GROPN( IWS )

      CALL GRFRM

      CALL SGSWND( 1985.0, 1990.0, -2.0, +2.0 )
      CALL SGSVPT( 0.2, 0.8, 0.3, 0.7 )
      CALL SGSTRN( 1 )
      CALL SGSTRF

      CALL UZRSET( 'UYUSER', 0.0 )
      CALL UZLSET( 'LBTWN', .TRUE. )
      CALL UXSFMT( '(I4)' )
      CALL UXAXDV( 'U', 0.25, 1.0 )
      CALL UZLSET( 'LBTWN', .FALSE. )
      CALL UXSTTL( 'U', 'YEAR', +1.0 )

      CALL UYAXDV( 'L', 0.25, 1.0 )
      CALL UYSTTL( 'L', 'S.O.I.', 0.0 )

      CALL UXMTTL( 'T', 'UXAXDV/UYAXDV', 0.0 )

      CALL GRCLS

      END
