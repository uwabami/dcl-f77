*-----------------------------------------------------------------------
      PROGRAM U2DF04

      PARAMETER ( NX=19, NY=19 )
      PARAMETER ( XMIN=0, XMAX=360, YMIN=-90, YMAX=90 )
      PARAMETER ( PI=3.141592, DRAD=PI/180, DZ=0.05 )

      REAL      P(NX,NY)


      DO 20 J = 1, NY
        DO 10 I = 1, NX
          ALON = ( XMIN + (XMAX-XMIN) * (I-1) / (NX-1) ) * DRAD
          ALAT = ( YMIN + (YMAX-YMIN) * (J-1) / (NY-1) ) * DRAD
          SLAT = SIN(ALAT)
          P(I,J) = COS(ALON) * (1-SLAT**2) * SIN(2*PI*SLAT) + DZ
   10   CONTINUE
   20 CONTINUE

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN( IWS )

      CALL SGLSET( 'LSOFTF', .TRUE. )

      CALL GRFRM

      CALL GRSWND( XMIN, XMAX, YMIN, YMAX )
      CALL GRSVPT( 0.2, 0.8, 0.2, 0.8 )
      CALL GRSTRN( 1 )
      CALL GRSTRF

      CALL USDAXS

      CALL UDCNTR( P, NX, NX, NY )
      CALL UETONE( P, NX, NX, NY )

      CALL GRCLS

      END
