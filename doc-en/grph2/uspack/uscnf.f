*=======================================================================
*     USGRPH  FIGURE FOR STANDARD UNIT.
*=======================================================================

      WRITE(*,*) 'workstation no.? '
      READ (*,*) IWS
      CALL GROPN(IWS)
      CALL GRFRM

*------------------------ USPACK ROUTINES ------------------------------

      CALL USCSET('CXTTL' , 'X-TITLE')
      CALL USCSET('CYTTL' , 'Y-TITLE')
      CALL USCSET('CXUNIT', 'XSUB')
      CALL USCSET('CYUNIT', 'YSUB')
      CALL SGPSET('UYMIN' ,    0.)
      CALL SGPSET('UYMAX' , 5000.)
      CALL SGPSET('UXMIN' ,    0.)
      CALL SGPSET('UXMAX' , 2000.)
      CALL UZPSET('INNER' , -1)

      CALL USPFIT
      CALL GRSTRF
      CALL USDAXS

*-----------------------------------------------------------------------

      CALL UZPGET('RSIZET2', RSZT2)
      CALL UZPGET('RSIZEL1', RSZL1)
      CALL UZPGET('RSIZEC1', RSZC1)
      CALL UZPGET('PAD1',    PAD1)
      CALL USPGET('MXDGTY',  MXDGTY)
      CALL USPGET('MXDGTSY', MXDGTS)
      CALL USPGET('SOFFYLT', SOFFY)
      CALL SGQVPT(VXMIN, VXMAX, VYMIN, VYMAX)

      X1 = RSZT2
      X2 = (MXDGTY+PAD1) * RSZL1
      X3 = (1+PAD1)      * RSZC1

      Y1 = RSZT2
      Y2 = (1+PAD1) * RSZL1
      Y3 = (1+PAD1) * RSZL1
      Y4 = (1+PAD1) * RSZC1

      SX = RSZT2+(MXDGTS+PAD1)*RSZL1
      SY = SOFFY

*----------------------------- Y-AXIS ----------------------------------

      CALL SGPSET('LPROP', .FALSE.)
      CALL SGSTXS(0.02)

      VY1 = (VYMIN+VYMAX)/2.
      VY2 = VY1 + (VYMAX-VYMIN)/5.*2.
      VY0 = (VY1+VY2)/2

      VX1 = VXMIN - X1
      VX2 = VX1   - X2
      VX3 = VX2   - X3

      CALL SGLNV(VX1, VY1, VX1, VY2)
      CALL SGLNV(VX2, VY1, VX2, VY2)
      CALL SGLNV(VX3, VY1, VX3, VY2)

      CALL SGLAV(VX3, VY0, VX2, VY0)
      CALL SGLAV(VX2, VY0, VX3, VY0)

      CALL SGLAV(VX2, VY0, VX1, VY0)
      CALL SGLAV(VX1, VY0, VX2, VY0)

      CALL SGLAV(VXMIN+0.02, VY0, VXMIN, VY0)

      CALL SGTXV(VXMIN+0.02,   VY0+0.02, 'x_1"')
      CALL SGTXV((VX1+VX2)/2., VY0+0.02, 'x_2"')
      CALL SGTXV((VX2+VX3)/2., VY0+0.02, 'x_3"')

*---------------------------- SUB LABEL --------------------------------

      VY0 = VYMAX + SY*1.5
      VY1 = VYMAX + SY*2

      CALL SGLNV(VXMIN   , VYMAX, VXMIN   , VY1)
      CALL SGLNV(VXMIN-SX, VYMAX, VXMIN-SX, VY1)

      CALL SGLAV(VXMIN-SX, VY0, VXMIN   , VY0)
      CALL SGLAV(VXMIN   , VY0, VXMIN-SX, VY0)
      CALL SGTXV(VXMIN-SX/2., VY0+0.02, 's_x"')

      CALL SGLNV(VXMIN-X2/2., VYMAX+SY, VXMIN+X2/2., VYMAX+SY)
      CALL SGLAV(VXMIN+X2/3., VYMAX+SY, VXMIN+X2/3., VYMAX)
      CALL SGLAV(VXMIN+X2/3., VYMAX   , VXMIN+X2/3., VYMAX+SY)

      CALL SGTXV(VXMIN+X2/2., VYMAX+SY/2., 's_y"')

*----------------------------- X-AXIS ----------------------------------

      VY1 = VYMIN - Y1
      VY2 = VY1  - Y2
      VY3 = VY2  - Y3
      VY4 = VY3  - Y4

      VX1 = VXMIN + (VXMAX-VXMIN)*2/3.
      VX2 = VXMAX + X2
      VX0 = VX2 - 0.03

      CALL SGLNV(VX1, VY1, VX2, VY1)
      CALL SGLNV(VX1, VY2, VX2, VY2)
      CALL SGLNV(VX1, VY3, VX2, VY3)
      CALL SGLNV(VX1, VY4, VX2, VY4)
      CALL SGLNV(VXMAX, VYMIN, VX2, VYMIN)

      CALL SGLAV(VX0, VY4, VX0, VY3)
      CALL SGLAV(VX0, VY3, VX0, VY4)
      
      CALL SGLAV(VX0, VY3, VX0, VY2)
      CALL SGLAV(VX0, VY2, VX0, VY3)
      
      CALL SGLAV(VX0, VY2, VX0, VY1)
      CALL SGLAV(VX0, VY1, VX0, VY2)
      
      CALL SGLAV(VX0, VYMIN+0.02, VX0, VYMIN)

      CALL SGTXV(VX2, VYMIN+0.02,   'y_1"')
      CALL SGTXV(VX2, (VY1+VY2)/2., 'y_2"')
      CALL SGTXV(VX2, (VY2+VY3)/2., 'y_3"')
      CALL SGTXV(VX2, (VY3+VY4)/2., 'y_4"')

      CALL GRCLS
      STOP
      END
