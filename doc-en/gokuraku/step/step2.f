*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM STEP2

      PARAMETER( NMAX=100 )
      REAL X(NMAX), Y(NMAX)

*-- データ ----
      ISEED = 1
      X(1) = 2.*(RNGU0(ISEED)-0.5)
      DO 10 N=2,NMAX
        X(N)   = 2.*(RNGU0(ISEED)-0.5)
        Y(N-1) = X(N)
   10 CONTINUE
      Y(NMAX) = X(1)

*-- グラフ ----
      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN( IWS )
      CALL GRFRM

      CALL USSPNT( NMAX, X, Y )
      CALL USPFIT
      CALL GRSTRF

      CALL USSTTL( 'X-TITLE', 'x-unit', 'Y-TITLE', 'y-unit' )
      CALL USDAXS

      CALL UUSMKI( 5 )
      CALL UUSMKS( 0.015 )
      CALL UUMRK( NMAX/4, X( 1), Y( 1) )

      CALL UUSMKT( 2 )
      CALL UUMRK( NMAX/4, X(26), Y(26) )

      CALL UUSMKT( 3 )
      CALL UUMRK( NMAX/4, X(51), Y(51) )

      CALL UUSMKT( 4 )
      CALL UUMRK( NMAX/4, X(76), Y(76) )

      CALL GRCLS

      END
