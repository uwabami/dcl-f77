      PROGRAM LAY2

      PARAMETER( NMAX=401, XMIN=1600, XMAX=2000 )
      REAL X(NMAX), Y(NMAX)

*-- データ ----
      Y0 = 0.5
      DO 10 N=1,NMAX
        X(N) = XMIN + (XMAX-XMIN)*(N-1)/(NMAX-1)
        Y(N) = 5.*Y0 + 10.
        Y0   = 3.7*Y0*(1.-Y0)
   10 CONTINUE

*-- グラフ ----
      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN( IWS )
      CALL SGLSET( 'LFULL', .TRUE. )
      CALL GRFRM

      CALL GRSWND( XMIN, XMAX,  11.0, 15.0 )
      CALL GRSVPT( 0.15, 0.95,  0.15, 0.65 )
      CALL USPFIT
      CALL GRSTRF

      CALL USSTTL( 'TIME', 'YEAR', 'TEMPERATURE', 'DEG' )
      CALL USDAXS

      CALL UULIN( NMAX, X, Y )

      CALL GRCLS

      END
