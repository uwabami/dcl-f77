      PROGRAM U2D3

      PARAMETER( NX=21, NY=21 )
      PARAMETER( XMIN=-1, XMAX=1, YMIN=-1, YMAX=1 )
      PARAMETER( DX=(XMAX-XMIN)/(NX-1), DY=(YMAX-YMIN)/(NY-1) )
      REAL U(NX,NY), V(NX,NY)

*-- データ ----
      DO 10 J=1,NY
      DO 10 I=1,NX
        X = XMIN + (I-1)*DX
        Y = YMIN + (J-1)*DY
        U(I,J) =   X
        V(I,J) = - Y
   10 CONTINUE

*-- グラフ ----
      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN( IWS )
      CALL GRFRM

      CALL GRSWND( XMIN, XMAX, YMIN, YMAX )
      CALL USPFIT
      CALL GRSTRF

      CALL USSTTL( 'X', 'km', 'Y', 'km' )
      CALL USDAXS

      CALL UGVECT( U, NX, V, NX, NX, NY )

      CALL GRCLS

      END
