      PROGRAM QUICK4

      PARAMETER( NX=37, NY=37 )
      PARAMETER( XMIN=0, XMAX=360, YMIN=-90, YMAX=90 )
      PARAMETER( PI=3.14159, DRAD=PI/180 )
      REAL P(NX,NY)

*-- 球面調和関数 ----
      DO 20 J=1,NY
        DO 10 I=1,NX
          ALON = ( XMIN + (XMAX-XMIN)*(I-1)/(NX-1) ) * DRAD
          ALAT = ( YMIN + (YMAX-YMIN)*(J-1)/(NY-1) ) * DRAD
          SLAT = SIN(ALAT)
          P(I,J) = 3*SQRT(1-SLAT**2)*SLAT*COS(ALON) - 0.5*(3*SLAT**2-1)
   10   CONTINUE
   20 CONTINUE

*-- グラフ ----
      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN( IWS )
      CALL GRFRM

      CALL GRSWND( XMIN, XMAX, YMIN, YMAX )
      CALL GRSVPT(  0.2,  0.8,  0.2,  0.8 )
      CALL GRSTRN( 1 )
      CALL GRSTRF

      CALL UETONE( P, NX, NX, NY )
      CALL USDAXS
      CALL UDCNTR( P, NX, NX, NY )

      CALL GRCLS

      END
