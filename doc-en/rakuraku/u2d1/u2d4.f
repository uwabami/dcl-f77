      PROGRAM U2D4

      PARAMETER( NX=37, NY=37 )
      PARAMETER( XMIN=0, XMAX=360, YMIN=-90, YMAX=90 )
      PARAMETER( PI=3.14159, DRAD=PI/180 )
      PARAMETER( KXMN=9, KXMX=33, KYMN=7, KYMX=31 )
      REAL P(NX,NY), ALON(NX), ALAT(NY)

       DO 10 I=1,NX
        ALON(I) = XMIN + (XMAX-XMIN)*(I-1)/(NX-1)
   10 CONTINUE
      DO 20 J=1,NY
        ALAT(J) = YMIN + (YMAX-YMIN)*(J-1)/(NY-1)
   20 CONTINUE

      DO 30 J=1,NY
        SLAT = SIN(ALAT(J)*DRAD)
      DO 30 I=1,NX
        P(I,J) = 3*SQRT(1-SLAT**2)*SLAT*COS(ALON(I)*DRAD)
     +         - 0.5*(3*SLAT**2-1)
   30 CONTINUE

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN( IWS )
      CALL GRFRM

      CALL GRSWND( ALON(KXMN), ALON(KXMX), ALAT(KYMN), ALAT(KYMX) )
      CALL GRSVPT(        0.2,        0.8,        0.2,        0.8 )
      CALL GRSTRN( 1 )
      CALL GRSTRF

      CALL USDAXS
      CALL UDCNTR( P(KXMN,KYMN), NX, KXMX-KXMN+1, KYMX-KYMN+1 )

      CALL GRCLS

      END
