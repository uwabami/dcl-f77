      PROGRAM U2D3

      PARAMETER( NX=37, NY=37, MY=7 )
      PARAMETER( XMIN=0, XMAX=360, YMIN=-90, YMAX=90 )
      PARAMETER( PI=3.14159, DRAD=PI/180 )
      REAL      P(NX,NY), UY1(NY), UY2(MY)
      CHARACTER CH(MY)*3

      DATA      CH/ 'SP ', '60S', '30S', 'EQ ', '30N', '60N', 'NP ' /

      DO 10 J=1,NY
      DO 10 I=1,NX
        ALON = ( XMIN + (XMAX-XMIN)*(I-1)/(NX-1) ) * DRAD
        ALAT = ( YMIN + (YMAX-YMIN)*(J-1)/(NY-1) ) * DRAD
        SLAT = SIN(ALAT)
        UY1(J) = SLAT
        P(I,J) = 3*SQRT(1-SLAT**2)*SLAT*COS(ALON) - 0.5*(3*SLAT**2-1)
   10 CONTINUE

      DO 20 J=1,MY
        UY2(J) = SIN( ( YMIN + (YMAX-YMIN)*(J-1)/(MY-1) ) * DRAD )
   20 CONTINUE

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN( IWS )
      CALL GRFRM

      CALL GRSWND( XMIN, XMAX, -1.,  1. )
      CALL GRSVPT(  0.2,  0.8, 0.2, 0.8 )
      CALL GRSTRN( 1 )
      CALL GRSTRF

      CALL UXAXDV( 'B', 10., 60. )
      CALL UXAXDV( 'T', 10., 60. )
      CALL UXSTTL( 'B', 'LONGITUDE', 0. )

      CALL UYAXDV( 'L', 0.1, 0.5 )
      CALL UYSTTL( 'L', 'SINE LATITUDE', 0. )

      CALL UZLSET( 'LABELYR', .TRUE. )
      CALL UYAXLB( 'R', UY1, NY, UY2, CH, 3, MY )
      CALL UYSTTL( 'R', 'LATITUDE', 0. )

      CALL UWSGXB( XMIN, XMAX, NX )
      CALL UWSGYA( UY1, NY )

      CALL UDCNTR( P, NX, NX, NY )

      CALL GRCLS

      END
