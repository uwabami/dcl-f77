      PROGRAM KIHON6

      PARAMETER( NMAX=50 )
      REAL      X(0:NMAX), Y(0:NMAX)

      R    = 3.7
      X(0) = 1950
      Y(0) = 0.5
      DO 10 N=0,NMAX-1
        X(N+1) = X(N) + 1
        Y(N+1) = R*Y(N)*(1.-Y(N))
   10 CONTINUE

      DO 20 N=0,NMAX
        Y(N) = -4*Y(N) + 20.
   20 CONTINUE

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN( IWS )
      CALL GRFRM

      CALL GRSWND( 1950., 2000., 15., 20. )
      CALL GRSVPT(   0.2,   0.8, 0.2, 0.8 )
      CALL GRSTRN( 1 )
      CALL GRSTRF

      CALL USDAXS
      CALL SGPLU( NMAX+1, X, Y )

      CALL GRCLS

      END
