      PROGRAM KIHONA

      PARAMETER( NMAX=40 )
      PARAMETER( PI=3.14159 )
      PARAMETER( XMIN=0., XMAX=4*PI, YMIN=-1., YMAX=1. )
      REAL X(0:NMAX), Y(0:NMAX)

      DT = XMAX/NMAX
      DO 10 N=0,NMAX
        X(N) = N*DT
        Y(N) = SIN(X(N))
   10 CONTINUE

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL SGOPN( IWS )
      CALL SGFRM

      CALL SGLSET( 'LCHAR', .TRUE. )

*-- ラベルつき折れ線 ----
      CALL SGSWND( XMIN, XMAX, YMIN, YMAX )
      CALL SGSVPT(   0.,   1.,  0.9,  1.0 )
      CALL SGSTRN( 1 )
      CALL SGSTRF

      CALL SGSPLC( 'A' )
      CALL SGSPLT( 1 )
      CALL SGPLU( NMAX+1, X, Y )

*-- 順序ラベル: A,B,C,... ----
      DO 20 I=1,2
        VYMIN = 0.9 - 0.1*I
        VYMAX = VYMIN + 0.1
        CALL SGSWND( XMIN, XMAX,  YMIN,  YMAX )
        CALL SGSVPT(   0.,   1., VYMIN, VYMAX )
        CALL SGSTRN( 1 )
        CALL SGSTRF

        CALL SGNPLC
        CALL SGPLU( NMAX+1, X, Y )
   20 CONTINUE

*-- 順序ラベル: K=1,K=2,... ----
      CALL SGSPLC( 'K=1' )

      DO 30 I=3,4
        VYMIN = 0.9 - 0.1*I
        VYMAX = VYMIN + 0.1
        CALL SGSWND( XMIN, XMAX,  YMIN,  YMAX )
        CALL SGSVPT(   0.,   1., VYMIN, VYMAX )
        CALL SGSTRN( 1 )
        CALL SGSTRF

        CALL SGSPLT( I-1 )
        CALL SGPLU( NMAX+1, X, Y )
        CALL SGNPLC
   30 CONTINUE

        CALL SGSPLT( 1 )

*--  ラベルの文字列の高さ ----
      CALL SGSWND( XMIN, XMAX, YMIN, YMAX )
      CALL SGSVPT(   0.,   1.,  0.4,  0.5 )
      CALL SGSTRN( 1 )
      CALL SGSTRF

      CALL SGSPLS( 0.01 )
      CALL SGSPLC( 'small' )
      CALL SGPLU( NMAX+1, X, Y )

      CALL SGSPLS( 0.02 )

*--  ラベルの角度 ----
      CALL SGSWND( XMIN, XMAX, YMIN, YMAX )
      CALL SGSVPT(   0.,   1.,  0.3,  0.4 )
      CALL SGSTRN( 1 )
      CALL SGSTRF

      CALL SGLSET( 'LROT', .TRUE. )
      CALL SGISET( 'IROT', 90 )
      CALL SGSPLC( 'ROT' )
      CALL SGPLU( NMAX+1, X, Y )

      CALL SGLSET( 'LROT', .FALSE. )

*--  ラベルの間隔 ----
      CALL SGSWND( XMIN, XMAX, YMIN, YMAX )
      CALL SGSVPT(   0.,   1.,  0.2,  0.3 )
      CALL SGSTRN( 1 )
      CALL SGSTRF

      CALL SGRSET( 'CWL', 5. )
      CALL SGSPLC( 'CWL' )
      CALL SGPLU( NMAX+1, X, Y )

      CALL SGRSET( 'CWL', 30. )

*--  ラベルの書き始め ----
      CALL SGSWND( XMIN, XMAX, YMIN, YMAX )
      CALL SGSVPT(   0.,   1.,  0.1,  0.2 )
      CALL SGSTRN( 1 )
      CALL SGSTRF

      CALL SGRSET( 'FFCT', 0.9 )
      CALL SGSPLC( 'FCT' )
      CALL SGPLU( NMAX+1, X, Y )

      CALL SGCLS

      END
