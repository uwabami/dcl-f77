      PROGRAM KIHON7

      CALL SWCSTX('FNAME','KIHON7')
      CALL SWLSTX('LSEP',.TRUE.)

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL SGOPN( IWS )
C      CALL SGLSET( 'LCLIP', .TRUE. )
*-- frame 1 ----
      CALL SGFRM
*                  XMIN,  XMAX,  YMIN, YMAX
      CALL SGSWND( -100., 100., -100., 100. )
      CALL SGSVPT(  0.0,   1.0,   0.0,  1.0 )
      CALL SGSTRN( 1 )
      CALL SGSTRF

      CALL APLOT

*-- frame 2 ----
      CALL SGFRM
*-- 左上 ----      XMIN,  XMAX,  YMIN, YMAX
      CALL SGSWND( -100., 100., -100., 100. )
      CALL SGSVPT(   0.1,  0.4,   0.6,  0.9 )
      CALL SGSTRN( 1 )
      CALL SGSTRF

      CALL APLOT

*-- 右上 ----       XMIN, XMAX, YMIN, YMAX
      CALL SGSWND( -100., 100., -40.,  40. )
      CALL SGSVPT(   0.6,  0.9,  0.6,  0.9 )
      CALL SGSTRN( 1 )
      CALL SGSTRF

      CALL APLOT

*-- 左下 ----       XMIN, XMAX,  YMIN, YMAX
      CALL SGSWND( -100., 100., -200.,   0. )
      CALL SGSVPT(   0.1,  0.4,   0.1,  0.4 )
      CALL SGSTRN( 1 )
      CALL SGSTRF

      CALL APLOT

      
*-- 右下 ----       XMIN, XMAX, YMIN,  YMAX
      CALL SGSWND( -100., 100.,   0., -200. )
      CALL SGSVPT(   0.6,  0.9,  0.1,   0.4 )
      CALL SGSTRN( 1 )
      CALL SGSTRF

      CALL APLOT

      CALL SGCLS

      END
*-----------------------------------------------------------------------
      SUBROUTINE APLOT

      PARAMETER( NMAX=40 )
      REAL X(0:NMAX), Y(0:NMAX)

      CALL SLPVPR( 1 )

*-- 円形 ----
      DT = 2.*3.14159 / NMAX
      DO 10 N=0,NMAX
        X(N) = 40.*SIN(N*DT)
        Y(N) = 40.*COS(N*DT)
   10 CONTINUE

      CALL SGPLU( NMAX+1, X, Y )

*-- 三角形 ----
      DT = 2.*3.14159 / 3
      DO 20 N=0,3
        X(N) = 40.*SIN(N*DT)
        Y(N) = 40.*COS(N*DT)
   20 CONTINUE

      CALL SGPLU( 4, X, Y )

*-- 文字列 ----
      CALL SGTXU( 0., 0., 'SGTXU' )

      RETURN
      END
