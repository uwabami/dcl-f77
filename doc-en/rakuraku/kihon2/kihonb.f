      PROGRAM KIHONB

      PARAMETER( NMAX=9 )
      REAL Y(NMAX)
      CHARACTER GREEK1*10, GREEK2*10, SYMBOL*10, USGI*3

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL SGOPN(IWS)
      CALL SGFRM

      X1 = 0.1
      X2 = 0.9
      XC = 0.5

*-- 鐃緒申鐃緒申 ----
      DO 10 N=1,NMAX
        Y(N) = 0.1*(10-N)
        CALL SGLNV( X1, Y(N), X2, Y(N) )
  10  CONTINUE

      CALL SGLNV( XC, 0.05, XC, 0.95 )

*-- 鐃叔フワ申鐃緒申鐃緒申 ----
      CALL SGTXV( XC, Y(1), 'SGTXV|SUP"RST_SUB"' )

*-- 添鐃緒申 ----
      CALL SGLSET( 'LCNTL', .TRUE. )
      CALL SGTXV( XC, Y(2), 'SGTXV|SUP"RST_SUB"' )

      CALL SGRSET( 'SMALL', 0.5 )
      CALL SGRSET( 'SHIFT', 0.5 )
      CALL SGTXV( XC, Y(3), 'SGTXV|SUP"RST_SUB"' )

*-- 鐃春ワ申鐃緒申鐃緒申 ----
      CALL SGTXV( XC, Y(4), 'ABCDEFG abcdefg' )

      CALL SGISET( 'IFONT', 2 )
      CALL SGTXV( XC, Y(5), 'ABCDEFG abcdefg' )

      CALL SGSTXI( 3 )
      CALL SGTXV( XC, Y(6), 'ABCDEFG abcdefg' )

      CALL SGISET( 'IFONT', 1 )
      CALL SGSTXI( 1 )

*-- 鐃緒申鐃所シ鐃緒申文鐃緒申 ----
      GREEK1 = USGI(128)//USGI(129)//USGI(130)//USGI(131)//USGI(132)//
     +         USGI(133)//USGI(134)//USGI(135)//USGI(136)//USGI(137)
      GREEK2 = USGI(152)//USGI(153)//USGI(154)//USGI(155)//USGI(156)//
     +         USGI(157)//USGI(158)//USGI(159)//USGI(160)//USGI(161)

      CALL SGTXV( XC, Y(7), GREEK1 )
      CALL SGTXV( XC, Y(8), GREEK2 )

*-- 鐃獣殊記鐃緒申 ----
      SYMBOL = USGI(189)//USGI(190)//USGI(191)//USGI(192)//USGI(193)//
     +         USGI(210)//USGI(211)//USGI(212)//USGI(217)//USGI(218)

      CALL SGTXV( XC, Y(9), SYMBOL )

      CALL SGCLS

      END
