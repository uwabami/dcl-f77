      PROGRAM MAP3D5

      PARAMETER( NX=21, NY=21 )
      PARAMETER(  XMIN=-10,  XMAX= 10,  YMIN=-10,  YMAX= 10 )
      PARAMETER( VXMIN=0.2, VXMAX=0.8, VYMIN=0.1, VYMAX=0.5 )
      PARAMETER(  ZMIN=0.0,  ZMAX= 20 )
      PARAMETER( VZMIN=0.0, VZMAX=0.6 )
      PARAMETER( XVP3=-0.7, YVP3=-0.7, ZVP3=1.2 )
      PARAMETER( XFC3=(VXMAX-VXMIN)/2, YFC3=(VYMAX-VYMIN)/2 )
      PARAMETER( ZFC3=(VZMAX-VZMIN)/2 )
      PARAMETER( DX1=1, DX2=5, DY1=1, DY2=4, DZ1=1, DZ2=5 )
      PARAMETER( KMAX=5, PMIN=0, PMAX=1 )
      REAL U(NX,NY), V(NX,NY), P(NX,NY)

      DO 10 J=1,NY
      DO 10 I=1,NX
        X = XMIN + (XMAX-XMIN)*(I-1)/(NX-1)
        Y = YMIN + (YMAX-YMIN)*(J-1)/(NY-1)
        U(I,J) =  X
        V(I,J) = -Y
        P(I,J) = EXP( -X**2/64 -Y**2/25 )
   10 CONTINUE

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL SGOPN( IWS )
      CALL SGFRM

*-- X-Y 平面: 下レベル ----
      CALL SGSWND(  XMIN,  XMAX,  YMIN,  YMAX )
      CALL SGSVPT( VXMIN, VXMAX, VYMIN, VYMAX )
      CALL SGSTRN( 1 )
      CALL SGSTRF

      CALL SCSPLN( 1, 2, VZMIN )
      CALL SCSEYE( XVP3, YVP3, ZVP3 )
      CALL SCSOBJ( XFC3, YFC3, ZFC3 )
      CALL SCSPRJ

      CALL UXAXDV( 'B', DX1, DX2 )
      CALL UXAXDV( 'T', DX1, DX2 )
      CALL UXSTTL( 'B', 'X-axis', 0. )

      CALL UYAXDV( 'L', DY1, DY2 )
      CALL UYAXDV( 'R', DY1, DY2 )
      CALL UYSTTL( 'L', 'Y-axis', 0. )

      CALL UGPSET( 'RSIZET', 0.014 )
      CALL UGVECT( U, NX, V, NX, NX, NY )

*-- X-Y 平面: 上レベル ----
      VZLEV = VZMIN + (VZMAX-VZMIN)*.6
      CALL SCSPLN( 1, 2, VZLEV )
      CALL SCSPRJ

      DP = (PMAX-PMIN)/KMAX
      DO 20 K=1,KMAX
        TLEV1 = (K-1)*DP
        TLEV2 = TLEV1 + DP
        IPAT  = 600 + K - 1
        CALL UESTLV( TLEV1, TLEV2, IPAT )
   20 CONTINUE
      CALL UETONE( P, NX, NX, NY )

      CALL UDPSET( 'LMSG' , .FALSE. )
      CALL UDGCLB( P, NX, NX, NY, 0.1 )
      CALL UDCNTR( P, NX, NX, NY )
      CALL SLPVPR( 1 )

*-- X-Z 平面 ----
      CALL SGSWND(  XMIN,  XMAX,  ZMIN,  ZMAX )
      CALL SGSVPT( VXMIN, VXMAX, VZMIN, VZMAX )
      CALL SGSTRN( 1 )
      CALL SGSTRF

      CALL SCSPLN( 1, 3, VYMAX )
      CALL SCSPRJ

      CALL UZINIT
      CALL UYAXDV( 'L', DZ1, DZ2 )
      CALL UYSTTL( 'L', 'Z-axis', 0. )

      CALL SGCLS

      END
