      PROGRAM USPAC1

      PARAMETER( NMAX=50 )
      REAL X(0:NMAX), Y(0:NMAX)

      R    = 3.7
      X(0) = 1950.
      Y(0) = 0.5
      DO 10 N=0,NMAX-1
        X(N+1) = X(N) + 1.
        Y(N+1) = R*Y(N)*(1.-Y(N))
   10 CONTINUE

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN( IWS )
      CALL GRFRM

      CALL USSTTL( 'TIME', 'YEAR', 'HEAT FLUX', 'W/m|2"' )
      CALL USGRPH( NMAX+1, X, Y )

      CALL GRCLS

      END
