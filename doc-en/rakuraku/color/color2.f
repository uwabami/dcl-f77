      PROGRAM COLOR2

      PARAMETER( NMAX=50 )
      REAL      X(0:NMAX), Y(0:NMAX)
      CHARACTER USGI*3, CTTL*32

      R    = 3.7
      X(0) = 1950
      Y(0) = 0.5
      DO 10 N=0,NMAX-1
        X(N+1) = X(N) + 1
        Y(N+1) = R*Y(N)*(1.-Y(N))
   10 CONTINUE
      YAVE = RAVE( Y, NMAX+1, 1 )
      DO 20 N=0,NMAX
        Y(N) = -4*(Y(N)-YAVE)
   20 CONTINUE

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN( IWS )
      CALL GRFRM

      CALL GRSWND( X(0), X(NMAX), -1.5, 2.0 )
      CALL GRSVPT(  0.2,     0.9,  0.2, 0.9 )
      CALL GRSTRN( 1 )
      CALL GRSTRF

      CALL SGISET( 'IFONT', 2 )
      CALL UZISET( 'INDEXT1', 402 )
      CALL UZISET( 'INDEXT2', 404 )
      CALL UZISET( 'INDEXL1', 303 )
      CALL UZISET( 'INDEXL2', 123 )
      CALL UXAXDV( 'B', 2., 10. )
      CALL UXAXDV( 'T', 2., 10. )
      CALL UXSTTL( 'B', 'YEAR', 0. )

      CALL UYAXDV( 'L', 0.1, 0.5 )
      CALL UYAXDV( 'R', 0.1, 0.5 )
      CTTL = USGI(131) // 'T [K]'
      CALL UYSTTL( 'L',CTTL, 0. )

      CALL UXMTTL( 'T', 'INTERANNUAL VARIATION', 0. )

      CALL SGSPLI( 24 )
      CALL SGPLU( NMAX+1, X, Y )
      CALL GRCLS

      END
